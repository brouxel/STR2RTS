#include "PEG42-iDCTcompare_nocache.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8811Post_CollapsedDataParallel_2_8757;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710;
buffer_int_t SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[42];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[42];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8767Pre_CollapsedDataParallel_1_8756;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_join[3];
buffer_float_t Pre_CollapsedDataParallel_1_8756WEIGHTED_ROUND_ROBIN_Splitter_8810;
buffer_float_t Post_CollapsedDataParallel_2_8757WEIGHTED_ROUND_ROBIN_Splitter_8820;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[42];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_8759AnonFilter_a2_8711;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[8];
buffer_int_t AnonFilter_a0_8683DUPLICATE_Splitter_8758;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[42];
buffer_int_t SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8821WEIGHTED_ROUND_ROBIN_Splitter_8830;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[8];


iDCT_2D_reference_coarse_8686_t iDCT_2D_reference_coarse_8686_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8812_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8813_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8814_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8815_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8816_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8817_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8818_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8819_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8822_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8823_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8824_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8825_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8826_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8827_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8828_s;
iDCT_2D_reference_coarse_8686_t iDCT_1D_reference_fine_8829_s;
iDCT8x8_1D_col_fast_8710_t iDCT8x8_1D_col_fast_8710_s;
AnonFilter_a2_8711_t AnonFilter_a2_8711_s;

void AnonFilter_a0_8683(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_8683DUPLICATE_Splitter_8758, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_8686(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_8686_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_8686_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_8768(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8769(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8770(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8771(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8772(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8773(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8774(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8775(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8776(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8777(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8778(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8779(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8780(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8781(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8782(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8783(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8784(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8785(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8786(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8787(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8788(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8789(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8790(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8791(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8792(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8793(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8794(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8795(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8796(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8797(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8798(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8799(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8800(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8801(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8802(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8803(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8804(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8805(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8806(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8807(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8808(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8809(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[41])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8766() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 42, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8767() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 42, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8767Pre_CollapsedDataParallel_1_8756, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_8756(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_8756WEIGHTED_ROUND_ROBIN_Splitter_8810, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8767Pre_CollapsedDataParallel_1_8756, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8767Pre_CollapsedDataParallel_1_8756) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8812(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8812_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8813(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8813_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8814(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8814_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8815(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8815_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8816(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8816_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8817(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8817_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8818(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8818_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8819(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8819_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8810() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8756WEIGHTED_ROUND_ROBIN_Splitter_8810));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8811() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8811Post_CollapsedDataParallel_2_8757, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8757(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_8757WEIGHTED_ROUND_ROBIN_Splitter_8820, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8811Post_CollapsedDataParallel_2_8757, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8811Post_CollapsedDataParallel_2_8757) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8822(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8822_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8823(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8823_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8824(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8824_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8825(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8825_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8826(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8826_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8827(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8827_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8828(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8828_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8829(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8829_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8820() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_8757WEIGHTED_ROUND_ROBIN_Splitter_8820));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8821() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8821WEIGHTED_ROUND_ROBIN_Splitter_8830, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_8832(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8833(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8834(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8835(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8836(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8837(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8838(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8839(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8840(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8841(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8842(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8843(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8844(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8845(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8846(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8847(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8848(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8849(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8850(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8851(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8852(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8853(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8854(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8855(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8856(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8857(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8858(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8859(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8860(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8861(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8862(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8863(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8864(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8865(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8866(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8867(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8868(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8869(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8870(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8871(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8872(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8873(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8830() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 42, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8821WEIGHTED_ROUND_ROBIN_Splitter_8830));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8831() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 42, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_8876(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0], 6) ; 
		x3 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0], 2) ; 
		x4 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0], 1) ; 
		x5 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0], 7) ; 
		x6 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0], 5) ; 
		x7 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8877(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1], 6) ; 
		x3 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1], 2) ; 
		x4 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1], 1) ; 
		x5 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1], 7) ; 
		x6 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1], 5) ; 
		x7 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8878(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2], 6) ; 
		x3 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2], 2) ; 
		x4 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2], 1) ; 
		x5 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2], 7) ; 
		x6 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2], 5) ; 
		x7 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8879(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3], 6) ; 
		x3 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3], 2) ; 
		x4 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3], 1) ; 
		x5 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3], 7) ; 
		x6 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3], 5) ; 
		x7 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8880(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4], 6) ; 
		x3 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4], 2) ; 
		x4 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4], 1) ; 
		x5 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4], 7) ; 
		x6 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4], 5) ; 
		x7 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8881(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5], 6) ; 
		x3 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5], 2) ; 
		x4 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5], 1) ; 
		x5 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5], 7) ; 
		x6 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5], 5) ; 
		x7 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8882(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6], 6) ; 
		x3 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6], 2) ; 
		x4 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6], 1) ; 
		x5 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6], 7) ; 
		x6 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6], 5) ; 
		x7 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8883(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7], 6) ; 
		x3 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7], 2) ; 
		x4 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7], 1) ; 
		x5 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7], 7) ; 
		x6 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7], 5) ; 
		x7 = peek_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8874() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8875() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, pop_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_8710(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_8710_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_8710_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_8710_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_8710_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_8710_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_8710_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_8710_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_8710_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_8710_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_join[2], iDCT8x8_1D_col_fast_8710_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_8758() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_8683DUPLICATE_Splitter_8758);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8759() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_8759AnonFilter_a2_8711, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_8711(){
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_8759AnonFilter_a2_8711) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_8759AnonFilter_a2_8711) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_8759AnonFilter_a2_8711) ; 
		AnonFilter_a2_8711_s.count = (AnonFilter_a2_8711_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_8711_s.errors = (AnonFilter_a2_8711_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_8711_s.errors / AnonFilter_a2_8711_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_8711_s.errors = (AnonFilter_a2_8711_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_8711_s.errors / AnonFilter_a2_8711_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8811Post_CollapsedDataParallel_2_8757);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_8875iDCT8x8_1D_col_fast_8710);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 42, __iter_init_1_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 42, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8767Pre_CollapsedDataParallel_1_8756);
	FOR(int, __iter_init_4_, 0, <, 3, __iter_init_4_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8684_8760_8884_8891_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8756WEIGHTED_ROUND_ROBIN_Splitter_8810);
	init_buffer_float(&Post_CollapsedDataParallel_2_8757WEIGHTED_ROUND_ROBIN_Splitter_8820);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 42, __iter_init_6_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_8885_8892_join[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_8759AnonFilter_a2_8711);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_split[__iter_init_7_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_8683DUPLICATE_Splitter_8758);
	FOR(int, __iter_init_8_, 0, <, 42, __iter_init_8_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_8888_8895_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin108_iDCT8x8_1D_row_fast_Fiss_8889_8896_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8886_8893_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8821WEIGHTED_ROUND_ROBIN_Splitter_8830);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8887_8894_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_8686
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_8686_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8812
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8812_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8813
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8813_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8814
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8814_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8815
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8815_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8816
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8816_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8817
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8817_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8818
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8818_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8819
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8819_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8822
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8822_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8823
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8823_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8824
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8824_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8825
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8825_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8826
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8826_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8827
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8827_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8828
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8828_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8829
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8829_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_8711
	 {
	AnonFilter_a2_8711_s.count = 0.0 ; 
	AnonFilter_a2_8711_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_8683();
		DUPLICATE_Splitter_8758();
			iDCT_2D_reference_coarse_8686();
			WEIGHTED_ROUND_ROBIN_Splitter_8766();
				AnonFilter_a3_8768();
				AnonFilter_a3_8769();
				AnonFilter_a3_8770();
				AnonFilter_a3_8771();
				AnonFilter_a3_8772();
				AnonFilter_a3_8773();
				AnonFilter_a3_8774();
				AnonFilter_a3_8775();
				AnonFilter_a3_8776();
				AnonFilter_a3_8777();
				AnonFilter_a3_8778();
				AnonFilter_a3_8779();
				AnonFilter_a3_8780();
				AnonFilter_a3_8781();
				AnonFilter_a3_8782();
				AnonFilter_a3_8783();
				AnonFilter_a3_8784();
				AnonFilter_a3_8785();
				AnonFilter_a3_8786();
				AnonFilter_a3_8787();
				AnonFilter_a3_8788();
				AnonFilter_a3_8789();
				AnonFilter_a3_8790();
				AnonFilter_a3_8791();
				AnonFilter_a3_8792();
				AnonFilter_a3_8793();
				AnonFilter_a3_8794();
				AnonFilter_a3_8795();
				AnonFilter_a3_8796();
				AnonFilter_a3_8797();
				AnonFilter_a3_8798();
				AnonFilter_a3_8799();
				AnonFilter_a3_8800();
				AnonFilter_a3_8801();
				AnonFilter_a3_8802();
				AnonFilter_a3_8803();
				AnonFilter_a3_8804();
				AnonFilter_a3_8805();
				AnonFilter_a3_8806();
				AnonFilter_a3_8807();
				AnonFilter_a3_8808();
				AnonFilter_a3_8809();
			WEIGHTED_ROUND_ROBIN_Joiner_8767();
			Pre_CollapsedDataParallel_1_8756();
			WEIGHTED_ROUND_ROBIN_Splitter_8810();
				iDCT_1D_reference_fine_8812();
				iDCT_1D_reference_fine_8813();
				iDCT_1D_reference_fine_8814();
				iDCT_1D_reference_fine_8815();
				iDCT_1D_reference_fine_8816();
				iDCT_1D_reference_fine_8817();
				iDCT_1D_reference_fine_8818();
				iDCT_1D_reference_fine_8819();
			WEIGHTED_ROUND_ROBIN_Joiner_8811();
			Post_CollapsedDataParallel_2_8757();
			WEIGHTED_ROUND_ROBIN_Splitter_8820();
				iDCT_1D_reference_fine_8822();
				iDCT_1D_reference_fine_8823();
				iDCT_1D_reference_fine_8824();
				iDCT_1D_reference_fine_8825();
				iDCT_1D_reference_fine_8826();
				iDCT_1D_reference_fine_8827();
				iDCT_1D_reference_fine_8828();
				iDCT_1D_reference_fine_8829();
			WEIGHTED_ROUND_ROBIN_Joiner_8821();
			WEIGHTED_ROUND_ROBIN_Splitter_8830();
				AnonFilter_a4_8832();
				AnonFilter_a4_8833();
				AnonFilter_a4_8834();
				AnonFilter_a4_8835();
				AnonFilter_a4_8836();
				AnonFilter_a4_8837();
				AnonFilter_a4_8838();
				AnonFilter_a4_8839();
				AnonFilter_a4_8840();
				AnonFilter_a4_8841();
				AnonFilter_a4_8842();
				AnonFilter_a4_8843();
				AnonFilter_a4_8844();
				AnonFilter_a4_8845();
				AnonFilter_a4_8846();
				AnonFilter_a4_8847();
				AnonFilter_a4_8848();
				AnonFilter_a4_8849();
				AnonFilter_a4_8850();
				AnonFilter_a4_8851();
				AnonFilter_a4_8852();
				AnonFilter_a4_8853();
				AnonFilter_a4_8854();
				AnonFilter_a4_8855();
				AnonFilter_a4_8856();
				AnonFilter_a4_8857();
				AnonFilter_a4_8858();
				AnonFilter_a4_8859();
				AnonFilter_a4_8860();
				AnonFilter_a4_8861();
				AnonFilter_a4_8862();
				AnonFilter_a4_8863();
				AnonFilter_a4_8864();
				AnonFilter_a4_8865();
				AnonFilter_a4_8866();
				AnonFilter_a4_8867();
				AnonFilter_a4_8868();
				AnonFilter_a4_8869();
				AnonFilter_a4_8870();
				AnonFilter_a4_8871();
				AnonFilter_a4_8872();
				AnonFilter_a4_8873();
			WEIGHTED_ROUND_ROBIN_Joiner_8831();
			WEIGHTED_ROUND_ROBIN_Splitter_8874();
				iDCT8x8_1D_row_fast_8876();
				iDCT8x8_1D_row_fast_8877();
				iDCT8x8_1D_row_fast_8878();
				iDCT8x8_1D_row_fast_8879();
				iDCT8x8_1D_row_fast_8880();
				iDCT8x8_1D_row_fast_8881();
				iDCT8x8_1D_row_fast_8882();
				iDCT8x8_1D_row_fast_8883();
			WEIGHTED_ROUND_ROBIN_Joiner_8875();
			iDCT8x8_1D_col_fast_8710();
		WEIGHTED_ROUND_ROBIN_Joiner_8759();
		AnonFilter_a2_8711();
	ENDFOR
	return EXIT_SUCCESS;
}
