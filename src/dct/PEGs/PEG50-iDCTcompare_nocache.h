#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=9600 on the compile command line
#else
#if BUF_SIZEMAX < 9600
#error BUF_SIZEMAX too small, it must be at least 9600
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_5902_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_5926_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_5927_t;
void AnonFilter_a0_5899();
void DUPLICATE_Splitter_5974();
void iDCT_2D_reference_coarse_5902();
void WEIGHTED_ROUND_ROBIN_Splitter_5982();
void AnonFilter_a3_5984();
void AnonFilter_a3_5985();
void AnonFilter_a3_5986();
void AnonFilter_a3_5987();
void AnonFilter_a3_5988();
void AnonFilter_a3_5989();
void AnonFilter_a3_5990();
void AnonFilter_a3_5991();
void AnonFilter_a3_5992();
void AnonFilter_a3_5993();
void AnonFilter_a3_5994();
void AnonFilter_a3_5995();
void AnonFilter_a3_5996();
void AnonFilter_a3_5997();
void AnonFilter_a3_5998();
void AnonFilter_a3_5999();
void AnonFilter_a3_6000();
void AnonFilter_a3_6001();
void AnonFilter_a3_6002();
void AnonFilter_a3_6003();
void AnonFilter_a3_6004();
void AnonFilter_a3_6005();
void AnonFilter_a3_6006();
void AnonFilter_a3_6007();
void AnonFilter_a3_6008();
void AnonFilter_a3_6009();
void AnonFilter_a3_6010();
void AnonFilter_a3_6011();
void AnonFilter_a3_6012();
void AnonFilter_a3_6013();
void AnonFilter_a3_6014();
void AnonFilter_a3_6015();
void AnonFilter_a3_6016();
void AnonFilter_a3_6017();
void AnonFilter_a3_6018();
void AnonFilter_a3_6019();
void AnonFilter_a3_6020();
void AnonFilter_a3_6021();
void AnonFilter_a3_6022();
void AnonFilter_a3_6023();
void AnonFilter_a3_6024();
void AnonFilter_a3_6025();
void AnonFilter_a3_6026();
void AnonFilter_a3_6027();
void AnonFilter_a3_6028();
void AnonFilter_a3_6029();
void AnonFilter_a3_6030();
void AnonFilter_a3_6031();
void AnonFilter_a3_6032();
void AnonFilter_a3_6033();
void WEIGHTED_ROUND_ROBIN_Joiner_5983();
void Pre_CollapsedDataParallel_1_5972();
void WEIGHTED_ROUND_ROBIN_Splitter_6034();
void iDCT_1D_reference_fine_6036();
void iDCT_1D_reference_fine_6037();
void iDCT_1D_reference_fine_6038();
void iDCT_1D_reference_fine_6039();
void iDCT_1D_reference_fine_6040();
void iDCT_1D_reference_fine_6041();
void iDCT_1D_reference_fine_6042();
void iDCT_1D_reference_fine_6043();
void WEIGHTED_ROUND_ROBIN_Joiner_6035();
void Post_CollapsedDataParallel_2_5973();
void WEIGHTED_ROUND_ROBIN_Splitter_6044();
void iDCT_1D_reference_fine_6046();
void iDCT_1D_reference_fine_6047();
void iDCT_1D_reference_fine_6048();
void iDCT_1D_reference_fine_6049();
void iDCT_1D_reference_fine_6050();
void iDCT_1D_reference_fine_6051();
void iDCT_1D_reference_fine_6052();
void iDCT_1D_reference_fine_6053();
void WEIGHTED_ROUND_ROBIN_Joiner_6045();
void WEIGHTED_ROUND_ROBIN_Splitter_6054();
void AnonFilter_a4_6056();
void AnonFilter_a4_6057();
void AnonFilter_a4_6058();
void AnonFilter_a4_6059();
void AnonFilter_a4_6060();
void AnonFilter_a4_6061();
void AnonFilter_a4_6062();
void AnonFilter_a4_6063();
void AnonFilter_a4_6064();
void AnonFilter_a4_6065();
void AnonFilter_a4_6066();
void AnonFilter_a4_6067();
void AnonFilter_a4_6068();
void AnonFilter_a4_6069();
void AnonFilter_a4_6070();
void AnonFilter_a4_6071();
void AnonFilter_a4_6072();
void AnonFilter_a4_6073();
void AnonFilter_a4_6074();
void AnonFilter_a4_6075();
void AnonFilter_a4_6076();
void AnonFilter_a4_6077();
void AnonFilter_a4_6078();
void AnonFilter_a4_6079();
void AnonFilter_a4_6080();
void AnonFilter_a4_6081();
void AnonFilter_a4_6082();
void AnonFilter_a4_6083();
void AnonFilter_a4_6084();
void AnonFilter_a4_6085();
void AnonFilter_a4_6086();
void AnonFilter_a4_6087();
void AnonFilter_a4_6088();
void AnonFilter_a4_6089();
void AnonFilter_a4_6090();
void AnonFilter_a4_6091();
void AnonFilter_a4_6092();
void AnonFilter_a4_6093();
void AnonFilter_a4_6094();
void AnonFilter_a4_6095();
void AnonFilter_a4_6096();
void AnonFilter_a4_6097();
void AnonFilter_a4_6098();
void AnonFilter_a4_6099();
void AnonFilter_a4_6100();
void AnonFilter_a4_6101();
void AnonFilter_a4_6102();
void AnonFilter_a4_6103();
void AnonFilter_a4_6104();
void AnonFilter_a4_6105();
void WEIGHTED_ROUND_ROBIN_Joiner_6055();
void WEIGHTED_ROUND_ROBIN_Splitter_6106();
void iDCT8x8_1D_row_fast_6108();
void iDCT8x8_1D_row_fast_6109();
void iDCT8x8_1D_row_fast_6110();
void iDCT8x8_1D_row_fast_6111();
void iDCT8x8_1D_row_fast_6112();
void iDCT8x8_1D_row_fast_6113();
void iDCT8x8_1D_row_fast_6114();
void iDCT8x8_1D_row_fast_6115();
void WEIGHTED_ROUND_ROBIN_Joiner_6107();
void iDCT8x8_1D_col_fast_5926();
void WEIGHTED_ROUND_ROBIN_Joiner_5975();
void AnonFilter_a2_5927();

#ifdef __cplusplus
}
#endif
#endif
