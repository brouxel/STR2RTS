#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=21120 on the compile command line
#else
#if BUF_SIZEMAX < 21120
#error BUF_SIZEMAX too small, it must be at least 21120
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_4032_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_4056_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_4057_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_4029();
void DUPLICATE_Splitter_4104();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_4032();
void WEIGHTED_ROUND_ROBIN_Splitter_4112();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_4114();
void AnonFilter_a3_4115();
void AnonFilter_a3_4116();
void AnonFilter_a3_4117();
void AnonFilter_a3_4118();
void AnonFilter_a3_4119();
void AnonFilter_a3_4120();
void AnonFilter_a3_4121();
void AnonFilter_a3_4122();
void AnonFilter_a3_4123();
void AnonFilter_a3_4124();
void AnonFilter_a3_4125();
void AnonFilter_a3_4126();
void AnonFilter_a3_4127();
void AnonFilter_a3_4128();
void AnonFilter_a3_4129();
void AnonFilter_a3_4130();
void AnonFilter_a3_4131();
void AnonFilter_a3_4132();
void AnonFilter_a3_4133();
void AnonFilter_a3_4134();
void AnonFilter_a3_4135();
void AnonFilter_a3_4136();
void AnonFilter_a3_4137();
void AnonFilter_a3_4138();
void AnonFilter_a3_4139();
void AnonFilter_a3_4140();
void AnonFilter_a3_4141();
void AnonFilter_a3_4142();
void AnonFilter_a3_4143();
void AnonFilter_a3_4144();
void AnonFilter_a3_4145();
void AnonFilter_a3_4146();
void AnonFilter_a3_4147();
void AnonFilter_a3_4148();
void AnonFilter_a3_4149();
void AnonFilter_a3_4150();
void AnonFilter_a3_4151();
void AnonFilter_a3_4152();
void AnonFilter_a3_4153();
void AnonFilter_a3_4154();
void AnonFilter_a3_4155();
void AnonFilter_a3_4156();
void AnonFilter_a3_4157();
void AnonFilter_a3_4158();
void AnonFilter_a3_4159();
void AnonFilter_a3_4160();
void AnonFilter_a3_4161();
void AnonFilter_a3_4162();
void AnonFilter_a3_4163();
void AnonFilter_a3_4164();
void AnonFilter_a3_4165();
void AnonFilter_a3_4166();
void AnonFilter_a3_4167();
void AnonFilter_a3_4168();
void WEIGHTED_ROUND_ROBIN_Joiner_4113();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_4102();
void WEIGHTED_ROUND_ROBIN_Splitter_4169();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_4171();
void iDCT_1D_reference_fine_4172();
void iDCT_1D_reference_fine_4173();
void iDCT_1D_reference_fine_4174();
void iDCT_1D_reference_fine_4175();
void iDCT_1D_reference_fine_4176();
void iDCT_1D_reference_fine_4177();
void iDCT_1D_reference_fine_4178();
void WEIGHTED_ROUND_ROBIN_Joiner_4170();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_4103();
void WEIGHTED_ROUND_ROBIN_Splitter_4179();
void iDCT_1D_reference_fine_4181();
void iDCT_1D_reference_fine_4182();
void iDCT_1D_reference_fine_4183();
void iDCT_1D_reference_fine_4184();
void iDCT_1D_reference_fine_4185();
void iDCT_1D_reference_fine_4186();
void iDCT_1D_reference_fine_4187();
void iDCT_1D_reference_fine_4188();
void WEIGHTED_ROUND_ROBIN_Joiner_4180();
void WEIGHTED_ROUND_ROBIN_Splitter_4189();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_4191();
void AnonFilter_a4_4192();
void AnonFilter_a4_4193();
void AnonFilter_a4_4194();
void AnonFilter_a4_4195();
void AnonFilter_a4_4196();
void AnonFilter_a4_4197();
void AnonFilter_a4_4198();
void AnonFilter_a4_4199();
void AnonFilter_a4_4200();
void AnonFilter_a4_4201();
void AnonFilter_a4_4202();
void AnonFilter_a4_4203();
void AnonFilter_a4_4204();
void AnonFilter_a4_4205();
void AnonFilter_a4_4206();
void AnonFilter_a4_4207();
void AnonFilter_a4_4208();
void AnonFilter_a4_4209();
void AnonFilter_a4_4210();
void AnonFilter_a4_4211();
void AnonFilter_a4_4212();
void AnonFilter_a4_4213();
void AnonFilter_a4_4214();
void AnonFilter_a4_4215();
void AnonFilter_a4_4216();
void AnonFilter_a4_4217();
void AnonFilter_a4_4218();
void AnonFilter_a4_4219();
void AnonFilter_a4_4220();
void AnonFilter_a4_4221();
void AnonFilter_a4_4222();
void AnonFilter_a4_4223();
void AnonFilter_a4_4224();
void AnonFilter_a4_4225();
void AnonFilter_a4_4226();
void AnonFilter_a4_4227();
void AnonFilter_a4_4228();
void AnonFilter_a4_4229();
void AnonFilter_a4_4230();
void AnonFilter_a4_4231();
void AnonFilter_a4_4232();
void AnonFilter_a4_4233();
void AnonFilter_a4_4234();
void AnonFilter_a4_4235();
void AnonFilter_a4_4236();
void AnonFilter_a4_4237();
void AnonFilter_a4_4238();
void AnonFilter_a4_4239();
void AnonFilter_a4_4240();
void AnonFilter_a4_4241();
void AnonFilter_a4_4242();
void AnonFilter_a4_4243();
void AnonFilter_a4_4244();
void AnonFilter_a4_4245();
void WEIGHTED_ROUND_ROBIN_Joiner_4190();
void WEIGHTED_ROUND_ROBIN_Splitter_4246();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_4248();
void iDCT8x8_1D_row_fast_4249();
void iDCT8x8_1D_row_fast_4250();
void iDCT8x8_1D_row_fast_4251();
void iDCT8x8_1D_row_fast_4252();
void iDCT8x8_1D_row_fast_4253();
void iDCT8x8_1D_row_fast_4254();
void iDCT8x8_1D_row_fast_4255();
void WEIGHTED_ROUND_ROBIN_Joiner_4247();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_4056();
void WEIGHTED_ROUND_ROBIN_Joiner_4105();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_4057();

#ifdef __cplusplus
}
#endif
#endif
