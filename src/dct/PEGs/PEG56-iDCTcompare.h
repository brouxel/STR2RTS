#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2688 on the compile command line
#else
#if BUF_SIZEMAX < 2688
#error BUF_SIZEMAX too small, it must be at least 2688
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_3646_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_3670_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_3671_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_3643();
void DUPLICATE_Splitter_3718();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_3646();
void WEIGHTED_ROUND_ROBIN_Splitter_3726();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_3728();
void AnonFilter_a3_3729();
void AnonFilter_a3_3730();
void AnonFilter_a3_3731();
void AnonFilter_a3_3732();
void AnonFilter_a3_3733();
void AnonFilter_a3_3734();
void AnonFilter_a3_3735();
void AnonFilter_a3_3736();
void AnonFilter_a3_3737();
void AnonFilter_a3_3738();
void AnonFilter_a3_3739();
void AnonFilter_a3_3740();
void AnonFilter_a3_3741();
void AnonFilter_a3_3742();
void AnonFilter_a3_3743();
void AnonFilter_a3_3744();
void AnonFilter_a3_3745();
void AnonFilter_a3_3746();
void AnonFilter_a3_3747();
void AnonFilter_a3_3748();
void AnonFilter_a3_3749();
void AnonFilter_a3_3750();
void AnonFilter_a3_3751();
void AnonFilter_a3_3752();
void AnonFilter_a3_3753();
void AnonFilter_a3_3754();
void AnonFilter_a3_3755();
void AnonFilter_a3_3756();
void AnonFilter_a3_3757();
void AnonFilter_a3_3758();
void AnonFilter_a3_3759();
void AnonFilter_a3_3760();
void AnonFilter_a3_3761();
void AnonFilter_a3_3762();
void AnonFilter_a3_3763();
void AnonFilter_a3_3764();
void AnonFilter_a3_3765();
void AnonFilter_a3_3766();
void AnonFilter_a3_3767();
void AnonFilter_a3_3768();
void AnonFilter_a3_3769();
void AnonFilter_a3_3770();
void AnonFilter_a3_3771();
void AnonFilter_a3_3772();
void AnonFilter_a3_3773();
void AnonFilter_a3_3774();
void AnonFilter_a3_3775();
void AnonFilter_a3_3776();
void AnonFilter_a3_3777();
void AnonFilter_a3_3778();
void AnonFilter_a3_3779();
void AnonFilter_a3_3780();
void AnonFilter_a3_3781();
void AnonFilter_a3_3782();
void AnonFilter_a3_3783();
void WEIGHTED_ROUND_ROBIN_Joiner_3727();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_3716();
void WEIGHTED_ROUND_ROBIN_Splitter_3784();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_3786();
void iDCT_1D_reference_fine_3787();
void iDCT_1D_reference_fine_3788();
void iDCT_1D_reference_fine_3789();
void iDCT_1D_reference_fine_3790();
void iDCT_1D_reference_fine_3791();
void iDCT_1D_reference_fine_3792();
void iDCT_1D_reference_fine_3793();
void WEIGHTED_ROUND_ROBIN_Joiner_3785();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_3717();
void WEIGHTED_ROUND_ROBIN_Splitter_3794();
void iDCT_1D_reference_fine_3796();
void iDCT_1D_reference_fine_3797();
void iDCT_1D_reference_fine_3798();
void iDCT_1D_reference_fine_3799();
void iDCT_1D_reference_fine_3800();
void iDCT_1D_reference_fine_3801();
void iDCT_1D_reference_fine_3802();
void iDCT_1D_reference_fine_3803();
void WEIGHTED_ROUND_ROBIN_Joiner_3795();
void WEIGHTED_ROUND_ROBIN_Splitter_3804();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_3806();
void AnonFilter_a4_3807();
void AnonFilter_a4_3808();
void AnonFilter_a4_3809();
void AnonFilter_a4_3810();
void AnonFilter_a4_3811();
void AnonFilter_a4_3812();
void AnonFilter_a4_3813();
void AnonFilter_a4_3814();
void AnonFilter_a4_3815();
void AnonFilter_a4_3816();
void AnonFilter_a4_3817();
void AnonFilter_a4_3818();
void AnonFilter_a4_3819();
void AnonFilter_a4_3820();
void AnonFilter_a4_3821();
void AnonFilter_a4_3822();
void AnonFilter_a4_3823();
void AnonFilter_a4_3824();
void AnonFilter_a4_3825();
void AnonFilter_a4_3826();
void AnonFilter_a4_3827();
void AnonFilter_a4_3828();
void AnonFilter_a4_3829();
void AnonFilter_a4_3830();
void AnonFilter_a4_3831();
void AnonFilter_a4_3832();
void AnonFilter_a4_3833();
void AnonFilter_a4_3834();
void AnonFilter_a4_3835();
void AnonFilter_a4_3836();
void AnonFilter_a4_3837();
void AnonFilter_a4_3838();
void AnonFilter_a4_3839();
void AnonFilter_a4_3840();
void AnonFilter_a4_3841();
void AnonFilter_a4_3842();
void AnonFilter_a4_3843();
void AnonFilter_a4_3844();
void AnonFilter_a4_3845();
void AnonFilter_a4_3846();
void AnonFilter_a4_3847();
void AnonFilter_a4_3848();
void AnonFilter_a4_3849();
void AnonFilter_a4_3850();
void AnonFilter_a4_3851();
void AnonFilter_a4_3852();
void AnonFilter_a4_3853();
void AnonFilter_a4_3854();
void AnonFilter_a4_3855();
void AnonFilter_a4_3856();
void AnonFilter_a4_3857();
void AnonFilter_a4_3858();
void AnonFilter_a4_3859();
void AnonFilter_a4_3860();
void AnonFilter_a4_3861();
void WEIGHTED_ROUND_ROBIN_Joiner_3805();
void WEIGHTED_ROUND_ROBIN_Splitter_3862();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_3864();
void iDCT8x8_1D_row_fast_3865();
void iDCT8x8_1D_row_fast_3866();
void iDCT8x8_1D_row_fast_3867();
void iDCT8x8_1D_row_fast_3868();
void iDCT8x8_1D_row_fast_3869();
void iDCT8x8_1D_row_fast_3870();
void iDCT8x8_1D_row_fast_3871();
void WEIGHTED_ROUND_ROBIN_Joiner_3863();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_3670();
void WEIGHTED_ROUND_ROBIN_Joiner_3719();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_3671();

#ifdef __cplusplus
}
#endif
#endif
