#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=19584 on the compile command line
#else
#if BUF_SIZEMAX < 19584
#error BUF_SIZEMAX too small, it must be at least 19584
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_5536_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_5560_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_5561_t;
void AnonFilter_a0_5533();
void DUPLICATE_Splitter_5608();
void iDCT_2D_reference_coarse_5536();
void WEIGHTED_ROUND_ROBIN_Splitter_5616();
void AnonFilter_a3_5618();
void AnonFilter_a3_5619();
void AnonFilter_a3_5620();
void AnonFilter_a3_5621();
void AnonFilter_a3_5622();
void AnonFilter_a3_5623();
void AnonFilter_a3_5624();
void AnonFilter_a3_5625();
void AnonFilter_a3_5626();
void AnonFilter_a3_5627();
void AnonFilter_a3_5628();
void AnonFilter_a3_5629();
void AnonFilter_a3_5630();
void AnonFilter_a3_5631();
void AnonFilter_a3_5632();
void AnonFilter_a3_5633();
void AnonFilter_a3_5634();
void AnonFilter_a3_5635();
void AnonFilter_a3_5636();
void AnonFilter_a3_5637();
void AnonFilter_a3_5638();
void AnonFilter_a3_5639();
void AnonFilter_a3_5640();
void AnonFilter_a3_5641();
void AnonFilter_a3_5642();
void AnonFilter_a3_5643();
void AnonFilter_a3_5644();
void AnonFilter_a3_5645();
void AnonFilter_a3_5646();
void AnonFilter_a3_5647();
void AnonFilter_a3_5648();
void AnonFilter_a3_5649();
void AnonFilter_a3_5650();
void AnonFilter_a3_5651();
void AnonFilter_a3_5652();
void AnonFilter_a3_5653();
void AnonFilter_a3_5654();
void AnonFilter_a3_5655();
void AnonFilter_a3_5656();
void AnonFilter_a3_5657();
void AnonFilter_a3_5658();
void AnonFilter_a3_5659();
void AnonFilter_a3_5660();
void AnonFilter_a3_5661();
void AnonFilter_a3_5662();
void AnonFilter_a3_5663();
void AnonFilter_a3_5664();
void AnonFilter_a3_5665();
void AnonFilter_a3_5666();
void AnonFilter_a3_5667();
void AnonFilter_a3_5668();
void WEIGHTED_ROUND_ROBIN_Joiner_5617();
void Pre_CollapsedDataParallel_1_5606();
void WEIGHTED_ROUND_ROBIN_Splitter_5669();
void iDCT_1D_reference_fine_5671();
void iDCT_1D_reference_fine_5672();
void iDCT_1D_reference_fine_5673();
void iDCT_1D_reference_fine_5674();
void iDCT_1D_reference_fine_5675();
void iDCT_1D_reference_fine_5676();
void iDCT_1D_reference_fine_5677();
void iDCT_1D_reference_fine_5678();
void WEIGHTED_ROUND_ROBIN_Joiner_5670();
void Post_CollapsedDataParallel_2_5607();
void WEIGHTED_ROUND_ROBIN_Splitter_5679();
void iDCT_1D_reference_fine_5681();
void iDCT_1D_reference_fine_5682();
void iDCT_1D_reference_fine_5683();
void iDCT_1D_reference_fine_5684();
void iDCT_1D_reference_fine_5685();
void iDCT_1D_reference_fine_5686();
void iDCT_1D_reference_fine_5687();
void iDCT_1D_reference_fine_5688();
void WEIGHTED_ROUND_ROBIN_Joiner_5680();
void WEIGHTED_ROUND_ROBIN_Splitter_5689();
void AnonFilter_a4_5691();
void AnonFilter_a4_5692();
void AnonFilter_a4_5693();
void AnonFilter_a4_5694();
void AnonFilter_a4_5695();
void AnonFilter_a4_5696();
void AnonFilter_a4_5697();
void AnonFilter_a4_5698();
void AnonFilter_a4_5699();
void AnonFilter_a4_5700();
void AnonFilter_a4_5701();
void AnonFilter_a4_5702();
void AnonFilter_a4_5703();
void AnonFilter_a4_5704();
void AnonFilter_a4_5705();
void AnonFilter_a4_5706();
void AnonFilter_a4_5707();
void AnonFilter_a4_5708();
void AnonFilter_a4_5709();
void AnonFilter_a4_5710();
void AnonFilter_a4_5711();
void AnonFilter_a4_5712();
void AnonFilter_a4_5713();
void AnonFilter_a4_5714();
void AnonFilter_a4_5715();
void AnonFilter_a4_5716();
void AnonFilter_a4_5717();
void AnonFilter_a4_5718();
void AnonFilter_a4_5719();
void AnonFilter_a4_5720();
void AnonFilter_a4_5721();
void AnonFilter_a4_5722();
void AnonFilter_a4_5723();
void AnonFilter_a4_5724();
void AnonFilter_a4_5725();
void AnonFilter_a4_5726();
void AnonFilter_a4_5727();
void AnonFilter_a4_5728();
void AnonFilter_a4_5729();
void AnonFilter_a4_5730();
void AnonFilter_a4_5731();
void AnonFilter_a4_5732();
void AnonFilter_a4_5733();
void AnonFilter_a4_5734();
void AnonFilter_a4_5735();
void AnonFilter_a4_5736();
void AnonFilter_a4_5737();
void AnonFilter_a4_5738();
void AnonFilter_a4_5739();
void AnonFilter_a4_5740();
void AnonFilter_a4_5741();
void WEIGHTED_ROUND_ROBIN_Joiner_5690();
void WEIGHTED_ROUND_ROBIN_Splitter_5742();
void iDCT8x8_1D_row_fast_5744();
void iDCT8x8_1D_row_fast_5745();
void iDCT8x8_1D_row_fast_5746();
void iDCT8x8_1D_row_fast_5747();
void iDCT8x8_1D_row_fast_5748();
void iDCT8x8_1D_row_fast_5749();
void iDCT8x8_1D_row_fast_5750();
void iDCT8x8_1D_row_fast_5751();
void WEIGHTED_ROUND_ROBIN_Joiner_5743();
void iDCT8x8_1D_col_fast_5560();
void WEIGHTED_ROUND_ROBIN_Joiner_5609();
void AnonFilter_a2_5561();

#ifdef __cplusplus
}
#endif
#endif
