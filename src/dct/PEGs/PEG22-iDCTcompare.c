#include "PEG22-iDCTcompare.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14631Post_CollapsedDataParallel_2_14597;
buffer_int_t SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[8];
buffer_float_t Post_CollapsedDataParallel_2_14597WEIGHTED_ROUND_ROBIN_Splitter_14640;
buffer_float_t Pre_CollapsedDataParallel_1_14596WEIGHTED_ROUND_ROBIN_Splitter_14630;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[22];
buffer_int_t SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[22];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[22];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14641WEIGHTED_ROUND_ROBIN_Splitter_14650;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_14599AnonFilter_a2_14551;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[22];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14607Pre_CollapsedDataParallel_1_14596;
buffer_int_t AnonFilter_a0_14523DUPLICATE_Splitter_14598;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_14675iDCT8x8_1D_col_fast_14550;


iDCT_2D_reference_coarse_14526_t iDCT_2D_reference_coarse_14526_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14632_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14633_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14634_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14635_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14636_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14637_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14638_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14639_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14642_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14643_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14644_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14645_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14646_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14647_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14648_s;
iDCT_2D_reference_coarse_14526_t iDCT_1D_reference_fine_14649_s;
iDCT8x8_1D_col_fast_14550_t iDCT8x8_1D_col_fast_14550_s;
AnonFilter_a2_14551_t AnonFilter_a2_14551_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_14523() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_14523DUPLICATE_Splitter_14598));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_14526_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_14526_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_14526() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_14608() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[0]));
	ENDFOR
}

void AnonFilter_a3_14609() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[1]));
	ENDFOR
}

void AnonFilter_a3_14610() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[2]));
	ENDFOR
}

void AnonFilter_a3_14611() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[3]));
	ENDFOR
}

void AnonFilter_a3_14612() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[4]));
	ENDFOR
}

void AnonFilter_a3_14613() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[5]));
	ENDFOR
}

void AnonFilter_a3_14614() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[6]));
	ENDFOR
}

void AnonFilter_a3_14615() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[7]));
	ENDFOR
}

void AnonFilter_a3_14616() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[8]));
	ENDFOR
}

void AnonFilter_a3_14617() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[9]));
	ENDFOR
}

void AnonFilter_a3_14618() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[10]));
	ENDFOR
}

void AnonFilter_a3_14619() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[11]));
	ENDFOR
}

void AnonFilter_a3_14620() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[12]));
	ENDFOR
}

void AnonFilter_a3_14621() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[13]));
	ENDFOR
}

void AnonFilter_a3_14622() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[14]));
	ENDFOR
}

void AnonFilter_a3_14623() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[15]));
	ENDFOR
}

void AnonFilter_a3_14624() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[16]));
	ENDFOR
}

void AnonFilter_a3_14625() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[17]));
	ENDFOR
}

void AnonFilter_a3_14626() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[18]));
	ENDFOR
}

void AnonFilter_a3_14627() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[19]));
	ENDFOR
}

void AnonFilter_a3_14628() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[20]));
	ENDFOR
}

void AnonFilter_a3_14629() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[21]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14606() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14607() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14607Pre_CollapsedDataParallel_1_14596, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_14596() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_14607Pre_CollapsedDataParallel_1_14596), &(Pre_CollapsedDataParallel_1_14596WEIGHTED_ROUND_ROBIN_Splitter_14630));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14632_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_14632() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_14633() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_14634() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_14635() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_14636() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_14637() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_14638() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_14639() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14630() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_14596WEIGHTED_ROUND_ROBIN_Splitter_14630));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14631() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14631Post_CollapsedDataParallel_2_14597, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_14597() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_14631Post_CollapsedDataParallel_2_14597), &(Post_CollapsedDataParallel_2_14597WEIGHTED_ROUND_ROBIN_Splitter_14640));
	ENDFOR
}

void iDCT_1D_reference_fine_14642() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_14643() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_14644() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_14645() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_14646() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_14647() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_14648() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_14649() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14640() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_14597WEIGHTED_ROUND_ROBIN_Splitter_14640));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14641() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14641WEIGHTED_ROUND_ROBIN_Splitter_14650, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_14652() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[0]));
	ENDFOR
}

void AnonFilter_a4_14653() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[1]));
	ENDFOR
}

void AnonFilter_a4_14654() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[2]));
	ENDFOR
}

void AnonFilter_a4_14655() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[3]));
	ENDFOR
}

void AnonFilter_a4_14656() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[4]));
	ENDFOR
}

void AnonFilter_a4_14657() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[5]));
	ENDFOR
}

void AnonFilter_a4_14658() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[6]));
	ENDFOR
}

void AnonFilter_a4_14659() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[7]));
	ENDFOR
}

void AnonFilter_a4_14660() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[8]));
	ENDFOR
}

void AnonFilter_a4_14661() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[9]));
	ENDFOR
}

void AnonFilter_a4_14662() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[10]));
	ENDFOR
}

void AnonFilter_a4_14663() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[11]));
	ENDFOR
}

void AnonFilter_a4_14664() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[12]));
	ENDFOR
}

void AnonFilter_a4_14665() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[13]));
	ENDFOR
}

void AnonFilter_a4_14666() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[14]));
	ENDFOR
}

void AnonFilter_a4_14667() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[15]));
	ENDFOR
}

void AnonFilter_a4_14668() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[16]));
	ENDFOR
}

void AnonFilter_a4_14669() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[17]));
	ENDFOR
}

void AnonFilter_a4_14670() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[18]));
	ENDFOR
}

void AnonFilter_a4_14671() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[19]));
	ENDFOR
}

void AnonFilter_a4_14672() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[20]));
	ENDFOR
}

void AnonFilter_a4_14673() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[21]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14650() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14641WEIGHTED_ROUND_ROBIN_Splitter_14650));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14651() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_14676() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[0]), &(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_14677() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[1]), &(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_14678() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[2]), &(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_14679() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[3]), &(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_14680() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[4]), &(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_14681() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[5]), &(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_14682() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[6]), &(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_14683() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[7]), &(SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14674() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14675() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_14675iDCT8x8_1D_col_fast_14550, pop_int(&SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_14550_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_14550_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_14550_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_14550_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_14550_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_14550_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_14550_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_14550_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_14550_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_14550_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_14550() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_14675iDCT8x8_1D_col_fast_14550), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_14598() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_14523DUPLICATE_Splitter_14598);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14599() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_14599AnonFilter_a2_14551, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_14551_s.count = (AnonFilter_a2_14551_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_14551_s.errors = (AnonFilter_a2_14551_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_14551_s.errors / AnonFilter_a2_14551_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_14551_s.errors = (AnonFilter_a2_14551_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_14551_s.errors / AnonFilter_a2_14551_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_14551() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_14599AnonFilter_a2_14551));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14631Post_CollapsedDataParallel_2_14597);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_14597WEIGHTED_ROUND_ROBIN_Splitter_14640);
	init_buffer_float(&Pre_CollapsedDataParallel_1_14596WEIGHTED_ROUND_ROBIN_Splitter_14630);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 22, __iter_init_4_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_14688_14695_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_int(&SplitJoin68_iDCT8x8_1D_row_fast_Fiss_14689_14696_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14524_14600_14684_14691_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14686_14693_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 22, __iter_init_8_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_14688_14695_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 22, __iter_init_9_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_14685_14692_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14641WEIGHTED_ROUND_ROBIN_Splitter_14650);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_14599AnonFilter_a2_14551);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14687_14694_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 22, __iter_init_11_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_14685_14692_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14607Pre_CollapsedDataParallel_1_14596);
	init_buffer_int(&AnonFilter_a0_14523DUPLICATE_Splitter_14598);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_14675iDCT8x8_1D_col_fast_14550);
// --- init: iDCT_2D_reference_coarse_14526
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_14526_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14632
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14632_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14633
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14633_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14634
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14634_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14635
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14635_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14636
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14636_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14637
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14637_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14638
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14638_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14639
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14639_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14642
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14642_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14643
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14643_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14644
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14644_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14645
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14645_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14646
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14646_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14647
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14647_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14648
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14648_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14649
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14649_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_14551
	 {
	AnonFilter_a2_14551_s.count = 0.0 ; 
	AnonFilter_a2_14551_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_14523();
		DUPLICATE_Splitter_14598();
			iDCT_2D_reference_coarse_14526();
			WEIGHTED_ROUND_ROBIN_Splitter_14606();
				AnonFilter_a3_14608();
				AnonFilter_a3_14609();
				AnonFilter_a3_14610();
				AnonFilter_a3_14611();
				AnonFilter_a3_14612();
				AnonFilter_a3_14613();
				AnonFilter_a3_14614();
				AnonFilter_a3_14615();
				AnonFilter_a3_14616();
				AnonFilter_a3_14617();
				AnonFilter_a3_14618();
				AnonFilter_a3_14619();
				AnonFilter_a3_14620();
				AnonFilter_a3_14621();
				AnonFilter_a3_14622();
				AnonFilter_a3_14623();
				AnonFilter_a3_14624();
				AnonFilter_a3_14625();
				AnonFilter_a3_14626();
				AnonFilter_a3_14627();
				AnonFilter_a3_14628();
				AnonFilter_a3_14629();
			WEIGHTED_ROUND_ROBIN_Joiner_14607();
			Pre_CollapsedDataParallel_1_14596();
			WEIGHTED_ROUND_ROBIN_Splitter_14630();
				iDCT_1D_reference_fine_14632();
				iDCT_1D_reference_fine_14633();
				iDCT_1D_reference_fine_14634();
				iDCT_1D_reference_fine_14635();
				iDCT_1D_reference_fine_14636();
				iDCT_1D_reference_fine_14637();
				iDCT_1D_reference_fine_14638();
				iDCT_1D_reference_fine_14639();
			WEIGHTED_ROUND_ROBIN_Joiner_14631();
			Post_CollapsedDataParallel_2_14597();
			WEIGHTED_ROUND_ROBIN_Splitter_14640();
				iDCT_1D_reference_fine_14642();
				iDCT_1D_reference_fine_14643();
				iDCT_1D_reference_fine_14644();
				iDCT_1D_reference_fine_14645();
				iDCT_1D_reference_fine_14646();
				iDCT_1D_reference_fine_14647();
				iDCT_1D_reference_fine_14648();
				iDCT_1D_reference_fine_14649();
			WEIGHTED_ROUND_ROBIN_Joiner_14641();
			WEIGHTED_ROUND_ROBIN_Splitter_14650();
				AnonFilter_a4_14652();
				AnonFilter_a4_14653();
				AnonFilter_a4_14654();
				AnonFilter_a4_14655();
				AnonFilter_a4_14656();
				AnonFilter_a4_14657();
				AnonFilter_a4_14658();
				AnonFilter_a4_14659();
				AnonFilter_a4_14660();
				AnonFilter_a4_14661();
				AnonFilter_a4_14662();
				AnonFilter_a4_14663();
				AnonFilter_a4_14664();
				AnonFilter_a4_14665();
				AnonFilter_a4_14666();
				AnonFilter_a4_14667();
				AnonFilter_a4_14668();
				AnonFilter_a4_14669();
				AnonFilter_a4_14670();
				AnonFilter_a4_14671();
				AnonFilter_a4_14672();
				AnonFilter_a4_14673();
			WEIGHTED_ROUND_ROBIN_Joiner_14651();
			WEIGHTED_ROUND_ROBIN_Splitter_14674();
				iDCT8x8_1D_row_fast_14676();
				iDCT8x8_1D_row_fast_14677();
				iDCT8x8_1D_row_fast_14678();
				iDCT8x8_1D_row_fast_14679();
				iDCT8x8_1D_row_fast_14680();
				iDCT8x8_1D_row_fast_14681();
				iDCT8x8_1D_row_fast_14682();
				iDCT8x8_1D_row_fast_14683();
			WEIGHTED_ROUND_ROBIN_Joiner_14675();
			iDCT8x8_1D_col_fast_14550();
		WEIGHTED_ROUND_ROBIN_Joiner_14599();
		AnonFilter_a2_14551();
	ENDFOR
	return EXIT_SUCCESS;
}
