#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4992 on the compile command line
#else
#if BUF_SIZEMAX < 4992
#error BUF_SIZEMAX too small, it must be at least 4992
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_13486_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_13510_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_13511_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_13483();
void DUPLICATE_Splitter_13558();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_13486();
void WEIGHTED_ROUND_ROBIN_Splitter_13566();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_13568();
void AnonFilter_a3_13569();
void AnonFilter_a3_13570();
void AnonFilter_a3_13571();
void AnonFilter_a3_13572();
void AnonFilter_a3_13573();
void AnonFilter_a3_13574();
void AnonFilter_a3_13575();
void AnonFilter_a3_13576();
void AnonFilter_a3_13577();
void AnonFilter_a3_13578();
void AnonFilter_a3_13579();
void AnonFilter_a3_13580();
void AnonFilter_a3_13581();
void AnonFilter_a3_13582();
void AnonFilter_a3_13583();
void AnonFilter_a3_13584();
void AnonFilter_a3_13585();
void AnonFilter_a3_13586();
void AnonFilter_a3_13587();
void AnonFilter_a3_13588();
void AnonFilter_a3_13589();
void AnonFilter_a3_13590();
void AnonFilter_a3_13591();
void AnonFilter_a3_13592();
void AnonFilter_a3_13593();
void WEIGHTED_ROUND_ROBIN_Joiner_13567();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_13556();
void WEIGHTED_ROUND_ROBIN_Splitter_13594();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_13596();
void iDCT_1D_reference_fine_13597();
void iDCT_1D_reference_fine_13598();
void iDCT_1D_reference_fine_13599();
void iDCT_1D_reference_fine_13600();
void iDCT_1D_reference_fine_13601();
void iDCT_1D_reference_fine_13602();
void iDCT_1D_reference_fine_13603();
void WEIGHTED_ROUND_ROBIN_Joiner_13595();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_13557();
void WEIGHTED_ROUND_ROBIN_Splitter_13604();
void iDCT_1D_reference_fine_13606();
void iDCT_1D_reference_fine_13607();
void iDCT_1D_reference_fine_13608();
void iDCT_1D_reference_fine_13609();
void iDCT_1D_reference_fine_13610();
void iDCT_1D_reference_fine_13611();
void iDCT_1D_reference_fine_13612();
void iDCT_1D_reference_fine_13613();
void WEIGHTED_ROUND_ROBIN_Joiner_13605();
void WEIGHTED_ROUND_ROBIN_Splitter_13614();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_13616();
void AnonFilter_a4_13617();
void AnonFilter_a4_13618();
void AnonFilter_a4_13619();
void AnonFilter_a4_13620();
void AnonFilter_a4_13621();
void AnonFilter_a4_13622();
void AnonFilter_a4_13623();
void AnonFilter_a4_13624();
void AnonFilter_a4_13625();
void AnonFilter_a4_13626();
void AnonFilter_a4_13627();
void AnonFilter_a4_13628();
void AnonFilter_a4_13629();
void AnonFilter_a4_13630();
void AnonFilter_a4_13631();
void AnonFilter_a4_13632();
void AnonFilter_a4_13633();
void AnonFilter_a4_13634();
void AnonFilter_a4_13635();
void AnonFilter_a4_13636();
void AnonFilter_a4_13637();
void AnonFilter_a4_13638();
void AnonFilter_a4_13639();
void AnonFilter_a4_13640();
void AnonFilter_a4_13641();
void WEIGHTED_ROUND_ROBIN_Joiner_13615();
void WEIGHTED_ROUND_ROBIN_Splitter_13642();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_13644();
void iDCT8x8_1D_row_fast_13645();
void iDCT8x8_1D_row_fast_13646();
void iDCT8x8_1D_row_fast_13647();
void iDCT8x8_1D_row_fast_13648();
void iDCT8x8_1D_row_fast_13649();
void iDCT8x8_1D_row_fast_13650();
void iDCT8x8_1D_row_fast_13651();
void WEIGHTED_ROUND_ROBIN_Joiner_13643();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_13510();
void WEIGHTED_ROUND_ROBIN_Joiner_13559();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_13511();

#ifdef __cplusplus
}
#endif
#endif
