#include "PEG18-iDCTcompare.h"

buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[18];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[18];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[18];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_split[3];
buffer_int_t AnonFilter_a0_15499DUPLICATE_Splitter_15574;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_15643iDCT8x8_1D_col_fast_15526;
buffer_int_t SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15613WEIGHTED_ROUND_ROBIN_Splitter_15622;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[18];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15603Post_CollapsedDataParallel_2_15573;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_join[3];
buffer_float_t Pre_CollapsedDataParallel_1_15572WEIGHTED_ROUND_ROBIN_Splitter_15602;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[8];
buffer_float_t Post_CollapsedDataParallel_2_15573WEIGHTED_ROUND_ROBIN_Splitter_15612;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_15575AnonFilter_a2_15527;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15583Pre_CollapsedDataParallel_1_15572;
buffer_int_t SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[8];


iDCT_2D_reference_coarse_15502_t iDCT_2D_reference_coarse_15502_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15604_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15605_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15606_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15607_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15608_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15609_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15610_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15611_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15614_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15615_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15616_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15617_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15618_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15619_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15620_s;
iDCT_2D_reference_coarse_15502_t iDCT_1D_reference_fine_15621_s;
iDCT8x8_1D_col_fast_15526_t iDCT8x8_1D_col_fast_15526_s;
AnonFilter_a2_15527_t AnonFilter_a2_15527_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_15499() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_15499DUPLICATE_Splitter_15574));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_15502_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_15502_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_15502() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_15584() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[0]));
	ENDFOR
}

void AnonFilter_a3_15585() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[1]));
	ENDFOR
}

void AnonFilter_a3_15586() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[2]));
	ENDFOR
}

void AnonFilter_a3_15587() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[3]));
	ENDFOR
}

void AnonFilter_a3_15588() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[4]));
	ENDFOR
}

void AnonFilter_a3_15589() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[5]));
	ENDFOR
}

void AnonFilter_a3_15590() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[6]));
	ENDFOR
}

void AnonFilter_a3_15591() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[7]));
	ENDFOR
}

void AnonFilter_a3_15592() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[8]));
	ENDFOR
}

void AnonFilter_a3_15593() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[9]));
	ENDFOR
}

void AnonFilter_a3_15594() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[10]));
	ENDFOR
}

void AnonFilter_a3_15595() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[11]));
	ENDFOR
}

void AnonFilter_a3_15596() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[12]));
	ENDFOR
}

void AnonFilter_a3_15597() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[13]));
	ENDFOR
}

void AnonFilter_a3_15598() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[14]));
	ENDFOR
}

void AnonFilter_a3_15599() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[15]));
	ENDFOR
}

void AnonFilter_a3_15600() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[16]));
	ENDFOR
}

void AnonFilter_a3_15601() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[17]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15582() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15583() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15583Pre_CollapsedDataParallel_1_15572, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_15572() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_15583Pre_CollapsedDataParallel_1_15572), &(Pre_CollapsedDataParallel_1_15572WEIGHTED_ROUND_ROBIN_Splitter_15602));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15604_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_15604() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_15605() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_15606() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_15607() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_15608() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_15609() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_15610() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_15611() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15602() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_15572WEIGHTED_ROUND_ROBIN_Splitter_15602));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15603() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15603Post_CollapsedDataParallel_2_15573, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_15573() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_15603Post_CollapsedDataParallel_2_15573), &(Post_CollapsedDataParallel_2_15573WEIGHTED_ROUND_ROBIN_Splitter_15612));
	ENDFOR
}

void iDCT_1D_reference_fine_15614() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_15615() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_15616() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_15617() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_15618() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_15619() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_15620() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_15621() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15612() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_15573WEIGHTED_ROUND_ROBIN_Splitter_15612));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15613() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15613WEIGHTED_ROUND_ROBIN_Splitter_15622, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_15624() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[0]));
	ENDFOR
}

void AnonFilter_a4_15625() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[1]));
	ENDFOR
}

void AnonFilter_a4_15626() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[2]));
	ENDFOR
}

void AnonFilter_a4_15627() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[3]));
	ENDFOR
}

void AnonFilter_a4_15628() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[4]));
	ENDFOR
}

void AnonFilter_a4_15629() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[5]));
	ENDFOR
}

void AnonFilter_a4_15630() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[6]));
	ENDFOR
}

void AnonFilter_a4_15631() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[7]));
	ENDFOR
}

void AnonFilter_a4_15632() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[8]));
	ENDFOR
}

void AnonFilter_a4_15633() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[9]));
	ENDFOR
}

void AnonFilter_a4_15634() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[10]));
	ENDFOR
}

void AnonFilter_a4_15635() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[11]));
	ENDFOR
}

void AnonFilter_a4_15636() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[12]));
	ENDFOR
}

void AnonFilter_a4_15637() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[13]));
	ENDFOR
}

void AnonFilter_a4_15638() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[14]));
	ENDFOR
}

void AnonFilter_a4_15639() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[15]));
	ENDFOR
}

void AnonFilter_a4_15640() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[16]));
	ENDFOR
}

void AnonFilter_a4_15641() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[17]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15622() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_15613WEIGHTED_ROUND_ROBIN_Splitter_15622));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15623() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_15644() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[0]), &(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15645() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[1]), &(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15646() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[2]), &(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15647() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[3]), &(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15648() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[4]), &(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15649() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[5]), &(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15650() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[6]), &(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15651() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[7]), &(SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15642() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15643() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_15643iDCT8x8_1D_col_fast_15526, pop_int(&SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_15526_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_15526_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_15526_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_15526_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_15526_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_15526_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_15526_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_15526_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_15526_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_15526_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_15526() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_15643iDCT8x8_1D_col_fast_15526), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_15574() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_15499DUPLICATE_Splitter_15574);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15575() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_15575AnonFilter_a2_15527, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_15527_s.count = (AnonFilter_a2_15527_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_15527_s.errors = (AnonFilter_a2_15527_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15527_s.errors / AnonFilter_a2_15527_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_15527_s.errors = (AnonFilter_a2_15527_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15527_s.errors / AnonFilter_a2_15527_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_15527() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_15575AnonFilter_a2_15527));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15655_15662_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 18, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_15656_15663_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 18, __iter_init_3_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_15656_15663_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 18, __iter_init_4_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_15653_15660_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_split[__iter_init_5_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_15499DUPLICATE_Splitter_15574);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_15643iDCT8x8_1D_col_fast_15526);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_int(&SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15613WEIGHTED_ROUND_ROBIN_Splitter_15622);
	FOR(int, __iter_init_7_, 0, <, 18, __iter_init_7_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_15653_15660_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15603Post_CollapsedDataParallel_2_15573);
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15500_15576_15652_15659_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_15572WEIGHTED_ROUND_ROBIN_Splitter_15602);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_15573WEIGHTED_ROUND_ROBIN_Splitter_15612);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15654_15661_split[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_15575AnonFilter_a2_15527);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15583Pre_CollapsedDataParallel_1_15572);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin60_iDCT8x8_1D_row_fast_Fiss_15657_15664_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_15502
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_15502_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15604
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15604_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15605
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15605_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15606
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15606_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15607
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15607_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15608
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15608_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15609
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15609_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15610
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15610_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15611
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15611_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15614
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15614_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15615
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15615_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15616
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15616_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15617
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15617_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15618
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15618_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15619
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15619_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15620
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15620_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15621
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15621_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_15527
	 {
	AnonFilter_a2_15527_s.count = 0.0 ; 
	AnonFilter_a2_15527_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_15499();
		DUPLICATE_Splitter_15574();
			iDCT_2D_reference_coarse_15502();
			WEIGHTED_ROUND_ROBIN_Splitter_15582();
				AnonFilter_a3_15584();
				AnonFilter_a3_15585();
				AnonFilter_a3_15586();
				AnonFilter_a3_15587();
				AnonFilter_a3_15588();
				AnonFilter_a3_15589();
				AnonFilter_a3_15590();
				AnonFilter_a3_15591();
				AnonFilter_a3_15592();
				AnonFilter_a3_15593();
				AnonFilter_a3_15594();
				AnonFilter_a3_15595();
				AnonFilter_a3_15596();
				AnonFilter_a3_15597();
				AnonFilter_a3_15598();
				AnonFilter_a3_15599();
				AnonFilter_a3_15600();
				AnonFilter_a3_15601();
			WEIGHTED_ROUND_ROBIN_Joiner_15583();
			Pre_CollapsedDataParallel_1_15572();
			WEIGHTED_ROUND_ROBIN_Splitter_15602();
				iDCT_1D_reference_fine_15604();
				iDCT_1D_reference_fine_15605();
				iDCT_1D_reference_fine_15606();
				iDCT_1D_reference_fine_15607();
				iDCT_1D_reference_fine_15608();
				iDCT_1D_reference_fine_15609();
				iDCT_1D_reference_fine_15610();
				iDCT_1D_reference_fine_15611();
			WEIGHTED_ROUND_ROBIN_Joiner_15603();
			Post_CollapsedDataParallel_2_15573();
			WEIGHTED_ROUND_ROBIN_Splitter_15612();
				iDCT_1D_reference_fine_15614();
				iDCT_1D_reference_fine_15615();
				iDCT_1D_reference_fine_15616();
				iDCT_1D_reference_fine_15617();
				iDCT_1D_reference_fine_15618();
				iDCT_1D_reference_fine_15619();
				iDCT_1D_reference_fine_15620();
				iDCT_1D_reference_fine_15621();
			WEIGHTED_ROUND_ROBIN_Joiner_15613();
			WEIGHTED_ROUND_ROBIN_Splitter_15622();
				AnonFilter_a4_15624();
				AnonFilter_a4_15625();
				AnonFilter_a4_15626();
				AnonFilter_a4_15627();
				AnonFilter_a4_15628();
				AnonFilter_a4_15629();
				AnonFilter_a4_15630();
				AnonFilter_a4_15631();
				AnonFilter_a4_15632();
				AnonFilter_a4_15633();
				AnonFilter_a4_15634();
				AnonFilter_a4_15635();
				AnonFilter_a4_15636();
				AnonFilter_a4_15637();
				AnonFilter_a4_15638();
				AnonFilter_a4_15639();
				AnonFilter_a4_15640();
				AnonFilter_a4_15641();
			WEIGHTED_ROUND_ROBIN_Joiner_15623();
			WEIGHTED_ROUND_ROBIN_Splitter_15642();
				iDCT8x8_1D_row_fast_15644();
				iDCT8x8_1D_row_fast_15645();
				iDCT8x8_1D_row_fast_15646();
				iDCT8x8_1D_row_fast_15647();
				iDCT8x8_1D_row_fast_15648();
				iDCT8x8_1D_row_fast_15649();
				iDCT8x8_1D_row_fast_15650();
				iDCT8x8_1D_row_fast_15651();
			WEIGHTED_ROUND_ROBIN_Joiner_15643();
			iDCT8x8_1D_col_fast_15526();
		WEIGHTED_ROUND_ROBIN_Joiner_15575();
		AnonFilter_a2_15527();
	ENDFOR
	return EXIT_SUCCESS;
}
