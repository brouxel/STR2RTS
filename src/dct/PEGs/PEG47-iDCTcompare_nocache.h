#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=18048 on the compile command line
#else
#if BUF_SIZEMAX < 18048
#error BUF_SIZEMAX too small, it must be at least 18048
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_6976_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_7000_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_7001_t;
void AnonFilter_a0_6973();
void DUPLICATE_Splitter_7048();
void iDCT_2D_reference_coarse_6976();
void WEIGHTED_ROUND_ROBIN_Splitter_7056();
void AnonFilter_a3_7058();
void AnonFilter_a3_7059();
void AnonFilter_a3_7060();
void AnonFilter_a3_7061();
void AnonFilter_a3_7062();
void AnonFilter_a3_7063();
void AnonFilter_a3_7064();
void AnonFilter_a3_7065();
void AnonFilter_a3_7066();
void AnonFilter_a3_7067();
void AnonFilter_a3_7068();
void AnonFilter_a3_7069();
void AnonFilter_a3_7070();
void AnonFilter_a3_7071();
void AnonFilter_a3_7072();
void AnonFilter_a3_7073();
void AnonFilter_a3_7074();
void AnonFilter_a3_7075();
void AnonFilter_a3_7076();
void AnonFilter_a3_7077();
void AnonFilter_a3_7078();
void AnonFilter_a3_7079();
void AnonFilter_a3_7080();
void AnonFilter_a3_7081();
void AnonFilter_a3_7082();
void AnonFilter_a3_7083();
void AnonFilter_a3_7084();
void AnonFilter_a3_7085();
void AnonFilter_a3_7086();
void AnonFilter_a3_7087();
void AnonFilter_a3_7088();
void AnonFilter_a3_7089();
void AnonFilter_a3_7090();
void AnonFilter_a3_7091();
void AnonFilter_a3_7092();
void AnonFilter_a3_7093();
void AnonFilter_a3_7094();
void AnonFilter_a3_7095();
void AnonFilter_a3_7096();
void AnonFilter_a3_7097();
void AnonFilter_a3_7098();
void AnonFilter_a3_7099();
void AnonFilter_a3_7100();
void AnonFilter_a3_7101();
void AnonFilter_a3_7102();
void AnonFilter_a3_7103();
void AnonFilter_a3_7104();
void WEIGHTED_ROUND_ROBIN_Joiner_7057();
void Pre_CollapsedDataParallel_1_7046();
void WEIGHTED_ROUND_ROBIN_Splitter_7105();
void iDCT_1D_reference_fine_7107();
void iDCT_1D_reference_fine_7108();
void iDCT_1D_reference_fine_7109();
void iDCT_1D_reference_fine_7110();
void iDCT_1D_reference_fine_7111();
void iDCT_1D_reference_fine_7112();
void iDCT_1D_reference_fine_7113();
void iDCT_1D_reference_fine_7114();
void WEIGHTED_ROUND_ROBIN_Joiner_7106();
void Post_CollapsedDataParallel_2_7047();
void WEIGHTED_ROUND_ROBIN_Splitter_7115();
void iDCT_1D_reference_fine_7117();
void iDCT_1D_reference_fine_7118();
void iDCT_1D_reference_fine_7119();
void iDCT_1D_reference_fine_7120();
void iDCT_1D_reference_fine_7121();
void iDCT_1D_reference_fine_7122();
void iDCT_1D_reference_fine_7123();
void iDCT_1D_reference_fine_7124();
void WEIGHTED_ROUND_ROBIN_Joiner_7116();
void WEIGHTED_ROUND_ROBIN_Splitter_7125();
void AnonFilter_a4_7127();
void AnonFilter_a4_7128();
void AnonFilter_a4_7129();
void AnonFilter_a4_7130();
void AnonFilter_a4_7131();
void AnonFilter_a4_7132();
void AnonFilter_a4_7133();
void AnonFilter_a4_7134();
void AnonFilter_a4_7135();
void AnonFilter_a4_7136();
void AnonFilter_a4_7137();
void AnonFilter_a4_7138();
void AnonFilter_a4_7139();
void AnonFilter_a4_7140();
void AnonFilter_a4_7141();
void AnonFilter_a4_7142();
void AnonFilter_a4_7143();
void AnonFilter_a4_7144();
void AnonFilter_a4_7145();
void AnonFilter_a4_7146();
void AnonFilter_a4_7147();
void AnonFilter_a4_7148();
void AnonFilter_a4_7149();
void AnonFilter_a4_7150();
void AnonFilter_a4_7151();
void AnonFilter_a4_7152();
void AnonFilter_a4_7153();
void AnonFilter_a4_7154();
void AnonFilter_a4_7155();
void AnonFilter_a4_7156();
void AnonFilter_a4_7157();
void AnonFilter_a4_7158();
void AnonFilter_a4_7159();
void AnonFilter_a4_7160();
void AnonFilter_a4_7161();
void AnonFilter_a4_7162();
void AnonFilter_a4_7163();
void AnonFilter_a4_7164();
void AnonFilter_a4_7165();
void AnonFilter_a4_7166();
void AnonFilter_a4_7167();
void AnonFilter_a4_7168();
void AnonFilter_a4_7169();
void AnonFilter_a4_7170();
void AnonFilter_a4_7171();
void AnonFilter_a4_7172();
void AnonFilter_a4_7173();
void WEIGHTED_ROUND_ROBIN_Joiner_7126();
void WEIGHTED_ROUND_ROBIN_Splitter_7174();
void iDCT8x8_1D_row_fast_7176();
void iDCT8x8_1D_row_fast_7177();
void iDCT8x8_1D_row_fast_7178();
void iDCT8x8_1D_row_fast_7179();
void iDCT8x8_1D_row_fast_7180();
void iDCT8x8_1D_row_fast_7181();
void iDCT8x8_1D_row_fast_7182();
void iDCT8x8_1D_row_fast_7183();
void WEIGHTED_ROUND_ROBIN_Joiner_7175();
void iDCT8x8_1D_col_fast_7000();
void WEIGHTED_ROUND_ROBIN_Joiner_7049();
void AnonFilter_a2_7001();

#ifdef __cplusplus
}
#endif
#endif
