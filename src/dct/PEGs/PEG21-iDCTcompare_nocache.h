#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=8064 on the compile command line
#else
#if BUF_SIZEMAX < 8064
#error BUF_SIZEMAX too small, it must be at least 8064
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_14776_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_14800_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_14801_t;
void AnonFilter_a0_14773();
void DUPLICATE_Splitter_14848();
void iDCT_2D_reference_coarse_14776();
void WEIGHTED_ROUND_ROBIN_Splitter_14856();
void AnonFilter_a3_14858();
void AnonFilter_a3_14859();
void AnonFilter_a3_14860();
void AnonFilter_a3_14861();
void AnonFilter_a3_14862();
void AnonFilter_a3_14863();
void AnonFilter_a3_14864();
void AnonFilter_a3_14865();
void AnonFilter_a3_14866();
void AnonFilter_a3_14867();
void AnonFilter_a3_14868();
void AnonFilter_a3_14869();
void AnonFilter_a3_14870();
void AnonFilter_a3_14871();
void AnonFilter_a3_14872();
void AnonFilter_a3_14873();
void AnonFilter_a3_14874();
void AnonFilter_a3_14875();
void AnonFilter_a3_14876();
void AnonFilter_a3_14877();
void AnonFilter_a3_14878();
void WEIGHTED_ROUND_ROBIN_Joiner_14857();
void Pre_CollapsedDataParallel_1_14846();
void WEIGHTED_ROUND_ROBIN_Splitter_14879();
void iDCT_1D_reference_fine_14881();
void iDCT_1D_reference_fine_14882();
void iDCT_1D_reference_fine_14883();
void iDCT_1D_reference_fine_14884();
void iDCT_1D_reference_fine_14885();
void iDCT_1D_reference_fine_14886();
void iDCT_1D_reference_fine_14887();
void iDCT_1D_reference_fine_14888();
void WEIGHTED_ROUND_ROBIN_Joiner_14880();
void Post_CollapsedDataParallel_2_14847();
void WEIGHTED_ROUND_ROBIN_Splitter_14889();
void iDCT_1D_reference_fine_14891();
void iDCT_1D_reference_fine_14892();
void iDCT_1D_reference_fine_14893();
void iDCT_1D_reference_fine_14894();
void iDCT_1D_reference_fine_14895();
void iDCT_1D_reference_fine_14896();
void iDCT_1D_reference_fine_14897();
void iDCT_1D_reference_fine_14898();
void WEIGHTED_ROUND_ROBIN_Joiner_14890();
void WEIGHTED_ROUND_ROBIN_Splitter_14899();
void AnonFilter_a4_14901();
void AnonFilter_a4_14902();
void AnonFilter_a4_14903();
void AnonFilter_a4_14904();
void AnonFilter_a4_14905();
void AnonFilter_a4_14906();
void AnonFilter_a4_14907();
void AnonFilter_a4_14908();
void AnonFilter_a4_14909();
void AnonFilter_a4_14910();
void AnonFilter_a4_14911();
void AnonFilter_a4_14912();
void AnonFilter_a4_14913();
void AnonFilter_a4_14914();
void AnonFilter_a4_14915();
void AnonFilter_a4_14916();
void AnonFilter_a4_14917();
void AnonFilter_a4_14918();
void AnonFilter_a4_14919();
void AnonFilter_a4_14920();
void AnonFilter_a4_14921();
void WEIGHTED_ROUND_ROBIN_Joiner_14900();
void WEIGHTED_ROUND_ROBIN_Splitter_14922();
void iDCT8x8_1D_row_fast_14924();
void iDCT8x8_1D_row_fast_14925();
void iDCT8x8_1D_row_fast_14926();
void iDCT8x8_1D_row_fast_14927();
void iDCT8x8_1D_row_fast_14928();
void iDCT8x8_1D_row_fast_14929();
void iDCT8x8_1D_row_fast_14930();
void iDCT8x8_1D_row_fast_14931();
void WEIGHTED_ROUND_ROBIN_Joiner_14923();
void iDCT8x8_1D_col_fast_14800();
void WEIGHTED_ROUND_ROBIN_Joiner_14849();
void AnonFilter_a2_14801();

#ifdef __cplusplus
}
#endif
#endif
