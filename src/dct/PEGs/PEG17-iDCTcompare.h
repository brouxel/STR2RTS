#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=6528 on the compile command line
#else
#if BUF_SIZEMAX < 6528
#error BUF_SIZEMAX too small, it must be at least 6528
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_15736_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_15760_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_15761_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_15733();
void DUPLICATE_Splitter_15808();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_15736();
void WEIGHTED_ROUND_ROBIN_Splitter_15816();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_15818();
void AnonFilter_a3_15819();
void AnonFilter_a3_15820();
void AnonFilter_a3_15821();
void AnonFilter_a3_15822();
void AnonFilter_a3_15823();
void AnonFilter_a3_15824();
void AnonFilter_a3_15825();
void AnonFilter_a3_15826();
void AnonFilter_a3_15827();
void AnonFilter_a3_15828();
void AnonFilter_a3_15829();
void AnonFilter_a3_15830();
void AnonFilter_a3_15831();
void AnonFilter_a3_15832();
void AnonFilter_a3_15833();
void AnonFilter_a3_15834();
void WEIGHTED_ROUND_ROBIN_Joiner_15817();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_15806();
void WEIGHTED_ROUND_ROBIN_Splitter_15835();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_15837();
void iDCT_1D_reference_fine_15838();
void iDCT_1D_reference_fine_15839();
void iDCT_1D_reference_fine_15840();
void iDCT_1D_reference_fine_15841();
void iDCT_1D_reference_fine_15842();
void iDCT_1D_reference_fine_15843();
void iDCT_1D_reference_fine_15844();
void WEIGHTED_ROUND_ROBIN_Joiner_15836();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_15807();
void WEIGHTED_ROUND_ROBIN_Splitter_15845();
void iDCT_1D_reference_fine_15847();
void iDCT_1D_reference_fine_15848();
void iDCT_1D_reference_fine_15849();
void iDCT_1D_reference_fine_15850();
void iDCT_1D_reference_fine_15851();
void iDCT_1D_reference_fine_15852();
void iDCT_1D_reference_fine_15853();
void iDCT_1D_reference_fine_15854();
void WEIGHTED_ROUND_ROBIN_Joiner_15846();
void WEIGHTED_ROUND_ROBIN_Splitter_15855();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_15857();
void AnonFilter_a4_15858();
void AnonFilter_a4_15859();
void AnonFilter_a4_15860();
void AnonFilter_a4_15861();
void AnonFilter_a4_15862();
void AnonFilter_a4_15863();
void AnonFilter_a4_15864();
void AnonFilter_a4_15865();
void AnonFilter_a4_15866();
void AnonFilter_a4_15867();
void AnonFilter_a4_15868();
void AnonFilter_a4_15869();
void AnonFilter_a4_15870();
void AnonFilter_a4_15871();
void AnonFilter_a4_15872();
void AnonFilter_a4_15873();
void WEIGHTED_ROUND_ROBIN_Joiner_15856();
void WEIGHTED_ROUND_ROBIN_Splitter_15874();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_15876();
void iDCT8x8_1D_row_fast_15877();
void iDCT8x8_1D_row_fast_15878();
void iDCT8x8_1D_row_fast_15879();
void iDCT8x8_1D_row_fast_15880();
void iDCT8x8_1D_row_fast_15881();
void iDCT8x8_1D_row_fast_15882();
void iDCT8x8_1D_row_fast_15883();
void WEIGHTED_ROUND_ROBIN_Joiner_15875();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_15760();
void WEIGHTED_ROUND_ROBIN_Joiner_15809();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_15761();

#ifdef __cplusplus
}
#endif
#endif
