#include "PEG40-iDCTcompare_nocache.h"

buffer_float_t Pre_CollapsedDataParallel_1_9412WEIGHTED_ROUND_ROBIN_Splitter_9464;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[8];
buffer_int_t AnonFilter_a0_9339DUPLICATE_Splitter_9414;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[40];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9475WEIGHTED_ROUND_ROBIN_Splitter_9484;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[40];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366;
buffer_int_t SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[40];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[8];
buffer_int_t SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[40];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412;
buffer_float_t Post_CollapsedDataParallel_2_9413WEIGHTED_ROUND_ROBIN_Splitter_9474;


iDCT_2D_reference_coarse_9342_t iDCT_2D_reference_coarse_9342_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9466_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9467_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9468_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9469_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9470_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9471_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9472_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9473_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9476_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9477_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9478_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9479_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9480_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9481_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9482_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9483_s;
iDCT8x8_1D_col_fast_9366_t iDCT8x8_1D_col_fast_9366_s;
AnonFilter_a2_9367_t AnonFilter_a2_9367_s;

void AnonFilter_a0_9339(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_9339DUPLICATE_Splitter_9414, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_9342(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_9342_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_9342_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_9424(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9425(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9426(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9427(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9428(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9429(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9430(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9431(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9432(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9433(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9434(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9435(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9436(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9437(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9438(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9439(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9440(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9441(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9442(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9443(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9444(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9445(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9446(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9447(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9448(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9449(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9450(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9451(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9452(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9453(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9454(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9455(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9456(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9457(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9458(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9459(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9460(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9461(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9462(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_9463(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[39])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9422() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9423() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_9412(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_9412WEIGHTED_ROUND_ROBIN_Splitter_9464, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9466(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9466_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9467(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9467_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9468(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9468_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9469(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9469_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9470(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9470_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9471(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9471_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9472(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9472_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9473(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9473_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9412WEIGHTED_ROUND_ROBIN_Splitter_9464));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9465() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9413(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_9413WEIGHTED_ROUND_ROBIN_Splitter_9474, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9476(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9476_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9477(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9477_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9478(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9478_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9479(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9479_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9480(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9480_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9481(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9481_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9482(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9482_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_9483(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9483_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9474() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_9413WEIGHTED_ROUND_ROBIN_Splitter_9474));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9475() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9475WEIGHTED_ROUND_ROBIN_Splitter_9484, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_9486(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9487(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9488(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9489(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9490(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9491(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9492(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9493(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9494(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9495(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9496(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9497(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9498(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9499(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9500(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9501(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9502(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9503(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9504(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9505(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9506(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9507(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9508(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9509(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9510(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9511(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9512(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9513(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9514(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9515(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9516(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9517(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9518(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9519(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9520(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9521(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9522(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9523(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9524(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_9525(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9484() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9475WEIGHTED_ROUND_ROBIN_Splitter_9484));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9485() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_9528(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0], 6) ; 
		x3 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0], 2) ; 
		x4 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0], 1) ; 
		x5 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0], 7) ; 
		x6 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0], 5) ; 
		x7 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_9529(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1], 6) ; 
		x3 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1], 2) ; 
		x4 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1], 1) ; 
		x5 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1], 7) ; 
		x6 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1], 5) ; 
		x7 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_9530(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2], 6) ; 
		x3 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2], 2) ; 
		x4 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2], 1) ; 
		x5 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2], 7) ; 
		x6 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2], 5) ; 
		x7 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_9531(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3], 6) ; 
		x3 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3], 2) ; 
		x4 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3], 1) ; 
		x5 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3], 7) ; 
		x6 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3], 5) ; 
		x7 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_9532(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4], 6) ; 
		x3 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4], 2) ; 
		x4 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4], 1) ; 
		x5 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4], 7) ; 
		x6 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4], 5) ; 
		x7 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_9533(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5], 6) ; 
		x3 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5], 2) ; 
		x4 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5], 1) ; 
		x5 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5], 7) ; 
		x6 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5], 5) ; 
		x7 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_9534(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6], 6) ; 
		x3 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6], 2) ; 
		x4 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6], 1) ; 
		x5 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6], 7) ; 
		x6 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6], 5) ; 
		x7 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_9535(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7], 6) ; 
		x3 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7], 2) ; 
		x4 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7], 1) ; 
		x5 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7], 7) ; 
		x6 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7], 5) ; 
		x7 = peek_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9526() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9527() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_9366(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_9366_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[2], iDCT8x8_1D_col_fast_9366_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_9414() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_9339DUPLICATE_Splitter_9414);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9415() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_9367(){
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367) ; 
		AnonFilter_a2_9367_s.count = (AnonFilter_a2_9367_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_9367_s.errors = (AnonFilter_a2_9367_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_9367_s.errors / AnonFilter_a2_9367_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_9367_s.errors = (AnonFilter_a2_9367_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_9367_s.errors / AnonFilter_a2_9367_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Pre_CollapsedDataParallel_1_9412WEIGHTED_ROUND_ROBIN_Splitter_9464);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_9339DUPLICATE_Splitter_9414);
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367);
	FOR(int, __iter_init_2_, 0, <, 40, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9475WEIGHTED_ROUND_ROBIN_Splitter_9484);
	FOR(int, __iter_init_3_, 0, <, 40, __iter_init_3_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 40, __iter_init_6_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 40, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412);
	init_buffer_float(&Post_CollapsedDataParallel_2_9413WEIGHTED_ROUND_ROBIN_Splitter_9474);
// --- init: iDCT_2D_reference_coarse_9342
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_9342_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9466
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9466_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9467
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9467_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9468
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9468_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9469
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9469_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9470
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9470_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9471
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9471_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9472
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9472_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9473
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9473_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9476
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9476_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9477
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9477_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9478
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9478_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9479
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9479_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9480
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9480_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9481
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9481_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9482
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9482_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9483
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9483_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_9367
	 {
	AnonFilter_a2_9367_s.count = 0.0 ; 
	AnonFilter_a2_9367_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_9339();
		DUPLICATE_Splitter_9414();
			iDCT_2D_reference_coarse_9342();
			WEIGHTED_ROUND_ROBIN_Splitter_9422();
				AnonFilter_a3_9424();
				AnonFilter_a3_9425();
				AnonFilter_a3_9426();
				AnonFilter_a3_9427();
				AnonFilter_a3_9428();
				AnonFilter_a3_9429();
				AnonFilter_a3_9430();
				AnonFilter_a3_9431();
				AnonFilter_a3_9432();
				AnonFilter_a3_9433();
				AnonFilter_a3_9434();
				AnonFilter_a3_9435();
				AnonFilter_a3_9436();
				AnonFilter_a3_9437();
				AnonFilter_a3_9438();
				AnonFilter_a3_9439();
				AnonFilter_a3_9440();
				AnonFilter_a3_9441();
				AnonFilter_a3_9442();
				AnonFilter_a3_9443();
				AnonFilter_a3_9444();
				AnonFilter_a3_9445();
				AnonFilter_a3_9446();
				AnonFilter_a3_9447();
				AnonFilter_a3_9448();
				AnonFilter_a3_9449();
				AnonFilter_a3_9450();
				AnonFilter_a3_9451();
				AnonFilter_a3_9452();
				AnonFilter_a3_9453();
				AnonFilter_a3_9454();
				AnonFilter_a3_9455();
				AnonFilter_a3_9456();
				AnonFilter_a3_9457();
				AnonFilter_a3_9458();
				AnonFilter_a3_9459();
				AnonFilter_a3_9460();
				AnonFilter_a3_9461();
				AnonFilter_a3_9462();
				AnonFilter_a3_9463();
			WEIGHTED_ROUND_ROBIN_Joiner_9423();
			Pre_CollapsedDataParallel_1_9412();
			WEIGHTED_ROUND_ROBIN_Splitter_9464();
				iDCT_1D_reference_fine_9466();
				iDCT_1D_reference_fine_9467();
				iDCT_1D_reference_fine_9468();
				iDCT_1D_reference_fine_9469();
				iDCT_1D_reference_fine_9470();
				iDCT_1D_reference_fine_9471();
				iDCT_1D_reference_fine_9472();
				iDCT_1D_reference_fine_9473();
			WEIGHTED_ROUND_ROBIN_Joiner_9465();
			Post_CollapsedDataParallel_2_9413();
			WEIGHTED_ROUND_ROBIN_Splitter_9474();
				iDCT_1D_reference_fine_9476();
				iDCT_1D_reference_fine_9477();
				iDCT_1D_reference_fine_9478();
				iDCT_1D_reference_fine_9479();
				iDCT_1D_reference_fine_9480();
				iDCT_1D_reference_fine_9481();
				iDCT_1D_reference_fine_9482();
				iDCT_1D_reference_fine_9483();
			WEIGHTED_ROUND_ROBIN_Joiner_9475();
			WEIGHTED_ROUND_ROBIN_Splitter_9484();
				AnonFilter_a4_9486();
				AnonFilter_a4_9487();
				AnonFilter_a4_9488();
				AnonFilter_a4_9489();
				AnonFilter_a4_9490();
				AnonFilter_a4_9491();
				AnonFilter_a4_9492();
				AnonFilter_a4_9493();
				AnonFilter_a4_9494();
				AnonFilter_a4_9495();
				AnonFilter_a4_9496();
				AnonFilter_a4_9497();
				AnonFilter_a4_9498();
				AnonFilter_a4_9499();
				AnonFilter_a4_9500();
				AnonFilter_a4_9501();
				AnonFilter_a4_9502();
				AnonFilter_a4_9503();
				AnonFilter_a4_9504();
				AnonFilter_a4_9505();
				AnonFilter_a4_9506();
				AnonFilter_a4_9507();
				AnonFilter_a4_9508();
				AnonFilter_a4_9509();
				AnonFilter_a4_9510();
				AnonFilter_a4_9511();
				AnonFilter_a4_9512();
				AnonFilter_a4_9513();
				AnonFilter_a4_9514();
				AnonFilter_a4_9515();
				AnonFilter_a4_9516();
				AnonFilter_a4_9517();
				AnonFilter_a4_9518();
				AnonFilter_a4_9519();
				AnonFilter_a4_9520();
				AnonFilter_a4_9521();
				AnonFilter_a4_9522();
				AnonFilter_a4_9523();
				AnonFilter_a4_9524();
				AnonFilter_a4_9525();
			WEIGHTED_ROUND_ROBIN_Joiner_9485();
			WEIGHTED_ROUND_ROBIN_Splitter_9526();
				iDCT8x8_1D_row_fast_9528();
				iDCT8x8_1D_row_fast_9529();
				iDCT8x8_1D_row_fast_9530();
				iDCT8x8_1D_row_fast_9531();
				iDCT8x8_1D_row_fast_9532();
				iDCT8x8_1D_row_fast_9533();
				iDCT8x8_1D_row_fast_9534();
				iDCT8x8_1D_row_fast_9535();
			WEIGHTED_ROUND_ROBIN_Joiner_9527();
			iDCT8x8_1D_col_fast_9366();
		WEIGHTED_ROUND_ROBIN_Joiner_9415();
		AnonFilter_a2_9367();
	ENDFOR
	return EXIT_SUCCESS;
}
