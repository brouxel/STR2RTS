#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2688 on the compile command line
#else
#if BUF_SIZEMAX < 2688
#error BUF_SIZEMAX too small, it must be at least 2688
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_16414_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_16438_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_16439_t;
void AnonFilter_a0_16411();
void DUPLICATE_Splitter_16486();
void iDCT_2D_reference_coarse_16414();
void WEIGHTED_ROUND_ROBIN_Splitter_16494();
void AnonFilter_a3_16496();
void AnonFilter_a3_16497();
void AnonFilter_a3_16498();
void AnonFilter_a3_16499();
void AnonFilter_a3_16500();
void AnonFilter_a3_16501();
void AnonFilter_a3_16502();
void AnonFilter_a3_16503();
void AnonFilter_a3_16504();
void AnonFilter_a3_16505();
void AnonFilter_a3_16506();
void AnonFilter_a3_16507();
void AnonFilter_a3_16508();
void AnonFilter_a3_16509();
void WEIGHTED_ROUND_ROBIN_Joiner_16495();
void Pre_CollapsedDataParallel_1_16484();
void WEIGHTED_ROUND_ROBIN_Splitter_16510();
void iDCT_1D_reference_fine_16512();
void iDCT_1D_reference_fine_16513();
void iDCT_1D_reference_fine_16514();
void iDCT_1D_reference_fine_16515();
void iDCT_1D_reference_fine_16516();
void iDCT_1D_reference_fine_16517();
void iDCT_1D_reference_fine_16518();
void iDCT_1D_reference_fine_16519();
void WEIGHTED_ROUND_ROBIN_Joiner_16511();
void Post_CollapsedDataParallel_2_16485();
void WEIGHTED_ROUND_ROBIN_Splitter_16520();
void iDCT_1D_reference_fine_16522();
void iDCT_1D_reference_fine_16523();
void iDCT_1D_reference_fine_16524();
void iDCT_1D_reference_fine_16525();
void iDCT_1D_reference_fine_16526();
void iDCT_1D_reference_fine_16527();
void iDCT_1D_reference_fine_16528();
void iDCT_1D_reference_fine_16529();
void WEIGHTED_ROUND_ROBIN_Joiner_16521();
void WEIGHTED_ROUND_ROBIN_Splitter_16530();
void AnonFilter_a4_16532();
void AnonFilter_a4_16533();
void AnonFilter_a4_16534();
void AnonFilter_a4_16535();
void AnonFilter_a4_16536();
void AnonFilter_a4_16537();
void AnonFilter_a4_16538();
void AnonFilter_a4_16539();
void AnonFilter_a4_16540();
void AnonFilter_a4_16541();
void AnonFilter_a4_16542();
void AnonFilter_a4_16543();
void AnonFilter_a4_16544();
void AnonFilter_a4_16545();
void WEIGHTED_ROUND_ROBIN_Joiner_16531();
void WEIGHTED_ROUND_ROBIN_Splitter_16546();
void iDCT8x8_1D_row_fast_16548();
void iDCT8x8_1D_row_fast_16549();
void iDCT8x8_1D_row_fast_16550();
void iDCT8x8_1D_row_fast_16551();
void iDCT8x8_1D_row_fast_16552();
void iDCT8x8_1D_row_fast_16553();
void iDCT8x8_1D_row_fast_16554();
void iDCT8x8_1D_row_fast_16555();
void WEIGHTED_ROUND_ROBIN_Joiner_16547();
void iDCT8x8_1D_col_fast_16438();
void WEIGHTED_ROUND_ROBIN_Joiner_16487();
void AnonFilter_a2_16439();

#ifdef __cplusplus
}
#endif
#endif
