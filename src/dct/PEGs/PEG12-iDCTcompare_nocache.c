#include "PEG12-iDCTcompare_nocache.h"

buffer_int_t SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[12];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16941Post_CollapsedDataParallel_2_16917;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_split[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[8];
buffer_int_t AnonFilter_a0_16843DUPLICATE_Splitter_16918;
buffer_float_t Pre_CollapsedDataParallel_1_16916WEIGHTED_ROUND_ROBIN_Splitter_16940;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16927Pre_CollapsedDataParallel_1_16916;
buffer_int_t SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[8];
buffer_float_t Post_CollapsedDataParallel_2_16917WEIGHTED_ROUND_ROBIN_Splitter_16950;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16951WEIGHTED_ROUND_ROBIN_Splitter_16960;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16919AnonFilter_a2_16871;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[12];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_join[3];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[12];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[12];


iDCT_2D_reference_coarse_16846_t iDCT_2D_reference_coarse_16846_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16942_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16943_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16944_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16945_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16946_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16947_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16948_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16949_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16952_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16953_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16954_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16955_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16956_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16957_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16958_s;
iDCT_2D_reference_coarse_16846_t iDCT_1D_reference_fine_16959_s;
iDCT8x8_1D_col_fast_16870_t iDCT8x8_1D_col_fast_16870_s;
AnonFilter_a2_16871_t AnonFilter_a2_16871_s;

void AnonFilter_a0_16843(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_16843DUPLICATE_Splitter_16918, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_16846(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_16846_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_16846_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_16928(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16929(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16930(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16931(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16932(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16933(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16934(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16935(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16936(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16937(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16938(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16939(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[11])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16926() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16927() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16927Pre_CollapsedDataParallel_1_16916, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_16916(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_16916WEIGHTED_ROUND_ROBIN_Splitter_16940, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_16927Pre_CollapsedDataParallel_1_16916, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16927Pre_CollapsedDataParallel_1_16916) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16942(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16942_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16943(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16943_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16944(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16944_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16945(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16945_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16946(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16946_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16947(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16947_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16948(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16948_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16949(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16949_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16940() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_16916WEIGHTED_ROUND_ROBIN_Splitter_16940));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16941() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16941Post_CollapsedDataParallel_2_16917, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_16917(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_16917WEIGHTED_ROUND_ROBIN_Splitter_16950, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_16941Post_CollapsedDataParallel_2_16917, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16941Post_CollapsedDataParallel_2_16917) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16952(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16952_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16953(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16953_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16954(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16954_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16955(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16955_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16956(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16956_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16957(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16957_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16958(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16958_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16959(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16959_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16950() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_16917WEIGHTED_ROUND_ROBIN_Splitter_16950));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16951() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16951WEIGHTED_ROUND_ROBIN_Splitter_16960, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_16962(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16963(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16964(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16965(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16966(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16967(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16968(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16969(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16970(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16971(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16972(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16973(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16960() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16951WEIGHTED_ROUND_ROBIN_Splitter_16960));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16961() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_16976(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0], 6) ; 
		x3 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0], 2) ; 
		x4 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0], 1) ; 
		x5 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0], 7) ; 
		x6 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0], 5) ; 
		x7 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16977(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1], 6) ; 
		x3 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1], 2) ; 
		x4 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1], 1) ; 
		x5 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1], 7) ; 
		x6 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1], 5) ; 
		x7 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16978(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2], 6) ; 
		x3 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2], 2) ; 
		x4 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2], 1) ; 
		x5 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2], 7) ; 
		x6 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2], 5) ; 
		x7 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16979(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3], 6) ; 
		x3 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3], 2) ; 
		x4 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3], 1) ; 
		x5 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3], 7) ; 
		x6 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3], 5) ; 
		x7 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16980(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4], 6) ; 
		x3 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4], 2) ; 
		x4 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4], 1) ; 
		x5 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4], 7) ; 
		x6 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4], 5) ; 
		x7 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16981(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5], 6) ; 
		x3 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5], 2) ; 
		x4 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5], 1) ; 
		x5 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5], 7) ; 
		x6 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5], 5) ; 
		x7 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16982(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6], 6) ; 
		x3 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6], 2) ; 
		x4 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6], 1) ; 
		x5 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6], 7) ; 
		x6 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6], 5) ; 
		x7 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16983(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7], 6) ; 
		x3 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7], 2) ; 
		x4 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7], 1) ; 
		x5 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7], 7) ; 
		x6 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7], 5) ; 
		x7 = peek_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16974() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16975() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, pop_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_16870(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_16870_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_16870_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_16870_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_16870_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_16870_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_16870_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_16870_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_16870_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_16870_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_join[2], iDCT8x8_1D_col_fast_16870_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_16918() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_16843DUPLICATE_Splitter_16918);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16919() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16919AnonFilter_a2_16871, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_16871(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16919AnonFilter_a2_16871) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16919AnonFilter_a2_16871) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16919AnonFilter_a2_16871) ; 
		AnonFilter_a2_16871_s.count = (AnonFilter_a2_16871_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_16871_s.errors = (AnonFilter_a2_16871_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_16871_s.errors / AnonFilter_a2_16871_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_16871_s.errors = (AnonFilter_a2_16871_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_16871_s.errors / AnonFilter_a2_16871_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16975iDCT8x8_1D_col_fast_16870);
	FOR(int, __iter_init_1_, 0, <, 12, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16941Post_CollapsedDataParallel_2_16917);
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_join[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_16843DUPLICATE_Splitter_16918);
	init_buffer_float(&Pre_CollapsedDataParallel_1_16916WEIGHTED_ROUND_ROBIN_Splitter_16940);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16927Pre_CollapsedDataParallel_1_16916);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin48_iDCT8x8_1D_row_fast_Fiss_16989_16996_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_16917WEIGHTED_ROUND_ROBIN_Splitter_16950);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16986_16993_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16951WEIGHTED_ROUND_ROBIN_Splitter_16960);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16919AnonFilter_a2_16871);
	FOR(int, __iter_init_7_, 0, <, 12, __iter_init_7_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16987_16994_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16844_16920_16984_16991_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 12, __iter_init_10_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_16985_16992_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 12, __iter_init_11_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_16988_16995_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_16846
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_16846_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16942
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16942_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16943
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16943_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16944
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16944_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16945
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16945_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16946
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16946_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16947
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16947_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16948
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16948_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16949
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16949_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16952
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16952_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16953
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16953_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16954
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16954_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16955
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16955_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16956
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16956_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16957
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16957_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16958
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16958_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16959
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16959_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_16871
	 {
	AnonFilter_a2_16871_s.count = 0.0 ; 
	AnonFilter_a2_16871_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_16843();
		DUPLICATE_Splitter_16918();
			iDCT_2D_reference_coarse_16846();
			WEIGHTED_ROUND_ROBIN_Splitter_16926();
				AnonFilter_a3_16928();
				AnonFilter_a3_16929();
				AnonFilter_a3_16930();
				AnonFilter_a3_16931();
				AnonFilter_a3_16932();
				AnonFilter_a3_16933();
				AnonFilter_a3_16934();
				AnonFilter_a3_16935();
				AnonFilter_a3_16936();
				AnonFilter_a3_16937();
				AnonFilter_a3_16938();
				AnonFilter_a3_16939();
			WEIGHTED_ROUND_ROBIN_Joiner_16927();
			Pre_CollapsedDataParallel_1_16916();
			WEIGHTED_ROUND_ROBIN_Splitter_16940();
				iDCT_1D_reference_fine_16942();
				iDCT_1D_reference_fine_16943();
				iDCT_1D_reference_fine_16944();
				iDCT_1D_reference_fine_16945();
				iDCT_1D_reference_fine_16946();
				iDCT_1D_reference_fine_16947();
				iDCT_1D_reference_fine_16948();
				iDCT_1D_reference_fine_16949();
			WEIGHTED_ROUND_ROBIN_Joiner_16941();
			Post_CollapsedDataParallel_2_16917();
			WEIGHTED_ROUND_ROBIN_Splitter_16950();
				iDCT_1D_reference_fine_16952();
				iDCT_1D_reference_fine_16953();
				iDCT_1D_reference_fine_16954();
				iDCT_1D_reference_fine_16955();
				iDCT_1D_reference_fine_16956();
				iDCT_1D_reference_fine_16957();
				iDCT_1D_reference_fine_16958();
				iDCT_1D_reference_fine_16959();
			WEIGHTED_ROUND_ROBIN_Joiner_16951();
			WEIGHTED_ROUND_ROBIN_Splitter_16960();
				AnonFilter_a4_16962();
				AnonFilter_a4_16963();
				AnonFilter_a4_16964();
				AnonFilter_a4_16965();
				AnonFilter_a4_16966();
				AnonFilter_a4_16967();
				AnonFilter_a4_16968();
				AnonFilter_a4_16969();
				AnonFilter_a4_16970();
				AnonFilter_a4_16971();
				AnonFilter_a4_16972();
				AnonFilter_a4_16973();
			WEIGHTED_ROUND_ROBIN_Joiner_16961();
			WEIGHTED_ROUND_ROBIN_Splitter_16974();
				iDCT8x8_1D_row_fast_16976();
				iDCT8x8_1D_row_fast_16977();
				iDCT8x8_1D_row_fast_16978();
				iDCT8x8_1D_row_fast_16979();
				iDCT8x8_1D_row_fast_16980();
				iDCT8x8_1D_row_fast_16981();
				iDCT8x8_1D_row_fast_16982();
				iDCT8x8_1D_row_fast_16983();
			WEIGHTED_ROUND_ROBIN_Joiner_16975();
			iDCT8x8_1D_col_fast_16870();
		WEIGHTED_ROUND_ROBIN_Joiner_16919();
		AnonFilter_a2_16871();
	ENDFOR
	return EXIT_SUCCESS;
}
