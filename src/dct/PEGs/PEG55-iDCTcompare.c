#include "PEG55-iDCTcompare.h"

buffer_float_t Pre_CollapsedDataParallel_1_4102WEIGHTED_ROUND_ROBIN_Splitter_4169;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[55];
buffer_int_t SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4189;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[55];
buffer_float_t Post_CollapsedDataParallel_2_4103WEIGHTED_ROUND_ROBIN_Splitter_4179;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_4247iDCT8x8_1D_col_fast_4056;
buffer_int_t SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_split[3];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[55];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[55];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4170Post_CollapsedDataParallel_2_4103;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[8];
buffer_int_t AnonFilter_a0_4029DUPLICATE_Splitter_4104;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_4105AnonFilter_a2_4057;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4113Pre_CollapsedDataParallel_1_4102;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[8];


iDCT_2D_reference_coarse_4032_t iDCT_2D_reference_coarse_4032_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4171_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4172_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4173_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4174_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4175_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4176_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4177_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4178_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4181_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4182_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4183_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4184_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4185_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4186_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4187_s;
iDCT_2D_reference_coarse_4032_t iDCT_1D_reference_fine_4188_s;
iDCT8x8_1D_col_fast_4056_t iDCT8x8_1D_col_fast_4056_s;
AnonFilter_a2_4057_t AnonFilter_a2_4057_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_4029() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_4029DUPLICATE_Splitter_4104));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_4032_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_4032_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_4032() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_4114() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[0]));
	ENDFOR
}

void AnonFilter_a3_4115() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[1]));
	ENDFOR
}

void AnonFilter_a3_4116() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[2]));
	ENDFOR
}

void AnonFilter_a3_4117() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[3]));
	ENDFOR
}

void AnonFilter_a3_4118() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[4]));
	ENDFOR
}

void AnonFilter_a3_4119() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[5]));
	ENDFOR
}

void AnonFilter_a3_4120() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[6]));
	ENDFOR
}

void AnonFilter_a3_4121() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[7]));
	ENDFOR
}

void AnonFilter_a3_4122() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[8]));
	ENDFOR
}

void AnonFilter_a3_4123() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[9]));
	ENDFOR
}

void AnonFilter_a3_4124() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[10]));
	ENDFOR
}

void AnonFilter_a3_4125() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[11]));
	ENDFOR
}

void AnonFilter_a3_4126() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[12]));
	ENDFOR
}

void AnonFilter_a3_4127() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[13]));
	ENDFOR
}

void AnonFilter_a3_4128() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[14]));
	ENDFOR
}

void AnonFilter_a3_4129() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[15]));
	ENDFOR
}

void AnonFilter_a3_4130() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[16]));
	ENDFOR
}

void AnonFilter_a3_4131() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[17]));
	ENDFOR
}

void AnonFilter_a3_4132() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[18]));
	ENDFOR
}

void AnonFilter_a3_4133() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[19]));
	ENDFOR
}

void AnonFilter_a3_4134() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[20]));
	ENDFOR
}

void AnonFilter_a3_4135() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[21]));
	ENDFOR
}

void AnonFilter_a3_4136() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[22]));
	ENDFOR
}

void AnonFilter_a3_4137() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[23]));
	ENDFOR
}

void AnonFilter_a3_4138() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[24]));
	ENDFOR
}

void AnonFilter_a3_4139() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[25]));
	ENDFOR
}

void AnonFilter_a3_4140() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[26]));
	ENDFOR
}

void AnonFilter_a3_4141() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[27]));
	ENDFOR
}

void AnonFilter_a3_4142() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[28]));
	ENDFOR
}

void AnonFilter_a3_4143() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[29]));
	ENDFOR
}

void AnonFilter_a3_4144() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[30]));
	ENDFOR
}

void AnonFilter_a3_4145() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[31]));
	ENDFOR
}

void AnonFilter_a3_4146() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[32]));
	ENDFOR
}

void AnonFilter_a3_4147() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[33]));
	ENDFOR
}

void AnonFilter_a3_4148() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[34]));
	ENDFOR
}

void AnonFilter_a3_4149() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[35]));
	ENDFOR
}

void AnonFilter_a3_4150() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[36]));
	ENDFOR
}

void AnonFilter_a3_4151() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[37]));
	ENDFOR
}

void AnonFilter_a3_4152() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[38]));
	ENDFOR
}

void AnonFilter_a3_4153() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[39]));
	ENDFOR
}

void AnonFilter_a3_4154() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[40]));
	ENDFOR
}

void AnonFilter_a3_4155() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[41]));
	ENDFOR
}

void AnonFilter_a3_4156() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[42]));
	ENDFOR
}

void AnonFilter_a3_4157() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[43]));
	ENDFOR
}

void AnonFilter_a3_4158() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[44]));
	ENDFOR
}

void AnonFilter_a3_4159() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[45]));
	ENDFOR
}

void AnonFilter_a3_4160() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[46]));
	ENDFOR
}

void AnonFilter_a3_4161() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[47]));
	ENDFOR
}

void AnonFilter_a3_4162() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[48]));
	ENDFOR
}

void AnonFilter_a3_4163() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[49]));
	ENDFOR
}

void AnonFilter_a3_4164() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[50]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[50]));
	ENDFOR
}

void AnonFilter_a3_4165() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[51]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[51]));
	ENDFOR
}

void AnonFilter_a3_4166() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[52]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[52]));
	ENDFOR
}

void AnonFilter_a3_4167() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[53]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[53]));
	ENDFOR
}

void AnonFilter_a3_4168() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[54]), &(SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[54]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4112() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 55, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4113() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 55, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4113Pre_CollapsedDataParallel_1_4102, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_4102() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_4113Pre_CollapsedDataParallel_1_4102), &(Pre_CollapsedDataParallel_1_4102WEIGHTED_ROUND_ROBIN_Splitter_4169));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4171_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_4171() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_4172() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_4173() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_4174() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_4175() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_4176() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_4177() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_4178() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4169() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4102WEIGHTED_ROUND_ROBIN_Splitter_4169));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4170Post_CollapsedDataParallel_2_4103, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_4103() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4170Post_CollapsedDataParallel_2_4103), &(Post_CollapsedDataParallel_2_4103WEIGHTED_ROUND_ROBIN_Splitter_4179));
	ENDFOR
}

void iDCT_1D_reference_fine_4181() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_4182() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_4183() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_4184() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_4185() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_4186() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_4187() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_4188() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_4103WEIGHTED_ROUND_ROBIN_Splitter_4179));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4189, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_4191() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[0]));
	ENDFOR
}

void AnonFilter_a4_4192() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[1]));
	ENDFOR
}

void AnonFilter_a4_4193() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[2]));
	ENDFOR
}

void AnonFilter_a4_4194() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[3]));
	ENDFOR
}

void AnonFilter_a4_4195() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[4]));
	ENDFOR
}

void AnonFilter_a4_4196() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[5]));
	ENDFOR
}

void AnonFilter_a4_4197() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[6]));
	ENDFOR
}

void AnonFilter_a4_4198() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[7]));
	ENDFOR
}

void AnonFilter_a4_4199() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[8]));
	ENDFOR
}

void AnonFilter_a4_4200() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[9]));
	ENDFOR
}

void AnonFilter_a4_4201() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[10]));
	ENDFOR
}

void AnonFilter_a4_4202() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[11]));
	ENDFOR
}

void AnonFilter_a4_4203() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[12]));
	ENDFOR
}

void AnonFilter_a4_4204() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[13]));
	ENDFOR
}

void AnonFilter_a4_4205() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[14]));
	ENDFOR
}

void AnonFilter_a4_4206() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[15]));
	ENDFOR
}

void AnonFilter_a4_4207() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[16]));
	ENDFOR
}

void AnonFilter_a4_4208() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[17]));
	ENDFOR
}

void AnonFilter_a4_4209() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[18]));
	ENDFOR
}

void AnonFilter_a4_4210() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[19]));
	ENDFOR
}

void AnonFilter_a4_4211() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[20]));
	ENDFOR
}

void AnonFilter_a4_4212() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[21]));
	ENDFOR
}

void AnonFilter_a4_4213() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[22]));
	ENDFOR
}

void AnonFilter_a4_4214() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[23]));
	ENDFOR
}

void AnonFilter_a4_4215() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[24]));
	ENDFOR
}

void AnonFilter_a4_4216() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[25]));
	ENDFOR
}

void AnonFilter_a4_4217() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[26]));
	ENDFOR
}

void AnonFilter_a4_4218() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[27]));
	ENDFOR
}

void AnonFilter_a4_4219() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[28]));
	ENDFOR
}

void AnonFilter_a4_4220() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[29]));
	ENDFOR
}

void AnonFilter_a4_4221() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[30]));
	ENDFOR
}

void AnonFilter_a4_4222() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[31]));
	ENDFOR
}

void AnonFilter_a4_4223() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[32]));
	ENDFOR
}

void AnonFilter_a4_4224() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[33]));
	ENDFOR
}

void AnonFilter_a4_4225() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[34]));
	ENDFOR
}

void AnonFilter_a4_4226() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[35]));
	ENDFOR
}

void AnonFilter_a4_4227() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[36]));
	ENDFOR
}

void AnonFilter_a4_4228() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[37]));
	ENDFOR
}

void AnonFilter_a4_4229() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[38]));
	ENDFOR
}

void AnonFilter_a4_4230() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[39]));
	ENDFOR
}

void AnonFilter_a4_4231() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[40]));
	ENDFOR
}

void AnonFilter_a4_4232() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[41]));
	ENDFOR
}

void AnonFilter_a4_4233() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[42]));
	ENDFOR
}

void AnonFilter_a4_4234() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[43]));
	ENDFOR
}

void AnonFilter_a4_4235() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[44]));
	ENDFOR
}

void AnonFilter_a4_4236() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[45]));
	ENDFOR
}

void AnonFilter_a4_4237() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[46]));
	ENDFOR
}

void AnonFilter_a4_4238() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[47]));
	ENDFOR
}

void AnonFilter_a4_4239() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[48]));
	ENDFOR
}

void AnonFilter_a4_4240() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[49]));
	ENDFOR
}

void AnonFilter_a4_4241() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[50]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[50]));
	ENDFOR
}

void AnonFilter_a4_4242() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[51]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[51]));
	ENDFOR
}

void AnonFilter_a4_4243() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[52]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[52]));
	ENDFOR
}

void AnonFilter_a4_4244() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[53]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[53]));
	ENDFOR
}

void AnonFilter_a4_4245() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[54]), &(SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[54]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4189() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 55, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4189));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4190() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 55, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_4248() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[0]), &(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_4249() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[1]), &(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_4250() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[2]), &(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_4251() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[3]), &(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_4252() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[4]), &(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_4253() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[5]), &(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_4254() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[6]), &(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_4255() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[7]), &(SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4246() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4247() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_4247iDCT8x8_1D_col_fast_4056, pop_int(&SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_4056_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_4056_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_4056_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_4056_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_4056_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_4056_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_4056_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_4056_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_4056_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_4056_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_4056() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_4247iDCT8x8_1D_col_fast_4056), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_4104() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3520, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_4029DUPLICATE_Splitter_4104);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4105() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3520, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_4105AnonFilter_a2_4057, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_4057_s.count = (AnonFilter_a2_4057_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_4057_s.errors = (AnonFilter_a2_4057_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_4057_s.errors / AnonFilter_a2_4057_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_4057_s.errors = (AnonFilter_a2_4057_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_4057_s.errors / AnonFilter_a2_4057_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_4057() {
	FOR(uint32_t, __iter_steady_, 0, <, 3520, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_4105AnonFilter_a2_4057));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Pre_CollapsedDataParallel_1_4102WEIGHTED_ROUND_ROBIN_Splitter_4169);
	FOR(int, __iter_init_0_, 0, <, 55, __iter_init_0_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_4260_4267_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4189);
	FOR(int, __iter_init_3_, 0, <, 55, __iter_init_3_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_4257_4264_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_4103WEIGHTED_ROUND_ROBIN_Splitter_4179);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_4247iDCT8x8_1D_col_fast_4056);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin134_iDCT8x8_1D_row_fast_Fiss_4261_4268_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4030_4106_4256_4263_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 55, __iter_init_6_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_4257_4264_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 55, __iter_init_7_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_4260_4267_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4170Post_CollapsedDataParallel_2_4103);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_split[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_4029DUPLICATE_Splitter_4104);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4259_4266_join[__iter_init_9_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_4105AnonFilter_a2_4057);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4113Pre_CollapsedDataParallel_1_4102);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4258_4265_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_4032
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_4032_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4171
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4171_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4172
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4172_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4173
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4173_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4174
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4174_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4175
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4175_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4176
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4176_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4177
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4177_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4178
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4178_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4181
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4181_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4182
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4182_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4183
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4183_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4184
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4184_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4185
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4185_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4186
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4186_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4187
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4187_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4188
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4188_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_4057
	 {
	AnonFilter_a2_4057_s.count = 0.0 ; 
	AnonFilter_a2_4057_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_4029();
		DUPLICATE_Splitter_4104();
			iDCT_2D_reference_coarse_4032();
			WEIGHTED_ROUND_ROBIN_Splitter_4112();
				AnonFilter_a3_4114();
				AnonFilter_a3_4115();
				AnonFilter_a3_4116();
				AnonFilter_a3_4117();
				AnonFilter_a3_4118();
				AnonFilter_a3_4119();
				AnonFilter_a3_4120();
				AnonFilter_a3_4121();
				AnonFilter_a3_4122();
				AnonFilter_a3_4123();
				AnonFilter_a3_4124();
				AnonFilter_a3_4125();
				AnonFilter_a3_4126();
				AnonFilter_a3_4127();
				AnonFilter_a3_4128();
				AnonFilter_a3_4129();
				AnonFilter_a3_4130();
				AnonFilter_a3_4131();
				AnonFilter_a3_4132();
				AnonFilter_a3_4133();
				AnonFilter_a3_4134();
				AnonFilter_a3_4135();
				AnonFilter_a3_4136();
				AnonFilter_a3_4137();
				AnonFilter_a3_4138();
				AnonFilter_a3_4139();
				AnonFilter_a3_4140();
				AnonFilter_a3_4141();
				AnonFilter_a3_4142();
				AnonFilter_a3_4143();
				AnonFilter_a3_4144();
				AnonFilter_a3_4145();
				AnonFilter_a3_4146();
				AnonFilter_a3_4147();
				AnonFilter_a3_4148();
				AnonFilter_a3_4149();
				AnonFilter_a3_4150();
				AnonFilter_a3_4151();
				AnonFilter_a3_4152();
				AnonFilter_a3_4153();
				AnonFilter_a3_4154();
				AnonFilter_a3_4155();
				AnonFilter_a3_4156();
				AnonFilter_a3_4157();
				AnonFilter_a3_4158();
				AnonFilter_a3_4159();
				AnonFilter_a3_4160();
				AnonFilter_a3_4161();
				AnonFilter_a3_4162();
				AnonFilter_a3_4163();
				AnonFilter_a3_4164();
				AnonFilter_a3_4165();
				AnonFilter_a3_4166();
				AnonFilter_a3_4167();
				AnonFilter_a3_4168();
			WEIGHTED_ROUND_ROBIN_Joiner_4113();
			Pre_CollapsedDataParallel_1_4102();
			WEIGHTED_ROUND_ROBIN_Splitter_4169();
				iDCT_1D_reference_fine_4171();
				iDCT_1D_reference_fine_4172();
				iDCT_1D_reference_fine_4173();
				iDCT_1D_reference_fine_4174();
				iDCT_1D_reference_fine_4175();
				iDCT_1D_reference_fine_4176();
				iDCT_1D_reference_fine_4177();
				iDCT_1D_reference_fine_4178();
			WEIGHTED_ROUND_ROBIN_Joiner_4170();
			Post_CollapsedDataParallel_2_4103();
			WEIGHTED_ROUND_ROBIN_Splitter_4179();
				iDCT_1D_reference_fine_4181();
				iDCT_1D_reference_fine_4182();
				iDCT_1D_reference_fine_4183();
				iDCT_1D_reference_fine_4184();
				iDCT_1D_reference_fine_4185();
				iDCT_1D_reference_fine_4186();
				iDCT_1D_reference_fine_4187();
				iDCT_1D_reference_fine_4188();
			WEIGHTED_ROUND_ROBIN_Joiner_4180();
			WEIGHTED_ROUND_ROBIN_Splitter_4189();
				AnonFilter_a4_4191();
				AnonFilter_a4_4192();
				AnonFilter_a4_4193();
				AnonFilter_a4_4194();
				AnonFilter_a4_4195();
				AnonFilter_a4_4196();
				AnonFilter_a4_4197();
				AnonFilter_a4_4198();
				AnonFilter_a4_4199();
				AnonFilter_a4_4200();
				AnonFilter_a4_4201();
				AnonFilter_a4_4202();
				AnonFilter_a4_4203();
				AnonFilter_a4_4204();
				AnonFilter_a4_4205();
				AnonFilter_a4_4206();
				AnonFilter_a4_4207();
				AnonFilter_a4_4208();
				AnonFilter_a4_4209();
				AnonFilter_a4_4210();
				AnonFilter_a4_4211();
				AnonFilter_a4_4212();
				AnonFilter_a4_4213();
				AnonFilter_a4_4214();
				AnonFilter_a4_4215();
				AnonFilter_a4_4216();
				AnonFilter_a4_4217();
				AnonFilter_a4_4218();
				AnonFilter_a4_4219();
				AnonFilter_a4_4220();
				AnonFilter_a4_4221();
				AnonFilter_a4_4222();
				AnonFilter_a4_4223();
				AnonFilter_a4_4224();
				AnonFilter_a4_4225();
				AnonFilter_a4_4226();
				AnonFilter_a4_4227();
				AnonFilter_a4_4228();
				AnonFilter_a4_4229();
				AnonFilter_a4_4230();
				AnonFilter_a4_4231();
				AnonFilter_a4_4232();
				AnonFilter_a4_4233();
				AnonFilter_a4_4234();
				AnonFilter_a4_4235();
				AnonFilter_a4_4236();
				AnonFilter_a4_4237();
				AnonFilter_a4_4238();
				AnonFilter_a4_4239();
				AnonFilter_a4_4240();
				AnonFilter_a4_4241();
				AnonFilter_a4_4242();
				AnonFilter_a4_4243();
				AnonFilter_a4_4244();
				AnonFilter_a4_4245();
			WEIGHTED_ROUND_ROBIN_Joiner_4190();
			WEIGHTED_ROUND_ROBIN_Splitter_4246();
				iDCT8x8_1D_row_fast_4248();
				iDCT8x8_1D_row_fast_4249();
				iDCT8x8_1D_row_fast_4250();
				iDCT8x8_1D_row_fast_4251();
				iDCT8x8_1D_row_fast_4252();
				iDCT8x8_1D_row_fast_4253();
				iDCT8x8_1D_row_fast_4254();
				iDCT8x8_1D_row_fast_4255();
			WEIGHTED_ROUND_ROBIN_Joiner_4247();
			iDCT8x8_1D_col_fast_4056();
		WEIGHTED_ROUND_ROBIN_Joiner_4105();
		AnonFilter_a2_4057();
	ENDFOR
	return EXIT_SUCCESS;
}
