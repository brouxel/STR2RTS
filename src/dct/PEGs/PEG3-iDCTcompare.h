#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1152 on the compile command line
#else
#if BUF_SIZEMAX < 1152
#error BUF_SIZEMAX too small, it must be at least 1152
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_18532_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_18556_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_18557_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_18529();
void DUPLICATE_Splitter_18604();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_18532();
void WEIGHTED_ROUND_ROBIN_Splitter_18612();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_18614();
void AnonFilter_a3_18615();
void AnonFilter_a3_18616();
void WEIGHTED_ROUND_ROBIN_Joiner_18613();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_18602();
void WEIGHTED_ROUND_ROBIN_Splitter_18617();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_18619();
void iDCT_1D_reference_fine_18620();
void iDCT_1D_reference_fine_18621();
void WEIGHTED_ROUND_ROBIN_Joiner_18618();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_18603();
void WEIGHTED_ROUND_ROBIN_Splitter_18622();
void iDCT_1D_reference_fine_18624();
void iDCT_1D_reference_fine_18625();
void iDCT_1D_reference_fine_18626();
void WEIGHTED_ROUND_ROBIN_Joiner_18623();
void WEIGHTED_ROUND_ROBIN_Splitter_18627();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_18629();
void AnonFilter_a4_18630();
void AnonFilter_a4_18631();
void WEIGHTED_ROUND_ROBIN_Joiner_18628();
void WEIGHTED_ROUND_ROBIN_Splitter_18632();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_18634();
void iDCT8x8_1D_row_fast_18635();
void iDCT8x8_1D_row_fast_18636();
void WEIGHTED_ROUND_ROBIN_Joiner_18633();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_18556();
void WEIGHTED_ROUND_ROBIN_Joiner_18605();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_18557();

#ifdef __cplusplus
}
#endif
#endif
