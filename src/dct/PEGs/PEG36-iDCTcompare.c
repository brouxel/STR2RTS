#include "PEG36-iDCTcompare.h"

buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[36];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_10679AnonFilter_a2_10631;
buffer_int_t SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_10676WEIGHTED_ROUND_ROBIN_Splitter_10724;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[8];
buffer_float_t Post_CollapsedDataParallel_2_10677WEIGHTED_ROUND_ROBIN_Splitter_10734;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[36];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10687Pre_CollapsedDataParallel_1_10676;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[36];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_10783iDCT8x8_1D_col_fast_10630;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10735WEIGHTED_ROUND_ROBIN_Splitter_10744;
buffer_int_t AnonFilter_a0_10603DUPLICATE_Splitter_10678;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[36];
buffer_int_t SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10725Post_CollapsedDataParallel_2_10677;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_join[3];


iDCT_2D_reference_coarse_10606_t iDCT_2D_reference_coarse_10606_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10726_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10727_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10728_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10729_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10730_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10731_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10732_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10733_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10736_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10737_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10738_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10739_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10740_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10741_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10742_s;
iDCT_2D_reference_coarse_10606_t iDCT_1D_reference_fine_10743_s;
iDCT8x8_1D_col_fast_10630_t iDCT8x8_1D_col_fast_10630_s;
AnonFilter_a2_10631_t AnonFilter_a2_10631_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_10603() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_10603DUPLICATE_Splitter_10678));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_10606_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_10606_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_10606() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_10688() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[0]));
	ENDFOR
}

void AnonFilter_a3_10689() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[1]));
	ENDFOR
}

void AnonFilter_a3_10690() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[2]));
	ENDFOR
}

void AnonFilter_a3_10691() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[3]));
	ENDFOR
}

void AnonFilter_a3_10692() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[4]));
	ENDFOR
}

void AnonFilter_a3_10693() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[5]));
	ENDFOR
}

void AnonFilter_a3_10694() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[6]));
	ENDFOR
}

void AnonFilter_a3_10695() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[7]));
	ENDFOR
}

void AnonFilter_a3_10696() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[8]));
	ENDFOR
}

void AnonFilter_a3_10697() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[9]));
	ENDFOR
}

void AnonFilter_a3_10698() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[10]));
	ENDFOR
}

void AnonFilter_a3_10699() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[11]));
	ENDFOR
}

void AnonFilter_a3_10700() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[12]));
	ENDFOR
}

void AnonFilter_a3_10701() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[13]));
	ENDFOR
}

void AnonFilter_a3_10702() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[14]));
	ENDFOR
}

void AnonFilter_a3_10703() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[15]));
	ENDFOR
}

void AnonFilter_a3_10704() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[16]));
	ENDFOR
}

void AnonFilter_a3_10705() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[17]));
	ENDFOR
}

void AnonFilter_a3_10706() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[18]));
	ENDFOR
}

void AnonFilter_a3_10707() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[19]));
	ENDFOR
}

void AnonFilter_a3_10708() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[20]));
	ENDFOR
}

void AnonFilter_a3_10709() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[21]));
	ENDFOR
}

void AnonFilter_a3_10710() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[22]));
	ENDFOR
}

void AnonFilter_a3_10711() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[23]));
	ENDFOR
}

void AnonFilter_a3_10712() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[24]));
	ENDFOR
}

void AnonFilter_a3_10713() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[25]));
	ENDFOR
}

void AnonFilter_a3_10714() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[26]));
	ENDFOR
}

void AnonFilter_a3_10715() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[27]));
	ENDFOR
}

void AnonFilter_a3_10716() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[28]));
	ENDFOR
}

void AnonFilter_a3_10717() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[29]));
	ENDFOR
}

void AnonFilter_a3_10718() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[30]));
	ENDFOR
}

void AnonFilter_a3_10719() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[31]));
	ENDFOR
}

void AnonFilter_a3_10720() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[32]));
	ENDFOR
}

void AnonFilter_a3_10721() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[33]));
	ENDFOR
}

void AnonFilter_a3_10722() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[34]));
	ENDFOR
}

void AnonFilter_a3_10723() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[35]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10686() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 36, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10687() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 36, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10687Pre_CollapsedDataParallel_1_10676, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_10676() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_10687Pre_CollapsedDataParallel_1_10676), &(Pre_CollapsedDataParallel_1_10676WEIGHTED_ROUND_ROBIN_Splitter_10724));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_10726_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_10726() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_10727() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_10728() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_10729() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_10730() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_10731() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_10732() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_10733() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10724() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10676WEIGHTED_ROUND_ROBIN_Splitter_10724));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10725() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10725Post_CollapsedDataParallel_2_10677, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_10677() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_10725Post_CollapsedDataParallel_2_10677), &(Post_CollapsedDataParallel_2_10677WEIGHTED_ROUND_ROBIN_Splitter_10734));
	ENDFOR
}

void iDCT_1D_reference_fine_10736() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_10737() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_10738() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_10739() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_10740() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_10741() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_10742() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_10743() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10734() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_10677WEIGHTED_ROUND_ROBIN_Splitter_10734));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10735() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10735WEIGHTED_ROUND_ROBIN_Splitter_10744, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_10746() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[0]));
	ENDFOR
}

void AnonFilter_a4_10747() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[1]));
	ENDFOR
}

void AnonFilter_a4_10748() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[2]));
	ENDFOR
}

void AnonFilter_a4_10749() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[3]));
	ENDFOR
}

void AnonFilter_a4_10750() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[4]));
	ENDFOR
}

void AnonFilter_a4_10751() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[5]));
	ENDFOR
}

void AnonFilter_a4_10752() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[6]));
	ENDFOR
}

void AnonFilter_a4_10753() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[7]));
	ENDFOR
}

void AnonFilter_a4_10754() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[8]));
	ENDFOR
}

void AnonFilter_a4_10755() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[9]));
	ENDFOR
}

void AnonFilter_a4_10756() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[10]));
	ENDFOR
}

void AnonFilter_a4_10757() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[11]));
	ENDFOR
}

void AnonFilter_a4_10758() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[12]));
	ENDFOR
}

void AnonFilter_a4_10759() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[13]));
	ENDFOR
}

void AnonFilter_a4_10760() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[14]));
	ENDFOR
}

void AnonFilter_a4_10761() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[15]));
	ENDFOR
}

void AnonFilter_a4_10762() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[16]));
	ENDFOR
}

void AnonFilter_a4_10763() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[17]));
	ENDFOR
}

void AnonFilter_a4_10764() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[18]));
	ENDFOR
}

void AnonFilter_a4_10765() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[19]));
	ENDFOR
}

void AnonFilter_a4_10766() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[20]));
	ENDFOR
}

void AnonFilter_a4_10767() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[21]));
	ENDFOR
}

void AnonFilter_a4_10768() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[22]));
	ENDFOR
}

void AnonFilter_a4_10769() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[23]));
	ENDFOR
}

void AnonFilter_a4_10770() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[24]));
	ENDFOR
}

void AnonFilter_a4_10771() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[25]));
	ENDFOR
}

void AnonFilter_a4_10772() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[26]));
	ENDFOR
}

void AnonFilter_a4_10773() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[27]));
	ENDFOR
}

void AnonFilter_a4_10774() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[28]));
	ENDFOR
}

void AnonFilter_a4_10775() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[29]));
	ENDFOR
}

void AnonFilter_a4_10776() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[30]));
	ENDFOR
}

void AnonFilter_a4_10777() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[31]));
	ENDFOR
}

void AnonFilter_a4_10778() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[32]));
	ENDFOR
}

void AnonFilter_a4_10779() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[33]));
	ENDFOR
}

void AnonFilter_a4_10780() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[34]));
	ENDFOR
}

void AnonFilter_a4_10781() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[35]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10744() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 36, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10735WEIGHTED_ROUND_ROBIN_Splitter_10744));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10745() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 36, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_10784() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[0]), &(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10785() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[1]), &(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10786() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[2]), &(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10787() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[3]), &(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10788() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[4]), &(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10789() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[5]), &(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10790() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[6]), &(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10791() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[7]), &(SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10782() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10783() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_10783iDCT8x8_1D_col_fast_10630, pop_int(&SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_10630_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_10630_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_10630_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_10630_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_10630_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_10630_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_10630_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_10630_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_10630_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_10630_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_10630() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_10783iDCT8x8_1D_col_fast_10630), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_10678() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_10603DUPLICATE_Splitter_10678);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10679() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_10679AnonFilter_a2_10631, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_10631_s.count = (AnonFilter_a2_10631_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_10631_s.errors = (AnonFilter_a2_10631_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_10631_s.errors / AnonFilter_a2_10631_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_10631_s.errors = (AnonFilter_a2_10631_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_10631_s.errors / AnonFilter_a2_10631_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_10631() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_10679AnonFilter_a2_10631));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 36, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_10793_10800_join[__iter_init_2_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_10679AnonFilter_a2_10631);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_int(&SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10676WEIGHTED_ROUND_ROBIN_Splitter_10724);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10795_10802_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10794_10801_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_10677WEIGHTED_ROUND_ROBIN_Splitter_10734);
	FOR(int, __iter_init_6_, 0, <, 36, __iter_init_6_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_10796_10803_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10687Pre_CollapsedDataParallel_1_10676);
	FOR(int, __iter_init_8_, 0, <, 36, __iter_init_8_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_10796_10803_join[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_10783iDCT8x8_1D_col_fast_10630);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10735WEIGHTED_ROUND_ROBIN_Splitter_10744);
	init_buffer_int(&AnonFilter_a0_10603DUPLICATE_Splitter_10678);
	FOR(int, __iter_init_9_, 0, <, 36, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_10793_10800_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_int(&SplitJoin96_iDCT8x8_1D_row_fast_Fiss_10797_10804_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10725Post_CollapsedDataParallel_2_10677);
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10604_10680_10792_10799_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_10606
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_10606_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10726
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10726_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10727
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10727_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10728
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10728_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10729
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10729_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10730
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10730_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10731
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10731_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10732
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10732_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10733
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10733_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10736
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10736_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10737
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10737_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10738
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10738_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10739
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10739_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10740
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10740_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10741
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10741_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10742
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10742_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10743
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10743_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_10631
	 {
	AnonFilter_a2_10631_s.count = 0.0 ; 
	AnonFilter_a2_10631_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_10603();
		DUPLICATE_Splitter_10678();
			iDCT_2D_reference_coarse_10606();
			WEIGHTED_ROUND_ROBIN_Splitter_10686();
				AnonFilter_a3_10688();
				AnonFilter_a3_10689();
				AnonFilter_a3_10690();
				AnonFilter_a3_10691();
				AnonFilter_a3_10692();
				AnonFilter_a3_10693();
				AnonFilter_a3_10694();
				AnonFilter_a3_10695();
				AnonFilter_a3_10696();
				AnonFilter_a3_10697();
				AnonFilter_a3_10698();
				AnonFilter_a3_10699();
				AnonFilter_a3_10700();
				AnonFilter_a3_10701();
				AnonFilter_a3_10702();
				AnonFilter_a3_10703();
				AnonFilter_a3_10704();
				AnonFilter_a3_10705();
				AnonFilter_a3_10706();
				AnonFilter_a3_10707();
				AnonFilter_a3_10708();
				AnonFilter_a3_10709();
				AnonFilter_a3_10710();
				AnonFilter_a3_10711();
				AnonFilter_a3_10712();
				AnonFilter_a3_10713();
				AnonFilter_a3_10714();
				AnonFilter_a3_10715();
				AnonFilter_a3_10716();
				AnonFilter_a3_10717();
				AnonFilter_a3_10718();
				AnonFilter_a3_10719();
				AnonFilter_a3_10720();
				AnonFilter_a3_10721();
				AnonFilter_a3_10722();
				AnonFilter_a3_10723();
			WEIGHTED_ROUND_ROBIN_Joiner_10687();
			Pre_CollapsedDataParallel_1_10676();
			WEIGHTED_ROUND_ROBIN_Splitter_10724();
				iDCT_1D_reference_fine_10726();
				iDCT_1D_reference_fine_10727();
				iDCT_1D_reference_fine_10728();
				iDCT_1D_reference_fine_10729();
				iDCT_1D_reference_fine_10730();
				iDCT_1D_reference_fine_10731();
				iDCT_1D_reference_fine_10732();
				iDCT_1D_reference_fine_10733();
			WEIGHTED_ROUND_ROBIN_Joiner_10725();
			Post_CollapsedDataParallel_2_10677();
			WEIGHTED_ROUND_ROBIN_Splitter_10734();
				iDCT_1D_reference_fine_10736();
				iDCT_1D_reference_fine_10737();
				iDCT_1D_reference_fine_10738();
				iDCT_1D_reference_fine_10739();
				iDCT_1D_reference_fine_10740();
				iDCT_1D_reference_fine_10741();
				iDCT_1D_reference_fine_10742();
				iDCT_1D_reference_fine_10743();
			WEIGHTED_ROUND_ROBIN_Joiner_10735();
			WEIGHTED_ROUND_ROBIN_Splitter_10744();
				AnonFilter_a4_10746();
				AnonFilter_a4_10747();
				AnonFilter_a4_10748();
				AnonFilter_a4_10749();
				AnonFilter_a4_10750();
				AnonFilter_a4_10751();
				AnonFilter_a4_10752();
				AnonFilter_a4_10753();
				AnonFilter_a4_10754();
				AnonFilter_a4_10755();
				AnonFilter_a4_10756();
				AnonFilter_a4_10757();
				AnonFilter_a4_10758();
				AnonFilter_a4_10759();
				AnonFilter_a4_10760();
				AnonFilter_a4_10761();
				AnonFilter_a4_10762();
				AnonFilter_a4_10763();
				AnonFilter_a4_10764();
				AnonFilter_a4_10765();
				AnonFilter_a4_10766();
				AnonFilter_a4_10767();
				AnonFilter_a4_10768();
				AnonFilter_a4_10769();
				AnonFilter_a4_10770();
				AnonFilter_a4_10771();
				AnonFilter_a4_10772();
				AnonFilter_a4_10773();
				AnonFilter_a4_10774();
				AnonFilter_a4_10775();
				AnonFilter_a4_10776();
				AnonFilter_a4_10777();
				AnonFilter_a4_10778();
				AnonFilter_a4_10779();
				AnonFilter_a4_10780();
				AnonFilter_a4_10781();
			WEIGHTED_ROUND_ROBIN_Joiner_10745();
			WEIGHTED_ROUND_ROBIN_Splitter_10782();
				iDCT8x8_1D_row_fast_10784();
				iDCT8x8_1D_row_fast_10785();
				iDCT8x8_1D_row_fast_10786();
				iDCT8x8_1D_row_fast_10787();
				iDCT8x8_1D_row_fast_10788();
				iDCT8x8_1D_row_fast_10789();
				iDCT8x8_1D_row_fast_10790();
				iDCT8x8_1D_row_fast_10791();
			WEIGHTED_ROUND_ROBIN_Joiner_10783();
			iDCT8x8_1D_col_fast_10630();
		WEIGHTED_ROUND_ROBIN_Joiner_10679();
		AnonFilter_a2_10631();
	ENDFOR
	return EXIT_SUCCESS;
}
