#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_15966_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_15990_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_15991_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_15963();
void DUPLICATE_Splitter_16038();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_15966();
void WEIGHTED_ROUND_ROBIN_Splitter_16046();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_16048();
void AnonFilter_a3_16049();
void AnonFilter_a3_16050();
void AnonFilter_a3_16051();
void AnonFilter_a3_16052();
void AnonFilter_a3_16053();
void AnonFilter_a3_16054();
void AnonFilter_a3_16055();
void AnonFilter_a3_16056();
void AnonFilter_a3_16057();
void AnonFilter_a3_16058();
void AnonFilter_a3_16059();
void AnonFilter_a3_16060();
void AnonFilter_a3_16061();
void AnonFilter_a3_16062();
void AnonFilter_a3_16063();
void WEIGHTED_ROUND_ROBIN_Joiner_16047();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_16036();
void WEIGHTED_ROUND_ROBIN_Splitter_16064();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_16066();
void iDCT_1D_reference_fine_16067();
void iDCT_1D_reference_fine_16068();
void iDCT_1D_reference_fine_16069();
void iDCT_1D_reference_fine_16070();
void iDCT_1D_reference_fine_16071();
void iDCT_1D_reference_fine_16072();
void iDCT_1D_reference_fine_16073();
void WEIGHTED_ROUND_ROBIN_Joiner_16065();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_16037();
void WEIGHTED_ROUND_ROBIN_Splitter_16074();
void iDCT_1D_reference_fine_16076();
void iDCT_1D_reference_fine_16077();
void iDCT_1D_reference_fine_16078();
void iDCT_1D_reference_fine_16079();
void iDCT_1D_reference_fine_16080();
void iDCT_1D_reference_fine_16081();
void iDCT_1D_reference_fine_16082();
void iDCT_1D_reference_fine_16083();
void WEIGHTED_ROUND_ROBIN_Joiner_16075();
void WEIGHTED_ROUND_ROBIN_Splitter_16084();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_16086();
void AnonFilter_a4_16087();
void AnonFilter_a4_16088();
void AnonFilter_a4_16089();
void AnonFilter_a4_16090();
void AnonFilter_a4_16091();
void AnonFilter_a4_16092();
void AnonFilter_a4_16093();
void AnonFilter_a4_16094();
void AnonFilter_a4_16095();
void AnonFilter_a4_16096();
void AnonFilter_a4_16097();
void AnonFilter_a4_16098();
void AnonFilter_a4_16099();
void AnonFilter_a4_16100();
void AnonFilter_a4_16101();
void WEIGHTED_ROUND_ROBIN_Joiner_16085();
void WEIGHTED_ROUND_ROBIN_Splitter_16102();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_16104();
void iDCT8x8_1D_row_fast_16105();
void iDCT8x8_1D_row_fast_16106();
void iDCT8x8_1D_row_fast_16107();
void iDCT8x8_1D_row_fast_16108();
void iDCT8x8_1D_row_fast_16109();
void iDCT8x8_1D_row_fast_16110();
void iDCT8x8_1D_row_fast_16111();
void WEIGHTED_ROUND_ROBIN_Joiner_16103();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_15990();
void WEIGHTED_ROUND_ROBIN_Joiner_16039();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_15991();

#ifdef __cplusplus
}
#endif
#endif
