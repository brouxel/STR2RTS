#include "PEG30-iDCTcompare_nocache.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12463Pre_CollapsedDataParallel_1_12452;
buffer_float_t Post_CollapsedDataParallel_2_12453WEIGHTED_ROUND_ROBIN_Splitter_12504;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_12455AnonFilter_a2_12407;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[30];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[30];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12505WEIGHTED_ROUND_ROBIN_Splitter_12514;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[30];
buffer_float_t Pre_CollapsedDataParallel_1_12452WEIGHTED_ROUND_ROBIN_Splitter_12494;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[30];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406;
buffer_int_t SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12495Post_CollapsedDataParallel_2_12453;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[8];
buffer_int_t SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[8];
buffer_int_t AnonFilter_a0_12379DUPLICATE_Splitter_12454;


iDCT_2D_reference_coarse_12382_t iDCT_2D_reference_coarse_12382_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12496_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12497_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12498_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12499_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12500_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12501_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12502_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12503_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12506_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12507_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12508_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12509_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12510_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12511_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12512_s;
iDCT_2D_reference_coarse_12382_t iDCT_1D_reference_fine_12513_s;
iDCT8x8_1D_col_fast_12406_t iDCT8x8_1D_col_fast_12406_s;
AnonFilter_a2_12407_t AnonFilter_a2_12407_s;

void AnonFilter_a0_12379(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_12379DUPLICATE_Splitter_12454, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_12382(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_12382_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_12382_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_12464(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12465(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12466(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12467(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12468(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12469(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12470(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12471(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12472(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12473(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12474(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12475(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12476(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12477(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12478(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12479(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12480(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12481(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12482(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12483(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12484(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12485(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12486(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12487(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12488(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12489(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12490(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12491(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12492(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12493(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[29])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12462() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12463() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12463Pre_CollapsedDataParallel_1_12452, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_12452(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_12452WEIGHTED_ROUND_ROBIN_Splitter_12494, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_12463Pre_CollapsedDataParallel_1_12452, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12463Pre_CollapsedDataParallel_1_12452) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12496(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12496_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12497(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12497_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12498(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12498_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12499(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12499_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12500(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12500_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12501(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12501_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12502(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12502_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12503(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12503_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12494() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_12452WEIGHTED_ROUND_ROBIN_Splitter_12494));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12495() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12495Post_CollapsedDataParallel_2_12453, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_12453(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_12453WEIGHTED_ROUND_ROBIN_Splitter_12504, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_12495Post_CollapsedDataParallel_2_12453, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12495Post_CollapsedDataParallel_2_12453) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12506(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12506_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12507(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12507_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12508(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12508_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12509(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12509_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12510(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12510_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12511(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12511_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12512(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12512_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12513(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12513_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12504() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_12453WEIGHTED_ROUND_ROBIN_Splitter_12504));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12505() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12505WEIGHTED_ROUND_ROBIN_Splitter_12514, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_12516(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12517(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12518(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12519(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12520(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12521(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12522(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12523(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12524(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12525(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12526(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12527(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12528(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12529(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12530(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12531(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12532(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12533(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12534(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12535(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12536(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12537(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12538(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12539(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12540(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12541(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12542(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12543(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12544(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12545(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12514() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12505WEIGHTED_ROUND_ROBIN_Splitter_12514));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12515() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_12548(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0], 6) ; 
		x3 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0], 2) ; 
		x4 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0], 1) ; 
		x5 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0], 7) ; 
		x6 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0], 5) ; 
		x7 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12549(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1], 6) ; 
		x3 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1], 2) ; 
		x4 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1], 1) ; 
		x5 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1], 7) ; 
		x6 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1], 5) ; 
		x7 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12550(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2], 6) ; 
		x3 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2], 2) ; 
		x4 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2], 1) ; 
		x5 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2], 7) ; 
		x6 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2], 5) ; 
		x7 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12551(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3], 6) ; 
		x3 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3], 2) ; 
		x4 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3], 1) ; 
		x5 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3], 7) ; 
		x6 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3], 5) ; 
		x7 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12552(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4], 6) ; 
		x3 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4], 2) ; 
		x4 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4], 1) ; 
		x5 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4], 7) ; 
		x6 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4], 5) ; 
		x7 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12553(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5], 6) ; 
		x3 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5], 2) ; 
		x4 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5], 1) ; 
		x5 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5], 7) ; 
		x6 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5], 5) ; 
		x7 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12554(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6], 6) ; 
		x3 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6], 2) ; 
		x4 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6], 1) ; 
		x5 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6], 7) ; 
		x6 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6], 5) ; 
		x7 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12555(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7], 6) ; 
		x3 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7], 2) ; 
		x4 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7], 1) ; 
		x5 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7], 7) ; 
		x6 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7], 5) ; 
		x7 = peek_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12546() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12547() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, pop_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_12406(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_12406_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_12406_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_12406_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_12406_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_12406_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_12406_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_12406_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_12406_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_12406_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_join[2], iDCT8x8_1D_col_fast_12406_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_12454() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_12379DUPLICATE_Splitter_12454);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12455() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_12455AnonFilter_a2_12407, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_12407(){
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12455AnonFilter_a2_12407) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12455AnonFilter_a2_12407) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12455AnonFilter_a2_12407) ; 
		AnonFilter_a2_12407_s.count = (AnonFilter_a2_12407_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_12407_s.errors = (AnonFilter_a2_12407_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_12407_s.errors / AnonFilter_a2_12407_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_12407_s.errors = (AnonFilter_a2_12407_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_12407_s.errors / AnonFilter_a2_12407_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12463Pre_CollapsedDataParallel_1_12452);
	init_buffer_float(&Post_CollapsedDataParallel_2_12453WEIGHTED_ROUND_ROBIN_Splitter_12504);
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_12455AnonFilter_a2_12407);
	FOR(int, __iter_init_1_, 0, <, 30, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 30, __iter_init_2_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12505WEIGHTED_ROUND_ROBIN_Splitter_12514);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12558_12565_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 30, __iter_init_5_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_12560_12567_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_12452WEIGHTED_ROUND_ROBIN_Splitter_12494);
	FOR(int, __iter_init_6_, 0, <, 30, __iter_init_6_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_12557_12564_join[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_12547iDCT8x8_1D_col_fast_12406);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12380_12456_12556_12563_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12495Post_CollapsedDataParallel_2_12453);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12559_12566_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin84_iDCT8x8_1D_row_fast_Fiss_12561_12568_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_12379DUPLICATE_Splitter_12454);
// --- init: iDCT_2D_reference_coarse_12382
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_12382_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12496
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12496_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12497
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12497_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12498
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12498_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12499
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12499_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12500
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12500_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12501
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12501_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12502
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12502_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12503
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12503_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12506
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12506_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12507
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12507_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12508
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12508_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12509
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12509_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12510
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12510_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12511
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12511_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12512
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12512_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12513
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12513_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_12407
	 {
	AnonFilter_a2_12407_s.count = 0.0 ; 
	AnonFilter_a2_12407_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_12379();
		DUPLICATE_Splitter_12454();
			iDCT_2D_reference_coarse_12382();
			WEIGHTED_ROUND_ROBIN_Splitter_12462();
				AnonFilter_a3_12464();
				AnonFilter_a3_12465();
				AnonFilter_a3_12466();
				AnonFilter_a3_12467();
				AnonFilter_a3_12468();
				AnonFilter_a3_12469();
				AnonFilter_a3_12470();
				AnonFilter_a3_12471();
				AnonFilter_a3_12472();
				AnonFilter_a3_12473();
				AnonFilter_a3_12474();
				AnonFilter_a3_12475();
				AnonFilter_a3_12476();
				AnonFilter_a3_12477();
				AnonFilter_a3_12478();
				AnonFilter_a3_12479();
				AnonFilter_a3_12480();
				AnonFilter_a3_12481();
				AnonFilter_a3_12482();
				AnonFilter_a3_12483();
				AnonFilter_a3_12484();
				AnonFilter_a3_12485();
				AnonFilter_a3_12486();
				AnonFilter_a3_12487();
				AnonFilter_a3_12488();
				AnonFilter_a3_12489();
				AnonFilter_a3_12490();
				AnonFilter_a3_12491();
				AnonFilter_a3_12492();
				AnonFilter_a3_12493();
			WEIGHTED_ROUND_ROBIN_Joiner_12463();
			Pre_CollapsedDataParallel_1_12452();
			WEIGHTED_ROUND_ROBIN_Splitter_12494();
				iDCT_1D_reference_fine_12496();
				iDCT_1D_reference_fine_12497();
				iDCT_1D_reference_fine_12498();
				iDCT_1D_reference_fine_12499();
				iDCT_1D_reference_fine_12500();
				iDCT_1D_reference_fine_12501();
				iDCT_1D_reference_fine_12502();
				iDCT_1D_reference_fine_12503();
			WEIGHTED_ROUND_ROBIN_Joiner_12495();
			Post_CollapsedDataParallel_2_12453();
			WEIGHTED_ROUND_ROBIN_Splitter_12504();
				iDCT_1D_reference_fine_12506();
				iDCT_1D_reference_fine_12507();
				iDCT_1D_reference_fine_12508();
				iDCT_1D_reference_fine_12509();
				iDCT_1D_reference_fine_12510();
				iDCT_1D_reference_fine_12511();
				iDCT_1D_reference_fine_12512();
				iDCT_1D_reference_fine_12513();
			WEIGHTED_ROUND_ROBIN_Joiner_12505();
			WEIGHTED_ROUND_ROBIN_Splitter_12514();
				AnonFilter_a4_12516();
				AnonFilter_a4_12517();
				AnonFilter_a4_12518();
				AnonFilter_a4_12519();
				AnonFilter_a4_12520();
				AnonFilter_a4_12521();
				AnonFilter_a4_12522();
				AnonFilter_a4_12523();
				AnonFilter_a4_12524();
				AnonFilter_a4_12525();
				AnonFilter_a4_12526();
				AnonFilter_a4_12527();
				AnonFilter_a4_12528();
				AnonFilter_a4_12529();
				AnonFilter_a4_12530();
				AnonFilter_a4_12531();
				AnonFilter_a4_12532();
				AnonFilter_a4_12533();
				AnonFilter_a4_12534();
				AnonFilter_a4_12535();
				AnonFilter_a4_12536();
				AnonFilter_a4_12537();
				AnonFilter_a4_12538();
				AnonFilter_a4_12539();
				AnonFilter_a4_12540();
				AnonFilter_a4_12541();
				AnonFilter_a4_12542();
				AnonFilter_a4_12543();
				AnonFilter_a4_12544();
				AnonFilter_a4_12545();
			WEIGHTED_ROUND_ROBIN_Joiner_12515();
			WEIGHTED_ROUND_ROBIN_Splitter_12546();
				iDCT8x8_1D_row_fast_12548();
				iDCT8x8_1D_row_fast_12549();
				iDCT8x8_1D_row_fast_12550();
				iDCT8x8_1D_row_fast_12551();
				iDCT8x8_1D_row_fast_12552();
				iDCT8x8_1D_row_fast_12553();
				iDCT8x8_1D_row_fast_12554();
				iDCT8x8_1D_row_fast_12555();
			WEIGHTED_ROUND_ROBIN_Joiner_12547();
			iDCT8x8_1D_col_fast_12406();
		WEIGHTED_ROUND_ROBIN_Joiner_12455();
		AnonFilter_a2_12407();
	ENDFOR
	return EXIT_SUCCESS;
}
