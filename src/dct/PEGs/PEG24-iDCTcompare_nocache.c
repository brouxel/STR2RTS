#include "PEG24-iDCTcompare_nocache.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_split[3];
buffer_int_t SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_14084WEIGHTED_ROUND_ROBIN_Splitter_14120;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[24];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[24];
buffer_int_t SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_14087AnonFilter_a2_14039;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14095Pre_CollapsedDataParallel_1_14084;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[24];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[24];
buffer_float_t Post_CollapsedDataParallel_2_14085WEIGHTED_ROUND_ROBIN_Splitter_14130;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[8];
buffer_int_t AnonFilter_a0_14011DUPLICATE_Splitter_14086;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14121Post_CollapsedDataParallel_2_14085;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14131WEIGHTED_ROUND_ROBIN_Splitter_14140;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[8];


iDCT_2D_reference_coarse_14014_t iDCT_2D_reference_coarse_14014_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14122_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14123_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14124_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14125_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14126_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14127_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14128_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14129_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14132_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14133_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14134_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14135_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14136_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14137_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14138_s;
iDCT_2D_reference_coarse_14014_t iDCT_1D_reference_fine_14139_s;
iDCT8x8_1D_col_fast_14038_t iDCT8x8_1D_col_fast_14038_s;
AnonFilter_a2_14039_t AnonFilter_a2_14039_s;

void AnonFilter_a0_14011(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_14011DUPLICATE_Splitter_14086, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_14014(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_14014_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_14014_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_14096(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14097(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14098(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14099(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14100(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14101(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14102(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14103(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14104(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14105(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14106(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14107(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14108(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14109(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14110(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14111(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14112(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14113(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14114(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14115(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14116(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14117(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14118(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14119(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[23])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14094() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 24, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14095() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 24, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14095Pre_CollapsedDataParallel_1_14084, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_14084(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_14084WEIGHTED_ROUND_ROBIN_Splitter_14120, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_14095Pre_CollapsedDataParallel_1_14084, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14095Pre_CollapsedDataParallel_1_14084) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14122(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14122_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14123(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14123_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14124(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14124_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14125(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14125_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14126(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14126_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14127(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14127_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14128(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14128_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14129(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14129_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14120() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_14084WEIGHTED_ROUND_ROBIN_Splitter_14120));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14121() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14121Post_CollapsedDataParallel_2_14085, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_14085(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_14085WEIGHTED_ROUND_ROBIN_Splitter_14130, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_14121Post_CollapsedDataParallel_2_14085, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14121Post_CollapsedDataParallel_2_14085) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14132(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14132_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14133(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14133_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14134(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14134_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14135(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14135_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14136(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14136_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14137(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14137_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14138(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14138_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14139(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14139_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14130() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_14085WEIGHTED_ROUND_ROBIN_Splitter_14130));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14131() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14131WEIGHTED_ROUND_ROBIN_Splitter_14140, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_14142(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14143(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14144(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14145(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14146(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14147(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14148(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14149(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14150(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14151(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14152(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14153(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14154(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14155(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14156(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14157(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14158(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14159(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14160(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14161(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14162(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14163(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14164(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14165(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14140() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 24, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14131WEIGHTED_ROUND_ROBIN_Splitter_14140));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14141() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 24, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_14168(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0], 6) ; 
		x3 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0], 2) ; 
		x4 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0], 1) ; 
		x5 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0], 7) ; 
		x6 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0], 5) ; 
		x7 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14169(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1], 6) ; 
		x3 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1], 2) ; 
		x4 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1], 1) ; 
		x5 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1], 7) ; 
		x6 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1], 5) ; 
		x7 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14170(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2], 6) ; 
		x3 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2], 2) ; 
		x4 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2], 1) ; 
		x5 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2], 7) ; 
		x6 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2], 5) ; 
		x7 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14171(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3], 6) ; 
		x3 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3], 2) ; 
		x4 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3], 1) ; 
		x5 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3], 7) ; 
		x6 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3], 5) ; 
		x7 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14172(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4], 6) ; 
		x3 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4], 2) ; 
		x4 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4], 1) ; 
		x5 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4], 7) ; 
		x6 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4], 5) ; 
		x7 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14173(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5], 6) ; 
		x3 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5], 2) ; 
		x4 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5], 1) ; 
		x5 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5], 7) ; 
		x6 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5], 5) ; 
		x7 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14174(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6], 6) ; 
		x3 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6], 2) ; 
		x4 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6], 1) ; 
		x5 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6], 7) ; 
		x6 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6], 5) ; 
		x7 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14175(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7], 6) ; 
		x3 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7], 2) ; 
		x4 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7], 1) ; 
		x5 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7], 7) ; 
		x6 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7], 5) ; 
		x7 = peek_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14166() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14167() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, pop_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_14038(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_14038_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_14038_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_14038_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_14038_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_14038_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_14038_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_14038_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_14038_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_14038_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_join[2], iDCT8x8_1D_col_fast_14038_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_14086() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_14011DUPLICATE_Splitter_14086);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14087() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_14087AnonFilter_a2_14039, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_14039(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14087AnonFilter_a2_14039) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14087AnonFilter_a2_14039) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14087AnonFilter_a2_14039) ; 
		AnonFilter_a2_14039_s.count = (AnonFilter_a2_14039_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_14039_s.errors = (AnonFilter_a2_14039_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_14039_s.errors / AnonFilter_a2_14039_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_14039_s.errors = (AnonFilter_a2_14039_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_14039_s.errors / AnonFilter_a2_14039_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_14084WEIGHTED_ROUND_ROBIN_Splitter_14120);
	FOR(int, __iter_init_2_, 0, <, 24, __iter_init_2_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 24, __iter_init_3_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin72_iDCT8x8_1D_row_fast_Fiss_14181_14188_split[__iter_init_4_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_14087AnonFilter_a2_14039);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14095Pre_CollapsedDataParallel_1_14084);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 24, __iter_init_6_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_14180_14187_join[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_14167iDCT8x8_1D_col_fast_14038);
	FOR(int, __iter_init_7_, 0, <, 24, __iter_init_7_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_14177_14184_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_14085WEIGHTED_ROUND_ROBIN_Splitter_14130);
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14012_14088_14176_14183_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_join[__iter_init_9_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_14011DUPLICATE_Splitter_14086);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14121Post_CollapsedDataParallel_2_14085);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14178_14185_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14131WEIGHTED_ROUND_ROBIN_Splitter_14140);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14179_14186_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_14014
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_14014_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14122
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14122_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14123
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14123_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14124
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14124_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14125
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14125_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14126
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14126_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14127
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14127_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14128
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14128_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14129
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14129_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14132
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14132_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14133
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14133_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14134
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14134_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14135
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14135_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14136
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14136_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14137
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14137_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14138
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14138_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14139
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14139_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_14039
	 {
	AnonFilter_a2_14039_s.count = 0.0 ; 
	AnonFilter_a2_14039_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_14011();
		DUPLICATE_Splitter_14086();
			iDCT_2D_reference_coarse_14014();
			WEIGHTED_ROUND_ROBIN_Splitter_14094();
				AnonFilter_a3_14096();
				AnonFilter_a3_14097();
				AnonFilter_a3_14098();
				AnonFilter_a3_14099();
				AnonFilter_a3_14100();
				AnonFilter_a3_14101();
				AnonFilter_a3_14102();
				AnonFilter_a3_14103();
				AnonFilter_a3_14104();
				AnonFilter_a3_14105();
				AnonFilter_a3_14106();
				AnonFilter_a3_14107();
				AnonFilter_a3_14108();
				AnonFilter_a3_14109();
				AnonFilter_a3_14110();
				AnonFilter_a3_14111();
				AnonFilter_a3_14112();
				AnonFilter_a3_14113();
				AnonFilter_a3_14114();
				AnonFilter_a3_14115();
				AnonFilter_a3_14116();
				AnonFilter_a3_14117();
				AnonFilter_a3_14118();
				AnonFilter_a3_14119();
			WEIGHTED_ROUND_ROBIN_Joiner_14095();
			Pre_CollapsedDataParallel_1_14084();
			WEIGHTED_ROUND_ROBIN_Splitter_14120();
				iDCT_1D_reference_fine_14122();
				iDCT_1D_reference_fine_14123();
				iDCT_1D_reference_fine_14124();
				iDCT_1D_reference_fine_14125();
				iDCT_1D_reference_fine_14126();
				iDCT_1D_reference_fine_14127();
				iDCT_1D_reference_fine_14128();
				iDCT_1D_reference_fine_14129();
			WEIGHTED_ROUND_ROBIN_Joiner_14121();
			Post_CollapsedDataParallel_2_14085();
			WEIGHTED_ROUND_ROBIN_Splitter_14130();
				iDCT_1D_reference_fine_14132();
				iDCT_1D_reference_fine_14133();
				iDCT_1D_reference_fine_14134();
				iDCT_1D_reference_fine_14135();
				iDCT_1D_reference_fine_14136();
				iDCT_1D_reference_fine_14137();
				iDCT_1D_reference_fine_14138();
				iDCT_1D_reference_fine_14139();
			WEIGHTED_ROUND_ROBIN_Joiner_14131();
			WEIGHTED_ROUND_ROBIN_Splitter_14140();
				AnonFilter_a4_14142();
				AnonFilter_a4_14143();
				AnonFilter_a4_14144();
				AnonFilter_a4_14145();
				AnonFilter_a4_14146();
				AnonFilter_a4_14147();
				AnonFilter_a4_14148();
				AnonFilter_a4_14149();
				AnonFilter_a4_14150();
				AnonFilter_a4_14151();
				AnonFilter_a4_14152();
				AnonFilter_a4_14153();
				AnonFilter_a4_14154();
				AnonFilter_a4_14155();
				AnonFilter_a4_14156();
				AnonFilter_a4_14157();
				AnonFilter_a4_14158();
				AnonFilter_a4_14159();
				AnonFilter_a4_14160();
				AnonFilter_a4_14161();
				AnonFilter_a4_14162();
				AnonFilter_a4_14163();
				AnonFilter_a4_14164();
				AnonFilter_a4_14165();
			WEIGHTED_ROUND_ROBIN_Joiner_14141();
			WEIGHTED_ROUND_ROBIN_Splitter_14166();
				iDCT8x8_1D_row_fast_14168();
				iDCT8x8_1D_row_fast_14169();
				iDCT8x8_1D_row_fast_14170();
				iDCT8x8_1D_row_fast_14171();
				iDCT8x8_1D_row_fast_14172();
				iDCT8x8_1D_row_fast_14173();
				iDCT8x8_1D_row_fast_14174();
				iDCT8x8_1D_row_fast_14175();
			WEIGHTED_ROUND_ROBIN_Joiner_14167();
			iDCT8x8_1D_col_fast_14038();
		WEIGHTED_ROUND_ROBIN_Joiner_14087();
		AnonFilter_a2_14039();
	ENDFOR
	return EXIT_SUCCESS;
}
