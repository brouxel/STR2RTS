#include "PEG52-iDCTcompare.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5311WEIGHTED_ROUND_ROBIN_Splitter_5320;
buffer_int_t AnonFilter_a0_5163DUPLICATE_Splitter_5238;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5247Pre_CollapsedDataParallel_1_5236;
buffer_float_t Post_CollapsedDataParallel_2_5237WEIGHTED_ROUND_ROBIN_Splitter_5310;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_5375iDCT8x8_1D_col_fast_5190;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[52];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[52];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_5239AnonFilter_a2_5191;
buffer_float_t Pre_CollapsedDataParallel_1_5236WEIGHTED_ROUND_ROBIN_Splitter_5300;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5301Post_CollapsedDataParallel_2_5237;
buffer_int_t SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[52];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[52];
buffer_int_t SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_join[3];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_split[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[8];


iDCT_2D_reference_coarse_5166_t iDCT_2D_reference_coarse_5166_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5302_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5303_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5304_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5305_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5306_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5307_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5308_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5309_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5312_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5313_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5314_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5315_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5316_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5317_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5318_s;
iDCT_2D_reference_coarse_5166_t iDCT_1D_reference_fine_5319_s;
iDCT8x8_1D_col_fast_5190_t iDCT8x8_1D_col_fast_5190_s;
AnonFilter_a2_5191_t AnonFilter_a2_5191_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_5163() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_5163DUPLICATE_Splitter_5238));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_5166_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_5166_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_5166() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_5248() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[0]));
	ENDFOR
}

void AnonFilter_a3_5249() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[1]));
	ENDFOR
}

void AnonFilter_a3_5250() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[2]));
	ENDFOR
}

void AnonFilter_a3_5251() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[3]));
	ENDFOR
}

void AnonFilter_a3_5252() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[4]));
	ENDFOR
}

void AnonFilter_a3_5253() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[5]));
	ENDFOR
}

void AnonFilter_a3_5254() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[6]));
	ENDFOR
}

void AnonFilter_a3_5255() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[7]));
	ENDFOR
}

void AnonFilter_a3_5256() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[8]));
	ENDFOR
}

void AnonFilter_a3_5257() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[9]));
	ENDFOR
}

void AnonFilter_a3_5258() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[10]));
	ENDFOR
}

void AnonFilter_a3_5259() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[11]));
	ENDFOR
}

void AnonFilter_a3_5260() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[12]));
	ENDFOR
}

void AnonFilter_a3_5261() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[13]));
	ENDFOR
}

void AnonFilter_a3_5262() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[14]));
	ENDFOR
}

void AnonFilter_a3_5263() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[15]));
	ENDFOR
}

void AnonFilter_a3_5264() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[16]));
	ENDFOR
}

void AnonFilter_a3_5265() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[17]));
	ENDFOR
}

void AnonFilter_a3_5266() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[18]));
	ENDFOR
}

void AnonFilter_a3_5267() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[19]));
	ENDFOR
}

void AnonFilter_a3_5268() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[20]));
	ENDFOR
}

void AnonFilter_a3_5269() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[21]));
	ENDFOR
}

void AnonFilter_a3_5270() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[22]));
	ENDFOR
}

void AnonFilter_a3_5271() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[23]));
	ENDFOR
}

void AnonFilter_a3_5272() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[24]));
	ENDFOR
}

void AnonFilter_a3_5273() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[25]));
	ENDFOR
}

void AnonFilter_a3_5274() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[26]));
	ENDFOR
}

void AnonFilter_a3_5275() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[27]));
	ENDFOR
}

void AnonFilter_a3_5276() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[28]));
	ENDFOR
}

void AnonFilter_a3_5277() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[29]));
	ENDFOR
}

void AnonFilter_a3_5278() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[30]));
	ENDFOR
}

void AnonFilter_a3_5279() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[31]));
	ENDFOR
}

void AnonFilter_a3_5280() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[32]));
	ENDFOR
}

void AnonFilter_a3_5281() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[33]));
	ENDFOR
}

void AnonFilter_a3_5282() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[34]));
	ENDFOR
}

void AnonFilter_a3_5283() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[35]));
	ENDFOR
}

void AnonFilter_a3_5284() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[36]));
	ENDFOR
}

void AnonFilter_a3_5285() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[37]));
	ENDFOR
}

void AnonFilter_a3_5286() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[38]));
	ENDFOR
}

void AnonFilter_a3_5287() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[39]));
	ENDFOR
}

void AnonFilter_a3_5288() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[40]));
	ENDFOR
}

void AnonFilter_a3_5289() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[41]));
	ENDFOR
}

void AnonFilter_a3_5290() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[42]));
	ENDFOR
}

void AnonFilter_a3_5291() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[43]));
	ENDFOR
}

void AnonFilter_a3_5292() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[44]));
	ENDFOR
}

void AnonFilter_a3_5293() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[45]));
	ENDFOR
}

void AnonFilter_a3_5294() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[46]));
	ENDFOR
}

void AnonFilter_a3_5295() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[47]));
	ENDFOR
}

void AnonFilter_a3_5296() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[48]));
	ENDFOR
}

void AnonFilter_a3_5297() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[49]));
	ENDFOR
}

void AnonFilter_a3_5298() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[50]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[50]));
	ENDFOR
}

void AnonFilter_a3_5299() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[51]), &(SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[51]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5246() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 52, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5247() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 52, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5247Pre_CollapsedDataParallel_1_5236, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_5236() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_5247Pre_CollapsedDataParallel_1_5236), &(Pre_CollapsedDataParallel_1_5236WEIGHTED_ROUND_ROBIN_Splitter_5300));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5302_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_5302() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_5303() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_5304() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_5305() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_5306() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_5307() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_5308() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_5309() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5300() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5236WEIGHTED_ROUND_ROBIN_Splitter_5300));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5301() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5301Post_CollapsedDataParallel_2_5237, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_5237() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5301Post_CollapsedDataParallel_2_5237), &(Post_CollapsedDataParallel_2_5237WEIGHTED_ROUND_ROBIN_Splitter_5310));
	ENDFOR
}

void iDCT_1D_reference_fine_5312() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_5313() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_5314() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_5315() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_5316() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_5317() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_5318() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_5319() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5310() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_5237WEIGHTED_ROUND_ROBIN_Splitter_5310));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5311() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5311WEIGHTED_ROUND_ROBIN_Splitter_5320, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_5322() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[0]));
	ENDFOR
}

void AnonFilter_a4_5323() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[1]));
	ENDFOR
}

void AnonFilter_a4_5324() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[2]));
	ENDFOR
}

void AnonFilter_a4_5325() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[3]));
	ENDFOR
}

void AnonFilter_a4_5326() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[4]));
	ENDFOR
}

void AnonFilter_a4_5327() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[5]));
	ENDFOR
}

void AnonFilter_a4_5328() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[6]));
	ENDFOR
}

void AnonFilter_a4_5329() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[7]));
	ENDFOR
}

void AnonFilter_a4_5330() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[8]));
	ENDFOR
}

void AnonFilter_a4_5331() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[9]));
	ENDFOR
}

void AnonFilter_a4_5332() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[10]));
	ENDFOR
}

void AnonFilter_a4_5333() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[11]));
	ENDFOR
}

void AnonFilter_a4_5334() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[12]));
	ENDFOR
}

void AnonFilter_a4_5335() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[13]));
	ENDFOR
}

void AnonFilter_a4_5336() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[14]));
	ENDFOR
}

void AnonFilter_a4_5337() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[15]));
	ENDFOR
}

void AnonFilter_a4_5338() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[16]));
	ENDFOR
}

void AnonFilter_a4_5339() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[17]));
	ENDFOR
}

void AnonFilter_a4_5340() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[18]));
	ENDFOR
}

void AnonFilter_a4_5341() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[19]));
	ENDFOR
}

void AnonFilter_a4_5342() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[20]));
	ENDFOR
}

void AnonFilter_a4_5343() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[21]));
	ENDFOR
}

void AnonFilter_a4_5344() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[22]));
	ENDFOR
}

void AnonFilter_a4_5345() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[23]));
	ENDFOR
}

void AnonFilter_a4_5346() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[24]));
	ENDFOR
}

void AnonFilter_a4_5347() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[25]));
	ENDFOR
}

void AnonFilter_a4_5348() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[26]));
	ENDFOR
}

void AnonFilter_a4_5349() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[27]));
	ENDFOR
}

void AnonFilter_a4_5350() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[28]));
	ENDFOR
}

void AnonFilter_a4_5351() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[29]));
	ENDFOR
}

void AnonFilter_a4_5352() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[30]));
	ENDFOR
}

void AnonFilter_a4_5353() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[31]));
	ENDFOR
}

void AnonFilter_a4_5354() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[32]));
	ENDFOR
}

void AnonFilter_a4_5355() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[33]));
	ENDFOR
}

void AnonFilter_a4_5356() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[34]));
	ENDFOR
}

void AnonFilter_a4_5357() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[35]));
	ENDFOR
}

void AnonFilter_a4_5358() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[36]));
	ENDFOR
}

void AnonFilter_a4_5359() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[37]));
	ENDFOR
}

void AnonFilter_a4_5360() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[38]));
	ENDFOR
}

void AnonFilter_a4_5361() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[39]));
	ENDFOR
}

void AnonFilter_a4_5362() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[40]));
	ENDFOR
}

void AnonFilter_a4_5363() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[41]));
	ENDFOR
}

void AnonFilter_a4_5364() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[42]));
	ENDFOR
}

void AnonFilter_a4_5365() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[43]));
	ENDFOR
}

void AnonFilter_a4_5366() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[44]));
	ENDFOR
}

void AnonFilter_a4_5367() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[45]));
	ENDFOR
}

void AnonFilter_a4_5368() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[46]));
	ENDFOR
}

void AnonFilter_a4_5369() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[47]));
	ENDFOR
}

void AnonFilter_a4_5370() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[48]));
	ENDFOR
}

void AnonFilter_a4_5371() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[49]));
	ENDFOR
}

void AnonFilter_a4_5372() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[50]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[50]));
	ENDFOR
}

void AnonFilter_a4_5373() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[51]), &(SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[51]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5320() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 52, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5311WEIGHTED_ROUND_ROBIN_Splitter_5320));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5321() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 52, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_5376() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[0]), &(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5377() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[1]), &(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5378() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[2]), &(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5379() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[3]), &(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5380() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[4]), &(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5381() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[5]), &(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5382() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[6]), &(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5383() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[7]), &(SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5374() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_5375iDCT8x8_1D_col_fast_5190, pop_int(&SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_5190_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_5190_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_5190_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_5190_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_5190_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_5190_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_5190_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_5190_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_5190_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_5190_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_5190() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_5375iDCT8x8_1D_col_fast_5190), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_5238() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_5163DUPLICATE_Splitter_5238);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5239() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_5239AnonFilter_a2_5191, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_5191_s.count = (AnonFilter_a2_5191_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_5191_s.errors = (AnonFilter_a2_5191_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_5191_s.errors / AnonFilter_a2_5191_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_5191_s.errors = (AnonFilter_a2_5191_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_5191_s.errors / AnonFilter_a2_5191_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_5191() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_5239AnonFilter_a2_5191));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5311WEIGHTED_ROUND_ROBIN_Splitter_5320);
	init_buffer_int(&AnonFilter_a0_5163DUPLICATE_Splitter_5238);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5247Pre_CollapsedDataParallel_1_5236);
	init_buffer_float(&Post_CollapsedDataParallel_2_5237WEIGHTED_ROUND_ROBIN_Splitter_5310);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_5375iDCT8x8_1D_col_fast_5190);
	FOR(int, __iter_init_0_, 0, <, 52, __iter_init_0_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_5385_5392_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 52, __iter_init_1_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_5388_5395_split[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_5239AnonFilter_a2_5191);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5236WEIGHTED_ROUND_ROBIN_Splitter_5300);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5301Post_CollapsedDataParallel_2_5237);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 52, __iter_init_4_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_5385_5392_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 52, __iter_init_5_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_5388_5395_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_int(&SplitJoin128_iDCT8x8_1D_row_fast_Fiss_5389_5396_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5386_5393_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5164_5240_5384_5391_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5387_5394_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_5166
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_5166_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5302
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5302_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5303
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5303_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5304
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5304_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5305
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5305_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5306
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5306_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5307
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5307_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5308
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5308_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5309
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5309_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5312
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5312_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5313
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5313_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5314
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5314_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5315
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5315_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5316
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5316_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5317
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5317_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5318
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5318_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5319
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5319_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_5191
	 {
	AnonFilter_a2_5191_s.count = 0.0 ; 
	AnonFilter_a2_5191_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_5163();
		DUPLICATE_Splitter_5238();
			iDCT_2D_reference_coarse_5166();
			WEIGHTED_ROUND_ROBIN_Splitter_5246();
				AnonFilter_a3_5248();
				AnonFilter_a3_5249();
				AnonFilter_a3_5250();
				AnonFilter_a3_5251();
				AnonFilter_a3_5252();
				AnonFilter_a3_5253();
				AnonFilter_a3_5254();
				AnonFilter_a3_5255();
				AnonFilter_a3_5256();
				AnonFilter_a3_5257();
				AnonFilter_a3_5258();
				AnonFilter_a3_5259();
				AnonFilter_a3_5260();
				AnonFilter_a3_5261();
				AnonFilter_a3_5262();
				AnonFilter_a3_5263();
				AnonFilter_a3_5264();
				AnonFilter_a3_5265();
				AnonFilter_a3_5266();
				AnonFilter_a3_5267();
				AnonFilter_a3_5268();
				AnonFilter_a3_5269();
				AnonFilter_a3_5270();
				AnonFilter_a3_5271();
				AnonFilter_a3_5272();
				AnonFilter_a3_5273();
				AnonFilter_a3_5274();
				AnonFilter_a3_5275();
				AnonFilter_a3_5276();
				AnonFilter_a3_5277();
				AnonFilter_a3_5278();
				AnonFilter_a3_5279();
				AnonFilter_a3_5280();
				AnonFilter_a3_5281();
				AnonFilter_a3_5282();
				AnonFilter_a3_5283();
				AnonFilter_a3_5284();
				AnonFilter_a3_5285();
				AnonFilter_a3_5286();
				AnonFilter_a3_5287();
				AnonFilter_a3_5288();
				AnonFilter_a3_5289();
				AnonFilter_a3_5290();
				AnonFilter_a3_5291();
				AnonFilter_a3_5292();
				AnonFilter_a3_5293();
				AnonFilter_a3_5294();
				AnonFilter_a3_5295();
				AnonFilter_a3_5296();
				AnonFilter_a3_5297();
				AnonFilter_a3_5298();
				AnonFilter_a3_5299();
			WEIGHTED_ROUND_ROBIN_Joiner_5247();
			Pre_CollapsedDataParallel_1_5236();
			WEIGHTED_ROUND_ROBIN_Splitter_5300();
				iDCT_1D_reference_fine_5302();
				iDCT_1D_reference_fine_5303();
				iDCT_1D_reference_fine_5304();
				iDCT_1D_reference_fine_5305();
				iDCT_1D_reference_fine_5306();
				iDCT_1D_reference_fine_5307();
				iDCT_1D_reference_fine_5308();
				iDCT_1D_reference_fine_5309();
			WEIGHTED_ROUND_ROBIN_Joiner_5301();
			Post_CollapsedDataParallel_2_5237();
			WEIGHTED_ROUND_ROBIN_Splitter_5310();
				iDCT_1D_reference_fine_5312();
				iDCT_1D_reference_fine_5313();
				iDCT_1D_reference_fine_5314();
				iDCT_1D_reference_fine_5315();
				iDCT_1D_reference_fine_5316();
				iDCT_1D_reference_fine_5317();
				iDCT_1D_reference_fine_5318();
				iDCT_1D_reference_fine_5319();
			WEIGHTED_ROUND_ROBIN_Joiner_5311();
			WEIGHTED_ROUND_ROBIN_Splitter_5320();
				AnonFilter_a4_5322();
				AnonFilter_a4_5323();
				AnonFilter_a4_5324();
				AnonFilter_a4_5325();
				AnonFilter_a4_5326();
				AnonFilter_a4_5327();
				AnonFilter_a4_5328();
				AnonFilter_a4_5329();
				AnonFilter_a4_5330();
				AnonFilter_a4_5331();
				AnonFilter_a4_5332();
				AnonFilter_a4_5333();
				AnonFilter_a4_5334();
				AnonFilter_a4_5335();
				AnonFilter_a4_5336();
				AnonFilter_a4_5337();
				AnonFilter_a4_5338();
				AnonFilter_a4_5339();
				AnonFilter_a4_5340();
				AnonFilter_a4_5341();
				AnonFilter_a4_5342();
				AnonFilter_a4_5343();
				AnonFilter_a4_5344();
				AnonFilter_a4_5345();
				AnonFilter_a4_5346();
				AnonFilter_a4_5347();
				AnonFilter_a4_5348();
				AnonFilter_a4_5349();
				AnonFilter_a4_5350();
				AnonFilter_a4_5351();
				AnonFilter_a4_5352();
				AnonFilter_a4_5353();
				AnonFilter_a4_5354();
				AnonFilter_a4_5355();
				AnonFilter_a4_5356();
				AnonFilter_a4_5357();
				AnonFilter_a4_5358();
				AnonFilter_a4_5359();
				AnonFilter_a4_5360();
				AnonFilter_a4_5361();
				AnonFilter_a4_5362();
				AnonFilter_a4_5363();
				AnonFilter_a4_5364();
				AnonFilter_a4_5365();
				AnonFilter_a4_5366();
				AnonFilter_a4_5367();
				AnonFilter_a4_5368();
				AnonFilter_a4_5369();
				AnonFilter_a4_5370();
				AnonFilter_a4_5371();
				AnonFilter_a4_5372();
				AnonFilter_a4_5373();
			WEIGHTED_ROUND_ROBIN_Joiner_5321();
			WEIGHTED_ROUND_ROBIN_Splitter_5374();
				iDCT8x8_1D_row_fast_5376();
				iDCT8x8_1D_row_fast_5377();
				iDCT8x8_1D_row_fast_5378();
				iDCT8x8_1D_row_fast_5379();
				iDCT8x8_1D_row_fast_5380();
				iDCT8x8_1D_row_fast_5381();
				iDCT8x8_1D_row_fast_5382();
				iDCT8x8_1D_row_fast_5383();
			WEIGHTED_ROUND_ROBIN_Joiner_5375();
			iDCT8x8_1D_col_fast_5190();
		WEIGHTED_ROUND_ROBIN_Joiner_5239();
		AnonFilter_a2_5191();
	ENDFOR
	return EXIT_SUCCESS;
}
