#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=8064 on the compile command line
#else
#if BUF_SIZEMAX < 8064
#error BUF_SIZEMAX too small, it must be at least 8064
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_8686_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_8710_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_8711_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_8683();
void DUPLICATE_Splitter_8758();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_8686();
void WEIGHTED_ROUND_ROBIN_Splitter_8766();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_8768();
void AnonFilter_a3_8769();
void AnonFilter_a3_8770();
void AnonFilter_a3_8771();
void AnonFilter_a3_8772();
void AnonFilter_a3_8773();
void AnonFilter_a3_8774();
void AnonFilter_a3_8775();
void AnonFilter_a3_8776();
void AnonFilter_a3_8777();
void AnonFilter_a3_8778();
void AnonFilter_a3_8779();
void AnonFilter_a3_8780();
void AnonFilter_a3_8781();
void AnonFilter_a3_8782();
void AnonFilter_a3_8783();
void AnonFilter_a3_8784();
void AnonFilter_a3_8785();
void AnonFilter_a3_8786();
void AnonFilter_a3_8787();
void AnonFilter_a3_8788();
void AnonFilter_a3_8789();
void AnonFilter_a3_8790();
void AnonFilter_a3_8791();
void AnonFilter_a3_8792();
void AnonFilter_a3_8793();
void AnonFilter_a3_8794();
void AnonFilter_a3_8795();
void AnonFilter_a3_8796();
void AnonFilter_a3_8797();
void AnonFilter_a3_8798();
void AnonFilter_a3_8799();
void AnonFilter_a3_8800();
void AnonFilter_a3_8801();
void AnonFilter_a3_8802();
void AnonFilter_a3_8803();
void AnonFilter_a3_8804();
void AnonFilter_a3_8805();
void AnonFilter_a3_8806();
void AnonFilter_a3_8807();
void AnonFilter_a3_8808();
void AnonFilter_a3_8809();
void WEIGHTED_ROUND_ROBIN_Joiner_8767();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_8756();
void WEIGHTED_ROUND_ROBIN_Splitter_8810();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_8812();
void iDCT_1D_reference_fine_8813();
void iDCT_1D_reference_fine_8814();
void iDCT_1D_reference_fine_8815();
void iDCT_1D_reference_fine_8816();
void iDCT_1D_reference_fine_8817();
void iDCT_1D_reference_fine_8818();
void iDCT_1D_reference_fine_8819();
void WEIGHTED_ROUND_ROBIN_Joiner_8811();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_8757();
void WEIGHTED_ROUND_ROBIN_Splitter_8820();
void iDCT_1D_reference_fine_8822();
void iDCT_1D_reference_fine_8823();
void iDCT_1D_reference_fine_8824();
void iDCT_1D_reference_fine_8825();
void iDCT_1D_reference_fine_8826();
void iDCT_1D_reference_fine_8827();
void iDCT_1D_reference_fine_8828();
void iDCT_1D_reference_fine_8829();
void WEIGHTED_ROUND_ROBIN_Joiner_8821();
void WEIGHTED_ROUND_ROBIN_Splitter_8830();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_8832();
void AnonFilter_a4_8833();
void AnonFilter_a4_8834();
void AnonFilter_a4_8835();
void AnonFilter_a4_8836();
void AnonFilter_a4_8837();
void AnonFilter_a4_8838();
void AnonFilter_a4_8839();
void AnonFilter_a4_8840();
void AnonFilter_a4_8841();
void AnonFilter_a4_8842();
void AnonFilter_a4_8843();
void AnonFilter_a4_8844();
void AnonFilter_a4_8845();
void AnonFilter_a4_8846();
void AnonFilter_a4_8847();
void AnonFilter_a4_8848();
void AnonFilter_a4_8849();
void AnonFilter_a4_8850();
void AnonFilter_a4_8851();
void AnonFilter_a4_8852();
void AnonFilter_a4_8853();
void AnonFilter_a4_8854();
void AnonFilter_a4_8855();
void AnonFilter_a4_8856();
void AnonFilter_a4_8857();
void AnonFilter_a4_8858();
void AnonFilter_a4_8859();
void AnonFilter_a4_8860();
void AnonFilter_a4_8861();
void AnonFilter_a4_8862();
void AnonFilter_a4_8863();
void AnonFilter_a4_8864();
void AnonFilter_a4_8865();
void AnonFilter_a4_8866();
void AnonFilter_a4_8867();
void AnonFilter_a4_8868();
void AnonFilter_a4_8869();
void AnonFilter_a4_8870();
void AnonFilter_a4_8871();
void AnonFilter_a4_8872();
void AnonFilter_a4_8873();
void WEIGHTED_ROUND_ROBIN_Joiner_8831();
void WEIGHTED_ROUND_ROBIN_Splitter_8874();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_8876();
void iDCT8x8_1D_row_fast_8877();
void iDCT8x8_1D_row_fast_8878();
void iDCT8x8_1D_row_fast_8879();
void iDCT8x8_1D_row_fast_8880();
void iDCT8x8_1D_row_fast_8881();
void iDCT8x8_1D_row_fast_8882();
void iDCT8x8_1D_row_fast_8883();
void WEIGHTED_ROUND_ROBIN_Joiner_8875();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_8710();
void WEIGHTED_ROUND_ROBIN_Joiner_8759();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_8711();

#ifdef __cplusplus
}
#endif
#endif
