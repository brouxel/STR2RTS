#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=13440 on the compile command line
#else
#if BUF_SIZEMAX < 13440
#error BUF_SIZEMAX too small, it must be at least 13440
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_10912_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_10936_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_10937_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_10909();
void DUPLICATE_Splitter_10984();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_10912();
void WEIGHTED_ROUND_ROBIN_Splitter_10992();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_10994();
void AnonFilter_a3_10995();
void AnonFilter_a3_10996();
void AnonFilter_a3_10997();
void AnonFilter_a3_10998();
void AnonFilter_a3_10999();
void AnonFilter_a3_11000();
void AnonFilter_a3_11001();
void AnonFilter_a3_11002();
void AnonFilter_a3_11003();
void AnonFilter_a3_11004();
void AnonFilter_a3_11005();
void AnonFilter_a3_11006();
void AnonFilter_a3_11007();
void AnonFilter_a3_11008();
void AnonFilter_a3_11009();
void AnonFilter_a3_11010();
void AnonFilter_a3_11011();
void AnonFilter_a3_11012();
void AnonFilter_a3_11013();
void AnonFilter_a3_11014();
void AnonFilter_a3_11015();
void AnonFilter_a3_11016();
void AnonFilter_a3_11017();
void AnonFilter_a3_11018();
void AnonFilter_a3_11019();
void AnonFilter_a3_11020();
void AnonFilter_a3_11021();
void AnonFilter_a3_11022();
void AnonFilter_a3_11023();
void AnonFilter_a3_11024();
void AnonFilter_a3_11025();
void AnonFilter_a3_11026();
void AnonFilter_a3_11027();
void AnonFilter_a3_11028();
void WEIGHTED_ROUND_ROBIN_Joiner_10993();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_10982();
void WEIGHTED_ROUND_ROBIN_Splitter_11029();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_11031();
void iDCT_1D_reference_fine_11032();
void iDCT_1D_reference_fine_11033();
void iDCT_1D_reference_fine_11034();
void iDCT_1D_reference_fine_11035();
void iDCT_1D_reference_fine_11036();
void iDCT_1D_reference_fine_11037();
void iDCT_1D_reference_fine_11038();
void WEIGHTED_ROUND_ROBIN_Joiner_11030();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_10983();
void WEIGHTED_ROUND_ROBIN_Splitter_11039();
void iDCT_1D_reference_fine_11041();
void iDCT_1D_reference_fine_11042();
void iDCT_1D_reference_fine_11043();
void iDCT_1D_reference_fine_11044();
void iDCT_1D_reference_fine_11045();
void iDCT_1D_reference_fine_11046();
void iDCT_1D_reference_fine_11047();
void iDCT_1D_reference_fine_11048();
void WEIGHTED_ROUND_ROBIN_Joiner_11040();
void WEIGHTED_ROUND_ROBIN_Splitter_11049();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_11051();
void AnonFilter_a4_11052();
void AnonFilter_a4_11053();
void AnonFilter_a4_11054();
void AnonFilter_a4_11055();
void AnonFilter_a4_11056();
void AnonFilter_a4_11057();
void AnonFilter_a4_11058();
void AnonFilter_a4_11059();
void AnonFilter_a4_11060();
void AnonFilter_a4_11061();
void AnonFilter_a4_11062();
void AnonFilter_a4_11063();
void AnonFilter_a4_11064();
void AnonFilter_a4_11065();
void AnonFilter_a4_11066();
void AnonFilter_a4_11067();
void AnonFilter_a4_11068();
void AnonFilter_a4_11069();
void AnonFilter_a4_11070();
void AnonFilter_a4_11071();
void AnonFilter_a4_11072();
void AnonFilter_a4_11073();
void AnonFilter_a4_11074();
void AnonFilter_a4_11075();
void AnonFilter_a4_11076();
void AnonFilter_a4_11077();
void AnonFilter_a4_11078();
void AnonFilter_a4_11079();
void AnonFilter_a4_11080();
void AnonFilter_a4_11081();
void AnonFilter_a4_11082();
void AnonFilter_a4_11083();
void AnonFilter_a4_11084();
void AnonFilter_a4_11085();
void WEIGHTED_ROUND_ROBIN_Joiner_11050();
void WEIGHTED_ROUND_ROBIN_Splitter_11086();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_11088();
void iDCT8x8_1D_row_fast_11089();
void iDCT8x8_1D_row_fast_11090();
void iDCT8x8_1D_row_fast_11091();
void iDCT8x8_1D_row_fast_11092();
void iDCT8x8_1D_row_fast_11093();
void iDCT8x8_1D_row_fast_11094();
void iDCT8x8_1D_row_fast_11095();
void WEIGHTED_ROUND_ROBIN_Joiner_11087();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_10936();
void WEIGHTED_ROUND_ROBIN_Joiner_10985();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_10937();

#ifdef __cplusplus
}
#endif
#endif
