#include "PEG53-iDCTcompare.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4863;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[8];
buffer_int_t AnonFilter_a0_4789DUPLICATE_Splitter_4864;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_4865AnonFilter_a2_4817;
buffer_float_t Pre_CollapsedDataParallel_1_4862WEIGHTED_ROUND_ROBIN_Splitter_4927;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4873Pre_CollapsedDataParallel_1_4862;
buffer_int_t SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[8];
buffer_int_t SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[8];
buffer_float_t Post_CollapsedDataParallel_2_4863WEIGHTED_ROUND_ROBIN_Splitter_4937;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4938WEIGHTED_ROUND_ROBIN_Splitter_4947;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_5003iDCT8x8_1D_col_fast_4816;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[53];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[53];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[53];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[53];


iDCT_2D_reference_coarse_4792_t iDCT_2D_reference_coarse_4792_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4929_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4930_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4931_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4932_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4933_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4934_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4935_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4936_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4939_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4940_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4941_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4942_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4943_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4944_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4945_s;
iDCT_2D_reference_coarse_4792_t iDCT_1D_reference_fine_4946_s;
iDCT8x8_1D_col_fast_4816_t iDCT8x8_1D_col_fast_4816_s;
AnonFilter_a2_4817_t AnonFilter_a2_4817_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_4789() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_4789DUPLICATE_Splitter_4864));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_4792_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_4792_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_4792() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_4874() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[0]));
	ENDFOR
}

void AnonFilter_a3_4875() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[1]));
	ENDFOR
}

void AnonFilter_a3_4876() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[2]));
	ENDFOR
}

void AnonFilter_a3_4877() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[3]));
	ENDFOR
}

void AnonFilter_a3_4878() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[4]));
	ENDFOR
}

void AnonFilter_a3_4879() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[5]));
	ENDFOR
}

void AnonFilter_a3_4880() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[6]));
	ENDFOR
}

void AnonFilter_a3_4881() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[7]));
	ENDFOR
}

void AnonFilter_a3_4882() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[8]));
	ENDFOR
}

void AnonFilter_a3_4883() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[9]));
	ENDFOR
}

void AnonFilter_a3_4884() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[10]));
	ENDFOR
}

void AnonFilter_a3_4885() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[11]));
	ENDFOR
}

void AnonFilter_a3_4886() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[12]));
	ENDFOR
}

void AnonFilter_a3_4887() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[13]));
	ENDFOR
}

void AnonFilter_a3_4888() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[14]));
	ENDFOR
}

void AnonFilter_a3_4889() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[15]));
	ENDFOR
}

void AnonFilter_a3_4890() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[16]));
	ENDFOR
}

void AnonFilter_a3_4891() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[17]));
	ENDFOR
}

void AnonFilter_a3_4892() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[18]));
	ENDFOR
}

void AnonFilter_a3_4893() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[19]));
	ENDFOR
}

void AnonFilter_a3_4894() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[20]));
	ENDFOR
}

void AnonFilter_a3_4895() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[21]));
	ENDFOR
}

void AnonFilter_a3_4896() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[22]));
	ENDFOR
}

void AnonFilter_a3_4897() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[23]));
	ENDFOR
}

void AnonFilter_a3_4898() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[24]));
	ENDFOR
}

void AnonFilter_a3_4899() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[25]));
	ENDFOR
}

void AnonFilter_a3_4900() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[26]));
	ENDFOR
}

void AnonFilter_a3_4901() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[27]));
	ENDFOR
}

void AnonFilter_a3_4902() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[28]));
	ENDFOR
}

void AnonFilter_a3_4903() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[29]));
	ENDFOR
}

void AnonFilter_a3_4904() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[30]));
	ENDFOR
}

void AnonFilter_a3_4905() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[31]));
	ENDFOR
}

void AnonFilter_a3_4906() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[32]));
	ENDFOR
}

void AnonFilter_a3_4907() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[33]));
	ENDFOR
}

void AnonFilter_a3_4908() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[34]));
	ENDFOR
}

void AnonFilter_a3_4909() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[35]));
	ENDFOR
}

void AnonFilter_a3_4910() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[36]));
	ENDFOR
}

void AnonFilter_a3_4911() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[37]));
	ENDFOR
}

void AnonFilter_a3_4912() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[38]));
	ENDFOR
}

void AnonFilter_a3_4913() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[39]));
	ENDFOR
}

void AnonFilter_a3_4914() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[40]));
	ENDFOR
}

void AnonFilter_a3_4915() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[41]));
	ENDFOR
}

void AnonFilter_a3_4916() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[42]));
	ENDFOR
}

void AnonFilter_a3_4917() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[43]));
	ENDFOR
}

void AnonFilter_a3_4918() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[44]));
	ENDFOR
}

void AnonFilter_a3_4919() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[45]));
	ENDFOR
}

void AnonFilter_a3_4920() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[46]));
	ENDFOR
}

void AnonFilter_a3_4921() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[47]));
	ENDFOR
}

void AnonFilter_a3_4922() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[48]));
	ENDFOR
}

void AnonFilter_a3_4923() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[49]));
	ENDFOR
}

void AnonFilter_a3_4924() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[50]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[50]));
	ENDFOR
}

void AnonFilter_a3_4925() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[51]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[51]));
	ENDFOR
}

void AnonFilter_a3_4926() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[52]), &(SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[52]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4872() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 53, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4873() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 53, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4873Pre_CollapsedDataParallel_1_4862, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_4862() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_4873Pre_CollapsedDataParallel_1_4862), &(Pre_CollapsedDataParallel_1_4862WEIGHTED_ROUND_ROBIN_Splitter_4927));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4929_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_4929() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_4930() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_4931() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_4932() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_4933() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_4934() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_4935() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_4936() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4927() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4862WEIGHTED_ROUND_ROBIN_Splitter_4927));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4928() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4863, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_4863() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4863), &(Post_CollapsedDataParallel_2_4863WEIGHTED_ROUND_ROBIN_Splitter_4937));
	ENDFOR
}

void iDCT_1D_reference_fine_4939() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_4940() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_4941() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_4942() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_4943() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_4944() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_4945() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_4946() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4937() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_4863WEIGHTED_ROUND_ROBIN_Splitter_4937));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4938() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938WEIGHTED_ROUND_ROBIN_Splitter_4947, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_4949() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[0]));
	ENDFOR
}

void AnonFilter_a4_4950() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[1]));
	ENDFOR
}

void AnonFilter_a4_4951() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[2]));
	ENDFOR
}

void AnonFilter_a4_4952() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[3]));
	ENDFOR
}

void AnonFilter_a4_4953() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[4]));
	ENDFOR
}

void AnonFilter_a4_4954() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[5]));
	ENDFOR
}

void AnonFilter_a4_4955() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[6]));
	ENDFOR
}

void AnonFilter_a4_4956() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[7]));
	ENDFOR
}

void AnonFilter_a4_4957() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[8]));
	ENDFOR
}

void AnonFilter_a4_4958() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[9]));
	ENDFOR
}

void AnonFilter_a4_4959() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[10]));
	ENDFOR
}

void AnonFilter_a4_4960() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[11]));
	ENDFOR
}

void AnonFilter_a4_4961() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[12]));
	ENDFOR
}

void AnonFilter_a4_4962() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[13]));
	ENDFOR
}

void AnonFilter_a4_4963() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[14]));
	ENDFOR
}

void AnonFilter_a4_4964() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[15]));
	ENDFOR
}

void AnonFilter_a4_4965() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[16]));
	ENDFOR
}

void AnonFilter_a4_4966() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[17]));
	ENDFOR
}

void AnonFilter_a4_4967() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[18]));
	ENDFOR
}

void AnonFilter_a4_4968() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[19]));
	ENDFOR
}

void AnonFilter_a4_4969() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[20]));
	ENDFOR
}

void AnonFilter_a4_4970() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[21]));
	ENDFOR
}

void AnonFilter_a4_4971() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[22]));
	ENDFOR
}

void AnonFilter_a4_4972() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[23]));
	ENDFOR
}

void AnonFilter_a4_4973() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[24]));
	ENDFOR
}

void AnonFilter_a4_4974() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[25]));
	ENDFOR
}

void AnonFilter_a4_4975() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[26]));
	ENDFOR
}

void AnonFilter_a4_4976() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[27]));
	ENDFOR
}

void AnonFilter_a4_4977() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[28]));
	ENDFOR
}

void AnonFilter_a4_4978() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[29]));
	ENDFOR
}

void AnonFilter_a4_4979() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[30]));
	ENDFOR
}

void AnonFilter_a4_4980() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[31]));
	ENDFOR
}

void AnonFilter_a4_4981() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[32]));
	ENDFOR
}

void AnonFilter_a4_4982() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[33]));
	ENDFOR
}

void AnonFilter_a4_4983() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[34]));
	ENDFOR
}

void AnonFilter_a4_4984() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[35]));
	ENDFOR
}

void AnonFilter_a4_4985() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[36]));
	ENDFOR
}

void AnonFilter_a4_4986() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[37]));
	ENDFOR
}

void AnonFilter_a4_4987() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[38]));
	ENDFOR
}

void AnonFilter_a4_4988() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[39]));
	ENDFOR
}

void AnonFilter_a4_4989() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[40]));
	ENDFOR
}

void AnonFilter_a4_4990() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[41]));
	ENDFOR
}

void AnonFilter_a4_4991() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[42]));
	ENDFOR
}

void AnonFilter_a4_4992() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[43]));
	ENDFOR
}

void AnonFilter_a4_4993() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[44]));
	ENDFOR
}

void AnonFilter_a4_4994() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[45]));
	ENDFOR
}

void AnonFilter_a4_4995() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[46]));
	ENDFOR
}

void AnonFilter_a4_4996() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[47]));
	ENDFOR
}

void AnonFilter_a4_4997() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[48]));
	ENDFOR
}

void AnonFilter_a4_4998() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[49]));
	ENDFOR
}

void AnonFilter_a4_4999() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[50]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[50]));
	ENDFOR
}

void AnonFilter_a4_5000() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[51]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[51]));
	ENDFOR
}

void AnonFilter_a4_5001() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[52]), &(SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[52]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4947() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 53, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938WEIGHTED_ROUND_ROBIN_Splitter_4947));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4948() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 53, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_5004() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[0]), &(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5005() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[1]), &(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5006() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[2]), &(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5007() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[3]), &(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5008() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[4]), &(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5009() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[5]), &(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5010() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[6]), &(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_5011() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[7]), &(SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5002() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5003() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_5003iDCT8x8_1D_col_fast_4816, pop_int(&SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_4816_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_4816_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_4816_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_4816_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_4816_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_4816_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_4816_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_4816_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_4816_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_4816_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_4816() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_5003iDCT8x8_1D_col_fast_4816), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_4864() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3392, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_4789DUPLICATE_Splitter_4864);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4865() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3392, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_4865AnonFilter_a2_4817, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_4817_s.count = (AnonFilter_a2_4817_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_4817_s.errors = (AnonFilter_a2_4817_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_4817_s.errors / AnonFilter_a2_4817_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_4817_s.errors = (AnonFilter_a2_4817_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_4817_s.errors / AnonFilter_a2_4817_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_4817() {
	FOR(uint32_t, __iter_steady_, 0, <, 3392, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_4865AnonFilter_a2_4817));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4863);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5015_5022_split[__iter_init_2_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_4789DUPLICATE_Splitter_4864);
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4790_4866_5012_5019_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_4865AnonFilter_a2_4817);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4862WEIGHTED_ROUND_ROBIN_Splitter_4927);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4873Pre_CollapsedDataParallel_1_4862);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_int(&SplitJoin130_iDCT8x8_1D_row_fast_Fiss_5017_5024_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_4863WEIGHTED_ROUND_ROBIN_Splitter_4937);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5014_5021_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938WEIGHTED_ROUND_ROBIN_Splitter_4947);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_5003iDCT8x8_1D_col_fast_4816);
	FOR(int, __iter_init_8_, 0, <, 53, __iter_init_8_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_5013_5020_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 53, __iter_init_9_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_5016_5023_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 53, __iter_init_10_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_5016_5023_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 53, __iter_init_11_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_5013_5020_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_4792
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_4792_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4929
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4929_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4930
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4930_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4931
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4931_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4932
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4932_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4933
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4933_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4934
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4934_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4935
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4935_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4936
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4936_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4939
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4939_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4940
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4940_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4941
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4941_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4942
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4942_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4943
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4943_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4944
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4944_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4945
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4945_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4946
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4946_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_4817
	 {
	AnonFilter_a2_4817_s.count = 0.0 ; 
	AnonFilter_a2_4817_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_4789();
		DUPLICATE_Splitter_4864();
			iDCT_2D_reference_coarse_4792();
			WEIGHTED_ROUND_ROBIN_Splitter_4872();
				AnonFilter_a3_4874();
				AnonFilter_a3_4875();
				AnonFilter_a3_4876();
				AnonFilter_a3_4877();
				AnonFilter_a3_4878();
				AnonFilter_a3_4879();
				AnonFilter_a3_4880();
				AnonFilter_a3_4881();
				AnonFilter_a3_4882();
				AnonFilter_a3_4883();
				AnonFilter_a3_4884();
				AnonFilter_a3_4885();
				AnonFilter_a3_4886();
				AnonFilter_a3_4887();
				AnonFilter_a3_4888();
				AnonFilter_a3_4889();
				AnonFilter_a3_4890();
				AnonFilter_a3_4891();
				AnonFilter_a3_4892();
				AnonFilter_a3_4893();
				AnonFilter_a3_4894();
				AnonFilter_a3_4895();
				AnonFilter_a3_4896();
				AnonFilter_a3_4897();
				AnonFilter_a3_4898();
				AnonFilter_a3_4899();
				AnonFilter_a3_4900();
				AnonFilter_a3_4901();
				AnonFilter_a3_4902();
				AnonFilter_a3_4903();
				AnonFilter_a3_4904();
				AnonFilter_a3_4905();
				AnonFilter_a3_4906();
				AnonFilter_a3_4907();
				AnonFilter_a3_4908();
				AnonFilter_a3_4909();
				AnonFilter_a3_4910();
				AnonFilter_a3_4911();
				AnonFilter_a3_4912();
				AnonFilter_a3_4913();
				AnonFilter_a3_4914();
				AnonFilter_a3_4915();
				AnonFilter_a3_4916();
				AnonFilter_a3_4917();
				AnonFilter_a3_4918();
				AnonFilter_a3_4919();
				AnonFilter_a3_4920();
				AnonFilter_a3_4921();
				AnonFilter_a3_4922();
				AnonFilter_a3_4923();
				AnonFilter_a3_4924();
				AnonFilter_a3_4925();
				AnonFilter_a3_4926();
			WEIGHTED_ROUND_ROBIN_Joiner_4873();
			Pre_CollapsedDataParallel_1_4862();
			WEIGHTED_ROUND_ROBIN_Splitter_4927();
				iDCT_1D_reference_fine_4929();
				iDCT_1D_reference_fine_4930();
				iDCT_1D_reference_fine_4931();
				iDCT_1D_reference_fine_4932();
				iDCT_1D_reference_fine_4933();
				iDCT_1D_reference_fine_4934();
				iDCT_1D_reference_fine_4935();
				iDCT_1D_reference_fine_4936();
			WEIGHTED_ROUND_ROBIN_Joiner_4928();
			Post_CollapsedDataParallel_2_4863();
			WEIGHTED_ROUND_ROBIN_Splitter_4937();
				iDCT_1D_reference_fine_4939();
				iDCT_1D_reference_fine_4940();
				iDCT_1D_reference_fine_4941();
				iDCT_1D_reference_fine_4942();
				iDCT_1D_reference_fine_4943();
				iDCT_1D_reference_fine_4944();
				iDCT_1D_reference_fine_4945();
				iDCT_1D_reference_fine_4946();
			WEIGHTED_ROUND_ROBIN_Joiner_4938();
			WEIGHTED_ROUND_ROBIN_Splitter_4947();
				AnonFilter_a4_4949();
				AnonFilter_a4_4950();
				AnonFilter_a4_4951();
				AnonFilter_a4_4952();
				AnonFilter_a4_4953();
				AnonFilter_a4_4954();
				AnonFilter_a4_4955();
				AnonFilter_a4_4956();
				AnonFilter_a4_4957();
				AnonFilter_a4_4958();
				AnonFilter_a4_4959();
				AnonFilter_a4_4960();
				AnonFilter_a4_4961();
				AnonFilter_a4_4962();
				AnonFilter_a4_4963();
				AnonFilter_a4_4964();
				AnonFilter_a4_4965();
				AnonFilter_a4_4966();
				AnonFilter_a4_4967();
				AnonFilter_a4_4968();
				AnonFilter_a4_4969();
				AnonFilter_a4_4970();
				AnonFilter_a4_4971();
				AnonFilter_a4_4972();
				AnonFilter_a4_4973();
				AnonFilter_a4_4974();
				AnonFilter_a4_4975();
				AnonFilter_a4_4976();
				AnonFilter_a4_4977();
				AnonFilter_a4_4978();
				AnonFilter_a4_4979();
				AnonFilter_a4_4980();
				AnonFilter_a4_4981();
				AnonFilter_a4_4982();
				AnonFilter_a4_4983();
				AnonFilter_a4_4984();
				AnonFilter_a4_4985();
				AnonFilter_a4_4986();
				AnonFilter_a4_4987();
				AnonFilter_a4_4988();
				AnonFilter_a4_4989();
				AnonFilter_a4_4990();
				AnonFilter_a4_4991();
				AnonFilter_a4_4992();
				AnonFilter_a4_4993();
				AnonFilter_a4_4994();
				AnonFilter_a4_4995();
				AnonFilter_a4_4996();
				AnonFilter_a4_4997();
				AnonFilter_a4_4998();
				AnonFilter_a4_4999();
				AnonFilter_a4_5000();
				AnonFilter_a4_5001();
			WEIGHTED_ROUND_ROBIN_Joiner_4948();
			WEIGHTED_ROUND_ROBIN_Splitter_5002();
				iDCT8x8_1D_row_fast_5004();
				iDCT8x8_1D_row_fast_5005();
				iDCT8x8_1D_row_fast_5006();
				iDCT8x8_1D_row_fast_5007();
				iDCT8x8_1D_row_fast_5008();
				iDCT8x8_1D_row_fast_5009();
				iDCT8x8_1D_row_fast_5010();
				iDCT8x8_1D_row_fast_5011();
			WEIGHTED_ROUND_ROBIN_Joiner_5003();
			iDCT8x8_1D_col_fast_4816();
		WEIGHTED_ROUND_ROBIN_Joiner_4865();
		AnonFilter_a2_4817();
	ENDFOR
	return EXIT_SUCCESS;
}
