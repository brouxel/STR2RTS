#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2688 on the compile command line
#else
#if BUF_SIZEMAX < 2688
#error BUF_SIZEMAX too small, it must be at least 2688
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_17856_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_17880_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_17881_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_17853();
void DUPLICATE_Splitter_17928();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_17856();
void WEIGHTED_ROUND_ROBIN_Splitter_17936();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_17938();
void AnonFilter_a3_17939();
void AnonFilter_a3_17940();
void AnonFilter_a3_17941();
void AnonFilter_a3_17942();
void AnonFilter_a3_17943();
void AnonFilter_a3_17944();
void WEIGHTED_ROUND_ROBIN_Joiner_17937();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_17926();
void WEIGHTED_ROUND_ROBIN_Splitter_17945();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_17947();
void iDCT_1D_reference_fine_17948();
void iDCT_1D_reference_fine_17949();
void iDCT_1D_reference_fine_17950();
void iDCT_1D_reference_fine_17951();
void iDCT_1D_reference_fine_17952();
void iDCT_1D_reference_fine_17953();
void WEIGHTED_ROUND_ROBIN_Joiner_17946();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_17927();
void WEIGHTED_ROUND_ROBIN_Splitter_17954();
void iDCT_1D_reference_fine_17956();
void iDCT_1D_reference_fine_17957();
void iDCT_1D_reference_fine_17958();
void iDCT_1D_reference_fine_17959();
void iDCT_1D_reference_fine_17960();
void iDCT_1D_reference_fine_17961();
void iDCT_1D_reference_fine_17962();
void WEIGHTED_ROUND_ROBIN_Joiner_17955();
void WEIGHTED_ROUND_ROBIN_Splitter_17963();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_17965();
void AnonFilter_a4_17966();
void AnonFilter_a4_17967();
void AnonFilter_a4_17968();
void AnonFilter_a4_17969();
void AnonFilter_a4_17970();
void AnonFilter_a4_17971();
void WEIGHTED_ROUND_ROBIN_Joiner_17964();
void WEIGHTED_ROUND_ROBIN_Splitter_17972();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_17974();
void iDCT8x8_1D_row_fast_17975();
void iDCT8x8_1D_row_fast_17976();
void iDCT8x8_1D_row_fast_17977();
void iDCT8x8_1D_row_fast_17978();
void iDCT8x8_1D_row_fast_17979();
void iDCT8x8_1D_row_fast_17980();
void WEIGHTED_ROUND_ROBIN_Joiner_17973();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_17880();
void WEIGHTED_ROUND_ROBIN_Joiner_17929();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_17881();

#ifdef __cplusplus
}
#endif
#endif
