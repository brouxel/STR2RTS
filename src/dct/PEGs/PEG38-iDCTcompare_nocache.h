#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=7296 on the compile command line
#else
#if BUF_SIZEMAX < 7296
#error BUF_SIZEMAX too small, it must be at least 7296
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_9982_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_10006_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_10007_t;
void AnonFilter_a0_9979();
void DUPLICATE_Splitter_10054();
void iDCT_2D_reference_coarse_9982();
void WEIGHTED_ROUND_ROBIN_Splitter_10062();
void AnonFilter_a3_10064();
void AnonFilter_a3_10065();
void AnonFilter_a3_10066();
void AnonFilter_a3_10067();
void AnonFilter_a3_10068();
void AnonFilter_a3_10069();
void AnonFilter_a3_10070();
void AnonFilter_a3_10071();
void AnonFilter_a3_10072();
void AnonFilter_a3_10073();
void AnonFilter_a3_10074();
void AnonFilter_a3_10075();
void AnonFilter_a3_10076();
void AnonFilter_a3_10077();
void AnonFilter_a3_10078();
void AnonFilter_a3_10079();
void AnonFilter_a3_10080();
void AnonFilter_a3_10081();
void AnonFilter_a3_10082();
void AnonFilter_a3_10083();
void AnonFilter_a3_10084();
void AnonFilter_a3_10085();
void AnonFilter_a3_10086();
void AnonFilter_a3_10087();
void AnonFilter_a3_10088();
void AnonFilter_a3_10089();
void AnonFilter_a3_10090();
void AnonFilter_a3_10091();
void AnonFilter_a3_10092();
void AnonFilter_a3_10093();
void AnonFilter_a3_10094();
void AnonFilter_a3_10095();
void AnonFilter_a3_10096();
void AnonFilter_a3_10097();
void AnonFilter_a3_10098();
void AnonFilter_a3_10099();
void AnonFilter_a3_10100();
void AnonFilter_a3_10101();
void WEIGHTED_ROUND_ROBIN_Joiner_10063();
void Pre_CollapsedDataParallel_1_10052();
void WEIGHTED_ROUND_ROBIN_Splitter_10102();
void iDCT_1D_reference_fine_10104();
void iDCT_1D_reference_fine_10105();
void iDCT_1D_reference_fine_10106();
void iDCT_1D_reference_fine_10107();
void iDCT_1D_reference_fine_10108();
void iDCT_1D_reference_fine_10109();
void iDCT_1D_reference_fine_10110();
void iDCT_1D_reference_fine_10111();
void WEIGHTED_ROUND_ROBIN_Joiner_10103();
void Post_CollapsedDataParallel_2_10053();
void WEIGHTED_ROUND_ROBIN_Splitter_10112();
void iDCT_1D_reference_fine_10114();
void iDCT_1D_reference_fine_10115();
void iDCT_1D_reference_fine_10116();
void iDCT_1D_reference_fine_10117();
void iDCT_1D_reference_fine_10118();
void iDCT_1D_reference_fine_10119();
void iDCT_1D_reference_fine_10120();
void iDCT_1D_reference_fine_10121();
void WEIGHTED_ROUND_ROBIN_Joiner_10113();
void WEIGHTED_ROUND_ROBIN_Splitter_10122();
void AnonFilter_a4_10124();
void AnonFilter_a4_10125();
void AnonFilter_a4_10126();
void AnonFilter_a4_10127();
void AnonFilter_a4_10128();
void AnonFilter_a4_10129();
void AnonFilter_a4_10130();
void AnonFilter_a4_10131();
void AnonFilter_a4_10132();
void AnonFilter_a4_10133();
void AnonFilter_a4_10134();
void AnonFilter_a4_10135();
void AnonFilter_a4_10136();
void AnonFilter_a4_10137();
void AnonFilter_a4_10138();
void AnonFilter_a4_10139();
void AnonFilter_a4_10140();
void AnonFilter_a4_10141();
void AnonFilter_a4_10142();
void AnonFilter_a4_10143();
void AnonFilter_a4_10144();
void AnonFilter_a4_10145();
void AnonFilter_a4_10146();
void AnonFilter_a4_10147();
void AnonFilter_a4_10148();
void AnonFilter_a4_10149();
void AnonFilter_a4_10150();
void AnonFilter_a4_10151();
void AnonFilter_a4_10152();
void AnonFilter_a4_10153();
void AnonFilter_a4_10154();
void AnonFilter_a4_10155();
void AnonFilter_a4_10156();
void AnonFilter_a4_10157();
void AnonFilter_a4_10158();
void AnonFilter_a4_10159();
void AnonFilter_a4_10160();
void AnonFilter_a4_10161();
void WEIGHTED_ROUND_ROBIN_Joiner_10123();
void WEIGHTED_ROUND_ROBIN_Splitter_10162();
void iDCT8x8_1D_row_fast_10164();
void iDCT8x8_1D_row_fast_10165();
void iDCT8x8_1D_row_fast_10166();
void iDCT8x8_1D_row_fast_10167();
void iDCT8x8_1D_row_fast_10168();
void iDCT8x8_1D_row_fast_10169();
void iDCT8x8_1D_row_fast_10170();
void iDCT8x8_1D_row_fast_10171();
void WEIGHTED_ROUND_ROBIN_Joiner_10163();
void iDCT8x8_1D_col_fast_10006();
void WEIGHTED_ROUND_ROBIN_Joiner_10055();
void AnonFilter_a2_10007();

#ifdef __cplusplus
}
#endif
#endif
