#include "PEG9-iDCTcompare.h"

buffer_int_t SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[8];
buffer_int_t SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[9];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17545Pre_CollapsedDataParallel_1_17534;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[9];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_17587iDCT8x8_1D_col_fast_17488;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17566WEIGHTED_ROUND_ROBIN_Splitter_17575;
buffer_float_t Pre_CollapsedDataParallel_1_17534WEIGHTED_ROUND_ROBIN_Splitter_17555;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_17537AnonFilter_a2_17489;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17556Post_CollapsedDataParallel_2_17535;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[9];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[9];
buffer_int_t AnonFilter_a0_17461DUPLICATE_Splitter_17536;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_split[3];
buffer_float_t Post_CollapsedDataParallel_2_17535WEIGHTED_ROUND_ROBIN_Splitter_17565;


iDCT_2D_reference_coarse_17464_t iDCT_2D_reference_coarse_17464_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17557_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17558_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17559_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17560_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17561_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17562_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17563_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17564_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17567_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17568_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17569_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17570_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17571_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17572_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17573_s;
iDCT_2D_reference_coarse_17464_t iDCT_1D_reference_fine_17574_s;
iDCT8x8_1D_col_fast_17488_t iDCT8x8_1D_col_fast_17488_s;
AnonFilter_a2_17489_t AnonFilter_a2_17489_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_17461() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_17461DUPLICATE_Splitter_17536));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_17464_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_17464_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_17464() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_17546() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[0]));
	ENDFOR
}

void AnonFilter_a3_17547() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[1]));
	ENDFOR
}

void AnonFilter_a3_17548() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[2]));
	ENDFOR
}

void AnonFilter_a3_17549() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[3]));
	ENDFOR
}

void AnonFilter_a3_17550() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[4]));
	ENDFOR
}

void AnonFilter_a3_17551() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[5]));
	ENDFOR
}

void AnonFilter_a3_17552() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[6]));
	ENDFOR
}

void AnonFilter_a3_17553() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[7]));
	ENDFOR
}

void AnonFilter_a3_17554() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[8]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17544() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17545() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17545Pre_CollapsedDataParallel_1_17534, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_17534() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_17545Pre_CollapsedDataParallel_1_17534), &(Pre_CollapsedDataParallel_1_17534WEIGHTED_ROUND_ROBIN_Splitter_17555));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17557_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_17557() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_17558() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_17559() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_17560() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_17561() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_17562() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_17563() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_17564() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17555() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_17534WEIGHTED_ROUND_ROBIN_Splitter_17555));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17556() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17556Post_CollapsedDataParallel_2_17535, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_17535() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_17556Post_CollapsedDataParallel_2_17535), &(Post_CollapsedDataParallel_2_17535WEIGHTED_ROUND_ROBIN_Splitter_17565));
	ENDFOR
}

void iDCT_1D_reference_fine_17567() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_17568() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_17569() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_17570() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_17571() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_17572() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_17573() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_17574() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17565() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_17535WEIGHTED_ROUND_ROBIN_Splitter_17565));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17566() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17566WEIGHTED_ROUND_ROBIN_Splitter_17575, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_17577() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[0]));
	ENDFOR
}

void AnonFilter_a4_17578() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[1]));
	ENDFOR
}

void AnonFilter_a4_17579() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[2]));
	ENDFOR
}

void AnonFilter_a4_17580() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[3]));
	ENDFOR
}

void AnonFilter_a4_17581() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[4]));
	ENDFOR
}

void AnonFilter_a4_17582() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[5]));
	ENDFOR
}

void AnonFilter_a4_17583() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[6]));
	ENDFOR
}

void AnonFilter_a4_17584() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[7]));
	ENDFOR
}

void AnonFilter_a4_17585() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[8]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17575() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_17566WEIGHTED_ROUND_ROBIN_Splitter_17575));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17576() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_17588() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[0]), &(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17589() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[1]), &(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17590() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[2]), &(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17591() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[3]), &(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17592() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[4]), &(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17593() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[5]), &(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17594() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[6]), &(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17595() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[7]), &(SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17586() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17587() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_17587iDCT8x8_1D_col_fast_17488, pop_int(&SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_17488_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_17488_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_17488_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_17488_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_17488_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_17488_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_17488_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_17488_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_17488_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_17488_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_17488() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_17587iDCT8x8_1D_col_fast_17488), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_17536() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_17461DUPLICATE_Splitter_17536);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17537() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_17537AnonFilter_a2_17489, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_17489_s.count = (AnonFilter_a2_17489_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_17489_s.errors = (AnonFilter_a2_17489_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_17489_s.errors / AnonFilter_a2_17489_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_17489_s.errors = (AnonFilter_a2_17489_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_17489_s.errors / AnonFilter_a2_17489_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_17489() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_17537AnonFilter_a2_17489));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin42_iDCT8x8_1D_row_fast_Fiss_17601_17608_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 9, __iter_init_3_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_17597_17604_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17599_17606_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17598_17605_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17545Pre_CollapsedDataParallel_1_17534);
	FOR(int, __iter_init_8_, 0, <, 9, __iter_init_8_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_17600_17607_join[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_17587iDCT8x8_1D_col_fast_17488);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17566WEIGHTED_ROUND_ROBIN_Splitter_17575);
	init_buffer_float(&Pre_CollapsedDataParallel_1_17534WEIGHTED_ROUND_ROBIN_Splitter_17555);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_17537AnonFilter_a2_17489);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17556Post_CollapsedDataParallel_2_17535);
	FOR(int, __iter_init_9_, 0, <, 9, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_17597_17604_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 9, __iter_init_10_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_17600_17607_split[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_17461DUPLICATE_Splitter_17536);
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17462_17538_17596_17603_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_17535WEIGHTED_ROUND_ROBIN_Splitter_17565);
// --- init: iDCT_2D_reference_coarse_17464
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_17464_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17557
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17557_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17558
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17558_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17559
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17559_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17560
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17560_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17561
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17561_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17562
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17562_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17563
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17563_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17564
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17564_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17567
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17567_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17568
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17568_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17569
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17569_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17570
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17570_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17571
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17571_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17572
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17572_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17573
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17573_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17574
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17574_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_17489
	 {
	AnonFilter_a2_17489_s.count = 0.0 ; 
	AnonFilter_a2_17489_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_17461();
		DUPLICATE_Splitter_17536();
			iDCT_2D_reference_coarse_17464();
			WEIGHTED_ROUND_ROBIN_Splitter_17544();
				AnonFilter_a3_17546();
				AnonFilter_a3_17547();
				AnonFilter_a3_17548();
				AnonFilter_a3_17549();
				AnonFilter_a3_17550();
				AnonFilter_a3_17551();
				AnonFilter_a3_17552();
				AnonFilter_a3_17553();
				AnonFilter_a3_17554();
			WEIGHTED_ROUND_ROBIN_Joiner_17545();
			Pre_CollapsedDataParallel_1_17534();
			WEIGHTED_ROUND_ROBIN_Splitter_17555();
				iDCT_1D_reference_fine_17557();
				iDCT_1D_reference_fine_17558();
				iDCT_1D_reference_fine_17559();
				iDCT_1D_reference_fine_17560();
				iDCT_1D_reference_fine_17561();
				iDCT_1D_reference_fine_17562();
				iDCT_1D_reference_fine_17563();
				iDCT_1D_reference_fine_17564();
			WEIGHTED_ROUND_ROBIN_Joiner_17556();
			Post_CollapsedDataParallel_2_17535();
			WEIGHTED_ROUND_ROBIN_Splitter_17565();
				iDCT_1D_reference_fine_17567();
				iDCT_1D_reference_fine_17568();
				iDCT_1D_reference_fine_17569();
				iDCT_1D_reference_fine_17570();
				iDCT_1D_reference_fine_17571();
				iDCT_1D_reference_fine_17572();
				iDCT_1D_reference_fine_17573();
				iDCT_1D_reference_fine_17574();
			WEIGHTED_ROUND_ROBIN_Joiner_17566();
			WEIGHTED_ROUND_ROBIN_Splitter_17575();
				AnonFilter_a4_17577();
				AnonFilter_a4_17578();
				AnonFilter_a4_17579();
				AnonFilter_a4_17580();
				AnonFilter_a4_17581();
				AnonFilter_a4_17582();
				AnonFilter_a4_17583();
				AnonFilter_a4_17584();
				AnonFilter_a4_17585();
			WEIGHTED_ROUND_ROBIN_Joiner_17576();
			WEIGHTED_ROUND_ROBIN_Splitter_17586();
				iDCT8x8_1D_row_fast_17588();
				iDCT8x8_1D_row_fast_17589();
				iDCT8x8_1D_row_fast_17590();
				iDCT8x8_1D_row_fast_17591();
				iDCT8x8_1D_row_fast_17592();
				iDCT8x8_1D_row_fast_17593();
				iDCT8x8_1D_row_fast_17594();
				iDCT8x8_1D_row_fast_17595();
			WEIGHTED_ROUND_ROBIN_Joiner_17587();
			iDCT8x8_1D_col_fast_17488();
		WEIGHTED_ROUND_ROBIN_Joiner_17537();
		AnonFilter_a2_17489();
	ENDFOR
	return EXIT_SUCCESS;
}
