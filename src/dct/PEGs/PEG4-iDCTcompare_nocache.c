#include "PEG4-iDCTcompare_nocache.h"

buffer_int_t AnonFilter_a0_18375DUPLICATE_Splitter_18450;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_18492_18499_join[4];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_18451AnonFilter_a2_18403;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18471WEIGHTED_ROUND_ROBIN_Splitter_18476;
buffer_int_t SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18459Pre_CollapsedDataParallel_1_18448;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18465Post_CollapsedDataParallel_2_18449;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_split[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[4];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_18489_18496_split[4];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_18492_18499_split[4];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_join[4];
buffer_float_t Post_CollapsedDataParallel_2_18449WEIGHTED_ROUND_ROBIN_Splitter_18470;
buffer_float_t Pre_CollapsedDataParallel_1_18448WEIGHTED_ROUND_ROBIN_Splitter_18464;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_join[3];
buffer_int_t SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[4];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_join[4];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_18489_18496_join[4];


iDCT_2D_reference_coarse_18378_t iDCT_2D_reference_coarse_18378_s;
iDCT_2D_reference_coarse_18378_t iDCT_1D_reference_fine_18466_s;
iDCT_2D_reference_coarse_18378_t iDCT_1D_reference_fine_18467_s;
iDCT_2D_reference_coarse_18378_t iDCT_1D_reference_fine_18468_s;
iDCT_2D_reference_coarse_18378_t iDCT_1D_reference_fine_18469_s;
iDCT_2D_reference_coarse_18378_t iDCT_1D_reference_fine_18472_s;
iDCT_2D_reference_coarse_18378_t iDCT_1D_reference_fine_18473_s;
iDCT_2D_reference_coarse_18378_t iDCT_1D_reference_fine_18474_s;
iDCT_2D_reference_coarse_18378_t iDCT_1D_reference_fine_18475_s;
iDCT8x8_1D_col_fast_18402_t iDCT8x8_1D_col_fast_18402_s;
AnonFilter_a2_18403_t AnonFilter_a2_18403_s;

void AnonFilter_a0_18375() {
	FOR(int, i, 0,  < , 64, i++) {
		push_int(&AnonFilter_a0_18375DUPLICATE_Splitter_18450, (((int) pow(3.0, i)) % 75)) ; 
	}
	ENDFOR
}


void iDCT_2D_reference_coarse_18378() {
	float block_x[8][8];
	FOR(int, i, 0,  < , 8, i++) {
		FOR(int, j, 0,  < , 8, j++) {
			block_x[i][j] = 0.0 ; 
			FOR(int, k, 0,  < , 8, k++) {
				block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_18378_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_split[0], ((8 * i) + k)))) ; 
			}
			ENDFOR
		}
		ENDFOR
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		FOR(int, j, 0,  < , 8, j++) {
			float block_y = 0.0;
			FOR(int, k, 0,  < , 8, k++) {
				block_y = (block_y + (iDCT_2D_reference_coarse_18378_s.coeff[k][i] * block_x[k][j])) ; 
			}
			ENDFOR
			block_y = ((float) floor((block_y + 0.5))) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_join[0], ((int) block_y)) ; 
		}
		ENDFOR
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_split[0]) ; 
	}
	ENDFOR
}


void AnonFilter_a3_18460(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_18461(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_18462(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_18463(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18458() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18459() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18459Pre_CollapsedDataParallel_1_18448, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_18448() {
 {
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
		iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
		FOR(int, _i, 0,  < , 8, _i++) {
			push_float(&Pre_CollapsedDataParallel_1_18448WEIGHTED_ROUND_ROBIN_Splitter_18464, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_18459Pre_CollapsedDataParallel_1_18448, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_18459Pre_CollapsedDataParallel_1_18448) ; 
}


void iDCT_1D_reference_fine_18466(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18466_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18467(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18467_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18468(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18468_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18469(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18469_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_18448WEIGHTED_ROUND_ROBIN_Splitter_18464));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18465() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18465Post_CollapsedDataParallel_2_18449, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_18449() {
 {
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int partialSum_i = 0;
		partialSum_i = 0 ; 
		partialSum_i = 0 ; 
 {
		FOR(int, _i, 0,  < , 8, _i++) {
			push_float(&Post_CollapsedDataParallel_2_18449WEIGHTED_ROUND_ROBIN_Splitter_18470, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_18465Post_CollapsedDataParallel_2_18449, (_k + (partialSum_i + 0)))) ; 
			partialSum_i = (partialSum_i + 8) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_18465Post_CollapsedDataParallel_2_18449) ; 
}


void iDCT_1D_reference_fine_18472(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18472_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18473(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18473_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18474(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18474_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18475(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18475_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18470() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_18449WEIGHTED_ROUND_ROBIN_Splitter_18470));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18471() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18471WEIGHTED_ROUND_ROBIN_Splitter_18476, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_18478(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_18479(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_18480(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_18481(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18476() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_18471WEIGHTED_ROUND_ROBIN_Splitter_18476));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18477() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_18484(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0], 6) ; 
		x3 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0], 2) ; 
		x4 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0], 1) ; 
		x5 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0], 7) ; 
		x6 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0], 5) ; 
		x7 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_18485(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1], 6) ; 
		x3 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1], 2) ; 
		x4 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1], 1) ; 
		x5 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1], 7) ; 
		x6 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1], 5) ; 
		x7 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_18486(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2], 6) ; 
		x3 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2], 2) ; 
		x4 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2], 1) ; 
		x5 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2], 7) ; 
		x6 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2], 5) ; 
		x7 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_18487(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3], 6) ; 
		x3 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3], 2) ; 
		x4 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3], 1) ; 
		x5 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3], 7) ; 
		x6 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3], 5) ; 
		x7 = peek_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18482() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18483() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, pop_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_18402() {
	FOR(int, c, 0,  < , 8, c++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, (c + 0)) ; 
		x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, (c + 32)) << 8) ; 
		x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, (c + 48)) ; 
		x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, (c + 16)) ; 
		x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, (c + 8)) ; 
		x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, (c + 56)) ; 
		x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, (c + 40)) ; 
		x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402, (c + 24)) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = ((x0 + 32) >> 6) ; 
			FOR(int, i, 0,  < , 8, i++) {
				iDCT8x8_1D_col_fast_18402_s.buffer[(c + (8 * i))] = x0 ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 8) + 8192) ; 
			x8 = ((565 * (x4 + x5)) + 4) ; 
			x4 = ((x8 + (2276 * x4)) >> 3) ; 
			x5 = ((x8 - (3406 * x5)) >> 3) ; 
			x8 = ((2408 * (x6 + x7)) + 4) ; 
			x6 = ((x8 - (799 * x6)) >> 3) ; 
			x7 = ((x8 - (4017 * x7)) >> 3) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = ((1108 * (x3 + x2)) + 4) ; 
			x2 = ((x1 - (3784 * x2)) >> 3) ; 
			x3 = ((x1 + (1568 * x3)) >> 3) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			iDCT8x8_1D_col_fast_18402_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
			iDCT8x8_1D_col_fast_18402_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
			iDCT8x8_1D_col_fast_18402_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
			iDCT8x8_1D_col_fast_18402_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
			iDCT8x8_1D_col_fast_18402_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
			iDCT8x8_1D_col_fast_18402_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
			iDCT8x8_1D_col_fast_18402_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
			iDCT8x8_1D_col_fast_18402_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
		}
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402) ; 
		push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_join[2], iDCT8x8_1D_col_fast_18402_s.buffer[i]) ; 
	}
	ENDFOR
}


void DUPLICATE_Splitter_18450() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_18375DUPLICATE_Splitter_18450);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18451() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_18451AnonFilter_a2_18403, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_18403(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_18451AnonFilter_a2_18403) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_18451AnonFilter_a2_18403) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_18451AnonFilter_a2_18403) ; 
		AnonFilter_a2_18403_s.count = (AnonFilter_a2_18403_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_18403_s.errors = (AnonFilter_a2_18403_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_18403_s.errors / AnonFilter_a2_18403_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_18403_s.errors = (AnonFilter_a2_18403_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_18403_s.errors / AnonFilter_a2_18403_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_int(&AnonFilter_a0_18375DUPLICATE_Splitter_18450);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_join[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_18483iDCT8x8_1D_col_fast_18402);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_18451AnonFilter_a2_18403);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18471WEIGHTED_ROUND_ROBIN_Splitter_18476);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18459Pre_CollapsedDataParallel_1_18448);
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18465Post_CollapsedDataParallel_2_18449);
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_18492_18499_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18490_18497_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_18449WEIGHTED_ROUND_ROBIN_Splitter_18470);
	init_buffer_float(&Pre_CollapsedDataParallel_1_18448WEIGHTED_ROUND_ROBIN_Splitter_18464);
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18376_18452_18488_18495_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_int(&SplitJoin24_iDCT8x8_1D_row_fast_Fiss_18493_18500_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18491_18498_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_18489_18496_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_18378
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_18378_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18466
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18466_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18467
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18467_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18468
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18468_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18469
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18469_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18472
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18472_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18473
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18473_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18474
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18474_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18475
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18475_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_18403
	 {
	AnonFilter_a2_18403_s.count = 0.0 ; 
	AnonFilter_a2_18403_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_18375();
		DUPLICATE_Splitter_18450();
			iDCT_2D_reference_coarse_18378();
			WEIGHTED_ROUND_ROBIN_Splitter_18458();
				AnonFilter_a3_18460();
				AnonFilter_a3_18461();
				AnonFilter_a3_18462();
				AnonFilter_a3_18463();
			WEIGHTED_ROUND_ROBIN_Joiner_18459();
			Pre_CollapsedDataParallel_1_18448();
			WEIGHTED_ROUND_ROBIN_Splitter_18464();
				iDCT_1D_reference_fine_18466();
				iDCT_1D_reference_fine_18467();
				iDCT_1D_reference_fine_18468();
				iDCT_1D_reference_fine_18469();
			WEIGHTED_ROUND_ROBIN_Joiner_18465();
			Post_CollapsedDataParallel_2_18449();
			WEIGHTED_ROUND_ROBIN_Splitter_18470();
				iDCT_1D_reference_fine_18472();
				iDCT_1D_reference_fine_18473();
				iDCT_1D_reference_fine_18474();
				iDCT_1D_reference_fine_18475();
			WEIGHTED_ROUND_ROBIN_Joiner_18471();
			WEIGHTED_ROUND_ROBIN_Splitter_18476();
				AnonFilter_a4_18478();
				AnonFilter_a4_18479();
				AnonFilter_a4_18480();
				AnonFilter_a4_18481();
			WEIGHTED_ROUND_ROBIN_Joiner_18477();
			WEIGHTED_ROUND_ROBIN_Splitter_18482();
				iDCT8x8_1D_row_fast_18484();
				iDCT8x8_1D_row_fast_18485();
				iDCT8x8_1D_row_fast_18486();
				iDCT8x8_1D_row_fast_18487();
			WEIGHTED_ROUND_ROBIN_Joiner_18483();
			iDCT8x8_1D_col_fast_18402();
		WEIGHTED_ROUND_ROBIN_Joiner_18451();
		AnonFilter_a2_18403();
	ENDFOR
	return EXIT_SUCCESS;
}
