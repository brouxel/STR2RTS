#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=9600 on the compile command line
#else
#if BUF_SIZEMAX < 9600
#error BUF_SIZEMAX too small, it must be at least 9600
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_13752_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_13776_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_13777_t;
void AnonFilter_a0_13749();
void DUPLICATE_Splitter_13824();
void iDCT_2D_reference_coarse_13752();
void WEIGHTED_ROUND_ROBIN_Splitter_13832();
void AnonFilter_a3_13834();
void AnonFilter_a3_13835();
void AnonFilter_a3_13836();
void AnonFilter_a3_13837();
void AnonFilter_a3_13838();
void AnonFilter_a3_13839();
void AnonFilter_a3_13840();
void AnonFilter_a3_13841();
void AnonFilter_a3_13842();
void AnonFilter_a3_13843();
void AnonFilter_a3_13844();
void AnonFilter_a3_13845();
void AnonFilter_a3_13846();
void AnonFilter_a3_13847();
void AnonFilter_a3_13848();
void AnonFilter_a3_13849();
void AnonFilter_a3_13850();
void AnonFilter_a3_13851();
void AnonFilter_a3_13852();
void AnonFilter_a3_13853();
void AnonFilter_a3_13854();
void AnonFilter_a3_13855();
void AnonFilter_a3_13856();
void AnonFilter_a3_13857();
void AnonFilter_a3_13858();
void WEIGHTED_ROUND_ROBIN_Joiner_13833();
void Pre_CollapsedDataParallel_1_13822();
void WEIGHTED_ROUND_ROBIN_Splitter_13859();
void iDCT_1D_reference_fine_13861();
void iDCT_1D_reference_fine_13862();
void iDCT_1D_reference_fine_13863();
void iDCT_1D_reference_fine_13864();
void iDCT_1D_reference_fine_13865();
void iDCT_1D_reference_fine_13866();
void iDCT_1D_reference_fine_13867();
void iDCT_1D_reference_fine_13868();
void WEIGHTED_ROUND_ROBIN_Joiner_13860();
void Post_CollapsedDataParallel_2_13823();
void WEIGHTED_ROUND_ROBIN_Splitter_13869();
void iDCT_1D_reference_fine_13871();
void iDCT_1D_reference_fine_13872();
void iDCT_1D_reference_fine_13873();
void iDCT_1D_reference_fine_13874();
void iDCT_1D_reference_fine_13875();
void iDCT_1D_reference_fine_13876();
void iDCT_1D_reference_fine_13877();
void iDCT_1D_reference_fine_13878();
void WEIGHTED_ROUND_ROBIN_Joiner_13870();
void WEIGHTED_ROUND_ROBIN_Splitter_13879();
void AnonFilter_a4_13881();
void AnonFilter_a4_13882();
void AnonFilter_a4_13883();
void AnonFilter_a4_13884();
void AnonFilter_a4_13885();
void AnonFilter_a4_13886();
void AnonFilter_a4_13887();
void AnonFilter_a4_13888();
void AnonFilter_a4_13889();
void AnonFilter_a4_13890();
void AnonFilter_a4_13891();
void AnonFilter_a4_13892();
void AnonFilter_a4_13893();
void AnonFilter_a4_13894();
void AnonFilter_a4_13895();
void AnonFilter_a4_13896();
void AnonFilter_a4_13897();
void AnonFilter_a4_13898();
void AnonFilter_a4_13899();
void AnonFilter_a4_13900();
void AnonFilter_a4_13901();
void AnonFilter_a4_13902();
void AnonFilter_a4_13903();
void AnonFilter_a4_13904();
void AnonFilter_a4_13905();
void WEIGHTED_ROUND_ROBIN_Joiner_13880();
void WEIGHTED_ROUND_ROBIN_Splitter_13906();
void iDCT8x8_1D_row_fast_13908();
void iDCT8x8_1D_row_fast_13909();
void iDCT8x8_1D_row_fast_13910();
void iDCT8x8_1D_row_fast_13911();
void iDCT8x8_1D_row_fast_13912();
void iDCT8x8_1D_row_fast_13913();
void iDCT8x8_1D_row_fast_13914();
void iDCT8x8_1D_row_fast_13915();
void WEIGHTED_ROUND_ROBIN_Joiner_13907();
void iDCT8x8_1D_col_fast_13776();
void WEIGHTED_ROUND_ROBIN_Joiner_13825();
void AnonFilter_a2_13777();

#ifdef __cplusplus
}
#endif
#endif
