#include "PEG31-iDCTcompare_nocache.h"

buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[31];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12220WEIGHTED_ROUND_ROBIN_Splitter_12229;
buffer_int_t SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12177Pre_CollapsedDataParallel_1_12166;
buffer_int_t AnonFilter_a0_12093DUPLICATE_Splitter_12168;
buffer_int_t SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_join[3];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[31];
buffer_float_t Post_CollapsedDataParallel_2_12167WEIGHTED_ROUND_ROBIN_Splitter_12219;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_12166WEIGHTED_ROUND_ROBIN_Splitter_12209;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12210Post_CollapsedDataParallel_2_12167;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[31];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_12169AnonFilter_a2_12121;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[31];


iDCT_2D_reference_coarse_12096_t iDCT_2D_reference_coarse_12096_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12211_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12212_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12213_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12214_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12215_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12216_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12217_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12218_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12221_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12222_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12223_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12224_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12225_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12226_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12227_s;
iDCT_2D_reference_coarse_12096_t iDCT_1D_reference_fine_12228_s;
iDCT8x8_1D_col_fast_12120_t iDCT8x8_1D_col_fast_12120_s;
AnonFilter_a2_12121_t AnonFilter_a2_12121_s;

void AnonFilter_a0_12093(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_12093DUPLICATE_Splitter_12168, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_12096(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_12096_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_12096_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_12178(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12179(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12180(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12181(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12182(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12183(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12184(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12185(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12186(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12187(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12188(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12189(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12190(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12191(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12192(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12193(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12194(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12195(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12196(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12197(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12198(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12199(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12200(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12201(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12202(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12203(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12204(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12205(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12206(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12207(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12208(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[30])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12176() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12177() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12177Pre_CollapsedDataParallel_1_12166, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_12166(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_12166WEIGHTED_ROUND_ROBIN_Splitter_12209, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_12177Pre_CollapsedDataParallel_1_12166, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12177Pre_CollapsedDataParallel_1_12166) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12211(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12211_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12212(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12212_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12213(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12213_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12214(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12214_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12215(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12215_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12216(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12216_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12217(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12217_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12218(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12218_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12209() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_12166WEIGHTED_ROUND_ROBIN_Splitter_12209));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12210() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12210Post_CollapsedDataParallel_2_12167, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_12167(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_12167WEIGHTED_ROUND_ROBIN_Splitter_12219, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_12210Post_CollapsedDataParallel_2_12167, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12210Post_CollapsedDataParallel_2_12167) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12221(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12221_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12222(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12222_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12223(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12223_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12224(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12224_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12225(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12225_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12226(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12226_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12227(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12227_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12228(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12228_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12219() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_12167WEIGHTED_ROUND_ROBIN_Splitter_12219));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12220() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12220WEIGHTED_ROUND_ROBIN_Splitter_12229, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_12231(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12232(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12233(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12234(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12235(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12236(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12237(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12238(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12239(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12240(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12241(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12242(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12243(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12244(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12245(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12246(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12247(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12248(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12249(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12250(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12251(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12252(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12253(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12254(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12255(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12256(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12257(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12258(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12259(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12260(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12261(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12229() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12220WEIGHTED_ROUND_ROBIN_Splitter_12229));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12230() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_12264(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0], 6) ; 
		x3 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0], 2) ; 
		x4 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0], 1) ; 
		x5 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0], 7) ; 
		x6 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0], 5) ; 
		x7 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12265(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1], 6) ; 
		x3 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1], 2) ; 
		x4 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1], 1) ; 
		x5 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1], 7) ; 
		x6 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1], 5) ; 
		x7 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12266(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2], 6) ; 
		x3 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2], 2) ; 
		x4 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2], 1) ; 
		x5 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2], 7) ; 
		x6 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2], 5) ; 
		x7 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12267(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3], 6) ; 
		x3 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3], 2) ; 
		x4 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3], 1) ; 
		x5 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3], 7) ; 
		x6 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3], 5) ; 
		x7 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12268(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4], 6) ; 
		x3 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4], 2) ; 
		x4 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4], 1) ; 
		x5 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4], 7) ; 
		x6 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4], 5) ; 
		x7 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12269(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5], 6) ; 
		x3 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5], 2) ; 
		x4 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5], 1) ; 
		x5 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5], 7) ; 
		x6 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5], 5) ; 
		x7 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12270(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6], 6) ; 
		x3 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6], 2) ; 
		x4 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6], 1) ; 
		x5 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6], 7) ; 
		x6 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6], 5) ; 
		x7 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12271(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7], 6) ; 
		x3 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7], 2) ; 
		x4 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7], 1) ; 
		x5 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7], 7) ; 
		x6 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7], 5) ; 
		x7 = peek_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12262() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12263() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, pop_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_12120(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_12120_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_12120_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_12120_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_12120_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_12120_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_12120_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_12120_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_12120_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_12120_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_join[2], iDCT8x8_1D_col_fast_12120_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_12168() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_12093DUPLICATE_Splitter_12168);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12169() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_12169AnonFilter_a2_12121, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_12121(){
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12169AnonFilter_a2_12121) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12169AnonFilter_a2_12121) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12169AnonFilter_a2_12121) ; 
		AnonFilter_a2_12121_s.count = (AnonFilter_a2_12121_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_12121_s.errors = (AnonFilter_a2_12121_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_12121_s.errors / AnonFilter_a2_12121_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_12121_s.errors = (AnonFilter_a2_12121_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_12121_s.errors / AnonFilter_a2_12121_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 31, __iter_init_0_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_12263iDCT8x8_1D_col_fast_12120);
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12220WEIGHTED_ROUND_ROBIN_Splitter_12229);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12177Pre_CollapsedDataParallel_1_12166);
	init_buffer_int(&AnonFilter_a0_12093DUPLICATE_Splitter_12168);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_int(&SplitJoin86_iDCT8x8_1D_row_fast_Fiss_12277_12284_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 3, __iter_init_4_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12094_12170_12272_12279_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 31, __iter_init_5_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_12273_12280_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_12167WEIGHTED_ROUND_ROBIN_Splitter_12219);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_12166WEIGHTED_ROUND_ROBIN_Splitter_12209);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12210Post_CollapsedDataParallel_2_12167);
	FOR(int, __iter_init_7_, 0, <, 31, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_join[__iter_init_7_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_12169AnonFilter_a2_12121);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12275_12282_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12274_12281_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 31, __iter_init_11_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_12276_12283_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_12096
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_12096_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12211
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12211_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12212
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12212_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12213
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12213_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12214
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12214_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12215
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12215_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12216
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12216_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12217
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12217_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12218
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12218_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12221
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12221_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12222
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12222_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12223
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12223_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12224
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12224_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12225
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12225_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12226
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12226_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12227
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12227_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12228
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12228_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_12121
	 {
	AnonFilter_a2_12121_s.count = 0.0 ; 
	AnonFilter_a2_12121_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_12093();
		DUPLICATE_Splitter_12168();
			iDCT_2D_reference_coarse_12096();
			WEIGHTED_ROUND_ROBIN_Splitter_12176();
				AnonFilter_a3_12178();
				AnonFilter_a3_12179();
				AnonFilter_a3_12180();
				AnonFilter_a3_12181();
				AnonFilter_a3_12182();
				AnonFilter_a3_12183();
				AnonFilter_a3_12184();
				AnonFilter_a3_12185();
				AnonFilter_a3_12186();
				AnonFilter_a3_12187();
				AnonFilter_a3_12188();
				AnonFilter_a3_12189();
				AnonFilter_a3_12190();
				AnonFilter_a3_12191();
				AnonFilter_a3_12192();
				AnonFilter_a3_12193();
				AnonFilter_a3_12194();
				AnonFilter_a3_12195();
				AnonFilter_a3_12196();
				AnonFilter_a3_12197();
				AnonFilter_a3_12198();
				AnonFilter_a3_12199();
				AnonFilter_a3_12200();
				AnonFilter_a3_12201();
				AnonFilter_a3_12202();
				AnonFilter_a3_12203();
				AnonFilter_a3_12204();
				AnonFilter_a3_12205();
				AnonFilter_a3_12206();
				AnonFilter_a3_12207();
				AnonFilter_a3_12208();
			WEIGHTED_ROUND_ROBIN_Joiner_12177();
			Pre_CollapsedDataParallel_1_12166();
			WEIGHTED_ROUND_ROBIN_Splitter_12209();
				iDCT_1D_reference_fine_12211();
				iDCT_1D_reference_fine_12212();
				iDCT_1D_reference_fine_12213();
				iDCT_1D_reference_fine_12214();
				iDCT_1D_reference_fine_12215();
				iDCT_1D_reference_fine_12216();
				iDCT_1D_reference_fine_12217();
				iDCT_1D_reference_fine_12218();
			WEIGHTED_ROUND_ROBIN_Joiner_12210();
			Post_CollapsedDataParallel_2_12167();
			WEIGHTED_ROUND_ROBIN_Splitter_12219();
				iDCT_1D_reference_fine_12221();
				iDCT_1D_reference_fine_12222();
				iDCT_1D_reference_fine_12223();
				iDCT_1D_reference_fine_12224();
				iDCT_1D_reference_fine_12225();
				iDCT_1D_reference_fine_12226();
				iDCT_1D_reference_fine_12227();
				iDCT_1D_reference_fine_12228();
			WEIGHTED_ROUND_ROBIN_Joiner_12220();
			WEIGHTED_ROUND_ROBIN_Splitter_12229();
				AnonFilter_a4_12231();
				AnonFilter_a4_12232();
				AnonFilter_a4_12233();
				AnonFilter_a4_12234();
				AnonFilter_a4_12235();
				AnonFilter_a4_12236();
				AnonFilter_a4_12237();
				AnonFilter_a4_12238();
				AnonFilter_a4_12239();
				AnonFilter_a4_12240();
				AnonFilter_a4_12241();
				AnonFilter_a4_12242();
				AnonFilter_a4_12243();
				AnonFilter_a4_12244();
				AnonFilter_a4_12245();
				AnonFilter_a4_12246();
				AnonFilter_a4_12247();
				AnonFilter_a4_12248();
				AnonFilter_a4_12249();
				AnonFilter_a4_12250();
				AnonFilter_a4_12251();
				AnonFilter_a4_12252();
				AnonFilter_a4_12253();
				AnonFilter_a4_12254();
				AnonFilter_a4_12255();
				AnonFilter_a4_12256();
				AnonFilter_a4_12257();
				AnonFilter_a4_12258();
				AnonFilter_a4_12259();
				AnonFilter_a4_12260();
				AnonFilter_a4_12261();
			WEIGHTED_ROUND_ROBIN_Joiner_12230();
			WEIGHTED_ROUND_ROBIN_Splitter_12262();
				iDCT8x8_1D_row_fast_12264();
				iDCT8x8_1D_row_fast_12265();
				iDCT8x8_1D_row_fast_12266();
				iDCT8x8_1D_row_fast_12267();
				iDCT8x8_1D_row_fast_12268();
				iDCT8x8_1D_row_fast_12269();
				iDCT8x8_1D_row_fast_12270();
				iDCT8x8_1D_row_fast_12271();
			WEIGHTED_ROUND_ROBIN_Joiner_12263();
			iDCT8x8_1D_col_fast_12120();
		WEIGHTED_ROUND_ROBIN_Joiner_12169();
		AnonFilter_a2_12121();
	ENDFOR
	return EXIT_SUCCESS;
}
