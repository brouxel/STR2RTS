#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=22656 on the compile command line
#else
#if BUF_SIZEMAX < 22656
#error BUF_SIZEMAX too small, it must be at least 22656
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_2464_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_2488_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_2489_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_2461();
void DUPLICATE_Splitter_2536();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_2464();
void WEIGHTED_ROUND_ROBIN_Splitter_2544();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_2546();
void AnonFilter_a3_2547();
void AnonFilter_a3_2548();
void AnonFilter_a3_2549();
void AnonFilter_a3_2550();
void AnonFilter_a3_2551();
void AnonFilter_a3_2552();
void AnonFilter_a3_2553();
void AnonFilter_a3_2554();
void AnonFilter_a3_2555();
void AnonFilter_a3_2556();
void AnonFilter_a3_2557();
void AnonFilter_a3_2558();
void AnonFilter_a3_2559();
void AnonFilter_a3_2560();
void AnonFilter_a3_2561();
void AnonFilter_a3_2562();
void AnonFilter_a3_2563();
void AnonFilter_a3_2564();
void AnonFilter_a3_2565();
void AnonFilter_a3_2566();
void AnonFilter_a3_2567();
void AnonFilter_a3_2568();
void AnonFilter_a3_2569();
void AnonFilter_a3_2570();
void AnonFilter_a3_2571();
void AnonFilter_a3_2572();
void AnonFilter_a3_2573();
void AnonFilter_a3_2574();
void AnonFilter_a3_2575();
void AnonFilter_a3_2576();
void AnonFilter_a3_2577();
void AnonFilter_a3_2578();
void AnonFilter_a3_2579();
void AnonFilter_a3_2580();
void AnonFilter_a3_2581();
void AnonFilter_a3_2582();
void AnonFilter_a3_2583();
void AnonFilter_a3_2584();
void AnonFilter_a3_2585();
void AnonFilter_a3_2586();
void AnonFilter_a3_2587();
void AnonFilter_a3_2588();
void AnonFilter_a3_2589();
void AnonFilter_a3_2590();
void AnonFilter_a3_2591();
void AnonFilter_a3_2592();
void AnonFilter_a3_2593();
void AnonFilter_a3_2594();
void AnonFilter_a3_2595();
void AnonFilter_a3_2596();
void AnonFilter_a3_2597();
void AnonFilter_a3_2598();
void AnonFilter_a3_2599();
void AnonFilter_a3_2600();
void AnonFilter_a3_2601();
void AnonFilter_a3_2602();
void AnonFilter_a3_2603();
void AnonFilter_a3_2604();
void WEIGHTED_ROUND_ROBIN_Joiner_2545();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_2534();
void WEIGHTED_ROUND_ROBIN_Splitter_2605();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_2607();
void iDCT_1D_reference_fine_2608();
void iDCT_1D_reference_fine_2609();
void iDCT_1D_reference_fine_2610();
void iDCT_1D_reference_fine_2611();
void iDCT_1D_reference_fine_2612();
void iDCT_1D_reference_fine_2613();
void iDCT_1D_reference_fine_2614();
void WEIGHTED_ROUND_ROBIN_Joiner_2606();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_2535();
void WEIGHTED_ROUND_ROBIN_Splitter_2615();
void iDCT_1D_reference_fine_2617();
void iDCT_1D_reference_fine_2618();
void iDCT_1D_reference_fine_2619();
void iDCT_1D_reference_fine_2620();
void iDCT_1D_reference_fine_2621();
void iDCT_1D_reference_fine_2622();
void iDCT_1D_reference_fine_2623();
void iDCT_1D_reference_fine_2624();
void WEIGHTED_ROUND_ROBIN_Joiner_2616();
void WEIGHTED_ROUND_ROBIN_Splitter_2625();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_2627();
void AnonFilter_a4_2628();
void AnonFilter_a4_2629();
void AnonFilter_a4_2630();
void AnonFilter_a4_2631();
void AnonFilter_a4_2632();
void AnonFilter_a4_2633();
void AnonFilter_a4_2634();
void AnonFilter_a4_2635();
void AnonFilter_a4_2636();
void AnonFilter_a4_2637();
void AnonFilter_a4_2638();
void AnonFilter_a4_2639();
void AnonFilter_a4_2640();
void AnonFilter_a4_2641();
void AnonFilter_a4_2642();
void AnonFilter_a4_2643();
void AnonFilter_a4_2644();
void AnonFilter_a4_2645();
void AnonFilter_a4_2646();
void AnonFilter_a4_2647();
void AnonFilter_a4_2648();
void AnonFilter_a4_2649();
void AnonFilter_a4_2650();
void AnonFilter_a4_2651();
void AnonFilter_a4_2652();
void AnonFilter_a4_2653();
void AnonFilter_a4_2654();
void AnonFilter_a4_2655();
void AnonFilter_a4_2656();
void AnonFilter_a4_2657();
void AnonFilter_a4_2658();
void AnonFilter_a4_2659();
void AnonFilter_a4_2660();
void AnonFilter_a4_2661();
void AnonFilter_a4_2662();
void AnonFilter_a4_2663();
void AnonFilter_a4_2664();
void AnonFilter_a4_2665();
void AnonFilter_a4_2666();
void AnonFilter_a4_2667();
void AnonFilter_a4_2668();
void AnonFilter_a4_2669();
void AnonFilter_a4_2670();
void AnonFilter_a4_2671();
void AnonFilter_a4_2672();
void AnonFilter_a4_2673();
void AnonFilter_a4_2674();
void AnonFilter_a4_2675();
void AnonFilter_a4_2676();
void AnonFilter_a4_2677();
void AnonFilter_a4_2678();
void AnonFilter_a4_2679();
void AnonFilter_a4_2680();
void AnonFilter_a4_2681();
void AnonFilter_a4_2682();
void AnonFilter_a4_2683();
void AnonFilter_a4_2684();
void AnonFilter_a4_2685();
void WEIGHTED_ROUND_ROBIN_Joiner_2626();
void WEIGHTED_ROUND_ROBIN_Splitter_2686();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_2688();
void iDCT8x8_1D_row_fast_2689();
void iDCT8x8_1D_row_fast_2690();
void iDCT8x8_1D_row_fast_2691();
void iDCT8x8_1D_row_fast_2692();
void iDCT8x8_1D_row_fast_2693();
void iDCT8x8_1D_row_fast_2694();
void iDCT8x8_1D_row_fast_2695();
void WEIGHTED_ROUND_ROBIN_Joiner_2687();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_2488();
void WEIGHTED_ROUND_ROBIN_Joiner_2537();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_2489();

#ifdef __cplusplus
}
#endif
#endif
