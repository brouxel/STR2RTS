#include "PEG51-iDCTcompare_nocache.h"

buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[51];
buffer_int_t SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_join[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5680WEIGHTED_ROUND_ROBIN_Splitter_5689;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5617Pre_CollapsedDataParallel_1_5606;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5670Post_CollapsedDataParallel_2_5607;
buffer_int_t SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_5606WEIGHTED_ROUND_ROBIN_Splitter_5669;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_5609AnonFilter_a2_5561;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560;
buffer_int_t AnonFilter_a0_5533DUPLICATE_Splitter_5608;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_split[3];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[51];
buffer_float_t Post_CollapsedDataParallel_2_5607WEIGHTED_ROUND_ROBIN_Splitter_5679;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[51];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[51];


iDCT_2D_reference_coarse_5536_t iDCT_2D_reference_coarse_5536_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5671_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5672_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5673_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5674_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5675_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5676_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5677_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5678_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5681_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5682_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5683_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5684_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5685_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5686_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5687_s;
iDCT_2D_reference_coarse_5536_t iDCT_1D_reference_fine_5688_s;
iDCT8x8_1D_col_fast_5560_t iDCT8x8_1D_col_fast_5560_s;
AnonFilter_a2_5561_t AnonFilter_a2_5561_s;

void AnonFilter_a0_5533(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_5533DUPLICATE_Splitter_5608, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_5536(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_5536_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_5536_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_5618(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5619(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5620(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5621(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5622(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5623(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5624(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5625(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5626(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5627(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5628(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5629(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5630(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5631(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5632(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5633(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5634(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5635(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5636(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5637(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5638(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5639(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5640(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5641(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5642(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5643(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5644(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5645(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5646(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5647(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5648(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5649(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5650(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5651(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5652(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5653(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5654(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5655(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5656(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5657(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5658(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5659(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5660(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5661(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5662(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5663(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[45])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5664(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[46], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[46])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5665(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[47], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[47])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5666(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[48], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[48])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5667(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[49], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[49])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_5668(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[50], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[50])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5616() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 51, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5617() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 51, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5617Pre_CollapsedDataParallel_1_5606, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_5606(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_5606WEIGHTED_ROUND_ROBIN_Splitter_5669, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5617Pre_CollapsedDataParallel_1_5606, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5617Pre_CollapsedDataParallel_1_5606) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5671(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5671_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5672(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5672_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5673(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5673_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5674(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5674_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5675(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5675_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5676(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5676_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5677(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5677_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5678(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5678_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5669() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5606WEIGHTED_ROUND_ROBIN_Splitter_5669));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5670() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5670Post_CollapsedDataParallel_2_5607, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5607(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_5607WEIGHTED_ROUND_ROBIN_Splitter_5679, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5670Post_CollapsedDataParallel_2_5607, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5670Post_CollapsedDataParallel_2_5607) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5681(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5681_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5682(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5682_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5683(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5683_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5684(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5684_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5685(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5685_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5686(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5686_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5687(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5687_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_5688(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_5688_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5679() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_5607WEIGHTED_ROUND_ROBIN_Splitter_5679));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5680() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5680WEIGHTED_ROUND_ROBIN_Splitter_5689, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_5691(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5692(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5693(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5694(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5695(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5696(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5697(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5698(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5699(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5700(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5701(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5702(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5703(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5704(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5705(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5706(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5707(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5708(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5709(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5710(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5711(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5712(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5713(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5714(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5715(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5716(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5717(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5718(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5719(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5720(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5721(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5722(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5723(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5724(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5725(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5726(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5727(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5728(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5729(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5730(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5731(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5732(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5733(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5734(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5735(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5736(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5737(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[46], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[46]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5738(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[47], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[47]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5739(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[48], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[48]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5740(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[49], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[49]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_5741(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[50], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[50]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5689() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 51, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5680WEIGHTED_ROUND_ROBIN_Splitter_5689));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5690() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 51, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_5744(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0], 6) ; 
		x3 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0], 2) ; 
		x4 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0], 1) ; 
		x5 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0], 7) ; 
		x6 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0], 5) ; 
		x7 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_5745(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1], 6) ; 
		x3 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1], 2) ; 
		x4 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1], 1) ; 
		x5 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1], 7) ; 
		x6 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1], 5) ; 
		x7 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_5746(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2], 6) ; 
		x3 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2], 2) ; 
		x4 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2], 1) ; 
		x5 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2], 7) ; 
		x6 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2], 5) ; 
		x7 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_5747(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3], 6) ; 
		x3 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3], 2) ; 
		x4 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3], 1) ; 
		x5 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3], 7) ; 
		x6 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3], 5) ; 
		x7 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_5748(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4], 6) ; 
		x3 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4], 2) ; 
		x4 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4], 1) ; 
		x5 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4], 7) ; 
		x6 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4], 5) ; 
		x7 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_5749(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5], 6) ; 
		x3 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5], 2) ; 
		x4 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5], 1) ; 
		x5 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5], 7) ; 
		x6 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5], 5) ; 
		x7 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_5750(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6], 6) ; 
		x3 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6], 2) ; 
		x4 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6], 1) ; 
		x5 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6], 7) ; 
		x6 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6], 5) ; 
		x7 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_5751(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7], 6) ; 
		x3 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7], 2) ; 
		x4 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7], 1) ; 
		x5 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7], 7) ; 
		x6 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7], 5) ; 
		x7 = peek_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5742() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5743() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, pop_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_5560(){
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_5560_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_5560_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_5560_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_5560_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_5560_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_5560_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_5560_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_5560_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_5560_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_join[2], iDCT8x8_1D_col_fast_5560_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_5608() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3264, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_5533DUPLICATE_Splitter_5608);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5609() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3264, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_5609AnonFilter_a2_5561, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_5561(){
	FOR(uint32_t, __iter_steady_, 0, <, 3264, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_5609AnonFilter_a2_5561) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_5609AnonFilter_a2_5561) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_5609AnonFilter_a2_5561) ; 
		AnonFilter_a2_5561_s.count = (AnonFilter_a2_5561_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_5561_s.errors = (AnonFilter_a2_5561_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_5561_s.errors / AnonFilter_a2_5561_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_5561_s.errors = (AnonFilter_a2_5561_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_5561_s.errors / AnonFilter_a2_5561_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 51, __iter_init_0_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_5755_5762_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5680WEIGHTED_ROUND_ROBIN_Splitter_5689);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5617Pre_CollapsedDataParallel_1_5606);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_5754_5761_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5670Post_CollapsedDataParallel_2_5607);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin126_iDCT8x8_1D_row_fast_Fiss_5757_5764_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_5606WEIGHTED_ROUND_ROBIN_Splitter_5669);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_5609AnonFilter_a2_5561);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_5743iDCT8x8_1D_col_fast_5560);
	init_buffer_int(&AnonFilter_a0_5533DUPLICATE_Splitter_5608);
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5534_5610_5752_5759_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 51, __iter_init_9_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_5607WEIGHTED_ROUND_ROBIN_Splitter_5679);
	FOR(int, __iter_init_10_, 0, <, 51, __iter_init_10_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_5756_5763_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 51, __iter_init_11_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_5753_5760_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_5536
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_5536_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5671
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5671_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5672
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5672_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5673
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5673_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5674
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5674_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5675
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5675_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5676
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5676_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5677
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5677_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5678
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5678_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5681
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5681_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5682
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5682_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5683
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5683_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5684
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5684_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5685
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5685_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5686
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5686_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5687
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5687_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_5688
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_5688_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_5561
	 {
	AnonFilter_a2_5561_s.count = 0.0 ; 
	AnonFilter_a2_5561_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_5533();
		DUPLICATE_Splitter_5608();
			iDCT_2D_reference_coarse_5536();
			WEIGHTED_ROUND_ROBIN_Splitter_5616();
				AnonFilter_a3_5618();
				AnonFilter_a3_5619();
				AnonFilter_a3_5620();
				AnonFilter_a3_5621();
				AnonFilter_a3_5622();
				AnonFilter_a3_5623();
				AnonFilter_a3_5624();
				AnonFilter_a3_5625();
				AnonFilter_a3_5626();
				AnonFilter_a3_5627();
				AnonFilter_a3_5628();
				AnonFilter_a3_5629();
				AnonFilter_a3_5630();
				AnonFilter_a3_5631();
				AnonFilter_a3_5632();
				AnonFilter_a3_5633();
				AnonFilter_a3_5634();
				AnonFilter_a3_5635();
				AnonFilter_a3_5636();
				AnonFilter_a3_5637();
				AnonFilter_a3_5638();
				AnonFilter_a3_5639();
				AnonFilter_a3_5640();
				AnonFilter_a3_5641();
				AnonFilter_a3_5642();
				AnonFilter_a3_5643();
				AnonFilter_a3_5644();
				AnonFilter_a3_5645();
				AnonFilter_a3_5646();
				AnonFilter_a3_5647();
				AnonFilter_a3_5648();
				AnonFilter_a3_5649();
				AnonFilter_a3_5650();
				AnonFilter_a3_5651();
				AnonFilter_a3_5652();
				AnonFilter_a3_5653();
				AnonFilter_a3_5654();
				AnonFilter_a3_5655();
				AnonFilter_a3_5656();
				AnonFilter_a3_5657();
				AnonFilter_a3_5658();
				AnonFilter_a3_5659();
				AnonFilter_a3_5660();
				AnonFilter_a3_5661();
				AnonFilter_a3_5662();
				AnonFilter_a3_5663();
				AnonFilter_a3_5664();
				AnonFilter_a3_5665();
				AnonFilter_a3_5666();
				AnonFilter_a3_5667();
				AnonFilter_a3_5668();
			WEIGHTED_ROUND_ROBIN_Joiner_5617();
			Pre_CollapsedDataParallel_1_5606();
			WEIGHTED_ROUND_ROBIN_Splitter_5669();
				iDCT_1D_reference_fine_5671();
				iDCT_1D_reference_fine_5672();
				iDCT_1D_reference_fine_5673();
				iDCT_1D_reference_fine_5674();
				iDCT_1D_reference_fine_5675();
				iDCT_1D_reference_fine_5676();
				iDCT_1D_reference_fine_5677();
				iDCT_1D_reference_fine_5678();
			WEIGHTED_ROUND_ROBIN_Joiner_5670();
			Post_CollapsedDataParallel_2_5607();
			WEIGHTED_ROUND_ROBIN_Splitter_5679();
				iDCT_1D_reference_fine_5681();
				iDCT_1D_reference_fine_5682();
				iDCT_1D_reference_fine_5683();
				iDCT_1D_reference_fine_5684();
				iDCT_1D_reference_fine_5685();
				iDCT_1D_reference_fine_5686();
				iDCT_1D_reference_fine_5687();
				iDCT_1D_reference_fine_5688();
			WEIGHTED_ROUND_ROBIN_Joiner_5680();
			WEIGHTED_ROUND_ROBIN_Splitter_5689();
				AnonFilter_a4_5691();
				AnonFilter_a4_5692();
				AnonFilter_a4_5693();
				AnonFilter_a4_5694();
				AnonFilter_a4_5695();
				AnonFilter_a4_5696();
				AnonFilter_a4_5697();
				AnonFilter_a4_5698();
				AnonFilter_a4_5699();
				AnonFilter_a4_5700();
				AnonFilter_a4_5701();
				AnonFilter_a4_5702();
				AnonFilter_a4_5703();
				AnonFilter_a4_5704();
				AnonFilter_a4_5705();
				AnonFilter_a4_5706();
				AnonFilter_a4_5707();
				AnonFilter_a4_5708();
				AnonFilter_a4_5709();
				AnonFilter_a4_5710();
				AnonFilter_a4_5711();
				AnonFilter_a4_5712();
				AnonFilter_a4_5713();
				AnonFilter_a4_5714();
				AnonFilter_a4_5715();
				AnonFilter_a4_5716();
				AnonFilter_a4_5717();
				AnonFilter_a4_5718();
				AnonFilter_a4_5719();
				AnonFilter_a4_5720();
				AnonFilter_a4_5721();
				AnonFilter_a4_5722();
				AnonFilter_a4_5723();
				AnonFilter_a4_5724();
				AnonFilter_a4_5725();
				AnonFilter_a4_5726();
				AnonFilter_a4_5727();
				AnonFilter_a4_5728();
				AnonFilter_a4_5729();
				AnonFilter_a4_5730();
				AnonFilter_a4_5731();
				AnonFilter_a4_5732();
				AnonFilter_a4_5733();
				AnonFilter_a4_5734();
				AnonFilter_a4_5735();
				AnonFilter_a4_5736();
				AnonFilter_a4_5737();
				AnonFilter_a4_5738();
				AnonFilter_a4_5739();
				AnonFilter_a4_5740();
				AnonFilter_a4_5741();
			WEIGHTED_ROUND_ROBIN_Joiner_5690();
			WEIGHTED_ROUND_ROBIN_Splitter_5742();
				iDCT8x8_1D_row_fast_5744();
				iDCT8x8_1D_row_fast_5745();
				iDCT8x8_1D_row_fast_5746();
				iDCT8x8_1D_row_fast_5747();
				iDCT8x8_1D_row_fast_5748();
				iDCT8x8_1D_row_fast_5749();
				iDCT8x8_1D_row_fast_5750();
				iDCT8x8_1D_row_fast_5751();
			WEIGHTED_ROUND_ROBIN_Joiner_5743();
			iDCT8x8_1D_col_fast_5560();
		WEIGHTED_ROUND_ROBIN_Joiner_5609();
		AnonFilter_a2_5561();
	ENDFOR
	return EXIT_SUCCESS;
}
