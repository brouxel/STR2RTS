#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=12672 on the compile command line
#else
#if BUF_SIZEMAX < 12672
#error BUF_SIZEMAX too small, it must be at least 12672
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_11512_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_11536_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_11537_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_11509();
void DUPLICATE_Splitter_11584();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_11512();
void WEIGHTED_ROUND_ROBIN_Splitter_11592();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_11594();
void AnonFilter_a3_11595();
void AnonFilter_a3_11596();
void AnonFilter_a3_11597();
void AnonFilter_a3_11598();
void AnonFilter_a3_11599();
void AnonFilter_a3_11600();
void AnonFilter_a3_11601();
void AnonFilter_a3_11602();
void AnonFilter_a3_11603();
void AnonFilter_a3_11604();
void AnonFilter_a3_11605();
void AnonFilter_a3_11606();
void AnonFilter_a3_11607();
void AnonFilter_a3_11608();
void AnonFilter_a3_11609();
void AnonFilter_a3_11610();
void AnonFilter_a3_11611();
void AnonFilter_a3_11612();
void AnonFilter_a3_11613();
void AnonFilter_a3_11614();
void AnonFilter_a3_11615();
void AnonFilter_a3_11616();
void AnonFilter_a3_11617();
void AnonFilter_a3_11618();
void AnonFilter_a3_11619();
void AnonFilter_a3_11620();
void AnonFilter_a3_11621();
void AnonFilter_a3_11622();
void AnonFilter_a3_11623();
void AnonFilter_a3_11624();
void AnonFilter_a3_11625();
void AnonFilter_a3_11626();
void WEIGHTED_ROUND_ROBIN_Joiner_11593();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_11582();
void WEIGHTED_ROUND_ROBIN_Splitter_11627();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_11629();
void iDCT_1D_reference_fine_11630();
void iDCT_1D_reference_fine_11631();
void iDCT_1D_reference_fine_11632();
void iDCT_1D_reference_fine_11633();
void iDCT_1D_reference_fine_11634();
void iDCT_1D_reference_fine_11635();
void iDCT_1D_reference_fine_11636();
void WEIGHTED_ROUND_ROBIN_Joiner_11628();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_11583();
void WEIGHTED_ROUND_ROBIN_Splitter_11637();
void iDCT_1D_reference_fine_11639();
void iDCT_1D_reference_fine_11640();
void iDCT_1D_reference_fine_11641();
void iDCT_1D_reference_fine_11642();
void iDCT_1D_reference_fine_11643();
void iDCT_1D_reference_fine_11644();
void iDCT_1D_reference_fine_11645();
void iDCT_1D_reference_fine_11646();
void WEIGHTED_ROUND_ROBIN_Joiner_11638();
void WEIGHTED_ROUND_ROBIN_Splitter_11647();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_11649();
void AnonFilter_a4_11650();
void AnonFilter_a4_11651();
void AnonFilter_a4_11652();
void AnonFilter_a4_11653();
void AnonFilter_a4_11654();
void AnonFilter_a4_11655();
void AnonFilter_a4_11656();
void AnonFilter_a4_11657();
void AnonFilter_a4_11658();
void AnonFilter_a4_11659();
void AnonFilter_a4_11660();
void AnonFilter_a4_11661();
void AnonFilter_a4_11662();
void AnonFilter_a4_11663();
void AnonFilter_a4_11664();
void AnonFilter_a4_11665();
void AnonFilter_a4_11666();
void AnonFilter_a4_11667();
void AnonFilter_a4_11668();
void AnonFilter_a4_11669();
void AnonFilter_a4_11670();
void AnonFilter_a4_11671();
void AnonFilter_a4_11672();
void AnonFilter_a4_11673();
void AnonFilter_a4_11674();
void AnonFilter_a4_11675();
void AnonFilter_a4_11676();
void AnonFilter_a4_11677();
void AnonFilter_a4_11678();
void AnonFilter_a4_11679();
void AnonFilter_a4_11680();
void AnonFilter_a4_11681();
void WEIGHTED_ROUND_ROBIN_Joiner_11648();
void WEIGHTED_ROUND_ROBIN_Splitter_11682();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_11684();
void iDCT8x8_1D_row_fast_11685();
void iDCT8x8_1D_row_fast_11686();
void iDCT8x8_1D_row_fast_11687();
void iDCT8x8_1D_row_fast_11688();
void iDCT8x8_1D_row_fast_11689();
void iDCT8x8_1D_row_fast_11690();
void iDCT8x8_1D_row_fast_11691();
void WEIGHTED_ROUND_ROBIN_Joiner_11683();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_11536();
void WEIGHTED_ROUND_ROBIN_Joiner_11585();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_11537();

#ifdef __cplusplus
}
#endif
#endif
