#include "PEG33-iDCTcompare.h"

buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11593Pre_CollapsedDataParallel_1_11582;
buffer_int_t AnonFilter_a0_11509DUPLICATE_Splitter_11584;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_split[3];
buffer_int_t SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[8];
buffer_float_t Post_CollapsedDataParallel_2_11583WEIGHTED_ROUND_ROBIN_Splitter_11637;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11638WEIGHTED_ROUND_ROBIN_Splitter_11647;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_11585AnonFilter_a2_11537;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[33];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[33];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11628Post_CollapsedDataParallel_2_11583;
buffer_int_t SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_11582WEIGHTED_ROUND_ROBIN_Splitter_11627;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_11683iDCT8x8_1D_col_fast_11536;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[33];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[33];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[8];


iDCT_2D_reference_coarse_11512_t iDCT_2D_reference_coarse_11512_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11629_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11630_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11631_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11632_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11633_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11634_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11635_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11636_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11639_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11640_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11641_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11642_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11643_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11644_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11645_s;
iDCT_2D_reference_coarse_11512_t iDCT_1D_reference_fine_11646_s;
iDCT8x8_1D_col_fast_11536_t iDCT8x8_1D_col_fast_11536_s;
AnonFilter_a2_11537_t AnonFilter_a2_11537_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_11509() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_11509DUPLICATE_Splitter_11584));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_11512_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_11512_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_11512() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_11594() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[0]));
	ENDFOR
}

void AnonFilter_a3_11595() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[1]));
	ENDFOR
}

void AnonFilter_a3_11596() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[2]));
	ENDFOR
}

void AnonFilter_a3_11597() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[3]));
	ENDFOR
}

void AnonFilter_a3_11598() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[4]));
	ENDFOR
}

void AnonFilter_a3_11599() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[5]));
	ENDFOR
}

void AnonFilter_a3_11600() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[6]));
	ENDFOR
}

void AnonFilter_a3_11601() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[7]));
	ENDFOR
}

void AnonFilter_a3_11602() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[8]));
	ENDFOR
}

void AnonFilter_a3_11603() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[9]));
	ENDFOR
}

void AnonFilter_a3_11604() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[10]));
	ENDFOR
}

void AnonFilter_a3_11605() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[11]));
	ENDFOR
}

void AnonFilter_a3_11606() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[12]));
	ENDFOR
}

void AnonFilter_a3_11607() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[13]));
	ENDFOR
}

void AnonFilter_a3_11608() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[14]));
	ENDFOR
}

void AnonFilter_a3_11609() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[15]));
	ENDFOR
}

void AnonFilter_a3_11610() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[16]));
	ENDFOR
}

void AnonFilter_a3_11611() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[17]));
	ENDFOR
}

void AnonFilter_a3_11612() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[18]));
	ENDFOR
}

void AnonFilter_a3_11613() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[19]));
	ENDFOR
}

void AnonFilter_a3_11614() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[20]));
	ENDFOR
}

void AnonFilter_a3_11615() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[21]));
	ENDFOR
}

void AnonFilter_a3_11616() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[22]));
	ENDFOR
}

void AnonFilter_a3_11617() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[23]));
	ENDFOR
}

void AnonFilter_a3_11618() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[24]));
	ENDFOR
}

void AnonFilter_a3_11619() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[25]));
	ENDFOR
}

void AnonFilter_a3_11620() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[26]));
	ENDFOR
}

void AnonFilter_a3_11621() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[27]));
	ENDFOR
}

void AnonFilter_a3_11622() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[28]));
	ENDFOR
}

void AnonFilter_a3_11623() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[29]));
	ENDFOR
}

void AnonFilter_a3_11624() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[30]));
	ENDFOR
}

void AnonFilter_a3_11625() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[31]));
	ENDFOR
}

void AnonFilter_a3_11626() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[32]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11592() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 33, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11593() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 33, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11593Pre_CollapsedDataParallel_1_11582, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_11582() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_11593Pre_CollapsedDataParallel_1_11582), &(Pre_CollapsedDataParallel_1_11582WEIGHTED_ROUND_ROBIN_Splitter_11627));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_11629_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_11629() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_11630() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_11631() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_11632() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_11633() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_11634() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_11635() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_11636() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11627() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11582WEIGHTED_ROUND_ROBIN_Splitter_11627));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11628() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11628Post_CollapsedDataParallel_2_11583, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_11583() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11628Post_CollapsedDataParallel_2_11583), &(Post_CollapsedDataParallel_2_11583WEIGHTED_ROUND_ROBIN_Splitter_11637));
	ENDFOR
}

void iDCT_1D_reference_fine_11639() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_11640() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_11641() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_11642() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_11643() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_11644() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_11645() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_11646() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11637() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_11583WEIGHTED_ROUND_ROBIN_Splitter_11637));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11638() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11638WEIGHTED_ROUND_ROBIN_Splitter_11647, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_11649() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[0]));
	ENDFOR
}

void AnonFilter_a4_11650() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[1]));
	ENDFOR
}

void AnonFilter_a4_11651() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[2]));
	ENDFOR
}

void AnonFilter_a4_11652() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[3]));
	ENDFOR
}

void AnonFilter_a4_11653() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[4]));
	ENDFOR
}

void AnonFilter_a4_11654() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[5]));
	ENDFOR
}

void AnonFilter_a4_11655() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[6]));
	ENDFOR
}

void AnonFilter_a4_11656() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[7]));
	ENDFOR
}

void AnonFilter_a4_11657() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[8]));
	ENDFOR
}

void AnonFilter_a4_11658() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[9]));
	ENDFOR
}

void AnonFilter_a4_11659() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[10]));
	ENDFOR
}

void AnonFilter_a4_11660() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[11]));
	ENDFOR
}

void AnonFilter_a4_11661() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[12]));
	ENDFOR
}

void AnonFilter_a4_11662() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[13]));
	ENDFOR
}

void AnonFilter_a4_11663() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[14]));
	ENDFOR
}

void AnonFilter_a4_11664() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[15]));
	ENDFOR
}

void AnonFilter_a4_11665() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[16]));
	ENDFOR
}

void AnonFilter_a4_11666() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[17]));
	ENDFOR
}

void AnonFilter_a4_11667() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[18]));
	ENDFOR
}

void AnonFilter_a4_11668() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[19]));
	ENDFOR
}

void AnonFilter_a4_11669() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[20]));
	ENDFOR
}

void AnonFilter_a4_11670() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[21]));
	ENDFOR
}

void AnonFilter_a4_11671() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[22]));
	ENDFOR
}

void AnonFilter_a4_11672() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[23]));
	ENDFOR
}

void AnonFilter_a4_11673() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[24]));
	ENDFOR
}

void AnonFilter_a4_11674() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[25]));
	ENDFOR
}

void AnonFilter_a4_11675() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[26]));
	ENDFOR
}

void AnonFilter_a4_11676() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[27]));
	ENDFOR
}

void AnonFilter_a4_11677() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[28]));
	ENDFOR
}

void AnonFilter_a4_11678() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[29]));
	ENDFOR
}

void AnonFilter_a4_11679() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[30]));
	ENDFOR
}

void AnonFilter_a4_11680() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[31]));
	ENDFOR
}

void AnonFilter_a4_11681() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[32]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11647() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 33, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11638WEIGHTED_ROUND_ROBIN_Splitter_11647));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11648() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 33, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_11684() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[0]), &(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11685() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[1]), &(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11686() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[2]), &(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11687() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[3]), &(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11688() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[4]), &(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11689() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[5]), &(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11690() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[6]), &(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11691() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[7]), &(SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11682() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11683() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_11683iDCT8x8_1D_col_fast_11536, pop_int(&SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_11536_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_11536_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_11536_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_11536_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_11536_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_11536_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_11536_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_11536_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_11536_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_11536_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_11536() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_11683iDCT8x8_1D_col_fast_11536), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_11584() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2112, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_11509DUPLICATE_Splitter_11584);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11585() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2112, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_11585AnonFilter_a2_11537, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_11537_s.count = (AnonFilter_a2_11537_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_11537_s.errors = (AnonFilter_a2_11537_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_11537_s.errors / AnonFilter_a2_11537_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_11537_s.errors = (AnonFilter_a2_11537_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_11537_s.errors / AnonFilter_a2_11537_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_11537() {
	FOR(uint32_t, __iter_steady_, 0, <, 2112, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_11585AnonFilter_a2_11537));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11593Pre_CollapsedDataParallel_1_11582);
	init_buffer_int(&AnonFilter_a0_11509DUPLICATE_Splitter_11584);
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_11583WEIGHTED_ROUND_ROBIN_Splitter_11637);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11638WEIGHTED_ROUND_ROBIN_Splitter_11647);
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11510_11586_11692_11699_join[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_11585AnonFilter_a2_11537);
	FOR(int, __iter_init_4_, 0, <, 33, __iter_init_4_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_11696_11703_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 33, __iter_init_6_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_11693_11700_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11628Post_CollapsedDataParallel_2_11583);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin90_iDCT8x8_1D_row_fast_Fiss_11697_11704_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11582WEIGHTED_ROUND_ROBIN_Splitter_11627);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11695_11702_join[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_11683iDCT8x8_1D_col_fast_11536);
	FOR(int, __iter_init_9_, 0, <, 33, __iter_init_9_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_11696_11703_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 33, __iter_init_10_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_11693_11700_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11694_11701_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_11512
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_11512_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11629
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11629_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11630
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11630_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11631
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11631_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11632
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11632_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11633
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11633_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11634
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11634_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11635
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11635_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11636
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11636_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11639
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11639_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11640
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11640_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11641
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11641_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11642
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11642_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11643
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11643_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11644
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11644_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11645
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11645_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11646
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11646_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_11537
	 {
	AnonFilter_a2_11537_s.count = 0.0 ; 
	AnonFilter_a2_11537_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_11509();
		DUPLICATE_Splitter_11584();
			iDCT_2D_reference_coarse_11512();
			WEIGHTED_ROUND_ROBIN_Splitter_11592();
				AnonFilter_a3_11594();
				AnonFilter_a3_11595();
				AnonFilter_a3_11596();
				AnonFilter_a3_11597();
				AnonFilter_a3_11598();
				AnonFilter_a3_11599();
				AnonFilter_a3_11600();
				AnonFilter_a3_11601();
				AnonFilter_a3_11602();
				AnonFilter_a3_11603();
				AnonFilter_a3_11604();
				AnonFilter_a3_11605();
				AnonFilter_a3_11606();
				AnonFilter_a3_11607();
				AnonFilter_a3_11608();
				AnonFilter_a3_11609();
				AnonFilter_a3_11610();
				AnonFilter_a3_11611();
				AnonFilter_a3_11612();
				AnonFilter_a3_11613();
				AnonFilter_a3_11614();
				AnonFilter_a3_11615();
				AnonFilter_a3_11616();
				AnonFilter_a3_11617();
				AnonFilter_a3_11618();
				AnonFilter_a3_11619();
				AnonFilter_a3_11620();
				AnonFilter_a3_11621();
				AnonFilter_a3_11622();
				AnonFilter_a3_11623();
				AnonFilter_a3_11624();
				AnonFilter_a3_11625();
				AnonFilter_a3_11626();
			WEIGHTED_ROUND_ROBIN_Joiner_11593();
			Pre_CollapsedDataParallel_1_11582();
			WEIGHTED_ROUND_ROBIN_Splitter_11627();
				iDCT_1D_reference_fine_11629();
				iDCT_1D_reference_fine_11630();
				iDCT_1D_reference_fine_11631();
				iDCT_1D_reference_fine_11632();
				iDCT_1D_reference_fine_11633();
				iDCT_1D_reference_fine_11634();
				iDCT_1D_reference_fine_11635();
				iDCT_1D_reference_fine_11636();
			WEIGHTED_ROUND_ROBIN_Joiner_11628();
			Post_CollapsedDataParallel_2_11583();
			WEIGHTED_ROUND_ROBIN_Splitter_11637();
				iDCT_1D_reference_fine_11639();
				iDCT_1D_reference_fine_11640();
				iDCT_1D_reference_fine_11641();
				iDCT_1D_reference_fine_11642();
				iDCT_1D_reference_fine_11643();
				iDCT_1D_reference_fine_11644();
				iDCT_1D_reference_fine_11645();
				iDCT_1D_reference_fine_11646();
			WEIGHTED_ROUND_ROBIN_Joiner_11638();
			WEIGHTED_ROUND_ROBIN_Splitter_11647();
				AnonFilter_a4_11649();
				AnonFilter_a4_11650();
				AnonFilter_a4_11651();
				AnonFilter_a4_11652();
				AnonFilter_a4_11653();
				AnonFilter_a4_11654();
				AnonFilter_a4_11655();
				AnonFilter_a4_11656();
				AnonFilter_a4_11657();
				AnonFilter_a4_11658();
				AnonFilter_a4_11659();
				AnonFilter_a4_11660();
				AnonFilter_a4_11661();
				AnonFilter_a4_11662();
				AnonFilter_a4_11663();
				AnonFilter_a4_11664();
				AnonFilter_a4_11665();
				AnonFilter_a4_11666();
				AnonFilter_a4_11667();
				AnonFilter_a4_11668();
				AnonFilter_a4_11669();
				AnonFilter_a4_11670();
				AnonFilter_a4_11671();
				AnonFilter_a4_11672();
				AnonFilter_a4_11673();
				AnonFilter_a4_11674();
				AnonFilter_a4_11675();
				AnonFilter_a4_11676();
				AnonFilter_a4_11677();
				AnonFilter_a4_11678();
				AnonFilter_a4_11679();
				AnonFilter_a4_11680();
				AnonFilter_a4_11681();
			WEIGHTED_ROUND_ROBIN_Joiner_11648();
			WEIGHTED_ROUND_ROBIN_Splitter_11682();
				iDCT8x8_1D_row_fast_11684();
				iDCT8x8_1D_row_fast_11685();
				iDCT8x8_1D_row_fast_11686();
				iDCT8x8_1D_row_fast_11687();
				iDCT8x8_1D_row_fast_11688();
				iDCT8x8_1D_row_fast_11689();
				iDCT8x8_1D_row_fast_11690();
				iDCT8x8_1D_row_fast_11691();
			WEIGHTED_ROUND_ROBIN_Joiner_11683();
			iDCT8x8_1D_col_fast_11536();
		WEIGHTED_ROUND_ROBIN_Joiner_11585();
		AnonFilter_a2_11537();
	ENDFOR
	return EXIT_SUCCESS;
}
