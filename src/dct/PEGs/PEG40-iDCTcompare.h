#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1920 on the compile command line
#else
#if BUF_SIZEMAX < 1920
#error BUF_SIZEMAX too small, it must be at least 1920
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_9342_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_9366_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_9367_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_9339();
void DUPLICATE_Splitter_9414();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_9342();
void WEIGHTED_ROUND_ROBIN_Splitter_9422();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_9424();
void AnonFilter_a3_9425();
void AnonFilter_a3_9426();
void AnonFilter_a3_9427();
void AnonFilter_a3_9428();
void AnonFilter_a3_9429();
void AnonFilter_a3_9430();
void AnonFilter_a3_9431();
void AnonFilter_a3_9432();
void AnonFilter_a3_9433();
void AnonFilter_a3_9434();
void AnonFilter_a3_9435();
void AnonFilter_a3_9436();
void AnonFilter_a3_9437();
void AnonFilter_a3_9438();
void AnonFilter_a3_9439();
void AnonFilter_a3_9440();
void AnonFilter_a3_9441();
void AnonFilter_a3_9442();
void AnonFilter_a3_9443();
void AnonFilter_a3_9444();
void AnonFilter_a3_9445();
void AnonFilter_a3_9446();
void AnonFilter_a3_9447();
void AnonFilter_a3_9448();
void AnonFilter_a3_9449();
void AnonFilter_a3_9450();
void AnonFilter_a3_9451();
void AnonFilter_a3_9452();
void AnonFilter_a3_9453();
void AnonFilter_a3_9454();
void AnonFilter_a3_9455();
void AnonFilter_a3_9456();
void AnonFilter_a3_9457();
void AnonFilter_a3_9458();
void AnonFilter_a3_9459();
void AnonFilter_a3_9460();
void AnonFilter_a3_9461();
void AnonFilter_a3_9462();
void AnonFilter_a3_9463();
void WEIGHTED_ROUND_ROBIN_Joiner_9423();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_9412();
void WEIGHTED_ROUND_ROBIN_Splitter_9464();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_9466();
void iDCT_1D_reference_fine_9467();
void iDCT_1D_reference_fine_9468();
void iDCT_1D_reference_fine_9469();
void iDCT_1D_reference_fine_9470();
void iDCT_1D_reference_fine_9471();
void iDCT_1D_reference_fine_9472();
void iDCT_1D_reference_fine_9473();
void WEIGHTED_ROUND_ROBIN_Joiner_9465();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_9413();
void WEIGHTED_ROUND_ROBIN_Splitter_9474();
void iDCT_1D_reference_fine_9476();
void iDCT_1D_reference_fine_9477();
void iDCT_1D_reference_fine_9478();
void iDCT_1D_reference_fine_9479();
void iDCT_1D_reference_fine_9480();
void iDCT_1D_reference_fine_9481();
void iDCT_1D_reference_fine_9482();
void iDCT_1D_reference_fine_9483();
void WEIGHTED_ROUND_ROBIN_Joiner_9475();
void WEIGHTED_ROUND_ROBIN_Splitter_9484();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_9486();
void AnonFilter_a4_9487();
void AnonFilter_a4_9488();
void AnonFilter_a4_9489();
void AnonFilter_a4_9490();
void AnonFilter_a4_9491();
void AnonFilter_a4_9492();
void AnonFilter_a4_9493();
void AnonFilter_a4_9494();
void AnonFilter_a4_9495();
void AnonFilter_a4_9496();
void AnonFilter_a4_9497();
void AnonFilter_a4_9498();
void AnonFilter_a4_9499();
void AnonFilter_a4_9500();
void AnonFilter_a4_9501();
void AnonFilter_a4_9502();
void AnonFilter_a4_9503();
void AnonFilter_a4_9504();
void AnonFilter_a4_9505();
void AnonFilter_a4_9506();
void AnonFilter_a4_9507();
void AnonFilter_a4_9508();
void AnonFilter_a4_9509();
void AnonFilter_a4_9510();
void AnonFilter_a4_9511();
void AnonFilter_a4_9512();
void AnonFilter_a4_9513();
void AnonFilter_a4_9514();
void AnonFilter_a4_9515();
void AnonFilter_a4_9516();
void AnonFilter_a4_9517();
void AnonFilter_a4_9518();
void AnonFilter_a4_9519();
void AnonFilter_a4_9520();
void AnonFilter_a4_9521();
void AnonFilter_a4_9522();
void AnonFilter_a4_9523();
void AnonFilter_a4_9524();
void AnonFilter_a4_9525();
void WEIGHTED_ROUND_ROBIN_Joiner_9485();
void WEIGHTED_ROUND_ROBIN_Splitter_9526();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_9528();
void iDCT8x8_1D_row_fast_9529();
void iDCT8x8_1D_row_fast_9530();
void iDCT8x8_1D_row_fast_9531();
void iDCT8x8_1D_row_fast_9532();
void iDCT8x8_1D_row_fast_9533();
void iDCT8x8_1D_row_fast_9534();
void iDCT8x8_1D_row_fast_9535();
void WEIGHTED_ROUND_ROBIN_Joiner_9527();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_9366();
void WEIGHTED_ROUND_ROBIN_Joiner_9415();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_9367();

#ifdef __cplusplus
}
#endif
#endif
