#include "PEG16-iDCTcompare.h"

buffer_int_t SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[16];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[16];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_split[3];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16075WEIGHTED_ROUND_ROBIN_Splitter_16084;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16039AnonFilter_a2_15991;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16047Pre_CollapsedDataParallel_1_16036;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[16];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[8];
buffer_int_t SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_join[3];
buffer_int_t AnonFilter_a0_15963DUPLICATE_Splitter_16038;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16065Post_CollapsedDataParallel_2_16037;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16103iDCT8x8_1D_col_fast_15990;
buffer_float_t Post_CollapsedDataParallel_2_16037WEIGHTED_ROUND_ROBIN_Splitter_16074;
buffer_float_t Pre_CollapsedDataParallel_1_16036WEIGHTED_ROUND_ROBIN_Splitter_16064;


iDCT_2D_reference_coarse_15966_t iDCT_2D_reference_coarse_15966_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16066_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16067_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16068_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16069_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16070_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16071_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16072_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16073_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16076_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16077_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16078_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16079_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16080_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16081_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16082_s;
iDCT_2D_reference_coarse_15966_t iDCT_1D_reference_fine_16083_s;
iDCT8x8_1D_col_fast_15990_t iDCT8x8_1D_col_fast_15990_s;
AnonFilter_a2_15991_t AnonFilter_a2_15991_s;

void AnonFilter_a0(buffer_int_t *chanout) {
	FOR(int, i, 0,  < , 64, i++) {
		push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
	}
	ENDFOR
}


void AnonFilter_a0_15963() {
	AnonFilter_a0(&(AnonFilter_a0_15963DUPLICATE_Splitter_16038));
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
	float block_x[8][8];
	FOR(int, i, 0,  < , 8, i++) {
		FOR(int, j, 0,  < , 8, j++) {
			block_x[i][j] = 0.0 ; 
			FOR(int, k, 0,  < , 8, k++) {
				block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_15966_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
			}
			ENDFOR
		}
		ENDFOR
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		FOR(int, j, 0,  < , 8, j++) {
			float block_y = 0.0;
			FOR(int, k, 0,  < , 8, k++) {
				block_y = (block_y + (iDCT_2D_reference_coarse_15966_s.coeff[k][i] * block_x[k][j])) ; 
			}
			ENDFOR
			block_y = ((float) floor((block_y + 0.5))) ; 
			push_int(&(*chanout), ((int) block_y)) ; 
		}
		ENDFOR
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_int(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT_2D_reference_coarse_15966() {
	iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_join[0]));
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_16048() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[0]));
	ENDFOR
}

void AnonFilter_a3_16049() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[1]));
	ENDFOR
}

void AnonFilter_a3_16050() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[2]));
	ENDFOR
}

void AnonFilter_a3_16051() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[3]));
	ENDFOR
}

void AnonFilter_a3_16052() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[4]));
	ENDFOR
}

void AnonFilter_a3_16053() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[5]));
	ENDFOR
}

void AnonFilter_a3_16054() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[6]));
	ENDFOR
}

void AnonFilter_a3_16055() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[7]));
	ENDFOR
}

void AnonFilter_a3_16056() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[8]));
	ENDFOR
}

void AnonFilter_a3_16057() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[9]));
	ENDFOR
}

void AnonFilter_a3_16058() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[10]));
	ENDFOR
}

void AnonFilter_a3_16059() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[11]));
	ENDFOR
}

void AnonFilter_a3_16060() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[12]));
	ENDFOR
}

void AnonFilter_a3_16061() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[13]));
	ENDFOR
}

void AnonFilter_a3_16062() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[14]));
	ENDFOR
}

void AnonFilter_a3_16063() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16046() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16047() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16047Pre_CollapsedDataParallel_1_16036, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
		FOR(int, _i, 0,  < , 8, _i++) {
			push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}
}
	pop_float(&(*chanin)) ; 
}


void Pre_CollapsedDataParallel_1_16036() {
	Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_16047Pre_CollapsedDataParallel_1_16036), &(Pre_CollapsedDataParallel_1_16036WEIGHTED_ROUND_ROBIN_Splitter_16064));
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
	FOR(int, x, 0,  < , 8, x++) {
		float tempsum = 0.0;
		FOR(int, u, 0,  < , 8, u++) {
			tempsum = (tempsum + (iDCT_1D_reference_fine_16066_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
		}
		ENDFOR
		push_float(&(*chanout), tempsum) ; 
	}
	ENDFOR
	FOR(int, u, 0,  < , 8, u++) {
		pop_float(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT_1D_reference_fine_16066() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[0]));
}

void iDCT_1D_reference_fine_16067() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[1]));
}

void iDCT_1D_reference_fine_16068() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[2]));
}

void iDCT_1D_reference_fine_16069() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[3]));
}

void iDCT_1D_reference_fine_16070() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[4]));
}

void iDCT_1D_reference_fine_16071() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[5]));
}

void iDCT_1D_reference_fine_16072() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[6]));
}

void iDCT_1D_reference_fine_16073() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_16064() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_16036WEIGHTED_ROUND_ROBIN_Splitter_16064));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_16065() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16065Post_CollapsedDataParallel_2_16037, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 8, _i++) {
			push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
			partialSum_i = (partialSum_i + 8) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}
}
	pop_float(&(*chanin)) ; 
}


void Post_CollapsedDataParallel_2_16037() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_16065Post_CollapsedDataParallel_2_16037), &(Post_CollapsedDataParallel_2_16037WEIGHTED_ROUND_ROBIN_Splitter_16074));
}

void iDCT_1D_reference_fine_16076() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[0]));
}

void iDCT_1D_reference_fine_16077() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[1]));
}

void iDCT_1D_reference_fine_16078() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[2]));
}

void iDCT_1D_reference_fine_16079() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[3]));
}

void iDCT_1D_reference_fine_16080() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[4]));
}

void iDCT_1D_reference_fine_16081() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[5]));
}

void iDCT_1D_reference_fine_16082() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[6]));
}

void iDCT_1D_reference_fine_16083() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_16074() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_16037WEIGHTED_ROUND_ROBIN_Splitter_16074));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_16075() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16075WEIGHTED_ROUND_ROBIN_Splitter_16084, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_16086() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[0]));
	ENDFOR
}

void AnonFilter_a4_16087() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[1]));
	ENDFOR
}

void AnonFilter_a4_16088() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[2]));
	ENDFOR
}

void AnonFilter_a4_16089() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[3]));
	ENDFOR
}

void AnonFilter_a4_16090() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[4]));
	ENDFOR
}

void AnonFilter_a4_16091() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[5]));
	ENDFOR
}

void AnonFilter_a4_16092() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[6]));
	ENDFOR
}

void AnonFilter_a4_16093() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[7]));
	ENDFOR
}

void AnonFilter_a4_16094() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[8]));
	ENDFOR
}

void AnonFilter_a4_16095() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[9]));
	ENDFOR
}

void AnonFilter_a4_16096() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[10]));
	ENDFOR
}

void AnonFilter_a4_16097() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[11]));
	ENDFOR
}

void AnonFilter_a4_16098() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[12]));
	ENDFOR
}

void AnonFilter_a4_16099() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[13]));
	ENDFOR
}

void AnonFilter_a4_16100() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[14]));
	ENDFOR
}

void AnonFilter_a4_16101() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16084() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16075WEIGHTED_ROUND_ROBIN_Splitter_16084));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16085() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
	int x0 = 0;
	int x1 = 0;
	int x2 = 0;
	int x3 = 0;
	int x4 = 0;
	int x5 = 0;
	int x6 = 0;
	int x7 = 0;
	int x8 = 0;
	x0 = peek_int(&(*chanin), 0) ; 
	x1 = (peek_int(&(*chanin), 4) << 11) ; 
	x2 = peek_int(&(*chanin), 6) ; 
	x3 = peek_int(&(*chanin), 2) ; 
	x4 = peek_int(&(*chanin), 1) ; 
	x5 = peek_int(&(*chanin), 7) ; 
	x6 = peek_int(&(*chanin), 5) ; 
	x7 = peek_int(&(*chanin), 3) ; 
	if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
		x0 = (x0 << 3) ; 
		FOR(int, i, 0,  < , 8, i++) {
			push_int(&(*chanout), x0) ; 
		}
		ENDFOR
	}
	else {
		x0 = ((x0 << 11) + 128) ; 
		x8 = (565 * (x4 + x5)) ; 
		x4 = (x8 + (2276 * x4)) ; 
		x5 = (x8 - (3406 * x5)) ; 
		x8 = (2408 * (x6 + x7)) ; 
		x6 = (x8 - (799 * x6)) ; 
		x7 = (x8 - (4017 * x7)) ; 
		x8 = (x0 + x1) ; 
		x0 = (x0 - x1) ; 
		x1 = (1108 * (x3 + x2)) ; 
		x2 = (x1 - (3784 * x2)) ; 
		x3 = (x1 + (1568 * x3)) ; 
		x1 = (x4 + x6) ; 
		x4 = (x4 - x6) ; 
		x6 = (x5 + x7) ; 
		x5 = (x5 - x7) ; 
		x7 = (x8 + x3) ; 
		x8 = (x8 - x3) ; 
		x3 = (x0 + x2) ; 
		x0 = (x0 - x2) ; 
		x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
		x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
		push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
		push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
		push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
		push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
		push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
		push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
		push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
		push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
	}
	FOR(int, i, 0,  < , 8, i++) {
		pop_int(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT8x8_1D_row_fast_16104() {
	iDCT8x8_1D_row_fast(&(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[0]), &(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[0]));
}

void iDCT8x8_1D_row_fast_16105() {
	iDCT8x8_1D_row_fast(&(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[1]), &(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[1]));
}

void iDCT8x8_1D_row_fast_16106() {
	iDCT8x8_1D_row_fast(&(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[2]), &(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[2]));
}

void iDCT8x8_1D_row_fast_16107() {
	iDCT8x8_1D_row_fast(&(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[3]), &(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[3]));
}

void iDCT8x8_1D_row_fast_16108() {
	iDCT8x8_1D_row_fast(&(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[4]), &(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[4]));
}

void iDCT8x8_1D_row_fast_16109() {
	iDCT8x8_1D_row_fast(&(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[5]), &(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[5]));
}

void iDCT8x8_1D_row_fast_16110() {
	iDCT8x8_1D_row_fast(&(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[6]), &(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[6]));
}

void iDCT8x8_1D_row_fast_16111() {
	iDCT8x8_1D_row_fast(&(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[7]), &(SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_16102() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_int(&SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_split[2]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_16103() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16103iDCT8x8_1D_col_fast_15990, pop_int(&SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
	FOR(int, c, 0,  < , 8, c++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), (c + 0)) ; 
		x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
		x2 = peek_int(&(*chanin), (c + 48)) ; 
		x3 = peek_int(&(*chanin), (c + 16)) ; 
		x4 = peek_int(&(*chanin), (c + 8)) ; 
		x5 = peek_int(&(*chanin), (c + 56)) ; 
		x6 = peek_int(&(*chanin), (c + 40)) ; 
		x7 = peek_int(&(*chanin), (c + 24)) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = ((x0 + 32) >> 6) ; 
			FOR(int, i, 0,  < , 8, i++) {
				iDCT8x8_1D_col_fast_15990_s.buffer[(c + (8 * i))] = x0 ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 8) + 8192) ; 
			x8 = ((565 * (x4 + x5)) + 4) ; 
			x4 = ((x8 + (2276 * x4)) >> 3) ; 
			x5 = ((x8 - (3406 * x5)) >> 3) ; 
			x8 = ((2408 * (x6 + x7)) + 4) ; 
			x6 = ((x8 - (799 * x6)) >> 3) ; 
			x7 = ((x8 - (4017 * x7)) >> 3) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = ((1108 * (x3 + x2)) + 4) ; 
			x2 = ((x1 - (3784 * x2)) >> 3) ; 
			x3 = ((x1 + (1568 * x3)) >> 3) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			iDCT8x8_1D_col_fast_15990_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
			iDCT8x8_1D_col_fast_15990_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
			iDCT8x8_1D_col_fast_15990_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
			iDCT8x8_1D_col_fast_15990_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
			iDCT8x8_1D_col_fast_15990_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
			iDCT8x8_1D_col_fast_15990_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
			iDCT8x8_1D_col_fast_15990_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
			iDCT8x8_1D_col_fast_15990_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
		}
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_int(&(*chanin)) ; 
		push_int(&(*chanout), iDCT8x8_1D_col_fast_15990_s.buffer[i]) ; 
	}
	ENDFOR
}


void iDCT8x8_1D_col_fast_15990() {
	iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_16103iDCT8x8_1D_col_fast_15990), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_join[2]));
}

void DUPLICATE_Splitter_16038() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_15963DUPLICATE_Splitter_16038);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16039AnonFilter_a2_15991, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_15991_s.count = (AnonFilter_a2_15991_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_15991_s.errors = (AnonFilter_a2_15991_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15991_s.errors / AnonFilter_a2_15991_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_15991_s.errors = (AnonFilter_a2_15991_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15991_s.errors / AnonFilter_a2_15991_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_15991() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_16039AnonFilter_a2_15991));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 16, __iter_init_1_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_16116_16123_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 16, __iter_init_3_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_16113_16120_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16115_16122_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_16116_16123_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16075WEIGHTED_ROUND_ROBIN_Splitter_16084);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16039AnonFilter_a2_15991);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16047Pre_CollapsedDataParallel_1_16036);
	FOR(int, __iter_init_8_, 0, <, 16, __iter_init_8_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_16113_16120_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16114_16121_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_int(&SplitJoin56_iDCT8x8_1D_row_fast_Fiss_16117_16124_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15964_16040_16112_16119_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_15963DUPLICATE_Splitter_16038);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16065Post_CollapsedDataParallel_2_16037);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16103iDCT8x8_1D_col_fast_15990);
	init_buffer_float(&Post_CollapsedDataParallel_2_16037WEIGHTED_ROUND_ROBIN_Splitter_16074);
	init_buffer_float(&Pre_CollapsedDataParallel_1_16036WEIGHTED_ROUND_ROBIN_Splitter_16064);
// --- init: iDCT_2D_reference_coarse_15966
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_15966_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16066
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16066_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16067
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16067_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16068
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16068_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16069
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16069_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16070
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16070_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16071
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16071_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16072
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16072_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16073
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16073_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16076
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16076_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16077
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16077_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16078
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16078_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16079
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16079_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16080
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16080_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16081
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16081_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16082
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16082_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16083
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16083_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_15991
	 {
	AnonFilter_a2_15991_s.count = 0.0 ; 
	AnonFilter_a2_15991_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_15963();
		DUPLICATE_Splitter_16038();
			iDCT_2D_reference_coarse_15966();
			WEIGHTED_ROUND_ROBIN_Splitter_16046();
				AnonFilter_a3_16048();
				AnonFilter_a3_16049();
				AnonFilter_a3_16050();
				AnonFilter_a3_16051();
				AnonFilter_a3_16052();
				AnonFilter_a3_16053();
				AnonFilter_a3_16054();
				AnonFilter_a3_16055();
				AnonFilter_a3_16056();
				AnonFilter_a3_16057();
				AnonFilter_a3_16058();
				AnonFilter_a3_16059();
				AnonFilter_a3_16060();
				AnonFilter_a3_16061();
				AnonFilter_a3_16062();
				AnonFilter_a3_16063();
			WEIGHTED_ROUND_ROBIN_Joiner_16047();
			Pre_CollapsedDataParallel_1_16036();
			WEIGHTED_ROUND_ROBIN_Splitter_16064();
				iDCT_1D_reference_fine_16066();
				iDCT_1D_reference_fine_16067();
				iDCT_1D_reference_fine_16068();
				iDCT_1D_reference_fine_16069();
				iDCT_1D_reference_fine_16070();
				iDCT_1D_reference_fine_16071();
				iDCT_1D_reference_fine_16072();
				iDCT_1D_reference_fine_16073();
			WEIGHTED_ROUND_ROBIN_Joiner_16065();
			Post_CollapsedDataParallel_2_16037();
			WEIGHTED_ROUND_ROBIN_Splitter_16074();
				iDCT_1D_reference_fine_16076();
				iDCT_1D_reference_fine_16077();
				iDCT_1D_reference_fine_16078();
				iDCT_1D_reference_fine_16079();
				iDCT_1D_reference_fine_16080();
				iDCT_1D_reference_fine_16081();
				iDCT_1D_reference_fine_16082();
				iDCT_1D_reference_fine_16083();
			WEIGHTED_ROUND_ROBIN_Joiner_16075();
			WEIGHTED_ROUND_ROBIN_Splitter_16084();
				AnonFilter_a4_16086();
				AnonFilter_a4_16087();
				AnonFilter_a4_16088();
				AnonFilter_a4_16089();
				AnonFilter_a4_16090();
				AnonFilter_a4_16091();
				AnonFilter_a4_16092();
				AnonFilter_a4_16093();
				AnonFilter_a4_16094();
				AnonFilter_a4_16095();
				AnonFilter_a4_16096();
				AnonFilter_a4_16097();
				AnonFilter_a4_16098();
				AnonFilter_a4_16099();
				AnonFilter_a4_16100();
				AnonFilter_a4_16101();
			WEIGHTED_ROUND_ROBIN_Joiner_16085();
			WEIGHTED_ROUND_ROBIN_Splitter_16102();
				iDCT8x8_1D_row_fast_16104();
				iDCT8x8_1D_row_fast_16105();
				iDCT8x8_1D_row_fast_16106();
				iDCT8x8_1D_row_fast_16107();
				iDCT8x8_1D_row_fast_16108();
				iDCT8x8_1D_row_fast_16109();
				iDCT8x8_1D_row_fast_16110();
				iDCT8x8_1D_row_fast_16111();
			WEIGHTED_ROUND_ROBIN_Joiner_16103();
			iDCT8x8_1D_col_fast_15990();
		WEIGHTED_ROUND_ROBIN_Joiner_16039();
		AnonFilter_a2_15991();
	ENDFOR
	return EXIT_SUCCESS;
}
