#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=16512 on the compile command line
#else
#if BUF_SIZEMAX < 16512
#error BUF_SIZEMAX too small, it must be at least 16512
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_8352_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_8376_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_8377_t;
void AnonFilter_a0_8349();
void DUPLICATE_Splitter_8424();
void iDCT_2D_reference_coarse_8352();
void WEIGHTED_ROUND_ROBIN_Splitter_8432();
void AnonFilter_a3_8434();
void AnonFilter_a3_8435();
void AnonFilter_a3_8436();
void AnonFilter_a3_8437();
void AnonFilter_a3_8438();
void AnonFilter_a3_8439();
void AnonFilter_a3_8440();
void AnonFilter_a3_8441();
void AnonFilter_a3_8442();
void AnonFilter_a3_8443();
void AnonFilter_a3_8444();
void AnonFilter_a3_8445();
void AnonFilter_a3_8446();
void AnonFilter_a3_8447();
void AnonFilter_a3_8448();
void AnonFilter_a3_8449();
void AnonFilter_a3_8450();
void AnonFilter_a3_8451();
void AnonFilter_a3_8452();
void AnonFilter_a3_8453();
void AnonFilter_a3_8454();
void AnonFilter_a3_8455();
void AnonFilter_a3_8456();
void AnonFilter_a3_8457();
void AnonFilter_a3_8458();
void AnonFilter_a3_8459();
void AnonFilter_a3_8460();
void AnonFilter_a3_8461();
void AnonFilter_a3_8462();
void AnonFilter_a3_8463();
void AnonFilter_a3_8464();
void AnonFilter_a3_8465();
void AnonFilter_a3_8466();
void AnonFilter_a3_8467();
void AnonFilter_a3_8468();
void AnonFilter_a3_8469();
void AnonFilter_a3_8470();
void AnonFilter_a3_8471();
void AnonFilter_a3_8472();
void AnonFilter_a3_8473();
void AnonFilter_a3_8474();
void AnonFilter_a3_8475();
void AnonFilter_a3_8476();
void WEIGHTED_ROUND_ROBIN_Joiner_8433();
void Pre_CollapsedDataParallel_1_8422();
void WEIGHTED_ROUND_ROBIN_Splitter_8477();
void iDCT_1D_reference_fine_8479();
void iDCT_1D_reference_fine_8480();
void iDCT_1D_reference_fine_8481();
void iDCT_1D_reference_fine_8482();
void iDCT_1D_reference_fine_8483();
void iDCT_1D_reference_fine_8484();
void iDCT_1D_reference_fine_8485();
void iDCT_1D_reference_fine_8486();
void WEIGHTED_ROUND_ROBIN_Joiner_8478();
void Post_CollapsedDataParallel_2_8423();
void WEIGHTED_ROUND_ROBIN_Splitter_8487();
void iDCT_1D_reference_fine_8489();
void iDCT_1D_reference_fine_8490();
void iDCT_1D_reference_fine_8491();
void iDCT_1D_reference_fine_8492();
void iDCT_1D_reference_fine_8493();
void iDCT_1D_reference_fine_8494();
void iDCT_1D_reference_fine_8495();
void iDCT_1D_reference_fine_8496();
void WEIGHTED_ROUND_ROBIN_Joiner_8488();
void WEIGHTED_ROUND_ROBIN_Splitter_8497();
void AnonFilter_a4_8499();
void AnonFilter_a4_8500();
void AnonFilter_a4_8501();
void AnonFilter_a4_8502();
void AnonFilter_a4_8503();
void AnonFilter_a4_8504();
void AnonFilter_a4_8505();
void AnonFilter_a4_8506();
void AnonFilter_a4_8507();
void AnonFilter_a4_8508();
void AnonFilter_a4_8509();
void AnonFilter_a4_8510();
void AnonFilter_a4_8511();
void AnonFilter_a4_8512();
void AnonFilter_a4_8513();
void AnonFilter_a4_8514();
void AnonFilter_a4_8515();
void AnonFilter_a4_8516();
void AnonFilter_a4_8517();
void AnonFilter_a4_8518();
void AnonFilter_a4_8519();
void AnonFilter_a4_8520();
void AnonFilter_a4_8521();
void AnonFilter_a4_8522();
void AnonFilter_a4_8523();
void AnonFilter_a4_8524();
void AnonFilter_a4_8525();
void AnonFilter_a4_8526();
void AnonFilter_a4_8527();
void AnonFilter_a4_8528();
void AnonFilter_a4_8529();
void AnonFilter_a4_8530();
void AnonFilter_a4_8531();
void AnonFilter_a4_8532();
void AnonFilter_a4_8533();
void AnonFilter_a4_8534();
void AnonFilter_a4_8535();
void AnonFilter_a4_8536();
void AnonFilter_a4_8537();
void AnonFilter_a4_8538();
void AnonFilter_a4_8539();
void AnonFilter_a4_8540();
void AnonFilter_a4_8541();
void WEIGHTED_ROUND_ROBIN_Joiner_8498();
void WEIGHTED_ROUND_ROBIN_Splitter_8542();
void iDCT8x8_1D_row_fast_8544();
void iDCT8x8_1D_row_fast_8545();
void iDCT8x8_1D_row_fast_8546();
void iDCT8x8_1D_row_fast_8547();
void iDCT8x8_1D_row_fast_8548();
void iDCT8x8_1D_row_fast_8549();
void iDCT8x8_1D_row_fast_8550();
void iDCT8x8_1D_row_fast_8551();
void WEIGHTED_ROUND_ROBIN_Joiner_8543();
void iDCT8x8_1D_col_fast_8376();
void WEIGHTED_ROUND_ROBIN_Joiner_8425();
void AnonFilter_a2_8377();

#ifdef __cplusplus
}
#endif
#endif
