#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=6528 on the compile command line
#else
#if BUF_SIZEMAX < 6528
#error BUF_SIZEMAX too small, it must be at least 6528
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_11214_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_11238_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_11239_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_11211();
void DUPLICATE_Splitter_11286();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_11214();
void WEIGHTED_ROUND_ROBIN_Splitter_11294();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_11296();
void AnonFilter_a3_11297();
void AnonFilter_a3_11298();
void AnonFilter_a3_11299();
void AnonFilter_a3_11300();
void AnonFilter_a3_11301();
void AnonFilter_a3_11302();
void AnonFilter_a3_11303();
void AnonFilter_a3_11304();
void AnonFilter_a3_11305();
void AnonFilter_a3_11306();
void AnonFilter_a3_11307();
void AnonFilter_a3_11308();
void AnonFilter_a3_11309();
void AnonFilter_a3_11310();
void AnonFilter_a3_11311();
void AnonFilter_a3_11312();
void AnonFilter_a3_11313();
void AnonFilter_a3_11314();
void AnonFilter_a3_11315();
void AnonFilter_a3_11316();
void AnonFilter_a3_11317();
void AnonFilter_a3_11318();
void AnonFilter_a3_11319();
void AnonFilter_a3_11320();
void AnonFilter_a3_11321();
void AnonFilter_a3_11322();
void AnonFilter_a3_11323();
void AnonFilter_a3_11324();
void AnonFilter_a3_11325();
void AnonFilter_a3_11326();
void AnonFilter_a3_11327();
void AnonFilter_a3_11328();
void AnonFilter_a3_11329();
void WEIGHTED_ROUND_ROBIN_Joiner_11295();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_11284();
void WEIGHTED_ROUND_ROBIN_Splitter_11330();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_11332();
void iDCT_1D_reference_fine_11333();
void iDCT_1D_reference_fine_11334();
void iDCT_1D_reference_fine_11335();
void iDCT_1D_reference_fine_11336();
void iDCT_1D_reference_fine_11337();
void iDCT_1D_reference_fine_11338();
void iDCT_1D_reference_fine_11339();
void WEIGHTED_ROUND_ROBIN_Joiner_11331();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_11285();
void WEIGHTED_ROUND_ROBIN_Splitter_11340();
void iDCT_1D_reference_fine_11342();
void iDCT_1D_reference_fine_11343();
void iDCT_1D_reference_fine_11344();
void iDCT_1D_reference_fine_11345();
void iDCT_1D_reference_fine_11346();
void iDCT_1D_reference_fine_11347();
void iDCT_1D_reference_fine_11348();
void iDCT_1D_reference_fine_11349();
void WEIGHTED_ROUND_ROBIN_Joiner_11341();
void WEIGHTED_ROUND_ROBIN_Splitter_11350();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_11352();
void AnonFilter_a4_11353();
void AnonFilter_a4_11354();
void AnonFilter_a4_11355();
void AnonFilter_a4_11356();
void AnonFilter_a4_11357();
void AnonFilter_a4_11358();
void AnonFilter_a4_11359();
void AnonFilter_a4_11360();
void AnonFilter_a4_11361();
void AnonFilter_a4_11362();
void AnonFilter_a4_11363();
void AnonFilter_a4_11364();
void AnonFilter_a4_11365();
void AnonFilter_a4_11366();
void AnonFilter_a4_11367();
void AnonFilter_a4_11368();
void AnonFilter_a4_11369();
void AnonFilter_a4_11370();
void AnonFilter_a4_11371();
void AnonFilter_a4_11372();
void AnonFilter_a4_11373();
void AnonFilter_a4_11374();
void AnonFilter_a4_11375();
void AnonFilter_a4_11376();
void AnonFilter_a4_11377();
void AnonFilter_a4_11378();
void AnonFilter_a4_11379();
void AnonFilter_a4_11380();
void AnonFilter_a4_11381();
void AnonFilter_a4_11382();
void AnonFilter_a4_11383();
void AnonFilter_a4_11384();
void AnonFilter_a4_11385();
void WEIGHTED_ROUND_ROBIN_Joiner_11351();
void WEIGHTED_ROUND_ROBIN_Splitter_11386();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_11388();
void iDCT8x8_1D_row_fast_11389();
void iDCT8x8_1D_row_fast_11390();
void iDCT8x8_1D_row_fast_11391();
void iDCT8x8_1D_row_fast_11392();
void iDCT8x8_1D_row_fast_11393();
void iDCT8x8_1D_row_fast_11394();
void iDCT8x8_1D_row_fast_11395();
void WEIGHTED_ROUND_ROBIN_Joiner_11387();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_11238();
void WEIGHTED_ROUND_ROBIN_Joiner_11287();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_11239();

#ifdef __cplusplus
}
#endif
#endif
