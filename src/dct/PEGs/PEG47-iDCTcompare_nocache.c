#include "PEG47-iDCTcompare_nocache.h"

buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[47];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_7049AnonFilter_a2_7001;
buffer_int_t AnonFilter_a0_6973DUPLICATE_Splitter_7048;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7106Post_CollapsedDataParallel_2_7047;
buffer_int_t SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7116WEIGHTED_ROUND_ROBIN_Splitter_7125;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[47];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_join[3];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[47];
buffer_int_t SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7057Pre_CollapsedDataParallel_1_7046;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[47];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_7046WEIGHTED_ROUND_ROBIN_Splitter_7105;
buffer_float_t Post_CollapsedDataParallel_2_7047WEIGHTED_ROUND_ROBIN_Splitter_7115;


iDCT_2D_reference_coarse_6976_t iDCT_2D_reference_coarse_6976_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7107_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7108_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7109_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7110_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7111_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7112_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7113_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7114_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7117_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7118_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7119_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7120_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7121_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7122_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7123_s;
iDCT_2D_reference_coarse_6976_t iDCT_1D_reference_fine_7124_s;
iDCT8x8_1D_col_fast_7000_t iDCT8x8_1D_col_fast_7000_s;
AnonFilter_a2_7001_t AnonFilter_a2_7001_s;

void AnonFilter_a0_6973(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_6973DUPLICATE_Splitter_7048, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_6976(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_6976_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_6976_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_7058(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7059(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7060(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7061(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7062(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7063(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7064(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7065(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7066(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7067(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7068(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7069(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7070(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7071(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7072(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7073(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7074(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7075(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7076(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7077(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7078(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7079(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7080(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7081(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7082(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7083(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7084(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7085(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7086(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7087(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7088(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7089(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7090(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7091(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7092(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7093(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7094(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7095(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7096(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7097(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7098(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7099(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7100(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7101(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7102(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7103(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[45])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7104(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[46], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[46])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7056() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 47, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7057() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 47, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7057Pre_CollapsedDataParallel_1_7046, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_7046(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_7046WEIGHTED_ROUND_ROBIN_Splitter_7105, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7057Pre_CollapsedDataParallel_1_7046, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7057Pre_CollapsedDataParallel_1_7046) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7107(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7107_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7108(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7108_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7109(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7109_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7110(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7110_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7111(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7111_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7112(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7112_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7113(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7113_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7114(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7114_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7105() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7046WEIGHTED_ROUND_ROBIN_Splitter_7105));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7106() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7106Post_CollapsedDataParallel_2_7047, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_7047(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_7047WEIGHTED_ROUND_ROBIN_Splitter_7115, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7106Post_CollapsedDataParallel_2_7047, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7106Post_CollapsedDataParallel_2_7047) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7117(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7117_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7118(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7118_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7119(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7119_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7120(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7120_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7121(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7121_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7122(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7122_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7123(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7123_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7124(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7124_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7115() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_7047WEIGHTED_ROUND_ROBIN_Splitter_7115));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7116() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7116WEIGHTED_ROUND_ROBIN_Splitter_7125, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_7127(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7128(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7129(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7130(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7131(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7132(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7133(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7134(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7135(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7136(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7137(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7138(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7139(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7140(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7141(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7142(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7143(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7144(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7145(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7146(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7147(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7148(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7149(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7150(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7151(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7152(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7153(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7154(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7155(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7156(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7157(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7158(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7159(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7160(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7161(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7162(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7163(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7164(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7165(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7166(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7167(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7168(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7169(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7170(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7171(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7172(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7173(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[46], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[46]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7125() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 47, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7116WEIGHTED_ROUND_ROBIN_Splitter_7125));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7126() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 47, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_7176(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0], 6) ; 
		x3 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0], 2) ; 
		x4 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0], 1) ; 
		x5 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0], 7) ; 
		x6 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0], 5) ; 
		x7 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7177(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1], 6) ; 
		x3 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1], 2) ; 
		x4 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1], 1) ; 
		x5 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1], 7) ; 
		x6 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1], 5) ; 
		x7 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7178(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2], 6) ; 
		x3 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2], 2) ; 
		x4 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2], 1) ; 
		x5 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2], 7) ; 
		x6 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2], 5) ; 
		x7 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7179(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3], 6) ; 
		x3 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3], 2) ; 
		x4 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3], 1) ; 
		x5 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3], 7) ; 
		x6 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3], 5) ; 
		x7 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7180(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4], 6) ; 
		x3 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4], 2) ; 
		x4 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4], 1) ; 
		x5 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4], 7) ; 
		x6 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4], 5) ; 
		x7 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7181(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5], 6) ; 
		x3 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5], 2) ; 
		x4 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5], 1) ; 
		x5 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5], 7) ; 
		x6 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5], 5) ; 
		x7 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7182(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6], 6) ; 
		x3 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6], 2) ; 
		x4 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6], 1) ; 
		x5 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6], 7) ; 
		x6 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6], 5) ; 
		x7 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7183(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7], 6) ; 
		x3 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7], 2) ; 
		x4 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7], 1) ; 
		x5 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7], 7) ; 
		x6 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7], 5) ; 
		x7 = peek_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7174() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7175() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, pop_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_7000(){
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_7000_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_7000_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_7000_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_7000_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_7000_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_7000_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_7000_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_7000_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_7000_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_join[2], iDCT8x8_1D_col_fast_7000_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_7048() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3008, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_6973DUPLICATE_Splitter_7048);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7049() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3008, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_7049AnonFilter_a2_7001, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_7001(){
	FOR(uint32_t, __iter_steady_, 0, <, 3008, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7049AnonFilter_a2_7001) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7049AnonFilter_a2_7001) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7049AnonFilter_a2_7001) ; 
		AnonFilter_a2_7001_s.count = (AnonFilter_a2_7001_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_7001_s.errors = (AnonFilter_a2_7001_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_7001_s.errors / AnonFilter_a2_7001_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_7001_s.errors = (AnonFilter_a2_7001_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_7001_s.errors / AnonFilter_a2_7001_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 47, __iter_init_0_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_split[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_7049AnonFilter_a2_7001);
	init_buffer_int(&AnonFilter_a0_6973DUPLICATE_Splitter_7048);
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_split[__iter_init_2_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_7175iDCT8x8_1D_col_fast_7000);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7106Post_CollapsedDataParallel_2_7047);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7186_7193_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7116WEIGHTED_ROUND_ROBIN_Splitter_7125);
	FOR(int, __iter_init_5_, 0, <, 47, __iter_init_5_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6974_7050_7184_7191_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 47, __iter_init_7_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_7185_7192_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin118_iDCT8x8_1D_row_fast_Fiss_7189_7196_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7057Pre_CollapsedDataParallel_1_7046);
	FOR(int, __iter_init_10_, 0, <, 47, __iter_init_10_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_7188_7195_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7187_7194_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7046WEIGHTED_ROUND_ROBIN_Splitter_7105);
	init_buffer_float(&Post_CollapsedDataParallel_2_7047WEIGHTED_ROUND_ROBIN_Splitter_7115);
// --- init: iDCT_2D_reference_coarse_6976
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_6976_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7107
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7107_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7108
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7108_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7109
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7109_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7110
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7110_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7111
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7111_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7112
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7112_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7113
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7113_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7114
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7114_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7117
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7117_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7118
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7118_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7119
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7119_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7120
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7120_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7121
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7121_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7122
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7122_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7123
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7123_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7124
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7124_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_7001
	 {
	AnonFilter_a2_7001_s.count = 0.0 ; 
	AnonFilter_a2_7001_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_6973();
		DUPLICATE_Splitter_7048();
			iDCT_2D_reference_coarse_6976();
			WEIGHTED_ROUND_ROBIN_Splitter_7056();
				AnonFilter_a3_7058();
				AnonFilter_a3_7059();
				AnonFilter_a3_7060();
				AnonFilter_a3_7061();
				AnonFilter_a3_7062();
				AnonFilter_a3_7063();
				AnonFilter_a3_7064();
				AnonFilter_a3_7065();
				AnonFilter_a3_7066();
				AnonFilter_a3_7067();
				AnonFilter_a3_7068();
				AnonFilter_a3_7069();
				AnonFilter_a3_7070();
				AnonFilter_a3_7071();
				AnonFilter_a3_7072();
				AnonFilter_a3_7073();
				AnonFilter_a3_7074();
				AnonFilter_a3_7075();
				AnonFilter_a3_7076();
				AnonFilter_a3_7077();
				AnonFilter_a3_7078();
				AnonFilter_a3_7079();
				AnonFilter_a3_7080();
				AnonFilter_a3_7081();
				AnonFilter_a3_7082();
				AnonFilter_a3_7083();
				AnonFilter_a3_7084();
				AnonFilter_a3_7085();
				AnonFilter_a3_7086();
				AnonFilter_a3_7087();
				AnonFilter_a3_7088();
				AnonFilter_a3_7089();
				AnonFilter_a3_7090();
				AnonFilter_a3_7091();
				AnonFilter_a3_7092();
				AnonFilter_a3_7093();
				AnonFilter_a3_7094();
				AnonFilter_a3_7095();
				AnonFilter_a3_7096();
				AnonFilter_a3_7097();
				AnonFilter_a3_7098();
				AnonFilter_a3_7099();
				AnonFilter_a3_7100();
				AnonFilter_a3_7101();
				AnonFilter_a3_7102();
				AnonFilter_a3_7103();
				AnonFilter_a3_7104();
			WEIGHTED_ROUND_ROBIN_Joiner_7057();
			Pre_CollapsedDataParallel_1_7046();
			WEIGHTED_ROUND_ROBIN_Splitter_7105();
				iDCT_1D_reference_fine_7107();
				iDCT_1D_reference_fine_7108();
				iDCT_1D_reference_fine_7109();
				iDCT_1D_reference_fine_7110();
				iDCT_1D_reference_fine_7111();
				iDCT_1D_reference_fine_7112();
				iDCT_1D_reference_fine_7113();
				iDCT_1D_reference_fine_7114();
			WEIGHTED_ROUND_ROBIN_Joiner_7106();
			Post_CollapsedDataParallel_2_7047();
			WEIGHTED_ROUND_ROBIN_Splitter_7115();
				iDCT_1D_reference_fine_7117();
				iDCT_1D_reference_fine_7118();
				iDCT_1D_reference_fine_7119();
				iDCT_1D_reference_fine_7120();
				iDCT_1D_reference_fine_7121();
				iDCT_1D_reference_fine_7122();
				iDCT_1D_reference_fine_7123();
				iDCT_1D_reference_fine_7124();
			WEIGHTED_ROUND_ROBIN_Joiner_7116();
			WEIGHTED_ROUND_ROBIN_Splitter_7125();
				AnonFilter_a4_7127();
				AnonFilter_a4_7128();
				AnonFilter_a4_7129();
				AnonFilter_a4_7130();
				AnonFilter_a4_7131();
				AnonFilter_a4_7132();
				AnonFilter_a4_7133();
				AnonFilter_a4_7134();
				AnonFilter_a4_7135();
				AnonFilter_a4_7136();
				AnonFilter_a4_7137();
				AnonFilter_a4_7138();
				AnonFilter_a4_7139();
				AnonFilter_a4_7140();
				AnonFilter_a4_7141();
				AnonFilter_a4_7142();
				AnonFilter_a4_7143();
				AnonFilter_a4_7144();
				AnonFilter_a4_7145();
				AnonFilter_a4_7146();
				AnonFilter_a4_7147();
				AnonFilter_a4_7148();
				AnonFilter_a4_7149();
				AnonFilter_a4_7150();
				AnonFilter_a4_7151();
				AnonFilter_a4_7152();
				AnonFilter_a4_7153();
				AnonFilter_a4_7154();
				AnonFilter_a4_7155();
				AnonFilter_a4_7156();
				AnonFilter_a4_7157();
				AnonFilter_a4_7158();
				AnonFilter_a4_7159();
				AnonFilter_a4_7160();
				AnonFilter_a4_7161();
				AnonFilter_a4_7162();
				AnonFilter_a4_7163();
				AnonFilter_a4_7164();
				AnonFilter_a4_7165();
				AnonFilter_a4_7166();
				AnonFilter_a4_7167();
				AnonFilter_a4_7168();
				AnonFilter_a4_7169();
				AnonFilter_a4_7170();
				AnonFilter_a4_7171();
				AnonFilter_a4_7172();
				AnonFilter_a4_7173();
			WEIGHTED_ROUND_ROBIN_Joiner_7126();
			WEIGHTED_ROUND_ROBIN_Splitter_7174();
				iDCT8x8_1D_row_fast_7176();
				iDCT8x8_1D_row_fast_7177();
				iDCT8x8_1D_row_fast_7178();
				iDCT8x8_1D_row_fast_7179();
				iDCT8x8_1D_row_fast_7180();
				iDCT8x8_1D_row_fast_7181();
				iDCT8x8_1D_row_fast_7182();
				iDCT8x8_1D_row_fast_7183();
			WEIGHTED_ROUND_ROBIN_Joiner_7175();
			iDCT8x8_1D_col_fast_7000();
		WEIGHTED_ROUND_ROBIN_Joiner_7049();
		AnonFilter_a2_7001();
	ENDFOR
	return EXIT_SUCCESS;
}
