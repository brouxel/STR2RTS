#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=11904 on the compile command line
#else
#if BUF_SIZEMAX < 11904
#error BUF_SIZEMAX too small, it must be at least 11904
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_1246_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_1270_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_1271_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_1243();
void DUPLICATE_Splitter_1318();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_1246();
void WEIGHTED_ROUND_ROBIN_Splitter_1326();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_1328();
void AnonFilter_a3_1329();
void AnonFilter_a3_1330();
void AnonFilter_a3_1331();
void AnonFilter_a3_1332();
void AnonFilter_a3_1333();
void AnonFilter_a3_1334();
void AnonFilter_a3_1335();
void AnonFilter_a3_1336();
void AnonFilter_a3_1337();
void AnonFilter_a3_1338();
void AnonFilter_a3_1339();
void AnonFilter_a3_1340();
void AnonFilter_a3_1341();
void AnonFilter_a3_1342();
void AnonFilter_a3_1343();
void AnonFilter_a3_1344();
void AnonFilter_a3_1345();
void AnonFilter_a3_1346();
void AnonFilter_a3_1347();
void AnonFilter_a3_1348();
void AnonFilter_a3_1349();
void AnonFilter_a3_1350();
void AnonFilter_a3_1351();
void AnonFilter_a3_1352();
void AnonFilter_a3_1353();
void AnonFilter_a3_1354();
void AnonFilter_a3_1355();
void AnonFilter_a3_1356();
void AnonFilter_a3_1357();
void AnonFilter_a3_1358();
void AnonFilter_a3_1359();
void AnonFilter_a3_1360();
void AnonFilter_a3_1361();
void AnonFilter_a3_1362();
void AnonFilter_a3_1363();
void AnonFilter_a3_1364();
void AnonFilter_a3_1365();
void AnonFilter_a3_1366();
void AnonFilter_a3_1367();
void AnonFilter_a3_1368();
void AnonFilter_a3_1369();
void AnonFilter_a3_1370();
void AnonFilter_a3_1371();
void AnonFilter_a3_1372();
void AnonFilter_a3_1373();
void AnonFilter_a3_1374();
void AnonFilter_a3_1375();
void AnonFilter_a3_1376();
void AnonFilter_a3_1377();
void AnonFilter_a3_1378();
void AnonFilter_a3_1379();
void AnonFilter_a3_1380();
void AnonFilter_a3_1381();
void AnonFilter_a3_1382();
void AnonFilter_a3_1383();
void AnonFilter_a3_1384();
void AnonFilter_a3_1385();
void AnonFilter_a3_1386();
void AnonFilter_a3_1387();
void AnonFilter_a3_1388();
void AnonFilter_a3_1389();
void WEIGHTED_ROUND_ROBIN_Joiner_1327();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_1316();
void WEIGHTED_ROUND_ROBIN_Splitter_1390();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_1392();
void iDCT_1D_reference_fine_1393();
void iDCT_1D_reference_fine_1394();
void iDCT_1D_reference_fine_1395();
void iDCT_1D_reference_fine_1396();
void iDCT_1D_reference_fine_1397();
void iDCT_1D_reference_fine_1398();
void iDCT_1D_reference_fine_1399();
void WEIGHTED_ROUND_ROBIN_Joiner_1391();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_1317();
void WEIGHTED_ROUND_ROBIN_Splitter_1400();
void iDCT_1D_reference_fine_1402();
void iDCT_1D_reference_fine_1403();
void iDCT_1D_reference_fine_1404();
void iDCT_1D_reference_fine_1405();
void iDCT_1D_reference_fine_1406();
void iDCT_1D_reference_fine_1407();
void iDCT_1D_reference_fine_1408();
void iDCT_1D_reference_fine_1409();
void WEIGHTED_ROUND_ROBIN_Joiner_1401();
void WEIGHTED_ROUND_ROBIN_Splitter_1410();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_1412();
void AnonFilter_a4_1413();
void AnonFilter_a4_1414();
void AnonFilter_a4_1415();
void AnonFilter_a4_1416();
void AnonFilter_a4_1417();
void AnonFilter_a4_1418();
void AnonFilter_a4_1419();
void AnonFilter_a4_1420();
void AnonFilter_a4_1421();
void AnonFilter_a4_1422();
void AnonFilter_a4_1423();
void AnonFilter_a4_1424();
void AnonFilter_a4_1425();
void AnonFilter_a4_1426();
void AnonFilter_a4_1427();
void AnonFilter_a4_1428();
void AnonFilter_a4_1429();
void AnonFilter_a4_1430();
void AnonFilter_a4_1431();
void AnonFilter_a4_1432();
void AnonFilter_a4_1433();
void AnonFilter_a4_1434();
void AnonFilter_a4_1435();
void AnonFilter_a4_1436();
void AnonFilter_a4_1437();
void AnonFilter_a4_1438();
void AnonFilter_a4_1439();
void AnonFilter_a4_1440();
void AnonFilter_a4_1441();
void AnonFilter_a4_1442();
void AnonFilter_a4_1443();
void AnonFilter_a4_1444();
void AnonFilter_a4_1445();
void AnonFilter_a4_1446();
void AnonFilter_a4_1447();
void AnonFilter_a4_1448();
void AnonFilter_a4_1449();
void AnonFilter_a4_1450();
void AnonFilter_a4_1451();
void AnonFilter_a4_1452();
void AnonFilter_a4_1453();
void AnonFilter_a4_1454();
void AnonFilter_a4_1455();
void AnonFilter_a4_1456();
void AnonFilter_a4_1457();
void AnonFilter_a4_1458();
void AnonFilter_a4_1459();
void AnonFilter_a4_1460();
void AnonFilter_a4_1461();
void AnonFilter_a4_1462();
void AnonFilter_a4_1463();
void AnonFilter_a4_1464();
void AnonFilter_a4_1465();
void AnonFilter_a4_1466();
void AnonFilter_a4_1467();
void AnonFilter_a4_1468();
void AnonFilter_a4_1469();
void AnonFilter_a4_1470();
void AnonFilter_a4_1471();
void AnonFilter_a4_1472();
void AnonFilter_a4_1473();
void WEIGHTED_ROUND_ROBIN_Joiner_1411();
void WEIGHTED_ROUND_ROBIN_Splitter_1474();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_1476();
void iDCT8x8_1D_row_fast_1477();
void iDCT8x8_1D_row_fast_1478();
void iDCT8x8_1D_row_fast_1479();
void iDCT8x8_1D_row_fast_1480();
void iDCT8x8_1D_row_fast_1481();
void iDCT8x8_1D_row_fast_1482();
void iDCT8x8_1D_row_fast_1483();
void WEIGHTED_ROUND_ROBIN_Joiner_1475();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_1270();
void WEIGHTED_ROUND_ROBIN_Joiner_1319();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_1271();

#ifdef __cplusplus
}
#endif
#endif
