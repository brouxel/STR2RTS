#include "PEG15-iDCTcompare_nocache.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_split[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16290Post_CollapsedDataParallel_2_16263;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[15];
buffer_int_t SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16273Pre_CollapsedDataParallel_1_16262;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216;
buffer_float_t Pre_CollapsedDataParallel_1_16262WEIGHTED_ROUND_ROBIN_Splitter_16289;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[8];
buffer_int_t AnonFilter_a0_16189DUPLICATE_Splitter_16264;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[15];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[15];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16265AnonFilter_a2_16217;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[15];
buffer_float_t Post_CollapsedDataParallel_2_16263WEIGHTED_ROUND_ROBIN_Splitter_16299;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[8];
buffer_int_t SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16300WEIGHTED_ROUND_ROBIN_Splitter_16309;


iDCT_2D_reference_coarse_16192_t iDCT_2D_reference_coarse_16192_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16291_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16292_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16293_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16294_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16295_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16296_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16297_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16298_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16301_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16302_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16303_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16304_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16305_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16306_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16307_s;
iDCT_2D_reference_coarse_16192_t iDCT_1D_reference_fine_16308_s;
iDCT8x8_1D_col_fast_16216_t iDCT8x8_1D_col_fast_16216_s;
AnonFilter_a2_16217_t AnonFilter_a2_16217_s;

void AnonFilter_a0_16189(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_16189DUPLICATE_Splitter_16264, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_16192(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_16192_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_16192_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_16274(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16275(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16276(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16277(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16278(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16279(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16280(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16281(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16282(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16283(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16284(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16285(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16286(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16287(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16288(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[14])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16272() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16273() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16273Pre_CollapsedDataParallel_1_16262, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_16262(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_16262WEIGHTED_ROUND_ROBIN_Splitter_16289, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_16273Pre_CollapsedDataParallel_1_16262, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16273Pre_CollapsedDataParallel_1_16262) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16291(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16291_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16292(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16292_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16293(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16293_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16294(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16294_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16295(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16295_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16296(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16296_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16297(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16297_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16298(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16298_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16289() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_16262WEIGHTED_ROUND_ROBIN_Splitter_16289));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16290() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16290Post_CollapsedDataParallel_2_16263, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_16263(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_16263WEIGHTED_ROUND_ROBIN_Splitter_16299, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_16290Post_CollapsedDataParallel_2_16263, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16290Post_CollapsedDataParallel_2_16263) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16301(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16301_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16302(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16302_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16303(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16303_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16304(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16304_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16305(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16305_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16306(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16306_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16307(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16307_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16308(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16308_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16299() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_16263WEIGHTED_ROUND_ROBIN_Splitter_16299));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16300() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16300WEIGHTED_ROUND_ROBIN_Splitter_16309, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_16311(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16312(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16313(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16314(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16315(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16316(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16317(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16318(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16319(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16320(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16321(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16322(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16323(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16324(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16325(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16309() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16300WEIGHTED_ROUND_ROBIN_Splitter_16309));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16310() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_16328(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0], 6) ; 
		x3 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0], 2) ; 
		x4 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0], 1) ; 
		x5 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0], 7) ; 
		x6 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0], 5) ; 
		x7 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16329(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1], 6) ; 
		x3 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1], 2) ; 
		x4 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1], 1) ; 
		x5 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1], 7) ; 
		x6 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1], 5) ; 
		x7 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16330(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2], 6) ; 
		x3 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2], 2) ; 
		x4 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2], 1) ; 
		x5 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2], 7) ; 
		x6 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2], 5) ; 
		x7 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16331(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3], 6) ; 
		x3 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3], 2) ; 
		x4 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3], 1) ; 
		x5 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3], 7) ; 
		x6 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3], 5) ; 
		x7 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16332(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4], 6) ; 
		x3 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4], 2) ; 
		x4 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4], 1) ; 
		x5 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4], 7) ; 
		x6 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4], 5) ; 
		x7 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16333(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5], 6) ; 
		x3 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5], 2) ; 
		x4 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5], 1) ; 
		x5 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5], 7) ; 
		x6 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5], 5) ; 
		x7 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16334(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6], 6) ; 
		x3 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6], 2) ; 
		x4 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6], 1) ; 
		x5 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6], 7) ; 
		x6 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6], 5) ; 
		x7 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16335(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7], 6) ; 
		x3 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7], 2) ; 
		x4 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7], 1) ; 
		x5 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7], 7) ; 
		x6 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7], 5) ; 
		x7 = peek_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16326() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16327() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, pop_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_16216(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_16216_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_16216_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_16216_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_16216_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_16216_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_16216_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_16216_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_16216_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_16216_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_join[2], iDCT8x8_1D_col_fast_16216_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_16264() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_16189DUPLICATE_Splitter_16264);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16265() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16265AnonFilter_a2_16217, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_16217(){
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16265AnonFilter_a2_16217) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16265AnonFilter_a2_16217) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16265AnonFilter_a2_16217) ; 
		AnonFilter_a2_16217_s.count = (AnonFilter_a2_16217_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_16217_s.errors = (AnonFilter_a2_16217_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_16217_s.errors / AnonFilter_a2_16217_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_16217_s.errors = (AnonFilter_a2_16217_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_16217_s.errors / AnonFilter_a2_16217_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16190_16266_16336_16343_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16290Post_CollapsedDataParallel_2_16263);
	FOR(int, __iter_init_3_, 0, <, 15, __iter_init_3_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16273Pre_CollapsedDataParallel_1_16262);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16327iDCT8x8_1D_col_fast_16216);
	init_buffer_float(&Pre_CollapsedDataParallel_1_16262WEIGHTED_ROUND_ROBIN_Splitter_16289);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16339_16346_join[__iter_init_5_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_16189DUPLICATE_Splitter_16264);
	FOR(int, __iter_init_6_, 0, <, 15, __iter_init_6_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 15, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_16340_16347_join[__iter_init_7_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16265AnonFilter_a2_16217);
	FOR(int, __iter_init_8_, 0, <, 15, __iter_init_8_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_16337_16344_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_16263WEIGHTED_ROUND_ROBIN_Splitter_16299);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_int(&SplitJoin54_iDCT8x8_1D_row_fast_Fiss_16341_16348_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16338_16345_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16300WEIGHTED_ROUND_ROBIN_Splitter_16309);
// --- init: iDCT_2D_reference_coarse_16192
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_16192_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16291
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16291_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16292
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16292_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16293
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16293_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16294
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16294_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16295
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16295_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16296
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16296_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16297
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16297_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16298
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16298_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16301
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16301_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16302
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16302_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16303
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16303_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16304
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16304_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16305
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16305_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16306
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16306_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16307
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16307_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16308
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16308_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_16217
	 {
	AnonFilter_a2_16217_s.count = 0.0 ; 
	AnonFilter_a2_16217_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_16189();
		DUPLICATE_Splitter_16264();
			iDCT_2D_reference_coarse_16192();
			WEIGHTED_ROUND_ROBIN_Splitter_16272();
				AnonFilter_a3_16274();
				AnonFilter_a3_16275();
				AnonFilter_a3_16276();
				AnonFilter_a3_16277();
				AnonFilter_a3_16278();
				AnonFilter_a3_16279();
				AnonFilter_a3_16280();
				AnonFilter_a3_16281();
				AnonFilter_a3_16282();
				AnonFilter_a3_16283();
				AnonFilter_a3_16284();
				AnonFilter_a3_16285();
				AnonFilter_a3_16286();
				AnonFilter_a3_16287();
				AnonFilter_a3_16288();
			WEIGHTED_ROUND_ROBIN_Joiner_16273();
			Pre_CollapsedDataParallel_1_16262();
			WEIGHTED_ROUND_ROBIN_Splitter_16289();
				iDCT_1D_reference_fine_16291();
				iDCT_1D_reference_fine_16292();
				iDCT_1D_reference_fine_16293();
				iDCT_1D_reference_fine_16294();
				iDCT_1D_reference_fine_16295();
				iDCT_1D_reference_fine_16296();
				iDCT_1D_reference_fine_16297();
				iDCT_1D_reference_fine_16298();
			WEIGHTED_ROUND_ROBIN_Joiner_16290();
			Post_CollapsedDataParallel_2_16263();
			WEIGHTED_ROUND_ROBIN_Splitter_16299();
				iDCT_1D_reference_fine_16301();
				iDCT_1D_reference_fine_16302();
				iDCT_1D_reference_fine_16303();
				iDCT_1D_reference_fine_16304();
				iDCT_1D_reference_fine_16305();
				iDCT_1D_reference_fine_16306();
				iDCT_1D_reference_fine_16307();
				iDCT_1D_reference_fine_16308();
			WEIGHTED_ROUND_ROBIN_Joiner_16300();
			WEIGHTED_ROUND_ROBIN_Splitter_16309();
				AnonFilter_a4_16311();
				AnonFilter_a4_16312();
				AnonFilter_a4_16313();
				AnonFilter_a4_16314();
				AnonFilter_a4_16315();
				AnonFilter_a4_16316();
				AnonFilter_a4_16317();
				AnonFilter_a4_16318();
				AnonFilter_a4_16319();
				AnonFilter_a4_16320();
				AnonFilter_a4_16321();
				AnonFilter_a4_16322();
				AnonFilter_a4_16323();
				AnonFilter_a4_16324();
				AnonFilter_a4_16325();
			WEIGHTED_ROUND_ROBIN_Joiner_16310();
			WEIGHTED_ROUND_ROBIN_Splitter_16326();
				iDCT8x8_1D_row_fast_16328();
				iDCT8x8_1D_row_fast_16329();
				iDCT8x8_1D_row_fast_16330();
				iDCT8x8_1D_row_fast_16331();
				iDCT8x8_1D_row_fast_16332();
				iDCT8x8_1D_row_fast_16333();
				iDCT8x8_1D_row_fast_16334();
				iDCT8x8_1D_row_fast_16335();
			WEIGHTED_ROUND_ROBIN_Joiner_16327();
			iDCT8x8_1D_col_fast_16216();
		WEIGHTED_ROUND_ROBIN_Joiner_16265();
		AnonFilter_a2_16217();
	ENDFOR
	return EXIT_SUCCESS;
}
