#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=7296 on the compile command line
#else
#if BUF_SIZEMAX < 7296
#error BUF_SIZEMAX too small, it must be at least 7296
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_15264_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_15288_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_15289_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_15261();
void DUPLICATE_Splitter_15336();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_15264();
void WEIGHTED_ROUND_ROBIN_Splitter_15344();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_15346();
void AnonFilter_a3_15347();
void AnonFilter_a3_15348();
void AnonFilter_a3_15349();
void AnonFilter_a3_15350();
void AnonFilter_a3_15351();
void AnonFilter_a3_15352();
void AnonFilter_a3_15353();
void AnonFilter_a3_15354();
void AnonFilter_a3_15355();
void AnonFilter_a3_15356();
void AnonFilter_a3_15357();
void AnonFilter_a3_15358();
void AnonFilter_a3_15359();
void AnonFilter_a3_15360();
void AnonFilter_a3_15361();
void AnonFilter_a3_15362();
void AnonFilter_a3_15363();
void AnonFilter_a3_15364();
void WEIGHTED_ROUND_ROBIN_Joiner_15345();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_15334();
void WEIGHTED_ROUND_ROBIN_Splitter_15365();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_15367();
void iDCT_1D_reference_fine_15368();
void iDCT_1D_reference_fine_15369();
void iDCT_1D_reference_fine_15370();
void iDCT_1D_reference_fine_15371();
void iDCT_1D_reference_fine_15372();
void iDCT_1D_reference_fine_15373();
void iDCT_1D_reference_fine_15374();
void WEIGHTED_ROUND_ROBIN_Joiner_15366();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_15335();
void WEIGHTED_ROUND_ROBIN_Splitter_15375();
void iDCT_1D_reference_fine_15377();
void iDCT_1D_reference_fine_15378();
void iDCT_1D_reference_fine_15379();
void iDCT_1D_reference_fine_15380();
void iDCT_1D_reference_fine_15381();
void iDCT_1D_reference_fine_15382();
void iDCT_1D_reference_fine_15383();
void iDCT_1D_reference_fine_15384();
void WEIGHTED_ROUND_ROBIN_Joiner_15376();
void WEIGHTED_ROUND_ROBIN_Splitter_15385();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_15387();
void AnonFilter_a4_15388();
void AnonFilter_a4_15389();
void AnonFilter_a4_15390();
void AnonFilter_a4_15391();
void AnonFilter_a4_15392();
void AnonFilter_a4_15393();
void AnonFilter_a4_15394();
void AnonFilter_a4_15395();
void AnonFilter_a4_15396();
void AnonFilter_a4_15397();
void AnonFilter_a4_15398();
void AnonFilter_a4_15399();
void AnonFilter_a4_15400();
void AnonFilter_a4_15401();
void AnonFilter_a4_15402();
void AnonFilter_a4_15403();
void AnonFilter_a4_15404();
void AnonFilter_a4_15405();
void WEIGHTED_ROUND_ROBIN_Joiner_15386();
void WEIGHTED_ROUND_ROBIN_Splitter_15406();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_15408();
void iDCT8x8_1D_row_fast_15409();
void iDCT8x8_1D_row_fast_15410();
void iDCT8x8_1D_row_fast_15411();
void iDCT8x8_1D_row_fast_15412();
void iDCT8x8_1D_row_fast_15413();
void iDCT8x8_1D_row_fast_15414();
void iDCT8x8_1D_row_fast_15415();
void WEIGHTED_ROUND_ROBIN_Joiner_15407();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_15288();
void WEIGHTED_ROUND_ROBIN_Joiner_15337();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_15289();

#ifdef __cplusplus
}
#endif
#endif
