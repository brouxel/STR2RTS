#include "PEG58-iDCTcompare.h"

buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_2935AnonFilter_a2_2887;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_3083iDCT8x8_1D_col_fast_2886;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[8];
buffer_int_t SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_split[3];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[58];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[58];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2943Pre_CollapsedDataParallel_1_2932;
buffer_float_t Post_CollapsedDataParallel_2_2933WEIGHTED_ROUND_ROBIN_Splitter_3012;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_2932WEIGHTED_ROUND_ROBIN_Splitter_3002;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3013WEIGHTED_ROUND_ROBIN_Splitter_3022;
buffer_int_t SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[58];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_join[3];
buffer_int_t AnonFilter_a0_2859DUPLICATE_Splitter_2934;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3003Post_CollapsedDataParallel_2_2933;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[58];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[8];


iDCT_2D_reference_coarse_2862_t iDCT_2D_reference_coarse_2862_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3004_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3005_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3006_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3007_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3008_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3009_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3010_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3011_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3014_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3015_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3016_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3017_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3018_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3019_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3020_s;
iDCT_2D_reference_coarse_2862_t iDCT_1D_reference_fine_3021_s;
iDCT8x8_1D_col_fast_2886_t iDCT8x8_1D_col_fast_2886_s;
AnonFilter_a2_2887_t AnonFilter_a2_2887_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_2859() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_2859DUPLICATE_Splitter_2934));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_2862_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_2862_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_2862() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_2944() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[0]));
	ENDFOR
}

void AnonFilter_a3_2945() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[1]));
	ENDFOR
}

void AnonFilter_a3_2946() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[2]));
	ENDFOR
}

void AnonFilter_a3_2947() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[3]));
	ENDFOR
}

void AnonFilter_a3_2948() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[4]));
	ENDFOR
}

void AnonFilter_a3_2949() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[5]));
	ENDFOR
}

void AnonFilter_a3_2950() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[6]));
	ENDFOR
}

void AnonFilter_a3_2951() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[7]));
	ENDFOR
}

void AnonFilter_a3_2952() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[8]));
	ENDFOR
}

void AnonFilter_a3_2953() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[9]));
	ENDFOR
}

void AnonFilter_a3_2954() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[10]));
	ENDFOR
}

void AnonFilter_a3_2955() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[11]));
	ENDFOR
}

void AnonFilter_a3_2956() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[12]));
	ENDFOR
}

void AnonFilter_a3_2957() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[13]));
	ENDFOR
}

void AnonFilter_a3_2958() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[14]));
	ENDFOR
}

void AnonFilter_a3_2959() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[15]));
	ENDFOR
}

void AnonFilter_a3_2960() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[16]));
	ENDFOR
}

void AnonFilter_a3_2961() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[17]));
	ENDFOR
}

void AnonFilter_a3_2962() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[18]));
	ENDFOR
}

void AnonFilter_a3_2963() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[19]));
	ENDFOR
}

void AnonFilter_a3_2964() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[20]));
	ENDFOR
}

void AnonFilter_a3_2965() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[21]));
	ENDFOR
}

void AnonFilter_a3_2966() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[22]));
	ENDFOR
}

void AnonFilter_a3_2967() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[23]));
	ENDFOR
}

void AnonFilter_a3_2968() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[24]));
	ENDFOR
}

void AnonFilter_a3_2969() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[25]));
	ENDFOR
}

void AnonFilter_a3_2970() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[26]));
	ENDFOR
}

void AnonFilter_a3_2971() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[27]));
	ENDFOR
}

void AnonFilter_a3_2972() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[28]));
	ENDFOR
}

void AnonFilter_a3_2973() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[29]));
	ENDFOR
}

void AnonFilter_a3_2974() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[30]));
	ENDFOR
}

void AnonFilter_a3_2975() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[31]));
	ENDFOR
}

void AnonFilter_a3_2976() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[32]));
	ENDFOR
}

void AnonFilter_a3_2977() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[33]));
	ENDFOR
}

void AnonFilter_a3_2978() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[34]));
	ENDFOR
}

void AnonFilter_a3_2979() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[35]));
	ENDFOR
}

void AnonFilter_a3_2980() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[36]));
	ENDFOR
}

void AnonFilter_a3_2981() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[37]));
	ENDFOR
}

void AnonFilter_a3_2982() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[38]));
	ENDFOR
}

void AnonFilter_a3_2983() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[39]));
	ENDFOR
}

void AnonFilter_a3_2984() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[40]));
	ENDFOR
}

void AnonFilter_a3_2985() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[41]));
	ENDFOR
}

void AnonFilter_a3_2986() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[42]));
	ENDFOR
}

void AnonFilter_a3_2987() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[43]));
	ENDFOR
}

void AnonFilter_a3_2988() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[44]));
	ENDFOR
}

void AnonFilter_a3_2989() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[45]));
	ENDFOR
}

void AnonFilter_a3_2990() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[46]));
	ENDFOR
}

void AnonFilter_a3_2991() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[47]));
	ENDFOR
}

void AnonFilter_a3_2992() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[48]));
	ENDFOR
}

void AnonFilter_a3_2993() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[49]));
	ENDFOR
}

void AnonFilter_a3_2994() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[50]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[50]));
	ENDFOR
}

void AnonFilter_a3_2995() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[51]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[51]));
	ENDFOR
}

void AnonFilter_a3_2996() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[52]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[52]));
	ENDFOR
}

void AnonFilter_a3_2997() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[53]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[53]));
	ENDFOR
}

void AnonFilter_a3_2998() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[54]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[54]));
	ENDFOR
}

void AnonFilter_a3_2999() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[55]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[55]));
	ENDFOR
}

void AnonFilter_a3_3000() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[56]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[56]));
	ENDFOR
}

void AnonFilter_a3_3001() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[57]), &(SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[57]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2942() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 58, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2943() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 58, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2943Pre_CollapsedDataParallel_1_2932, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_2932() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_2943Pre_CollapsedDataParallel_1_2932), &(Pre_CollapsedDataParallel_1_2932WEIGHTED_ROUND_ROBIN_Splitter_3002));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3004_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_3004() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_3005() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_3006() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_3007() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_3008() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_3009() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_3010() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_3011() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3002() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_2932WEIGHTED_ROUND_ROBIN_Splitter_3002));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3003() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3003Post_CollapsedDataParallel_2_2933, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_2933() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3003Post_CollapsedDataParallel_2_2933), &(Post_CollapsedDataParallel_2_2933WEIGHTED_ROUND_ROBIN_Splitter_3012));
	ENDFOR
}

void iDCT_1D_reference_fine_3014() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_3015() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_3016() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_3017() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_3018() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_3019() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_3020() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_3021() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3012() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_2933WEIGHTED_ROUND_ROBIN_Splitter_3012));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3013() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3013WEIGHTED_ROUND_ROBIN_Splitter_3022, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_3024() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[0]));
	ENDFOR
}

void AnonFilter_a4_3025() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[1]));
	ENDFOR
}

void AnonFilter_a4_3026() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[2]));
	ENDFOR
}

void AnonFilter_a4_3027() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[3]));
	ENDFOR
}

void AnonFilter_a4_3028() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[4]));
	ENDFOR
}

void AnonFilter_a4_3029() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[5]));
	ENDFOR
}

void AnonFilter_a4_3030() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[6]));
	ENDFOR
}

void AnonFilter_a4_3031() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[7]));
	ENDFOR
}

void AnonFilter_a4_3032() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[8]));
	ENDFOR
}

void AnonFilter_a4_3033() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[9]));
	ENDFOR
}

void AnonFilter_a4_3034() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[10]));
	ENDFOR
}

void AnonFilter_a4_3035() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[11]));
	ENDFOR
}

void AnonFilter_a4_3036() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[12]));
	ENDFOR
}

void AnonFilter_a4_3037() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[13]));
	ENDFOR
}

void AnonFilter_a4_3038() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[14]));
	ENDFOR
}

void AnonFilter_a4_3039() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[15]));
	ENDFOR
}

void AnonFilter_a4_3040() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[16]));
	ENDFOR
}

void AnonFilter_a4_3041() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[17]));
	ENDFOR
}

void AnonFilter_a4_3042() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[18]));
	ENDFOR
}

void AnonFilter_a4_3043() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[19]));
	ENDFOR
}

void AnonFilter_a4_3044() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[20]));
	ENDFOR
}

void AnonFilter_a4_3045() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[21]));
	ENDFOR
}

void AnonFilter_a4_3046() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[22]));
	ENDFOR
}

void AnonFilter_a4_3047() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[23]));
	ENDFOR
}

void AnonFilter_a4_3048() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[24]));
	ENDFOR
}

void AnonFilter_a4_3049() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[25]));
	ENDFOR
}

void AnonFilter_a4_3050() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[26]));
	ENDFOR
}

void AnonFilter_a4_3051() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[27]));
	ENDFOR
}

void AnonFilter_a4_3052() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[28]));
	ENDFOR
}

void AnonFilter_a4_3053() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[29]));
	ENDFOR
}

void AnonFilter_a4_3054() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[30]));
	ENDFOR
}

void AnonFilter_a4_3055() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[31]));
	ENDFOR
}

void AnonFilter_a4_3056() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[32]));
	ENDFOR
}

void AnonFilter_a4_3057() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[33]));
	ENDFOR
}

void AnonFilter_a4_3058() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[34]));
	ENDFOR
}

void AnonFilter_a4_3059() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[35]));
	ENDFOR
}

void AnonFilter_a4_3060() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[36]));
	ENDFOR
}

void AnonFilter_a4_3061() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[37]));
	ENDFOR
}

void AnonFilter_a4_3062() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[38]));
	ENDFOR
}

void AnonFilter_a4_3063() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[39]));
	ENDFOR
}

void AnonFilter_a4_3064() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[40]));
	ENDFOR
}

void AnonFilter_a4_3065() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[41]));
	ENDFOR
}

void AnonFilter_a4_3066() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[42]));
	ENDFOR
}

void AnonFilter_a4_3067() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[43]));
	ENDFOR
}

void AnonFilter_a4_3068() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[44]));
	ENDFOR
}

void AnonFilter_a4_3069() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[45]));
	ENDFOR
}

void AnonFilter_a4_3070() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[46]));
	ENDFOR
}

void AnonFilter_a4_3071() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[47]));
	ENDFOR
}

void AnonFilter_a4_3072() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[48]));
	ENDFOR
}

void AnonFilter_a4_3073() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[49]));
	ENDFOR
}

void AnonFilter_a4_3074() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[50]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[50]));
	ENDFOR
}

void AnonFilter_a4_3075() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[51]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[51]));
	ENDFOR
}

void AnonFilter_a4_3076() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[52]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[52]));
	ENDFOR
}

void AnonFilter_a4_3077() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[53]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[53]));
	ENDFOR
}

void AnonFilter_a4_3078() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[54]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[54]));
	ENDFOR
}

void AnonFilter_a4_3079() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[55]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[55]));
	ENDFOR
}

void AnonFilter_a4_3080() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[56]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[56]));
	ENDFOR
}

void AnonFilter_a4_3081() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[57]), &(SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[57]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3022() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 58, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3013WEIGHTED_ROUND_ROBIN_Splitter_3022));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3023() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 58, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_3084() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[0]), &(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3085() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[1]), &(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3086() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[2]), &(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3087() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[3]), &(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3088() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[4]), &(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3089() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[5]), &(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3090() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[6]), &(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3091() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[7]), &(SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3082() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3083() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_3083iDCT8x8_1D_col_fast_2886, pop_int(&SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_2886_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_2886_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_2886_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_2886_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_2886_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_2886_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_2886_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_2886_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_2886_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_2886_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_2886() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_3083iDCT8x8_1D_col_fast_2886), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_2934() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_2859DUPLICATE_Splitter_2934);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2935() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_2935AnonFilter_a2_2887, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_2887_s.count = (AnonFilter_a2_2887_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_2887_s.errors = (AnonFilter_a2_2887_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_2887_s.errors / AnonFilter_a2_2887_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_2887_s.errors = (AnonFilter_a2_2887_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_2887_s.errors / AnonFilter_a2_2887_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_2887() {
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_2935AnonFilter_a2_2887));
	ENDFOR
}

void __stream_init__() {
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_2935AnonFilter_a2_2887);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_3083iDCT8x8_1D_col_fast_2886);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 58, __iter_init_3_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_3093_3100_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 58, __iter_init_4_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_3096_3103_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2943Pre_CollapsedDataParallel_1_2932);
	init_buffer_float(&Post_CollapsedDataParallel_2_2933WEIGHTED_ROUND_ROBIN_Splitter_3012);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3095_3102_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_2932WEIGHTED_ROUND_ROBIN_Splitter_3002);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3013WEIGHTED_ROUND_ROBIN_Splitter_3022);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_int(&SplitJoin140_iDCT8x8_1D_row_fast_Fiss_3097_3104_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 58, __iter_init_8_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_3093_3100_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2860_2936_3092_3099_join[__iter_init_9_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_2859DUPLICATE_Splitter_2934);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3003Post_CollapsedDataParallel_2_2933);
	FOR(int, __iter_init_10_, 0, <, 58, __iter_init_10_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_3096_3103_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3094_3101_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_2862
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_2862_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3004
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3004_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3005
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3005_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3006
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3006_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3007
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3007_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3008
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3008_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3009
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3009_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3010
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3010_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3011
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3011_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3014
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3014_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3015
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3015_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3016
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3016_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3017
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3017_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3018
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3018_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3019
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3019_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3020
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3020_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3021
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3021_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_2887
	 {
	AnonFilter_a2_2887_s.count = 0.0 ; 
	AnonFilter_a2_2887_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_2859();
		DUPLICATE_Splitter_2934();
			iDCT_2D_reference_coarse_2862();
			WEIGHTED_ROUND_ROBIN_Splitter_2942();
				AnonFilter_a3_2944();
				AnonFilter_a3_2945();
				AnonFilter_a3_2946();
				AnonFilter_a3_2947();
				AnonFilter_a3_2948();
				AnonFilter_a3_2949();
				AnonFilter_a3_2950();
				AnonFilter_a3_2951();
				AnonFilter_a3_2952();
				AnonFilter_a3_2953();
				AnonFilter_a3_2954();
				AnonFilter_a3_2955();
				AnonFilter_a3_2956();
				AnonFilter_a3_2957();
				AnonFilter_a3_2958();
				AnonFilter_a3_2959();
				AnonFilter_a3_2960();
				AnonFilter_a3_2961();
				AnonFilter_a3_2962();
				AnonFilter_a3_2963();
				AnonFilter_a3_2964();
				AnonFilter_a3_2965();
				AnonFilter_a3_2966();
				AnonFilter_a3_2967();
				AnonFilter_a3_2968();
				AnonFilter_a3_2969();
				AnonFilter_a3_2970();
				AnonFilter_a3_2971();
				AnonFilter_a3_2972();
				AnonFilter_a3_2973();
				AnonFilter_a3_2974();
				AnonFilter_a3_2975();
				AnonFilter_a3_2976();
				AnonFilter_a3_2977();
				AnonFilter_a3_2978();
				AnonFilter_a3_2979();
				AnonFilter_a3_2980();
				AnonFilter_a3_2981();
				AnonFilter_a3_2982();
				AnonFilter_a3_2983();
				AnonFilter_a3_2984();
				AnonFilter_a3_2985();
				AnonFilter_a3_2986();
				AnonFilter_a3_2987();
				AnonFilter_a3_2988();
				AnonFilter_a3_2989();
				AnonFilter_a3_2990();
				AnonFilter_a3_2991();
				AnonFilter_a3_2992();
				AnonFilter_a3_2993();
				AnonFilter_a3_2994();
				AnonFilter_a3_2995();
				AnonFilter_a3_2996();
				AnonFilter_a3_2997();
				AnonFilter_a3_2998();
				AnonFilter_a3_2999();
				AnonFilter_a3_3000();
				AnonFilter_a3_3001();
			WEIGHTED_ROUND_ROBIN_Joiner_2943();
			Pre_CollapsedDataParallel_1_2932();
			WEIGHTED_ROUND_ROBIN_Splitter_3002();
				iDCT_1D_reference_fine_3004();
				iDCT_1D_reference_fine_3005();
				iDCT_1D_reference_fine_3006();
				iDCT_1D_reference_fine_3007();
				iDCT_1D_reference_fine_3008();
				iDCT_1D_reference_fine_3009();
				iDCT_1D_reference_fine_3010();
				iDCT_1D_reference_fine_3011();
			WEIGHTED_ROUND_ROBIN_Joiner_3003();
			Post_CollapsedDataParallel_2_2933();
			WEIGHTED_ROUND_ROBIN_Splitter_3012();
				iDCT_1D_reference_fine_3014();
				iDCT_1D_reference_fine_3015();
				iDCT_1D_reference_fine_3016();
				iDCT_1D_reference_fine_3017();
				iDCT_1D_reference_fine_3018();
				iDCT_1D_reference_fine_3019();
				iDCT_1D_reference_fine_3020();
				iDCT_1D_reference_fine_3021();
			WEIGHTED_ROUND_ROBIN_Joiner_3013();
			WEIGHTED_ROUND_ROBIN_Splitter_3022();
				AnonFilter_a4_3024();
				AnonFilter_a4_3025();
				AnonFilter_a4_3026();
				AnonFilter_a4_3027();
				AnonFilter_a4_3028();
				AnonFilter_a4_3029();
				AnonFilter_a4_3030();
				AnonFilter_a4_3031();
				AnonFilter_a4_3032();
				AnonFilter_a4_3033();
				AnonFilter_a4_3034();
				AnonFilter_a4_3035();
				AnonFilter_a4_3036();
				AnonFilter_a4_3037();
				AnonFilter_a4_3038();
				AnonFilter_a4_3039();
				AnonFilter_a4_3040();
				AnonFilter_a4_3041();
				AnonFilter_a4_3042();
				AnonFilter_a4_3043();
				AnonFilter_a4_3044();
				AnonFilter_a4_3045();
				AnonFilter_a4_3046();
				AnonFilter_a4_3047();
				AnonFilter_a4_3048();
				AnonFilter_a4_3049();
				AnonFilter_a4_3050();
				AnonFilter_a4_3051();
				AnonFilter_a4_3052();
				AnonFilter_a4_3053();
				AnonFilter_a4_3054();
				AnonFilter_a4_3055();
				AnonFilter_a4_3056();
				AnonFilter_a4_3057();
				AnonFilter_a4_3058();
				AnonFilter_a4_3059();
				AnonFilter_a4_3060();
				AnonFilter_a4_3061();
				AnonFilter_a4_3062();
				AnonFilter_a4_3063();
				AnonFilter_a4_3064();
				AnonFilter_a4_3065();
				AnonFilter_a4_3066();
				AnonFilter_a4_3067();
				AnonFilter_a4_3068();
				AnonFilter_a4_3069();
				AnonFilter_a4_3070();
				AnonFilter_a4_3071();
				AnonFilter_a4_3072();
				AnonFilter_a4_3073();
				AnonFilter_a4_3074();
				AnonFilter_a4_3075();
				AnonFilter_a4_3076();
				AnonFilter_a4_3077();
				AnonFilter_a4_3078();
				AnonFilter_a4_3079();
				AnonFilter_a4_3080();
				AnonFilter_a4_3081();
			WEIGHTED_ROUND_ROBIN_Joiner_3023();
			WEIGHTED_ROUND_ROBIN_Splitter_3082();
				iDCT8x8_1D_row_fast_3084();
				iDCT8x8_1D_row_fast_3085();
				iDCT8x8_1D_row_fast_3086();
				iDCT8x8_1D_row_fast_3087();
				iDCT8x8_1D_row_fast_3088();
				iDCT8x8_1D_row_fast_3089();
				iDCT8x8_1D_row_fast_3090();
				iDCT8x8_1D_row_fast_3091();
			WEIGHTED_ROUND_ROBIN_Joiner_3083();
			iDCT8x8_1D_col_fast_2886();
		WEIGHTED_ROUND_ROBIN_Joiner_2935();
		AnonFilter_a2_2887();
	ENDFOR
	return EXIT_SUCCESS;
}
