#include "PEG63-iDCTcompare.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_split[3];
buffer_int_t SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_1063iDCT8x8_1D_col_fast_856;
buffer_int_t AnonFilter_a0_829DUPLICATE_Splitter_904;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[63];
buffer_float_t Pre_CollapsedDataParallel_1_902WEIGHTED_ROUND_ROBIN_Splitter_977;
buffer_float_t Post_CollapsedDataParallel_2_903WEIGHTED_ROUND_ROBIN_Splitter_987;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_988WEIGHTED_ROUND_ROBIN_Splitter_997;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[8];
buffer_int_t SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[63];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[63];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_978Post_CollapsedDataParallel_2_903;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[63];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_905AnonFilter_a2_857;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_913Pre_CollapsedDataParallel_1_902;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[8];


iDCT_2D_reference_coarse_832_t iDCT_2D_reference_coarse_832_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_979_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_980_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_981_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_982_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_983_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_984_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_985_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_986_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_989_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_990_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_991_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_992_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_993_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_994_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_995_s;
iDCT_2D_reference_coarse_832_t iDCT_1D_reference_fine_996_s;
iDCT8x8_1D_col_fast_856_t iDCT8x8_1D_col_fast_856_s;
AnonFilter_a2_857_t AnonFilter_a2_857_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_829() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_829DUPLICATE_Splitter_904));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_832_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_832_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_832() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_914() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[0]));
	ENDFOR
}

void AnonFilter_a3_915() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[1]));
	ENDFOR
}

void AnonFilter_a3_916() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[2]));
	ENDFOR
}

void AnonFilter_a3_917() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[3]));
	ENDFOR
}

void AnonFilter_a3_918() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[4]));
	ENDFOR
}

void AnonFilter_a3_919() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[5]));
	ENDFOR
}

void AnonFilter_a3_920() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[6]));
	ENDFOR
}

void AnonFilter_a3_921() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[7]));
	ENDFOR
}

void AnonFilter_a3_922() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[8]));
	ENDFOR
}

void AnonFilter_a3_923() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[9]));
	ENDFOR
}

void AnonFilter_a3_924() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[10]));
	ENDFOR
}

void AnonFilter_a3_925() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[11]));
	ENDFOR
}

void AnonFilter_a3_926() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[12]));
	ENDFOR
}

void AnonFilter_a3_927() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[13]));
	ENDFOR
}

void AnonFilter_a3_928() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[14]));
	ENDFOR
}

void AnonFilter_a3_929() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[15]));
	ENDFOR
}

void AnonFilter_a3_930() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[16]));
	ENDFOR
}

void AnonFilter_a3_931() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[17]));
	ENDFOR
}

void AnonFilter_a3_932() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[18]));
	ENDFOR
}

void AnonFilter_a3_933() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[19]));
	ENDFOR
}

void AnonFilter_a3_934() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[20]));
	ENDFOR
}

void AnonFilter_a3_935() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[21]));
	ENDFOR
}

void AnonFilter_a3_936() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[22]));
	ENDFOR
}

void AnonFilter_a3_937() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[23]));
	ENDFOR
}

void AnonFilter_a3_938() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[24]));
	ENDFOR
}

void AnonFilter_a3_939() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[25]));
	ENDFOR
}

void AnonFilter_a3_940() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[26]));
	ENDFOR
}

void AnonFilter_a3_941() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[27]));
	ENDFOR
}

void AnonFilter_a3_942() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[28]));
	ENDFOR
}

void AnonFilter_a3_943() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[29]));
	ENDFOR
}

void AnonFilter_a3_944() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[30]));
	ENDFOR
}

void AnonFilter_a3_945() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[31]));
	ENDFOR
}

void AnonFilter_a3_946() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[32]));
	ENDFOR
}

void AnonFilter_a3_947() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[33]));
	ENDFOR
}

void AnonFilter_a3_948() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[34]));
	ENDFOR
}

void AnonFilter_a3_949() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[35]));
	ENDFOR
}

void AnonFilter_a3_950() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[36]));
	ENDFOR
}

void AnonFilter_a3_951() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[37]));
	ENDFOR
}

void AnonFilter_a3_952() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[38]));
	ENDFOR
}

void AnonFilter_a3_953() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[39]));
	ENDFOR
}

void AnonFilter_a3_954() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[40]));
	ENDFOR
}

void AnonFilter_a3_955() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[41]));
	ENDFOR
}

void AnonFilter_a3_956() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[42]));
	ENDFOR
}

void AnonFilter_a3_957() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[43]));
	ENDFOR
}

void AnonFilter_a3_958() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[44]));
	ENDFOR
}

void AnonFilter_a3_959() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[45]));
	ENDFOR
}

void AnonFilter_a3_960() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[46]));
	ENDFOR
}

void AnonFilter_a3_961() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[47]));
	ENDFOR
}

void AnonFilter_a3_962() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[48]));
	ENDFOR
}

void AnonFilter_a3_963() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[49]));
	ENDFOR
}

void AnonFilter_a3_964() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[50]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[50]));
	ENDFOR
}

void AnonFilter_a3_965() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[51]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[51]));
	ENDFOR
}

void AnonFilter_a3_966() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[52]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[52]));
	ENDFOR
}

void AnonFilter_a3_967() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[53]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[53]));
	ENDFOR
}

void AnonFilter_a3_968() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[54]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[54]));
	ENDFOR
}

void AnonFilter_a3_969() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[55]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[55]));
	ENDFOR
}

void AnonFilter_a3_970() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[56]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[56]));
	ENDFOR
}

void AnonFilter_a3_971() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[57]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[57]));
	ENDFOR
}

void AnonFilter_a3_972() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[58]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[58]));
	ENDFOR
}

void AnonFilter_a3_973() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[59]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[59]));
	ENDFOR
}

void AnonFilter_a3_974() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[60]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[60]));
	ENDFOR
}

void AnonFilter_a3_975() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[61]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[61]));
	ENDFOR
}

void AnonFilter_a3_976() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[62]), &(SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[62]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_912() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_913() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_913Pre_CollapsedDataParallel_1_902, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_902() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_913Pre_CollapsedDataParallel_1_902), &(Pre_CollapsedDataParallel_1_902WEIGHTED_ROUND_ROBIN_Splitter_977));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_979_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_979() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_980() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_981() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_982() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_983() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_984() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_985() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_986() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_977() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_902WEIGHTED_ROUND_ROBIN_Splitter_977));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_978() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_978Post_CollapsedDataParallel_2_903, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_903() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_978Post_CollapsedDataParallel_2_903), &(Post_CollapsedDataParallel_2_903WEIGHTED_ROUND_ROBIN_Splitter_987));
	ENDFOR
}

void iDCT_1D_reference_fine_989() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_990() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_991() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_992() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_993() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_994() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_995() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_996() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_987() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_903WEIGHTED_ROUND_ROBIN_Splitter_987));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_988() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_988WEIGHTED_ROUND_ROBIN_Splitter_997, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_999() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[0]));
	ENDFOR
}

void AnonFilter_a4_1000() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[1]));
	ENDFOR
}

void AnonFilter_a4_1001() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[2]));
	ENDFOR
}

void AnonFilter_a4_1002() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[3]));
	ENDFOR
}

void AnonFilter_a4_1003() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[4]));
	ENDFOR
}

void AnonFilter_a4_1004() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[5]));
	ENDFOR
}

void AnonFilter_a4_1005() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[6]));
	ENDFOR
}

void AnonFilter_a4_1006() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[7]));
	ENDFOR
}

void AnonFilter_a4_1007() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[8]));
	ENDFOR
}

void AnonFilter_a4_1008() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[9]));
	ENDFOR
}

void AnonFilter_a4_1009() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[10]));
	ENDFOR
}

void AnonFilter_a4_1010() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[11]));
	ENDFOR
}

void AnonFilter_a4_1011() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[12]));
	ENDFOR
}

void AnonFilter_a4_1012() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[13]));
	ENDFOR
}

void AnonFilter_a4_1013() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[14]));
	ENDFOR
}

void AnonFilter_a4_1014() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[15]));
	ENDFOR
}

void AnonFilter_a4_1015() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[16]));
	ENDFOR
}

void AnonFilter_a4_1016() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[17]));
	ENDFOR
}

void AnonFilter_a4_1017() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[18]));
	ENDFOR
}

void AnonFilter_a4_1018() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[19]));
	ENDFOR
}

void AnonFilter_a4_1019() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[20]));
	ENDFOR
}

void AnonFilter_a4_1020() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[21]));
	ENDFOR
}

void AnonFilter_a4_1021() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[22]));
	ENDFOR
}

void AnonFilter_a4_1022() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[23]));
	ENDFOR
}

void AnonFilter_a4_1023() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[24]));
	ENDFOR
}

void AnonFilter_a4_1024() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[25]));
	ENDFOR
}

void AnonFilter_a4_1025() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[26]));
	ENDFOR
}

void AnonFilter_a4_1026() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[27]));
	ENDFOR
}

void AnonFilter_a4_1027() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[28]));
	ENDFOR
}

void AnonFilter_a4_1028() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[29]));
	ENDFOR
}

void AnonFilter_a4_1029() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[30]));
	ENDFOR
}

void AnonFilter_a4_1030() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[31]));
	ENDFOR
}

void AnonFilter_a4_1031() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[32]));
	ENDFOR
}

void AnonFilter_a4_1032() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[33]));
	ENDFOR
}

void AnonFilter_a4_1033() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[34]));
	ENDFOR
}

void AnonFilter_a4_1034() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[35]));
	ENDFOR
}

void AnonFilter_a4_1035() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[36]));
	ENDFOR
}

void AnonFilter_a4_1036() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[37]));
	ENDFOR
}

void AnonFilter_a4_1037() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[38]));
	ENDFOR
}

void AnonFilter_a4_1038() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[39]));
	ENDFOR
}

void AnonFilter_a4_1039() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[40]));
	ENDFOR
}

void AnonFilter_a4_1040() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[41]));
	ENDFOR
}

void AnonFilter_a4_1041() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[42]));
	ENDFOR
}

void AnonFilter_a4_1042() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[43]));
	ENDFOR
}

void AnonFilter_a4_1043() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[44]));
	ENDFOR
}

void AnonFilter_a4_1044() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[45]));
	ENDFOR
}

void AnonFilter_a4_1045() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[46]));
	ENDFOR
}

void AnonFilter_a4_1046() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[47]));
	ENDFOR
}

void AnonFilter_a4_1047() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[48]));
	ENDFOR
}

void AnonFilter_a4_1048() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[49]));
	ENDFOR
}

void AnonFilter_a4_1049() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[50]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[50]));
	ENDFOR
}

void AnonFilter_a4_1050() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[51]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[51]));
	ENDFOR
}

void AnonFilter_a4_1051() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[52]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[52]));
	ENDFOR
}

void AnonFilter_a4_1052() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[53]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[53]));
	ENDFOR
}

void AnonFilter_a4_1053() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[54]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[54]));
	ENDFOR
}

void AnonFilter_a4_1054() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[55]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[55]));
	ENDFOR
}

void AnonFilter_a4_1055() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[56]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[56]));
	ENDFOR
}

void AnonFilter_a4_1056() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[57]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[57]));
	ENDFOR
}

void AnonFilter_a4_1057() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[58]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[58]));
	ENDFOR
}

void AnonFilter_a4_1058() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[59]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[59]));
	ENDFOR
}

void AnonFilter_a4_1059() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[60]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[60]));
	ENDFOR
}

void AnonFilter_a4_1060() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[61]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[61]));
	ENDFOR
}

void AnonFilter_a4_1061() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[62]), &(SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[62]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_997() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_988WEIGHTED_ROUND_ROBIN_Splitter_997));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_998() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_1064() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[0]), &(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1065() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[1]), &(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1066() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[2]), &(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1067() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[3]), &(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1068() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[4]), &(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1069() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[5]), &(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1070() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[6]), &(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1071() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[7]), &(SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1062() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1063() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_1063iDCT8x8_1D_col_fast_856, pop_int(&SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_856_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_856_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_856_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_856_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_856_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_856_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_856_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_856_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_856_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_856_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_856() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_1063iDCT8x8_1D_col_fast_856), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_904() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4032, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_829DUPLICATE_Splitter_904);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_905() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4032, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_905AnonFilter_a2_857, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_857_s.count = (AnonFilter_a2_857_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_857_s.errors = (AnonFilter_a2_857_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_857_s.errors / AnonFilter_a2_857_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_857_s.errors = (AnonFilter_a2_857_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_857_s.errors / AnonFilter_a2_857_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_857() {
	FOR(uint32_t, __iter_steady_, 0, <, 4032, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_905AnonFilter_a2_857));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_join[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_1063iDCT8x8_1D_col_fast_856);
	init_buffer_int(&AnonFilter_a0_829DUPLICATE_Splitter_904);
	FOR(int, __iter_init_2_, 0, <, 63, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_1076_1083_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_902WEIGHTED_ROUND_ROBIN_Splitter_977);
	init_buffer_float(&Post_CollapsedDataParallel_2_903WEIGHTED_ROUND_ROBIN_Splitter_987);
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_830_906_1072_1079_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_988WEIGHTED_ROUND_ROBIN_Splitter_997);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_int(&SplitJoin150_iDCT8x8_1D_row_fast_Fiss_1077_1084_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 63, __iter_init_7_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_1073_1080_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1074_1081_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 63, __iter_init_9_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_1073_1080_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_978Post_CollapsedDataParallel_2_903);
	FOR(int, __iter_init_10_, 0, <, 63, __iter_init_10_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_1076_1083_join[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_905AnonFilter_a2_857);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_913Pre_CollapsedDataParallel_1_902);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1075_1082_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_832
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_832_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_979
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_979_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_980
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_980_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_981
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_981_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_982
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_982_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_983
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_983_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_984
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_984_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_985
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_985_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_986
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_986_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_989
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_989_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_990
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_990_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_991
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_991_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_992
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_992_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_993
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_993_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_994
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_994_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_995
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_995_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_996
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_996_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_857
	 {
	AnonFilter_a2_857_s.count = 0.0 ; 
	AnonFilter_a2_857_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_829();
		DUPLICATE_Splitter_904();
			iDCT_2D_reference_coarse_832();
			WEIGHTED_ROUND_ROBIN_Splitter_912();
				AnonFilter_a3_914();
				AnonFilter_a3_915();
				AnonFilter_a3_916();
				AnonFilter_a3_917();
				AnonFilter_a3_918();
				AnonFilter_a3_919();
				AnonFilter_a3_920();
				AnonFilter_a3_921();
				AnonFilter_a3_922();
				AnonFilter_a3_923();
				AnonFilter_a3_924();
				AnonFilter_a3_925();
				AnonFilter_a3_926();
				AnonFilter_a3_927();
				AnonFilter_a3_928();
				AnonFilter_a3_929();
				AnonFilter_a3_930();
				AnonFilter_a3_931();
				AnonFilter_a3_932();
				AnonFilter_a3_933();
				AnonFilter_a3_934();
				AnonFilter_a3_935();
				AnonFilter_a3_936();
				AnonFilter_a3_937();
				AnonFilter_a3_938();
				AnonFilter_a3_939();
				AnonFilter_a3_940();
				AnonFilter_a3_941();
				AnonFilter_a3_942();
				AnonFilter_a3_943();
				AnonFilter_a3_944();
				AnonFilter_a3_945();
				AnonFilter_a3_946();
				AnonFilter_a3_947();
				AnonFilter_a3_948();
				AnonFilter_a3_949();
				AnonFilter_a3_950();
				AnonFilter_a3_951();
				AnonFilter_a3_952();
				AnonFilter_a3_953();
				AnonFilter_a3_954();
				AnonFilter_a3_955();
				AnonFilter_a3_956();
				AnonFilter_a3_957();
				AnonFilter_a3_958();
				AnonFilter_a3_959();
				AnonFilter_a3_960();
				AnonFilter_a3_961();
				AnonFilter_a3_962();
				AnonFilter_a3_963();
				AnonFilter_a3_964();
				AnonFilter_a3_965();
				AnonFilter_a3_966();
				AnonFilter_a3_967();
				AnonFilter_a3_968();
				AnonFilter_a3_969();
				AnonFilter_a3_970();
				AnonFilter_a3_971();
				AnonFilter_a3_972();
				AnonFilter_a3_973();
				AnonFilter_a3_974();
				AnonFilter_a3_975();
				AnonFilter_a3_976();
			WEIGHTED_ROUND_ROBIN_Joiner_913();
			Pre_CollapsedDataParallel_1_902();
			WEIGHTED_ROUND_ROBIN_Splitter_977();
				iDCT_1D_reference_fine_979();
				iDCT_1D_reference_fine_980();
				iDCT_1D_reference_fine_981();
				iDCT_1D_reference_fine_982();
				iDCT_1D_reference_fine_983();
				iDCT_1D_reference_fine_984();
				iDCT_1D_reference_fine_985();
				iDCT_1D_reference_fine_986();
			WEIGHTED_ROUND_ROBIN_Joiner_978();
			Post_CollapsedDataParallel_2_903();
			WEIGHTED_ROUND_ROBIN_Splitter_987();
				iDCT_1D_reference_fine_989();
				iDCT_1D_reference_fine_990();
				iDCT_1D_reference_fine_991();
				iDCT_1D_reference_fine_992();
				iDCT_1D_reference_fine_993();
				iDCT_1D_reference_fine_994();
				iDCT_1D_reference_fine_995();
				iDCT_1D_reference_fine_996();
			WEIGHTED_ROUND_ROBIN_Joiner_988();
			WEIGHTED_ROUND_ROBIN_Splitter_997();
				AnonFilter_a4_999();
				AnonFilter_a4_1000();
				AnonFilter_a4_1001();
				AnonFilter_a4_1002();
				AnonFilter_a4_1003();
				AnonFilter_a4_1004();
				AnonFilter_a4_1005();
				AnonFilter_a4_1006();
				AnonFilter_a4_1007();
				AnonFilter_a4_1008();
				AnonFilter_a4_1009();
				AnonFilter_a4_1010();
				AnonFilter_a4_1011();
				AnonFilter_a4_1012();
				AnonFilter_a4_1013();
				AnonFilter_a4_1014();
				AnonFilter_a4_1015();
				AnonFilter_a4_1016();
				AnonFilter_a4_1017();
				AnonFilter_a4_1018();
				AnonFilter_a4_1019();
				AnonFilter_a4_1020();
				AnonFilter_a4_1021();
				AnonFilter_a4_1022();
				AnonFilter_a4_1023();
				AnonFilter_a4_1024();
				AnonFilter_a4_1025();
				AnonFilter_a4_1026();
				AnonFilter_a4_1027();
				AnonFilter_a4_1028();
				AnonFilter_a4_1029();
				AnonFilter_a4_1030();
				AnonFilter_a4_1031();
				AnonFilter_a4_1032();
				AnonFilter_a4_1033();
				AnonFilter_a4_1034();
				AnonFilter_a4_1035();
				AnonFilter_a4_1036();
				AnonFilter_a4_1037();
				AnonFilter_a4_1038();
				AnonFilter_a4_1039();
				AnonFilter_a4_1040();
				AnonFilter_a4_1041();
				AnonFilter_a4_1042();
				AnonFilter_a4_1043();
				AnonFilter_a4_1044();
				AnonFilter_a4_1045();
				AnonFilter_a4_1046();
				AnonFilter_a4_1047();
				AnonFilter_a4_1048();
				AnonFilter_a4_1049();
				AnonFilter_a4_1050();
				AnonFilter_a4_1051();
				AnonFilter_a4_1052();
				AnonFilter_a4_1053();
				AnonFilter_a4_1054();
				AnonFilter_a4_1055();
				AnonFilter_a4_1056();
				AnonFilter_a4_1057();
				AnonFilter_a4_1058();
				AnonFilter_a4_1059();
				AnonFilter_a4_1060();
				AnonFilter_a4_1061();
			WEIGHTED_ROUND_ROBIN_Joiner_998();
			WEIGHTED_ROUND_ROBIN_Splitter_1062();
				iDCT8x8_1D_row_fast_1064();
				iDCT8x8_1D_row_fast_1065();
				iDCT8x8_1D_row_fast_1066();
				iDCT8x8_1D_row_fast_1067();
				iDCT8x8_1D_row_fast_1068();
				iDCT8x8_1D_row_fast_1069();
				iDCT8x8_1D_row_fast_1070();
				iDCT8x8_1D_row_fast_1071();
			WEIGHTED_ROUND_ROBIN_Joiner_1063();
			iDCT8x8_1D_col_fast_856();
		WEIGHTED_ROUND_ROBIN_Joiner_905();
		AnonFilter_a2_857();
	ENDFOR
	return EXIT_SUCCESS;
}
