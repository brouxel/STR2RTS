#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=5760 on the compile command line
#else
#if BUF_SIZEMAX < 5760
#error BUF_SIZEMAX too small, it must be at least 5760
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_12382_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_12406_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_12407_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_12379();
void DUPLICATE_Splitter_12454();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_12382();
void WEIGHTED_ROUND_ROBIN_Splitter_12462();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_12464();
void AnonFilter_a3_12465();
void AnonFilter_a3_12466();
void AnonFilter_a3_12467();
void AnonFilter_a3_12468();
void AnonFilter_a3_12469();
void AnonFilter_a3_12470();
void AnonFilter_a3_12471();
void AnonFilter_a3_12472();
void AnonFilter_a3_12473();
void AnonFilter_a3_12474();
void AnonFilter_a3_12475();
void AnonFilter_a3_12476();
void AnonFilter_a3_12477();
void AnonFilter_a3_12478();
void AnonFilter_a3_12479();
void AnonFilter_a3_12480();
void AnonFilter_a3_12481();
void AnonFilter_a3_12482();
void AnonFilter_a3_12483();
void AnonFilter_a3_12484();
void AnonFilter_a3_12485();
void AnonFilter_a3_12486();
void AnonFilter_a3_12487();
void AnonFilter_a3_12488();
void AnonFilter_a3_12489();
void AnonFilter_a3_12490();
void AnonFilter_a3_12491();
void AnonFilter_a3_12492();
void AnonFilter_a3_12493();
void WEIGHTED_ROUND_ROBIN_Joiner_12463();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_12452();
void WEIGHTED_ROUND_ROBIN_Splitter_12494();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_12496();
void iDCT_1D_reference_fine_12497();
void iDCT_1D_reference_fine_12498();
void iDCT_1D_reference_fine_12499();
void iDCT_1D_reference_fine_12500();
void iDCT_1D_reference_fine_12501();
void iDCT_1D_reference_fine_12502();
void iDCT_1D_reference_fine_12503();
void WEIGHTED_ROUND_ROBIN_Joiner_12495();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_12453();
void WEIGHTED_ROUND_ROBIN_Splitter_12504();
void iDCT_1D_reference_fine_12506();
void iDCT_1D_reference_fine_12507();
void iDCT_1D_reference_fine_12508();
void iDCT_1D_reference_fine_12509();
void iDCT_1D_reference_fine_12510();
void iDCT_1D_reference_fine_12511();
void iDCT_1D_reference_fine_12512();
void iDCT_1D_reference_fine_12513();
void WEIGHTED_ROUND_ROBIN_Joiner_12505();
void WEIGHTED_ROUND_ROBIN_Splitter_12514();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_12516();
void AnonFilter_a4_12517();
void AnonFilter_a4_12518();
void AnonFilter_a4_12519();
void AnonFilter_a4_12520();
void AnonFilter_a4_12521();
void AnonFilter_a4_12522();
void AnonFilter_a4_12523();
void AnonFilter_a4_12524();
void AnonFilter_a4_12525();
void AnonFilter_a4_12526();
void AnonFilter_a4_12527();
void AnonFilter_a4_12528();
void AnonFilter_a4_12529();
void AnonFilter_a4_12530();
void AnonFilter_a4_12531();
void AnonFilter_a4_12532();
void AnonFilter_a4_12533();
void AnonFilter_a4_12534();
void AnonFilter_a4_12535();
void AnonFilter_a4_12536();
void AnonFilter_a4_12537();
void AnonFilter_a4_12538();
void AnonFilter_a4_12539();
void AnonFilter_a4_12540();
void AnonFilter_a4_12541();
void AnonFilter_a4_12542();
void AnonFilter_a4_12543();
void AnonFilter_a4_12544();
void AnonFilter_a4_12545();
void WEIGHTED_ROUND_ROBIN_Joiner_12515();
void WEIGHTED_ROUND_ROBIN_Splitter_12546();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_12548();
void iDCT8x8_1D_row_fast_12549();
void iDCT8x8_1D_row_fast_12550();
void iDCT8x8_1D_row_fast_12551();
void iDCT8x8_1D_row_fast_12552();
void iDCT8x8_1D_row_fast_12553();
void iDCT8x8_1D_row_fast_12554();
void iDCT8x8_1D_row_fast_12555();
void WEIGHTED_ROUND_ROBIN_Joiner_12547();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_12406();
void WEIGHTED_ROUND_ROBIN_Joiner_12455();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_12407();

#ifdef __cplusplus
}
#endif
#endif
