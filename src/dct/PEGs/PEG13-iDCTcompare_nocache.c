#include "PEG13-iDCTcompare_nocache.h"

buffer_float_t Pre_CollapsedDataParallel_1_16702WEIGHTED_ROUND_ROBIN_Splitter_16727;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16705AnonFilter_a2_16657;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16713Pre_CollapsedDataParallel_1_16702;
buffer_int_t SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[8];
buffer_int_t SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[8];
buffer_float_t Post_CollapsedDataParallel_2_16703WEIGHTED_ROUND_ROBIN_Splitter_16737;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[13];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16738WEIGHTED_ROUND_ROBIN_Splitter_16747;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[13];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_join[3];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_split[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16728Post_CollapsedDataParallel_2_16703;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[13];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[13];
buffer_int_t AnonFilter_a0_16629DUPLICATE_Splitter_16704;


iDCT_2D_reference_coarse_16632_t iDCT_2D_reference_coarse_16632_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16729_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16730_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16731_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16732_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16733_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16734_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16735_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16736_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16739_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16740_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16741_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16742_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16743_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16744_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16745_s;
iDCT_2D_reference_coarse_16632_t iDCT_1D_reference_fine_16746_s;
iDCT8x8_1D_col_fast_16656_t iDCT8x8_1D_col_fast_16656_s;
AnonFilter_a2_16657_t AnonFilter_a2_16657_s;

void AnonFilter_a0_16629(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_16629DUPLICATE_Splitter_16704, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_16632(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_16632_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_16632_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_16714(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16715(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16716(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16717(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16718(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16719(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16720(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16721(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16722(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16723(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16724(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16725(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_16726(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[12])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16712() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16713() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16713Pre_CollapsedDataParallel_1_16702, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_16702(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_16702WEIGHTED_ROUND_ROBIN_Splitter_16727, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_16713Pre_CollapsedDataParallel_1_16702, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16713Pre_CollapsedDataParallel_1_16702) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16729(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16729_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16730(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16730_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16731(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16731_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16732(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16732_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16733(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16733_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16734(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16734_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16735(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16735_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16736(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16736_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16727() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_16702WEIGHTED_ROUND_ROBIN_Splitter_16727));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16728() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16728Post_CollapsedDataParallel_2_16703, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_16703(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_16703WEIGHTED_ROUND_ROBIN_Splitter_16737, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_16728Post_CollapsedDataParallel_2_16703, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16728Post_CollapsedDataParallel_2_16703) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16739(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16739_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16740(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16740_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16741(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16741_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16742(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16742_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16743(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16743_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16744(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16744_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16745(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16745_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_16746(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16746_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16737() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_16703WEIGHTED_ROUND_ROBIN_Splitter_16737));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16738() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16738WEIGHTED_ROUND_ROBIN_Splitter_16747, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_16749(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16750(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16751(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16752(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16753(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16754(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16755(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16756(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16757(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16758(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16759(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16760(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_16761(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16747() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16738WEIGHTED_ROUND_ROBIN_Splitter_16747));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16748() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_16764(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0], 6) ; 
		x3 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0], 2) ; 
		x4 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0], 1) ; 
		x5 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0], 7) ; 
		x6 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0], 5) ; 
		x7 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16765(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1], 6) ; 
		x3 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1], 2) ; 
		x4 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1], 1) ; 
		x5 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1], 7) ; 
		x6 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1], 5) ; 
		x7 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16766(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2], 6) ; 
		x3 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2], 2) ; 
		x4 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2], 1) ; 
		x5 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2], 7) ; 
		x6 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2], 5) ; 
		x7 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16767(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3], 6) ; 
		x3 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3], 2) ; 
		x4 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3], 1) ; 
		x5 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3], 7) ; 
		x6 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3], 5) ; 
		x7 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16768(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4], 6) ; 
		x3 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4], 2) ; 
		x4 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4], 1) ; 
		x5 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4], 7) ; 
		x6 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4], 5) ; 
		x7 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16769(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5], 6) ; 
		x3 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5], 2) ; 
		x4 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5], 1) ; 
		x5 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5], 7) ; 
		x6 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5], 5) ; 
		x7 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16770(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6], 6) ; 
		x3 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6], 2) ; 
		x4 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6], 1) ; 
		x5 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6], 7) ; 
		x6 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6], 5) ; 
		x7 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_16771(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7], 6) ; 
		x3 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7], 2) ; 
		x4 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7], 1) ; 
		x5 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7], 7) ; 
		x6 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7], 5) ; 
		x7 = peek_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16762() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16763() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, pop_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_16656(){
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_16656_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_16656_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_16656_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_16656_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_16656_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_16656_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_16656_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_16656_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_16656_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_join[2], iDCT8x8_1D_col_fast_16656_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_16704() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_16629DUPLICATE_Splitter_16704);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16705() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16705AnonFilter_a2_16657, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_16657(){
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16705AnonFilter_a2_16657) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16705AnonFilter_a2_16657) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_16705AnonFilter_a2_16657) ; 
		AnonFilter_a2_16657_s.count = (AnonFilter_a2_16657_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_16657_s.errors = (AnonFilter_a2_16657_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_16657_s.errors / AnonFilter_a2_16657_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_16657_s.errors = (AnonFilter_a2_16657_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_16657_s.errors / AnonFilter_a2_16657_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Pre_CollapsedDataParallel_1_16702WEIGHTED_ROUND_ROBIN_Splitter_16727);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16705AnonFilter_a2_16657);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16713Pre_CollapsedDataParallel_1_16702);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin50_iDCT8x8_1D_row_fast_Fiss_16777_16784_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_16703WEIGHTED_ROUND_ROBIN_Splitter_16737);
	FOR(int, __iter_init_2_, 0, <, 13, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16738WEIGHTED_ROUND_ROBIN_Splitter_16747);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_split[__iter_init_4_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16763iDCT8x8_1D_col_fast_16656);
	FOR(int, __iter_init_5_, 0, <, 13, __iter_init_5_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16630_16706_16772_16779_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16774_16781_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16775_16782_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16728Post_CollapsedDataParallel_2_16703);
	FOR(int, __iter_init_10_, 0, <, 13, __iter_init_10_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_16776_16783_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 13, __iter_init_11_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_16773_16780_split[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_16629DUPLICATE_Splitter_16704);
// --- init: iDCT_2D_reference_coarse_16632
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_16632_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16729
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16729_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16730
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16730_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16731
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16731_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16732
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16732_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16733
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16733_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16734
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16734_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16735
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16735_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16736
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16736_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16739
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16739_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16740
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16740_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16741
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16741_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16742
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16742_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16743
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16743_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16744
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16744_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16745
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16745_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16746
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16746_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_16657
	 {
	AnonFilter_a2_16657_s.count = 0.0 ; 
	AnonFilter_a2_16657_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_16629();
		DUPLICATE_Splitter_16704();
			iDCT_2D_reference_coarse_16632();
			WEIGHTED_ROUND_ROBIN_Splitter_16712();
				AnonFilter_a3_16714();
				AnonFilter_a3_16715();
				AnonFilter_a3_16716();
				AnonFilter_a3_16717();
				AnonFilter_a3_16718();
				AnonFilter_a3_16719();
				AnonFilter_a3_16720();
				AnonFilter_a3_16721();
				AnonFilter_a3_16722();
				AnonFilter_a3_16723();
				AnonFilter_a3_16724();
				AnonFilter_a3_16725();
				AnonFilter_a3_16726();
			WEIGHTED_ROUND_ROBIN_Joiner_16713();
			Pre_CollapsedDataParallel_1_16702();
			WEIGHTED_ROUND_ROBIN_Splitter_16727();
				iDCT_1D_reference_fine_16729();
				iDCT_1D_reference_fine_16730();
				iDCT_1D_reference_fine_16731();
				iDCT_1D_reference_fine_16732();
				iDCT_1D_reference_fine_16733();
				iDCT_1D_reference_fine_16734();
				iDCT_1D_reference_fine_16735();
				iDCT_1D_reference_fine_16736();
			WEIGHTED_ROUND_ROBIN_Joiner_16728();
			Post_CollapsedDataParallel_2_16703();
			WEIGHTED_ROUND_ROBIN_Splitter_16737();
				iDCT_1D_reference_fine_16739();
				iDCT_1D_reference_fine_16740();
				iDCT_1D_reference_fine_16741();
				iDCT_1D_reference_fine_16742();
				iDCT_1D_reference_fine_16743();
				iDCT_1D_reference_fine_16744();
				iDCT_1D_reference_fine_16745();
				iDCT_1D_reference_fine_16746();
			WEIGHTED_ROUND_ROBIN_Joiner_16738();
			WEIGHTED_ROUND_ROBIN_Splitter_16747();
				AnonFilter_a4_16749();
				AnonFilter_a4_16750();
				AnonFilter_a4_16751();
				AnonFilter_a4_16752();
				AnonFilter_a4_16753();
				AnonFilter_a4_16754();
				AnonFilter_a4_16755();
				AnonFilter_a4_16756();
				AnonFilter_a4_16757();
				AnonFilter_a4_16758();
				AnonFilter_a4_16759();
				AnonFilter_a4_16760();
				AnonFilter_a4_16761();
			WEIGHTED_ROUND_ROBIN_Joiner_16748();
			WEIGHTED_ROUND_ROBIN_Splitter_16762();
				iDCT8x8_1D_row_fast_16764();
				iDCT8x8_1D_row_fast_16765();
				iDCT8x8_1D_row_fast_16766();
				iDCT8x8_1D_row_fast_16767();
				iDCT8x8_1D_row_fast_16768();
				iDCT8x8_1D_row_fast_16769();
				iDCT8x8_1D_row_fast_16770();
				iDCT8x8_1D_row_fast_16771();
			WEIGHTED_ROUND_ROBIN_Joiner_16763();
			iDCT8x8_1D_col_fast_16656();
		WEIGHTED_ROUND_ROBIN_Joiner_16705();
		AnonFilter_a2_16657();
	ENDFOR
	return EXIT_SUCCESS;
}
