#include "PEG32-iDCTcompare.h"

buffer_float_t Post_CollapsedDataParallel_2_11877WEIGHTED_ROUND_ROBIN_Splitter_11930;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_11879AnonFilter_a2_11831;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11887Pre_CollapsedDataParallel_1_11876;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[32];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11931WEIGHTED_ROUND_ROBIN_Splitter_11940;
buffer_int_t SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_split[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[8];
buffer_int_t SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_11876WEIGHTED_ROUND_ROBIN_Splitter_11920;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[32];
buffer_int_t AnonFilter_a0_11803DUPLICATE_Splitter_11878;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[32];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_11975iDCT8x8_1D_col_fast_11830;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[32];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11921Post_CollapsedDataParallel_2_11877;


iDCT_2D_reference_coarse_11806_t iDCT_2D_reference_coarse_11806_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11922_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11923_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11924_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11925_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11926_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11927_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11928_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11929_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11932_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11933_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11934_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11935_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11936_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11937_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11938_s;
iDCT_2D_reference_coarse_11806_t iDCT_1D_reference_fine_11939_s;
iDCT8x8_1D_col_fast_11830_t iDCT8x8_1D_col_fast_11830_s;
AnonFilter_a2_11831_t AnonFilter_a2_11831_s;

void AnonFilter_a0(buffer_int_t *chanout) {
	FOR(int, i, 0,  < , 64, i++) {
		push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
	}
	ENDFOR
}


void AnonFilter_a0_11803() {
	AnonFilter_a0(&(AnonFilter_a0_11803DUPLICATE_Splitter_11878));
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
	float block_x[8][8];
	FOR(int, i, 0,  < , 8, i++) {
		FOR(int, j, 0,  < , 8, j++) {
			block_x[i][j] = 0.0 ; 
			FOR(int, k, 0,  < , 8, k++) {
				block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_11806_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
			}
			ENDFOR
		}
		ENDFOR
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		FOR(int, j, 0,  < , 8, j++) {
			float block_y = 0.0;
			FOR(int, k, 0,  < , 8, k++) {
				block_y = (block_y + (iDCT_2D_reference_coarse_11806_s.coeff[k][i] * block_x[k][j])) ; 
			}
			ENDFOR
			block_y = ((float) floor((block_y + 0.5))) ; 
			push_int(&(*chanout), ((int) block_y)) ; 
		}
		ENDFOR
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_int(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT_2D_reference_coarse_11806() {
	iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_join[0]));
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_11888() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[0]));
	ENDFOR
}

void AnonFilter_a3_11889() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[1]));
	ENDFOR
}

void AnonFilter_a3_11890() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[2]));
	ENDFOR
}

void AnonFilter_a3_11891() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[3]));
	ENDFOR
}

void AnonFilter_a3_11892() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[4]));
	ENDFOR
}

void AnonFilter_a3_11893() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[5]));
	ENDFOR
}

void AnonFilter_a3_11894() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[6]));
	ENDFOR
}

void AnonFilter_a3_11895() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[7]));
	ENDFOR
}

void AnonFilter_a3_11896() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[8]));
	ENDFOR
}

void AnonFilter_a3_11897() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[9]));
	ENDFOR
}

void AnonFilter_a3_11898() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[10]));
	ENDFOR
}

void AnonFilter_a3_11899() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[11]));
	ENDFOR
}

void AnonFilter_a3_11900() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[12]));
	ENDFOR
}

void AnonFilter_a3_11901() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[13]));
	ENDFOR
}

void AnonFilter_a3_11902() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[14]));
	ENDFOR
}

void AnonFilter_a3_11903() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[15]));
	ENDFOR
}

void AnonFilter_a3_11904() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[16]));
	ENDFOR
}

void AnonFilter_a3_11905() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[17]));
	ENDFOR
}

void AnonFilter_a3_11906() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[18]));
	ENDFOR
}

void AnonFilter_a3_11907() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[19]));
	ENDFOR
}

void AnonFilter_a3_11908() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[20]));
	ENDFOR
}

void AnonFilter_a3_11909() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[21]));
	ENDFOR
}

void AnonFilter_a3_11910() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[22]));
	ENDFOR
}

void AnonFilter_a3_11911() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[23]));
	ENDFOR
}

void AnonFilter_a3_11912() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[24]));
	ENDFOR
}

void AnonFilter_a3_11913() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[25]));
	ENDFOR
}

void AnonFilter_a3_11914() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[26]));
	ENDFOR
}

void AnonFilter_a3_11915() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[27]));
	ENDFOR
}

void AnonFilter_a3_11916() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[28]));
	ENDFOR
}

void AnonFilter_a3_11917() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[29]));
	ENDFOR
}

void AnonFilter_a3_11918() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[30]));
	ENDFOR
}

void AnonFilter_a3_11919() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[31]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11886() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11887() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11887Pre_CollapsedDataParallel_1_11876, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
		FOR(int, _i, 0,  < , 8, _i++) {
			push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}
}
	pop_float(&(*chanin)) ; 
}


void Pre_CollapsedDataParallel_1_11876() {
	Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_11887Pre_CollapsedDataParallel_1_11876), &(Pre_CollapsedDataParallel_1_11876WEIGHTED_ROUND_ROBIN_Splitter_11920));
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
	FOR(int, x, 0,  < , 8, x++) {
		float tempsum = 0.0;
		FOR(int, u, 0,  < , 8, u++) {
			tempsum = (tempsum + (iDCT_1D_reference_fine_11922_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
		}
		ENDFOR
		push_float(&(*chanout), tempsum) ; 
	}
	ENDFOR
	FOR(int, u, 0,  < , 8, u++) {
		pop_float(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT_1D_reference_fine_11922() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[0]));
}

void iDCT_1D_reference_fine_11923() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[1]));
}

void iDCT_1D_reference_fine_11924() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[2]));
}

void iDCT_1D_reference_fine_11925() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[3]));
}

void iDCT_1D_reference_fine_11926() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[4]));
}

void iDCT_1D_reference_fine_11927() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[5]));
}

void iDCT_1D_reference_fine_11928() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[6]));
}

void iDCT_1D_reference_fine_11929() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_11920() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11876WEIGHTED_ROUND_ROBIN_Splitter_11920));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_11921() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11921Post_CollapsedDataParallel_2_11877, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 8, _i++) {
			push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
			partialSum_i = (partialSum_i + 8) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}
}
	pop_float(&(*chanin)) ; 
}


void Post_CollapsedDataParallel_2_11877() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11921Post_CollapsedDataParallel_2_11877), &(Post_CollapsedDataParallel_2_11877WEIGHTED_ROUND_ROBIN_Splitter_11930));
}

void iDCT_1D_reference_fine_11932() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[0]));
}

void iDCT_1D_reference_fine_11933() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[1]));
}

void iDCT_1D_reference_fine_11934() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[2]));
}

void iDCT_1D_reference_fine_11935() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[3]));
}

void iDCT_1D_reference_fine_11936() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[4]));
}

void iDCT_1D_reference_fine_11937() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[5]));
}

void iDCT_1D_reference_fine_11938() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[6]));
}

void iDCT_1D_reference_fine_11939() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_11930() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_11877WEIGHTED_ROUND_ROBIN_Splitter_11930));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_11931() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11931WEIGHTED_ROUND_ROBIN_Splitter_11940, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_11942() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[0]));
	ENDFOR
}

void AnonFilter_a4_11943() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[1]));
	ENDFOR
}

void AnonFilter_a4_11944() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[2]));
	ENDFOR
}

void AnonFilter_a4_11945() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[3]));
	ENDFOR
}

void AnonFilter_a4_11946() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[4]));
	ENDFOR
}

void AnonFilter_a4_11947() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[5]));
	ENDFOR
}

void AnonFilter_a4_11948() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[6]));
	ENDFOR
}

void AnonFilter_a4_11949() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[7]));
	ENDFOR
}

void AnonFilter_a4_11950() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[8]));
	ENDFOR
}

void AnonFilter_a4_11951() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[9]));
	ENDFOR
}

void AnonFilter_a4_11952() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[10]));
	ENDFOR
}

void AnonFilter_a4_11953() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[11]));
	ENDFOR
}

void AnonFilter_a4_11954() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[12]));
	ENDFOR
}

void AnonFilter_a4_11955() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[13]));
	ENDFOR
}

void AnonFilter_a4_11956() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[14]));
	ENDFOR
}

void AnonFilter_a4_11957() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[15]));
	ENDFOR
}

void AnonFilter_a4_11958() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[16]));
	ENDFOR
}

void AnonFilter_a4_11959() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[17]));
	ENDFOR
}

void AnonFilter_a4_11960() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[18]));
	ENDFOR
}

void AnonFilter_a4_11961() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[19]));
	ENDFOR
}

void AnonFilter_a4_11962() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[20]));
	ENDFOR
}

void AnonFilter_a4_11963() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[21]));
	ENDFOR
}

void AnonFilter_a4_11964() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[22]));
	ENDFOR
}

void AnonFilter_a4_11965() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[23]));
	ENDFOR
}

void AnonFilter_a4_11966() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[24]));
	ENDFOR
}

void AnonFilter_a4_11967() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[25]));
	ENDFOR
}

void AnonFilter_a4_11968() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[26]));
	ENDFOR
}

void AnonFilter_a4_11969() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[27]));
	ENDFOR
}

void AnonFilter_a4_11970() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[28]));
	ENDFOR
}

void AnonFilter_a4_11971() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[29]));
	ENDFOR
}

void AnonFilter_a4_11972() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[30]));
	ENDFOR
}

void AnonFilter_a4_11973() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[31]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11940() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11931WEIGHTED_ROUND_ROBIN_Splitter_11940));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11941() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
	int x0 = 0;
	int x1 = 0;
	int x2 = 0;
	int x3 = 0;
	int x4 = 0;
	int x5 = 0;
	int x6 = 0;
	int x7 = 0;
	int x8 = 0;
	x0 = peek_int(&(*chanin), 0) ; 
	x1 = (peek_int(&(*chanin), 4) << 11) ; 
	x2 = peek_int(&(*chanin), 6) ; 
	x3 = peek_int(&(*chanin), 2) ; 
	x4 = peek_int(&(*chanin), 1) ; 
	x5 = peek_int(&(*chanin), 7) ; 
	x6 = peek_int(&(*chanin), 5) ; 
	x7 = peek_int(&(*chanin), 3) ; 
	if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
		x0 = (x0 << 3) ; 
		FOR(int, i, 0,  < , 8, i++) {
			push_int(&(*chanout), x0) ; 
		}
		ENDFOR
	}
	else {
		x0 = ((x0 << 11) + 128) ; 
		x8 = (565 * (x4 + x5)) ; 
		x4 = (x8 + (2276 * x4)) ; 
		x5 = (x8 - (3406 * x5)) ; 
		x8 = (2408 * (x6 + x7)) ; 
		x6 = (x8 - (799 * x6)) ; 
		x7 = (x8 - (4017 * x7)) ; 
		x8 = (x0 + x1) ; 
		x0 = (x0 - x1) ; 
		x1 = (1108 * (x3 + x2)) ; 
		x2 = (x1 - (3784 * x2)) ; 
		x3 = (x1 + (1568 * x3)) ; 
		x1 = (x4 + x6) ; 
		x4 = (x4 - x6) ; 
		x6 = (x5 + x7) ; 
		x5 = (x5 - x7) ; 
		x7 = (x8 + x3) ; 
		x8 = (x8 - x3) ; 
		x3 = (x0 + x2) ; 
		x0 = (x0 - x2) ; 
		x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
		x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
		push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
		push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
		push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
		push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
		push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
		push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
		push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
		push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
	}
	FOR(int, i, 0,  < , 8, i++) {
		pop_int(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT8x8_1D_row_fast_11976() {
	iDCT8x8_1D_row_fast(&(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[0]), &(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[0]));
}

void iDCT8x8_1D_row_fast_11977() {
	iDCT8x8_1D_row_fast(&(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[1]), &(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[1]));
}

void iDCT8x8_1D_row_fast_11978() {
	iDCT8x8_1D_row_fast(&(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[2]), &(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[2]));
}

void iDCT8x8_1D_row_fast_11979() {
	iDCT8x8_1D_row_fast(&(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[3]), &(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[3]));
}

void iDCT8x8_1D_row_fast_11980() {
	iDCT8x8_1D_row_fast(&(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[4]), &(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[4]));
}

void iDCT8x8_1D_row_fast_11981() {
	iDCT8x8_1D_row_fast(&(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[5]), &(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[5]));
}

void iDCT8x8_1D_row_fast_11982() {
	iDCT8x8_1D_row_fast(&(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[6]), &(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[6]));
}

void iDCT8x8_1D_row_fast_11983() {
	iDCT8x8_1D_row_fast(&(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[7]), &(SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_11974() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_int(&SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_split[2]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_11975() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_11975iDCT8x8_1D_col_fast_11830, pop_int(&SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
	FOR(int, c, 0,  < , 8, c++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), (c + 0)) ; 
		x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
		x2 = peek_int(&(*chanin), (c + 48)) ; 
		x3 = peek_int(&(*chanin), (c + 16)) ; 
		x4 = peek_int(&(*chanin), (c + 8)) ; 
		x5 = peek_int(&(*chanin), (c + 56)) ; 
		x6 = peek_int(&(*chanin), (c + 40)) ; 
		x7 = peek_int(&(*chanin), (c + 24)) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = ((x0 + 32) >> 6) ; 
			FOR(int, i, 0,  < , 8, i++) {
				iDCT8x8_1D_col_fast_11830_s.buffer[(c + (8 * i))] = x0 ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 8) + 8192) ; 
			x8 = ((565 * (x4 + x5)) + 4) ; 
			x4 = ((x8 + (2276 * x4)) >> 3) ; 
			x5 = ((x8 - (3406 * x5)) >> 3) ; 
			x8 = ((2408 * (x6 + x7)) + 4) ; 
			x6 = ((x8 - (799 * x6)) >> 3) ; 
			x7 = ((x8 - (4017 * x7)) >> 3) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = ((1108 * (x3 + x2)) + 4) ; 
			x2 = ((x1 - (3784 * x2)) >> 3) ; 
			x3 = ((x1 + (1568 * x3)) >> 3) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			iDCT8x8_1D_col_fast_11830_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
			iDCT8x8_1D_col_fast_11830_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
			iDCT8x8_1D_col_fast_11830_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
			iDCT8x8_1D_col_fast_11830_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
			iDCT8x8_1D_col_fast_11830_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
			iDCT8x8_1D_col_fast_11830_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
			iDCT8x8_1D_col_fast_11830_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
			iDCT8x8_1D_col_fast_11830_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
		}
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_int(&(*chanin)) ; 
		push_int(&(*chanout), iDCT8x8_1D_col_fast_11830_s.buffer[i]) ; 
	}
	ENDFOR
}


void iDCT8x8_1D_col_fast_11830() {
	iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_11975iDCT8x8_1D_col_fast_11830), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_join[2]));
}

void DUPLICATE_Splitter_11878() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_11803DUPLICATE_Splitter_11878);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11879() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_11879AnonFilter_a2_11831, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_11831_s.count = (AnonFilter_a2_11831_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_11831_s.errors = (AnonFilter_a2_11831_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_11831_s.errors / AnonFilter_a2_11831_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_11831_s.errors = (AnonFilter_a2_11831_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_11831_s.errors / AnonFilter_a2_11831_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_11831() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_11879AnonFilter_a2_11831));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Post_CollapsedDataParallel_2_11877WEIGHTED_ROUND_ROBIN_Splitter_11930);
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_split[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_11879AnonFilter_a2_11831);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11887Pre_CollapsedDataParallel_1_11876);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 32, __iter_init_3_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_11985_11992_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11931WEIGHTED_ROUND_ROBIN_Splitter_11940);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11804_11880_11984_11991_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11987_11994_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin88_iDCT8x8_1D_row_fast_Fiss_11989_11996_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11876WEIGHTED_ROUND_ROBIN_Splitter_11920);
	FOR(int, __iter_init_8_, 0, <, 32, __iter_init_8_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_11988_11995_split[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_11803DUPLICATE_Splitter_11878);
	FOR(int, __iter_init_9_, 0, <, 32, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_11985_11992_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11986_11993_join[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_11975iDCT8x8_1D_col_fast_11830);
	FOR(int, __iter_init_11_, 0, <, 32, __iter_init_11_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_11988_11995_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11921Post_CollapsedDataParallel_2_11877);
// --- init: iDCT_2D_reference_coarse_11806
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_11806_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11922
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11922_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11923
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11923_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11924
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11924_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11925
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11925_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11926
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11926_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11927
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11927_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11928
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11928_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11929
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11929_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11932
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11932_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11933
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11933_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11934
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11934_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11935
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11935_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11936
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11936_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11937
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11937_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11938
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11938_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11939
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11939_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_11831
	 {
	AnonFilter_a2_11831_s.count = 0.0 ; 
	AnonFilter_a2_11831_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_11803();
		DUPLICATE_Splitter_11878();
			iDCT_2D_reference_coarse_11806();
			WEIGHTED_ROUND_ROBIN_Splitter_11886();
				AnonFilter_a3_11888();
				AnonFilter_a3_11889();
				AnonFilter_a3_11890();
				AnonFilter_a3_11891();
				AnonFilter_a3_11892();
				AnonFilter_a3_11893();
				AnonFilter_a3_11894();
				AnonFilter_a3_11895();
				AnonFilter_a3_11896();
				AnonFilter_a3_11897();
				AnonFilter_a3_11898();
				AnonFilter_a3_11899();
				AnonFilter_a3_11900();
				AnonFilter_a3_11901();
				AnonFilter_a3_11902();
				AnonFilter_a3_11903();
				AnonFilter_a3_11904();
				AnonFilter_a3_11905();
				AnonFilter_a3_11906();
				AnonFilter_a3_11907();
				AnonFilter_a3_11908();
				AnonFilter_a3_11909();
				AnonFilter_a3_11910();
				AnonFilter_a3_11911();
				AnonFilter_a3_11912();
				AnonFilter_a3_11913();
				AnonFilter_a3_11914();
				AnonFilter_a3_11915();
				AnonFilter_a3_11916();
				AnonFilter_a3_11917();
				AnonFilter_a3_11918();
				AnonFilter_a3_11919();
			WEIGHTED_ROUND_ROBIN_Joiner_11887();
			Pre_CollapsedDataParallel_1_11876();
			WEIGHTED_ROUND_ROBIN_Splitter_11920();
				iDCT_1D_reference_fine_11922();
				iDCT_1D_reference_fine_11923();
				iDCT_1D_reference_fine_11924();
				iDCT_1D_reference_fine_11925();
				iDCT_1D_reference_fine_11926();
				iDCT_1D_reference_fine_11927();
				iDCT_1D_reference_fine_11928();
				iDCT_1D_reference_fine_11929();
			WEIGHTED_ROUND_ROBIN_Joiner_11921();
			Post_CollapsedDataParallel_2_11877();
			WEIGHTED_ROUND_ROBIN_Splitter_11930();
				iDCT_1D_reference_fine_11932();
				iDCT_1D_reference_fine_11933();
				iDCT_1D_reference_fine_11934();
				iDCT_1D_reference_fine_11935();
				iDCT_1D_reference_fine_11936();
				iDCT_1D_reference_fine_11937();
				iDCT_1D_reference_fine_11938();
				iDCT_1D_reference_fine_11939();
			WEIGHTED_ROUND_ROBIN_Joiner_11931();
			WEIGHTED_ROUND_ROBIN_Splitter_11940();
				AnonFilter_a4_11942();
				AnonFilter_a4_11943();
				AnonFilter_a4_11944();
				AnonFilter_a4_11945();
				AnonFilter_a4_11946();
				AnonFilter_a4_11947();
				AnonFilter_a4_11948();
				AnonFilter_a4_11949();
				AnonFilter_a4_11950();
				AnonFilter_a4_11951();
				AnonFilter_a4_11952();
				AnonFilter_a4_11953();
				AnonFilter_a4_11954();
				AnonFilter_a4_11955();
				AnonFilter_a4_11956();
				AnonFilter_a4_11957();
				AnonFilter_a4_11958();
				AnonFilter_a4_11959();
				AnonFilter_a4_11960();
				AnonFilter_a4_11961();
				AnonFilter_a4_11962();
				AnonFilter_a4_11963();
				AnonFilter_a4_11964();
				AnonFilter_a4_11965();
				AnonFilter_a4_11966();
				AnonFilter_a4_11967();
				AnonFilter_a4_11968();
				AnonFilter_a4_11969();
				AnonFilter_a4_11970();
				AnonFilter_a4_11971();
				AnonFilter_a4_11972();
				AnonFilter_a4_11973();
			WEIGHTED_ROUND_ROBIN_Joiner_11941();
			WEIGHTED_ROUND_ROBIN_Splitter_11974();
				iDCT8x8_1D_row_fast_11976();
				iDCT8x8_1D_row_fast_11977();
				iDCT8x8_1D_row_fast_11978();
				iDCT8x8_1D_row_fast_11979();
				iDCT8x8_1D_row_fast_11980();
				iDCT8x8_1D_row_fast_11981();
				iDCT8x8_1D_row_fast_11982();
				iDCT8x8_1D_row_fast_11983();
			WEIGHTED_ROUND_ROBIN_Joiner_11975();
			iDCT8x8_1D_col_fast_11830();
		WEIGHTED_ROUND_ROBIN_Joiner_11879();
		AnonFilter_a2_11831();
	ENDFOR
	return EXIT_SUCCESS;
}
