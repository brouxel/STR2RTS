#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=10368 on the compile command line
#else
#if BUF_SIZEMAX < 10368
#error BUF_SIZEMAX too small, it must be at least 10368
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_4414_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_4438_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_4439_t;
void AnonFilter_a0_4411();
void DUPLICATE_Splitter_4486();
void iDCT_2D_reference_coarse_4414();
void WEIGHTED_ROUND_ROBIN_Splitter_4494();
void AnonFilter_a3_4496();
void AnonFilter_a3_4497();
void AnonFilter_a3_4498();
void AnonFilter_a3_4499();
void AnonFilter_a3_4500();
void AnonFilter_a3_4501();
void AnonFilter_a3_4502();
void AnonFilter_a3_4503();
void AnonFilter_a3_4504();
void AnonFilter_a3_4505();
void AnonFilter_a3_4506();
void AnonFilter_a3_4507();
void AnonFilter_a3_4508();
void AnonFilter_a3_4509();
void AnonFilter_a3_4510();
void AnonFilter_a3_4511();
void AnonFilter_a3_4512();
void AnonFilter_a3_4513();
void AnonFilter_a3_4514();
void AnonFilter_a3_4515();
void AnonFilter_a3_4516();
void AnonFilter_a3_4517();
void AnonFilter_a3_4518();
void AnonFilter_a3_4519();
void AnonFilter_a3_4520();
void AnonFilter_a3_4521();
void AnonFilter_a3_4522();
void AnonFilter_a3_4523();
void AnonFilter_a3_4524();
void AnonFilter_a3_4525();
void AnonFilter_a3_4526();
void AnonFilter_a3_4527();
void AnonFilter_a3_4528();
void AnonFilter_a3_4529();
void AnonFilter_a3_4530();
void AnonFilter_a3_4531();
void AnonFilter_a3_4532();
void AnonFilter_a3_4533();
void AnonFilter_a3_4534();
void AnonFilter_a3_4535();
void AnonFilter_a3_4536();
void AnonFilter_a3_4537();
void AnonFilter_a3_4538();
void AnonFilter_a3_4539();
void AnonFilter_a3_4540();
void AnonFilter_a3_4541();
void AnonFilter_a3_4542();
void AnonFilter_a3_4543();
void AnonFilter_a3_4544();
void AnonFilter_a3_4545();
void AnonFilter_a3_4546();
void AnonFilter_a3_4547();
void AnonFilter_a3_4548();
void AnonFilter_a3_4549();
void WEIGHTED_ROUND_ROBIN_Joiner_4495();
void Pre_CollapsedDataParallel_1_4484();
void WEIGHTED_ROUND_ROBIN_Splitter_4550();
void iDCT_1D_reference_fine_4552();
void iDCT_1D_reference_fine_4553();
void iDCT_1D_reference_fine_4554();
void iDCT_1D_reference_fine_4555();
void iDCT_1D_reference_fine_4556();
void iDCT_1D_reference_fine_4557();
void iDCT_1D_reference_fine_4558();
void iDCT_1D_reference_fine_4559();
void WEIGHTED_ROUND_ROBIN_Joiner_4551();
void Post_CollapsedDataParallel_2_4485();
void WEIGHTED_ROUND_ROBIN_Splitter_4560();
void iDCT_1D_reference_fine_4562();
void iDCT_1D_reference_fine_4563();
void iDCT_1D_reference_fine_4564();
void iDCT_1D_reference_fine_4565();
void iDCT_1D_reference_fine_4566();
void iDCT_1D_reference_fine_4567();
void iDCT_1D_reference_fine_4568();
void iDCT_1D_reference_fine_4569();
void WEIGHTED_ROUND_ROBIN_Joiner_4561();
void WEIGHTED_ROUND_ROBIN_Splitter_4570();
void AnonFilter_a4_4572();
void AnonFilter_a4_4573();
void AnonFilter_a4_4574();
void AnonFilter_a4_4575();
void AnonFilter_a4_4576();
void AnonFilter_a4_4577();
void AnonFilter_a4_4578();
void AnonFilter_a4_4579();
void AnonFilter_a4_4580();
void AnonFilter_a4_4581();
void AnonFilter_a4_4582();
void AnonFilter_a4_4583();
void AnonFilter_a4_4584();
void AnonFilter_a4_4585();
void AnonFilter_a4_4586();
void AnonFilter_a4_4587();
void AnonFilter_a4_4588();
void AnonFilter_a4_4589();
void AnonFilter_a4_4590();
void AnonFilter_a4_4591();
void AnonFilter_a4_4592();
void AnonFilter_a4_4593();
void AnonFilter_a4_4594();
void AnonFilter_a4_4595();
void AnonFilter_a4_4596();
void AnonFilter_a4_4597();
void AnonFilter_a4_4598();
void AnonFilter_a4_4599();
void AnonFilter_a4_4600();
void AnonFilter_a4_4601();
void AnonFilter_a4_4602();
void AnonFilter_a4_4603();
void AnonFilter_a4_4604();
void AnonFilter_a4_4605();
void AnonFilter_a4_4606();
void AnonFilter_a4_4607();
void AnonFilter_a4_4608();
void AnonFilter_a4_4609();
void AnonFilter_a4_4610();
void AnonFilter_a4_4611();
void AnonFilter_a4_4612();
void AnonFilter_a4_4613();
void AnonFilter_a4_4614();
void AnonFilter_a4_4615();
void AnonFilter_a4_4616();
void AnonFilter_a4_4617();
void AnonFilter_a4_4618();
void AnonFilter_a4_4619();
void AnonFilter_a4_4620();
void AnonFilter_a4_4621();
void AnonFilter_a4_4622();
void AnonFilter_a4_4623();
void AnonFilter_a4_4624();
void AnonFilter_a4_4625();
void WEIGHTED_ROUND_ROBIN_Joiner_4571();
void WEIGHTED_ROUND_ROBIN_Splitter_4626();
void iDCT8x8_1D_row_fast_4628();
void iDCT8x8_1D_row_fast_4629();
void iDCT8x8_1D_row_fast_4630();
void iDCT8x8_1D_row_fast_4631();
void iDCT8x8_1D_row_fast_4632();
void iDCT8x8_1D_row_fast_4633();
void iDCT8x8_1D_row_fast_4634();
void iDCT8x8_1D_row_fast_4635();
void WEIGHTED_ROUND_ROBIN_Joiner_4627();
void iDCT8x8_1D_col_fast_4438();
void WEIGHTED_ROUND_ROBIN_Joiner_4487();
void AnonFilter_a2_4439();

#ifdef __cplusplus
}
#endif
#endif
