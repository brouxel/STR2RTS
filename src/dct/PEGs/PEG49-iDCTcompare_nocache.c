#include "PEG49-iDCTcompare_nocache.h"

buffer_float_t Post_CollapsedDataParallel_2_6335WEIGHTED_ROUND_ROBIN_Splitter_6405;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[49];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[49];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_split[3];
buffer_int_t SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_6334WEIGHTED_ROUND_ROBIN_Splitter_6395;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6396Post_CollapsedDataParallel_2_6335;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6345Pre_CollapsedDataParallel_1_6334;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[49];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_6337AnonFilter_a2_6289;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[49];
buffer_int_t AnonFilter_a0_6261DUPLICATE_Splitter_6336;
buffer_int_t SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6406WEIGHTED_ROUND_ROBIN_Splitter_6415;


iDCT_2D_reference_coarse_6264_t iDCT_2D_reference_coarse_6264_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6397_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6398_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6399_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6400_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6401_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6402_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6403_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6404_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6407_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6408_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6409_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6410_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6411_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6412_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6413_s;
iDCT_2D_reference_coarse_6264_t iDCT_1D_reference_fine_6414_s;
iDCT8x8_1D_col_fast_6288_t iDCT8x8_1D_col_fast_6288_s;
AnonFilter_a2_6289_t AnonFilter_a2_6289_s;

void AnonFilter_a0_6261(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_6261DUPLICATE_Splitter_6336, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_6264(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_6264_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_6264_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_6346(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6347(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6348(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6349(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6350(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6351(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6352(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6353(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6354(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6355(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6356(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6357(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6358(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6359(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6360(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6361(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6362(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6363(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6364(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6365(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6366(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6367(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6368(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6369(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6370(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6371(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6372(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6373(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6374(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6375(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6376(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6377(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6378(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6379(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6380(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6381(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6382(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6383(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6384(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6385(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6386(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6387(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6388(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6389(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6390(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6391(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[45])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6392(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[46], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[46])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6393(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[47], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[47])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6394(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[48], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[48])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6344() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 49, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6345() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 49, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6345Pre_CollapsedDataParallel_1_6334, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_6334(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_6334WEIGHTED_ROUND_ROBIN_Splitter_6395, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_6345Pre_CollapsedDataParallel_1_6334, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6345Pre_CollapsedDataParallel_1_6334) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6397(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6397_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6398(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6398_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6399(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6399_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6400(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6400_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6401(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6401_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6402(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6402_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6403(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6403_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6404(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6404_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6395() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6334WEIGHTED_ROUND_ROBIN_Splitter_6395));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6396() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6396Post_CollapsedDataParallel_2_6335, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6335(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_6335WEIGHTED_ROUND_ROBIN_Splitter_6405, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_6396Post_CollapsedDataParallel_2_6335, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6396Post_CollapsedDataParallel_2_6335) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6407(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6407_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6408(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6408_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6409(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6409_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6410(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6410_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6411(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6411_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6412(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6412_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6413(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6413_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6414(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6414_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6405() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_6335WEIGHTED_ROUND_ROBIN_Splitter_6405));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6406() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6406WEIGHTED_ROUND_ROBIN_Splitter_6415, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_6417(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6418(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6419(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6420(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6421(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6422(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6423(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6424(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6425(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6426(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6427(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6428(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6429(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6430(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6431(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6432(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6433(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6434(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6435(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6436(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6437(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6438(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6439(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6440(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6441(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6442(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6443(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6444(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6445(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6446(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6447(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6448(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6449(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6450(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6451(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6452(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6453(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6454(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6455(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6456(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6457(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6458(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6459(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6460(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6461(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6462(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6463(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[46], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[46]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6464(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[47], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[47]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6465(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[48], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[48]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6415() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 49, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6406WEIGHTED_ROUND_ROBIN_Splitter_6415));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6416() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 49, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_6468(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0], 6) ; 
		x3 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0], 2) ; 
		x4 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0], 1) ; 
		x5 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0], 7) ; 
		x6 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0], 5) ; 
		x7 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6469(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1], 6) ; 
		x3 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1], 2) ; 
		x4 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1], 1) ; 
		x5 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1], 7) ; 
		x6 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1], 5) ; 
		x7 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6470(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2], 6) ; 
		x3 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2], 2) ; 
		x4 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2], 1) ; 
		x5 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2], 7) ; 
		x6 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2], 5) ; 
		x7 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6471(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3], 6) ; 
		x3 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3], 2) ; 
		x4 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3], 1) ; 
		x5 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3], 7) ; 
		x6 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3], 5) ; 
		x7 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6472(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4], 6) ; 
		x3 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4], 2) ; 
		x4 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4], 1) ; 
		x5 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4], 7) ; 
		x6 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4], 5) ; 
		x7 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6473(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5], 6) ; 
		x3 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5], 2) ; 
		x4 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5], 1) ; 
		x5 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5], 7) ; 
		x6 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5], 5) ; 
		x7 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6474(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6], 6) ; 
		x3 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6], 2) ; 
		x4 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6], 1) ; 
		x5 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6], 7) ; 
		x6 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6], 5) ; 
		x7 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6475(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7], 6) ; 
		x3 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7], 2) ; 
		x4 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7], 1) ; 
		x5 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7], 7) ; 
		x6 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7], 5) ; 
		x7 = peek_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6466() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6467() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, pop_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_6288(){
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_6288_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_6288_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_6288_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_6288_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_6288_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_6288_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_6288_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_6288_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_6288_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_join[2], iDCT8x8_1D_col_fast_6288_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_6336() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3136, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_6261DUPLICATE_Splitter_6336);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6337() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3136, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_6337AnonFilter_a2_6289, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_6289(){
	FOR(uint32_t, __iter_steady_, 0, <, 3136, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_6337AnonFilter_a2_6289) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_6337AnonFilter_a2_6289) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_6337AnonFilter_a2_6289) ; 
		AnonFilter_a2_6289_s.count = (AnonFilter_a2_6289_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_6289_s.errors = (AnonFilter_a2_6289_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_6289_s.errors / AnonFilter_a2_6289_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_6289_s.errors = (AnonFilter_a2_6289_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_6289_s.errors / AnonFilter_a2_6289_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Post_CollapsedDataParallel_2_6335WEIGHTED_ROUND_ROBIN_Splitter_6405);
	FOR(int, __iter_init_0_, 0, <, 49, __iter_init_0_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_join[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_6467iDCT8x8_1D_col_fast_6288);
	FOR(int, __iter_init_2_, 0, <, 49, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6262_6338_6476_6483_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6334WEIGHTED_ROUND_ROBIN_Splitter_6395);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6396Post_CollapsedDataParallel_2_6335);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6345Pre_CollapsedDataParallel_1_6334);
	FOR(int, __iter_init_6_, 0, <, 49, __iter_init_6_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_6477_6484_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6478_6485_split[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_6337AnonFilter_a2_6289);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6479_6486_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 49, __iter_init_10_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_6480_6487_join[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_6261DUPLICATE_Splitter_6336);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin122_iDCT8x8_1D_row_fast_Fiss_6481_6488_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6406WEIGHTED_ROUND_ROBIN_Splitter_6415);
// --- init: iDCT_2D_reference_coarse_6264
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_6264_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6397
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6397_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6398
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6398_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6399
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6399_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6400
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6400_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6401
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6401_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6402
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6402_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6403
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6403_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6404
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6404_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6407
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6407_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6408
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6408_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6409
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6409_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6410
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6410_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6411
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6411_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6412
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6412_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6413
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6413_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6414
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6414_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_6289
	 {
	AnonFilter_a2_6289_s.count = 0.0 ; 
	AnonFilter_a2_6289_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_6261();
		DUPLICATE_Splitter_6336();
			iDCT_2D_reference_coarse_6264();
			WEIGHTED_ROUND_ROBIN_Splitter_6344();
				AnonFilter_a3_6346();
				AnonFilter_a3_6347();
				AnonFilter_a3_6348();
				AnonFilter_a3_6349();
				AnonFilter_a3_6350();
				AnonFilter_a3_6351();
				AnonFilter_a3_6352();
				AnonFilter_a3_6353();
				AnonFilter_a3_6354();
				AnonFilter_a3_6355();
				AnonFilter_a3_6356();
				AnonFilter_a3_6357();
				AnonFilter_a3_6358();
				AnonFilter_a3_6359();
				AnonFilter_a3_6360();
				AnonFilter_a3_6361();
				AnonFilter_a3_6362();
				AnonFilter_a3_6363();
				AnonFilter_a3_6364();
				AnonFilter_a3_6365();
				AnonFilter_a3_6366();
				AnonFilter_a3_6367();
				AnonFilter_a3_6368();
				AnonFilter_a3_6369();
				AnonFilter_a3_6370();
				AnonFilter_a3_6371();
				AnonFilter_a3_6372();
				AnonFilter_a3_6373();
				AnonFilter_a3_6374();
				AnonFilter_a3_6375();
				AnonFilter_a3_6376();
				AnonFilter_a3_6377();
				AnonFilter_a3_6378();
				AnonFilter_a3_6379();
				AnonFilter_a3_6380();
				AnonFilter_a3_6381();
				AnonFilter_a3_6382();
				AnonFilter_a3_6383();
				AnonFilter_a3_6384();
				AnonFilter_a3_6385();
				AnonFilter_a3_6386();
				AnonFilter_a3_6387();
				AnonFilter_a3_6388();
				AnonFilter_a3_6389();
				AnonFilter_a3_6390();
				AnonFilter_a3_6391();
				AnonFilter_a3_6392();
				AnonFilter_a3_6393();
				AnonFilter_a3_6394();
			WEIGHTED_ROUND_ROBIN_Joiner_6345();
			Pre_CollapsedDataParallel_1_6334();
			WEIGHTED_ROUND_ROBIN_Splitter_6395();
				iDCT_1D_reference_fine_6397();
				iDCT_1D_reference_fine_6398();
				iDCT_1D_reference_fine_6399();
				iDCT_1D_reference_fine_6400();
				iDCT_1D_reference_fine_6401();
				iDCT_1D_reference_fine_6402();
				iDCT_1D_reference_fine_6403();
				iDCT_1D_reference_fine_6404();
			WEIGHTED_ROUND_ROBIN_Joiner_6396();
			Post_CollapsedDataParallel_2_6335();
			WEIGHTED_ROUND_ROBIN_Splitter_6405();
				iDCT_1D_reference_fine_6407();
				iDCT_1D_reference_fine_6408();
				iDCT_1D_reference_fine_6409();
				iDCT_1D_reference_fine_6410();
				iDCT_1D_reference_fine_6411();
				iDCT_1D_reference_fine_6412();
				iDCT_1D_reference_fine_6413();
				iDCT_1D_reference_fine_6414();
			WEIGHTED_ROUND_ROBIN_Joiner_6406();
			WEIGHTED_ROUND_ROBIN_Splitter_6415();
				AnonFilter_a4_6417();
				AnonFilter_a4_6418();
				AnonFilter_a4_6419();
				AnonFilter_a4_6420();
				AnonFilter_a4_6421();
				AnonFilter_a4_6422();
				AnonFilter_a4_6423();
				AnonFilter_a4_6424();
				AnonFilter_a4_6425();
				AnonFilter_a4_6426();
				AnonFilter_a4_6427();
				AnonFilter_a4_6428();
				AnonFilter_a4_6429();
				AnonFilter_a4_6430();
				AnonFilter_a4_6431();
				AnonFilter_a4_6432();
				AnonFilter_a4_6433();
				AnonFilter_a4_6434();
				AnonFilter_a4_6435();
				AnonFilter_a4_6436();
				AnonFilter_a4_6437();
				AnonFilter_a4_6438();
				AnonFilter_a4_6439();
				AnonFilter_a4_6440();
				AnonFilter_a4_6441();
				AnonFilter_a4_6442();
				AnonFilter_a4_6443();
				AnonFilter_a4_6444();
				AnonFilter_a4_6445();
				AnonFilter_a4_6446();
				AnonFilter_a4_6447();
				AnonFilter_a4_6448();
				AnonFilter_a4_6449();
				AnonFilter_a4_6450();
				AnonFilter_a4_6451();
				AnonFilter_a4_6452();
				AnonFilter_a4_6453();
				AnonFilter_a4_6454();
				AnonFilter_a4_6455();
				AnonFilter_a4_6456();
				AnonFilter_a4_6457();
				AnonFilter_a4_6458();
				AnonFilter_a4_6459();
				AnonFilter_a4_6460();
				AnonFilter_a4_6461();
				AnonFilter_a4_6462();
				AnonFilter_a4_6463();
				AnonFilter_a4_6464();
				AnonFilter_a4_6465();
			WEIGHTED_ROUND_ROBIN_Joiner_6416();
			WEIGHTED_ROUND_ROBIN_Splitter_6466();
				iDCT8x8_1D_row_fast_6468();
				iDCT8x8_1D_row_fast_6469();
				iDCT8x8_1D_row_fast_6470();
				iDCT8x8_1D_row_fast_6471();
				iDCT8x8_1D_row_fast_6472();
				iDCT8x8_1D_row_fast_6473();
				iDCT8x8_1D_row_fast_6474();
				iDCT8x8_1D_row_fast_6475();
			WEIGHTED_ROUND_ROBIN_Joiner_6467();
			iDCT8x8_1D_col_fast_6288();
		WEIGHTED_ROUND_ROBIN_Joiner_6337();
		AnonFilter_a2_6289();
	ENDFOR
	return EXIT_SUCCESS;
}
