#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=17280 on the compile command line
#else
#if BUF_SIZEMAX < 17280
#error BUF_SIZEMAX too small, it must be at least 17280
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_7672_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_7696_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_7697_t;
void AnonFilter_a0_7669();
void DUPLICATE_Splitter_7744();
void iDCT_2D_reference_coarse_7672();
void WEIGHTED_ROUND_ROBIN_Splitter_7752();
void AnonFilter_a3_7754();
void AnonFilter_a3_7755();
void AnonFilter_a3_7756();
void AnonFilter_a3_7757();
void AnonFilter_a3_7758();
void AnonFilter_a3_7759();
void AnonFilter_a3_7760();
void AnonFilter_a3_7761();
void AnonFilter_a3_7762();
void AnonFilter_a3_7763();
void AnonFilter_a3_7764();
void AnonFilter_a3_7765();
void AnonFilter_a3_7766();
void AnonFilter_a3_7767();
void AnonFilter_a3_7768();
void AnonFilter_a3_7769();
void AnonFilter_a3_7770();
void AnonFilter_a3_7771();
void AnonFilter_a3_7772();
void AnonFilter_a3_7773();
void AnonFilter_a3_7774();
void AnonFilter_a3_7775();
void AnonFilter_a3_7776();
void AnonFilter_a3_7777();
void AnonFilter_a3_7778();
void AnonFilter_a3_7779();
void AnonFilter_a3_7780();
void AnonFilter_a3_7781();
void AnonFilter_a3_7782();
void AnonFilter_a3_7783();
void AnonFilter_a3_7784();
void AnonFilter_a3_7785();
void AnonFilter_a3_7786();
void AnonFilter_a3_7787();
void AnonFilter_a3_7788();
void AnonFilter_a3_7789();
void AnonFilter_a3_7790();
void AnonFilter_a3_7791();
void AnonFilter_a3_7792();
void AnonFilter_a3_7793();
void AnonFilter_a3_7794();
void AnonFilter_a3_7795();
void AnonFilter_a3_7796();
void AnonFilter_a3_7797();
void AnonFilter_a3_7798();
void WEIGHTED_ROUND_ROBIN_Joiner_7753();
void Pre_CollapsedDataParallel_1_7742();
void WEIGHTED_ROUND_ROBIN_Splitter_7799();
void iDCT_1D_reference_fine_7801();
void iDCT_1D_reference_fine_7802();
void iDCT_1D_reference_fine_7803();
void iDCT_1D_reference_fine_7804();
void iDCT_1D_reference_fine_7805();
void iDCT_1D_reference_fine_7806();
void iDCT_1D_reference_fine_7807();
void iDCT_1D_reference_fine_7808();
void WEIGHTED_ROUND_ROBIN_Joiner_7800();
void Post_CollapsedDataParallel_2_7743();
void WEIGHTED_ROUND_ROBIN_Splitter_7809();
void iDCT_1D_reference_fine_7811();
void iDCT_1D_reference_fine_7812();
void iDCT_1D_reference_fine_7813();
void iDCT_1D_reference_fine_7814();
void iDCT_1D_reference_fine_7815();
void iDCT_1D_reference_fine_7816();
void iDCT_1D_reference_fine_7817();
void iDCT_1D_reference_fine_7818();
void WEIGHTED_ROUND_ROBIN_Joiner_7810();
void WEIGHTED_ROUND_ROBIN_Splitter_7819();
void AnonFilter_a4_7821();
void AnonFilter_a4_7822();
void AnonFilter_a4_7823();
void AnonFilter_a4_7824();
void AnonFilter_a4_7825();
void AnonFilter_a4_7826();
void AnonFilter_a4_7827();
void AnonFilter_a4_7828();
void AnonFilter_a4_7829();
void AnonFilter_a4_7830();
void AnonFilter_a4_7831();
void AnonFilter_a4_7832();
void AnonFilter_a4_7833();
void AnonFilter_a4_7834();
void AnonFilter_a4_7835();
void AnonFilter_a4_7836();
void AnonFilter_a4_7837();
void AnonFilter_a4_7838();
void AnonFilter_a4_7839();
void AnonFilter_a4_7840();
void AnonFilter_a4_7841();
void AnonFilter_a4_7842();
void AnonFilter_a4_7843();
void AnonFilter_a4_7844();
void AnonFilter_a4_7845();
void AnonFilter_a4_7846();
void AnonFilter_a4_7847();
void AnonFilter_a4_7848();
void AnonFilter_a4_7849();
void AnonFilter_a4_7850();
void AnonFilter_a4_7851();
void AnonFilter_a4_7852();
void AnonFilter_a4_7853();
void AnonFilter_a4_7854();
void AnonFilter_a4_7855();
void AnonFilter_a4_7856();
void AnonFilter_a4_7857();
void AnonFilter_a4_7858();
void AnonFilter_a4_7859();
void AnonFilter_a4_7860();
void AnonFilter_a4_7861();
void AnonFilter_a4_7862();
void AnonFilter_a4_7863();
void AnonFilter_a4_7864();
void AnonFilter_a4_7865();
void WEIGHTED_ROUND_ROBIN_Joiner_7820();
void WEIGHTED_ROUND_ROBIN_Splitter_7866();
void iDCT8x8_1D_row_fast_7868();
void iDCT8x8_1D_row_fast_7869();
void iDCT8x8_1D_row_fast_7870();
void iDCT8x8_1D_row_fast_7871();
void iDCT8x8_1D_row_fast_7872();
void iDCT8x8_1D_row_fast_7873();
void iDCT8x8_1D_row_fast_7874();
void iDCT8x8_1D_row_fast_7875();
void WEIGHTED_ROUND_ROBIN_Joiner_7867();
void iDCT8x8_1D_col_fast_7696();
void WEIGHTED_ROUND_ROBIN_Joiner_7745();
void AnonFilter_a2_7697();

#ifdef __cplusplus
}
#endif
#endif
