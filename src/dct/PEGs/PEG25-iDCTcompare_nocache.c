#include "PEG25-iDCTcompare_nocache.h"

buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[25];
buffer_int_t AnonFilter_a0_13749DUPLICATE_Splitter_13824;
buffer_float_t Pre_CollapsedDataParallel_1_13822WEIGHTED_ROUND_ROBIN_Splitter_13859;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13860Post_CollapsedDataParallel_2_13823;
buffer_int_t SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_13825AnonFilter_a2_13777;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13833Pre_CollapsedDataParallel_1_13822;
buffer_int_t SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13870WEIGHTED_ROUND_ROBIN_Splitter_13879;
buffer_float_t Post_CollapsedDataParallel_2_13823WEIGHTED_ROUND_ROBIN_Splitter_13869;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_join[3];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[25];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[25];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[25];


iDCT_2D_reference_coarse_13752_t iDCT_2D_reference_coarse_13752_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13861_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13862_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13863_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13864_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13865_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13866_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13867_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13868_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13871_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13872_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13873_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13874_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13875_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13876_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13877_s;
iDCT_2D_reference_coarse_13752_t iDCT_1D_reference_fine_13878_s;
iDCT8x8_1D_col_fast_13776_t iDCT8x8_1D_col_fast_13776_s;
AnonFilter_a2_13777_t AnonFilter_a2_13777_s;

void AnonFilter_a0_13749(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_13749DUPLICATE_Splitter_13824, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_13752(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_13752_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_13752_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_13834(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13835(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13836(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13837(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13838(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13839(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13840(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13841(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13842(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13843(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13844(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13845(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13846(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13847(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13848(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13849(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13850(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13851(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13852(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13853(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13854(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13855(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13856(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13857(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_13858(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[24])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13832() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13833() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13833Pre_CollapsedDataParallel_1_13822, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_13822(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_13822WEIGHTED_ROUND_ROBIN_Splitter_13859, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_13833Pre_CollapsedDataParallel_1_13822, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_13833Pre_CollapsedDataParallel_1_13822) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13861(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13861_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13862(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13862_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13863(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13863_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13864(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13864_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13865(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13865_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13866(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13866_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13867(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13867_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13868(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13868_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13859() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_13822WEIGHTED_ROUND_ROBIN_Splitter_13859));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13860() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13860Post_CollapsedDataParallel_2_13823, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_13823(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_13823WEIGHTED_ROUND_ROBIN_Splitter_13869, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_13860Post_CollapsedDataParallel_2_13823, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_13860Post_CollapsedDataParallel_2_13823) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13871(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13871_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13872(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13872_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13873(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13873_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13874(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13874_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13875(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13875_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13876(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13876_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13877(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13877_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_13878(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13878_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13869() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_13823WEIGHTED_ROUND_ROBIN_Splitter_13869));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13870() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13870WEIGHTED_ROUND_ROBIN_Splitter_13879, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_13881(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13882(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13883(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13884(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13885(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13886(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13887(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13888(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13889(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13890(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13891(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13892(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13893(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13894(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13895(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13896(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13897(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13898(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13899(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13900(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13901(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13902(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13903(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13904(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_13905(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13879() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_13870WEIGHTED_ROUND_ROBIN_Splitter_13879));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13880() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_13908(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0], 6) ; 
		x3 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0], 2) ; 
		x4 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0], 1) ; 
		x5 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0], 7) ; 
		x6 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0], 5) ; 
		x7 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_13909(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1], 6) ; 
		x3 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1], 2) ; 
		x4 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1], 1) ; 
		x5 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1], 7) ; 
		x6 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1], 5) ; 
		x7 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_13910(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2], 6) ; 
		x3 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2], 2) ; 
		x4 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2], 1) ; 
		x5 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2], 7) ; 
		x6 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2], 5) ; 
		x7 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_13911(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3], 6) ; 
		x3 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3], 2) ; 
		x4 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3], 1) ; 
		x5 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3], 7) ; 
		x6 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3], 5) ; 
		x7 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_13912(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4], 6) ; 
		x3 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4], 2) ; 
		x4 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4], 1) ; 
		x5 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4], 7) ; 
		x6 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4], 5) ; 
		x7 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_13913(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5], 6) ; 
		x3 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5], 2) ; 
		x4 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5], 1) ; 
		x5 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5], 7) ; 
		x6 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5], 5) ; 
		x7 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_13914(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6], 6) ; 
		x3 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6], 2) ; 
		x4 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6], 1) ; 
		x5 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6], 7) ; 
		x6 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6], 5) ; 
		x7 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_13915(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7], 6) ; 
		x3 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7], 2) ; 
		x4 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7], 1) ; 
		x5 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7], 7) ; 
		x6 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7], 5) ; 
		x7 = peek_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13906() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13907() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, pop_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_13776(){
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_13776_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_13776_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_13776_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_13776_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_13776_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_13776_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_13776_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_13776_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_13776_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_join[2], iDCT8x8_1D_col_fast_13776_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_13824() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_13749DUPLICATE_Splitter_13824);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13825() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_13825AnonFilter_a2_13777, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_13777(){
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_13825AnonFilter_a2_13777) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_13825AnonFilter_a2_13777) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_13825AnonFilter_a2_13777) ; 
		AnonFilter_a2_13777_s.count = (AnonFilter_a2_13777_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_13777_s.errors = (AnonFilter_a2_13777_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_13777_s.errors / AnonFilter_a2_13777_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_13777_s.errors = (AnonFilter_a2_13777_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_13777_s.errors / AnonFilter_a2_13777_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 25, __iter_init_1_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_split[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_13749DUPLICATE_Splitter_13824);
	init_buffer_float(&Pre_CollapsedDataParallel_1_13822WEIGHTED_ROUND_ROBIN_Splitter_13859);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13860Post_CollapsedDataParallel_2_13823);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_13825AnonFilter_a2_13777);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13833Pre_CollapsedDataParallel_1_13822);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin74_iDCT8x8_1D_row_fast_Fiss_13921_13928_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13870WEIGHTED_ROUND_ROBIN_Splitter_13879);
	init_buffer_float(&Post_CollapsedDataParallel_2_13823WEIGHTED_ROUND_ROBIN_Splitter_13869);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13919_13926_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 25, __iter_init_7_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13918_13925_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13750_13826_13916_13923_split[__iter_init_9_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_13907iDCT8x8_1D_col_fast_13776);
	FOR(int, __iter_init_10_, 0, <, 25, __iter_init_10_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_13917_13924_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 25, __iter_init_11_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_13920_13927_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_13752
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_13752_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13861
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13861_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13862
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13862_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13863
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13863_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13864
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13864_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13865
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13865_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13866
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13866_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13867
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13867_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13868
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13868_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13871
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13871_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13872
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13872_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13873
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13873_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13874
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13874_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13875
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13875_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13876
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13876_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13877
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13877_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13878
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13878_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_13777
	 {
	AnonFilter_a2_13777_s.count = 0.0 ; 
	AnonFilter_a2_13777_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_13749();
		DUPLICATE_Splitter_13824();
			iDCT_2D_reference_coarse_13752();
			WEIGHTED_ROUND_ROBIN_Splitter_13832();
				AnonFilter_a3_13834();
				AnonFilter_a3_13835();
				AnonFilter_a3_13836();
				AnonFilter_a3_13837();
				AnonFilter_a3_13838();
				AnonFilter_a3_13839();
				AnonFilter_a3_13840();
				AnonFilter_a3_13841();
				AnonFilter_a3_13842();
				AnonFilter_a3_13843();
				AnonFilter_a3_13844();
				AnonFilter_a3_13845();
				AnonFilter_a3_13846();
				AnonFilter_a3_13847();
				AnonFilter_a3_13848();
				AnonFilter_a3_13849();
				AnonFilter_a3_13850();
				AnonFilter_a3_13851();
				AnonFilter_a3_13852();
				AnonFilter_a3_13853();
				AnonFilter_a3_13854();
				AnonFilter_a3_13855();
				AnonFilter_a3_13856();
				AnonFilter_a3_13857();
				AnonFilter_a3_13858();
			WEIGHTED_ROUND_ROBIN_Joiner_13833();
			Pre_CollapsedDataParallel_1_13822();
			WEIGHTED_ROUND_ROBIN_Splitter_13859();
				iDCT_1D_reference_fine_13861();
				iDCT_1D_reference_fine_13862();
				iDCT_1D_reference_fine_13863();
				iDCT_1D_reference_fine_13864();
				iDCT_1D_reference_fine_13865();
				iDCT_1D_reference_fine_13866();
				iDCT_1D_reference_fine_13867();
				iDCT_1D_reference_fine_13868();
			WEIGHTED_ROUND_ROBIN_Joiner_13860();
			Post_CollapsedDataParallel_2_13823();
			WEIGHTED_ROUND_ROBIN_Splitter_13869();
				iDCT_1D_reference_fine_13871();
				iDCT_1D_reference_fine_13872();
				iDCT_1D_reference_fine_13873();
				iDCT_1D_reference_fine_13874();
				iDCT_1D_reference_fine_13875();
				iDCT_1D_reference_fine_13876();
				iDCT_1D_reference_fine_13877();
				iDCT_1D_reference_fine_13878();
			WEIGHTED_ROUND_ROBIN_Joiner_13870();
			WEIGHTED_ROUND_ROBIN_Splitter_13879();
				AnonFilter_a4_13881();
				AnonFilter_a4_13882();
				AnonFilter_a4_13883();
				AnonFilter_a4_13884();
				AnonFilter_a4_13885();
				AnonFilter_a4_13886();
				AnonFilter_a4_13887();
				AnonFilter_a4_13888();
				AnonFilter_a4_13889();
				AnonFilter_a4_13890();
				AnonFilter_a4_13891();
				AnonFilter_a4_13892();
				AnonFilter_a4_13893();
				AnonFilter_a4_13894();
				AnonFilter_a4_13895();
				AnonFilter_a4_13896();
				AnonFilter_a4_13897();
				AnonFilter_a4_13898();
				AnonFilter_a4_13899();
				AnonFilter_a4_13900();
				AnonFilter_a4_13901();
				AnonFilter_a4_13902();
				AnonFilter_a4_13903();
				AnonFilter_a4_13904();
				AnonFilter_a4_13905();
			WEIGHTED_ROUND_ROBIN_Joiner_13880();
			WEIGHTED_ROUND_ROBIN_Splitter_13906();
				iDCT8x8_1D_row_fast_13908();
				iDCT8x8_1D_row_fast_13909();
				iDCT8x8_1D_row_fast_13910();
				iDCT8x8_1D_row_fast_13911();
				iDCT8x8_1D_row_fast_13912();
				iDCT8x8_1D_row_fast_13913();
				iDCT8x8_1D_row_fast_13914();
				iDCT8x8_1D_row_fast_13915();
			WEIGHTED_ROUND_ROBIN_Joiner_13907();
			iDCT8x8_1D_col_fast_13776();
		WEIGHTED_ROUND_ROBIN_Joiner_13825();
		AnonFilter_a2_13777();
	ENDFOR
	return EXIT_SUCCESS;
}
