#include "PEG11-iDCTcompare.h"

buffer_int_t AnonFilter_a0_17053DUPLICATE_Splitter_17128;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17137Pre_CollapsedDataParallel_1_17126;
buffer_float_t Pre_CollapsedDataParallel_1_17126WEIGHTED_ROUND_ROBIN_Splitter_17149;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_join[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_17183iDCT8x8_1D_col_fast_17080;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[11];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[11];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17160WEIGHTED_ROUND_ROBIN_Splitter_17169;
buffer_float_t Post_CollapsedDataParallel_2_17127WEIGHTED_ROUND_ROBIN_Splitter_17159;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_17129AnonFilter_a2_17081;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[11];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17150Post_CollapsedDataParallel_2_17127;
buffer_int_t SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[8];
buffer_int_t SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[11];


iDCT_2D_reference_coarse_17056_t iDCT_2D_reference_coarse_17056_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17151_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17152_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17153_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17154_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17155_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17156_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17157_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17158_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17161_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17162_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17163_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17164_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17165_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17166_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17167_s;
iDCT_2D_reference_coarse_17056_t iDCT_1D_reference_fine_17168_s;
iDCT8x8_1D_col_fast_17080_t iDCT8x8_1D_col_fast_17080_s;
AnonFilter_a2_17081_t AnonFilter_a2_17081_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_17053() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_17053DUPLICATE_Splitter_17128));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_17056_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_17056_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_17056() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_17138() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[0]));
	ENDFOR
}

void AnonFilter_a3_17139() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[1]));
	ENDFOR
}

void AnonFilter_a3_17140() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[2]));
	ENDFOR
}

void AnonFilter_a3_17141() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[3]));
	ENDFOR
}

void AnonFilter_a3_17142() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[4]));
	ENDFOR
}

void AnonFilter_a3_17143() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[5]));
	ENDFOR
}

void AnonFilter_a3_17144() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[6]));
	ENDFOR
}

void AnonFilter_a3_17145() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[7]));
	ENDFOR
}

void AnonFilter_a3_17146() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[8]));
	ENDFOR
}

void AnonFilter_a3_17147() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[9]));
	ENDFOR
}

void AnonFilter_a3_17148() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17136() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17137() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17137Pre_CollapsedDataParallel_1_17126, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_17126() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_17137Pre_CollapsedDataParallel_1_17126), &(Pre_CollapsedDataParallel_1_17126WEIGHTED_ROUND_ROBIN_Splitter_17149));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17151_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_17151() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_17152() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_17153() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_17154() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_17155() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_17156() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_17157() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_17158() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17149() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_17126WEIGHTED_ROUND_ROBIN_Splitter_17149));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17150() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17150Post_CollapsedDataParallel_2_17127, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_17127() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_17150Post_CollapsedDataParallel_2_17127), &(Post_CollapsedDataParallel_2_17127WEIGHTED_ROUND_ROBIN_Splitter_17159));
	ENDFOR
}

void iDCT_1D_reference_fine_17161() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_17162() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_17163() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_17164() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_17165() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_17166() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_17167() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_17168() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17159() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_17127WEIGHTED_ROUND_ROBIN_Splitter_17159));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17160() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17160WEIGHTED_ROUND_ROBIN_Splitter_17169, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_17171() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[0]));
	ENDFOR
}

void AnonFilter_a4_17172() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[1]));
	ENDFOR
}

void AnonFilter_a4_17173() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[2]));
	ENDFOR
}

void AnonFilter_a4_17174() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[3]));
	ENDFOR
}

void AnonFilter_a4_17175() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[4]));
	ENDFOR
}

void AnonFilter_a4_17176() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[5]));
	ENDFOR
}

void AnonFilter_a4_17177() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[6]));
	ENDFOR
}

void AnonFilter_a4_17178() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[7]));
	ENDFOR
}

void AnonFilter_a4_17179() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[8]));
	ENDFOR
}

void AnonFilter_a4_17180() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[9]));
	ENDFOR
}

void AnonFilter_a4_17181() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17169() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_17160WEIGHTED_ROUND_ROBIN_Splitter_17169));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_17184() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[0]), &(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17185() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[1]), &(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17186() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[2]), &(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17187() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[3]), &(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17188() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[4]), &(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17189() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[5]), &(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17190() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[6]), &(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_17191() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[7]), &(SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17182() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_17183iDCT8x8_1D_col_fast_17080, pop_int(&SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_17080_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_17080_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_17080_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_17080_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_17080_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_17080_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_17080_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_17080_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_17080_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_17080_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_17080() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_17183iDCT8x8_1D_col_fast_17080), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_17128() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_17053DUPLICATE_Splitter_17128);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17129() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_17129AnonFilter_a2_17081, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_17081_s.count = (AnonFilter_a2_17081_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_17081_s.errors = (AnonFilter_a2_17081_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_17081_s.errors / AnonFilter_a2_17081_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_17081_s.errors = (AnonFilter_a2_17081_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_17081_s.errors / AnonFilter_a2_17081_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_17081() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_17129AnonFilter_a2_17081));
	ENDFOR
}

void __stream_init__() {
	init_buffer_int(&AnonFilter_a0_17053DUPLICATE_Splitter_17128);
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17137Pre_CollapsedDataParallel_1_17126);
	init_buffer_float(&Pre_CollapsedDataParallel_1_17126WEIGHTED_ROUND_ROBIN_Splitter_17149);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17054_17130_17192_17199_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_join[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_17183iDCT8x8_1D_col_fast_17080);
	FOR(int, __iter_init_4_, 0, <, 11, __iter_init_4_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_17193_17200_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 11, __iter_init_5_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_17193_17200_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17160WEIGHTED_ROUND_ROBIN_Splitter_17169);
	init_buffer_float(&Post_CollapsedDataParallel_2_17127WEIGHTED_ROUND_ROBIN_Splitter_17159);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_17129AnonFilter_a2_17081);
	FOR(int, __iter_init_6_, 0, <, 11, __iter_init_6_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_17196_17203_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17194_17201_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17195_17202_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17150Post_CollapsedDataParallel_2_17127);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_int(&SplitJoin46_iDCT8x8_1D_row_fast_Fiss_17197_17204_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 11, __iter_init_11_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_17196_17203_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_17056
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_17056_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17151
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17151_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17152
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17152_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17153
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17153_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17154
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17154_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17155
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17155_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17156
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17156_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17157
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17157_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17158
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17158_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17161
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17161_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17162
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17162_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17163
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17163_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17164
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17164_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17165
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17165_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17166
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17166_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17167
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17167_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17168
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17168_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_17081
	 {
	AnonFilter_a2_17081_s.count = 0.0 ; 
	AnonFilter_a2_17081_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_17053();
		DUPLICATE_Splitter_17128();
			iDCT_2D_reference_coarse_17056();
			WEIGHTED_ROUND_ROBIN_Splitter_17136();
				AnonFilter_a3_17138();
				AnonFilter_a3_17139();
				AnonFilter_a3_17140();
				AnonFilter_a3_17141();
				AnonFilter_a3_17142();
				AnonFilter_a3_17143();
				AnonFilter_a3_17144();
				AnonFilter_a3_17145();
				AnonFilter_a3_17146();
				AnonFilter_a3_17147();
				AnonFilter_a3_17148();
			WEIGHTED_ROUND_ROBIN_Joiner_17137();
			Pre_CollapsedDataParallel_1_17126();
			WEIGHTED_ROUND_ROBIN_Splitter_17149();
				iDCT_1D_reference_fine_17151();
				iDCT_1D_reference_fine_17152();
				iDCT_1D_reference_fine_17153();
				iDCT_1D_reference_fine_17154();
				iDCT_1D_reference_fine_17155();
				iDCT_1D_reference_fine_17156();
				iDCT_1D_reference_fine_17157();
				iDCT_1D_reference_fine_17158();
			WEIGHTED_ROUND_ROBIN_Joiner_17150();
			Post_CollapsedDataParallel_2_17127();
			WEIGHTED_ROUND_ROBIN_Splitter_17159();
				iDCT_1D_reference_fine_17161();
				iDCT_1D_reference_fine_17162();
				iDCT_1D_reference_fine_17163();
				iDCT_1D_reference_fine_17164();
				iDCT_1D_reference_fine_17165();
				iDCT_1D_reference_fine_17166();
				iDCT_1D_reference_fine_17167();
				iDCT_1D_reference_fine_17168();
			WEIGHTED_ROUND_ROBIN_Joiner_17160();
			WEIGHTED_ROUND_ROBIN_Splitter_17169();
				AnonFilter_a4_17171();
				AnonFilter_a4_17172();
				AnonFilter_a4_17173();
				AnonFilter_a4_17174();
				AnonFilter_a4_17175();
				AnonFilter_a4_17176();
				AnonFilter_a4_17177();
				AnonFilter_a4_17178();
				AnonFilter_a4_17179();
				AnonFilter_a4_17180();
				AnonFilter_a4_17181();
			WEIGHTED_ROUND_ROBIN_Joiner_17170();
			WEIGHTED_ROUND_ROBIN_Splitter_17182();
				iDCT8x8_1D_row_fast_17184();
				iDCT8x8_1D_row_fast_17185();
				iDCT8x8_1D_row_fast_17186();
				iDCT8x8_1D_row_fast_17187();
				iDCT8x8_1D_row_fast_17188();
				iDCT8x8_1D_row_fast_17189();
				iDCT8x8_1D_row_fast_17190();
				iDCT8x8_1D_row_fast_17191();
			WEIGHTED_ROUND_ROBIN_Joiner_17183();
			iDCT8x8_1D_col_fast_17080();
		WEIGHTED_ROUND_ROBIN_Joiner_17129();
		AnonFilter_a2_17081();
	ENDFOR
	return EXIT_SUCCESS;
}
