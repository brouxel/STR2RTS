#include "PEG21-iDCTcompare_nocache.h"

buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[8];
buffer_int_t AnonFilter_a0_14773DUPLICATE_Splitter_14848;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14880Post_CollapsedDataParallel_2_14847;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[21];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_14849AnonFilter_a2_14801;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[21];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[21];
buffer_float_t Pre_CollapsedDataParallel_1_14846WEIGHTED_ROUND_ROBIN_Splitter_14879;
buffer_int_t SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[8];
buffer_float_t Post_CollapsedDataParallel_2_14847WEIGHTED_ROUND_ROBIN_Splitter_14889;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14857Pre_CollapsedDataParallel_1_14846;
buffer_int_t SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14890WEIGHTED_ROUND_ROBIN_Splitter_14899;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[21];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[8];


iDCT_2D_reference_coarse_14776_t iDCT_2D_reference_coarse_14776_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14881_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14882_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14883_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14884_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14885_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14886_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14887_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14888_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14891_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14892_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14893_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14894_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14895_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14896_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14897_s;
iDCT_2D_reference_coarse_14776_t iDCT_1D_reference_fine_14898_s;
iDCT8x8_1D_col_fast_14800_t iDCT8x8_1D_col_fast_14800_s;
AnonFilter_a2_14801_t AnonFilter_a2_14801_s;

void AnonFilter_a0_14773(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_14773DUPLICATE_Splitter_14848, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_14776(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_14776_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_14776_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_14858(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14859(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14860(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14861(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14862(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14863(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14864(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14865(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14866(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14867(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14868(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14869(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14870(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14871(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14872(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14873(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14874(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14875(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14876(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14877(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14878(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[20])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14856() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14857() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14857Pre_CollapsedDataParallel_1_14846, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_14846(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_14846WEIGHTED_ROUND_ROBIN_Splitter_14879, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_14857Pre_CollapsedDataParallel_1_14846, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14857Pre_CollapsedDataParallel_1_14846) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14881(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14881_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14882(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14882_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14883(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14883_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14884(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14884_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14885(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14885_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14886(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14886_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14887(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14887_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14888(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14888_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14879() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_14846WEIGHTED_ROUND_ROBIN_Splitter_14879));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14880() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14880Post_CollapsedDataParallel_2_14847, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_14847(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_14847WEIGHTED_ROUND_ROBIN_Splitter_14889, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_14880Post_CollapsedDataParallel_2_14847, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14880Post_CollapsedDataParallel_2_14847) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14891(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14891_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14892(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14892_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14893(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14893_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14894(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14894_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14895(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14895_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14896(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14896_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14897(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14897_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14898(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14898_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14889() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_14847WEIGHTED_ROUND_ROBIN_Splitter_14889));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14890() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14890WEIGHTED_ROUND_ROBIN_Splitter_14899, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_14901(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14902(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14903(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14904(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14905(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14906(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14907(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14908(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14909(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14910(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14911(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14912(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14913(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14914(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14915(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14916(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14917(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14918(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14919(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14920(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14921(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14899() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14890WEIGHTED_ROUND_ROBIN_Splitter_14899));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14900() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_14924(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0], 6) ; 
		x3 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0], 2) ; 
		x4 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0], 1) ; 
		x5 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0], 7) ; 
		x6 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0], 5) ; 
		x7 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14925(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1], 6) ; 
		x3 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1], 2) ; 
		x4 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1], 1) ; 
		x5 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1], 7) ; 
		x6 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1], 5) ; 
		x7 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14926(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2], 6) ; 
		x3 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2], 2) ; 
		x4 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2], 1) ; 
		x5 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2], 7) ; 
		x6 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2], 5) ; 
		x7 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14927(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3], 6) ; 
		x3 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3], 2) ; 
		x4 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3], 1) ; 
		x5 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3], 7) ; 
		x6 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3], 5) ; 
		x7 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14928(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4], 6) ; 
		x3 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4], 2) ; 
		x4 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4], 1) ; 
		x5 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4], 7) ; 
		x6 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4], 5) ; 
		x7 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14929(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5], 6) ; 
		x3 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5], 2) ; 
		x4 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5], 1) ; 
		x5 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5], 7) ; 
		x6 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5], 5) ; 
		x7 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14930(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6], 6) ; 
		x3 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6], 2) ; 
		x4 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6], 1) ; 
		x5 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6], 7) ; 
		x6 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6], 5) ; 
		x7 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14931(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7], 6) ; 
		x3 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7], 2) ; 
		x4 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7], 1) ; 
		x5 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7], 7) ; 
		x6 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7], 5) ; 
		x7 = peek_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14922() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14923() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, pop_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_14800(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_14800_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_14800_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_14800_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_14800_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_14800_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_14800_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_14800_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_14800_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_14800_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_join[2], iDCT8x8_1D_col_fast_14800_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_14848() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_14773DUPLICATE_Splitter_14848);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14849() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_14849AnonFilter_a2_14801, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_14801(){
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14849AnonFilter_a2_14801) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14849AnonFilter_a2_14801) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14849AnonFilter_a2_14801) ; 
		AnonFilter_a2_14801_s.count = (AnonFilter_a2_14801_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_14801_s.errors = (AnonFilter_a2_14801_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_14801_s.errors / AnonFilter_a2_14801_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_14801_s.errors = (AnonFilter_a2_14801_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_14801_s.errors / AnonFilter_a2_14801_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_14773DUPLICATE_Splitter_14848);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14880Post_CollapsedDataParallel_2_14847);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 21, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_14849AnonFilter_a2_14801);
	FOR(int, __iter_init_4_, 0, <, 21, __iter_init_4_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14935_14942_split[__iter_init_5_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_14923iDCT8x8_1D_col_fast_14800);
	FOR(int, __iter_init_6_, 0, <, 21, __iter_init_6_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_14936_14943_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_14846WEIGHTED_ROUND_ROBIN_Splitter_14879);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_14847WEIGHTED_ROUND_ROBIN_Splitter_14889);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14857Pre_CollapsedDataParallel_1_14846);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin66_iDCT8x8_1D_row_fast_Fiss_14937_14944_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14890WEIGHTED_ROUND_ROBIN_Splitter_14899);
	FOR(int, __iter_init_9_, 0, <, 21, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_14933_14940_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14774_14850_14932_14939_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14934_14941_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_14776
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_14776_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14881
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14881_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14882
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14882_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14883
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14883_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14884
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14884_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14885
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14885_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14886
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14886_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14887
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14887_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14888
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14888_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14891
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14891_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14892
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14892_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14893
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14893_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14894
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14894_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14895
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14895_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14896
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14896_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14897
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14897_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14898
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14898_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_14801
	 {
	AnonFilter_a2_14801_s.count = 0.0 ; 
	AnonFilter_a2_14801_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_14773();
		DUPLICATE_Splitter_14848();
			iDCT_2D_reference_coarse_14776();
			WEIGHTED_ROUND_ROBIN_Splitter_14856();
				AnonFilter_a3_14858();
				AnonFilter_a3_14859();
				AnonFilter_a3_14860();
				AnonFilter_a3_14861();
				AnonFilter_a3_14862();
				AnonFilter_a3_14863();
				AnonFilter_a3_14864();
				AnonFilter_a3_14865();
				AnonFilter_a3_14866();
				AnonFilter_a3_14867();
				AnonFilter_a3_14868();
				AnonFilter_a3_14869();
				AnonFilter_a3_14870();
				AnonFilter_a3_14871();
				AnonFilter_a3_14872();
				AnonFilter_a3_14873();
				AnonFilter_a3_14874();
				AnonFilter_a3_14875();
				AnonFilter_a3_14876();
				AnonFilter_a3_14877();
				AnonFilter_a3_14878();
			WEIGHTED_ROUND_ROBIN_Joiner_14857();
			Pre_CollapsedDataParallel_1_14846();
			WEIGHTED_ROUND_ROBIN_Splitter_14879();
				iDCT_1D_reference_fine_14881();
				iDCT_1D_reference_fine_14882();
				iDCT_1D_reference_fine_14883();
				iDCT_1D_reference_fine_14884();
				iDCT_1D_reference_fine_14885();
				iDCT_1D_reference_fine_14886();
				iDCT_1D_reference_fine_14887();
				iDCT_1D_reference_fine_14888();
			WEIGHTED_ROUND_ROBIN_Joiner_14880();
			Post_CollapsedDataParallel_2_14847();
			WEIGHTED_ROUND_ROBIN_Splitter_14889();
				iDCT_1D_reference_fine_14891();
				iDCT_1D_reference_fine_14892();
				iDCT_1D_reference_fine_14893();
				iDCT_1D_reference_fine_14894();
				iDCT_1D_reference_fine_14895();
				iDCT_1D_reference_fine_14896();
				iDCT_1D_reference_fine_14897();
				iDCT_1D_reference_fine_14898();
			WEIGHTED_ROUND_ROBIN_Joiner_14890();
			WEIGHTED_ROUND_ROBIN_Splitter_14899();
				AnonFilter_a4_14901();
				AnonFilter_a4_14902();
				AnonFilter_a4_14903();
				AnonFilter_a4_14904();
				AnonFilter_a4_14905();
				AnonFilter_a4_14906();
				AnonFilter_a4_14907();
				AnonFilter_a4_14908();
				AnonFilter_a4_14909();
				AnonFilter_a4_14910();
				AnonFilter_a4_14911();
				AnonFilter_a4_14912();
				AnonFilter_a4_14913();
				AnonFilter_a4_14914();
				AnonFilter_a4_14915();
				AnonFilter_a4_14916();
				AnonFilter_a4_14917();
				AnonFilter_a4_14918();
				AnonFilter_a4_14919();
				AnonFilter_a4_14920();
				AnonFilter_a4_14921();
			WEIGHTED_ROUND_ROBIN_Joiner_14900();
			WEIGHTED_ROUND_ROBIN_Splitter_14922();
				iDCT8x8_1D_row_fast_14924();
				iDCT8x8_1D_row_fast_14925();
				iDCT8x8_1D_row_fast_14926();
				iDCT8x8_1D_row_fast_14927();
				iDCT8x8_1D_row_fast_14928();
				iDCT8x8_1D_row_fast_14929();
				iDCT8x8_1D_row_fast_14930();
				iDCT8x8_1D_row_fast_14931();
			WEIGHTED_ROUND_ROBIN_Joiner_14923();
			iDCT8x8_1D_col_fast_14800();
		WEIGHTED_ROUND_ROBIN_Joiner_14849();
		AnonFilter_a2_14801();
	ENDFOR
	return EXIT_SUCCESS;
}
