#include "PEG38-iDCTcompare.h"

buffer_int_t SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_10055AnonFilter_a2_10007;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[38];
buffer_float_t Post_CollapsedDataParallel_2_10053WEIGHTED_ROUND_ROBIN_Splitter_10112;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10103Post_CollapsedDataParallel_2_10053;
buffer_float_t Pre_CollapsedDataParallel_1_10052WEIGHTED_ROUND_ROBIN_Splitter_10102;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_10163iDCT8x8_1D_col_fast_10006;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[38];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10063Pre_CollapsedDataParallel_1_10052;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10113WEIGHTED_ROUND_ROBIN_Splitter_10122;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[38];
buffer_int_t SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[38];
buffer_int_t AnonFilter_a0_9979DUPLICATE_Splitter_10054;


iDCT_2D_reference_coarse_9982_t iDCT_2D_reference_coarse_9982_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10104_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10105_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10106_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10107_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10108_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10109_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10110_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10111_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10114_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10115_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10116_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10117_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10118_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10119_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10120_s;
iDCT_2D_reference_coarse_9982_t iDCT_1D_reference_fine_10121_s;
iDCT8x8_1D_col_fast_10006_t iDCT8x8_1D_col_fast_10006_s;
AnonFilter_a2_10007_t AnonFilter_a2_10007_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_9979() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_9979DUPLICATE_Splitter_10054));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_9982_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_9982_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_9982() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_10064() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[0]));
	ENDFOR
}

void AnonFilter_a3_10065() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[1]));
	ENDFOR
}

void AnonFilter_a3_10066() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[2]));
	ENDFOR
}

void AnonFilter_a3_10067() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[3]));
	ENDFOR
}

void AnonFilter_a3_10068() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[4]));
	ENDFOR
}

void AnonFilter_a3_10069() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[5]));
	ENDFOR
}

void AnonFilter_a3_10070() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[6]));
	ENDFOR
}

void AnonFilter_a3_10071() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[7]));
	ENDFOR
}

void AnonFilter_a3_10072() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[8]));
	ENDFOR
}

void AnonFilter_a3_10073() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[9]));
	ENDFOR
}

void AnonFilter_a3_10074() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[10]));
	ENDFOR
}

void AnonFilter_a3_10075() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[11]));
	ENDFOR
}

void AnonFilter_a3_10076() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[12]));
	ENDFOR
}

void AnonFilter_a3_10077() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[13]));
	ENDFOR
}

void AnonFilter_a3_10078() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[14]));
	ENDFOR
}

void AnonFilter_a3_10079() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[15]));
	ENDFOR
}

void AnonFilter_a3_10080() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[16]));
	ENDFOR
}

void AnonFilter_a3_10081() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[17]));
	ENDFOR
}

void AnonFilter_a3_10082() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[18]));
	ENDFOR
}

void AnonFilter_a3_10083() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[19]));
	ENDFOR
}

void AnonFilter_a3_10084() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[20]));
	ENDFOR
}

void AnonFilter_a3_10085() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[21]));
	ENDFOR
}

void AnonFilter_a3_10086() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[22]));
	ENDFOR
}

void AnonFilter_a3_10087() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[23]));
	ENDFOR
}

void AnonFilter_a3_10088() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[24]));
	ENDFOR
}

void AnonFilter_a3_10089() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[25]));
	ENDFOR
}

void AnonFilter_a3_10090() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[26]));
	ENDFOR
}

void AnonFilter_a3_10091() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[27]));
	ENDFOR
}

void AnonFilter_a3_10092() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[28]));
	ENDFOR
}

void AnonFilter_a3_10093() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[29]));
	ENDFOR
}

void AnonFilter_a3_10094() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[30]));
	ENDFOR
}

void AnonFilter_a3_10095() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[31]));
	ENDFOR
}

void AnonFilter_a3_10096() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[32]));
	ENDFOR
}

void AnonFilter_a3_10097() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[33]));
	ENDFOR
}

void AnonFilter_a3_10098() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[34]));
	ENDFOR
}

void AnonFilter_a3_10099() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[35]));
	ENDFOR
}

void AnonFilter_a3_10100() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[36]));
	ENDFOR
}

void AnonFilter_a3_10101() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[37]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10062() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 38, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10063() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 38, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10063Pre_CollapsedDataParallel_1_10052, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_10052() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_10063Pre_CollapsedDataParallel_1_10052), &(Pre_CollapsedDataParallel_1_10052WEIGHTED_ROUND_ROBIN_Splitter_10102));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_10104_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_10104() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_10105() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_10106() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_10107() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_10108() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_10109() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_10110() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_10111() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10102() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10052WEIGHTED_ROUND_ROBIN_Splitter_10102));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10103() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10103Post_CollapsedDataParallel_2_10053, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_10053() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_10103Post_CollapsedDataParallel_2_10053), &(Post_CollapsedDataParallel_2_10053WEIGHTED_ROUND_ROBIN_Splitter_10112));
	ENDFOR
}

void iDCT_1D_reference_fine_10114() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_10115() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_10116() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_10117() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_10118() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_10119() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_10120() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_10121() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10112() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_10053WEIGHTED_ROUND_ROBIN_Splitter_10112));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10113() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10113WEIGHTED_ROUND_ROBIN_Splitter_10122, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_10124() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[0]));
	ENDFOR
}

void AnonFilter_a4_10125() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[1]));
	ENDFOR
}

void AnonFilter_a4_10126() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[2]));
	ENDFOR
}

void AnonFilter_a4_10127() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[3]));
	ENDFOR
}

void AnonFilter_a4_10128() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[4]));
	ENDFOR
}

void AnonFilter_a4_10129() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[5]));
	ENDFOR
}

void AnonFilter_a4_10130() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[6]));
	ENDFOR
}

void AnonFilter_a4_10131() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[7]));
	ENDFOR
}

void AnonFilter_a4_10132() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[8]));
	ENDFOR
}

void AnonFilter_a4_10133() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[9]));
	ENDFOR
}

void AnonFilter_a4_10134() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[10]));
	ENDFOR
}

void AnonFilter_a4_10135() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[11]));
	ENDFOR
}

void AnonFilter_a4_10136() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[12]));
	ENDFOR
}

void AnonFilter_a4_10137() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[13]));
	ENDFOR
}

void AnonFilter_a4_10138() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[14]));
	ENDFOR
}

void AnonFilter_a4_10139() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[15]));
	ENDFOR
}

void AnonFilter_a4_10140() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[16]));
	ENDFOR
}

void AnonFilter_a4_10141() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[17]));
	ENDFOR
}

void AnonFilter_a4_10142() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[18]));
	ENDFOR
}

void AnonFilter_a4_10143() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[19]));
	ENDFOR
}

void AnonFilter_a4_10144() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[20]));
	ENDFOR
}

void AnonFilter_a4_10145() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[21]));
	ENDFOR
}

void AnonFilter_a4_10146() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[22]));
	ENDFOR
}

void AnonFilter_a4_10147() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[23]));
	ENDFOR
}

void AnonFilter_a4_10148() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[24]));
	ENDFOR
}

void AnonFilter_a4_10149() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[25]));
	ENDFOR
}

void AnonFilter_a4_10150() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[26]));
	ENDFOR
}

void AnonFilter_a4_10151() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[27]));
	ENDFOR
}

void AnonFilter_a4_10152() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[28]));
	ENDFOR
}

void AnonFilter_a4_10153() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[29]));
	ENDFOR
}

void AnonFilter_a4_10154() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[30]));
	ENDFOR
}

void AnonFilter_a4_10155() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[31]));
	ENDFOR
}

void AnonFilter_a4_10156() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[32]));
	ENDFOR
}

void AnonFilter_a4_10157() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[33]));
	ENDFOR
}

void AnonFilter_a4_10158() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[34]));
	ENDFOR
}

void AnonFilter_a4_10159() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[35]));
	ENDFOR
}

void AnonFilter_a4_10160() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[36]));
	ENDFOR
}

void AnonFilter_a4_10161() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[37]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10122() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 38, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10113WEIGHTED_ROUND_ROBIN_Splitter_10122));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10123() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 38, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_10164() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[0]), &(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10165() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[1]), &(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10166() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[2]), &(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10167() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[3]), &(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10168() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[4]), &(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10169() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[5]), &(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10170() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[6]), &(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10171() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[7]), &(SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10162() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10163() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_10163iDCT8x8_1D_col_fast_10006, pop_int(&SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_10006_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_10006_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_10006_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_10006_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_10006_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_10006_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_10006_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_10006_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_10006_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_10006_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_10006() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_10163iDCT8x8_1D_col_fast_10006), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_10054() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_9979DUPLICATE_Splitter_10054);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10055() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_10055AnonFilter_a2_10007, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_10007_s.count = (AnonFilter_a2_10007_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_10007_s.errors = (AnonFilter_a2_10007_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_10007_s.errors / AnonFilter_a2_10007_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_10007_s.errors = (AnonFilter_a2_10007_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_10007_s.errors / AnonFilter_a2_10007_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_10007() {
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_10055AnonFilter_a2_10007));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_split[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_10055AnonFilter_a2_10007);
	FOR(int, __iter_init_2_, 0, <, 38, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_10176_10183_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_10053WEIGHTED_ROUND_ROBIN_Splitter_10112);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10103Post_CollapsedDataParallel_2_10053);
	init_buffer_float(&Pre_CollapsedDataParallel_1_10052WEIGHTED_ROUND_ROBIN_Splitter_10102);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_10163iDCT8x8_1D_col_fast_10006);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10175_10182_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 38, __iter_init_5_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_10173_10180_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10063Pre_CollapsedDataParallel_1_10052);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10113WEIGHTED_ROUND_ROBIN_Splitter_10122);
	FOR(int, __iter_init_7_, 0, <, 38, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_10176_10183_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin100_iDCT8x8_1D_row_fast_Fiss_10177_10184_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9980_10056_10172_10179_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10174_10181_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 38, __iter_init_11_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_10173_10180_split[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_9979DUPLICATE_Splitter_10054);
// --- init: iDCT_2D_reference_coarse_9982
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_9982_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10104
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10104_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10105
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10105_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10106
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10106_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10107
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10107_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10108
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10108_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10109
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10109_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10110
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10110_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10111
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10111_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10114
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10114_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10115
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10115_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10116
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10116_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10117
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10117_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10118
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10118_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10119
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10119_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10120
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10120_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10121
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10121_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_10007
	 {
	AnonFilter_a2_10007_s.count = 0.0 ; 
	AnonFilter_a2_10007_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_9979();
		DUPLICATE_Splitter_10054();
			iDCT_2D_reference_coarse_9982();
			WEIGHTED_ROUND_ROBIN_Splitter_10062();
				AnonFilter_a3_10064();
				AnonFilter_a3_10065();
				AnonFilter_a3_10066();
				AnonFilter_a3_10067();
				AnonFilter_a3_10068();
				AnonFilter_a3_10069();
				AnonFilter_a3_10070();
				AnonFilter_a3_10071();
				AnonFilter_a3_10072();
				AnonFilter_a3_10073();
				AnonFilter_a3_10074();
				AnonFilter_a3_10075();
				AnonFilter_a3_10076();
				AnonFilter_a3_10077();
				AnonFilter_a3_10078();
				AnonFilter_a3_10079();
				AnonFilter_a3_10080();
				AnonFilter_a3_10081();
				AnonFilter_a3_10082();
				AnonFilter_a3_10083();
				AnonFilter_a3_10084();
				AnonFilter_a3_10085();
				AnonFilter_a3_10086();
				AnonFilter_a3_10087();
				AnonFilter_a3_10088();
				AnonFilter_a3_10089();
				AnonFilter_a3_10090();
				AnonFilter_a3_10091();
				AnonFilter_a3_10092();
				AnonFilter_a3_10093();
				AnonFilter_a3_10094();
				AnonFilter_a3_10095();
				AnonFilter_a3_10096();
				AnonFilter_a3_10097();
				AnonFilter_a3_10098();
				AnonFilter_a3_10099();
				AnonFilter_a3_10100();
				AnonFilter_a3_10101();
			WEIGHTED_ROUND_ROBIN_Joiner_10063();
			Pre_CollapsedDataParallel_1_10052();
			WEIGHTED_ROUND_ROBIN_Splitter_10102();
				iDCT_1D_reference_fine_10104();
				iDCT_1D_reference_fine_10105();
				iDCT_1D_reference_fine_10106();
				iDCT_1D_reference_fine_10107();
				iDCT_1D_reference_fine_10108();
				iDCT_1D_reference_fine_10109();
				iDCT_1D_reference_fine_10110();
				iDCT_1D_reference_fine_10111();
			WEIGHTED_ROUND_ROBIN_Joiner_10103();
			Post_CollapsedDataParallel_2_10053();
			WEIGHTED_ROUND_ROBIN_Splitter_10112();
				iDCT_1D_reference_fine_10114();
				iDCT_1D_reference_fine_10115();
				iDCT_1D_reference_fine_10116();
				iDCT_1D_reference_fine_10117();
				iDCT_1D_reference_fine_10118();
				iDCT_1D_reference_fine_10119();
				iDCT_1D_reference_fine_10120();
				iDCT_1D_reference_fine_10121();
			WEIGHTED_ROUND_ROBIN_Joiner_10113();
			WEIGHTED_ROUND_ROBIN_Splitter_10122();
				AnonFilter_a4_10124();
				AnonFilter_a4_10125();
				AnonFilter_a4_10126();
				AnonFilter_a4_10127();
				AnonFilter_a4_10128();
				AnonFilter_a4_10129();
				AnonFilter_a4_10130();
				AnonFilter_a4_10131();
				AnonFilter_a4_10132();
				AnonFilter_a4_10133();
				AnonFilter_a4_10134();
				AnonFilter_a4_10135();
				AnonFilter_a4_10136();
				AnonFilter_a4_10137();
				AnonFilter_a4_10138();
				AnonFilter_a4_10139();
				AnonFilter_a4_10140();
				AnonFilter_a4_10141();
				AnonFilter_a4_10142();
				AnonFilter_a4_10143();
				AnonFilter_a4_10144();
				AnonFilter_a4_10145();
				AnonFilter_a4_10146();
				AnonFilter_a4_10147();
				AnonFilter_a4_10148();
				AnonFilter_a4_10149();
				AnonFilter_a4_10150();
				AnonFilter_a4_10151();
				AnonFilter_a4_10152();
				AnonFilter_a4_10153();
				AnonFilter_a4_10154();
				AnonFilter_a4_10155();
				AnonFilter_a4_10156();
				AnonFilter_a4_10157();
				AnonFilter_a4_10158();
				AnonFilter_a4_10159();
				AnonFilter_a4_10160();
				AnonFilter_a4_10161();
			WEIGHTED_ROUND_ROBIN_Joiner_10123();
			WEIGHTED_ROUND_ROBIN_Splitter_10162();
				iDCT8x8_1D_row_fast_10164();
				iDCT8x8_1D_row_fast_10165();
				iDCT8x8_1D_row_fast_10166();
				iDCT8x8_1D_row_fast_10167();
				iDCT8x8_1D_row_fast_10168();
				iDCT8x8_1D_row_fast_10169();
				iDCT8x8_1D_row_fast_10170();
				iDCT8x8_1D_row_fast_10171();
			WEIGHTED_ROUND_ROBIN_Joiner_10163();
			iDCT8x8_1D_col_fast_10006();
		WEIGHTED_ROUND_ROBIN_Joiner_10055();
		AnonFilter_a2_10007();
	ENDFOR
	return EXIT_SUCCESS;
}
