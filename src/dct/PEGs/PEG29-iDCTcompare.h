#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=11136 on the compile command line
#else
#if BUF_SIZEMAX < 11136
#error BUF_SIZEMAX too small, it must be at least 11136
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_12664_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_12688_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_12689_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_12661();
void DUPLICATE_Splitter_12736();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_12664();
void WEIGHTED_ROUND_ROBIN_Splitter_12744();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_12746();
void AnonFilter_a3_12747();
void AnonFilter_a3_12748();
void AnonFilter_a3_12749();
void AnonFilter_a3_12750();
void AnonFilter_a3_12751();
void AnonFilter_a3_12752();
void AnonFilter_a3_12753();
void AnonFilter_a3_12754();
void AnonFilter_a3_12755();
void AnonFilter_a3_12756();
void AnonFilter_a3_12757();
void AnonFilter_a3_12758();
void AnonFilter_a3_12759();
void AnonFilter_a3_12760();
void AnonFilter_a3_12761();
void AnonFilter_a3_12762();
void AnonFilter_a3_12763();
void AnonFilter_a3_12764();
void AnonFilter_a3_12765();
void AnonFilter_a3_12766();
void AnonFilter_a3_12767();
void AnonFilter_a3_12768();
void AnonFilter_a3_12769();
void AnonFilter_a3_12770();
void AnonFilter_a3_12771();
void AnonFilter_a3_12772();
void AnonFilter_a3_12773();
void AnonFilter_a3_12774();
void WEIGHTED_ROUND_ROBIN_Joiner_12745();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_12734();
void WEIGHTED_ROUND_ROBIN_Splitter_12775();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_12777();
void iDCT_1D_reference_fine_12778();
void iDCT_1D_reference_fine_12779();
void iDCT_1D_reference_fine_12780();
void iDCT_1D_reference_fine_12781();
void iDCT_1D_reference_fine_12782();
void iDCT_1D_reference_fine_12783();
void iDCT_1D_reference_fine_12784();
void WEIGHTED_ROUND_ROBIN_Joiner_12776();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_12735();
void WEIGHTED_ROUND_ROBIN_Splitter_12785();
void iDCT_1D_reference_fine_12787();
void iDCT_1D_reference_fine_12788();
void iDCT_1D_reference_fine_12789();
void iDCT_1D_reference_fine_12790();
void iDCT_1D_reference_fine_12791();
void iDCT_1D_reference_fine_12792();
void iDCT_1D_reference_fine_12793();
void iDCT_1D_reference_fine_12794();
void WEIGHTED_ROUND_ROBIN_Joiner_12786();
void WEIGHTED_ROUND_ROBIN_Splitter_12795();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_12797();
void AnonFilter_a4_12798();
void AnonFilter_a4_12799();
void AnonFilter_a4_12800();
void AnonFilter_a4_12801();
void AnonFilter_a4_12802();
void AnonFilter_a4_12803();
void AnonFilter_a4_12804();
void AnonFilter_a4_12805();
void AnonFilter_a4_12806();
void AnonFilter_a4_12807();
void AnonFilter_a4_12808();
void AnonFilter_a4_12809();
void AnonFilter_a4_12810();
void AnonFilter_a4_12811();
void AnonFilter_a4_12812();
void AnonFilter_a4_12813();
void AnonFilter_a4_12814();
void AnonFilter_a4_12815();
void AnonFilter_a4_12816();
void AnonFilter_a4_12817();
void AnonFilter_a4_12818();
void AnonFilter_a4_12819();
void AnonFilter_a4_12820();
void AnonFilter_a4_12821();
void AnonFilter_a4_12822();
void AnonFilter_a4_12823();
void AnonFilter_a4_12824();
void AnonFilter_a4_12825();
void WEIGHTED_ROUND_ROBIN_Joiner_12796();
void WEIGHTED_ROUND_ROBIN_Splitter_12826();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_12828();
void iDCT8x8_1D_row_fast_12829();
void iDCT8x8_1D_row_fast_12830();
void iDCT8x8_1D_row_fast_12831();
void iDCT8x8_1D_row_fast_12832();
void iDCT8x8_1D_row_fast_12833();
void iDCT8x8_1D_row_fast_12834();
void iDCT8x8_1D_row_fast_12835();
void WEIGHTED_ROUND_ROBIN_Joiner_12827();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_12688();
void WEIGHTED_ROUND_ROBIN_Joiner_12737();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_12689();

#ifdef __cplusplus
}
#endif
#endif
