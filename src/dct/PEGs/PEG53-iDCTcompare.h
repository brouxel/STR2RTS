#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=20352 on the compile command line
#else
#if BUF_SIZEMAX < 20352
#error BUF_SIZEMAX too small, it must be at least 20352
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_4792_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_4816_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_4817_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_4789();
void DUPLICATE_Splitter_4864();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_4792();
void WEIGHTED_ROUND_ROBIN_Splitter_4872();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_4874();
void AnonFilter_a3_4875();
void AnonFilter_a3_4876();
void AnonFilter_a3_4877();
void AnonFilter_a3_4878();
void AnonFilter_a3_4879();
void AnonFilter_a3_4880();
void AnonFilter_a3_4881();
void AnonFilter_a3_4882();
void AnonFilter_a3_4883();
void AnonFilter_a3_4884();
void AnonFilter_a3_4885();
void AnonFilter_a3_4886();
void AnonFilter_a3_4887();
void AnonFilter_a3_4888();
void AnonFilter_a3_4889();
void AnonFilter_a3_4890();
void AnonFilter_a3_4891();
void AnonFilter_a3_4892();
void AnonFilter_a3_4893();
void AnonFilter_a3_4894();
void AnonFilter_a3_4895();
void AnonFilter_a3_4896();
void AnonFilter_a3_4897();
void AnonFilter_a3_4898();
void AnonFilter_a3_4899();
void AnonFilter_a3_4900();
void AnonFilter_a3_4901();
void AnonFilter_a3_4902();
void AnonFilter_a3_4903();
void AnonFilter_a3_4904();
void AnonFilter_a3_4905();
void AnonFilter_a3_4906();
void AnonFilter_a3_4907();
void AnonFilter_a3_4908();
void AnonFilter_a3_4909();
void AnonFilter_a3_4910();
void AnonFilter_a3_4911();
void AnonFilter_a3_4912();
void AnonFilter_a3_4913();
void AnonFilter_a3_4914();
void AnonFilter_a3_4915();
void AnonFilter_a3_4916();
void AnonFilter_a3_4917();
void AnonFilter_a3_4918();
void AnonFilter_a3_4919();
void AnonFilter_a3_4920();
void AnonFilter_a3_4921();
void AnonFilter_a3_4922();
void AnonFilter_a3_4923();
void AnonFilter_a3_4924();
void AnonFilter_a3_4925();
void AnonFilter_a3_4926();
void WEIGHTED_ROUND_ROBIN_Joiner_4873();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_4862();
void WEIGHTED_ROUND_ROBIN_Splitter_4927();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_4929();
void iDCT_1D_reference_fine_4930();
void iDCT_1D_reference_fine_4931();
void iDCT_1D_reference_fine_4932();
void iDCT_1D_reference_fine_4933();
void iDCT_1D_reference_fine_4934();
void iDCT_1D_reference_fine_4935();
void iDCT_1D_reference_fine_4936();
void WEIGHTED_ROUND_ROBIN_Joiner_4928();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_4863();
void WEIGHTED_ROUND_ROBIN_Splitter_4937();
void iDCT_1D_reference_fine_4939();
void iDCT_1D_reference_fine_4940();
void iDCT_1D_reference_fine_4941();
void iDCT_1D_reference_fine_4942();
void iDCT_1D_reference_fine_4943();
void iDCT_1D_reference_fine_4944();
void iDCT_1D_reference_fine_4945();
void iDCT_1D_reference_fine_4946();
void WEIGHTED_ROUND_ROBIN_Joiner_4938();
void WEIGHTED_ROUND_ROBIN_Splitter_4947();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_4949();
void AnonFilter_a4_4950();
void AnonFilter_a4_4951();
void AnonFilter_a4_4952();
void AnonFilter_a4_4953();
void AnonFilter_a4_4954();
void AnonFilter_a4_4955();
void AnonFilter_a4_4956();
void AnonFilter_a4_4957();
void AnonFilter_a4_4958();
void AnonFilter_a4_4959();
void AnonFilter_a4_4960();
void AnonFilter_a4_4961();
void AnonFilter_a4_4962();
void AnonFilter_a4_4963();
void AnonFilter_a4_4964();
void AnonFilter_a4_4965();
void AnonFilter_a4_4966();
void AnonFilter_a4_4967();
void AnonFilter_a4_4968();
void AnonFilter_a4_4969();
void AnonFilter_a4_4970();
void AnonFilter_a4_4971();
void AnonFilter_a4_4972();
void AnonFilter_a4_4973();
void AnonFilter_a4_4974();
void AnonFilter_a4_4975();
void AnonFilter_a4_4976();
void AnonFilter_a4_4977();
void AnonFilter_a4_4978();
void AnonFilter_a4_4979();
void AnonFilter_a4_4980();
void AnonFilter_a4_4981();
void AnonFilter_a4_4982();
void AnonFilter_a4_4983();
void AnonFilter_a4_4984();
void AnonFilter_a4_4985();
void AnonFilter_a4_4986();
void AnonFilter_a4_4987();
void AnonFilter_a4_4988();
void AnonFilter_a4_4989();
void AnonFilter_a4_4990();
void AnonFilter_a4_4991();
void AnonFilter_a4_4992();
void AnonFilter_a4_4993();
void AnonFilter_a4_4994();
void AnonFilter_a4_4995();
void AnonFilter_a4_4996();
void AnonFilter_a4_4997();
void AnonFilter_a4_4998();
void AnonFilter_a4_4999();
void AnonFilter_a4_5000();
void AnonFilter_a4_5001();
void WEIGHTED_ROUND_ROBIN_Joiner_4948();
void WEIGHTED_ROUND_ROBIN_Splitter_5002();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_5004();
void iDCT8x8_1D_row_fast_5005();
void iDCT8x8_1D_row_fast_5006();
void iDCT8x8_1D_row_fast_5007();
void iDCT8x8_1D_row_fast_5008();
void iDCT8x8_1D_row_fast_5009();
void iDCT8x8_1D_row_fast_5010();
void iDCT8x8_1D_row_fast_5011();
void WEIGHTED_ROUND_ROBIN_Joiner_5003();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_4816();
void WEIGHTED_ROUND_ROBIN_Joiner_4865();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_4817();

#ifdef __cplusplus
}
#endif
#endif
