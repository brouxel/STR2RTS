#include "PEG40-iDCTcompare.h"

buffer_float_t Pre_CollapsedDataParallel_1_9412WEIGHTED_ROUND_ROBIN_Splitter_9464;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[8];
buffer_int_t AnonFilter_a0_9339DUPLICATE_Splitter_9414;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[40];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9475WEIGHTED_ROUND_ROBIN_Splitter_9484;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[40];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366;
buffer_int_t SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[40];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[8];
buffer_int_t SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[40];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412;
buffer_float_t Post_CollapsedDataParallel_2_9413WEIGHTED_ROUND_ROBIN_Splitter_9474;


iDCT_2D_reference_coarse_9342_t iDCT_2D_reference_coarse_9342_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9466_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9467_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9468_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9469_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9470_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9471_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9472_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9473_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9476_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9477_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9478_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9479_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9480_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9481_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9482_s;
iDCT_2D_reference_coarse_9342_t iDCT_1D_reference_fine_9483_s;
iDCT8x8_1D_col_fast_9366_t iDCT8x8_1D_col_fast_9366_s;
AnonFilter_a2_9367_t AnonFilter_a2_9367_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_9339() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_9339DUPLICATE_Splitter_9414));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_9342_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_9342_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_9342() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_9424() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[0]));
	ENDFOR
}

void AnonFilter_a3_9425() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[1]));
	ENDFOR
}

void AnonFilter_a3_9426() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[2]));
	ENDFOR
}

void AnonFilter_a3_9427() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[3]));
	ENDFOR
}

void AnonFilter_a3_9428() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[4]));
	ENDFOR
}

void AnonFilter_a3_9429() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[5]));
	ENDFOR
}

void AnonFilter_a3_9430() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[6]));
	ENDFOR
}

void AnonFilter_a3_9431() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[7]));
	ENDFOR
}

void AnonFilter_a3_9432() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[8]));
	ENDFOR
}

void AnonFilter_a3_9433() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[9]));
	ENDFOR
}

void AnonFilter_a3_9434() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[10]));
	ENDFOR
}

void AnonFilter_a3_9435() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[11]));
	ENDFOR
}

void AnonFilter_a3_9436() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[12]));
	ENDFOR
}

void AnonFilter_a3_9437() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[13]));
	ENDFOR
}

void AnonFilter_a3_9438() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[14]));
	ENDFOR
}

void AnonFilter_a3_9439() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[15]));
	ENDFOR
}

void AnonFilter_a3_9440() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[16]));
	ENDFOR
}

void AnonFilter_a3_9441() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[17]));
	ENDFOR
}

void AnonFilter_a3_9442() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[18]));
	ENDFOR
}

void AnonFilter_a3_9443() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[19]));
	ENDFOR
}

void AnonFilter_a3_9444() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[20]));
	ENDFOR
}

void AnonFilter_a3_9445() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[21]));
	ENDFOR
}

void AnonFilter_a3_9446() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[22]));
	ENDFOR
}

void AnonFilter_a3_9447() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[23]));
	ENDFOR
}

void AnonFilter_a3_9448() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[24]));
	ENDFOR
}

void AnonFilter_a3_9449() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[25]));
	ENDFOR
}

void AnonFilter_a3_9450() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[26]));
	ENDFOR
}

void AnonFilter_a3_9451() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[27]));
	ENDFOR
}

void AnonFilter_a3_9452() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[28]));
	ENDFOR
}

void AnonFilter_a3_9453() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[29]));
	ENDFOR
}

void AnonFilter_a3_9454() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[30]));
	ENDFOR
}

void AnonFilter_a3_9455() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[31]));
	ENDFOR
}

void AnonFilter_a3_9456() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[32]));
	ENDFOR
}

void AnonFilter_a3_9457() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[33]));
	ENDFOR
}

void AnonFilter_a3_9458() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[34]));
	ENDFOR
}

void AnonFilter_a3_9459() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[35]));
	ENDFOR
}

void AnonFilter_a3_9460() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[36]));
	ENDFOR
}

void AnonFilter_a3_9461() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[37]));
	ENDFOR
}

void AnonFilter_a3_9462() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[38]));
	ENDFOR
}

void AnonFilter_a3_9463() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[39]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9422() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9423() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_9412() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412), &(Pre_CollapsedDataParallel_1_9412WEIGHTED_ROUND_ROBIN_Splitter_9464));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9466_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_9466() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_9467() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_9468() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_9469() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_9470() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_9471() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_9472() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_9473() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9412WEIGHTED_ROUND_ROBIN_Splitter_9464));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9465() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_9413() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413), &(Post_CollapsedDataParallel_2_9413WEIGHTED_ROUND_ROBIN_Splitter_9474));
	ENDFOR
}

void iDCT_1D_reference_fine_9476() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_9477() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_9478() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_9479() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_9480() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_9481() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_9482() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_9483() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9474() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_9413WEIGHTED_ROUND_ROBIN_Splitter_9474));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9475() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9475WEIGHTED_ROUND_ROBIN_Splitter_9484, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_9486() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[0]));
	ENDFOR
}

void AnonFilter_a4_9487() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[1]));
	ENDFOR
}

void AnonFilter_a4_9488() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[2]));
	ENDFOR
}

void AnonFilter_a4_9489() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[3]));
	ENDFOR
}

void AnonFilter_a4_9490() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[4]));
	ENDFOR
}

void AnonFilter_a4_9491() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[5]));
	ENDFOR
}

void AnonFilter_a4_9492() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[6]));
	ENDFOR
}

void AnonFilter_a4_9493() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[7]));
	ENDFOR
}

void AnonFilter_a4_9494() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[8]));
	ENDFOR
}

void AnonFilter_a4_9495() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[9]));
	ENDFOR
}

void AnonFilter_a4_9496() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[10]));
	ENDFOR
}

void AnonFilter_a4_9497() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[11]));
	ENDFOR
}

void AnonFilter_a4_9498() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[12]));
	ENDFOR
}

void AnonFilter_a4_9499() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[13]));
	ENDFOR
}

void AnonFilter_a4_9500() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[14]));
	ENDFOR
}

void AnonFilter_a4_9501() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[15]));
	ENDFOR
}

void AnonFilter_a4_9502() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[16]));
	ENDFOR
}

void AnonFilter_a4_9503() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[17]));
	ENDFOR
}

void AnonFilter_a4_9504() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[18]));
	ENDFOR
}

void AnonFilter_a4_9505() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[19]));
	ENDFOR
}

void AnonFilter_a4_9506() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[20]));
	ENDFOR
}

void AnonFilter_a4_9507() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[21]));
	ENDFOR
}

void AnonFilter_a4_9508() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[22]));
	ENDFOR
}

void AnonFilter_a4_9509() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[23]));
	ENDFOR
}

void AnonFilter_a4_9510() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[24]));
	ENDFOR
}

void AnonFilter_a4_9511() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[25]));
	ENDFOR
}

void AnonFilter_a4_9512() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[26]));
	ENDFOR
}

void AnonFilter_a4_9513() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[27]));
	ENDFOR
}

void AnonFilter_a4_9514() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[28]));
	ENDFOR
}

void AnonFilter_a4_9515() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[29]));
	ENDFOR
}

void AnonFilter_a4_9516() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[30]));
	ENDFOR
}

void AnonFilter_a4_9517() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[31]));
	ENDFOR
}

void AnonFilter_a4_9518() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[32]));
	ENDFOR
}

void AnonFilter_a4_9519() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[33]));
	ENDFOR
}

void AnonFilter_a4_9520() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[34]));
	ENDFOR
}

void AnonFilter_a4_9521() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[35]));
	ENDFOR
}

void AnonFilter_a4_9522() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[36]));
	ENDFOR
}

void AnonFilter_a4_9523() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[37]));
	ENDFOR
}

void AnonFilter_a4_9524() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[38]));
	ENDFOR
}

void AnonFilter_a4_9525() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[39]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9484() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9475WEIGHTED_ROUND_ROBIN_Splitter_9484));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9485() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_9528() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[0]), &(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9529() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[1]), &(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9530() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[2]), &(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9531() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[3]), &(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9532() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[4]), &(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9533() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[5]), &(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9534() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[6]), &(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9535() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[7]), &(SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9526() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9527() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366, pop_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_9366_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_9366_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_9366_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_9366() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_9414() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_9339DUPLICATE_Splitter_9414);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9415() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_9367_s.count = (AnonFilter_a2_9367_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_9367_s.errors = (AnonFilter_a2_9367_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_9367_s.errors / AnonFilter_a2_9367_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_9367_s.errors = (AnonFilter_a2_9367_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_9367_s.errors / AnonFilter_a2_9367_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_9367() {
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Pre_CollapsedDataParallel_1_9412WEIGHTED_ROUND_ROBIN_Splitter_9464);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_join[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_9339DUPLICATE_Splitter_9414);
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_join[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_9415AnonFilter_a2_9367);
	FOR(int, __iter_init_2_, 0, <, 40, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9465Post_CollapsedDataParallel_2_9413);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9475WEIGHTED_ROUND_ROBIN_Splitter_9484);
	FOR(int, __iter_init_3_, 0, <, 40, __iter_init_3_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_9527iDCT8x8_1D_col_fast_9366);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 40, __iter_init_6_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_9540_9547_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9539_9546_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin104_iDCT8x8_1D_row_fast_Fiss_9541_9548_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 40, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_9537_9544_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9340_9416_9536_9543_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9538_9545_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9423Pre_CollapsedDataParallel_1_9412);
	init_buffer_float(&Post_CollapsedDataParallel_2_9413WEIGHTED_ROUND_ROBIN_Splitter_9474);
// --- init: iDCT_2D_reference_coarse_9342
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_9342_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9466
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9466_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9467
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9467_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9468
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9468_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9469
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9469_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9470
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9470_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9471
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9471_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9472
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9472_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9473
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9473_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9476
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9476_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9477
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9477_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9478
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9478_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9479
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9479_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9480
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9480_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9481
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9481_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9482
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9482_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9483
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9483_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_9367
	 {
	AnonFilter_a2_9367_s.count = 0.0 ; 
	AnonFilter_a2_9367_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_9339();
		DUPLICATE_Splitter_9414();
			iDCT_2D_reference_coarse_9342();
			WEIGHTED_ROUND_ROBIN_Splitter_9422();
				AnonFilter_a3_9424();
				AnonFilter_a3_9425();
				AnonFilter_a3_9426();
				AnonFilter_a3_9427();
				AnonFilter_a3_9428();
				AnonFilter_a3_9429();
				AnonFilter_a3_9430();
				AnonFilter_a3_9431();
				AnonFilter_a3_9432();
				AnonFilter_a3_9433();
				AnonFilter_a3_9434();
				AnonFilter_a3_9435();
				AnonFilter_a3_9436();
				AnonFilter_a3_9437();
				AnonFilter_a3_9438();
				AnonFilter_a3_9439();
				AnonFilter_a3_9440();
				AnonFilter_a3_9441();
				AnonFilter_a3_9442();
				AnonFilter_a3_9443();
				AnonFilter_a3_9444();
				AnonFilter_a3_9445();
				AnonFilter_a3_9446();
				AnonFilter_a3_9447();
				AnonFilter_a3_9448();
				AnonFilter_a3_9449();
				AnonFilter_a3_9450();
				AnonFilter_a3_9451();
				AnonFilter_a3_9452();
				AnonFilter_a3_9453();
				AnonFilter_a3_9454();
				AnonFilter_a3_9455();
				AnonFilter_a3_9456();
				AnonFilter_a3_9457();
				AnonFilter_a3_9458();
				AnonFilter_a3_9459();
				AnonFilter_a3_9460();
				AnonFilter_a3_9461();
				AnonFilter_a3_9462();
				AnonFilter_a3_9463();
			WEIGHTED_ROUND_ROBIN_Joiner_9423();
			Pre_CollapsedDataParallel_1_9412();
			WEIGHTED_ROUND_ROBIN_Splitter_9464();
				iDCT_1D_reference_fine_9466();
				iDCT_1D_reference_fine_9467();
				iDCT_1D_reference_fine_9468();
				iDCT_1D_reference_fine_9469();
				iDCT_1D_reference_fine_9470();
				iDCT_1D_reference_fine_9471();
				iDCT_1D_reference_fine_9472();
				iDCT_1D_reference_fine_9473();
			WEIGHTED_ROUND_ROBIN_Joiner_9465();
			Post_CollapsedDataParallel_2_9413();
			WEIGHTED_ROUND_ROBIN_Splitter_9474();
				iDCT_1D_reference_fine_9476();
				iDCT_1D_reference_fine_9477();
				iDCT_1D_reference_fine_9478();
				iDCT_1D_reference_fine_9479();
				iDCT_1D_reference_fine_9480();
				iDCT_1D_reference_fine_9481();
				iDCT_1D_reference_fine_9482();
				iDCT_1D_reference_fine_9483();
			WEIGHTED_ROUND_ROBIN_Joiner_9475();
			WEIGHTED_ROUND_ROBIN_Splitter_9484();
				AnonFilter_a4_9486();
				AnonFilter_a4_9487();
				AnonFilter_a4_9488();
				AnonFilter_a4_9489();
				AnonFilter_a4_9490();
				AnonFilter_a4_9491();
				AnonFilter_a4_9492();
				AnonFilter_a4_9493();
				AnonFilter_a4_9494();
				AnonFilter_a4_9495();
				AnonFilter_a4_9496();
				AnonFilter_a4_9497();
				AnonFilter_a4_9498();
				AnonFilter_a4_9499();
				AnonFilter_a4_9500();
				AnonFilter_a4_9501();
				AnonFilter_a4_9502();
				AnonFilter_a4_9503();
				AnonFilter_a4_9504();
				AnonFilter_a4_9505();
				AnonFilter_a4_9506();
				AnonFilter_a4_9507();
				AnonFilter_a4_9508();
				AnonFilter_a4_9509();
				AnonFilter_a4_9510();
				AnonFilter_a4_9511();
				AnonFilter_a4_9512();
				AnonFilter_a4_9513();
				AnonFilter_a4_9514();
				AnonFilter_a4_9515();
				AnonFilter_a4_9516();
				AnonFilter_a4_9517();
				AnonFilter_a4_9518();
				AnonFilter_a4_9519();
				AnonFilter_a4_9520();
				AnonFilter_a4_9521();
				AnonFilter_a4_9522();
				AnonFilter_a4_9523();
				AnonFilter_a4_9524();
				AnonFilter_a4_9525();
			WEIGHTED_ROUND_ROBIN_Joiner_9485();
			WEIGHTED_ROUND_ROBIN_Splitter_9526();
				iDCT8x8_1D_row_fast_9528();
				iDCT8x8_1D_row_fast_9529();
				iDCT8x8_1D_row_fast_9530();
				iDCT8x8_1D_row_fast_9531();
				iDCT8x8_1D_row_fast_9532();
				iDCT8x8_1D_row_fast_9533();
				iDCT8x8_1D_row_fast_9534();
				iDCT8x8_1D_row_fast_9535();
			WEIGHTED_ROUND_ROBIN_Joiner_9527();
			iDCT8x8_1D_col_fast_9366();
		WEIGHTED_ROUND_ROBIN_Joiner_9415();
		AnonFilter_a2_9367();
	ENDFOR
	return EXIT_SUCCESS;
}
