#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=8832 on the compile command line
#else
#if BUF_SIZEMAX < 8832
#error BUF_SIZEMAX too small, it must be at least 8832
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_14272_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_14296_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_14297_t;
void AnonFilter_a0_14269();
void DUPLICATE_Splitter_14344();
void iDCT_2D_reference_coarse_14272();
void WEIGHTED_ROUND_ROBIN_Splitter_14352();
void AnonFilter_a3_14354();
void AnonFilter_a3_14355();
void AnonFilter_a3_14356();
void AnonFilter_a3_14357();
void AnonFilter_a3_14358();
void AnonFilter_a3_14359();
void AnonFilter_a3_14360();
void AnonFilter_a3_14361();
void AnonFilter_a3_14362();
void AnonFilter_a3_14363();
void AnonFilter_a3_14364();
void AnonFilter_a3_14365();
void AnonFilter_a3_14366();
void AnonFilter_a3_14367();
void AnonFilter_a3_14368();
void AnonFilter_a3_14369();
void AnonFilter_a3_14370();
void AnonFilter_a3_14371();
void AnonFilter_a3_14372();
void AnonFilter_a3_14373();
void AnonFilter_a3_14374();
void AnonFilter_a3_14375();
void AnonFilter_a3_14376();
void WEIGHTED_ROUND_ROBIN_Joiner_14353();
void Pre_CollapsedDataParallel_1_14342();
void WEIGHTED_ROUND_ROBIN_Splitter_14377();
void iDCT_1D_reference_fine_14379();
void iDCT_1D_reference_fine_14380();
void iDCT_1D_reference_fine_14381();
void iDCT_1D_reference_fine_14382();
void iDCT_1D_reference_fine_14383();
void iDCT_1D_reference_fine_14384();
void iDCT_1D_reference_fine_14385();
void iDCT_1D_reference_fine_14386();
void WEIGHTED_ROUND_ROBIN_Joiner_14378();
void Post_CollapsedDataParallel_2_14343();
void WEIGHTED_ROUND_ROBIN_Splitter_14387();
void iDCT_1D_reference_fine_14389();
void iDCT_1D_reference_fine_14390();
void iDCT_1D_reference_fine_14391();
void iDCT_1D_reference_fine_14392();
void iDCT_1D_reference_fine_14393();
void iDCT_1D_reference_fine_14394();
void iDCT_1D_reference_fine_14395();
void iDCT_1D_reference_fine_14396();
void WEIGHTED_ROUND_ROBIN_Joiner_14388();
void WEIGHTED_ROUND_ROBIN_Splitter_14397();
void AnonFilter_a4_14399();
void AnonFilter_a4_14400();
void AnonFilter_a4_14401();
void AnonFilter_a4_14402();
void AnonFilter_a4_14403();
void AnonFilter_a4_14404();
void AnonFilter_a4_14405();
void AnonFilter_a4_14406();
void AnonFilter_a4_14407();
void AnonFilter_a4_14408();
void AnonFilter_a4_14409();
void AnonFilter_a4_14410();
void AnonFilter_a4_14411();
void AnonFilter_a4_14412();
void AnonFilter_a4_14413();
void AnonFilter_a4_14414();
void AnonFilter_a4_14415();
void AnonFilter_a4_14416();
void AnonFilter_a4_14417();
void AnonFilter_a4_14418();
void AnonFilter_a4_14419();
void AnonFilter_a4_14420();
void AnonFilter_a4_14421();
void WEIGHTED_ROUND_ROBIN_Joiner_14398();
void WEIGHTED_ROUND_ROBIN_Splitter_14422();
void iDCT8x8_1D_row_fast_14424();
void iDCT8x8_1D_row_fast_14425();
void iDCT8x8_1D_row_fast_14426();
void iDCT8x8_1D_row_fast_14427();
void iDCT8x8_1D_row_fast_14428();
void iDCT8x8_1D_row_fast_14429();
void iDCT8x8_1D_row_fast_14430();
void iDCT8x8_1D_row_fast_14431();
void WEIGHTED_ROUND_ROBIN_Joiner_14423();
void iDCT8x8_1D_col_fast_14296();
void WEIGHTED_ROUND_ROBIN_Joiner_14345();
void AnonFilter_a2_14297();

#ifdef __cplusplus
}
#endif
#endif
