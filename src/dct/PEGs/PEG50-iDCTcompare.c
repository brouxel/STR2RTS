#include "PEG50-iDCTcompare.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6035Post_CollapsedDataParallel_2_5973;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6045WEIGHTED_ROUND_ROBIN_Splitter_6054;
buffer_int_t SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[50];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[50];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_split[3];
buffer_int_t AnonFilter_a0_5899DUPLICATE_Splitter_5974;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_5975AnonFilter_a2_5927;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[50];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_join[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[8];
buffer_int_t SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5983Pre_CollapsedDataParallel_1_5972;
buffer_float_t Pre_CollapsedDataParallel_1_5972WEIGHTED_ROUND_ROBIN_Splitter_6034;
buffer_float_t Post_CollapsedDataParallel_2_5973WEIGHTED_ROUND_ROBIN_Splitter_6044;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_6107iDCT8x8_1D_col_fast_5926;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[50];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[8];


iDCT_2D_reference_coarse_5902_t iDCT_2D_reference_coarse_5902_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6036_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6037_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6038_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6039_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6040_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6041_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6042_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6043_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6046_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6047_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6048_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6049_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6050_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6051_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6052_s;
iDCT_2D_reference_coarse_5902_t iDCT_1D_reference_fine_6053_s;
iDCT8x8_1D_col_fast_5926_t iDCT8x8_1D_col_fast_5926_s;
AnonFilter_a2_5927_t AnonFilter_a2_5927_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_5899() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_5899DUPLICATE_Splitter_5974));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_5902_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_5902_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_5902() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_5984() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[0]));
	ENDFOR
}

void AnonFilter_a3_5985() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[1]));
	ENDFOR
}

void AnonFilter_a3_5986() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[2]));
	ENDFOR
}

void AnonFilter_a3_5987() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[3]));
	ENDFOR
}

void AnonFilter_a3_5988() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[4]));
	ENDFOR
}

void AnonFilter_a3_5989() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[5]));
	ENDFOR
}

void AnonFilter_a3_5990() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[6]));
	ENDFOR
}

void AnonFilter_a3_5991() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[7]));
	ENDFOR
}

void AnonFilter_a3_5992() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[8]));
	ENDFOR
}

void AnonFilter_a3_5993() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[9]));
	ENDFOR
}

void AnonFilter_a3_5994() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[10]));
	ENDFOR
}

void AnonFilter_a3_5995() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[11]));
	ENDFOR
}

void AnonFilter_a3_5996() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[12]));
	ENDFOR
}

void AnonFilter_a3_5997() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[13]));
	ENDFOR
}

void AnonFilter_a3_5998() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[14]));
	ENDFOR
}

void AnonFilter_a3_5999() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[15]));
	ENDFOR
}

void AnonFilter_a3_6000() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[16]));
	ENDFOR
}

void AnonFilter_a3_6001() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[17]));
	ENDFOR
}

void AnonFilter_a3_6002() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[18]));
	ENDFOR
}

void AnonFilter_a3_6003() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[19]));
	ENDFOR
}

void AnonFilter_a3_6004() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[20]));
	ENDFOR
}

void AnonFilter_a3_6005() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[21]));
	ENDFOR
}

void AnonFilter_a3_6006() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[22]));
	ENDFOR
}

void AnonFilter_a3_6007() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[23]));
	ENDFOR
}

void AnonFilter_a3_6008() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[24]));
	ENDFOR
}

void AnonFilter_a3_6009() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[25]));
	ENDFOR
}

void AnonFilter_a3_6010() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[26]));
	ENDFOR
}

void AnonFilter_a3_6011() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[27]));
	ENDFOR
}

void AnonFilter_a3_6012() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[28]));
	ENDFOR
}

void AnonFilter_a3_6013() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[29]));
	ENDFOR
}

void AnonFilter_a3_6014() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[30]));
	ENDFOR
}

void AnonFilter_a3_6015() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[31]));
	ENDFOR
}

void AnonFilter_a3_6016() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[32]));
	ENDFOR
}

void AnonFilter_a3_6017() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[33]));
	ENDFOR
}

void AnonFilter_a3_6018() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[34]));
	ENDFOR
}

void AnonFilter_a3_6019() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[35]));
	ENDFOR
}

void AnonFilter_a3_6020() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[36]));
	ENDFOR
}

void AnonFilter_a3_6021() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[37]));
	ENDFOR
}

void AnonFilter_a3_6022() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[38]));
	ENDFOR
}

void AnonFilter_a3_6023() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[39]));
	ENDFOR
}

void AnonFilter_a3_6024() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[40]));
	ENDFOR
}

void AnonFilter_a3_6025() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[41]));
	ENDFOR
}

void AnonFilter_a3_6026() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[42]));
	ENDFOR
}

void AnonFilter_a3_6027() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[43]));
	ENDFOR
}

void AnonFilter_a3_6028() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[44]));
	ENDFOR
}

void AnonFilter_a3_6029() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[45]));
	ENDFOR
}

void AnonFilter_a3_6030() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[46]));
	ENDFOR
}

void AnonFilter_a3_6031() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[47]));
	ENDFOR
}

void AnonFilter_a3_6032() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[48]));
	ENDFOR
}

void AnonFilter_a3_6033() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[49]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5982() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 50, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5983() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 50, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5983Pre_CollapsedDataParallel_1_5972, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_5972() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_5983Pre_CollapsedDataParallel_1_5972), &(Pre_CollapsedDataParallel_1_5972WEIGHTED_ROUND_ROBIN_Splitter_6034));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6036_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_6036() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_6037() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_6038() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_6039() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_6040() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_6041() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_6042() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_6043() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6034() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5972WEIGHTED_ROUND_ROBIN_Splitter_6034));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6035() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6035Post_CollapsedDataParallel_2_5973, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_5973() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_6035Post_CollapsedDataParallel_2_5973), &(Post_CollapsedDataParallel_2_5973WEIGHTED_ROUND_ROBIN_Splitter_6044));
	ENDFOR
}

void iDCT_1D_reference_fine_6046() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_6047() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_6048() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_6049() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_6050() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_6051() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_6052() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_6053() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6044() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_5973WEIGHTED_ROUND_ROBIN_Splitter_6044));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6045() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6045WEIGHTED_ROUND_ROBIN_Splitter_6054, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_6056() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[0]));
	ENDFOR
}

void AnonFilter_a4_6057() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[1]));
	ENDFOR
}

void AnonFilter_a4_6058() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[2]));
	ENDFOR
}

void AnonFilter_a4_6059() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[3]));
	ENDFOR
}

void AnonFilter_a4_6060() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[4]));
	ENDFOR
}

void AnonFilter_a4_6061() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[5]));
	ENDFOR
}

void AnonFilter_a4_6062() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[6]));
	ENDFOR
}

void AnonFilter_a4_6063() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[7]));
	ENDFOR
}

void AnonFilter_a4_6064() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[8]));
	ENDFOR
}

void AnonFilter_a4_6065() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[9]));
	ENDFOR
}

void AnonFilter_a4_6066() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[10]));
	ENDFOR
}

void AnonFilter_a4_6067() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[11]));
	ENDFOR
}

void AnonFilter_a4_6068() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[12]));
	ENDFOR
}

void AnonFilter_a4_6069() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[13]));
	ENDFOR
}

void AnonFilter_a4_6070() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[14]));
	ENDFOR
}

void AnonFilter_a4_6071() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[15]));
	ENDFOR
}

void AnonFilter_a4_6072() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[16]));
	ENDFOR
}

void AnonFilter_a4_6073() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[17]));
	ENDFOR
}

void AnonFilter_a4_6074() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[18]));
	ENDFOR
}

void AnonFilter_a4_6075() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[19]));
	ENDFOR
}

void AnonFilter_a4_6076() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[20]));
	ENDFOR
}

void AnonFilter_a4_6077() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[21]));
	ENDFOR
}

void AnonFilter_a4_6078() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[22]));
	ENDFOR
}

void AnonFilter_a4_6079() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[23]));
	ENDFOR
}

void AnonFilter_a4_6080() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[24]));
	ENDFOR
}

void AnonFilter_a4_6081() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[25]));
	ENDFOR
}

void AnonFilter_a4_6082() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[26]));
	ENDFOR
}

void AnonFilter_a4_6083() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[27]));
	ENDFOR
}

void AnonFilter_a4_6084() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[28]));
	ENDFOR
}

void AnonFilter_a4_6085() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[29]));
	ENDFOR
}

void AnonFilter_a4_6086() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[30]));
	ENDFOR
}

void AnonFilter_a4_6087() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[31]));
	ENDFOR
}

void AnonFilter_a4_6088() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[32]));
	ENDFOR
}

void AnonFilter_a4_6089() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[33]));
	ENDFOR
}

void AnonFilter_a4_6090() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[34]));
	ENDFOR
}

void AnonFilter_a4_6091() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[35]));
	ENDFOR
}

void AnonFilter_a4_6092() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[36]));
	ENDFOR
}

void AnonFilter_a4_6093() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[37]));
	ENDFOR
}

void AnonFilter_a4_6094() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[38]));
	ENDFOR
}

void AnonFilter_a4_6095() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[39]));
	ENDFOR
}

void AnonFilter_a4_6096() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[40]));
	ENDFOR
}

void AnonFilter_a4_6097() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[41]));
	ENDFOR
}

void AnonFilter_a4_6098() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[42]));
	ENDFOR
}

void AnonFilter_a4_6099() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[43]));
	ENDFOR
}

void AnonFilter_a4_6100() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[44]));
	ENDFOR
}

void AnonFilter_a4_6101() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[45]));
	ENDFOR
}

void AnonFilter_a4_6102() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[46]));
	ENDFOR
}

void AnonFilter_a4_6103() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[47]));
	ENDFOR
}

void AnonFilter_a4_6104() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[48]));
	ENDFOR
}

void AnonFilter_a4_6105() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[49]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6054() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 50, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6045WEIGHTED_ROUND_ROBIN_Splitter_6054));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6055() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 50, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_6108() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[0]), &(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_6109() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[1]), &(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_6110() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[2]), &(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_6111() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[3]), &(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_6112() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[4]), &(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_6113() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[5]), &(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_6114() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[6]), &(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_6115() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[7]), &(SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6106() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6107() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_6107iDCT8x8_1D_col_fast_5926, pop_int(&SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_5926_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_5926_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_5926_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_5926_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_5926_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_5926_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_5926_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_5926_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_5926_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_5926_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_5926() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_6107iDCT8x8_1D_col_fast_5926), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_5974() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_5899DUPLICATE_Splitter_5974);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5975() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_5975AnonFilter_a2_5927, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_5927_s.count = (AnonFilter_a2_5927_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_5927_s.errors = (AnonFilter_a2_5927_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_5927_s.errors / AnonFilter_a2_5927_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_5927_s.errors = (AnonFilter_a2_5927_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_5927_s.errors / AnonFilter_a2_5927_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_5927() {
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_5975AnonFilter_a2_5927));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6035Post_CollapsedDataParallel_2_5973);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6045WEIGHTED_ROUND_ROBIN_Splitter_6054);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 50, __iter_init_1_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_6120_6127_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 50, __iter_init_2_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_6120_6127_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_5899DUPLICATE_Splitter_5974);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_5975AnonFilter_a2_5927);
	FOR(int, __iter_init_4_, 0, <, 50, __iter_init_4_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_6117_6124_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_5900_5976_6116_6123_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin124_iDCT8x8_1D_row_fast_Fiss_6121_6128_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5983Pre_CollapsedDataParallel_1_5972);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5972WEIGHTED_ROUND_ROBIN_Splitter_6034);
	init_buffer_float(&Post_CollapsedDataParallel_2_5973WEIGHTED_ROUND_ROBIN_Splitter_6044);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_6107iDCT8x8_1D_col_fast_5926);
	FOR(int, __iter_init_8_, 0, <, 50, __iter_init_8_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_6117_6124_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6119_6126_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6118_6125_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_5902
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_5902_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6036
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6036_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6037
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6037_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6038
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6038_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6039
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6039_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6040
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6040_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6041
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6041_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6042
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6042_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6043
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6043_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6046
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6046_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6047
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6047_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6048
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6048_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6049
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6049_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6050
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6050_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6051
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6051_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6052
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6052_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6053
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6053_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_5927
	 {
	AnonFilter_a2_5927_s.count = 0.0 ; 
	AnonFilter_a2_5927_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_5899();
		DUPLICATE_Splitter_5974();
			iDCT_2D_reference_coarse_5902();
			WEIGHTED_ROUND_ROBIN_Splitter_5982();
				AnonFilter_a3_5984();
				AnonFilter_a3_5985();
				AnonFilter_a3_5986();
				AnonFilter_a3_5987();
				AnonFilter_a3_5988();
				AnonFilter_a3_5989();
				AnonFilter_a3_5990();
				AnonFilter_a3_5991();
				AnonFilter_a3_5992();
				AnonFilter_a3_5993();
				AnonFilter_a3_5994();
				AnonFilter_a3_5995();
				AnonFilter_a3_5996();
				AnonFilter_a3_5997();
				AnonFilter_a3_5998();
				AnonFilter_a3_5999();
				AnonFilter_a3_6000();
				AnonFilter_a3_6001();
				AnonFilter_a3_6002();
				AnonFilter_a3_6003();
				AnonFilter_a3_6004();
				AnonFilter_a3_6005();
				AnonFilter_a3_6006();
				AnonFilter_a3_6007();
				AnonFilter_a3_6008();
				AnonFilter_a3_6009();
				AnonFilter_a3_6010();
				AnonFilter_a3_6011();
				AnonFilter_a3_6012();
				AnonFilter_a3_6013();
				AnonFilter_a3_6014();
				AnonFilter_a3_6015();
				AnonFilter_a3_6016();
				AnonFilter_a3_6017();
				AnonFilter_a3_6018();
				AnonFilter_a3_6019();
				AnonFilter_a3_6020();
				AnonFilter_a3_6021();
				AnonFilter_a3_6022();
				AnonFilter_a3_6023();
				AnonFilter_a3_6024();
				AnonFilter_a3_6025();
				AnonFilter_a3_6026();
				AnonFilter_a3_6027();
				AnonFilter_a3_6028();
				AnonFilter_a3_6029();
				AnonFilter_a3_6030();
				AnonFilter_a3_6031();
				AnonFilter_a3_6032();
				AnonFilter_a3_6033();
			WEIGHTED_ROUND_ROBIN_Joiner_5983();
			Pre_CollapsedDataParallel_1_5972();
			WEIGHTED_ROUND_ROBIN_Splitter_6034();
				iDCT_1D_reference_fine_6036();
				iDCT_1D_reference_fine_6037();
				iDCT_1D_reference_fine_6038();
				iDCT_1D_reference_fine_6039();
				iDCT_1D_reference_fine_6040();
				iDCT_1D_reference_fine_6041();
				iDCT_1D_reference_fine_6042();
				iDCT_1D_reference_fine_6043();
			WEIGHTED_ROUND_ROBIN_Joiner_6035();
			Post_CollapsedDataParallel_2_5973();
			WEIGHTED_ROUND_ROBIN_Splitter_6044();
				iDCT_1D_reference_fine_6046();
				iDCT_1D_reference_fine_6047();
				iDCT_1D_reference_fine_6048();
				iDCT_1D_reference_fine_6049();
				iDCT_1D_reference_fine_6050();
				iDCT_1D_reference_fine_6051();
				iDCT_1D_reference_fine_6052();
				iDCT_1D_reference_fine_6053();
			WEIGHTED_ROUND_ROBIN_Joiner_6045();
			WEIGHTED_ROUND_ROBIN_Splitter_6054();
				AnonFilter_a4_6056();
				AnonFilter_a4_6057();
				AnonFilter_a4_6058();
				AnonFilter_a4_6059();
				AnonFilter_a4_6060();
				AnonFilter_a4_6061();
				AnonFilter_a4_6062();
				AnonFilter_a4_6063();
				AnonFilter_a4_6064();
				AnonFilter_a4_6065();
				AnonFilter_a4_6066();
				AnonFilter_a4_6067();
				AnonFilter_a4_6068();
				AnonFilter_a4_6069();
				AnonFilter_a4_6070();
				AnonFilter_a4_6071();
				AnonFilter_a4_6072();
				AnonFilter_a4_6073();
				AnonFilter_a4_6074();
				AnonFilter_a4_6075();
				AnonFilter_a4_6076();
				AnonFilter_a4_6077();
				AnonFilter_a4_6078();
				AnonFilter_a4_6079();
				AnonFilter_a4_6080();
				AnonFilter_a4_6081();
				AnonFilter_a4_6082();
				AnonFilter_a4_6083();
				AnonFilter_a4_6084();
				AnonFilter_a4_6085();
				AnonFilter_a4_6086();
				AnonFilter_a4_6087();
				AnonFilter_a4_6088();
				AnonFilter_a4_6089();
				AnonFilter_a4_6090();
				AnonFilter_a4_6091();
				AnonFilter_a4_6092();
				AnonFilter_a4_6093();
				AnonFilter_a4_6094();
				AnonFilter_a4_6095();
				AnonFilter_a4_6096();
				AnonFilter_a4_6097();
				AnonFilter_a4_6098();
				AnonFilter_a4_6099();
				AnonFilter_a4_6100();
				AnonFilter_a4_6101();
				AnonFilter_a4_6102();
				AnonFilter_a4_6103();
				AnonFilter_a4_6104();
				AnonFilter_a4_6105();
			WEIGHTED_ROUND_ROBIN_Joiner_6055();
			WEIGHTED_ROUND_ROBIN_Splitter_6106();
				iDCT8x8_1D_row_fast_6108();
				iDCT8x8_1D_row_fast_6109();
				iDCT8x8_1D_row_fast_6110();
				iDCT8x8_1D_row_fast_6111();
				iDCT8x8_1D_row_fast_6112();
				iDCT8x8_1D_row_fast_6113();
				iDCT8x8_1D_row_fast_6114();
				iDCT8x8_1D_row_fast_6115();
			WEIGHTED_ROUND_ROBIN_Joiner_6107();
			iDCT8x8_1D_col_fast_5926();
		WEIGHTED_ROUND_ROBIN_Joiner_5975();
		AnonFilter_a2_5927();
	ENDFOR
	return EXIT_SUCCESS;
}
