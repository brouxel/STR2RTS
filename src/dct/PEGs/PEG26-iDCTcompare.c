#include "PEG26-iDCTcompare.h"

buffer_int_t AnonFilter_a0_13483DUPLICATE_Splitter_13558;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[26];
buffer_int_t SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_13556WEIGHTED_ROUND_ROBIN_Splitter_13594;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[26];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_13559AnonFilter_a2_13511;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13567Pre_CollapsedDataParallel_1_13556;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[8];
buffer_float_t Post_CollapsedDataParallel_2_13557WEIGHTED_ROUND_ROBIN_Splitter_13604;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[26];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_13643iDCT8x8_1D_col_fast_13510;
buffer_int_t SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[26];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13595Post_CollapsedDataParallel_2_13557;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13605WEIGHTED_ROUND_ROBIN_Splitter_13614;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[8];


iDCT_2D_reference_coarse_13486_t iDCT_2D_reference_coarse_13486_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13596_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13597_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13598_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13599_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13600_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13601_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13602_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13603_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13606_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13607_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13608_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13609_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13610_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13611_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13612_s;
iDCT_2D_reference_coarse_13486_t iDCT_1D_reference_fine_13613_s;
iDCT8x8_1D_col_fast_13510_t iDCT8x8_1D_col_fast_13510_s;
AnonFilter_a2_13511_t AnonFilter_a2_13511_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_13483() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_13483DUPLICATE_Splitter_13558));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_13486_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_13486_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_13486() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_13568() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[0]));
	ENDFOR
}

void AnonFilter_a3_13569() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[1]));
	ENDFOR
}

void AnonFilter_a3_13570() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[2]));
	ENDFOR
}

void AnonFilter_a3_13571() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[3]));
	ENDFOR
}

void AnonFilter_a3_13572() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[4]));
	ENDFOR
}

void AnonFilter_a3_13573() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[5]));
	ENDFOR
}

void AnonFilter_a3_13574() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[6]));
	ENDFOR
}

void AnonFilter_a3_13575() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[7]));
	ENDFOR
}

void AnonFilter_a3_13576() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[8]));
	ENDFOR
}

void AnonFilter_a3_13577() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[9]));
	ENDFOR
}

void AnonFilter_a3_13578() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[10]));
	ENDFOR
}

void AnonFilter_a3_13579() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[11]));
	ENDFOR
}

void AnonFilter_a3_13580() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[12]));
	ENDFOR
}

void AnonFilter_a3_13581() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[13]));
	ENDFOR
}

void AnonFilter_a3_13582() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[14]));
	ENDFOR
}

void AnonFilter_a3_13583() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[15]));
	ENDFOR
}

void AnonFilter_a3_13584() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[16]));
	ENDFOR
}

void AnonFilter_a3_13585() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[17]));
	ENDFOR
}

void AnonFilter_a3_13586() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[18]));
	ENDFOR
}

void AnonFilter_a3_13587() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[19]));
	ENDFOR
}

void AnonFilter_a3_13588() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[20]));
	ENDFOR
}

void AnonFilter_a3_13589() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[21]));
	ENDFOR
}

void AnonFilter_a3_13590() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[22]));
	ENDFOR
}

void AnonFilter_a3_13591() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[23]));
	ENDFOR
}

void AnonFilter_a3_13592() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[24]));
	ENDFOR
}

void AnonFilter_a3_13593() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[25]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13566() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13567() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13567Pre_CollapsedDataParallel_1_13556, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_13556() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_13567Pre_CollapsedDataParallel_1_13556), &(Pre_CollapsedDataParallel_1_13556WEIGHTED_ROUND_ROBIN_Splitter_13594));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13596_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_13596() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_13597() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_13598() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_13599() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_13600() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_13601() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_13602() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_13603() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13594() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_13556WEIGHTED_ROUND_ROBIN_Splitter_13594));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13595() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13595Post_CollapsedDataParallel_2_13557, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_13557() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_13595Post_CollapsedDataParallel_2_13557), &(Post_CollapsedDataParallel_2_13557WEIGHTED_ROUND_ROBIN_Splitter_13604));
	ENDFOR
}

void iDCT_1D_reference_fine_13606() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_13607() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_13608() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_13609() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_13610() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_13611() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_13612() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_13613() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13604() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_13557WEIGHTED_ROUND_ROBIN_Splitter_13604));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13605() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13605WEIGHTED_ROUND_ROBIN_Splitter_13614, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_13616() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[0]));
	ENDFOR
}

void AnonFilter_a4_13617() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[1]));
	ENDFOR
}

void AnonFilter_a4_13618() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[2]));
	ENDFOR
}

void AnonFilter_a4_13619() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[3]));
	ENDFOR
}

void AnonFilter_a4_13620() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[4]));
	ENDFOR
}

void AnonFilter_a4_13621() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[5]));
	ENDFOR
}

void AnonFilter_a4_13622() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[6]));
	ENDFOR
}

void AnonFilter_a4_13623() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[7]));
	ENDFOR
}

void AnonFilter_a4_13624() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[8]));
	ENDFOR
}

void AnonFilter_a4_13625() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[9]));
	ENDFOR
}

void AnonFilter_a4_13626() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[10]));
	ENDFOR
}

void AnonFilter_a4_13627() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[11]));
	ENDFOR
}

void AnonFilter_a4_13628() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[12]));
	ENDFOR
}

void AnonFilter_a4_13629() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[13]));
	ENDFOR
}

void AnonFilter_a4_13630() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[14]));
	ENDFOR
}

void AnonFilter_a4_13631() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[15]));
	ENDFOR
}

void AnonFilter_a4_13632() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[16]));
	ENDFOR
}

void AnonFilter_a4_13633() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[17]));
	ENDFOR
}

void AnonFilter_a4_13634() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[18]));
	ENDFOR
}

void AnonFilter_a4_13635() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[19]));
	ENDFOR
}

void AnonFilter_a4_13636() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[20]));
	ENDFOR
}

void AnonFilter_a4_13637() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[21]));
	ENDFOR
}

void AnonFilter_a4_13638() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[22]));
	ENDFOR
}

void AnonFilter_a4_13639() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[23]));
	ENDFOR
}

void AnonFilter_a4_13640() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[24]));
	ENDFOR
}

void AnonFilter_a4_13641() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[25]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13614() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_13605WEIGHTED_ROUND_ROBIN_Splitter_13614));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13615() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_13644() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[0]), &(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13645() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[1]), &(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13646() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[2]), &(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13647() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[3]), &(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13648() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[4]), &(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13649() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[5]), &(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13650() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[6]), &(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13651() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[7]), &(SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13642() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13643() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_13643iDCT8x8_1D_col_fast_13510, pop_int(&SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_13510_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_13510_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_13510_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_13510_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_13510_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_13510_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_13510_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_13510_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_13510_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_13510_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_13510() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_13643iDCT8x8_1D_col_fast_13510), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_13558() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_13483DUPLICATE_Splitter_13558);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13559() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_13559AnonFilter_a2_13511, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_13511_s.count = (AnonFilter_a2_13511_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_13511_s.errors = (AnonFilter_a2_13511_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_13511_s.errors / AnonFilter_a2_13511_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_13511_s.errors = (AnonFilter_a2_13511_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_13511_s.errors / AnonFilter_a2_13511_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_13511() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_13559AnonFilter_a2_13511));
	ENDFOR
}

void __stream_init__() {
	init_buffer_int(&AnonFilter_a0_13483DUPLICATE_Splitter_13558);
	FOR(int, __iter_init_0_, 0, <, 26, __iter_init_0_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_13656_13663_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_13556WEIGHTED_ROUND_ROBIN_Splitter_13594);
	FOR(int, __iter_init_2_, 0, <, 26, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_13656_13663_split[__iter_init_2_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_13559AnonFilter_a2_13511);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13567Pre_CollapsedDataParallel_1_13556);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_13557WEIGHTED_ROUND_ROBIN_Splitter_13604);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13655_13662_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 26, __iter_init_5_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_13653_13660_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_join[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_13643iDCT8x8_1D_col_fast_13510);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin76_iDCT8x8_1D_row_fast_Fiss_13657_13664_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 26, __iter_init_8_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_13653_13660_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13484_13560_13652_13659_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13595Post_CollapsedDataParallel_2_13557);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13605WEIGHTED_ROUND_ROBIN_Splitter_13614);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13654_13661_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_13486
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_13486_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13596
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13596_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13597
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13597_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13598
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13598_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13599
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13599_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13600
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13600_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13601
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13601_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13602
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13602_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13603
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13603_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13606
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13606_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13607
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13607_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13608
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13608_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13609
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13609_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13610
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13610_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13611
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13611_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13612
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13612_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13613
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13613_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_13511
	 {
	AnonFilter_a2_13511_s.count = 0.0 ; 
	AnonFilter_a2_13511_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_13483();
		DUPLICATE_Splitter_13558();
			iDCT_2D_reference_coarse_13486();
			WEIGHTED_ROUND_ROBIN_Splitter_13566();
				AnonFilter_a3_13568();
				AnonFilter_a3_13569();
				AnonFilter_a3_13570();
				AnonFilter_a3_13571();
				AnonFilter_a3_13572();
				AnonFilter_a3_13573();
				AnonFilter_a3_13574();
				AnonFilter_a3_13575();
				AnonFilter_a3_13576();
				AnonFilter_a3_13577();
				AnonFilter_a3_13578();
				AnonFilter_a3_13579();
				AnonFilter_a3_13580();
				AnonFilter_a3_13581();
				AnonFilter_a3_13582();
				AnonFilter_a3_13583();
				AnonFilter_a3_13584();
				AnonFilter_a3_13585();
				AnonFilter_a3_13586();
				AnonFilter_a3_13587();
				AnonFilter_a3_13588();
				AnonFilter_a3_13589();
				AnonFilter_a3_13590();
				AnonFilter_a3_13591();
				AnonFilter_a3_13592();
				AnonFilter_a3_13593();
			WEIGHTED_ROUND_ROBIN_Joiner_13567();
			Pre_CollapsedDataParallel_1_13556();
			WEIGHTED_ROUND_ROBIN_Splitter_13594();
				iDCT_1D_reference_fine_13596();
				iDCT_1D_reference_fine_13597();
				iDCT_1D_reference_fine_13598();
				iDCT_1D_reference_fine_13599();
				iDCT_1D_reference_fine_13600();
				iDCT_1D_reference_fine_13601();
				iDCT_1D_reference_fine_13602();
				iDCT_1D_reference_fine_13603();
			WEIGHTED_ROUND_ROBIN_Joiner_13595();
			Post_CollapsedDataParallel_2_13557();
			WEIGHTED_ROUND_ROBIN_Splitter_13604();
				iDCT_1D_reference_fine_13606();
				iDCT_1D_reference_fine_13607();
				iDCT_1D_reference_fine_13608();
				iDCT_1D_reference_fine_13609();
				iDCT_1D_reference_fine_13610();
				iDCT_1D_reference_fine_13611();
				iDCT_1D_reference_fine_13612();
				iDCT_1D_reference_fine_13613();
			WEIGHTED_ROUND_ROBIN_Joiner_13605();
			WEIGHTED_ROUND_ROBIN_Splitter_13614();
				AnonFilter_a4_13616();
				AnonFilter_a4_13617();
				AnonFilter_a4_13618();
				AnonFilter_a4_13619();
				AnonFilter_a4_13620();
				AnonFilter_a4_13621();
				AnonFilter_a4_13622();
				AnonFilter_a4_13623();
				AnonFilter_a4_13624();
				AnonFilter_a4_13625();
				AnonFilter_a4_13626();
				AnonFilter_a4_13627();
				AnonFilter_a4_13628();
				AnonFilter_a4_13629();
				AnonFilter_a4_13630();
				AnonFilter_a4_13631();
				AnonFilter_a4_13632();
				AnonFilter_a4_13633();
				AnonFilter_a4_13634();
				AnonFilter_a4_13635();
				AnonFilter_a4_13636();
				AnonFilter_a4_13637();
				AnonFilter_a4_13638();
				AnonFilter_a4_13639();
				AnonFilter_a4_13640();
				AnonFilter_a4_13641();
			WEIGHTED_ROUND_ROBIN_Joiner_13615();
			WEIGHTED_ROUND_ROBIN_Splitter_13642();
				iDCT8x8_1D_row_fast_13644();
				iDCT8x8_1D_row_fast_13645();
				iDCT8x8_1D_row_fast_13646();
				iDCT8x8_1D_row_fast_13647();
				iDCT8x8_1D_row_fast_13648();
				iDCT8x8_1D_row_fast_13649();
				iDCT8x8_1D_row_fast_13650();
				iDCT8x8_1D_row_fast_13651();
			WEIGHTED_ROUND_ROBIN_Joiner_13643();
			iDCT8x8_1D_col_fast_13510();
		WEIGHTED_ROUND_ROBIN_Joiner_13559();
		AnonFilter_a2_13511();
	ENDFOR
	return EXIT_SUCCESS;
}
