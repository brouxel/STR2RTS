#include "PEG14-iDCTcompare.h"

buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[14];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_split[3];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_join[3];
buffer_float_t Pre_CollapsedDataParallel_1_16484WEIGHTED_ROUND_ROBIN_Splitter_16510;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[14];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[8];
buffer_float_t Post_CollapsedDataParallel_2_16485WEIGHTED_ROUND_ROBIN_Splitter_16520;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[14];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[14];
buffer_int_t AnonFilter_a0_16411DUPLICATE_Splitter_16486;
buffer_int_t SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16511Post_CollapsedDataParallel_2_16485;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16487AnonFilter_a2_16439;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_16547iDCT8x8_1D_col_fast_16438;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16521WEIGHTED_ROUND_ROBIN_Splitter_16530;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_16495Pre_CollapsedDataParallel_1_16484;
buffer_int_t SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[8];


iDCT_2D_reference_coarse_16414_t iDCT_2D_reference_coarse_16414_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16512_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16513_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16514_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16515_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16516_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16517_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16518_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16519_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16522_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16523_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16524_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16525_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16526_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16527_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16528_s;
iDCT_2D_reference_coarse_16414_t iDCT_1D_reference_fine_16529_s;
iDCT8x8_1D_col_fast_16438_t iDCT8x8_1D_col_fast_16438_s;
AnonFilter_a2_16439_t AnonFilter_a2_16439_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_16411() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_16411DUPLICATE_Splitter_16486));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_16414_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_16414_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_16414() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_16496() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[0]));
	ENDFOR
}

void AnonFilter_a3_16497() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[1]));
	ENDFOR
}

void AnonFilter_a3_16498() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[2]));
	ENDFOR
}

void AnonFilter_a3_16499() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[3]));
	ENDFOR
}

void AnonFilter_a3_16500() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[4]));
	ENDFOR
}

void AnonFilter_a3_16501() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[5]));
	ENDFOR
}

void AnonFilter_a3_16502() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[6]));
	ENDFOR
}

void AnonFilter_a3_16503() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[7]));
	ENDFOR
}

void AnonFilter_a3_16504() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[8]));
	ENDFOR
}

void AnonFilter_a3_16505() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[9]));
	ENDFOR
}

void AnonFilter_a3_16506() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[10]));
	ENDFOR
}

void AnonFilter_a3_16507() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[11]));
	ENDFOR
}

void AnonFilter_a3_16508() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[12]));
	ENDFOR
}

void AnonFilter_a3_16509() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16494() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16495() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16495Pre_CollapsedDataParallel_1_16484, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_16484() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_16495Pre_CollapsedDataParallel_1_16484), &(Pre_CollapsedDataParallel_1_16484WEIGHTED_ROUND_ROBIN_Splitter_16510));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_16512_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_16512() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_16513() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_16514() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_16515() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_16516() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_16517() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_16518() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_16519() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16510() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_16484WEIGHTED_ROUND_ROBIN_Splitter_16510));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16511() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16511Post_CollapsedDataParallel_2_16485, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_16485() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_16511Post_CollapsedDataParallel_2_16485), &(Post_CollapsedDataParallel_2_16485WEIGHTED_ROUND_ROBIN_Splitter_16520));
	ENDFOR
}

void iDCT_1D_reference_fine_16522() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_16523() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_16524() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_16525() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_16526() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_16527() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_16528() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_16529() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16520() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_16485WEIGHTED_ROUND_ROBIN_Splitter_16520));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16521() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_16521WEIGHTED_ROUND_ROBIN_Splitter_16530, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_16532() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[0]));
	ENDFOR
}

void AnonFilter_a4_16533() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[1]));
	ENDFOR
}

void AnonFilter_a4_16534() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[2]));
	ENDFOR
}

void AnonFilter_a4_16535() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[3]));
	ENDFOR
}

void AnonFilter_a4_16536() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[4]));
	ENDFOR
}

void AnonFilter_a4_16537() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[5]));
	ENDFOR
}

void AnonFilter_a4_16538() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[6]));
	ENDFOR
}

void AnonFilter_a4_16539() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[7]));
	ENDFOR
}

void AnonFilter_a4_16540() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[8]));
	ENDFOR
}

void AnonFilter_a4_16541() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[9]));
	ENDFOR
}

void AnonFilter_a4_16542() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[10]));
	ENDFOR
}

void AnonFilter_a4_16543() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[11]));
	ENDFOR
}

void AnonFilter_a4_16544() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[12]));
	ENDFOR
}

void AnonFilter_a4_16545() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16530() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_16521WEIGHTED_ROUND_ROBIN_Splitter_16530));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16531() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_16548() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[0]), &(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_16549() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[1]), &(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_16550() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[2]), &(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_16551() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[3]), &(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_16552() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[4]), &(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_16553() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[5]), &(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_16554() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[6]), &(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_16555() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[7]), &(SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_16546() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16547() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16547iDCT8x8_1D_col_fast_16438, pop_int(&SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_16438_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_16438_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_16438_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_16438_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_16438_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_16438_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_16438_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_16438_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_16438_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_16438_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_16438() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_16547iDCT8x8_1D_col_fast_16438), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_16486() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_16411DUPLICATE_Splitter_16486);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_16487() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_16487AnonFilter_a2_16439, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_16439_s.count = (AnonFilter_a2_16439_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_16439_s.errors = (AnonFilter_a2_16439_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_16439_s.errors / AnonFilter_a2_16439_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_16439_s.errors = (AnonFilter_a2_16439_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_16439_s.errors / AnonFilter_a2_16439_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_16439() {
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_16487AnonFilter_a2_16439));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 14, __iter_init_0_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_16560_16567_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_16412_16488_16556_16563_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_16484WEIGHTED_ROUND_ROBIN_Splitter_16510);
	FOR(int, __iter_init_4_, 0, <, 14, __iter_init_4_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_16557_16564_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_16485WEIGHTED_ROUND_ROBIN_Splitter_16520);
	FOR(int, __iter_init_6_, 0, <, 14, __iter_init_6_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_16557_16564_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 14, __iter_init_7_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_16560_16567_split[__iter_init_7_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_16411DUPLICATE_Splitter_16486);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16511Post_CollapsedDataParallel_2_16485);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16487AnonFilter_a2_16439);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_16547iDCT8x8_1D_col_fast_16438);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16521WEIGHTED_ROUND_ROBIN_Splitter_16530);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_16495Pre_CollapsedDataParallel_1_16484);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin52_iDCT8x8_1D_row_fast_Fiss_16561_16568_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_16558_16565_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_16559_16566_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_16414
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_16414_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16512
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16512_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16513
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16513_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16514
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16514_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16515
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16515_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16516
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16516_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16517
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16517_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16518
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16518_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16519
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16519_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16522
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16522_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16523
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16523_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16524
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16524_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16525
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16525_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16526
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16526_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16527
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16527_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16528
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16528_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_16529
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_16529_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_16439
	 {
	AnonFilter_a2_16439_s.count = 0.0 ; 
	AnonFilter_a2_16439_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_16411();
		DUPLICATE_Splitter_16486();
			iDCT_2D_reference_coarse_16414();
			WEIGHTED_ROUND_ROBIN_Splitter_16494();
				AnonFilter_a3_16496();
				AnonFilter_a3_16497();
				AnonFilter_a3_16498();
				AnonFilter_a3_16499();
				AnonFilter_a3_16500();
				AnonFilter_a3_16501();
				AnonFilter_a3_16502();
				AnonFilter_a3_16503();
				AnonFilter_a3_16504();
				AnonFilter_a3_16505();
				AnonFilter_a3_16506();
				AnonFilter_a3_16507();
				AnonFilter_a3_16508();
				AnonFilter_a3_16509();
			WEIGHTED_ROUND_ROBIN_Joiner_16495();
			Pre_CollapsedDataParallel_1_16484();
			WEIGHTED_ROUND_ROBIN_Splitter_16510();
				iDCT_1D_reference_fine_16512();
				iDCT_1D_reference_fine_16513();
				iDCT_1D_reference_fine_16514();
				iDCT_1D_reference_fine_16515();
				iDCT_1D_reference_fine_16516();
				iDCT_1D_reference_fine_16517();
				iDCT_1D_reference_fine_16518();
				iDCT_1D_reference_fine_16519();
			WEIGHTED_ROUND_ROBIN_Joiner_16511();
			Post_CollapsedDataParallel_2_16485();
			WEIGHTED_ROUND_ROBIN_Splitter_16520();
				iDCT_1D_reference_fine_16522();
				iDCT_1D_reference_fine_16523();
				iDCT_1D_reference_fine_16524();
				iDCT_1D_reference_fine_16525();
				iDCT_1D_reference_fine_16526();
				iDCT_1D_reference_fine_16527();
				iDCT_1D_reference_fine_16528();
				iDCT_1D_reference_fine_16529();
			WEIGHTED_ROUND_ROBIN_Joiner_16521();
			WEIGHTED_ROUND_ROBIN_Splitter_16530();
				AnonFilter_a4_16532();
				AnonFilter_a4_16533();
				AnonFilter_a4_16534();
				AnonFilter_a4_16535();
				AnonFilter_a4_16536();
				AnonFilter_a4_16537();
				AnonFilter_a4_16538();
				AnonFilter_a4_16539();
				AnonFilter_a4_16540();
				AnonFilter_a4_16541();
				AnonFilter_a4_16542();
				AnonFilter_a4_16543();
				AnonFilter_a4_16544();
				AnonFilter_a4_16545();
			WEIGHTED_ROUND_ROBIN_Joiner_16531();
			WEIGHTED_ROUND_ROBIN_Splitter_16546();
				iDCT8x8_1D_row_fast_16548();
				iDCT8x8_1D_row_fast_16549();
				iDCT8x8_1D_row_fast_16550();
				iDCT8x8_1D_row_fast_16551();
				iDCT8x8_1D_row_fast_16552();
				iDCT8x8_1D_row_fast_16553();
				iDCT8x8_1D_row_fast_16554();
				iDCT8x8_1D_row_fast_16555();
			WEIGHTED_ROUND_ROBIN_Joiner_16547();
			iDCT8x8_1D_col_fast_16438();
		WEIGHTED_ROUND_ROBIN_Joiner_16487();
		AnonFilter_a2_16439();
	ENDFOR
	return EXIT_SUCCESS;
}
