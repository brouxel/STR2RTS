#include "PEG56-iDCTcompare_nocache.h"

buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_3719AnonFilter_a2_3671;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[56];
buffer_int_t AnonFilter_a0_3643DUPLICATE_Splitter_3718;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[56];
buffer_int_t SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[8];
buffer_float_t Post_CollapsedDataParallel_2_3717WEIGHTED_ROUND_ROBIN_Splitter_3794;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3727Pre_CollapsedDataParallel_1_3716;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_split[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[56];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3795WEIGHTED_ROUND_ROBIN_Splitter_3804;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[56];
buffer_float_t Pre_CollapsedDataParallel_1_3716WEIGHTED_ROUND_ROBIN_Splitter_3784;
buffer_int_t SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3785Post_CollapsedDataParallel_2_3717;


iDCT_2D_reference_coarse_3646_t iDCT_2D_reference_coarse_3646_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3786_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3787_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3788_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3789_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3790_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3791_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3792_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3793_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3796_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3797_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3798_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3799_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3800_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3801_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3802_s;
iDCT_2D_reference_coarse_3646_t iDCT_1D_reference_fine_3803_s;
iDCT8x8_1D_col_fast_3670_t iDCT8x8_1D_col_fast_3670_s;
AnonFilter_a2_3671_t AnonFilter_a2_3671_s;

void AnonFilter_a0_3643(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_3643DUPLICATE_Splitter_3718, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_3646(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_3646_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_3646_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_3728(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3729(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3730(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3731(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3732(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3733(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3734(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3735(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3736(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3737(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3738(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3739(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3740(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3741(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3742(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3743(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3744(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3745(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3746(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3747(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3748(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3749(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3750(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3751(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3752(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3753(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3754(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3755(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3756(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3757(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3758(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3759(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3760(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3761(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3762(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3763(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3764(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3765(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3766(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3767(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3768(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3769(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3770(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3771(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3772(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3773(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[45])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3774(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[46], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[46])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3775(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[47], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[47])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3776(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[48], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[48])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3777(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[49], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[49])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3778(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[50], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[50])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3779(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[51], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[51])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3780(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[52], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[52])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3781(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[53], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[53])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3782(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[54], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[54])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_3783(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[55], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[55])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3726() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 56, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3727() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 56, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3727Pre_CollapsedDataParallel_1_3716, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_3716(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_3716WEIGHTED_ROUND_ROBIN_Splitter_3784, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_3727Pre_CollapsedDataParallel_1_3716, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3727Pre_CollapsedDataParallel_1_3716) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3786(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3786_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3787(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3787_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3788(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3788_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3789(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3789_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3790(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3790_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3791(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3791_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3792(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3792_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3793(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3793_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3784() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3716WEIGHTED_ROUND_ROBIN_Splitter_3784));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3785() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3785Post_CollapsedDataParallel_2_3717, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3717(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_3717WEIGHTED_ROUND_ROBIN_Splitter_3794, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_3785Post_CollapsedDataParallel_2_3717, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3785Post_CollapsedDataParallel_2_3717) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3796(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3796_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3797(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3797_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3798(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3798_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3799(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3799_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3800(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3800_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3801(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3801_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3802(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3802_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_3803(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3803_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3794() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_3717WEIGHTED_ROUND_ROBIN_Splitter_3794));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3795() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3795WEIGHTED_ROUND_ROBIN_Splitter_3804, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_3806(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3807(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3808(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3809(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3810(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3811(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3812(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3813(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3814(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3815(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3816(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3817(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3818(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3819(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3820(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3821(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3822(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3823(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3824(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3825(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3826(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3827(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3828(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3829(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3830(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3831(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3832(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3833(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3834(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3835(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3836(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3837(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3838(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3839(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3840(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3841(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3842(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3843(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3844(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3845(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3846(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3847(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3848(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3849(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3850(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3851(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3852(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[46], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[46]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3853(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[47], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[47]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3854(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[48], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[48]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3855(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[49], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[49]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3856(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[50], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[50]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3857(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[51], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[51]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3858(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[52], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[52]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3859(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[53], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[53]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3860(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[54], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[54]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_3861(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[55], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[55]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3804() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 56, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3795WEIGHTED_ROUND_ROBIN_Splitter_3804));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3805() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 56, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_3864(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0], 6) ; 
		x3 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0], 2) ; 
		x4 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0], 1) ; 
		x5 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0], 7) ; 
		x6 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0], 5) ; 
		x7 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_3865(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1], 6) ; 
		x3 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1], 2) ; 
		x4 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1], 1) ; 
		x5 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1], 7) ; 
		x6 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1], 5) ; 
		x7 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_3866(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2], 6) ; 
		x3 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2], 2) ; 
		x4 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2], 1) ; 
		x5 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2], 7) ; 
		x6 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2], 5) ; 
		x7 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_3867(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3], 6) ; 
		x3 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3], 2) ; 
		x4 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3], 1) ; 
		x5 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3], 7) ; 
		x6 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3], 5) ; 
		x7 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_3868(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4], 6) ; 
		x3 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4], 2) ; 
		x4 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4], 1) ; 
		x5 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4], 7) ; 
		x6 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4], 5) ; 
		x7 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_3869(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5], 6) ; 
		x3 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5], 2) ; 
		x4 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5], 1) ; 
		x5 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5], 7) ; 
		x6 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5], 5) ; 
		x7 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_3870(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6], 6) ; 
		x3 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6], 2) ; 
		x4 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6], 1) ; 
		x5 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6], 7) ; 
		x6 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6], 5) ; 
		x7 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_3871(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7], 6) ; 
		x3 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7], 2) ; 
		x4 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7], 1) ; 
		x5 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7], 7) ; 
		x6 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7], 5) ; 
		x7 = peek_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3862() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3863() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, pop_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_3670(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_3670_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_3670_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_3670_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_3670_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_3670_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_3670_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_3670_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_3670_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_3670_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_join[2], iDCT8x8_1D_col_fast_3670_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_3718() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_3643DUPLICATE_Splitter_3718);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3719() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_3719AnonFilter_a2_3671, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_3671(){
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_3719AnonFilter_a2_3671) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_3719AnonFilter_a2_3671) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_3719AnonFilter_a2_3671) ; 
		AnonFilter_a2_3671_s.count = (AnonFilter_a2_3671_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_3671_s.errors = (AnonFilter_a2_3671_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_3671_s.errors / AnonFilter_a2_3671_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_3671_s.errors = (AnonFilter_a2_3671_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_3671_s.errors / AnonFilter_a2_3671_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_3719AnonFilter_a2_3671);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 56, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_join[__iter_init_2_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_3643DUPLICATE_Splitter_3718);
	FOR(int, __iter_init_3_, 0, <, 56, __iter_init_3_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_3717WEIGHTED_ROUND_ROBIN_Splitter_3794);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3727Pre_CollapsedDataParallel_1_3716);
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3875_3882_join[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_3863iDCT8x8_1D_col_fast_3670);
	FOR(int, __iter_init_7_, 0, <, 56, __iter_init_7_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_3873_3880_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3644_3720_3872_3879_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3874_3881_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3795WEIGHTED_ROUND_ROBIN_Splitter_3804);
	FOR(int, __iter_init_10_, 0, <, 56, __iter_init_10_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_3876_3883_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3716WEIGHTED_ROUND_ROBIN_Splitter_3784);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin136_iDCT8x8_1D_row_fast_Fiss_3877_3884_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3785Post_CollapsedDataParallel_2_3717);
// --- init: iDCT_2D_reference_coarse_3646
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_3646_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3786
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3786_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3787
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3787_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3788
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3788_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3789
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3789_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3790
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3790_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3791
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3791_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3792
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3792_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3793
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3793_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3796
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3796_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3797
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3797_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3798
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3798_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3799
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3799_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3800
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3800_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3801
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3801_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3802
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3802_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3803
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3803_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_3671
	 {
	AnonFilter_a2_3671_s.count = 0.0 ; 
	AnonFilter_a2_3671_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_3643();
		DUPLICATE_Splitter_3718();
			iDCT_2D_reference_coarse_3646();
			WEIGHTED_ROUND_ROBIN_Splitter_3726();
				AnonFilter_a3_3728();
				AnonFilter_a3_3729();
				AnonFilter_a3_3730();
				AnonFilter_a3_3731();
				AnonFilter_a3_3732();
				AnonFilter_a3_3733();
				AnonFilter_a3_3734();
				AnonFilter_a3_3735();
				AnonFilter_a3_3736();
				AnonFilter_a3_3737();
				AnonFilter_a3_3738();
				AnonFilter_a3_3739();
				AnonFilter_a3_3740();
				AnonFilter_a3_3741();
				AnonFilter_a3_3742();
				AnonFilter_a3_3743();
				AnonFilter_a3_3744();
				AnonFilter_a3_3745();
				AnonFilter_a3_3746();
				AnonFilter_a3_3747();
				AnonFilter_a3_3748();
				AnonFilter_a3_3749();
				AnonFilter_a3_3750();
				AnonFilter_a3_3751();
				AnonFilter_a3_3752();
				AnonFilter_a3_3753();
				AnonFilter_a3_3754();
				AnonFilter_a3_3755();
				AnonFilter_a3_3756();
				AnonFilter_a3_3757();
				AnonFilter_a3_3758();
				AnonFilter_a3_3759();
				AnonFilter_a3_3760();
				AnonFilter_a3_3761();
				AnonFilter_a3_3762();
				AnonFilter_a3_3763();
				AnonFilter_a3_3764();
				AnonFilter_a3_3765();
				AnonFilter_a3_3766();
				AnonFilter_a3_3767();
				AnonFilter_a3_3768();
				AnonFilter_a3_3769();
				AnonFilter_a3_3770();
				AnonFilter_a3_3771();
				AnonFilter_a3_3772();
				AnonFilter_a3_3773();
				AnonFilter_a3_3774();
				AnonFilter_a3_3775();
				AnonFilter_a3_3776();
				AnonFilter_a3_3777();
				AnonFilter_a3_3778();
				AnonFilter_a3_3779();
				AnonFilter_a3_3780();
				AnonFilter_a3_3781();
				AnonFilter_a3_3782();
				AnonFilter_a3_3783();
			WEIGHTED_ROUND_ROBIN_Joiner_3727();
			Pre_CollapsedDataParallel_1_3716();
			WEIGHTED_ROUND_ROBIN_Splitter_3784();
				iDCT_1D_reference_fine_3786();
				iDCT_1D_reference_fine_3787();
				iDCT_1D_reference_fine_3788();
				iDCT_1D_reference_fine_3789();
				iDCT_1D_reference_fine_3790();
				iDCT_1D_reference_fine_3791();
				iDCT_1D_reference_fine_3792();
				iDCT_1D_reference_fine_3793();
			WEIGHTED_ROUND_ROBIN_Joiner_3785();
			Post_CollapsedDataParallel_2_3717();
			WEIGHTED_ROUND_ROBIN_Splitter_3794();
				iDCT_1D_reference_fine_3796();
				iDCT_1D_reference_fine_3797();
				iDCT_1D_reference_fine_3798();
				iDCT_1D_reference_fine_3799();
				iDCT_1D_reference_fine_3800();
				iDCT_1D_reference_fine_3801();
				iDCT_1D_reference_fine_3802();
				iDCT_1D_reference_fine_3803();
			WEIGHTED_ROUND_ROBIN_Joiner_3795();
			WEIGHTED_ROUND_ROBIN_Splitter_3804();
				AnonFilter_a4_3806();
				AnonFilter_a4_3807();
				AnonFilter_a4_3808();
				AnonFilter_a4_3809();
				AnonFilter_a4_3810();
				AnonFilter_a4_3811();
				AnonFilter_a4_3812();
				AnonFilter_a4_3813();
				AnonFilter_a4_3814();
				AnonFilter_a4_3815();
				AnonFilter_a4_3816();
				AnonFilter_a4_3817();
				AnonFilter_a4_3818();
				AnonFilter_a4_3819();
				AnonFilter_a4_3820();
				AnonFilter_a4_3821();
				AnonFilter_a4_3822();
				AnonFilter_a4_3823();
				AnonFilter_a4_3824();
				AnonFilter_a4_3825();
				AnonFilter_a4_3826();
				AnonFilter_a4_3827();
				AnonFilter_a4_3828();
				AnonFilter_a4_3829();
				AnonFilter_a4_3830();
				AnonFilter_a4_3831();
				AnonFilter_a4_3832();
				AnonFilter_a4_3833();
				AnonFilter_a4_3834();
				AnonFilter_a4_3835();
				AnonFilter_a4_3836();
				AnonFilter_a4_3837();
				AnonFilter_a4_3838();
				AnonFilter_a4_3839();
				AnonFilter_a4_3840();
				AnonFilter_a4_3841();
				AnonFilter_a4_3842();
				AnonFilter_a4_3843();
				AnonFilter_a4_3844();
				AnonFilter_a4_3845();
				AnonFilter_a4_3846();
				AnonFilter_a4_3847();
				AnonFilter_a4_3848();
				AnonFilter_a4_3849();
				AnonFilter_a4_3850();
				AnonFilter_a4_3851();
				AnonFilter_a4_3852();
				AnonFilter_a4_3853();
				AnonFilter_a4_3854();
				AnonFilter_a4_3855();
				AnonFilter_a4_3856();
				AnonFilter_a4_3857();
				AnonFilter_a4_3858();
				AnonFilter_a4_3859();
				AnonFilter_a4_3860();
				AnonFilter_a4_3861();
			WEIGHTED_ROUND_ROBIN_Joiner_3805();
			WEIGHTED_ROUND_ROBIN_Splitter_3862();
				iDCT8x8_1D_row_fast_3864();
				iDCT8x8_1D_row_fast_3865();
				iDCT8x8_1D_row_fast_3866();
				iDCT8x8_1D_row_fast_3867();
				iDCT8x8_1D_row_fast_3868();
				iDCT8x8_1D_row_fast_3869();
				iDCT8x8_1D_row_fast_3870();
				iDCT8x8_1D_row_fast_3871();
			WEIGHTED_ROUND_ROBIN_Joiner_3863();
			iDCT8x8_1D_col_fast_3670();
		WEIGHTED_ROUND_ROBIN_Joiner_3719();
		AnonFilter_a2_3671();
	ENDFOR
	return EXIT_SUCCESS;
}
