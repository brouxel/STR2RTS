#include "PEG3-iDCTcompare.h"

buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_18605AnonFilter_a2_18557;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18618Post_CollapsedDataParallel_2_18603;
buffer_int_t SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_join[3];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_18638_18645_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_join[3];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_18641_18648_split[3];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_18641_18648_join[3];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_18633iDCT8x8_1D_col_fast_18556;
buffer_float_t Pre_CollapsedDataParallel_1_18602WEIGHTED_ROUND_ROBIN_Splitter_18617;
buffer_int_t AnonFilter_a0_18529DUPLICATE_Splitter_18604;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18613Pre_CollapsedDataParallel_1_18602;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_join[3];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_18638_18645_split[3];
buffer_int_t SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_split[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18623WEIGHTED_ROUND_ROBIN_Splitter_18627;
buffer_float_t Post_CollapsedDataParallel_2_18603WEIGHTED_ROUND_ROBIN_Splitter_18622;


iDCT_2D_reference_coarse_18532_t iDCT_2D_reference_coarse_18532_s;
iDCT_2D_reference_coarse_18532_t iDCT_1D_reference_fine_18619_s;
iDCT_2D_reference_coarse_18532_t iDCT_1D_reference_fine_18620_s;
iDCT_2D_reference_coarse_18532_t iDCT_1D_reference_fine_18621_s;
iDCT_2D_reference_coarse_18532_t iDCT_1D_reference_fine_18624_s;
iDCT_2D_reference_coarse_18532_t iDCT_1D_reference_fine_18625_s;
iDCT_2D_reference_coarse_18532_t iDCT_1D_reference_fine_18626_s;
iDCT8x8_1D_col_fast_18556_t iDCT8x8_1D_col_fast_18556_s;
AnonFilter_a2_18557_t AnonFilter_a2_18557_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_18529() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_18529DUPLICATE_Splitter_18604));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_18532_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_18532_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_18532() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_18614() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_18638_18645_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_18638_18645_join[0]));
	ENDFOR
}

void AnonFilter_a3_18615() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_18638_18645_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_18638_18645_join[1]));
	ENDFOR
}

void AnonFilter_a3_18616() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_18638_18645_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_18638_18645_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18612() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_18638_18645_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18613() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18613Pre_CollapsedDataParallel_1_18602, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_18638_18645_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_18602() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_18613Pre_CollapsedDataParallel_1_18602), &(Pre_CollapsedDataParallel_1_18602WEIGHTED_ROUND_ROBIN_Splitter_18617));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18619_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_18619() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_18620() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_18621() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18617() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_18602WEIGHTED_ROUND_ROBIN_Splitter_18617));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18618() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18618Post_CollapsedDataParallel_2_18603, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_18603() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_18618Post_CollapsedDataParallel_2_18603), &(Post_CollapsedDataParallel_2_18603WEIGHTED_ROUND_ROBIN_Splitter_18622));
	ENDFOR
}

void iDCT_1D_reference_fine_18624() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_18625() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_18626() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18622() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_18603WEIGHTED_ROUND_ROBIN_Splitter_18622));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18623() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18623WEIGHTED_ROUND_ROBIN_Splitter_18627, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_18629() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_18641_18648_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_18641_18648_join[0]));
	ENDFOR
}

void AnonFilter_a4_18630() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_18641_18648_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_18641_18648_join[1]));
	ENDFOR
}

void AnonFilter_a4_18631() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_18641_18648_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_18641_18648_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18627() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_18641_18648_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_18623WEIGHTED_ROUND_ROBIN_Splitter_18627));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18628() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_18641_18648_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_18634() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_split[0]), &(SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_18635() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_split[1]), &(SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_18636() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_split[2]), &(SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18632() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18633() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_18633iDCT8x8_1D_col_fast_18556, pop_int(&SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_18556_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_18556_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_18556_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_18556_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_18556_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_18556_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_18556_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_18556_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_18556_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_18556_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_18556() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_18633iDCT8x8_1D_col_fast_18556), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_18604() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_18529DUPLICATE_Splitter_18604);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18605() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_18605AnonFilter_a2_18557, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_18557_s.count = (AnonFilter_a2_18557_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_18557_s.errors = (AnonFilter_a2_18557_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_18557_s.errors / AnonFilter_a2_18557_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_18557_s.errors = (AnonFilter_a2_18557_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_18557_s.errors / AnonFilter_a2_18557_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_18557() {
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_18605AnonFilter_a2_18557));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_join[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_18605AnonFilter_a2_18557);
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18618Post_CollapsedDataParallel_2_18603);
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_18638_18645_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 3, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18639_18646_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_18641_18648_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_18641_18648_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_split[__iter_init_7_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_18633iDCT8x8_1D_col_fast_18556);
	init_buffer_float(&Pre_CollapsedDataParallel_1_18602WEIGHTED_ROUND_ROBIN_Splitter_18617);
	init_buffer_int(&AnonFilter_a0_18529DUPLICATE_Splitter_18604);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18613Pre_CollapsedDataParallel_1_18602);
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18530_18606_18637_18644_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_18638_18645_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin20_iDCT8x8_1D_row_fast_Fiss_18642_18649_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18640_18647_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18623WEIGHTED_ROUND_ROBIN_Splitter_18627);
	init_buffer_float(&Post_CollapsedDataParallel_2_18603WEIGHTED_ROUND_ROBIN_Splitter_18622);
// --- init: iDCT_2D_reference_coarse_18532
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_18532_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18619
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18619_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18620
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18620_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18621
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18621_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18624
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18624_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18625
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18625_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18626
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18626_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_18557
	 {
	AnonFilter_a2_18557_s.count = 0.0 ; 
	AnonFilter_a2_18557_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_18529();
		DUPLICATE_Splitter_18604();
			iDCT_2D_reference_coarse_18532();
			WEIGHTED_ROUND_ROBIN_Splitter_18612();
				AnonFilter_a3_18614();
				AnonFilter_a3_18615();
				AnonFilter_a3_18616();
			WEIGHTED_ROUND_ROBIN_Joiner_18613();
			Pre_CollapsedDataParallel_1_18602();
			WEIGHTED_ROUND_ROBIN_Splitter_18617();
				iDCT_1D_reference_fine_18619();
				iDCT_1D_reference_fine_18620();
				iDCT_1D_reference_fine_18621();
			WEIGHTED_ROUND_ROBIN_Joiner_18618();
			Post_CollapsedDataParallel_2_18603();
			WEIGHTED_ROUND_ROBIN_Splitter_18622();
				iDCT_1D_reference_fine_18624();
				iDCT_1D_reference_fine_18625();
				iDCT_1D_reference_fine_18626();
			WEIGHTED_ROUND_ROBIN_Joiner_18623();
			WEIGHTED_ROUND_ROBIN_Splitter_18627();
				AnonFilter_a4_18629();
				AnonFilter_a4_18630();
				AnonFilter_a4_18631();
			WEIGHTED_ROUND_ROBIN_Joiner_18628();
			WEIGHTED_ROUND_ROBIN_Splitter_18632();
				iDCT8x8_1D_row_fast_18634();
				iDCT8x8_1D_row_fast_18635();
				iDCT8x8_1D_row_fast_18636();
			WEIGHTED_ROUND_ROBIN_Joiner_18633();
			iDCT8x8_1D_col_fast_18556();
		WEIGHTED_ROUND_ROBIN_Joiner_18605();
		AnonFilter_a2_18557();
	ENDFOR
	return EXIT_SUCCESS;
}
