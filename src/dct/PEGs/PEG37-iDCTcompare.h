#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=14208 on the compile command line
#else
#if BUF_SIZEMAX < 14208
#error BUF_SIZEMAX too small, it must be at least 14208
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_10296_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_10320_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_10321_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_10293();
void DUPLICATE_Splitter_10368();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_10296();
void WEIGHTED_ROUND_ROBIN_Splitter_10376();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_10378();
void AnonFilter_a3_10379();
void AnonFilter_a3_10380();
void AnonFilter_a3_10381();
void AnonFilter_a3_10382();
void AnonFilter_a3_10383();
void AnonFilter_a3_10384();
void AnonFilter_a3_10385();
void AnonFilter_a3_10386();
void AnonFilter_a3_10387();
void AnonFilter_a3_10388();
void AnonFilter_a3_10389();
void AnonFilter_a3_10390();
void AnonFilter_a3_10391();
void AnonFilter_a3_10392();
void AnonFilter_a3_10393();
void AnonFilter_a3_10394();
void AnonFilter_a3_10395();
void AnonFilter_a3_10396();
void AnonFilter_a3_10397();
void AnonFilter_a3_10398();
void AnonFilter_a3_10399();
void AnonFilter_a3_10400();
void AnonFilter_a3_10401();
void AnonFilter_a3_10402();
void AnonFilter_a3_10403();
void AnonFilter_a3_10404();
void AnonFilter_a3_10405();
void AnonFilter_a3_10406();
void AnonFilter_a3_10407();
void AnonFilter_a3_10408();
void AnonFilter_a3_10409();
void AnonFilter_a3_10410();
void AnonFilter_a3_10411();
void AnonFilter_a3_10412();
void AnonFilter_a3_10413();
void AnonFilter_a3_10414();
void WEIGHTED_ROUND_ROBIN_Joiner_10377();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_10366();
void WEIGHTED_ROUND_ROBIN_Splitter_10415();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_10417();
void iDCT_1D_reference_fine_10418();
void iDCT_1D_reference_fine_10419();
void iDCT_1D_reference_fine_10420();
void iDCT_1D_reference_fine_10421();
void iDCT_1D_reference_fine_10422();
void iDCT_1D_reference_fine_10423();
void iDCT_1D_reference_fine_10424();
void WEIGHTED_ROUND_ROBIN_Joiner_10416();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_10367();
void WEIGHTED_ROUND_ROBIN_Splitter_10425();
void iDCT_1D_reference_fine_10427();
void iDCT_1D_reference_fine_10428();
void iDCT_1D_reference_fine_10429();
void iDCT_1D_reference_fine_10430();
void iDCT_1D_reference_fine_10431();
void iDCT_1D_reference_fine_10432();
void iDCT_1D_reference_fine_10433();
void iDCT_1D_reference_fine_10434();
void WEIGHTED_ROUND_ROBIN_Joiner_10426();
void WEIGHTED_ROUND_ROBIN_Splitter_10435();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_10437();
void AnonFilter_a4_10438();
void AnonFilter_a4_10439();
void AnonFilter_a4_10440();
void AnonFilter_a4_10441();
void AnonFilter_a4_10442();
void AnonFilter_a4_10443();
void AnonFilter_a4_10444();
void AnonFilter_a4_10445();
void AnonFilter_a4_10446();
void AnonFilter_a4_10447();
void AnonFilter_a4_10448();
void AnonFilter_a4_10449();
void AnonFilter_a4_10450();
void AnonFilter_a4_10451();
void AnonFilter_a4_10452();
void AnonFilter_a4_10453();
void AnonFilter_a4_10454();
void AnonFilter_a4_10455();
void AnonFilter_a4_10456();
void AnonFilter_a4_10457();
void AnonFilter_a4_10458();
void AnonFilter_a4_10459();
void AnonFilter_a4_10460();
void AnonFilter_a4_10461();
void AnonFilter_a4_10462();
void AnonFilter_a4_10463();
void AnonFilter_a4_10464();
void AnonFilter_a4_10465();
void AnonFilter_a4_10466();
void AnonFilter_a4_10467();
void AnonFilter_a4_10468();
void AnonFilter_a4_10469();
void AnonFilter_a4_10470();
void AnonFilter_a4_10471();
void AnonFilter_a4_10472();
void AnonFilter_a4_10473();
void WEIGHTED_ROUND_ROBIN_Joiner_10436();
void WEIGHTED_ROUND_ROBIN_Splitter_10474();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_10476();
void iDCT8x8_1D_row_fast_10477();
void iDCT8x8_1D_row_fast_10478();
void iDCT8x8_1D_row_fast_10479();
void iDCT8x8_1D_row_fast_10480();
void iDCT8x8_1D_row_fast_10481();
void iDCT8x8_1D_row_fast_10482();
void iDCT8x8_1D_row_fast_10483();
void WEIGHTED_ROUND_ROBIN_Joiner_10475();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_10320();
void WEIGHTED_ROUND_ROBIN_Joiner_10369();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_10321();

#ifdef __cplusplus
}
#endif
#endif
