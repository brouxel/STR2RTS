#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1152 on the compile command line
#else
#if BUF_SIZEMAX < 1152
#error BUF_SIZEMAX too small, it must be at least 1152
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_16846_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_16870_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_16871_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_16843();
void DUPLICATE_Splitter_16918();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_16846();
void WEIGHTED_ROUND_ROBIN_Splitter_16926();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_16928();
void AnonFilter_a3_16929();
void AnonFilter_a3_16930();
void AnonFilter_a3_16931();
void AnonFilter_a3_16932();
void AnonFilter_a3_16933();
void AnonFilter_a3_16934();
void AnonFilter_a3_16935();
void AnonFilter_a3_16936();
void AnonFilter_a3_16937();
void AnonFilter_a3_16938();
void AnonFilter_a3_16939();
void WEIGHTED_ROUND_ROBIN_Joiner_16927();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_16916();
void WEIGHTED_ROUND_ROBIN_Splitter_16940();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_16942();
void iDCT_1D_reference_fine_16943();
void iDCT_1D_reference_fine_16944();
void iDCT_1D_reference_fine_16945();
void iDCT_1D_reference_fine_16946();
void iDCT_1D_reference_fine_16947();
void iDCT_1D_reference_fine_16948();
void iDCT_1D_reference_fine_16949();
void WEIGHTED_ROUND_ROBIN_Joiner_16941();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_16917();
void WEIGHTED_ROUND_ROBIN_Splitter_16950();
void iDCT_1D_reference_fine_16952();
void iDCT_1D_reference_fine_16953();
void iDCT_1D_reference_fine_16954();
void iDCT_1D_reference_fine_16955();
void iDCT_1D_reference_fine_16956();
void iDCT_1D_reference_fine_16957();
void iDCT_1D_reference_fine_16958();
void iDCT_1D_reference_fine_16959();
void WEIGHTED_ROUND_ROBIN_Joiner_16951();
void WEIGHTED_ROUND_ROBIN_Splitter_16960();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_16962();
void AnonFilter_a4_16963();
void AnonFilter_a4_16964();
void AnonFilter_a4_16965();
void AnonFilter_a4_16966();
void AnonFilter_a4_16967();
void AnonFilter_a4_16968();
void AnonFilter_a4_16969();
void AnonFilter_a4_16970();
void AnonFilter_a4_16971();
void AnonFilter_a4_16972();
void AnonFilter_a4_16973();
void WEIGHTED_ROUND_ROBIN_Joiner_16961();
void WEIGHTED_ROUND_ROBIN_Splitter_16974();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_16976();
void iDCT8x8_1D_row_fast_16977();
void iDCT8x8_1D_row_fast_16978();
void iDCT8x8_1D_row_fast_16979();
void iDCT8x8_1D_row_fast_16980();
void iDCT8x8_1D_row_fast_16981();
void iDCT8x8_1D_row_fast_16982();
void iDCT8x8_1D_row_fast_16983();
void WEIGHTED_ROUND_ROBIN_Joiner_16975();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_16870();
void WEIGHTED_ROUND_ROBIN_Joiner_16919();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_16871();

#ifdef __cplusplus
}
#endif
#endif
