#include "PEG41-iDCTcompare.h"

buffer_float_t Pre_CollapsedDataParallel_1_9086WEIGHTED_ROUND_ROBIN_Splitter_9139;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_9089AnonFilter_a2_9041;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[41];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[41];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9140Post_CollapsedDataParallel_2_9087;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_split[3];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[41];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9097Pre_CollapsedDataParallel_1_9086;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9150WEIGHTED_ROUND_ROBIN_Splitter_9159;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_9203iDCT8x8_1D_col_fast_9040;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[41];
buffer_int_t SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[8];
buffer_int_t SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_join[3];
buffer_int_t AnonFilter_a0_9013DUPLICATE_Splitter_9088;
buffer_float_t Post_CollapsedDataParallel_2_9087WEIGHTED_ROUND_ROBIN_Splitter_9149;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[8];


iDCT_2D_reference_coarse_9016_t iDCT_2D_reference_coarse_9016_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9141_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9142_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9143_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9144_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9145_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9146_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9147_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9148_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9151_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9152_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9153_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9154_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9155_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9156_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9157_s;
iDCT_2D_reference_coarse_9016_t iDCT_1D_reference_fine_9158_s;
iDCT8x8_1D_col_fast_9040_t iDCT8x8_1D_col_fast_9040_s;
AnonFilter_a2_9041_t AnonFilter_a2_9041_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_9013() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_9013DUPLICATE_Splitter_9088));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_9016_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_9016_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_9016() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_9098() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[0]));
	ENDFOR
}

void AnonFilter_a3_9099() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[1]));
	ENDFOR
}

void AnonFilter_a3_9100() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[2]));
	ENDFOR
}

void AnonFilter_a3_9101() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[3]));
	ENDFOR
}

void AnonFilter_a3_9102() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[4]));
	ENDFOR
}

void AnonFilter_a3_9103() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[5]));
	ENDFOR
}

void AnonFilter_a3_9104() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[6]));
	ENDFOR
}

void AnonFilter_a3_9105() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[7]));
	ENDFOR
}

void AnonFilter_a3_9106() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[8]));
	ENDFOR
}

void AnonFilter_a3_9107() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[9]));
	ENDFOR
}

void AnonFilter_a3_9108() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[10]));
	ENDFOR
}

void AnonFilter_a3_9109() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[11]));
	ENDFOR
}

void AnonFilter_a3_9110() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[12]));
	ENDFOR
}

void AnonFilter_a3_9111() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[13]));
	ENDFOR
}

void AnonFilter_a3_9112() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[14]));
	ENDFOR
}

void AnonFilter_a3_9113() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[15]));
	ENDFOR
}

void AnonFilter_a3_9114() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[16]));
	ENDFOR
}

void AnonFilter_a3_9115() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[17]));
	ENDFOR
}

void AnonFilter_a3_9116() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[18]));
	ENDFOR
}

void AnonFilter_a3_9117() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[19]));
	ENDFOR
}

void AnonFilter_a3_9118() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[20]));
	ENDFOR
}

void AnonFilter_a3_9119() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[21]));
	ENDFOR
}

void AnonFilter_a3_9120() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[22]));
	ENDFOR
}

void AnonFilter_a3_9121() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[23]));
	ENDFOR
}

void AnonFilter_a3_9122() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[24]));
	ENDFOR
}

void AnonFilter_a3_9123() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[25]));
	ENDFOR
}

void AnonFilter_a3_9124() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[26]));
	ENDFOR
}

void AnonFilter_a3_9125() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[27]));
	ENDFOR
}

void AnonFilter_a3_9126() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[28]));
	ENDFOR
}

void AnonFilter_a3_9127() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[29]));
	ENDFOR
}

void AnonFilter_a3_9128() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[30]));
	ENDFOR
}

void AnonFilter_a3_9129() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[31]));
	ENDFOR
}

void AnonFilter_a3_9130() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[32]));
	ENDFOR
}

void AnonFilter_a3_9131() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[33]));
	ENDFOR
}

void AnonFilter_a3_9132() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[34]));
	ENDFOR
}

void AnonFilter_a3_9133() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[35]));
	ENDFOR
}

void AnonFilter_a3_9134() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[36]));
	ENDFOR
}

void AnonFilter_a3_9135() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[37]));
	ENDFOR
}

void AnonFilter_a3_9136() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[38]));
	ENDFOR
}

void AnonFilter_a3_9137() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[39]));
	ENDFOR
}

void AnonFilter_a3_9138() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[40]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9096() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 41, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9097() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 41, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9097Pre_CollapsedDataParallel_1_9086, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_9086() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_9097Pre_CollapsedDataParallel_1_9086), &(Pre_CollapsedDataParallel_1_9086WEIGHTED_ROUND_ROBIN_Splitter_9139));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9141_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_9141() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_9142() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_9143() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_9144() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_9145() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_9146() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_9147() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_9148() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9139() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9086WEIGHTED_ROUND_ROBIN_Splitter_9139));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9140() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9140Post_CollapsedDataParallel_2_9087, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_9087() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9140Post_CollapsedDataParallel_2_9087), &(Post_CollapsedDataParallel_2_9087WEIGHTED_ROUND_ROBIN_Splitter_9149));
	ENDFOR
}

void iDCT_1D_reference_fine_9151() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_9152() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_9153() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_9154() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_9155() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_9156() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_9157() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_9158() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9149() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_9087WEIGHTED_ROUND_ROBIN_Splitter_9149));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9150() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9150WEIGHTED_ROUND_ROBIN_Splitter_9159, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_9161() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[0]));
	ENDFOR
}

void AnonFilter_a4_9162() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[1]));
	ENDFOR
}

void AnonFilter_a4_9163() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[2]));
	ENDFOR
}

void AnonFilter_a4_9164() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[3]));
	ENDFOR
}

void AnonFilter_a4_9165() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[4]));
	ENDFOR
}

void AnonFilter_a4_9166() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[5]));
	ENDFOR
}

void AnonFilter_a4_9167() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[6]));
	ENDFOR
}

void AnonFilter_a4_9168() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[7]));
	ENDFOR
}

void AnonFilter_a4_9169() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[8]));
	ENDFOR
}

void AnonFilter_a4_9170() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[9]));
	ENDFOR
}

void AnonFilter_a4_9171() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[10]));
	ENDFOR
}

void AnonFilter_a4_9172() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[11]));
	ENDFOR
}

void AnonFilter_a4_9173() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[12]));
	ENDFOR
}

void AnonFilter_a4_9174() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[13]));
	ENDFOR
}

void AnonFilter_a4_9175() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[14]));
	ENDFOR
}

void AnonFilter_a4_9176() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[15]));
	ENDFOR
}

void AnonFilter_a4_9177() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[16]));
	ENDFOR
}

void AnonFilter_a4_9178() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[17]));
	ENDFOR
}

void AnonFilter_a4_9179() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[18]));
	ENDFOR
}

void AnonFilter_a4_9180() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[19]));
	ENDFOR
}

void AnonFilter_a4_9181() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[20]));
	ENDFOR
}

void AnonFilter_a4_9182() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[21]));
	ENDFOR
}

void AnonFilter_a4_9183() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[22]));
	ENDFOR
}

void AnonFilter_a4_9184() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[23]));
	ENDFOR
}

void AnonFilter_a4_9185() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[24]));
	ENDFOR
}

void AnonFilter_a4_9186() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[25]));
	ENDFOR
}

void AnonFilter_a4_9187() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[26]));
	ENDFOR
}

void AnonFilter_a4_9188() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[27]));
	ENDFOR
}

void AnonFilter_a4_9189() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[28]));
	ENDFOR
}

void AnonFilter_a4_9190() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[29]));
	ENDFOR
}

void AnonFilter_a4_9191() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[30]));
	ENDFOR
}

void AnonFilter_a4_9192() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[31]));
	ENDFOR
}

void AnonFilter_a4_9193() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[32]));
	ENDFOR
}

void AnonFilter_a4_9194() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[33]));
	ENDFOR
}

void AnonFilter_a4_9195() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[34]));
	ENDFOR
}

void AnonFilter_a4_9196() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[35]));
	ENDFOR
}

void AnonFilter_a4_9197() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[36]));
	ENDFOR
}

void AnonFilter_a4_9198() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[37]));
	ENDFOR
}

void AnonFilter_a4_9199() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[38]));
	ENDFOR
}

void AnonFilter_a4_9200() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[39]));
	ENDFOR
}

void AnonFilter_a4_9201() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[40]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9159() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 41, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9150WEIGHTED_ROUND_ROBIN_Splitter_9159));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9160() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 41, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_9204() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[0]), &(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9205() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[1]), &(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9206() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[2]), &(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9207() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[3]), &(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9208() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[4]), &(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9209() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[5]), &(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9210() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[6]), &(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9211() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[7]), &(SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9202() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9203() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_9203iDCT8x8_1D_col_fast_9040, pop_int(&SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_9040_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_9040_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_9040_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_9040_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_9040_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_9040_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_9040_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_9040_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_9040_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_9040_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_9040() {
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_9203iDCT8x8_1D_col_fast_9040), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_9088() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2624, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_9013DUPLICATE_Splitter_9088);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9089() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2624, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_9089AnonFilter_a2_9041, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_9041_s.count = (AnonFilter_a2_9041_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_9041_s.errors = (AnonFilter_a2_9041_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_9041_s.errors / AnonFilter_a2_9041_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_9041_s.errors = (AnonFilter_a2_9041_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_9041_s.errors / AnonFilter_a2_9041_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_9041() {
	FOR(uint32_t, __iter_steady_, 0, <, 2624, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_9089AnonFilter_a2_9041));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Pre_CollapsedDataParallel_1_9086WEIGHTED_ROUND_ROBIN_Splitter_9139);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_9089AnonFilter_a2_9041);
	FOR(int, __iter_init_0_, 0, <, 41, __iter_init_0_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_9213_9220_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 41, __iter_init_1_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_9213_9220_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9140Post_CollapsedDataParallel_2_9087);
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 41, __iter_init_4_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_9216_9223_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9097Pre_CollapsedDataParallel_1_9086);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9150WEIGHTED_ROUND_ROBIN_Splitter_9159);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_join[__iter_init_5_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_9203iDCT8x8_1D_col_fast_9040);
	FOR(int, __iter_init_6_, 0, <, 41, __iter_init_6_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_9216_9223_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin106_iDCT8x8_1D_row_fast_Fiss_9217_9224_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9014_9090_9212_9219_join[__iter_init_9_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_9013DUPLICATE_Splitter_9088);
	init_buffer_float(&Post_CollapsedDataParallel_2_9087WEIGHTED_ROUND_ROBIN_Splitter_9149);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9215_9222_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9214_9221_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_9016
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_9016_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9141
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9141_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9142
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9142_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9143
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9143_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9144
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9144_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9145
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9145_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9146
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9146_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9147
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9147_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9148
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9148_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9151
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9151_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9152
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9152_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9153
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9153_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9154
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9154_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9155
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9155_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9156
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9156_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9157
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9157_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9158
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9158_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_9041
	 {
	AnonFilter_a2_9041_s.count = 0.0 ; 
	AnonFilter_a2_9041_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_9013();
		DUPLICATE_Splitter_9088();
			iDCT_2D_reference_coarse_9016();
			WEIGHTED_ROUND_ROBIN_Splitter_9096();
				AnonFilter_a3_9098();
				AnonFilter_a3_9099();
				AnonFilter_a3_9100();
				AnonFilter_a3_9101();
				AnonFilter_a3_9102();
				AnonFilter_a3_9103();
				AnonFilter_a3_9104();
				AnonFilter_a3_9105();
				AnonFilter_a3_9106();
				AnonFilter_a3_9107();
				AnonFilter_a3_9108();
				AnonFilter_a3_9109();
				AnonFilter_a3_9110();
				AnonFilter_a3_9111();
				AnonFilter_a3_9112();
				AnonFilter_a3_9113();
				AnonFilter_a3_9114();
				AnonFilter_a3_9115();
				AnonFilter_a3_9116();
				AnonFilter_a3_9117();
				AnonFilter_a3_9118();
				AnonFilter_a3_9119();
				AnonFilter_a3_9120();
				AnonFilter_a3_9121();
				AnonFilter_a3_9122();
				AnonFilter_a3_9123();
				AnonFilter_a3_9124();
				AnonFilter_a3_9125();
				AnonFilter_a3_9126();
				AnonFilter_a3_9127();
				AnonFilter_a3_9128();
				AnonFilter_a3_9129();
				AnonFilter_a3_9130();
				AnonFilter_a3_9131();
				AnonFilter_a3_9132();
				AnonFilter_a3_9133();
				AnonFilter_a3_9134();
				AnonFilter_a3_9135();
				AnonFilter_a3_9136();
				AnonFilter_a3_9137();
				AnonFilter_a3_9138();
			WEIGHTED_ROUND_ROBIN_Joiner_9097();
			Pre_CollapsedDataParallel_1_9086();
			WEIGHTED_ROUND_ROBIN_Splitter_9139();
				iDCT_1D_reference_fine_9141();
				iDCT_1D_reference_fine_9142();
				iDCT_1D_reference_fine_9143();
				iDCT_1D_reference_fine_9144();
				iDCT_1D_reference_fine_9145();
				iDCT_1D_reference_fine_9146();
				iDCT_1D_reference_fine_9147();
				iDCT_1D_reference_fine_9148();
			WEIGHTED_ROUND_ROBIN_Joiner_9140();
			Post_CollapsedDataParallel_2_9087();
			WEIGHTED_ROUND_ROBIN_Splitter_9149();
				iDCT_1D_reference_fine_9151();
				iDCT_1D_reference_fine_9152();
				iDCT_1D_reference_fine_9153();
				iDCT_1D_reference_fine_9154();
				iDCT_1D_reference_fine_9155();
				iDCT_1D_reference_fine_9156();
				iDCT_1D_reference_fine_9157();
				iDCT_1D_reference_fine_9158();
			WEIGHTED_ROUND_ROBIN_Joiner_9150();
			WEIGHTED_ROUND_ROBIN_Splitter_9159();
				AnonFilter_a4_9161();
				AnonFilter_a4_9162();
				AnonFilter_a4_9163();
				AnonFilter_a4_9164();
				AnonFilter_a4_9165();
				AnonFilter_a4_9166();
				AnonFilter_a4_9167();
				AnonFilter_a4_9168();
				AnonFilter_a4_9169();
				AnonFilter_a4_9170();
				AnonFilter_a4_9171();
				AnonFilter_a4_9172();
				AnonFilter_a4_9173();
				AnonFilter_a4_9174();
				AnonFilter_a4_9175();
				AnonFilter_a4_9176();
				AnonFilter_a4_9177();
				AnonFilter_a4_9178();
				AnonFilter_a4_9179();
				AnonFilter_a4_9180();
				AnonFilter_a4_9181();
				AnonFilter_a4_9182();
				AnonFilter_a4_9183();
				AnonFilter_a4_9184();
				AnonFilter_a4_9185();
				AnonFilter_a4_9186();
				AnonFilter_a4_9187();
				AnonFilter_a4_9188();
				AnonFilter_a4_9189();
				AnonFilter_a4_9190();
				AnonFilter_a4_9191();
				AnonFilter_a4_9192();
				AnonFilter_a4_9193();
				AnonFilter_a4_9194();
				AnonFilter_a4_9195();
				AnonFilter_a4_9196();
				AnonFilter_a4_9197();
				AnonFilter_a4_9198();
				AnonFilter_a4_9199();
				AnonFilter_a4_9200();
				AnonFilter_a4_9201();
			WEIGHTED_ROUND_ROBIN_Joiner_9160();
			WEIGHTED_ROUND_ROBIN_Splitter_9202();
				iDCT8x8_1D_row_fast_9204();
				iDCT8x8_1D_row_fast_9205();
				iDCT8x8_1D_row_fast_9206();
				iDCT8x8_1D_row_fast_9207();
				iDCT8x8_1D_row_fast_9208();
				iDCT8x8_1D_row_fast_9209();
				iDCT8x8_1D_row_fast_9210();
				iDCT8x8_1D_row_fast_9211();
			WEIGHTED_ROUND_ROBIN_Joiner_9203();
			iDCT8x8_1D_col_fast_9040();
		WEIGHTED_ROUND_ROBIN_Joiner_9089();
		AnonFilter_a2_9041();
	ENDFOR
	return EXIT_SUCCESS;
}
