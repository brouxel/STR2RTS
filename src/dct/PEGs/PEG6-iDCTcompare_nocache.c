#include "PEG6-iDCTcompare_nocache.h"

buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_join[3];
buffer_int_t SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18129Post_CollapsedDataParallel_2_18111;
buffer_int_t SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[6];
buffer_float_t Pre_CollapsedDataParallel_1_18110WEIGHTED_ROUND_ROBIN_Splitter_18128;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[6];
buffer_float_t Post_CollapsedDataParallel_2_18111WEIGHTED_ROUND_ROBIN_Splitter_18136;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[6];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_split[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18121Pre_CollapsedDataParallel_1_18110;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18137WEIGHTED_ROUND_ROBIN_Splitter_18144;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[6];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_18113AnonFilter_a2_18065;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[6];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[6];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[6];
buffer_int_t AnonFilter_a0_18037DUPLICATE_Splitter_18112;


iDCT_2D_reference_coarse_18040_t iDCT_2D_reference_coarse_18040_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18130_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18131_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18132_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18133_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18134_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18135_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18138_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18139_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18140_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18141_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18142_s;
iDCT_2D_reference_coarse_18040_t iDCT_1D_reference_fine_18143_s;
iDCT8x8_1D_col_fast_18064_t iDCT8x8_1D_col_fast_18064_s;
AnonFilter_a2_18065_t AnonFilter_a2_18065_s;

void AnonFilter_a0_18037(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_18037DUPLICATE_Splitter_18112, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_18040(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_18040_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_18040_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_18122(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_18123(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_18124(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_18125(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_18126(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_18127(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[5])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18120() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18121() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18121Pre_CollapsedDataParallel_1_18110, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_18110(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_18110WEIGHTED_ROUND_ROBIN_Splitter_18128, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_18121Pre_CollapsedDataParallel_1_18110, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_18121Pre_CollapsedDataParallel_1_18110) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18130(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18130_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18131(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18131_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18132(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18132_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18133(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18133_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18134(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18134_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18135(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18135_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18128() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_18110WEIGHTED_ROUND_ROBIN_Splitter_18128));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18129() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18129Post_CollapsedDataParallel_2_18111, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_18111(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_18111WEIGHTED_ROUND_ROBIN_Splitter_18136, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_18129Post_CollapsedDataParallel_2_18111, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_18129Post_CollapsedDataParallel_2_18111) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18138(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18138_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18139(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18139_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18140(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18140_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18141(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18141_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18142(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18142_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_18143(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18143_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18136() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_18111WEIGHTED_ROUND_ROBIN_Splitter_18136));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18137() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18137WEIGHTED_ROUND_ROBIN_Splitter_18144, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_18146(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_18147(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_18148(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_18149(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_18150(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_18151(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18144() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_18137WEIGHTED_ROUND_ROBIN_Splitter_18144));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18145() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_18154(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0], 6) ; 
		x3 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0], 2) ; 
		x4 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0], 1) ; 
		x5 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0], 7) ; 
		x6 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0], 5) ; 
		x7 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_18155(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1], 6) ; 
		x3 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1], 2) ; 
		x4 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1], 1) ; 
		x5 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1], 7) ; 
		x6 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1], 5) ; 
		x7 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_18156(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2], 6) ; 
		x3 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2], 2) ; 
		x4 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2], 1) ; 
		x5 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2], 7) ; 
		x6 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2], 5) ; 
		x7 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_18157(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3], 6) ; 
		x3 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3], 2) ; 
		x4 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3], 1) ; 
		x5 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3], 7) ; 
		x6 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3], 5) ; 
		x7 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_18158(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4], 6) ; 
		x3 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4], 2) ; 
		x4 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4], 1) ; 
		x5 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4], 7) ; 
		x6 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4], 5) ; 
		x7 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_18159(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5], 6) ; 
		x3 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5], 2) ; 
		x4 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5], 1) ; 
		x5 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5], 7) ; 
		x6 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5], 5) ; 
		x7 = peek_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18152() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18153() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, pop_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_18064(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_18064_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_18064_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_18064_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_18064_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_18064_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_18064_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_18064_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_18064_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_18064_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_join[2], iDCT8x8_1D_col_fast_18064_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_18112() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_18037DUPLICATE_Splitter_18112);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18113() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_18113AnonFilter_a2_18065, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_18065(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_18113AnonFilter_a2_18065) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_18113AnonFilter_a2_18065) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_18113AnonFilter_a2_18065) ; 
		AnonFilter_a2_18065_s.count = (AnonFilter_a2_18065_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_18065_s.errors = (AnonFilter_a2_18065_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_18065_s.errors / AnonFilter_a2_18065_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_18065_s.errors = (AnonFilter_a2_18065_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_18065_s.errors / AnonFilter_a2_18065_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_18153iDCT8x8_1D_col_fast_18064);
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 6, __iter_init_1_++)
		init_buffer_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18129Post_CollapsedDataParallel_2_18111);
	FOR(int, __iter_init_2_, 0, <, 6, __iter_init_2_++)
		init_buffer_int(&SplitJoin32_iDCT8x8_1D_row_fast_Fiss_18165_18172_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_18110WEIGHTED_ROUND_ROBIN_Splitter_18128);
	FOR(int, __iter_init_3_, 0, <, 6, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_18111WEIGHTED_ROUND_ROBIN_Splitter_18136);
	FOR(int, __iter_init_4_, 0, <, 6, __iter_init_4_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18038_18114_18160_18167_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 6, __iter_init_6_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18162_18169_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18121Pre_CollapsedDataParallel_1_18110);
	FOR(int, __iter_init_7_, 0, <, 6, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18137WEIGHTED_ROUND_ROBIN_Splitter_18144);
	FOR(int, __iter_init_8_, 0, <, 6, __iter_init_8_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_18161_18168_split[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_18113AnonFilter_a2_18065);
	FOR(int, __iter_init_9_, 0, <, 6, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18163_18170_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 6, __iter_init_10_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 6, __iter_init_11_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_18164_18171_split[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_18037DUPLICATE_Splitter_18112);
// --- init: iDCT_2D_reference_coarse_18040
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_18040_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18130
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18130_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18131
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18131_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18132
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18132_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18133
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18133_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18134
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18134_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18135
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18135_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18138
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18138_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18139
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18139_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18140
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18140_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18141
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18141_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18142
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18142_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18143
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18143_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_18065
	 {
	AnonFilter_a2_18065_s.count = 0.0 ; 
	AnonFilter_a2_18065_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_18037();
		DUPLICATE_Splitter_18112();
			iDCT_2D_reference_coarse_18040();
			WEIGHTED_ROUND_ROBIN_Splitter_18120();
				AnonFilter_a3_18122();
				AnonFilter_a3_18123();
				AnonFilter_a3_18124();
				AnonFilter_a3_18125();
				AnonFilter_a3_18126();
				AnonFilter_a3_18127();
			WEIGHTED_ROUND_ROBIN_Joiner_18121();
			Pre_CollapsedDataParallel_1_18110();
			WEIGHTED_ROUND_ROBIN_Splitter_18128();
				iDCT_1D_reference_fine_18130();
				iDCT_1D_reference_fine_18131();
				iDCT_1D_reference_fine_18132();
				iDCT_1D_reference_fine_18133();
				iDCT_1D_reference_fine_18134();
				iDCT_1D_reference_fine_18135();
			WEIGHTED_ROUND_ROBIN_Joiner_18129();
			Post_CollapsedDataParallel_2_18111();
			WEIGHTED_ROUND_ROBIN_Splitter_18136();
				iDCT_1D_reference_fine_18138();
				iDCT_1D_reference_fine_18139();
				iDCT_1D_reference_fine_18140();
				iDCT_1D_reference_fine_18141();
				iDCT_1D_reference_fine_18142();
				iDCT_1D_reference_fine_18143();
			WEIGHTED_ROUND_ROBIN_Joiner_18137();
			WEIGHTED_ROUND_ROBIN_Splitter_18144();
				AnonFilter_a4_18146();
				AnonFilter_a4_18147();
				AnonFilter_a4_18148();
				AnonFilter_a4_18149();
				AnonFilter_a4_18150();
				AnonFilter_a4_18151();
			WEIGHTED_ROUND_ROBIN_Joiner_18145();
			WEIGHTED_ROUND_ROBIN_Splitter_18152();
				iDCT8x8_1D_row_fast_18154();
				iDCT8x8_1D_row_fast_18155();
				iDCT8x8_1D_row_fast_18156();
				iDCT8x8_1D_row_fast_18157();
				iDCT8x8_1D_row_fast_18158();
				iDCT8x8_1D_row_fast_18159();
			WEIGHTED_ROUND_ROBIN_Joiner_18153();
			iDCT8x8_1D_col_fast_18064();
		WEIGHTED_ROUND_ROBIN_Joiner_18113();
		AnonFilter_a2_18065();
	ENDFOR
	return EXIT_SUCCESS;
}
