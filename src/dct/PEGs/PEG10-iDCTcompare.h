#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1920 on the compile command line
#else
#if BUF_SIZEMAX < 1920
#error BUF_SIZEMAX too small, it must be at least 1920
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_17262_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_17286_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_17287_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_17259();
void DUPLICATE_Splitter_17334();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_17262();
void WEIGHTED_ROUND_ROBIN_Splitter_17342();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_17344();
void AnonFilter_a3_17345();
void AnonFilter_a3_17346();
void AnonFilter_a3_17347();
void AnonFilter_a3_17348();
void AnonFilter_a3_17349();
void AnonFilter_a3_17350();
void AnonFilter_a3_17351();
void AnonFilter_a3_17352();
void AnonFilter_a3_17353();
void WEIGHTED_ROUND_ROBIN_Joiner_17343();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_17332();
void WEIGHTED_ROUND_ROBIN_Splitter_17354();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_17356();
void iDCT_1D_reference_fine_17357();
void iDCT_1D_reference_fine_17358();
void iDCT_1D_reference_fine_17359();
void iDCT_1D_reference_fine_17360();
void iDCT_1D_reference_fine_17361();
void iDCT_1D_reference_fine_17362();
void iDCT_1D_reference_fine_17363();
void WEIGHTED_ROUND_ROBIN_Joiner_17355();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_17333();
void WEIGHTED_ROUND_ROBIN_Splitter_17364();
void iDCT_1D_reference_fine_17366();
void iDCT_1D_reference_fine_17367();
void iDCT_1D_reference_fine_17368();
void iDCT_1D_reference_fine_17369();
void iDCT_1D_reference_fine_17370();
void iDCT_1D_reference_fine_17371();
void iDCT_1D_reference_fine_17372();
void iDCT_1D_reference_fine_17373();
void WEIGHTED_ROUND_ROBIN_Joiner_17365();
void WEIGHTED_ROUND_ROBIN_Splitter_17374();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_17376();
void AnonFilter_a4_17377();
void AnonFilter_a4_17378();
void AnonFilter_a4_17379();
void AnonFilter_a4_17380();
void AnonFilter_a4_17381();
void AnonFilter_a4_17382();
void AnonFilter_a4_17383();
void AnonFilter_a4_17384();
void AnonFilter_a4_17385();
void WEIGHTED_ROUND_ROBIN_Joiner_17375();
void WEIGHTED_ROUND_ROBIN_Splitter_17386();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_17388();
void iDCT8x8_1D_row_fast_17389();
void iDCT8x8_1D_row_fast_17390();
void iDCT8x8_1D_row_fast_17391();
void iDCT8x8_1D_row_fast_17392();
void iDCT8x8_1D_row_fast_17393();
void iDCT8x8_1D_row_fast_17394();
void iDCT8x8_1D_row_fast_17395();
void WEIGHTED_ROUND_ROBIN_Joiner_17387();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_17286();
void WEIGHTED_ROUND_ROBIN_Joiner_17335();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_17287();

#ifdef __cplusplus
}
#endif
#endif
