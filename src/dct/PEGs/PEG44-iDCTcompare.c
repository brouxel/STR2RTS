#include "PEG44-iDCTcompare.h"

buffer_float_t Post_CollapsedDataParallel_2_8085WEIGHTED_ROUND_ROBIN_Splitter_8150;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[8];
buffer_int_t SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[44];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8151WEIGHTED_ROUND_ROBIN_Splitter_8160;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[44];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[8];
buffer_int_t AnonFilter_a0_8011DUPLICATE_Splitter_8086;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_split[3];
buffer_float_t Pre_CollapsedDataParallel_1_8084WEIGHTED_ROUND_ROBIN_Splitter_8140;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[44];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[44];
buffer_int_t SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8141Post_CollapsedDataParallel_2_8085;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_8207iDCT8x8_1D_col_fast_8038;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8095Pre_CollapsedDataParallel_1_8084;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_8087AnonFilter_a2_8039;


iDCT_2D_reference_coarse_8014_t iDCT_2D_reference_coarse_8014_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8142_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8143_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8144_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8145_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8146_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8147_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8148_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8149_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8152_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8153_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8154_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8155_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8156_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8157_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8158_s;
iDCT_2D_reference_coarse_8014_t iDCT_1D_reference_fine_8159_s;
iDCT8x8_1D_col_fast_8038_t iDCT8x8_1D_col_fast_8038_s;
AnonFilter_a2_8039_t AnonFilter_a2_8039_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_8011() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_8011DUPLICATE_Splitter_8086));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_8014_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_8014_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_8014() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_8096() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[0]));
	ENDFOR
}

void AnonFilter_a3_8097() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[1]));
	ENDFOR
}

void AnonFilter_a3_8098() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[2]));
	ENDFOR
}

void AnonFilter_a3_8099() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[3]));
	ENDFOR
}

void AnonFilter_a3_8100() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[4]));
	ENDFOR
}

void AnonFilter_a3_8101() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[5]));
	ENDFOR
}

void AnonFilter_a3_8102() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[6]));
	ENDFOR
}

void AnonFilter_a3_8103() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[7]));
	ENDFOR
}

void AnonFilter_a3_8104() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[8]));
	ENDFOR
}

void AnonFilter_a3_8105() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[9]));
	ENDFOR
}

void AnonFilter_a3_8106() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[10]));
	ENDFOR
}

void AnonFilter_a3_8107() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[11]));
	ENDFOR
}

void AnonFilter_a3_8108() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[12]));
	ENDFOR
}

void AnonFilter_a3_8109() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[13]));
	ENDFOR
}

void AnonFilter_a3_8110() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[14]));
	ENDFOR
}

void AnonFilter_a3_8111() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[15]));
	ENDFOR
}

void AnonFilter_a3_8112() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[16]));
	ENDFOR
}

void AnonFilter_a3_8113() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[17]));
	ENDFOR
}

void AnonFilter_a3_8114() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[18]));
	ENDFOR
}

void AnonFilter_a3_8115() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[19]));
	ENDFOR
}

void AnonFilter_a3_8116() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[20]));
	ENDFOR
}

void AnonFilter_a3_8117() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[21]));
	ENDFOR
}

void AnonFilter_a3_8118() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[22]));
	ENDFOR
}

void AnonFilter_a3_8119() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[23]));
	ENDFOR
}

void AnonFilter_a3_8120() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[24]));
	ENDFOR
}

void AnonFilter_a3_8121() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[25]));
	ENDFOR
}

void AnonFilter_a3_8122() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[26]));
	ENDFOR
}

void AnonFilter_a3_8123() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[27]));
	ENDFOR
}

void AnonFilter_a3_8124() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[28]));
	ENDFOR
}

void AnonFilter_a3_8125() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[29]));
	ENDFOR
}

void AnonFilter_a3_8126() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[30]));
	ENDFOR
}

void AnonFilter_a3_8127() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[31]));
	ENDFOR
}

void AnonFilter_a3_8128() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[32]));
	ENDFOR
}

void AnonFilter_a3_8129() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[33]));
	ENDFOR
}

void AnonFilter_a3_8130() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[34]));
	ENDFOR
}

void AnonFilter_a3_8131() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[35]));
	ENDFOR
}

void AnonFilter_a3_8132() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[36]));
	ENDFOR
}

void AnonFilter_a3_8133() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[37]));
	ENDFOR
}

void AnonFilter_a3_8134() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[38]));
	ENDFOR
}

void AnonFilter_a3_8135() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[39]));
	ENDFOR
}

void AnonFilter_a3_8136() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[40]));
	ENDFOR
}

void AnonFilter_a3_8137() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[41]));
	ENDFOR
}

void AnonFilter_a3_8138() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[42]));
	ENDFOR
}

void AnonFilter_a3_8139() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[43]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8094() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 44, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8095() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 44, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8095Pre_CollapsedDataParallel_1_8084, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_8084() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_8095Pre_CollapsedDataParallel_1_8084), &(Pre_CollapsedDataParallel_1_8084WEIGHTED_ROUND_ROBIN_Splitter_8140));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8142_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_8142() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_8143() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_8144() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_8145() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_8146() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_8147() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_8148() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_8149() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8140() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8084WEIGHTED_ROUND_ROBIN_Splitter_8140));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8141() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8141Post_CollapsedDataParallel_2_8085, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_8085() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_8141Post_CollapsedDataParallel_2_8085), &(Post_CollapsedDataParallel_2_8085WEIGHTED_ROUND_ROBIN_Splitter_8150));
	ENDFOR
}

void iDCT_1D_reference_fine_8152() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_8153() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_8154() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_8155() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_8156() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_8157() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_8158() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_8159() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8150() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_8085WEIGHTED_ROUND_ROBIN_Splitter_8150));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8151() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8151WEIGHTED_ROUND_ROBIN_Splitter_8160, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_8162() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[0]));
	ENDFOR
}

void AnonFilter_a4_8163() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[1]));
	ENDFOR
}

void AnonFilter_a4_8164() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[2]));
	ENDFOR
}

void AnonFilter_a4_8165() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[3]));
	ENDFOR
}

void AnonFilter_a4_8166() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[4]));
	ENDFOR
}

void AnonFilter_a4_8167() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[5]));
	ENDFOR
}

void AnonFilter_a4_8168() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[6]));
	ENDFOR
}

void AnonFilter_a4_8169() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[7]));
	ENDFOR
}

void AnonFilter_a4_8170() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[8]));
	ENDFOR
}

void AnonFilter_a4_8171() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[9]));
	ENDFOR
}

void AnonFilter_a4_8172() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[10]));
	ENDFOR
}

void AnonFilter_a4_8173() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[11]));
	ENDFOR
}

void AnonFilter_a4_8174() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[12]));
	ENDFOR
}

void AnonFilter_a4_8175() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[13]));
	ENDFOR
}

void AnonFilter_a4_8176() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[14]));
	ENDFOR
}

void AnonFilter_a4_8177() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[15]));
	ENDFOR
}

void AnonFilter_a4_8178() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[16]));
	ENDFOR
}

void AnonFilter_a4_8179() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[17]));
	ENDFOR
}

void AnonFilter_a4_8180() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[18]));
	ENDFOR
}

void AnonFilter_a4_8181() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[19]));
	ENDFOR
}

void AnonFilter_a4_8182() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[20]));
	ENDFOR
}

void AnonFilter_a4_8183() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[21]));
	ENDFOR
}

void AnonFilter_a4_8184() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[22]));
	ENDFOR
}

void AnonFilter_a4_8185() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[23]));
	ENDFOR
}

void AnonFilter_a4_8186() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[24]));
	ENDFOR
}

void AnonFilter_a4_8187() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[25]));
	ENDFOR
}

void AnonFilter_a4_8188() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[26]));
	ENDFOR
}

void AnonFilter_a4_8189() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[27]));
	ENDFOR
}

void AnonFilter_a4_8190() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[28]));
	ENDFOR
}

void AnonFilter_a4_8191() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[29]));
	ENDFOR
}

void AnonFilter_a4_8192() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[30]));
	ENDFOR
}

void AnonFilter_a4_8193() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[31]));
	ENDFOR
}

void AnonFilter_a4_8194() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[32]));
	ENDFOR
}

void AnonFilter_a4_8195() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[33]));
	ENDFOR
}

void AnonFilter_a4_8196() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[34]));
	ENDFOR
}

void AnonFilter_a4_8197() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[35]));
	ENDFOR
}

void AnonFilter_a4_8198() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[36]));
	ENDFOR
}

void AnonFilter_a4_8199() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[37]));
	ENDFOR
}

void AnonFilter_a4_8200() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[38]));
	ENDFOR
}

void AnonFilter_a4_8201() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[39]));
	ENDFOR
}

void AnonFilter_a4_8202() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[40]));
	ENDFOR
}

void AnonFilter_a4_8203() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[41]));
	ENDFOR
}

void AnonFilter_a4_8204() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[42]));
	ENDFOR
}

void AnonFilter_a4_8205() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[43]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8160() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 44, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8151WEIGHTED_ROUND_ROBIN_Splitter_8160));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8161() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 44, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_8208() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[0]), &(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_8209() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[1]), &(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_8210() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[2]), &(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_8211() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[3]), &(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_8212() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[4]), &(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_8213() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[5]), &(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_8214() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[6]), &(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_8215() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[7]), &(SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8206() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8207() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_8207iDCT8x8_1D_col_fast_8038, pop_int(&SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_8038_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_8038_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_8038_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_8038_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_8038_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_8038_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_8038_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_8038_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_8038_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_8038_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_8038() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_8207iDCT8x8_1D_col_fast_8038), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_8086() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_8011DUPLICATE_Splitter_8086);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8087() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_8087AnonFilter_a2_8039, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_8039_s.count = (AnonFilter_a2_8039_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_8039_s.errors = (AnonFilter_a2_8039_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_8039_s.errors / AnonFilter_a2_8039_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_8039_s.errors = (AnonFilter_a2_8039_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_8039_s.errors / AnonFilter_a2_8039_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_8039() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_8087AnonFilter_a2_8039));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Post_CollapsedDataParallel_2_8085WEIGHTED_ROUND_ROBIN_Splitter_8150);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 44, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_8217_8224_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8151WEIGHTED_ROUND_ROBIN_Splitter_8160);
	FOR(int, __iter_init_3_, 0, <, 44, __iter_init_3_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_8217_8224_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_join[__iter_init_4_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_8011DUPLICATE_Splitter_8086);
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8084WEIGHTED_ROUND_ROBIN_Splitter_8140);
	FOR(int, __iter_init_6_, 0, <, 44, __iter_init_6_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_8220_8227_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 44, __iter_init_7_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_8220_8227_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin112_iDCT8x8_1D_row_fast_Fiss_8221_8228_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8141Post_CollapsedDataParallel_2_8085);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8219_8226_split[__iter_init_9_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_8207iDCT8x8_1D_col_fast_8038);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8095Pre_CollapsedDataParallel_1_8084);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8218_8225_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8012_8088_8216_8223_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_8087AnonFilter_a2_8039);
// --- init: iDCT_2D_reference_coarse_8014
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_8014_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8142
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8142_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8143
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8143_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8144
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8144_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8145
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8145_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8146
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8146_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8147
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8147_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8148
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8148_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8149
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8149_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8152
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8152_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8153
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8153_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8154
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8154_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8155
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8155_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8156
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8156_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8157
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8157_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8158
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8158_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8159
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8159_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_8039
	 {
	AnonFilter_a2_8039_s.count = 0.0 ; 
	AnonFilter_a2_8039_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_8011();
		DUPLICATE_Splitter_8086();
			iDCT_2D_reference_coarse_8014();
			WEIGHTED_ROUND_ROBIN_Splitter_8094();
				AnonFilter_a3_8096();
				AnonFilter_a3_8097();
				AnonFilter_a3_8098();
				AnonFilter_a3_8099();
				AnonFilter_a3_8100();
				AnonFilter_a3_8101();
				AnonFilter_a3_8102();
				AnonFilter_a3_8103();
				AnonFilter_a3_8104();
				AnonFilter_a3_8105();
				AnonFilter_a3_8106();
				AnonFilter_a3_8107();
				AnonFilter_a3_8108();
				AnonFilter_a3_8109();
				AnonFilter_a3_8110();
				AnonFilter_a3_8111();
				AnonFilter_a3_8112();
				AnonFilter_a3_8113();
				AnonFilter_a3_8114();
				AnonFilter_a3_8115();
				AnonFilter_a3_8116();
				AnonFilter_a3_8117();
				AnonFilter_a3_8118();
				AnonFilter_a3_8119();
				AnonFilter_a3_8120();
				AnonFilter_a3_8121();
				AnonFilter_a3_8122();
				AnonFilter_a3_8123();
				AnonFilter_a3_8124();
				AnonFilter_a3_8125();
				AnonFilter_a3_8126();
				AnonFilter_a3_8127();
				AnonFilter_a3_8128();
				AnonFilter_a3_8129();
				AnonFilter_a3_8130();
				AnonFilter_a3_8131();
				AnonFilter_a3_8132();
				AnonFilter_a3_8133();
				AnonFilter_a3_8134();
				AnonFilter_a3_8135();
				AnonFilter_a3_8136();
				AnonFilter_a3_8137();
				AnonFilter_a3_8138();
				AnonFilter_a3_8139();
			WEIGHTED_ROUND_ROBIN_Joiner_8095();
			Pre_CollapsedDataParallel_1_8084();
			WEIGHTED_ROUND_ROBIN_Splitter_8140();
				iDCT_1D_reference_fine_8142();
				iDCT_1D_reference_fine_8143();
				iDCT_1D_reference_fine_8144();
				iDCT_1D_reference_fine_8145();
				iDCT_1D_reference_fine_8146();
				iDCT_1D_reference_fine_8147();
				iDCT_1D_reference_fine_8148();
				iDCT_1D_reference_fine_8149();
			WEIGHTED_ROUND_ROBIN_Joiner_8141();
			Post_CollapsedDataParallel_2_8085();
			WEIGHTED_ROUND_ROBIN_Splitter_8150();
				iDCT_1D_reference_fine_8152();
				iDCT_1D_reference_fine_8153();
				iDCT_1D_reference_fine_8154();
				iDCT_1D_reference_fine_8155();
				iDCT_1D_reference_fine_8156();
				iDCT_1D_reference_fine_8157();
				iDCT_1D_reference_fine_8158();
				iDCT_1D_reference_fine_8159();
			WEIGHTED_ROUND_ROBIN_Joiner_8151();
			WEIGHTED_ROUND_ROBIN_Splitter_8160();
				AnonFilter_a4_8162();
				AnonFilter_a4_8163();
				AnonFilter_a4_8164();
				AnonFilter_a4_8165();
				AnonFilter_a4_8166();
				AnonFilter_a4_8167();
				AnonFilter_a4_8168();
				AnonFilter_a4_8169();
				AnonFilter_a4_8170();
				AnonFilter_a4_8171();
				AnonFilter_a4_8172();
				AnonFilter_a4_8173();
				AnonFilter_a4_8174();
				AnonFilter_a4_8175();
				AnonFilter_a4_8176();
				AnonFilter_a4_8177();
				AnonFilter_a4_8178();
				AnonFilter_a4_8179();
				AnonFilter_a4_8180();
				AnonFilter_a4_8181();
				AnonFilter_a4_8182();
				AnonFilter_a4_8183();
				AnonFilter_a4_8184();
				AnonFilter_a4_8185();
				AnonFilter_a4_8186();
				AnonFilter_a4_8187();
				AnonFilter_a4_8188();
				AnonFilter_a4_8189();
				AnonFilter_a4_8190();
				AnonFilter_a4_8191();
				AnonFilter_a4_8192();
				AnonFilter_a4_8193();
				AnonFilter_a4_8194();
				AnonFilter_a4_8195();
				AnonFilter_a4_8196();
				AnonFilter_a4_8197();
				AnonFilter_a4_8198();
				AnonFilter_a4_8199();
				AnonFilter_a4_8200();
				AnonFilter_a4_8201();
				AnonFilter_a4_8202();
				AnonFilter_a4_8203();
				AnonFilter_a4_8204();
				AnonFilter_a4_8205();
			WEIGHTED_ROUND_ROBIN_Joiner_8161();
			WEIGHTED_ROUND_ROBIN_Splitter_8206();
				iDCT8x8_1D_row_fast_8208();
				iDCT8x8_1D_row_fast_8209();
				iDCT8x8_1D_row_fast_8210();
				iDCT8x8_1D_row_fast_8211();
				iDCT8x8_1D_row_fast_8212();
				iDCT8x8_1D_row_fast_8213();
				iDCT8x8_1D_row_fast_8214();
				iDCT8x8_1D_row_fast_8215();
			WEIGHTED_ROUND_ROBIN_Joiner_8207();
			iDCT8x8_1D_col_fast_8038();
		WEIGHTED_ROUND_ROBIN_Joiner_8087();
		AnonFilter_a2_8039();
	ENDFOR
	return EXIT_SUCCESS;
}
