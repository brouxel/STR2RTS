#include "PEG60-iDCTcompare.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2205Post_CollapsedDataParallel_2_2133;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2215WEIGHTED_ROUND_ROBIN_Splitter_2224;
buffer_int_t SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[8];
buffer_int_t AnonFilter_a0_2059DUPLICATE_Splitter_2134;
buffer_int_t SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_2132WEIGHTED_ROUND_ROBIN_Splitter_2204;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[60];
buffer_float_t Post_CollapsedDataParallel_2_2133WEIGHTED_ROUND_ROBIN_Splitter_2214;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2143Pre_CollapsedDataParallel_1_2132;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_2287iDCT8x8_1D_col_fast_2086;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[60];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[60];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_2135AnonFilter_a2_2087;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[60];


iDCT_2D_reference_coarse_2062_t iDCT_2D_reference_coarse_2062_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2206_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2207_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2208_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2209_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2210_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2211_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2212_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2213_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2216_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2217_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2218_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2219_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2220_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2221_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2222_s;
iDCT_2D_reference_coarse_2062_t iDCT_1D_reference_fine_2223_s;
iDCT8x8_1D_col_fast_2086_t iDCT8x8_1D_col_fast_2086_s;
AnonFilter_a2_2087_t AnonFilter_a2_2087_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_2059() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_2059DUPLICATE_Splitter_2134));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_2062_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_2062_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_2062() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_2144() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[0]));
	ENDFOR
}

void AnonFilter_a3_2145() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[1]));
	ENDFOR
}

void AnonFilter_a3_2146() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[2]));
	ENDFOR
}

void AnonFilter_a3_2147() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[3]));
	ENDFOR
}

void AnonFilter_a3_2148() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[4]));
	ENDFOR
}

void AnonFilter_a3_2149() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[5]));
	ENDFOR
}

void AnonFilter_a3_2150() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[6]));
	ENDFOR
}

void AnonFilter_a3_2151() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[7]));
	ENDFOR
}

void AnonFilter_a3_2152() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[8]));
	ENDFOR
}

void AnonFilter_a3_2153() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[9]));
	ENDFOR
}

void AnonFilter_a3_2154() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[10]));
	ENDFOR
}

void AnonFilter_a3_2155() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[11]));
	ENDFOR
}

void AnonFilter_a3_2156() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[12]));
	ENDFOR
}

void AnonFilter_a3_2157() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[13]));
	ENDFOR
}

void AnonFilter_a3_2158() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[14]));
	ENDFOR
}

void AnonFilter_a3_2159() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[15]));
	ENDFOR
}

void AnonFilter_a3_2160() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[16]));
	ENDFOR
}

void AnonFilter_a3_2161() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[17]));
	ENDFOR
}

void AnonFilter_a3_2162() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[18]));
	ENDFOR
}

void AnonFilter_a3_2163() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[19]));
	ENDFOR
}

void AnonFilter_a3_2164() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[20]));
	ENDFOR
}

void AnonFilter_a3_2165() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[21]));
	ENDFOR
}

void AnonFilter_a3_2166() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[22]));
	ENDFOR
}

void AnonFilter_a3_2167() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[23]));
	ENDFOR
}

void AnonFilter_a3_2168() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[24]));
	ENDFOR
}

void AnonFilter_a3_2169() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[25]));
	ENDFOR
}

void AnonFilter_a3_2170() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[26]));
	ENDFOR
}

void AnonFilter_a3_2171() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[27]));
	ENDFOR
}

void AnonFilter_a3_2172() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[28]));
	ENDFOR
}

void AnonFilter_a3_2173() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[29]));
	ENDFOR
}

void AnonFilter_a3_2174() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[30]));
	ENDFOR
}

void AnonFilter_a3_2175() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[31]));
	ENDFOR
}

void AnonFilter_a3_2176() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[32]));
	ENDFOR
}

void AnonFilter_a3_2177() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[33]));
	ENDFOR
}

void AnonFilter_a3_2178() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[34]));
	ENDFOR
}

void AnonFilter_a3_2179() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[35]));
	ENDFOR
}

void AnonFilter_a3_2180() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[36]));
	ENDFOR
}

void AnonFilter_a3_2181() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[37]));
	ENDFOR
}

void AnonFilter_a3_2182() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[38]));
	ENDFOR
}

void AnonFilter_a3_2183() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[39]));
	ENDFOR
}

void AnonFilter_a3_2184() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[40]));
	ENDFOR
}

void AnonFilter_a3_2185() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[41]));
	ENDFOR
}

void AnonFilter_a3_2186() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[42]));
	ENDFOR
}

void AnonFilter_a3_2187() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[43]));
	ENDFOR
}

void AnonFilter_a3_2188() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[44]));
	ENDFOR
}

void AnonFilter_a3_2189() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[45]));
	ENDFOR
}

void AnonFilter_a3_2190() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[46]));
	ENDFOR
}

void AnonFilter_a3_2191() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[47]));
	ENDFOR
}

void AnonFilter_a3_2192() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[48]));
	ENDFOR
}

void AnonFilter_a3_2193() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[49]));
	ENDFOR
}

void AnonFilter_a3_2194() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[50]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[50]));
	ENDFOR
}

void AnonFilter_a3_2195() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[51]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[51]));
	ENDFOR
}

void AnonFilter_a3_2196() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[52]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[52]));
	ENDFOR
}

void AnonFilter_a3_2197() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[53]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[53]));
	ENDFOR
}

void AnonFilter_a3_2198() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[54]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[54]));
	ENDFOR
}

void AnonFilter_a3_2199() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[55]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[55]));
	ENDFOR
}

void AnonFilter_a3_2200() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[56]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[56]));
	ENDFOR
}

void AnonFilter_a3_2201() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[57]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[57]));
	ENDFOR
}

void AnonFilter_a3_2202() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[58]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[58]));
	ENDFOR
}

void AnonFilter_a3_2203() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[59]), &(SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[59]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2142() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 60, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2143() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 60, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2143Pre_CollapsedDataParallel_1_2132, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_2132() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_2143Pre_CollapsedDataParallel_1_2132), &(Pre_CollapsedDataParallel_1_2132WEIGHTED_ROUND_ROBIN_Splitter_2204));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2206_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_2206() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_2207() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_2208() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_2209() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_2210() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_2211() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_2212() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_2213() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2204() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_2132WEIGHTED_ROUND_ROBIN_Splitter_2204));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2205() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2205Post_CollapsedDataParallel_2_2133, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_2133() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_2205Post_CollapsedDataParallel_2_2133), &(Post_CollapsedDataParallel_2_2133WEIGHTED_ROUND_ROBIN_Splitter_2214));
	ENDFOR
}

void iDCT_1D_reference_fine_2216() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_2217() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_2218() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_2219() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_2220() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_2221() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_2222() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_2223() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2214() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_2133WEIGHTED_ROUND_ROBIN_Splitter_2214));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2215() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2215WEIGHTED_ROUND_ROBIN_Splitter_2224, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_2226() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[0]));
	ENDFOR
}

void AnonFilter_a4_2227() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[1]));
	ENDFOR
}

void AnonFilter_a4_2228() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[2]));
	ENDFOR
}

void AnonFilter_a4_2229() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[3]));
	ENDFOR
}

void AnonFilter_a4_2230() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[4]));
	ENDFOR
}

void AnonFilter_a4_2231() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[5]));
	ENDFOR
}

void AnonFilter_a4_2232() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[6]));
	ENDFOR
}

void AnonFilter_a4_2233() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[7]));
	ENDFOR
}

void AnonFilter_a4_2234() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[8]));
	ENDFOR
}

void AnonFilter_a4_2235() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[9]));
	ENDFOR
}

void AnonFilter_a4_2236() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[10]));
	ENDFOR
}

void AnonFilter_a4_2237() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[11]));
	ENDFOR
}

void AnonFilter_a4_2238() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[12]));
	ENDFOR
}

void AnonFilter_a4_2239() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[13]));
	ENDFOR
}

void AnonFilter_a4_2240() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[14]));
	ENDFOR
}

void AnonFilter_a4_2241() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[15]));
	ENDFOR
}

void AnonFilter_a4_2242() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[16]));
	ENDFOR
}

void AnonFilter_a4_2243() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[17]));
	ENDFOR
}

void AnonFilter_a4_2244() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[18]));
	ENDFOR
}

void AnonFilter_a4_2245() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[19]));
	ENDFOR
}

void AnonFilter_a4_2246() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[20]));
	ENDFOR
}

void AnonFilter_a4_2247() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[21]));
	ENDFOR
}

void AnonFilter_a4_2248() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[22]));
	ENDFOR
}

void AnonFilter_a4_2249() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[23]));
	ENDFOR
}

void AnonFilter_a4_2250() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[24]));
	ENDFOR
}

void AnonFilter_a4_2251() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[25]));
	ENDFOR
}

void AnonFilter_a4_2252() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[26]));
	ENDFOR
}

void AnonFilter_a4_2253() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[27]));
	ENDFOR
}

void AnonFilter_a4_2254() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[28]));
	ENDFOR
}

void AnonFilter_a4_2255() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[29]));
	ENDFOR
}

void AnonFilter_a4_2256() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[30]));
	ENDFOR
}

void AnonFilter_a4_2257() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[31]));
	ENDFOR
}

void AnonFilter_a4_2258() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[32]));
	ENDFOR
}

void AnonFilter_a4_2259() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[33]));
	ENDFOR
}

void AnonFilter_a4_2260() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[34]));
	ENDFOR
}

void AnonFilter_a4_2261() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[35]));
	ENDFOR
}

void AnonFilter_a4_2262() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[36]));
	ENDFOR
}

void AnonFilter_a4_2263() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[37]));
	ENDFOR
}

void AnonFilter_a4_2264() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[38]));
	ENDFOR
}

void AnonFilter_a4_2265() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[39]));
	ENDFOR
}

void AnonFilter_a4_2266() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[40]));
	ENDFOR
}

void AnonFilter_a4_2267() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[41]));
	ENDFOR
}

void AnonFilter_a4_2268() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[42]));
	ENDFOR
}

void AnonFilter_a4_2269() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[43]));
	ENDFOR
}

void AnonFilter_a4_2270() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[44]));
	ENDFOR
}

void AnonFilter_a4_2271() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[45]));
	ENDFOR
}

void AnonFilter_a4_2272() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[46]));
	ENDFOR
}

void AnonFilter_a4_2273() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[47]));
	ENDFOR
}

void AnonFilter_a4_2274() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[48]));
	ENDFOR
}

void AnonFilter_a4_2275() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[49]));
	ENDFOR
}

void AnonFilter_a4_2276() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[50]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[50]));
	ENDFOR
}

void AnonFilter_a4_2277() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[51]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[51]));
	ENDFOR
}

void AnonFilter_a4_2278() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[52]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[52]));
	ENDFOR
}

void AnonFilter_a4_2279() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[53]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[53]));
	ENDFOR
}

void AnonFilter_a4_2280() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[54]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[54]));
	ENDFOR
}

void AnonFilter_a4_2281() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[55]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[55]));
	ENDFOR
}

void AnonFilter_a4_2282() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[56]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[56]));
	ENDFOR
}

void AnonFilter_a4_2283() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[57]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[57]));
	ENDFOR
}

void AnonFilter_a4_2284() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[58]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[58]));
	ENDFOR
}

void AnonFilter_a4_2285() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[59]), &(SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[59]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2224() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 60, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2215WEIGHTED_ROUND_ROBIN_Splitter_2224));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2225() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 60, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_2288() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[0]), &(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_2289() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[1]), &(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_2290() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[2]), &(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_2291() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[3]), &(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_2292() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[4]), &(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_2293() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[5]), &(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_2294() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[6]), &(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_2295() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[7]), &(SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2286() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2287() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_2287iDCT8x8_1D_col_fast_2086, pop_int(&SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_2086_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_2086_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_2086_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_2086_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_2086_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_2086_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_2086_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_2086_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_2086_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_2086_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_2086() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_2287iDCT8x8_1D_col_fast_2086), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_2134() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_2059DUPLICATE_Splitter_2134);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2135() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_2135AnonFilter_a2_2087, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_2087_s.count = (AnonFilter_a2_2087_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_2087_s.errors = (AnonFilter_a2_2087_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_2087_s.errors / AnonFilter_a2_2087_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_2087_s.errors = (AnonFilter_a2_2087_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_2087_s.errors / AnonFilter_a2_2087_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_2087() {
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_2135AnonFilter_a2_2087));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2205Post_CollapsedDataParallel_2_2133);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2215WEIGHTED_ROUND_ROBIN_Splitter_2224);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_int(&SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_join[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_2059DUPLICATE_Splitter_2134);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin144_iDCT8x8_1D_row_fast_Fiss_2301_2308_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_2132WEIGHTED_ROUND_ROBIN_Splitter_2204);
	FOR(int, __iter_init_5_, 0, <, 60, __iter_init_5_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_2300_2307_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_2133WEIGHTED_ROUND_ROBIN_Splitter_2214);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2143Pre_CollapsedDataParallel_1_2132);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_2287iDCT8x8_1D_col_fast_2086);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2298_2305_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 60, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_2300_2307_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 60, __iter_init_8_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_2297_2304_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2060_2136_2296_2303_split[__iter_init_9_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_2135AnonFilter_a2_2087);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2299_2306_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 60, __iter_init_11_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_2297_2304_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_2062
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_2062_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2206
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2206_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2207
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2207_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2208
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2208_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2209
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2209_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2210
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2210_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2211
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2211_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2212
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2212_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2213
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2213_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2216
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2216_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2217
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2217_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2218
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2218_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2219
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2219_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2220
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2220_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2221
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2221_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2222
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2222_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2223
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2223_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_2087
	 {
	AnonFilter_a2_2087_s.count = 0.0 ; 
	AnonFilter_a2_2087_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_2059();
		DUPLICATE_Splitter_2134();
			iDCT_2D_reference_coarse_2062();
			WEIGHTED_ROUND_ROBIN_Splitter_2142();
				AnonFilter_a3_2144();
				AnonFilter_a3_2145();
				AnonFilter_a3_2146();
				AnonFilter_a3_2147();
				AnonFilter_a3_2148();
				AnonFilter_a3_2149();
				AnonFilter_a3_2150();
				AnonFilter_a3_2151();
				AnonFilter_a3_2152();
				AnonFilter_a3_2153();
				AnonFilter_a3_2154();
				AnonFilter_a3_2155();
				AnonFilter_a3_2156();
				AnonFilter_a3_2157();
				AnonFilter_a3_2158();
				AnonFilter_a3_2159();
				AnonFilter_a3_2160();
				AnonFilter_a3_2161();
				AnonFilter_a3_2162();
				AnonFilter_a3_2163();
				AnonFilter_a3_2164();
				AnonFilter_a3_2165();
				AnonFilter_a3_2166();
				AnonFilter_a3_2167();
				AnonFilter_a3_2168();
				AnonFilter_a3_2169();
				AnonFilter_a3_2170();
				AnonFilter_a3_2171();
				AnonFilter_a3_2172();
				AnonFilter_a3_2173();
				AnonFilter_a3_2174();
				AnonFilter_a3_2175();
				AnonFilter_a3_2176();
				AnonFilter_a3_2177();
				AnonFilter_a3_2178();
				AnonFilter_a3_2179();
				AnonFilter_a3_2180();
				AnonFilter_a3_2181();
				AnonFilter_a3_2182();
				AnonFilter_a3_2183();
				AnonFilter_a3_2184();
				AnonFilter_a3_2185();
				AnonFilter_a3_2186();
				AnonFilter_a3_2187();
				AnonFilter_a3_2188();
				AnonFilter_a3_2189();
				AnonFilter_a3_2190();
				AnonFilter_a3_2191();
				AnonFilter_a3_2192();
				AnonFilter_a3_2193();
				AnonFilter_a3_2194();
				AnonFilter_a3_2195();
				AnonFilter_a3_2196();
				AnonFilter_a3_2197();
				AnonFilter_a3_2198();
				AnonFilter_a3_2199();
				AnonFilter_a3_2200();
				AnonFilter_a3_2201();
				AnonFilter_a3_2202();
				AnonFilter_a3_2203();
			WEIGHTED_ROUND_ROBIN_Joiner_2143();
			Pre_CollapsedDataParallel_1_2132();
			WEIGHTED_ROUND_ROBIN_Splitter_2204();
				iDCT_1D_reference_fine_2206();
				iDCT_1D_reference_fine_2207();
				iDCT_1D_reference_fine_2208();
				iDCT_1D_reference_fine_2209();
				iDCT_1D_reference_fine_2210();
				iDCT_1D_reference_fine_2211();
				iDCT_1D_reference_fine_2212();
				iDCT_1D_reference_fine_2213();
			WEIGHTED_ROUND_ROBIN_Joiner_2205();
			Post_CollapsedDataParallel_2_2133();
			WEIGHTED_ROUND_ROBIN_Splitter_2214();
				iDCT_1D_reference_fine_2216();
				iDCT_1D_reference_fine_2217();
				iDCT_1D_reference_fine_2218();
				iDCT_1D_reference_fine_2219();
				iDCT_1D_reference_fine_2220();
				iDCT_1D_reference_fine_2221();
				iDCT_1D_reference_fine_2222();
				iDCT_1D_reference_fine_2223();
			WEIGHTED_ROUND_ROBIN_Joiner_2215();
			WEIGHTED_ROUND_ROBIN_Splitter_2224();
				AnonFilter_a4_2226();
				AnonFilter_a4_2227();
				AnonFilter_a4_2228();
				AnonFilter_a4_2229();
				AnonFilter_a4_2230();
				AnonFilter_a4_2231();
				AnonFilter_a4_2232();
				AnonFilter_a4_2233();
				AnonFilter_a4_2234();
				AnonFilter_a4_2235();
				AnonFilter_a4_2236();
				AnonFilter_a4_2237();
				AnonFilter_a4_2238();
				AnonFilter_a4_2239();
				AnonFilter_a4_2240();
				AnonFilter_a4_2241();
				AnonFilter_a4_2242();
				AnonFilter_a4_2243();
				AnonFilter_a4_2244();
				AnonFilter_a4_2245();
				AnonFilter_a4_2246();
				AnonFilter_a4_2247();
				AnonFilter_a4_2248();
				AnonFilter_a4_2249();
				AnonFilter_a4_2250();
				AnonFilter_a4_2251();
				AnonFilter_a4_2252();
				AnonFilter_a4_2253();
				AnonFilter_a4_2254();
				AnonFilter_a4_2255();
				AnonFilter_a4_2256();
				AnonFilter_a4_2257();
				AnonFilter_a4_2258();
				AnonFilter_a4_2259();
				AnonFilter_a4_2260();
				AnonFilter_a4_2261();
				AnonFilter_a4_2262();
				AnonFilter_a4_2263();
				AnonFilter_a4_2264();
				AnonFilter_a4_2265();
				AnonFilter_a4_2266();
				AnonFilter_a4_2267();
				AnonFilter_a4_2268();
				AnonFilter_a4_2269();
				AnonFilter_a4_2270();
				AnonFilter_a4_2271();
				AnonFilter_a4_2272();
				AnonFilter_a4_2273();
				AnonFilter_a4_2274();
				AnonFilter_a4_2275();
				AnonFilter_a4_2276();
				AnonFilter_a4_2277();
				AnonFilter_a4_2278();
				AnonFilter_a4_2279();
				AnonFilter_a4_2280();
				AnonFilter_a4_2281();
				AnonFilter_a4_2282();
				AnonFilter_a4_2283();
				AnonFilter_a4_2284();
				AnonFilter_a4_2285();
			WEIGHTED_ROUND_ROBIN_Joiner_2225();
			WEIGHTED_ROUND_ROBIN_Splitter_2286();
				iDCT8x8_1D_row_fast_2288();
				iDCT8x8_1D_row_fast_2289();
				iDCT8x8_1D_row_fast_2290();
				iDCT8x8_1D_row_fast_2291();
				iDCT8x8_1D_row_fast_2292();
				iDCT8x8_1D_row_fast_2293();
				iDCT8x8_1D_row_fast_2294();
				iDCT8x8_1D_row_fast_2295();
			WEIGHTED_ROUND_ROBIN_Joiner_2287();
			iDCT8x8_1D_col_fast_2086();
		WEIGHTED_ROUND_ROBIN_Joiner_2135();
		AnonFilter_a2_2087();
	ENDFOR
	return EXIT_SUCCESS;
}
