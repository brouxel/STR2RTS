#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3456 on the compile command line
#else
#if BUF_SIZEMAX < 3456
#error BUF_SIZEMAX too small, it must be at least 3456
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_17464_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_17488_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_17489_t;
void AnonFilter_a0_17461();
void DUPLICATE_Splitter_17536();
void iDCT_2D_reference_coarse_17464();
void WEIGHTED_ROUND_ROBIN_Splitter_17544();
void AnonFilter_a3_17546();
void AnonFilter_a3_17547();
void AnonFilter_a3_17548();
void AnonFilter_a3_17549();
void AnonFilter_a3_17550();
void AnonFilter_a3_17551();
void AnonFilter_a3_17552();
void AnonFilter_a3_17553();
void AnonFilter_a3_17554();
void WEIGHTED_ROUND_ROBIN_Joiner_17545();
void Pre_CollapsedDataParallel_1_17534();
void WEIGHTED_ROUND_ROBIN_Splitter_17555();
void iDCT_1D_reference_fine_17557();
void iDCT_1D_reference_fine_17558();
void iDCT_1D_reference_fine_17559();
void iDCT_1D_reference_fine_17560();
void iDCT_1D_reference_fine_17561();
void iDCT_1D_reference_fine_17562();
void iDCT_1D_reference_fine_17563();
void iDCT_1D_reference_fine_17564();
void WEIGHTED_ROUND_ROBIN_Joiner_17556();
void Post_CollapsedDataParallel_2_17535();
void WEIGHTED_ROUND_ROBIN_Splitter_17565();
void iDCT_1D_reference_fine_17567();
void iDCT_1D_reference_fine_17568();
void iDCT_1D_reference_fine_17569();
void iDCT_1D_reference_fine_17570();
void iDCT_1D_reference_fine_17571();
void iDCT_1D_reference_fine_17572();
void iDCT_1D_reference_fine_17573();
void iDCT_1D_reference_fine_17574();
void WEIGHTED_ROUND_ROBIN_Joiner_17566();
void WEIGHTED_ROUND_ROBIN_Splitter_17575();
void AnonFilter_a4_17577();
void AnonFilter_a4_17578();
void AnonFilter_a4_17579();
void AnonFilter_a4_17580();
void AnonFilter_a4_17581();
void AnonFilter_a4_17582();
void AnonFilter_a4_17583();
void AnonFilter_a4_17584();
void AnonFilter_a4_17585();
void WEIGHTED_ROUND_ROBIN_Joiner_17576();
void WEIGHTED_ROUND_ROBIN_Splitter_17586();
void iDCT8x8_1D_row_fast_17588();
void iDCT8x8_1D_row_fast_17589();
void iDCT8x8_1D_row_fast_17590();
void iDCT8x8_1D_row_fast_17591();
void iDCT8x8_1D_row_fast_17592();
void iDCT8x8_1D_row_fast_17593();
void iDCT8x8_1D_row_fast_17594();
void iDCT8x8_1D_row_fast_17595();
void WEIGHTED_ROUND_ROBIN_Joiner_17587();
void iDCT8x8_1D_col_fast_17488();
void WEIGHTED_ROUND_ROBIN_Joiner_17537();
void AnonFilter_a2_17489();

#ifdef __cplusplus
}
#endif
#endif
