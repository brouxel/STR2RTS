#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=21888 on the compile command line
#else
#if BUF_SIZEMAX < 21888
#error BUF_SIZEMAX too small, it must be at least 21888
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_3256_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_3280_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_3281_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_3253();
void DUPLICATE_Splitter_3328();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_3256();
void WEIGHTED_ROUND_ROBIN_Splitter_3336();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_3338();
void AnonFilter_a3_3339();
void AnonFilter_a3_3340();
void AnonFilter_a3_3341();
void AnonFilter_a3_3342();
void AnonFilter_a3_3343();
void AnonFilter_a3_3344();
void AnonFilter_a3_3345();
void AnonFilter_a3_3346();
void AnonFilter_a3_3347();
void AnonFilter_a3_3348();
void AnonFilter_a3_3349();
void AnonFilter_a3_3350();
void AnonFilter_a3_3351();
void AnonFilter_a3_3352();
void AnonFilter_a3_3353();
void AnonFilter_a3_3354();
void AnonFilter_a3_3355();
void AnonFilter_a3_3356();
void AnonFilter_a3_3357();
void AnonFilter_a3_3358();
void AnonFilter_a3_3359();
void AnonFilter_a3_3360();
void AnonFilter_a3_3361();
void AnonFilter_a3_3362();
void AnonFilter_a3_3363();
void AnonFilter_a3_3364();
void AnonFilter_a3_3365();
void AnonFilter_a3_3366();
void AnonFilter_a3_3367();
void AnonFilter_a3_3368();
void AnonFilter_a3_3369();
void AnonFilter_a3_3370();
void AnonFilter_a3_3371();
void AnonFilter_a3_3372();
void AnonFilter_a3_3373();
void AnonFilter_a3_3374();
void AnonFilter_a3_3375();
void AnonFilter_a3_3376();
void AnonFilter_a3_3377();
void AnonFilter_a3_3378();
void AnonFilter_a3_3379();
void AnonFilter_a3_3380();
void AnonFilter_a3_3381();
void AnonFilter_a3_3382();
void AnonFilter_a3_3383();
void AnonFilter_a3_3384();
void AnonFilter_a3_3385();
void AnonFilter_a3_3386();
void AnonFilter_a3_3387();
void AnonFilter_a3_3388();
void AnonFilter_a3_3389();
void AnonFilter_a3_3390();
void AnonFilter_a3_3391();
void AnonFilter_a3_3392();
void AnonFilter_a3_3393();
void AnonFilter_a3_3394();
void WEIGHTED_ROUND_ROBIN_Joiner_3337();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_3326();
void WEIGHTED_ROUND_ROBIN_Splitter_3395();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_3397();
void iDCT_1D_reference_fine_3398();
void iDCT_1D_reference_fine_3399();
void iDCT_1D_reference_fine_3400();
void iDCT_1D_reference_fine_3401();
void iDCT_1D_reference_fine_3402();
void iDCT_1D_reference_fine_3403();
void iDCT_1D_reference_fine_3404();
void WEIGHTED_ROUND_ROBIN_Joiner_3396();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_3327();
void WEIGHTED_ROUND_ROBIN_Splitter_3405();
void iDCT_1D_reference_fine_3407();
void iDCT_1D_reference_fine_3408();
void iDCT_1D_reference_fine_3409();
void iDCT_1D_reference_fine_3410();
void iDCT_1D_reference_fine_3411();
void iDCT_1D_reference_fine_3412();
void iDCT_1D_reference_fine_3413();
void iDCT_1D_reference_fine_3414();
void WEIGHTED_ROUND_ROBIN_Joiner_3406();
void WEIGHTED_ROUND_ROBIN_Splitter_3415();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_3417();
void AnonFilter_a4_3418();
void AnonFilter_a4_3419();
void AnonFilter_a4_3420();
void AnonFilter_a4_3421();
void AnonFilter_a4_3422();
void AnonFilter_a4_3423();
void AnonFilter_a4_3424();
void AnonFilter_a4_3425();
void AnonFilter_a4_3426();
void AnonFilter_a4_3427();
void AnonFilter_a4_3428();
void AnonFilter_a4_3429();
void AnonFilter_a4_3430();
void AnonFilter_a4_3431();
void AnonFilter_a4_3432();
void AnonFilter_a4_3433();
void AnonFilter_a4_3434();
void AnonFilter_a4_3435();
void AnonFilter_a4_3436();
void AnonFilter_a4_3437();
void AnonFilter_a4_3438();
void AnonFilter_a4_3439();
void AnonFilter_a4_3440();
void AnonFilter_a4_3441();
void AnonFilter_a4_3442();
void AnonFilter_a4_3443();
void AnonFilter_a4_3444();
void AnonFilter_a4_3445();
void AnonFilter_a4_3446();
void AnonFilter_a4_3447();
void AnonFilter_a4_3448();
void AnonFilter_a4_3449();
void AnonFilter_a4_3450();
void AnonFilter_a4_3451();
void AnonFilter_a4_3452();
void AnonFilter_a4_3453();
void AnonFilter_a4_3454();
void AnonFilter_a4_3455();
void AnonFilter_a4_3456();
void AnonFilter_a4_3457();
void AnonFilter_a4_3458();
void AnonFilter_a4_3459();
void AnonFilter_a4_3460();
void AnonFilter_a4_3461();
void AnonFilter_a4_3462();
void AnonFilter_a4_3463();
void AnonFilter_a4_3464();
void AnonFilter_a4_3465();
void AnonFilter_a4_3466();
void AnonFilter_a4_3467();
void AnonFilter_a4_3468();
void AnonFilter_a4_3469();
void AnonFilter_a4_3470();
void AnonFilter_a4_3471();
void AnonFilter_a4_3472();
void AnonFilter_a4_3473();
void WEIGHTED_ROUND_ROBIN_Joiner_3416();
void WEIGHTED_ROUND_ROBIN_Splitter_3474();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_3476();
void iDCT8x8_1D_row_fast_3477();
void iDCT8x8_1D_row_fast_3478();
void iDCT8x8_1D_row_fast_3479();
void iDCT8x8_1D_row_fast_3480();
void iDCT8x8_1D_row_fast_3481();
void iDCT8x8_1D_row_fast_3482();
void iDCT8x8_1D_row_fast_3483();
void WEIGHTED_ROUND_ROBIN_Joiner_3475();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_3280();
void WEIGHTED_ROUND_ROBIN_Joiner_3329();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_3281();

#ifdef __cplusplus
}
#endif
#endif
