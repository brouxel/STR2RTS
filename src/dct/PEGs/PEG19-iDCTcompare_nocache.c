#include "PEG19-iDCTcompare_nocache.h"

buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15366Post_CollapsedDataParallel_2_15335;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_15337AnonFilter_a2_15289;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[19];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_15334WEIGHTED_ROUND_ROBIN_Splitter_15365;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[19];
buffer_float_t Post_CollapsedDataParallel_2_15335WEIGHTED_ROUND_ROBIN_Splitter_15375;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[19];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15345Pre_CollapsedDataParallel_1_15334;
buffer_int_t AnonFilter_a0_15261DUPLICATE_Splitter_15336;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[19];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15376WEIGHTED_ROUND_ROBIN_Splitter_15385;
buffer_int_t SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[8];
buffer_int_t SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[8];


iDCT_2D_reference_coarse_15264_t iDCT_2D_reference_coarse_15264_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15367_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15368_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15369_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15370_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15371_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15372_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15373_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15374_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15377_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15378_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15379_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15380_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15381_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15382_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15383_s;
iDCT_2D_reference_coarse_15264_t iDCT_1D_reference_fine_15384_s;
iDCT8x8_1D_col_fast_15288_t iDCT8x8_1D_col_fast_15288_s;
AnonFilter_a2_15289_t AnonFilter_a2_15289_s;

void AnonFilter_a0_15261(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_15261DUPLICATE_Splitter_15336, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_15264(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_15264_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_15264_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_15346(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15347(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15348(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15349(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15350(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15351(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15352(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15353(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15354(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15355(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15356(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15357(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15358(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15359(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15360(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15361(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15362(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15363(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_15364(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[18])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15344() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15345() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15345Pre_CollapsedDataParallel_1_15334, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_15334(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_15334WEIGHTED_ROUND_ROBIN_Splitter_15365, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_15345Pre_CollapsedDataParallel_1_15334, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_15345Pre_CollapsedDataParallel_1_15334) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15367(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15367_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15368(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15368_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15369(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15369_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15370(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15370_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15371(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15371_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15372(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15372_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15373(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15373_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15374(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15374_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15365() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_15334WEIGHTED_ROUND_ROBIN_Splitter_15365));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15366() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15366Post_CollapsedDataParallel_2_15335, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_15335(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_15335WEIGHTED_ROUND_ROBIN_Splitter_15375, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_15366Post_CollapsedDataParallel_2_15335, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_15366Post_CollapsedDataParallel_2_15335) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15377(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15377_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15378(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15378_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15379(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15379_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15380(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15380_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15381(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15381_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15382(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15382_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15383(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15383_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_15384(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15384_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_15335WEIGHTED_ROUND_ROBIN_Splitter_15375));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15376() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15376WEIGHTED_ROUND_ROBIN_Splitter_15385, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_15387(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15388(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15389(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15390(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15391(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15392(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15393(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15394(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15395(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15396(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15397(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15398(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15399(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15400(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15401(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15402(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15403(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15404(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_15405(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_15376WEIGHTED_ROUND_ROBIN_Splitter_15385));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15386() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_15408(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0], 6) ; 
		x3 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0], 2) ; 
		x4 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0], 1) ; 
		x5 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0], 7) ; 
		x6 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0], 5) ; 
		x7 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_15409(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1], 6) ; 
		x3 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1], 2) ; 
		x4 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1], 1) ; 
		x5 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1], 7) ; 
		x6 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1], 5) ; 
		x7 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_15410(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2], 6) ; 
		x3 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2], 2) ; 
		x4 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2], 1) ; 
		x5 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2], 7) ; 
		x6 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2], 5) ; 
		x7 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_15411(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3], 6) ; 
		x3 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3], 2) ; 
		x4 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3], 1) ; 
		x5 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3], 7) ; 
		x6 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3], 5) ; 
		x7 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_15412(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4], 6) ; 
		x3 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4], 2) ; 
		x4 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4], 1) ; 
		x5 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4], 7) ; 
		x6 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4], 5) ; 
		x7 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_15413(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5], 6) ; 
		x3 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5], 2) ; 
		x4 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5], 1) ; 
		x5 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5], 7) ; 
		x6 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5], 5) ; 
		x7 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_15414(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6], 6) ; 
		x3 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6], 2) ; 
		x4 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6], 1) ; 
		x5 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6], 7) ; 
		x6 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6], 5) ; 
		x7 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_15415(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7], 6) ; 
		x3 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7], 2) ; 
		x4 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7], 1) ; 
		x5 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7], 7) ; 
		x6 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7], 5) ; 
		x7 = peek_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15406() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15407() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, pop_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_15288(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_15288_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_15288_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_15288_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_15288_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_15288_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_15288_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_15288_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_15288_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_15288_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_join[2], iDCT8x8_1D_col_fast_15288_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_15336() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_15261DUPLICATE_Splitter_15336);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15337() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_15337AnonFilter_a2_15289, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_15289(){
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_15337AnonFilter_a2_15289) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_15337AnonFilter_a2_15289) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_15337AnonFilter_a2_15289) ; 
		AnonFilter_a2_15289_s.count = (AnonFilter_a2_15289_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_15289_s.errors = (AnonFilter_a2_15289_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15289_s.errors / AnonFilter_a2_15289_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_15289_s.errors = (AnonFilter_a2_15289_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15289_s.errors / AnonFilter_a2_15289_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15366Post_CollapsedDataParallel_2_15335);
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_join[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_15337AnonFilter_a2_15289);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 19, __iter_init_3_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15418_15425_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_15334WEIGHTED_ROUND_ROBIN_Splitter_15365);
	FOR(int, __iter_init_5_, 0, <, 19, __iter_init_5_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_15335WEIGHTED_ROUND_ROBIN_Splitter_15375);
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15262_15338_15416_15423_split[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_15407iDCT8x8_1D_col_fast_15288);
	FOR(int, __iter_init_7_, 0, <, 19, __iter_init_7_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_15417_15424_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15345Pre_CollapsedDataParallel_1_15334);
	init_buffer_int(&AnonFilter_a0_15261DUPLICATE_Splitter_15336);
	FOR(int, __iter_init_8_, 0, <, 19, __iter_init_8_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_15420_15427_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15376WEIGHTED_ROUND_ROBIN_Splitter_15385);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15419_15426_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin62_iDCT8x8_1D_row_fast_Fiss_15421_15428_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_15264
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_15264_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15367
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15367_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15368
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15368_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15369
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15369_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15370
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15370_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15371
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15371_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15372
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15372_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15373
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15373_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15374
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15374_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15377
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15377_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15378
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15378_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15379
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15379_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15380
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15380_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15381
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15381_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15382
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15382_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15383
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15383_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15384
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15384_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_15289
	 {
	AnonFilter_a2_15289_s.count = 0.0 ; 
	AnonFilter_a2_15289_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_15261();
		DUPLICATE_Splitter_15336();
			iDCT_2D_reference_coarse_15264();
			WEIGHTED_ROUND_ROBIN_Splitter_15344();
				AnonFilter_a3_15346();
				AnonFilter_a3_15347();
				AnonFilter_a3_15348();
				AnonFilter_a3_15349();
				AnonFilter_a3_15350();
				AnonFilter_a3_15351();
				AnonFilter_a3_15352();
				AnonFilter_a3_15353();
				AnonFilter_a3_15354();
				AnonFilter_a3_15355();
				AnonFilter_a3_15356();
				AnonFilter_a3_15357();
				AnonFilter_a3_15358();
				AnonFilter_a3_15359();
				AnonFilter_a3_15360();
				AnonFilter_a3_15361();
				AnonFilter_a3_15362();
				AnonFilter_a3_15363();
				AnonFilter_a3_15364();
			WEIGHTED_ROUND_ROBIN_Joiner_15345();
			Pre_CollapsedDataParallel_1_15334();
			WEIGHTED_ROUND_ROBIN_Splitter_15365();
				iDCT_1D_reference_fine_15367();
				iDCT_1D_reference_fine_15368();
				iDCT_1D_reference_fine_15369();
				iDCT_1D_reference_fine_15370();
				iDCT_1D_reference_fine_15371();
				iDCT_1D_reference_fine_15372();
				iDCT_1D_reference_fine_15373();
				iDCT_1D_reference_fine_15374();
			WEIGHTED_ROUND_ROBIN_Joiner_15366();
			Post_CollapsedDataParallel_2_15335();
			WEIGHTED_ROUND_ROBIN_Splitter_15375();
				iDCT_1D_reference_fine_15377();
				iDCT_1D_reference_fine_15378();
				iDCT_1D_reference_fine_15379();
				iDCT_1D_reference_fine_15380();
				iDCT_1D_reference_fine_15381();
				iDCT_1D_reference_fine_15382();
				iDCT_1D_reference_fine_15383();
				iDCT_1D_reference_fine_15384();
			WEIGHTED_ROUND_ROBIN_Joiner_15376();
			WEIGHTED_ROUND_ROBIN_Splitter_15385();
				AnonFilter_a4_15387();
				AnonFilter_a4_15388();
				AnonFilter_a4_15389();
				AnonFilter_a4_15390();
				AnonFilter_a4_15391();
				AnonFilter_a4_15392();
				AnonFilter_a4_15393();
				AnonFilter_a4_15394();
				AnonFilter_a4_15395();
				AnonFilter_a4_15396();
				AnonFilter_a4_15397();
				AnonFilter_a4_15398();
				AnonFilter_a4_15399();
				AnonFilter_a4_15400();
				AnonFilter_a4_15401();
				AnonFilter_a4_15402();
				AnonFilter_a4_15403();
				AnonFilter_a4_15404();
				AnonFilter_a4_15405();
			WEIGHTED_ROUND_ROBIN_Joiner_15386();
			WEIGHTED_ROUND_ROBIN_Splitter_15406();
				iDCT8x8_1D_row_fast_15408();
				iDCT8x8_1D_row_fast_15409();
				iDCT8x8_1D_row_fast_15410();
				iDCT8x8_1D_row_fast_15411();
				iDCT8x8_1D_row_fast_15412();
				iDCT8x8_1D_row_fast_15413();
				iDCT8x8_1D_row_fast_15414();
				iDCT8x8_1D_row_fast_15415();
			WEIGHTED_ROUND_ROBIN_Joiner_15407();
			iDCT8x8_1D_col_fast_15288();
		WEIGHTED_ROUND_ROBIN_Joiner_15337();
		AnonFilter_a2_15289();
	ENDFOR
	return EXIT_SUCCESS;
}
