#include "PEG5-iDCTcompare.h"

buffer_float_t Pre_CollapsedDataParallel_1_18284WEIGHTED_ROUND_ROBIN_Splitter_18301;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_split[5];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18302Post_CollapsedDataParallel_2_18285;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_18333_18340_join[5];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_18323iDCT8x8_1D_col_fast_18238;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18309WEIGHTED_ROUND_ROBIN_Splitter_18315;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_18295Pre_CollapsedDataParallel_1_18284;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_split[5];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_join[5];
buffer_int_t AnonFilter_a0_18211DUPLICATE_Splitter_18286;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_split[3];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_18330_18337_join[5];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_join[5];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_18287AnonFilter_a2_18239;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_18333_18340_split[5];
buffer_int_t SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_split[5];
buffer_int_t SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_join[5];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_join[3];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_18330_18337_split[5];
buffer_float_t Post_CollapsedDataParallel_2_18285WEIGHTED_ROUND_ROBIN_Splitter_18308;


iDCT_2D_reference_coarse_18214_t iDCT_2D_reference_coarse_18214_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18303_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18304_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18305_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18306_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18307_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18310_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18311_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18312_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18313_s;
iDCT_2D_reference_coarse_18214_t iDCT_1D_reference_fine_18314_s;
iDCT8x8_1D_col_fast_18238_t iDCT8x8_1D_col_fast_18238_s;
AnonFilter_a2_18239_t AnonFilter_a2_18239_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_18211() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_18211DUPLICATE_Splitter_18286));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_18214_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_18214_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_18214() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_18296() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_join[0]));
	ENDFOR
}

void AnonFilter_a3_18297() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_join[1]));
	ENDFOR
}

void AnonFilter_a3_18298() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_join[2]));
	ENDFOR
}

void AnonFilter_a3_18299() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_join[3]));
	ENDFOR
}

void AnonFilter_a3_18300() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_18330_18337_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18294() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 5, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_18330_18337_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18295() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 5, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18295Pre_CollapsedDataParallel_1_18284, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_18330_18337_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_18284() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_18295Pre_CollapsedDataParallel_1_18284), &(Pre_CollapsedDataParallel_1_18284WEIGHTED_ROUND_ROBIN_Splitter_18301));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_18303_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_18303() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_18304() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_18305() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_18306() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_18307() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18301() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_18284WEIGHTED_ROUND_ROBIN_Splitter_18301));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18302() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18302Post_CollapsedDataParallel_2_18285, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_18285() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_18302Post_CollapsedDataParallel_2_18285), &(Post_CollapsedDataParallel_2_18285WEIGHTED_ROUND_ROBIN_Splitter_18308));
	ENDFOR
}

void iDCT_1D_reference_fine_18310() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_18311() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_18312() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_18313() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_18314() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18308() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_18285WEIGHTED_ROUND_ROBIN_Splitter_18308));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18309() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_18309WEIGHTED_ROUND_ROBIN_Splitter_18315, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_18317() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_join[0]));
	ENDFOR
}

void AnonFilter_a4_18318() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_join[1]));
	ENDFOR
}

void AnonFilter_a4_18319() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_join[2]));
	ENDFOR
}

void AnonFilter_a4_18320() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_join[3]));
	ENDFOR
}

void AnonFilter_a4_18321() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_18333_18340_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18315() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 5, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_18333_18340_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_18309WEIGHTED_ROUND_ROBIN_Splitter_18315));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18316() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 5, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_18333_18340_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_18324() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_split[0]), &(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_18325() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_split[1]), &(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_18326() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_split[2]), &(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_18327() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_split[3]), &(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_18328() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_split[4]), &(SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_18322() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18323() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_18323iDCT8x8_1D_col_fast_18238, pop_int(&SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_18238_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_18238_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_18238_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_18238_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_18238_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_18238_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_18238_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_18238_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_18238_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_18238_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_18238() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_18323iDCT8x8_1D_col_fast_18238), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_18286() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_18211DUPLICATE_Splitter_18286);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_18287() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_18287AnonFilter_a2_18239, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_18239_s.count = (AnonFilter_a2_18239_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_18239_s.errors = (AnonFilter_a2_18239_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_18239_s.errors / AnonFilter_a2_18239_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_18239_s.errors = (AnonFilter_a2_18239_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_18239_s.errors / AnonFilter_a2_18239_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_18239() {
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_18287AnonFilter_a2_18239));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Pre_CollapsedDataParallel_1_18284WEIGHTED_ROUND_ROBIN_Splitter_18301);
	FOR(int, __iter_init_0_, 0, <, 5, __iter_init_0_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18302Post_CollapsedDataParallel_2_18285);
	FOR(int, __iter_init_1_, 0, <, 5, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_18333_18340_join[__iter_init_1_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_18323iDCT8x8_1D_col_fast_18238);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18309WEIGHTED_ROUND_ROBIN_Splitter_18315);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_18295Pre_CollapsedDataParallel_1_18284);
	FOR(int, __iter_init_2_, 0, <, 5, __iter_init_2_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 5, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_18331_18338_join[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_18211DUPLICATE_Splitter_18286);
	FOR(int, __iter_init_4_, 0, <, 3, __iter_init_4_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 5, __iter_init_5_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_18330_18337_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 5, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_18332_18339_join[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_18287AnonFilter_a2_18239);
	FOR(int, __iter_init_7_, 0, <, 5, __iter_init_7_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_18333_18340_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 5, __iter_init_8_++)
		init_buffer_int(&SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 5, __iter_init_9_++)
		init_buffer_int(&SplitJoin28_iDCT8x8_1D_row_fast_Fiss_18334_18341_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_18212_18288_18329_18336_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 5, __iter_init_11_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_18330_18337_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_18285WEIGHTED_ROUND_ROBIN_Splitter_18308);
// --- init: iDCT_2D_reference_coarse_18214
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_18214_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18303
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18303_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18304
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18304_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18305
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18305_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18306
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18306_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18307
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18307_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18310
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18310_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18311
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18311_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18312
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18312_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18313
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18313_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_18314
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_18314_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_18239
	 {
	AnonFilter_a2_18239_s.count = 0.0 ; 
	AnonFilter_a2_18239_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_18211();
		DUPLICATE_Splitter_18286();
			iDCT_2D_reference_coarse_18214();
			WEIGHTED_ROUND_ROBIN_Splitter_18294();
				AnonFilter_a3_18296();
				AnonFilter_a3_18297();
				AnonFilter_a3_18298();
				AnonFilter_a3_18299();
				AnonFilter_a3_18300();
			WEIGHTED_ROUND_ROBIN_Joiner_18295();
			Pre_CollapsedDataParallel_1_18284();
			WEIGHTED_ROUND_ROBIN_Splitter_18301();
				iDCT_1D_reference_fine_18303();
				iDCT_1D_reference_fine_18304();
				iDCT_1D_reference_fine_18305();
				iDCT_1D_reference_fine_18306();
				iDCT_1D_reference_fine_18307();
			WEIGHTED_ROUND_ROBIN_Joiner_18302();
			Post_CollapsedDataParallel_2_18285();
			WEIGHTED_ROUND_ROBIN_Splitter_18308();
				iDCT_1D_reference_fine_18310();
				iDCT_1D_reference_fine_18311();
				iDCT_1D_reference_fine_18312();
				iDCT_1D_reference_fine_18313();
				iDCT_1D_reference_fine_18314();
			WEIGHTED_ROUND_ROBIN_Joiner_18309();
			WEIGHTED_ROUND_ROBIN_Splitter_18315();
				AnonFilter_a4_18317();
				AnonFilter_a4_18318();
				AnonFilter_a4_18319();
				AnonFilter_a4_18320();
				AnonFilter_a4_18321();
			WEIGHTED_ROUND_ROBIN_Joiner_18316();
			WEIGHTED_ROUND_ROBIN_Splitter_18322();
				iDCT8x8_1D_row_fast_18324();
				iDCT8x8_1D_row_fast_18325();
				iDCT8x8_1D_row_fast_18326();
				iDCT8x8_1D_row_fast_18327();
				iDCT8x8_1D_row_fast_18328();
			WEIGHTED_ROUND_ROBIN_Joiner_18323();
			iDCT8x8_1D_col_fast_18238();
		WEIGHTED_ROUND_ROBIN_Joiner_18287();
		AnonFilter_a2_18239();
	ENDFOR
	return EXIT_SUCCESS;
}
