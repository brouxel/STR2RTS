#include "PEG34-iDCTcompare.h"

buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_11387iDCT8x8_1D_col_fast_11238;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11295Pre_CollapsedDataParallel_1_11284;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[34];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[8];
buffer_float_t Post_CollapsedDataParallel_2_11285WEIGHTED_ROUND_ROBIN_Splitter_11340;
buffer_float_t Pre_CollapsedDataParallel_1_11284WEIGHTED_ROUND_ROBIN_Splitter_11330;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[34];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[34];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11341WEIGHTED_ROUND_ROBIN_Splitter_11350;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[34];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_join[3];
buffer_int_t SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[8];
buffer_int_t SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_11287AnonFilter_a2_11239;
buffer_int_t AnonFilter_a0_11211DUPLICATE_Splitter_11286;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11331Post_CollapsedDataParallel_2_11285;


iDCT_2D_reference_coarse_11214_t iDCT_2D_reference_coarse_11214_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11332_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11333_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11334_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11335_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11336_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11337_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11338_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11339_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11342_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11343_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11344_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11345_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11346_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11347_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11348_s;
iDCT_2D_reference_coarse_11214_t iDCT_1D_reference_fine_11349_s;
iDCT8x8_1D_col_fast_11238_t iDCT8x8_1D_col_fast_11238_s;
AnonFilter_a2_11239_t AnonFilter_a2_11239_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_11211() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_11211DUPLICATE_Splitter_11286));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_11214_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_11214_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_11214() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_11296() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[0]));
	ENDFOR
}

void AnonFilter_a3_11297() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[1]));
	ENDFOR
}

void AnonFilter_a3_11298() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[2]));
	ENDFOR
}

void AnonFilter_a3_11299() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[3]));
	ENDFOR
}

void AnonFilter_a3_11300() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[4]));
	ENDFOR
}

void AnonFilter_a3_11301() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[5]));
	ENDFOR
}

void AnonFilter_a3_11302() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[6]));
	ENDFOR
}

void AnonFilter_a3_11303() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[7]));
	ENDFOR
}

void AnonFilter_a3_11304() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[8]));
	ENDFOR
}

void AnonFilter_a3_11305() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[9]));
	ENDFOR
}

void AnonFilter_a3_11306() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[10]));
	ENDFOR
}

void AnonFilter_a3_11307() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[11]));
	ENDFOR
}

void AnonFilter_a3_11308() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[12]));
	ENDFOR
}

void AnonFilter_a3_11309() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[13]));
	ENDFOR
}

void AnonFilter_a3_11310() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[14]));
	ENDFOR
}

void AnonFilter_a3_11311() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[15]));
	ENDFOR
}

void AnonFilter_a3_11312() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[16]));
	ENDFOR
}

void AnonFilter_a3_11313() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[17]));
	ENDFOR
}

void AnonFilter_a3_11314() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[18]));
	ENDFOR
}

void AnonFilter_a3_11315() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[19]));
	ENDFOR
}

void AnonFilter_a3_11316() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[20]));
	ENDFOR
}

void AnonFilter_a3_11317() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[21]));
	ENDFOR
}

void AnonFilter_a3_11318() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[22]));
	ENDFOR
}

void AnonFilter_a3_11319() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[23]));
	ENDFOR
}

void AnonFilter_a3_11320() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[24]));
	ENDFOR
}

void AnonFilter_a3_11321() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[25]));
	ENDFOR
}

void AnonFilter_a3_11322() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[26]));
	ENDFOR
}

void AnonFilter_a3_11323() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[27]));
	ENDFOR
}

void AnonFilter_a3_11324() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[28]));
	ENDFOR
}

void AnonFilter_a3_11325() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[29]));
	ENDFOR
}

void AnonFilter_a3_11326() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[30]));
	ENDFOR
}

void AnonFilter_a3_11327() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[31]));
	ENDFOR
}

void AnonFilter_a3_11328() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[32]));
	ENDFOR
}

void AnonFilter_a3_11329() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[33]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11294() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 34, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11295() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 34, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11295Pre_CollapsedDataParallel_1_11284, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_11284() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_11295Pre_CollapsedDataParallel_1_11284), &(Pre_CollapsedDataParallel_1_11284WEIGHTED_ROUND_ROBIN_Splitter_11330));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_11332_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_11332() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_11333() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_11334() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_11335() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_11336() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_11337() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_11338() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_11339() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11330() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11284WEIGHTED_ROUND_ROBIN_Splitter_11330));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11331() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11331Post_CollapsedDataParallel_2_11285, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_11285() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11331Post_CollapsedDataParallel_2_11285), &(Post_CollapsedDataParallel_2_11285WEIGHTED_ROUND_ROBIN_Splitter_11340));
	ENDFOR
}

void iDCT_1D_reference_fine_11342() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_11343() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_11344() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_11345() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_11346() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_11347() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_11348() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_11349() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11340() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_11285WEIGHTED_ROUND_ROBIN_Splitter_11340));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11341() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11341WEIGHTED_ROUND_ROBIN_Splitter_11350, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_11352() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[0]));
	ENDFOR
}

void AnonFilter_a4_11353() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[1]));
	ENDFOR
}

void AnonFilter_a4_11354() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[2]));
	ENDFOR
}

void AnonFilter_a4_11355() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[3]));
	ENDFOR
}

void AnonFilter_a4_11356() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[4]));
	ENDFOR
}

void AnonFilter_a4_11357() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[5]));
	ENDFOR
}

void AnonFilter_a4_11358() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[6]));
	ENDFOR
}

void AnonFilter_a4_11359() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[7]));
	ENDFOR
}

void AnonFilter_a4_11360() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[8]));
	ENDFOR
}

void AnonFilter_a4_11361() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[9]));
	ENDFOR
}

void AnonFilter_a4_11362() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[10]));
	ENDFOR
}

void AnonFilter_a4_11363() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[11]));
	ENDFOR
}

void AnonFilter_a4_11364() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[12]));
	ENDFOR
}

void AnonFilter_a4_11365() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[13]));
	ENDFOR
}

void AnonFilter_a4_11366() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[14]));
	ENDFOR
}

void AnonFilter_a4_11367() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[15]));
	ENDFOR
}

void AnonFilter_a4_11368() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[16]));
	ENDFOR
}

void AnonFilter_a4_11369() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[17]));
	ENDFOR
}

void AnonFilter_a4_11370() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[18]));
	ENDFOR
}

void AnonFilter_a4_11371() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[19]));
	ENDFOR
}

void AnonFilter_a4_11372() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[20]));
	ENDFOR
}

void AnonFilter_a4_11373() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[21]));
	ENDFOR
}

void AnonFilter_a4_11374() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[22]));
	ENDFOR
}

void AnonFilter_a4_11375() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[23]));
	ENDFOR
}

void AnonFilter_a4_11376() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[24]));
	ENDFOR
}

void AnonFilter_a4_11377() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[25]));
	ENDFOR
}

void AnonFilter_a4_11378() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[26]));
	ENDFOR
}

void AnonFilter_a4_11379() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[27]));
	ENDFOR
}

void AnonFilter_a4_11380() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[28]));
	ENDFOR
}

void AnonFilter_a4_11381() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[29]));
	ENDFOR
}

void AnonFilter_a4_11382() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[30]));
	ENDFOR
}

void AnonFilter_a4_11383() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[31]));
	ENDFOR
}

void AnonFilter_a4_11384() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[32]));
	ENDFOR
}

void AnonFilter_a4_11385() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[33]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11350() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 34, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11341WEIGHTED_ROUND_ROBIN_Splitter_11350));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11351() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 34, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_11388() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[0]), &(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11389() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[1]), &(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11390() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[2]), &(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11391() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[3]), &(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11392() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[4]), &(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11393() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[5]), &(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11394() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[6]), &(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11395() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[7]), &(SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11386() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_11387iDCT8x8_1D_col_fast_11238, pop_int(&SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_11238_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_11238_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_11238_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_11238_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_11238_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_11238_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_11238_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_11238_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_11238_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_11238_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_11238() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_11387iDCT8x8_1D_col_fast_11238), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_11286() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_11211DUPLICATE_Splitter_11286);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11287() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_11287AnonFilter_a2_11239, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_11239_s.count = (AnonFilter_a2_11239_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_11239_s.errors = (AnonFilter_a2_11239_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_11239_s.errors / AnonFilter_a2_11239_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_11239_s.errors = (AnonFilter_a2_11239_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_11239_s.errors / AnonFilter_a2_11239_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_11239() {
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_11287AnonFilter_a2_11239));
	ENDFOR
}

void __stream_init__() {
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_11387iDCT8x8_1D_col_fast_11238);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11295Pre_CollapsedDataParallel_1_11284);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 34, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_11400_11407_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11398_11405_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_11285WEIGHTED_ROUND_ROBIN_Splitter_11340);
	init_buffer_float(&Pre_CollapsedDataParallel_1_11284WEIGHTED_ROUND_ROBIN_Splitter_11330);
	FOR(int, __iter_init_4_, 0, <, 34, __iter_init_4_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_11397_11404_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 34, __iter_init_5_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_11397_11404_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11341WEIGHTED_ROUND_ROBIN_Splitter_11350);
	FOR(int, __iter_init_6_, 0, <, 34, __iter_init_6_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_11400_11407_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11399_11406_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_int(&SplitJoin92_iDCT8x8_1D_row_fast_Fiss_11401_11408_join[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_11287AnonFilter_a2_11239);
	init_buffer_int(&AnonFilter_a0_11211DUPLICATE_Splitter_11286);
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_11212_11288_11396_11403_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11331Post_CollapsedDataParallel_2_11285);
// --- init: iDCT_2D_reference_coarse_11214
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_11214_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11332
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11332_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11333
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11333_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11334
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11334_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11335
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11335_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11336
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11336_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11337
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11337_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11338
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11338_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11339
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11339_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11342
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11342_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11343
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11343_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11344
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11344_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11345
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11345_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11346
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11346_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11347
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11347_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11348
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11348_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11349
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11349_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_11239
	 {
	AnonFilter_a2_11239_s.count = 0.0 ; 
	AnonFilter_a2_11239_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_11211();
		DUPLICATE_Splitter_11286();
			iDCT_2D_reference_coarse_11214();
			WEIGHTED_ROUND_ROBIN_Splitter_11294();
				AnonFilter_a3_11296();
				AnonFilter_a3_11297();
				AnonFilter_a3_11298();
				AnonFilter_a3_11299();
				AnonFilter_a3_11300();
				AnonFilter_a3_11301();
				AnonFilter_a3_11302();
				AnonFilter_a3_11303();
				AnonFilter_a3_11304();
				AnonFilter_a3_11305();
				AnonFilter_a3_11306();
				AnonFilter_a3_11307();
				AnonFilter_a3_11308();
				AnonFilter_a3_11309();
				AnonFilter_a3_11310();
				AnonFilter_a3_11311();
				AnonFilter_a3_11312();
				AnonFilter_a3_11313();
				AnonFilter_a3_11314();
				AnonFilter_a3_11315();
				AnonFilter_a3_11316();
				AnonFilter_a3_11317();
				AnonFilter_a3_11318();
				AnonFilter_a3_11319();
				AnonFilter_a3_11320();
				AnonFilter_a3_11321();
				AnonFilter_a3_11322();
				AnonFilter_a3_11323();
				AnonFilter_a3_11324();
				AnonFilter_a3_11325();
				AnonFilter_a3_11326();
				AnonFilter_a3_11327();
				AnonFilter_a3_11328();
				AnonFilter_a3_11329();
			WEIGHTED_ROUND_ROBIN_Joiner_11295();
			Pre_CollapsedDataParallel_1_11284();
			WEIGHTED_ROUND_ROBIN_Splitter_11330();
				iDCT_1D_reference_fine_11332();
				iDCT_1D_reference_fine_11333();
				iDCT_1D_reference_fine_11334();
				iDCT_1D_reference_fine_11335();
				iDCT_1D_reference_fine_11336();
				iDCT_1D_reference_fine_11337();
				iDCT_1D_reference_fine_11338();
				iDCT_1D_reference_fine_11339();
			WEIGHTED_ROUND_ROBIN_Joiner_11331();
			Post_CollapsedDataParallel_2_11285();
			WEIGHTED_ROUND_ROBIN_Splitter_11340();
				iDCT_1D_reference_fine_11342();
				iDCT_1D_reference_fine_11343();
				iDCT_1D_reference_fine_11344();
				iDCT_1D_reference_fine_11345();
				iDCT_1D_reference_fine_11346();
				iDCT_1D_reference_fine_11347();
				iDCT_1D_reference_fine_11348();
				iDCT_1D_reference_fine_11349();
			WEIGHTED_ROUND_ROBIN_Joiner_11341();
			WEIGHTED_ROUND_ROBIN_Splitter_11350();
				AnonFilter_a4_11352();
				AnonFilter_a4_11353();
				AnonFilter_a4_11354();
				AnonFilter_a4_11355();
				AnonFilter_a4_11356();
				AnonFilter_a4_11357();
				AnonFilter_a4_11358();
				AnonFilter_a4_11359();
				AnonFilter_a4_11360();
				AnonFilter_a4_11361();
				AnonFilter_a4_11362();
				AnonFilter_a4_11363();
				AnonFilter_a4_11364();
				AnonFilter_a4_11365();
				AnonFilter_a4_11366();
				AnonFilter_a4_11367();
				AnonFilter_a4_11368();
				AnonFilter_a4_11369();
				AnonFilter_a4_11370();
				AnonFilter_a4_11371();
				AnonFilter_a4_11372();
				AnonFilter_a4_11373();
				AnonFilter_a4_11374();
				AnonFilter_a4_11375();
				AnonFilter_a4_11376();
				AnonFilter_a4_11377();
				AnonFilter_a4_11378();
				AnonFilter_a4_11379();
				AnonFilter_a4_11380();
				AnonFilter_a4_11381();
				AnonFilter_a4_11382();
				AnonFilter_a4_11383();
				AnonFilter_a4_11384();
				AnonFilter_a4_11385();
			WEIGHTED_ROUND_ROBIN_Joiner_11351();
			WEIGHTED_ROUND_ROBIN_Splitter_11386();
				iDCT8x8_1D_row_fast_11388();
				iDCT8x8_1D_row_fast_11389();
				iDCT8x8_1D_row_fast_11390();
				iDCT8x8_1D_row_fast_11391();
				iDCT8x8_1D_row_fast_11392();
				iDCT8x8_1D_row_fast_11393();
				iDCT8x8_1D_row_fast_11394();
				iDCT8x8_1D_row_fast_11395();
			WEIGHTED_ROUND_ROBIN_Joiner_11387();
			iDCT8x8_1D_col_fast_11238();
		WEIGHTED_ROUND_ROBIN_Joiner_11287();
		AnonFilter_a2_11239();
	ENDFOR
	return EXIT_SUCCESS;
}
