#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=24192 on the compile command line
#else
#if BUF_SIZEMAX < 24192
#error BUF_SIZEMAX too small, it must be at least 24192
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_832_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_856_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_857_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_829();
void DUPLICATE_Splitter_904();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_832();
void WEIGHTED_ROUND_ROBIN_Splitter_912();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_914();
void AnonFilter_a3_915();
void AnonFilter_a3_916();
void AnonFilter_a3_917();
void AnonFilter_a3_918();
void AnonFilter_a3_919();
void AnonFilter_a3_920();
void AnonFilter_a3_921();
void AnonFilter_a3_922();
void AnonFilter_a3_923();
void AnonFilter_a3_924();
void AnonFilter_a3_925();
void AnonFilter_a3_926();
void AnonFilter_a3_927();
void AnonFilter_a3_928();
void AnonFilter_a3_929();
void AnonFilter_a3_930();
void AnonFilter_a3_931();
void AnonFilter_a3_932();
void AnonFilter_a3_933();
void AnonFilter_a3_934();
void AnonFilter_a3_935();
void AnonFilter_a3_936();
void AnonFilter_a3_937();
void AnonFilter_a3_938();
void AnonFilter_a3_939();
void AnonFilter_a3_940();
void AnonFilter_a3_941();
void AnonFilter_a3_942();
void AnonFilter_a3_943();
void AnonFilter_a3_944();
void AnonFilter_a3_945();
void AnonFilter_a3_946();
void AnonFilter_a3_947();
void AnonFilter_a3_948();
void AnonFilter_a3_949();
void AnonFilter_a3_950();
void AnonFilter_a3_951();
void AnonFilter_a3_952();
void AnonFilter_a3_953();
void AnonFilter_a3_954();
void AnonFilter_a3_955();
void AnonFilter_a3_956();
void AnonFilter_a3_957();
void AnonFilter_a3_958();
void AnonFilter_a3_959();
void AnonFilter_a3_960();
void AnonFilter_a3_961();
void AnonFilter_a3_962();
void AnonFilter_a3_963();
void AnonFilter_a3_964();
void AnonFilter_a3_965();
void AnonFilter_a3_966();
void AnonFilter_a3_967();
void AnonFilter_a3_968();
void AnonFilter_a3_969();
void AnonFilter_a3_970();
void AnonFilter_a3_971();
void AnonFilter_a3_972();
void AnonFilter_a3_973();
void AnonFilter_a3_974();
void AnonFilter_a3_975();
void AnonFilter_a3_976();
void WEIGHTED_ROUND_ROBIN_Joiner_913();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_902();
void WEIGHTED_ROUND_ROBIN_Splitter_977();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_979();
void iDCT_1D_reference_fine_980();
void iDCT_1D_reference_fine_981();
void iDCT_1D_reference_fine_982();
void iDCT_1D_reference_fine_983();
void iDCT_1D_reference_fine_984();
void iDCT_1D_reference_fine_985();
void iDCT_1D_reference_fine_986();
void WEIGHTED_ROUND_ROBIN_Joiner_978();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_903();
void WEIGHTED_ROUND_ROBIN_Splitter_987();
void iDCT_1D_reference_fine_989();
void iDCT_1D_reference_fine_990();
void iDCT_1D_reference_fine_991();
void iDCT_1D_reference_fine_992();
void iDCT_1D_reference_fine_993();
void iDCT_1D_reference_fine_994();
void iDCT_1D_reference_fine_995();
void iDCT_1D_reference_fine_996();
void WEIGHTED_ROUND_ROBIN_Joiner_988();
void WEIGHTED_ROUND_ROBIN_Splitter_997();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_999();
void AnonFilter_a4_1000();
void AnonFilter_a4_1001();
void AnonFilter_a4_1002();
void AnonFilter_a4_1003();
void AnonFilter_a4_1004();
void AnonFilter_a4_1005();
void AnonFilter_a4_1006();
void AnonFilter_a4_1007();
void AnonFilter_a4_1008();
void AnonFilter_a4_1009();
void AnonFilter_a4_1010();
void AnonFilter_a4_1011();
void AnonFilter_a4_1012();
void AnonFilter_a4_1013();
void AnonFilter_a4_1014();
void AnonFilter_a4_1015();
void AnonFilter_a4_1016();
void AnonFilter_a4_1017();
void AnonFilter_a4_1018();
void AnonFilter_a4_1019();
void AnonFilter_a4_1020();
void AnonFilter_a4_1021();
void AnonFilter_a4_1022();
void AnonFilter_a4_1023();
void AnonFilter_a4_1024();
void AnonFilter_a4_1025();
void AnonFilter_a4_1026();
void AnonFilter_a4_1027();
void AnonFilter_a4_1028();
void AnonFilter_a4_1029();
void AnonFilter_a4_1030();
void AnonFilter_a4_1031();
void AnonFilter_a4_1032();
void AnonFilter_a4_1033();
void AnonFilter_a4_1034();
void AnonFilter_a4_1035();
void AnonFilter_a4_1036();
void AnonFilter_a4_1037();
void AnonFilter_a4_1038();
void AnonFilter_a4_1039();
void AnonFilter_a4_1040();
void AnonFilter_a4_1041();
void AnonFilter_a4_1042();
void AnonFilter_a4_1043();
void AnonFilter_a4_1044();
void AnonFilter_a4_1045();
void AnonFilter_a4_1046();
void AnonFilter_a4_1047();
void AnonFilter_a4_1048();
void AnonFilter_a4_1049();
void AnonFilter_a4_1050();
void AnonFilter_a4_1051();
void AnonFilter_a4_1052();
void AnonFilter_a4_1053();
void AnonFilter_a4_1054();
void AnonFilter_a4_1055();
void AnonFilter_a4_1056();
void AnonFilter_a4_1057();
void AnonFilter_a4_1058();
void AnonFilter_a4_1059();
void AnonFilter_a4_1060();
void AnonFilter_a4_1061();
void WEIGHTED_ROUND_ROBIN_Joiner_998();
void WEIGHTED_ROUND_ROBIN_Splitter_1062();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_1064();
void iDCT8x8_1D_row_fast_1065();
void iDCT8x8_1D_row_fast_1066();
void iDCT8x8_1D_row_fast_1067();
void iDCT8x8_1D_row_fast_1068();
void iDCT8x8_1D_row_fast_1069();
void iDCT8x8_1D_row_fast_1070();
void iDCT8x8_1D_row_fast_1071();
void WEIGHTED_ROUND_ROBIN_Joiner_1063();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_856();
void WEIGHTED_ROUND_ROBIN_Joiner_905();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_857();

#ifdef __cplusplus
}
#endif
#endif
