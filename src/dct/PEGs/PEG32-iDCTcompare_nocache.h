#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_11806_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_11830_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_11831_t;
void AnonFilter_a0_11803();
void DUPLICATE_Splitter_11878();
void iDCT_2D_reference_coarse_11806();
void WEIGHTED_ROUND_ROBIN_Splitter_11886();
void AnonFilter_a3_11888();
void AnonFilter_a3_11889();
void AnonFilter_a3_11890();
void AnonFilter_a3_11891();
void AnonFilter_a3_11892();
void AnonFilter_a3_11893();
void AnonFilter_a3_11894();
void AnonFilter_a3_11895();
void AnonFilter_a3_11896();
void AnonFilter_a3_11897();
void AnonFilter_a3_11898();
void AnonFilter_a3_11899();
void AnonFilter_a3_11900();
void AnonFilter_a3_11901();
void AnonFilter_a3_11902();
void AnonFilter_a3_11903();
void AnonFilter_a3_11904();
void AnonFilter_a3_11905();
void AnonFilter_a3_11906();
void AnonFilter_a3_11907();
void AnonFilter_a3_11908();
void AnonFilter_a3_11909();
void AnonFilter_a3_11910();
void AnonFilter_a3_11911();
void AnonFilter_a3_11912();
void AnonFilter_a3_11913();
void AnonFilter_a3_11914();
void AnonFilter_a3_11915();
void AnonFilter_a3_11916();
void AnonFilter_a3_11917();
void AnonFilter_a3_11918();
void AnonFilter_a3_11919();
void WEIGHTED_ROUND_ROBIN_Joiner_11887();
void Pre_CollapsedDataParallel_1_11876();
void WEIGHTED_ROUND_ROBIN_Splitter_11920();
void iDCT_1D_reference_fine_11922();
void iDCT_1D_reference_fine_11923();
void iDCT_1D_reference_fine_11924();
void iDCT_1D_reference_fine_11925();
void iDCT_1D_reference_fine_11926();
void iDCT_1D_reference_fine_11927();
void iDCT_1D_reference_fine_11928();
void iDCT_1D_reference_fine_11929();
void WEIGHTED_ROUND_ROBIN_Joiner_11921();
void Post_CollapsedDataParallel_2_11877();
void WEIGHTED_ROUND_ROBIN_Splitter_11930();
void iDCT_1D_reference_fine_11932();
void iDCT_1D_reference_fine_11933();
void iDCT_1D_reference_fine_11934();
void iDCT_1D_reference_fine_11935();
void iDCT_1D_reference_fine_11936();
void iDCT_1D_reference_fine_11937();
void iDCT_1D_reference_fine_11938();
void iDCT_1D_reference_fine_11939();
void WEIGHTED_ROUND_ROBIN_Joiner_11931();
void WEIGHTED_ROUND_ROBIN_Splitter_11940();
void AnonFilter_a4_11942();
void AnonFilter_a4_11943();
void AnonFilter_a4_11944();
void AnonFilter_a4_11945();
void AnonFilter_a4_11946();
void AnonFilter_a4_11947();
void AnonFilter_a4_11948();
void AnonFilter_a4_11949();
void AnonFilter_a4_11950();
void AnonFilter_a4_11951();
void AnonFilter_a4_11952();
void AnonFilter_a4_11953();
void AnonFilter_a4_11954();
void AnonFilter_a4_11955();
void AnonFilter_a4_11956();
void AnonFilter_a4_11957();
void AnonFilter_a4_11958();
void AnonFilter_a4_11959();
void AnonFilter_a4_11960();
void AnonFilter_a4_11961();
void AnonFilter_a4_11962();
void AnonFilter_a4_11963();
void AnonFilter_a4_11964();
void AnonFilter_a4_11965();
void AnonFilter_a4_11966();
void AnonFilter_a4_11967();
void AnonFilter_a4_11968();
void AnonFilter_a4_11969();
void AnonFilter_a4_11970();
void AnonFilter_a4_11971();
void AnonFilter_a4_11972();
void AnonFilter_a4_11973();
void WEIGHTED_ROUND_ROBIN_Joiner_11941();
void WEIGHTED_ROUND_ROBIN_Splitter_11974();
void iDCT8x8_1D_row_fast_11976();
void iDCT8x8_1D_row_fast_11977();
void iDCT8x8_1D_row_fast_11978();
void iDCT8x8_1D_row_fast_11979();
void iDCT8x8_1D_row_fast_11980();
void iDCT8x8_1D_row_fast_11981();
void iDCT8x8_1D_row_fast_11982();
void iDCT8x8_1D_row_fast_11983();
void WEIGHTED_ROUND_ROBIN_Joiner_11975();
void iDCT8x8_1D_col_fast_11830();
void WEIGHTED_ROUND_ROBIN_Joiner_11879();
void AnonFilter_a2_11831();

#ifdef __cplusplus
}
#endif
#endif
