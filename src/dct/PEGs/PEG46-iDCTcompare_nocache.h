#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=8832 on the compile command line
#else
#if BUF_SIZEMAX < 8832
#error BUF_SIZEMAX too small, it must be at least 8832
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_7326_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_7350_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_7351_t;
void AnonFilter_a0_7323();
void DUPLICATE_Splitter_7398();
void iDCT_2D_reference_coarse_7326();
void WEIGHTED_ROUND_ROBIN_Splitter_7406();
void AnonFilter_a3_7408();
void AnonFilter_a3_7409();
void AnonFilter_a3_7410();
void AnonFilter_a3_7411();
void AnonFilter_a3_7412();
void AnonFilter_a3_7413();
void AnonFilter_a3_7414();
void AnonFilter_a3_7415();
void AnonFilter_a3_7416();
void AnonFilter_a3_7417();
void AnonFilter_a3_7418();
void AnonFilter_a3_7419();
void AnonFilter_a3_7420();
void AnonFilter_a3_7421();
void AnonFilter_a3_7422();
void AnonFilter_a3_7423();
void AnonFilter_a3_7424();
void AnonFilter_a3_7425();
void AnonFilter_a3_7426();
void AnonFilter_a3_7427();
void AnonFilter_a3_7428();
void AnonFilter_a3_7429();
void AnonFilter_a3_7430();
void AnonFilter_a3_7431();
void AnonFilter_a3_7432();
void AnonFilter_a3_7433();
void AnonFilter_a3_7434();
void AnonFilter_a3_7435();
void AnonFilter_a3_7436();
void AnonFilter_a3_7437();
void AnonFilter_a3_7438();
void AnonFilter_a3_7439();
void AnonFilter_a3_7440();
void AnonFilter_a3_7441();
void AnonFilter_a3_7442();
void AnonFilter_a3_7443();
void AnonFilter_a3_7444();
void AnonFilter_a3_7445();
void AnonFilter_a3_7446();
void AnonFilter_a3_7447();
void AnonFilter_a3_7448();
void AnonFilter_a3_7449();
void AnonFilter_a3_7450();
void AnonFilter_a3_7451();
void AnonFilter_a3_7452();
void AnonFilter_a3_7453();
void WEIGHTED_ROUND_ROBIN_Joiner_7407();
void Pre_CollapsedDataParallel_1_7396();
void WEIGHTED_ROUND_ROBIN_Splitter_7454();
void iDCT_1D_reference_fine_7456();
void iDCT_1D_reference_fine_7457();
void iDCT_1D_reference_fine_7458();
void iDCT_1D_reference_fine_7459();
void iDCT_1D_reference_fine_7460();
void iDCT_1D_reference_fine_7461();
void iDCT_1D_reference_fine_7462();
void iDCT_1D_reference_fine_7463();
void WEIGHTED_ROUND_ROBIN_Joiner_7455();
void Post_CollapsedDataParallel_2_7397();
void WEIGHTED_ROUND_ROBIN_Splitter_7464();
void iDCT_1D_reference_fine_7466();
void iDCT_1D_reference_fine_7467();
void iDCT_1D_reference_fine_7468();
void iDCT_1D_reference_fine_7469();
void iDCT_1D_reference_fine_7470();
void iDCT_1D_reference_fine_7471();
void iDCT_1D_reference_fine_7472();
void iDCT_1D_reference_fine_7473();
void WEIGHTED_ROUND_ROBIN_Joiner_7465();
void WEIGHTED_ROUND_ROBIN_Splitter_7474();
void AnonFilter_a4_7476();
void AnonFilter_a4_7477();
void AnonFilter_a4_7478();
void AnonFilter_a4_7479();
void AnonFilter_a4_7480();
void AnonFilter_a4_7481();
void AnonFilter_a4_7482();
void AnonFilter_a4_7483();
void AnonFilter_a4_7484();
void AnonFilter_a4_7485();
void AnonFilter_a4_7486();
void AnonFilter_a4_7487();
void AnonFilter_a4_7488();
void AnonFilter_a4_7489();
void AnonFilter_a4_7490();
void AnonFilter_a4_7491();
void AnonFilter_a4_7492();
void AnonFilter_a4_7493();
void AnonFilter_a4_7494();
void AnonFilter_a4_7495();
void AnonFilter_a4_7496();
void AnonFilter_a4_7497();
void AnonFilter_a4_7498();
void AnonFilter_a4_7499();
void AnonFilter_a4_7500();
void AnonFilter_a4_7501();
void AnonFilter_a4_7502();
void AnonFilter_a4_7503();
void AnonFilter_a4_7504();
void AnonFilter_a4_7505();
void AnonFilter_a4_7506();
void AnonFilter_a4_7507();
void AnonFilter_a4_7508();
void AnonFilter_a4_7509();
void AnonFilter_a4_7510();
void AnonFilter_a4_7511();
void AnonFilter_a4_7512();
void AnonFilter_a4_7513();
void AnonFilter_a4_7514();
void AnonFilter_a4_7515();
void AnonFilter_a4_7516();
void AnonFilter_a4_7517();
void AnonFilter_a4_7518();
void AnonFilter_a4_7519();
void AnonFilter_a4_7520();
void AnonFilter_a4_7521();
void WEIGHTED_ROUND_ROBIN_Joiner_7475();
void WEIGHTED_ROUND_ROBIN_Splitter_7522();
void iDCT8x8_1D_row_fast_7524();
void iDCT8x8_1D_row_fast_7525();
void iDCT8x8_1D_row_fast_7526();
void iDCT8x8_1D_row_fast_7527();
void iDCT8x8_1D_row_fast_7528();
void iDCT8x8_1D_row_fast_7529();
void iDCT8x8_1D_row_fast_7530();
void iDCT8x8_1D_row_fast_7531();
void WEIGHTED_ROUND_ROBIN_Joiner_7523();
void iDCT8x8_1D_col_fast_7350();
void WEIGHTED_ROUND_ROBIN_Joiner_7399();
void AnonFilter_a2_7351();

#ifdef __cplusplus
}
#endif
#endif
