#include "PEG20-iDCTcompare.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15125Post_CollapsedDataParallel_2_15093;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_15095AnonFilter_a2_15047;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[20];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15103Pre_CollapsedDataParallel_1_15092;
buffer_int_t SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[20];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[20];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[8];
buffer_int_t AnonFilter_a0_15019DUPLICATE_Splitter_15094;
buffer_float_t Post_CollapsedDataParallel_2_15093WEIGHTED_ROUND_ROBIN_Splitter_15134;
buffer_float_t Pre_CollapsedDataParallel_1_15092WEIGHTED_ROUND_ROBIN_Splitter_15124;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[20];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_15167iDCT8x8_1D_col_fast_15046;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15135WEIGHTED_ROUND_ROBIN_Splitter_15144;
buffer_int_t SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[8];


iDCT_2D_reference_coarse_15022_t iDCT_2D_reference_coarse_15022_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15126_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15127_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15128_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15129_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15130_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15131_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15132_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15133_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15136_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15137_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15138_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15139_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15140_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15141_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15142_s;
iDCT_2D_reference_coarse_15022_t iDCT_1D_reference_fine_15143_s;
iDCT8x8_1D_col_fast_15046_t iDCT8x8_1D_col_fast_15046_s;
AnonFilter_a2_15047_t AnonFilter_a2_15047_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_15019() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_15019DUPLICATE_Splitter_15094));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_15022_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_15022_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_15022() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_15104() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[0]));
	ENDFOR
}

void AnonFilter_a3_15105() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[1]));
	ENDFOR
}

void AnonFilter_a3_15106() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[2]));
	ENDFOR
}

void AnonFilter_a3_15107() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[3]));
	ENDFOR
}

void AnonFilter_a3_15108() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[4]));
	ENDFOR
}

void AnonFilter_a3_15109() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[5]));
	ENDFOR
}

void AnonFilter_a3_15110() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[6]));
	ENDFOR
}

void AnonFilter_a3_15111() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[7]));
	ENDFOR
}

void AnonFilter_a3_15112() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[8]));
	ENDFOR
}

void AnonFilter_a3_15113() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[9]));
	ENDFOR
}

void AnonFilter_a3_15114() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[10]));
	ENDFOR
}

void AnonFilter_a3_15115() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[11]));
	ENDFOR
}

void AnonFilter_a3_15116() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[12]));
	ENDFOR
}

void AnonFilter_a3_15117() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[13]));
	ENDFOR
}

void AnonFilter_a3_15118() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[14]));
	ENDFOR
}

void AnonFilter_a3_15119() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[15]));
	ENDFOR
}

void AnonFilter_a3_15120() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[16]));
	ENDFOR
}

void AnonFilter_a3_15121() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[17]));
	ENDFOR
}

void AnonFilter_a3_15122() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[18]));
	ENDFOR
}

void AnonFilter_a3_15123() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[19]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15102() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15103() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15103Pre_CollapsedDataParallel_1_15092, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_15092() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_15103Pre_CollapsedDataParallel_1_15092), &(Pre_CollapsedDataParallel_1_15092WEIGHTED_ROUND_ROBIN_Splitter_15124));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15126_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_15126() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_15127() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_15128() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_15129() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_15130() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_15131() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_15132() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_15133() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15124() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_15092WEIGHTED_ROUND_ROBIN_Splitter_15124));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15125() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15125Post_CollapsedDataParallel_2_15093, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_15093() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_15125Post_CollapsedDataParallel_2_15093), &(Post_CollapsedDataParallel_2_15093WEIGHTED_ROUND_ROBIN_Splitter_15134));
	ENDFOR
}

void iDCT_1D_reference_fine_15136() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_15137() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_15138() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_15139() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_15140() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_15141() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_15142() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_15143() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15134() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_15093WEIGHTED_ROUND_ROBIN_Splitter_15134));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15135() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15135WEIGHTED_ROUND_ROBIN_Splitter_15144, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_15146() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[0]));
	ENDFOR
}

void AnonFilter_a4_15147() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[1]));
	ENDFOR
}

void AnonFilter_a4_15148() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[2]));
	ENDFOR
}

void AnonFilter_a4_15149() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[3]));
	ENDFOR
}

void AnonFilter_a4_15150() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[4]));
	ENDFOR
}

void AnonFilter_a4_15151() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[5]));
	ENDFOR
}

void AnonFilter_a4_15152() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[6]));
	ENDFOR
}

void AnonFilter_a4_15153() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[7]));
	ENDFOR
}

void AnonFilter_a4_15154() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[8]));
	ENDFOR
}

void AnonFilter_a4_15155() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[9]));
	ENDFOR
}

void AnonFilter_a4_15156() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[10]));
	ENDFOR
}

void AnonFilter_a4_15157() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[11]));
	ENDFOR
}

void AnonFilter_a4_15158() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[12]));
	ENDFOR
}

void AnonFilter_a4_15159() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[13]));
	ENDFOR
}

void AnonFilter_a4_15160() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[14]));
	ENDFOR
}

void AnonFilter_a4_15161() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[15]));
	ENDFOR
}

void AnonFilter_a4_15162() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[16]));
	ENDFOR
}

void AnonFilter_a4_15163() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[17]));
	ENDFOR
}

void AnonFilter_a4_15164() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[18]));
	ENDFOR
}

void AnonFilter_a4_15165() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[19]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15144() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_15135WEIGHTED_ROUND_ROBIN_Splitter_15144));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15145() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_15168() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[0]), &(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15169() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[1]), &(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15170() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[2]), &(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15171() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[3]), &(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15172() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[4]), &(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15173() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[5]), &(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15174() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[6]), &(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15175() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[7]), &(SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15166() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15167() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_15167iDCT8x8_1D_col_fast_15046, pop_int(&SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_15046_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_15046_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_15046_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_15046_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_15046_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_15046_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_15046_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_15046_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_15046_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_15046_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_15046() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_15167iDCT8x8_1D_col_fast_15046), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_15094() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_15019DUPLICATE_Splitter_15094);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15095() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_15095AnonFilter_a2_15047, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_15047_s.count = (AnonFilter_a2_15047_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_15047_s.errors = (AnonFilter_a2_15047_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15047_s.errors / AnonFilter_a2_15047_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_15047_s.errors = (AnonFilter_a2_15047_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15047_s.errors / AnonFilter_a2_15047_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_15047() {
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_15095AnonFilter_a2_15047));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15125Post_CollapsedDataParallel_2_15093);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_15095AnonFilter_a2_15047);
	FOR(int, __iter_init_0_, 0, <, 20, __iter_init_0_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_15177_15184_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15103Pre_CollapsedDataParallel_1_15092);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 20, __iter_init_3_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_15177_15184_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 20, __iter_init_4_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_15180_15187_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15179_15186_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15178_15185_split[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_15019DUPLICATE_Splitter_15094);
	init_buffer_float(&Post_CollapsedDataParallel_2_15093WEIGHTED_ROUND_ROBIN_Splitter_15134);
	init_buffer_float(&Pre_CollapsedDataParallel_1_15092WEIGHTED_ROUND_ROBIN_Splitter_15124);
	FOR(int, __iter_init_9_, 0, <, 20, __iter_init_9_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_15180_15187_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15020_15096_15176_15183_split[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_15167iDCT8x8_1D_col_fast_15046);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15135WEIGHTED_ROUND_ROBIN_Splitter_15144);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin64_iDCT8x8_1D_row_fast_Fiss_15181_15188_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_15022
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_15022_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15126
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15126_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15127
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15127_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15128
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15128_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15129
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15129_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15130
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15130_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15131
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15131_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15132
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15132_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15133
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15133_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15136
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15136_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15137
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15137_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15138
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15138_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15139
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15139_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15140
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15140_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15141
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15141_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15142
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15142_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15143
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15143_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_15047
	 {
	AnonFilter_a2_15047_s.count = 0.0 ; 
	AnonFilter_a2_15047_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_15019();
		DUPLICATE_Splitter_15094();
			iDCT_2D_reference_coarse_15022();
			WEIGHTED_ROUND_ROBIN_Splitter_15102();
				AnonFilter_a3_15104();
				AnonFilter_a3_15105();
				AnonFilter_a3_15106();
				AnonFilter_a3_15107();
				AnonFilter_a3_15108();
				AnonFilter_a3_15109();
				AnonFilter_a3_15110();
				AnonFilter_a3_15111();
				AnonFilter_a3_15112();
				AnonFilter_a3_15113();
				AnonFilter_a3_15114();
				AnonFilter_a3_15115();
				AnonFilter_a3_15116();
				AnonFilter_a3_15117();
				AnonFilter_a3_15118();
				AnonFilter_a3_15119();
				AnonFilter_a3_15120();
				AnonFilter_a3_15121();
				AnonFilter_a3_15122();
				AnonFilter_a3_15123();
			WEIGHTED_ROUND_ROBIN_Joiner_15103();
			Pre_CollapsedDataParallel_1_15092();
			WEIGHTED_ROUND_ROBIN_Splitter_15124();
				iDCT_1D_reference_fine_15126();
				iDCT_1D_reference_fine_15127();
				iDCT_1D_reference_fine_15128();
				iDCT_1D_reference_fine_15129();
				iDCT_1D_reference_fine_15130();
				iDCT_1D_reference_fine_15131();
				iDCT_1D_reference_fine_15132();
				iDCT_1D_reference_fine_15133();
			WEIGHTED_ROUND_ROBIN_Joiner_15125();
			Post_CollapsedDataParallel_2_15093();
			WEIGHTED_ROUND_ROBIN_Splitter_15134();
				iDCT_1D_reference_fine_15136();
				iDCT_1D_reference_fine_15137();
				iDCT_1D_reference_fine_15138();
				iDCT_1D_reference_fine_15139();
				iDCT_1D_reference_fine_15140();
				iDCT_1D_reference_fine_15141();
				iDCT_1D_reference_fine_15142();
				iDCT_1D_reference_fine_15143();
			WEIGHTED_ROUND_ROBIN_Joiner_15135();
			WEIGHTED_ROUND_ROBIN_Splitter_15144();
				AnonFilter_a4_15146();
				AnonFilter_a4_15147();
				AnonFilter_a4_15148();
				AnonFilter_a4_15149();
				AnonFilter_a4_15150();
				AnonFilter_a4_15151();
				AnonFilter_a4_15152();
				AnonFilter_a4_15153();
				AnonFilter_a4_15154();
				AnonFilter_a4_15155();
				AnonFilter_a4_15156();
				AnonFilter_a4_15157();
				AnonFilter_a4_15158();
				AnonFilter_a4_15159();
				AnonFilter_a4_15160();
				AnonFilter_a4_15161();
				AnonFilter_a4_15162();
				AnonFilter_a4_15163();
				AnonFilter_a4_15164();
				AnonFilter_a4_15165();
			WEIGHTED_ROUND_ROBIN_Joiner_15145();
			WEIGHTED_ROUND_ROBIN_Splitter_15166();
				iDCT8x8_1D_row_fast_15168();
				iDCT8x8_1D_row_fast_15169();
				iDCT8x8_1D_row_fast_15170();
				iDCT8x8_1D_row_fast_15171();
				iDCT8x8_1D_row_fast_15172();
				iDCT8x8_1D_row_fast_15173();
				iDCT8x8_1D_row_fast_15174();
				iDCT8x8_1D_row_fast_15175();
			WEIGHTED_ROUND_ROBIN_Joiner_15167();
			iDCT8x8_1D_col_fast_15046();
		WEIGHTED_ROUND_ROBIN_Joiner_15095();
		AnonFilter_a2_15047();
	ENDFOR
	return EXIT_SUCCESS;
}
