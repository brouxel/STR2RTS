#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3456 on the compile command line
#else
#if BUF_SIZEMAX < 3456
#error BUF_SIZEMAX too small, it must be at least 3456
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_10606_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_10630_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_10631_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_10603();
void DUPLICATE_Splitter_10678();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_10606();
void WEIGHTED_ROUND_ROBIN_Splitter_10686();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_10688();
void AnonFilter_a3_10689();
void AnonFilter_a3_10690();
void AnonFilter_a3_10691();
void AnonFilter_a3_10692();
void AnonFilter_a3_10693();
void AnonFilter_a3_10694();
void AnonFilter_a3_10695();
void AnonFilter_a3_10696();
void AnonFilter_a3_10697();
void AnonFilter_a3_10698();
void AnonFilter_a3_10699();
void AnonFilter_a3_10700();
void AnonFilter_a3_10701();
void AnonFilter_a3_10702();
void AnonFilter_a3_10703();
void AnonFilter_a3_10704();
void AnonFilter_a3_10705();
void AnonFilter_a3_10706();
void AnonFilter_a3_10707();
void AnonFilter_a3_10708();
void AnonFilter_a3_10709();
void AnonFilter_a3_10710();
void AnonFilter_a3_10711();
void AnonFilter_a3_10712();
void AnonFilter_a3_10713();
void AnonFilter_a3_10714();
void AnonFilter_a3_10715();
void AnonFilter_a3_10716();
void AnonFilter_a3_10717();
void AnonFilter_a3_10718();
void AnonFilter_a3_10719();
void AnonFilter_a3_10720();
void AnonFilter_a3_10721();
void AnonFilter_a3_10722();
void AnonFilter_a3_10723();
void WEIGHTED_ROUND_ROBIN_Joiner_10687();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_10676();
void WEIGHTED_ROUND_ROBIN_Splitter_10724();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_10726();
void iDCT_1D_reference_fine_10727();
void iDCT_1D_reference_fine_10728();
void iDCT_1D_reference_fine_10729();
void iDCT_1D_reference_fine_10730();
void iDCT_1D_reference_fine_10731();
void iDCT_1D_reference_fine_10732();
void iDCT_1D_reference_fine_10733();
void WEIGHTED_ROUND_ROBIN_Joiner_10725();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_10677();
void WEIGHTED_ROUND_ROBIN_Splitter_10734();
void iDCT_1D_reference_fine_10736();
void iDCT_1D_reference_fine_10737();
void iDCT_1D_reference_fine_10738();
void iDCT_1D_reference_fine_10739();
void iDCT_1D_reference_fine_10740();
void iDCT_1D_reference_fine_10741();
void iDCT_1D_reference_fine_10742();
void iDCT_1D_reference_fine_10743();
void WEIGHTED_ROUND_ROBIN_Joiner_10735();
void WEIGHTED_ROUND_ROBIN_Splitter_10744();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_10746();
void AnonFilter_a4_10747();
void AnonFilter_a4_10748();
void AnonFilter_a4_10749();
void AnonFilter_a4_10750();
void AnonFilter_a4_10751();
void AnonFilter_a4_10752();
void AnonFilter_a4_10753();
void AnonFilter_a4_10754();
void AnonFilter_a4_10755();
void AnonFilter_a4_10756();
void AnonFilter_a4_10757();
void AnonFilter_a4_10758();
void AnonFilter_a4_10759();
void AnonFilter_a4_10760();
void AnonFilter_a4_10761();
void AnonFilter_a4_10762();
void AnonFilter_a4_10763();
void AnonFilter_a4_10764();
void AnonFilter_a4_10765();
void AnonFilter_a4_10766();
void AnonFilter_a4_10767();
void AnonFilter_a4_10768();
void AnonFilter_a4_10769();
void AnonFilter_a4_10770();
void AnonFilter_a4_10771();
void AnonFilter_a4_10772();
void AnonFilter_a4_10773();
void AnonFilter_a4_10774();
void AnonFilter_a4_10775();
void AnonFilter_a4_10776();
void AnonFilter_a4_10777();
void AnonFilter_a4_10778();
void AnonFilter_a4_10779();
void AnonFilter_a4_10780();
void AnonFilter_a4_10781();
void WEIGHTED_ROUND_ROBIN_Joiner_10745();
void WEIGHTED_ROUND_ROBIN_Splitter_10782();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_10784();
void iDCT8x8_1D_row_fast_10785();
void iDCT8x8_1D_row_fast_10786();
void iDCT8x8_1D_row_fast_10787();
void iDCT8x8_1D_row_fast_10788();
void iDCT8x8_1D_row_fast_10789();
void iDCT8x8_1D_row_fast_10790();
void iDCT8x8_1D_row_fast_10791();
void WEIGHTED_ROUND_ROBIN_Joiner_10783();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_10630();
void WEIGHTED_ROUND_ROBIN_Joiner_10679();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_10631();

#ifdef __cplusplus
}
#endif
#endif
