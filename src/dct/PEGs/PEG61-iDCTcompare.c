#include "PEG61-iDCTcompare.h"

buffer_int_t SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_1883iDCT8x8_1D_col_fast_1680;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[61];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[61];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[61];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_1729AnonFilter_a2_1681;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_split[3];
buffer_int_t SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1737Pre_CollapsedDataParallel_1_1726;
buffer_float_t Post_CollapsedDataParallel_2_1727WEIGHTED_ROUND_ROBIN_Splitter_1809;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1810WEIGHTED_ROUND_ROBIN_Splitter_1819;
buffer_float_t Pre_CollapsedDataParallel_1_1726WEIGHTED_ROUND_ROBIN_Splitter_1799;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1800Post_CollapsedDataParallel_2_1727;
buffer_int_t AnonFilter_a0_1653DUPLICATE_Splitter_1728;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[61];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_join[3];


iDCT_2D_reference_coarse_1656_t iDCT_2D_reference_coarse_1656_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1801_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1802_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1803_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1804_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1805_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1806_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1807_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1808_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1811_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1812_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1813_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1814_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1815_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1816_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1817_s;
iDCT_2D_reference_coarse_1656_t iDCT_1D_reference_fine_1818_s;
iDCT8x8_1D_col_fast_1680_t iDCT8x8_1D_col_fast_1680_s;
AnonFilter_a2_1681_t AnonFilter_a2_1681_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_1653() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_1653DUPLICATE_Splitter_1728));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_1656_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_1656_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_1656() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_1738() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[0]));
	ENDFOR
}

void AnonFilter_a3_1739() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[1]));
	ENDFOR
}

void AnonFilter_a3_1740() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[2]));
	ENDFOR
}

void AnonFilter_a3_1741() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[3]));
	ENDFOR
}

void AnonFilter_a3_1742() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[4]));
	ENDFOR
}

void AnonFilter_a3_1743() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[5]));
	ENDFOR
}

void AnonFilter_a3_1744() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[6]));
	ENDFOR
}

void AnonFilter_a3_1745() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[7]));
	ENDFOR
}

void AnonFilter_a3_1746() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[8]));
	ENDFOR
}

void AnonFilter_a3_1747() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[9]));
	ENDFOR
}

void AnonFilter_a3_1748() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[10]));
	ENDFOR
}

void AnonFilter_a3_1749() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[11]));
	ENDFOR
}

void AnonFilter_a3_1750() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[12]));
	ENDFOR
}

void AnonFilter_a3_1751() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[13]));
	ENDFOR
}

void AnonFilter_a3_1752() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[14]));
	ENDFOR
}

void AnonFilter_a3_1753() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[15]));
	ENDFOR
}

void AnonFilter_a3_1754() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[16]));
	ENDFOR
}

void AnonFilter_a3_1755() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[17]));
	ENDFOR
}

void AnonFilter_a3_1756() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[18]));
	ENDFOR
}

void AnonFilter_a3_1757() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[19]));
	ENDFOR
}

void AnonFilter_a3_1758() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[20]));
	ENDFOR
}

void AnonFilter_a3_1759() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[21]));
	ENDFOR
}

void AnonFilter_a3_1760() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[22]));
	ENDFOR
}

void AnonFilter_a3_1761() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[23]));
	ENDFOR
}

void AnonFilter_a3_1762() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[24]));
	ENDFOR
}

void AnonFilter_a3_1763() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[25]));
	ENDFOR
}

void AnonFilter_a3_1764() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[26]));
	ENDFOR
}

void AnonFilter_a3_1765() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[27]));
	ENDFOR
}

void AnonFilter_a3_1766() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[28]));
	ENDFOR
}

void AnonFilter_a3_1767() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[29]));
	ENDFOR
}

void AnonFilter_a3_1768() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[30]));
	ENDFOR
}

void AnonFilter_a3_1769() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[31]));
	ENDFOR
}

void AnonFilter_a3_1770() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[32]));
	ENDFOR
}

void AnonFilter_a3_1771() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[33]));
	ENDFOR
}

void AnonFilter_a3_1772() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[34]));
	ENDFOR
}

void AnonFilter_a3_1773() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[35]));
	ENDFOR
}

void AnonFilter_a3_1774() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[36]));
	ENDFOR
}

void AnonFilter_a3_1775() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[37]));
	ENDFOR
}

void AnonFilter_a3_1776() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[38]));
	ENDFOR
}

void AnonFilter_a3_1777() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[39]));
	ENDFOR
}

void AnonFilter_a3_1778() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[40]));
	ENDFOR
}

void AnonFilter_a3_1779() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[41]));
	ENDFOR
}

void AnonFilter_a3_1780() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[42]));
	ENDFOR
}

void AnonFilter_a3_1781() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[43]));
	ENDFOR
}

void AnonFilter_a3_1782() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[44]));
	ENDFOR
}

void AnonFilter_a3_1783() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[45]));
	ENDFOR
}

void AnonFilter_a3_1784() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[46]));
	ENDFOR
}

void AnonFilter_a3_1785() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[47]));
	ENDFOR
}

void AnonFilter_a3_1786() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[48]));
	ENDFOR
}

void AnonFilter_a3_1787() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[49]));
	ENDFOR
}

void AnonFilter_a3_1788() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[50]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[50]));
	ENDFOR
}

void AnonFilter_a3_1789() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[51]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[51]));
	ENDFOR
}

void AnonFilter_a3_1790() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[52]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[52]));
	ENDFOR
}

void AnonFilter_a3_1791() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[53]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[53]));
	ENDFOR
}

void AnonFilter_a3_1792() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[54]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[54]));
	ENDFOR
}

void AnonFilter_a3_1793() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[55]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[55]));
	ENDFOR
}

void AnonFilter_a3_1794() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[56]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[56]));
	ENDFOR
}

void AnonFilter_a3_1795() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[57]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[57]));
	ENDFOR
}

void AnonFilter_a3_1796() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[58]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[58]));
	ENDFOR
}

void AnonFilter_a3_1797() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[59]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[59]));
	ENDFOR
}

void AnonFilter_a3_1798() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[60]), &(SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[60]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1736() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 61, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1737() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 61, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1737Pre_CollapsedDataParallel_1_1726, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_1726() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_1737Pre_CollapsedDataParallel_1_1726), &(Pre_CollapsedDataParallel_1_1726WEIGHTED_ROUND_ROBIN_Splitter_1799));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1801_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_1801() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_1802() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_1803() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_1804() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_1805() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_1806() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_1807() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_1808() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1799() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_1726WEIGHTED_ROUND_ROBIN_Splitter_1799));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1800() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1800Post_CollapsedDataParallel_2_1727, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_1727() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_1800Post_CollapsedDataParallel_2_1727), &(Post_CollapsedDataParallel_2_1727WEIGHTED_ROUND_ROBIN_Splitter_1809));
	ENDFOR
}

void iDCT_1D_reference_fine_1811() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_1812() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_1813() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_1814() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_1815() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_1816() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_1817() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_1818() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1809() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_1727WEIGHTED_ROUND_ROBIN_Splitter_1809));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1810() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1810WEIGHTED_ROUND_ROBIN_Splitter_1819, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_1821() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[0]));
	ENDFOR
}

void AnonFilter_a4_1822() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[1]));
	ENDFOR
}

void AnonFilter_a4_1823() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[2]));
	ENDFOR
}

void AnonFilter_a4_1824() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[3]));
	ENDFOR
}

void AnonFilter_a4_1825() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[4]));
	ENDFOR
}

void AnonFilter_a4_1826() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[5]));
	ENDFOR
}

void AnonFilter_a4_1827() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[6]));
	ENDFOR
}

void AnonFilter_a4_1828() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[7]));
	ENDFOR
}

void AnonFilter_a4_1829() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[8]));
	ENDFOR
}

void AnonFilter_a4_1830() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[9]));
	ENDFOR
}

void AnonFilter_a4_1831() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[10]));
	ENDFOR
}

void AnonFilter_a4_1832() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[11]));
	ENDFOR
}

void AnonFilter_a4_1833() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[12]));
	ENDFOR
}

void AnonFilter_a4_1834() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[13]));
	ENDFOR
}

void AnonFilter_a4_1835() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[14]));
	ENDFOR
}

void AnonFilter_a4_1836() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[15]));
	ENDFOR
}

void AnonFilter_a4_1837() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[16]));
	ENDFOR
}

void AnonFilter_a4_1838() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[17]));
	ENDFOR
}

void AnonFilter_a4_1839() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[18]));
	ENDFOR
}

void AnonFilter_a4_1840() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[19]));
	ENDFOR
}

void AnonFilter_a4_1841() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[20]));
	ENDFOR
}

void AnonFilter_a4_1842() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[21]));
	ENDFOR
}

void AnonFilter_a4_1843() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[22]));
	ENDFOR
}

void AnonFilter_a4_1844() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[23]));
	ENDFOR
}

void AnonFilter_a4_1845() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[24]));
	ENDFOR
}

void AnonFilter_a4_1846() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[25]));
	ENDFOR
}

void AnonFilter_a4_1847() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[26]));
	ENDFOR
}

void AnonFilter_a4_1848() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[27]));
	ENDFOR
}

void AnonFilter_a4_1849() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[28]));
	ENDFOR
}

void AnonFilter_a4_1850() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[29]));
	ENDFOR
}

void AnonFilter_a4_1851() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[30]));
	ENDFOR
}

void AnonFilter_a4_1852() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[31]));
	ENDFOR
}

void AnonFilter_a4_1853() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[32]));
	ENDFOR
}

void AnonFilter_a4_1854() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[33]));
	ENDFOR
}

void AnonFilter_a4_1855() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[34]));
	ENDFOR
}

void AnonFilter_a4_1856() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[35]));
	ENDFOR
}

void AnonFilter_a4_1857() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[36]));
	ENDFOR
}

void AnonFilter_a4_1858() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[37]));
	ENDFOR
}

void AnonFilter_a4_1859() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[38]));
	ENDFOR
}

void AnonFilter_a4_1860() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[39]));
	ENDFOR
}

void AnonFilter_a4_1861() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[40]));
	ENDFOR
}

void AnonFilter_a4_1862() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[41]));
	ENDFOR
}

void AnonFilter_a4_1863() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[42]));
	ENDFOR
}

void AnonFilter_a4_1864() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[43]));
	ENDFOR
}

void AnonFilter_a4_1865() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[44]));
	ENDFOR
}

void AnonFilter_a4_1866() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[45]));
	ENDFOR
}

void AnonFilter_a4_1867() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[46]));
	ENDFOR
}

void AnonFilter_a4_1868() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[47]));
	ENDFOR
}

void AnonFilter_a4_1869() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[48]));
	ENDFOR
}

void AnonFilter_a4_1870() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[49]));
	ENDFOR
}

void AnonFilter_a4_1871() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[50]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[50]));
	ENDFOR
}

void AnonFilter_a4_1872() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[51]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[51]));
	ENDFOR
}

void AnonFilter_a4_1873() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[52]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[52]));
	ENDFOR
}

void AnonFilter_a4_1874() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[53]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[53]));
	ENDFOR
}

void AnonFilter_a4_1875() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[54]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[54]));
	ENDFOR
}

void AnonFilter_a4_1876() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[55]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[55]));
	ENDFOR
}

void AnonFilter_a4_1877() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[56]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[56]));
	ENDFOR
}

void AnonFilter_a4_1878() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[57]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[57]));
	ENDFOR
}

void AnonFilter_a4_1879() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[58]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[58]));
	ENDFOR
}

void AnonFilter_a4_1880() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[59]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[59]));
	ENDFOR
}

void AnonFilter_a4_1881() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[60]), &(SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[60]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1819() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 61, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1810WEIGHTED_ROUND_ROBIN_Splitter_1819));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1820() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 61, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_1884() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[0]), &(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1885() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[1]), &(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1886() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[2]), &(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1887() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[3]), &(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1888() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[4]), &(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1889() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[5]), &(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1890() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[6]), &(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_1891() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[7]), &(SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1882() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1883() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_1883iDCT8x8_1D_col_fast_1680, pop_int(&SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_1680_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_1680_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_1680_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_1680_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_1680_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_1680_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_1680_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_1680_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_1680_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_1680_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_1680() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_1883iDCT8x8_1D_col_fast_1680), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_1728() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3904, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_1653DUPLICATE_Splitter_1728);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1729() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3904, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_1729AnonFilter_a2_1681, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_1681_s.count = (AnonFilter_a2_1681_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_1681_s.errors = (AnonFilter_a2_1681_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_1681_s.errors / AnonFilter_a2_1681_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_1681_s.errors = (AnonFilter_a2_1681_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_1681_s.errors / AnonFilter_a2_1681_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_1681() {
	FOR(uint32_t, __iter_steady_, 0, <, 3904, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_1729AnonFilter_a2_1681));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_1883iDCT8x8_1D_col_fast_1680);
	FOR(int, __iter_init_1_, 0, <, 61, __iter_init_1_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_1896_1903_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 61, __iter_init_2_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_1896_1903_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1895_1902_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 61, __iter_init_6_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_1893_1900_join[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_1729AnonFilter_a2_1681);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1894_1901_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin146_iDCT8x8_1D_row_fast_Fiss_1897_1904_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1737Pre_CollapsedDataParallel_1_1726);
	init_buffer_float(&Post_CollapsedDataParallel_2_1727WEIGHTED_ROUND_ROBIN_Splitter_1809);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1810WEIGHTED_ROUND_ROBIN_Splitter_1819);
	init_buffer_float(&Pre_CollapsedDataParallel_1_1726WEIGHTED_ROUND_ROBIN_Splitter_1799);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1800Post_CollapsedDataParallel_2_1727);
	init_buffer_int(&AnonFilter_a0_1653DUPLICATE_Splitter_1728);
	FOR(int, __iter_init_10_, 0, <, 61, __iter_init_10_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_1893_1900_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1654_1730_1892_1899_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_1656
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_1656_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1801
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1801_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1802
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1802_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1803
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1803_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1804
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1804_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1805
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1805_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1806
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1806_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1807
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1807_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1808
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1808_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1811
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1811_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1812
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1812_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1813
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1813_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1814
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1814_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1815
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1815_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1816
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1816_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1817
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1817_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1818
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1818_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_1681
	 {
	AnonFilter_a2_1681_s.count = 0.0 ; 
	AnonFilter_a2_1681_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_1653();
		DUPLICATE_Splitter_1728();
			iDCT_2D_reference_coarse_1656();
			WEIGHTED_ROUND_ROBIN_Splitter_1736();
				AnonFilter_a3_1738();
				AnonFilter_a3_1739();
				AnonFilter_a3_1740();
				AnonFilter_a3_1741();
				AnonFilter_a3_1742();
				AnonFilter_a3_1743();
				AnonFilter_a3_1744();
				AnonFilter_a3_1745();
				AnonFilter_a3_1746();
				AnonFilter_a3_1747();
				AnonFilter_a3_1748();
				AnonFilter_a3_1749();
				AnonFilter_a3_1750();
				AnonFilter_a3_1751();
				AnonFilter_a3_1752();
				AnonFilter_a3_1753();
				AnonFilter_a3_1754();
				AnonFilter_a3_1755();
				AnonFilter_a3_1756();
				AnonFilter_a3_1757();
				AnonFilter_a3_1758();
				AnonFilter_a3_1759();
				AnonFilter_a3_1760();
				AnonFilter_a3_1761();
				AnonFilter_a3_1762();
				AnonFilter_a3_1763();
				AnonFilter_a3_1764();
				AnonFilter_a3_1765();
				AnonFilter_a3_1766();
				AnonFilter_a3_1767();
				AnonFilter_a3_1768();
				AnonFilter_a3_1769();
				AnonFilter_a3_1770();
				AnonFilter_a3_1771();
				AnonFilter_a3_1772();
				AnonFilter_a3_1773();
				AnonFilter_a3_1774();
				AnonFilter_a3_1775();
				AnonFilter_a3_1776();
				AnonFilter_a3_1777();
				AnonFilter_a3_1778();
				AnonFilter_a3_1779();
				AnonFilter_a3_1780();
				AnonFilter_a3_1781();
				AnonFilter_a3_1782();
				AnonFilter_a3_1783();
				AnonFilter_a3_1784();
				AnonFilter_a3_1785();
				AnonFilter_a3_1786();
				AnonFilter_a3_1787();
				AnonFilter_a3_1788();
				AnonFilter_a3_1789();
				AnonFilter_a3_1790();
				AnonFilter_a3_1791();
				AnonFilter_a3_1792();
				AnonFilter_a3_1793();
				AnonFilter_a3_1794();
				AnonFilter_a3_1795();
				AnonFilter_a3_1796();
				AnonFilter_a3_1797();
				AnonFilter_a3_1798();
			WEIGHTED_ROUND_ROBIN_Joiner_1737();
			Pre_CollapsedDataParallel_1_1726();
			WEIGHTED_ROUND_ROBIN_Splitter_1799();
				iDCT_1D_reference_fine_1801();
				iDCT_1D_reference_fine_1802();
				iDCT_1D_reference_fine_1803();
				iDCT_1D_reference_fine_1804();
				iDCT_1D_reference_fine_1805();
				iDCT_1D_reference_fine_1806();
				iDCT_1D_reference_fine_1807();
				iDCT_1D_reference_fine_1808();
			WEIGHTED_ROUND_ROBIN_Joiner_1800();
			Post_CollapsedDataParallel_2_1727();
			WEIGHTED_ROUND_ROBIN_Splitter_1809();
				iDCT_1D_reference_fine_1811();
				iDCT_1D_reference_fine_1812();
				iDCT_1D_reference_fine_1813();
				iDCT_1D_reference_fine_1814();
				iDCT_1D_reference_fine_1815();
				iDCT_1D_reference_fine_1816();
				iDCT_1D_reference_fine_1817();
				iDCT_1D_reference_fine_1818();
			WEIGHTED_ROUND_ROBIN_Joiner_1810();
			WEIGHTED_ROUND_ROBIN_Splitter_1819();
				AnonFilter_a4_1821();
				AnonFilter_a4_1822();
				AnonFilter_a4_1823();
				AnonFilter_a4_1824();
				AnonFilter_a4_1825();
				AnonFilter_a4_1826();
				AnonFilter_a4_1827();
				AnonFilter_a4_1828();
				AnonFilter_a4_1829();
				AnonFilter_a4_1830();
				AnonFilter_a4_1831();
				AnonFilter_a4_1832();
				AnonFilter_a4_1833();
				AnonFilter_a4_1834();
				AnonFilter_a4_1835();
				AnonFilter_a4_1836();
				AnonFilter_a4_1837();
				AnonFilter_a4_1838();
				AnonFilter_a4_1839();
				AnonFilter_a4_1840();
				AnonFilter_a4_1841();
				AnonFilter_a4_1842();
				AnonFilter_a4_1843();
				AnonFilter_a4_1844();
				AnonFilter_a4_1845();
				AnonFilter_a4_1846();
				AnonFilter_a4_1847();
				AnonFilter_a4_1848();
				AnonFilter_a4_1849();
				AnonFilter_a4_1850();
				AnonFilter_a4_1851();
				AnonFilter_a4_1852();
				AnonFilter_a4_1853();
				AnonFilter_a4_1854();
				AnonFilter_a4_1855();
				AnonFilter_a4_1856();
				AnonFilter_a4_1857();
				AnonFilter_a4_1858();
				AnonFilter_a4_1859();
				AnonFilter_a4_1860();
				AnonFilter_a4_1861();
				AnonFilter_a4_1862();
				AnonFilter_a4_1863();
				AnonFilter_a4_1864();
				AnonFilter_a4_1865();
				AnonFilter_a4_1866();
				AnonFilter_a4_1867();
				AnonFilter_a4_1868();
				AnonFilter_a4_1869();
				AnonFilter_a4_1870();
				AnonFilter_a4_1871();
				AnonFilter_a4_1872();
				AnonFilter_a4_1873();
				AnonFilter_a4_1874();
				AnonFilter_a4_1875();
				AnonFilter_a4_1876();
				AnonFilter_a4_1877();
				AnonFilter_a4_1878();
				AnonFilter_a4_1879();
				AnonFilter_a4_1880();
				AnonFilter_a4_1881();
			WEIGHTED_ROUND_ROBIN_Joiner_1820();
			WEIGHTED_ROUND_ROBIN_Splitter_1882();
				iDCT8x8_1D_row_fast_1884();
				iDCT8x8_1D_row_fast_1885();
				iDCT8x8_1D_row_fast_1886();
				iDCT8x8_1D_row_fast_1887();
				iDCT8x8_1D_row_fast_1888();
				iDCT8x8_1D_row_fast_1889();
				iDCT8x8_1D_row_fast_1890();
				iDCT8x8_1D_row_fast_1891();
			WEIGHTED_ROUND_ROBIN_Joiner_1883();
			iDCT8x8_1D_col_fast_1680();
		WEIGHTED_ROUND_ROBIN_Joiner_1729();
		AnonFilter_a2_1681();
	ENDFOR
	return EXIT_SUCCESS;
}
