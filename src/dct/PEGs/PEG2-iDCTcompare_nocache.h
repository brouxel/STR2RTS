#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_18676_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_18700_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_18701_t;
void AnonFilter_a0_18673();
void DUPLICATE_Splitter_18748();
void iDCT_2D_reference_coarse_18676();
void WEIGHTED_ROUND_ROBIN_Splitter_18756();
void AnonFilter_a3_18758();
void AnonFilter_a3_18759();
void WEIGHTED_ROUND_ROBIN_Joiner_18757();
void Pre_CollapsedDataParallel_1_18746();
void WEIGHTED_ROUND_ROBIN_Splitter_18760();
void iDCT_1D_reference_fine_18762();
void iDCT_1D_reference_fine_18763();
void WEIGHTED_ROUND_ROBIN_Joiner_18761();
void Post_CollapsedDataParallel_2_18747();
void WEIGHTED_ROUND_ROBIN_Splitter_18764();
void iDCT_1D_reference_fine_18766();
void iDCT_1D_reference_fine_18767();
void WEIGHTED_ROUND_ROBIN_Joiner_18765();
void WEIGHTED_ROUND_ROBIN_Splitter_18768();
void AnonFilter_a4_18770();
void AnonFilter_a4_18771();
void WEIGHTED_ROUND_ROBIN_Joiner_18769();
void WEIGHTED_ROUND_ROBIN_Splitter_18772();
void iDCT8x8_1D_row_fast_18774();
void iDCT8x8_1D_row_fast_18775();
void WEIGHTED_ROUND_ROBIN_Joiner_18773();
void iDCT8x8_1D_col_fast_18700();
void WEIGHTED_ROUND_ROBIN_Joiner_18749();
void AnonFilter_a2_18701();

#ifdef __cplusplus
}
#endif
#endif
