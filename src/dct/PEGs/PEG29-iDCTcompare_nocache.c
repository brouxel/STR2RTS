#include "PEG29-iDCTcompare_nocache.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_split[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[29];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[29];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[29];
buffer_int_t SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_12737AnonFilter_a2_12689;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12786WEIGHTED_ROUND_ROBIN_Splitter_12795;
buffer_int_t SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[29];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12776Post_CollapsedDataParallel_2_12735;
buffer_int_t AnonFilter_a0_12661DUPLICATE_Splitter_12736;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_join[3];
buffer_float_t Post_CollapsedDataParallel_2_12735WEIGHTED_ROUND_ROBIN_Splitter_12785;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12745Pre_CollapsedDataParallel_1_12734;
buffer_float_t Pre_CollapsedDataParallel_1_12734WEIGHTED_ROUND_ROBIN_Splitter_12775;


iDCT_2D_reference_coarse_12664_t iDCT_2D_reference_coarse_12664_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12777_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12778_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12779_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12780_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12781_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12782_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12783_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12784_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12787_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12788_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12789_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12790_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12791_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12792_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12793_s;
iDCT_2D_reference_coarse_12664_t iDCT_1D_reference_fine_12794_s;
iDCT8x8_1D_col_fast_12688_t iDCT8x8_1D_col_fast_12688_s;
AnonFilter_a2_12689_t AnonFilter_a2_12689_s;

void AnonFilter_a0_12661(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_12661DUPLICATE_Splitter_12736, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_12664(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_12664_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_12664_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_12746(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12747(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12748(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12749(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12750(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12751(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12752(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12753(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12754(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12755(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12756(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12757(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12758(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12759(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12760(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12761(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12762(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12763(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12764(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12765(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12766(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12767(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12768(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12769(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12770(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12771(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12772(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12773(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_12774(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[28])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12744() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12745() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12745Pre_CollapsedDataParallel_1_12734, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_12734(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_12734WEIGHTED_ROUND_ROBIN_Splitter_12775, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_12745Pre_CollapsedDataParallel_1_12734, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12745Pre_CollapsedDataParallel_1_12734) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12777(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12777_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12778(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12778_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12779(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12779_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12780(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12780_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12781(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12781_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12782(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12782_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12783(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12783_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12784(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12784_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12775() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_12734WEIGHTED_ROUND_ROBIN_Splitter_12775));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12776() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12776Post_CollapsedDataParallel_2_12735, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_12735(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_12735WEIGHTED_ROUND_ROBIN_Splitter_12785, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_12776Post_CollapsedDataParallel_2_12735, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12776Post_CollapsedDataParallel_2_12735) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12787(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12787_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12788(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12788_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12789(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12789_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12790(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12790_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12791(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12791_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12792(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12792_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12793(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12793_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_12794(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_12794_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12785() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_12735WEIGHTED_ROUND_ROBIN_Splitter_12785));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12786() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12786WEIGHTED_ROUND_ROBIN_Splitter_12795, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_12797(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12798(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12799(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12800(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12801(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12802(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12803(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12804(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12805(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12806(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12807(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12808(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12809(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12810(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12811(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12812(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12813(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12814(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12815(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12816(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12817(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12818(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12819(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12820(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12821(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12822(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12823(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12824(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_12825(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12795() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12786WEIGHTED_ROUND_ROBIN_Splitter_12795));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12796() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_12828(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0], 6) ; 
		x3 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0], 2) ; 
		x4 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0], 1) ; 
		x5 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0], 7) ; 
		x6 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0], 5) ; 
		x7 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12829(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1], 6) ; 
		x3 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1], 2) ; 
		x4 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1], 1) ; 
		x5 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1], 7) ; 
		x6 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1], 5) ; 
		x7 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12830(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2], 6) ; 
		x3 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2], 2) ; 
		x4 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2], 1) ; 
		x5 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2], 7) ; 
		x6 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2], 5) ; 
		x7 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12831(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3], 6) ; 
		x3 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3], 2) ; 
		x4 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3], 1) ; 
		x5 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3], 7) ; 
		x6 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3], 5) ; 
		x7 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12832(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4], 6) ; 
		x3 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4], 2) ; 
		x4 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4], 1) ; 
		x5 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4], 7) ; 
		x6 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4], 5) ; 
		x7 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12833(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5], 6) ; 
		x3 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5], 2) ; 
		x4 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5], 1) ; 
		x5 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5], 7) ; 
		x6 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5], 5) ; 
		x7 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12834(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6], 6) ; 
		x3 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6], 2) ; 
		x4 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6], 1) ; 
		x5 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6], 7) ; 
		x6 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6], 5) ; 
		x7 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_12835(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7], 6) ; 
		x3 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7], 2) ; 
		x4 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7], 1) ; 
		x5 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7], 7) ; 
		x6 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7], 5) ; 
		x7 = peek_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12826() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12827() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, pop_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_12688(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_12688_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_12688_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_12688_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_12688_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_12688_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_12688_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_12688_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_12688_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_12688_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_join[2], iDCT8x8_1D_col_fast_12688_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_12736() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_12661DUPLICATE_Splitter_12736);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12737() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_12737AnonFilter_a2_12689, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_12689(){
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12737AnonFilter_a2_12689) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12737AnonFilter_a2_12689) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_12737AnonFilter_a2_12689) ; 
		AnonFilter_a2_12689_s.count = (AnonFilter_a2_12689_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_12689_s.errors = (AnonFilter_a2_12689_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_12689_s.errors / AnonFilter_a2_12689_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_12689_s.errors = (AnonFilter_a2_12689_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_12689_s.errors / AnonFilter_a2_12689_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 29, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_12839_12846_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 29, __iter_init_4_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 29, __iter_init_5_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_12840_12847_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_join[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_12737AnonFilter_a2_12689);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12786WEIGHTED_ROUND_ROBIN_Splitter_12795);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin82_iDCT8x8_1D_row_fast_Fiss_12841_12848_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 29, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_12837_12844_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12776Post_CollapsedDataParallel_2_12735);
	init_buffer_int(&AnonFilter_a0_12661DUPLICATE_Splitter_12736);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_12838_12845_split[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_12827iDCT8x8_1D_col_fast_12688);
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12662_12738_12836_12843_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_12735WEIGHTED_ROUND_ROBIN_Splitter_12785);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12745Pre_CollapsedDataParallel_1_12734);
	init_buffer_float(&Pre_CollapsedDataParallel_1_12734WEIGHTED_ROUND_ROBIN_Splitter_12775);
// --- init: iDCT_2D_reference_coarse_12664
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_12664_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12777
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12777_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12778
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12778_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12779
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12779_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12780
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12780_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12781
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12781_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12782
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12782_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12783
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12783_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12784
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12784_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12787
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12787_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12788
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12788_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12789
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12789_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12790
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12790_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12791
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12791_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12792
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12792_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12793
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12793_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_12794
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_12794_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_12689
	 {
	AnonFilter_a2_12689_s.count = 0.0 ; 
	AnonFilter_a2_12689_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_12661();
		DUPLICATE_Splitter_12736();
			iDCT_2D_reference_coarse_12664();
			WEIGHTED_ROUND_ROBIN_Splitter_12744();
				AnonFilter_a3_12746();
				AnonFilter_a3_12747();
				AnonFilter_a3_12748();
				AnonFilter_a3_12749();
				AnonFilter_a3_12750();
				AnonFilter_a3_12751();
				AnonFilter_a3_12752();
				AnonFilter_a3_12753();
				AnonFilter_a3_12754();
				AnonFilter_a3_12755();
				AnonFilter_a3_12756();
				AnonFilter_a3_12757();
				AnonFilter_a3_12758();
				AnonFilter_a3_12759();
				AnonFilter_a3_12760();
				AnonFilter_a3_12761();
				AnonFilter_a3_12762();
				AnonFilter_a3_12763();
				AnonFilter_a3_12764();
				AnonFilter_a3_12765();
				AnonFilter_a3_12766();
				AnonFilter_a3_12767();
				AnonFilter_a3_12768();
				AnonFilter_a3_12769();
				AnonFilter_a3_12770();
				AnonFilter_a3_12771();
				AnonFilter_a3_12772();
				AnonFilter_a3_12773();
				AnonFilter_a3_12774();
			WEIGHTED_ROUND_ROBIN_Joiner_12745();
			Pre_CollapsedDataParallel_1_12734();
			WEIGHTED_ROUND_ROBIN_Splitter_12775();
				iDCT_1D_reference_fine_12777();
				iDCT_1D_reference_fine_12778();
				iDCT_1D_reference_fine_12779();
				iDCT_1D_reference_fine_12780();
				iDCT_1D_reference_fine_12781();
				iDCT_1D_reference_fine_12782();
				iDCT_1D_reference_fine_12783();
				iDCT_1D_reference_fine_12784();
			WEIGHTED_ROUND_ROBIN_Joiner_12776();
			Post_CollapsedDataParallel_2_12735();
			WEIGHTED_ROUND_ROBIN_Splitter_12785();
				iDCT_1D_reference_fine_12787();
				iDCT_1D_reference_fine_12788();
				iDCT_1D_reference_fine_12789();
				iDCT_1D_reference_fine_12790();
				iDCT_1D_reference_fine_12791();
				iDCT_1D_reference_fine_12792();
				iDCT_1D_reference_fine_12793();
				iDCT_1D_reference_fine_12794();
			WEIGHTED_ROUND_ROBIN_Joiner_12786();
			WEIGHTED_ROUND_ROBIN_Splitter_12795();
				AnonFilter_a4_12797();
				AnonFilter_a4_12798();
				AnonFilter_a4_12799();
				AnonFilter_a4_12800();
				AnonFilter_a4_12801();
				AnonFilter_a4_12802();
				AnonFilter_a4_12803();
				AnonFilter_a4_12804();
				AnonFilter_a4_12805();
				AnonFilter_a4_12806();
				AnonFilter_a4_12807();
				AnonFilter_a4_12808();
				AnonFilter_a4_12809();
				AnonFilter_a4_12810();
				AnonFilter_a4_12811();
				AnonFilter_a4_12812();
				AnonFilter_a4_12813();
				AnonFilter_a4_12814();
				AnonFilter_a4_12815();
				AnonFilter_a4_12816();
				AnonFilter_a4_12817();
				AnonFilter_a4_12818();
				AnonFilter_a4_12819();
				AnonFilter_a4_12820();
				AnonFilter_a4_12821();
				AnonFilter_a4_12822();
				AnonFilter_a4_12823();
				AnonFilter_a4_12824();
				AnonFilter_a4_12825();
			WEIGHTED_ROUND_ROBIN_Joiner_12796();
			WEIGHTED_ROUND_ROBIN_Splitter_12826();
				iDCT8x8_1D_row_fast_12828();
				iDCT8x8_1D_row_fast_12829();
				iDCT8x8_1D_row_fast_12830();
				iDCT8x8_1D_row_fast_12831();
				iDCT8x8_1D_row_fast_12832();
				iDCT8x8_1D_row_fast_12833();
				iDCT8x8_1D_row_fast_12834();
				iDCT8x8_1D_row_fast_12835();
			WEIGHTED_ROUND_ROBIN_Joiner_12827();
			iDCT8x8_1D_col_fast_12688();
		WEIGHTED_ROUND_ROBIN_Joiner_12737();
		AnonFilter_a2_12689();
	ENDFOR
	return EXIT_SUCCESS;
}
