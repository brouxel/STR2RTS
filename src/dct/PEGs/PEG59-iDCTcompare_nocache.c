#include "PEG59-iDCTcompare_nocache.h"

buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[59];
buffer_float_t Pre_CollapsedDataParallel_1_2534WEIGHTED_ROUND_ROBIN_Splitter_2605;
buffer_float_t Post_CollapsedDataParallel_2_2535WEIGHTED_ROUND_ROBIN_Splitter_2615;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[8];
buffer_int_t SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2616WEIGHTED_ROUND_ROBIN_Splitter_2625;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[59];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_split[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[8];
buffer_int_t SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_join[3];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[59];
buffer_int_t AnonFilter_a0_2461DUPLICATE_Splitter_2536;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2545Pre_CollapsedDataParallel_1_2534;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2606Post_CollapsedDataParallel_2_2535;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_2537AnonFilter_a2_2489;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[59];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488;


iDCT_2D_reference_coarse_2464_t iDCT_2D_reference_coarse_2464_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2607_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2608_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2609_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2610_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2611_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2612_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2613_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2614_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2617_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2618_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2619_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2620_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2621_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2622_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2623_s;
iDCT_2D_reference_coarse_2464_t iDCT_1D_reference_fine_2624_s;
iDCT8x8_1D_col_fast_2488_t iDCT8x8_1D_col_fast_2488_s;
AnonFilter_a2_2489_t AnonFilter_a2_2489_s;

void AnonFilter_a0_2461(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_2461DUPLICATE_Splitter_2536, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_2464(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_2464_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_2464_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_2546(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2547(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2548(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2549(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2550(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2551(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2552(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2553(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2554(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2555(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2556(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2557(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2558(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2559(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2560(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2561(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2562(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2563(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2564(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2565(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2566(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2567(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2568(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2569(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2570(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2571(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2572(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2573(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2574(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2575(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2576(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2577(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2578(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2579(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2580(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2581(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2582(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2583(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2584(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2585(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2586(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2587(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2588(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2589(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2590(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2591(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[45])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2592(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[46], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[46])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2593(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[47], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[47])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2594(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[48], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[48])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2595(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[49], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[49])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2596(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[50], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[50])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2597(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[51], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[51])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2598(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[52], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[52])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2599(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[53], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[53])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2600(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[54], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[54])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2601(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[55], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[55])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2602(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[56], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[56])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2603(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[57], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[57])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_2604(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[58], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[58])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2544() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 59, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2545() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 59, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2545Pre_CollapsedDataParallel_1_2534, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_2534(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_2534WEIGHTED_ROUND_ROBIN_Splitter_2605, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_2545Pre_CollapsedDataParallel_1_2534, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2545Pre_CollapsedDataParallel_1_2534) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2607(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2607_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2608(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2608_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2609(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2609_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2610(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2610_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2611(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2611_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2612(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2612_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2613(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2613_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2614(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2614_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2605() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_2534WEIGHTED_ROUND_ROBIN_Splitter_2605));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2606() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2606Post_CollapsedDataParallel_2_2535, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_2535(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_2535WEIGHTED_ROUND_ROBIN_Splitter_2615, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_2606Post_CollapsedDataParallel_2_2535, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2606Post_CollapsedDataParallel_2_2535) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2617(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2617_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2618(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2618_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2619(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2619_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2620(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2620_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2621(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2621_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2622(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2622_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2623(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2623_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_2624(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_2624_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2615() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_2535WEIGHTED_ROUND_ROBIN_Splitter_2615));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2616() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2616WEIGHTED_ROUND_ROBIN_Splitter_2625, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_2627(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2628(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2629(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2630(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2631(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2632(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2633(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2634(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2635(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2636(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2637(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2638(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2639(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2640(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2641(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2642(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2643(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2644(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2645(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2646(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2647(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2648(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2649(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2650(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2651(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2652(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2653(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2654(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2655(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2656(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2657(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2658(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2659(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2660(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2661(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2662(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2663(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2664(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2665(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2666(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2667(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2668(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2669(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2670(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2671(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2672(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2673(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[46], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[46]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2674(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[47], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[47]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2675(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[48], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[48]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2676(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[49], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[49]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2677(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[50], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[50]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2678(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[51], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[51]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2679(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[52], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[52]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2680(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[53], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[53]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2681(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[54], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[54]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2682(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[55], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[55]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2683(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[56], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[56]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2684(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[57], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[57]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_2685(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[58], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[58]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2625() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 59, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2616WEIGHTED_ROUND_ROBIN_Splitter_2625));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2626() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 59, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_2688(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0], 6) ; 
		x3 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0], 2) ; 
		x4 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0], 1) ; 
		x5 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0], 7) ; 
		x6 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0], 5) ; 
		x7 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_2689(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1], 6) ; 
		x3 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1], 2) ; 
		x4 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1], 1) ; 
		x5 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1], 7) ; 
		x6 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1], 5) ; 
		x7 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_2690(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2], 6) ; 
		x3 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2], 2) ; 
		x4 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2], 1) ; 
		x5 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2], 7) ; 
		x6 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2], 5) ; 
		x7 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_2691(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3], 6) ; 
		x3 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3], 2) ; 
		x4 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3], 1) ; 
		x5 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3], 7) ; 
		x6 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3], 5) ; 
		x7 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_2692(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4], 6) ; 
		x3 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4], 2) ; 
		x4 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4], 1) ; 
		x5 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4], 7) ; 
		x6 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4], 5) ; 
		x7 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_2693(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5], 6) ; 
		x3 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5], 2) ; 
		x4 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5], 1) ; 
		x5 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5], 7) ; 
		x6 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5], 5) ; 
		x7 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_2694(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6], 6) ; 
		x3 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6], 2) ; 
		x4 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6], 1) ; 
		x5 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6], 7) ; 
		x6 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6], 5) ; 
		x7 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_2695(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7], 6) ; 
		x3 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7], 2) ; 
		x4 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7], 1) ; 
		x5 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7], 7) ; 
		x6 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7], 5) ; 
		x7 = peek_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2686() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2687() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, pop_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_2488(){
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_2488_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_2488_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_2488_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_2488_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_2488_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_2488_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_2488_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_2488_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_2488_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_join[2], iDCT8x8_1D_col_fast_2488_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_2536() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3776, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_2461DUPLICATE_Splitter_2536);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2537() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3776, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_2537AnonFilter_a2_2489, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_2489(){
	FOR(uint32_t, __iter_steady_, 0, <, 3776, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_2537AnonFilter_a2_2489) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_2537AnonFilter_a2_2489) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_2537AnonFilter_a2_2489) ; 
		AnonFilter_a2_2489_s.count = (AnonFilter_a2_2489_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_2489_s.errors = (AnonFilter_a2_2489_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_2489_s.errors / AnonFilter_a2_2489_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_2489_s.errors = (AnonFilter_a2_2489_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_2489_s.errors / AnonFilter_a2_2489_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 59, __iter_init_0_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_2534WEIGHTED_ROUND_ROBIN_Splitter_2605);
	init_buffer_float(&Post_CollapsedDataParallel_2_2535WEIGHTED_ROUND_ROBIN_Splitter_2615);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_2699_2706_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2616WEIGHTED_ROUND_ROBIN_Splitter_2625);
	FOR(int, __iter_init_5_, 0, <, 59, __iter_init_5_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_2698_2705_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin142_iDCT8x8_1D_row_fast_Fiss_2701_2708_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_2462_2538_2696_2703_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 59, __iter_init_10_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_2700_2707_split[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_2461DUPLICATE_Splitter_2536);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2545Pre_CollapsedDataParallel_1_2534);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2606Post_CollapsedDataParallel_2_2535);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_2537AnonFilter_a2_2489);
	FOR(int, __iter_init_11_, 0, <, 59, __iter_init_11_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_2697_2704_split[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_2687iDCT8x8_1D_col_fast_2488);
// --- init: iDCT_2D_reference_coarse_2464
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_2464_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2607
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2607_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2608
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2608_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2609
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2609_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2610
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2610_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2611
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2611_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2612
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2612_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2613
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2613_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2614
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2614_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2617
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2617_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2618
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2618_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2619
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2619_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2620
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2620_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2621
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2621_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2622
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2622_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2623
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2623_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_2624
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_2624_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_2489
	 {
	AnonFilter_a2_2489_s.count = 0.0 ; 
	AnonFilter_a2_2489_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_2461();
		DUPLICATE_Splitter_2536();
			iDCT_2D_reference_coarse_2464();
			WEIGHTED_ROUND_ROBIN_Splitter_2544();
				AnonFilter_a3_2546();
				AnonFilter_a3_2547();
				AnonFilter_a3_2548();
				AnonFilter_a3_2549();
				AnonFilter_a3_2550();
				AnonFilter_a3_2551();
				AnonFilter_a3_2552();
				AnonFilter_a3_2553();
				AnonFilter_a3_2554();
				AnonFilter_a3_2555();
				AnonFilter_a3_2556();
				AnonFilter_a3_2557();
				AnonFilter_a3_2558();
				AnonFilter_a3_2559();
				AnonFilter_a3_2560();
				AnonFilter_a3_2561();
				AnonFilter_a3_2562();
				AnonFilter_a3_2563();
				AnonFilter_a3_2564();
				AnonFilter_a3_2565();
				AnonFilter_a3_2566();
				AnonFilter_a3_2567();
				AnonFilter_a3_2568();
				AnonFilter_a3_2569();
				AnonFilter_a3_2570();
				AnonFilter_a3_2571();
				AnonFilter_a3_2572();
				AnonFilter_a3_2573();
				AnonFilter_a3_2574();
				AnonFilter_a3_2575();
				AnonFilter_a3_2576();
				AnonFilter_a3_2577();
				AnonFilter_a3_2578();
				AnonFilter_a3_2579();
				AnonFilter_a3_2580();
				AnonFilter_a3_2581();
				AnonFilter_a3_2582();
				AnonFilter_a3_2583();
				AnonFilter_a3_2584();
				AnonFilter_a3_2585();
				AnonFilter_a3_2586();
				AnonFilter_a3_2587();
				AnonFilter_a3_2588();
				AnonFilter_a3_2589();
				AnonFilter_a3_2590();
				AnonFilter_a3_2591();
				AnonFilter_a3_2592();
				AnonFilter_a3_2593();
				AnonFilter_a3_2594();
				AnonFilter_a3_2595();
				AnonFilter_a3_2596();
				AnonFilter_a3_2597();
				AnonFilter_a3_2598();
				AnonFilter_a3_2599();
				AnonFilter_a3_2600();
				AnonFilter_a3_2601();
				AnonFilter_a3_2602();
				AnonFilter_a3_2603();
				AnonFilter_a3_2604();
			WEIGHTED_ROUND_ROBIN_Joiner_2545();
			Pre_CollapsedDataParallel_1_2534();
			WEIGHTED_ROUND_ROBIN_Splitter_2605();
				iDCT_1D_reference_fine_2607();
				iDCT_1D_reference_fine_2608();
				iDCT_1D_reference_fine_2609();
				iDCT_1D_reference_fine_2610();
				iDCT_1D_reference_fine_2611();
				iDCT_1D_reference_fine_2612();
				iDCT_1D_reference_fine_2613();
				iDCT_1D_reference_fine_2614();
			WEIGHTED_ROUND_ROBIN_Joiner_2606();
			Post_CollapsedDataParallel_2_2535();
			WEIGHTED_ROUND_ROBIN_Splitter_2615();
				iDCT_1D_reference_fine_2617();
				iDCT_1D_reference_fine_2618();
				iDCT_1D_reference_fine_2619();
				iDCT_1D_reference_fine_2620();
				iDCT_1D_reference_fine_2621();
				iDCT_1D_reference_fine_2622();
				iDCT_1D_reference_fine_2623();
				iDCT_1D_reference_fine_2624();
			WEIGHTED_ROUND_ROBIN_Joiner_2616();
			WEIGHTED_ROUND_ROBIN_Splitter_2625();
				AnonFilter_a4_2627();
				AnonFilter_a4_2628();
				AnonFilter_a4_2629();
				AnonFilter_a4_2630();
				AnonFilter_a4_2631();
				AnonFilter_a4_2632();
				AnonFilter_a4_2633();
				AnonFilter_a4_2634();
				AnonFilter_a4_2635();
				AnonFilter_a4_2636();
				AnonFilter_a4_2637();
				AnonFilter_a4_2638();
				AnonFilter_a4_2639();
				AnonFilter_a4_2640();
				AnonFilter_a4_2641();
				AnonFilter_a4_2642();
				AnonFilter_a4_2643();
				AnonFilter_a4_2644();
				AnonFilter_a4_2645();
				AnonFilter_a4_2646();
				AnonFilter_a4_2647();
				AnonFilter_a4_2648();
				AnonFilter_a4_2649();
				AnonFilter_a4_2650();
				AnonFilter_a4_2651();
				AnonFilter_a4_2652();
				AnonFilter_a4_2653();
				AnonFilter_a4_2654();
				AnonFilter_a4_2655();
				AnonFilter_a4_2656();
				AnonFilter_a4_2657();
				AnonFilter_a4_2658();
				AnonFilter_a4_2659();
				AnonFilter_a4_2660();
				AnonFilter_a4_2661();
				AnonFilter_a4_2662();
				AnonFilter_a4_2663();
				AnonFilter_a4_2664();
				AnonFilter_a4_2665();
				AnonFilter_a4_2666();
				AnonFilter_a4_2667();
				AnonFilter_a4_2668();
				AnonFilter_a4_2669();
				AnonFilter_a4_2670();
				AnonFilter_a4_2671();
				AnonFilter_a4_2672();
				AnonFilter_a4_2673();
				AnonFilter_a4_2674();
				AnonFilter_a4_2675();
				AnonFilter_a4_2676();
				AnonFilter_a4_2677();
				AnonFilter_a4_2678();
				AnonFilter_a4_2679();
				AnonFilter_a4_2680();
				AnonFilter_a4_2681();
				AnonFilter_a4_2682();
				AnonFilter_a4_2683();
				AnonFilter_a4_2684();
				AnonFilter_a4_2685();
			WEIGHTED_ROUND_ROBIN_Joiner_2626();
			WEIGHTED_ROUND_ROBIN_Splitter_2686();
				iDCT8x8_1D_row_fast_2688();
				iDCT8x8_1D_row_fast_2689();
				iDCT8x8_1D_row_fast_2690();
				iDCT8x8_1D_row_fast_2691();
				iDCT8x8_1D_row_fast_2692();
				iDCT8x8_1D_row_fast_2693();
				iDCT8x8_1D_row_fast_2694();
				iDCT8x8_1D_row_fast_2695();
			WEIGHTED_ROUND_ROBIN_Joiner_2687();
			iDCT8x8_1D_col_fast_2488();
		WEIGHTED_ROUND_ROBIN_Joiner_2537();
		AnonFilter_a2_2489();
	ENDFOR
	return EXIT_SUCCESS;
}
