#include "PEG45-iDCTcompare_nocache.h"

buffer_int_t SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_7745AnonFilter_a2_7697;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_7742WEIGHTED_ROUND_ROBIN_Splitter_7799;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[8];
buffer_int_t AnonFilter_a0_7669DUPLICATE_Splitter_7744;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[45];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7753Pre_CollapsedDataParallel_1_7742;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7800Post_CollapsedDataParallel_2_7743;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[45];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[45];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_join[3];
buffer_int_t SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[8];
buffer_float_t Post_CollapsedDataParallel_2_7743WEIGHTED_ROUND_ROBIN_Splitter_7809;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[45];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7810WEIGHTED_ROUND_ROBIN_Splitter_7819;


iDCT_2D_reference_coarse_7672_t iDCT_2D_reference_coarse_7672_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7801_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7802_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7803_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7804_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7805_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7806_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7807_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7808_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7811_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7812_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7813_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7814_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7815_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7816_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7817_s;
iDCT_2D_reference_coarse_7672_t iDCT_1D_reference_fine_7818_s;
iDCT8x8_1D_col_fast_7696_t iDCT8x8_1D_col_fast_7696_s;
AnonFilter_a2_7697_t AnonFilter_a2_7697_s;

void AnonFilter_a0_7669(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_7669DUPLICATE_Splitter_7744, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_7672(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_7672_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_7672_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_7754(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7755(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7756(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7757(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7758(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7759(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7760(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7761(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7762(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7763(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7764(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7765(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7766(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7767(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7768(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7769(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7770(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7771(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7772(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7773(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7774(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7775(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7776(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7777(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7778(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7779(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7780(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7781(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7782(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7783(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7784(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7785(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7786(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7787(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7788(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7789(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7790(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7791(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7792(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7793(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7794(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7795(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7796(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7797(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7798(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[44])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7752() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 45, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7753() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 45, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7753Pre_CollapsedDataParallel_1_7742, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_7742(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_7742WEIGHTED_ROUND_ROBIN_Splitter_7799, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7753Pre_CollapsedDataParallel_1_7742, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7753Pre_CollapsedDataParallel_1_7742) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7801(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7801_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7802(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7802_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7803(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7803_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7804(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7804_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7805(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7805_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7806(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7806_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7807(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7807_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7808(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7808_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7799() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7742WEIGHTED_ROUND_ROBIN_Splitter_7799));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7800() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7800Post_CollapsedDataParallel_2_7743, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_7743(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_7743WEIGHTED_ROUND_ROBIN_Splitter_7809, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7800Post_CollapsedDataParallel_2_7743, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7800Post_CollapsedDataParallel_2_7743) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7811(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7811_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7812(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7812_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7813(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7813_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7814(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7814_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7815(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7815_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7816(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7816_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7817(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7817_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7818(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7818_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7809() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_7743WEIGHTED_ROUND_ROBIN_Splitter_7809));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7810() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7810WEIGHTED_ROUND_ROBIN_Splitter_7819, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_7821(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7822(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7823(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7824(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7825(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7826(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7827(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7828(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7829(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7830(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7831(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7832(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7833(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7834(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7835(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7836(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7837(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7838(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7839(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7840(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7841(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7842(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7843(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7844(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7845(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7846(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7847(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7848(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7849(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7850(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7851(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7852(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7853(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7854(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7855(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7856(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7857(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7858(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7859(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7860(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7861(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7862(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7863(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7864(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7865(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7819() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 45, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7810WEIGHTED_ROUND_ROBIN_Splitter_7819));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7820() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 45, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_7868(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0], 6) ; 
		x3 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0], 2) ; 
		x4 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0], 1) ; 
		x5 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0], 7) ; 
		x6 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0], 5) ; 
		x7 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7869(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1], 6) ; 
		x3 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1], 2) ; 
		x4 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1], 1) ; 
		x5 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1], 7) ; 
		x6 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1], 5) ; 
		x7 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7870(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2], 6) ; 
		x3 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2], 2) ; 
		x4 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2], 1) ; 
		x5 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2], 7) ; 
		x6 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2], 5) ; 
		x7 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7871(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3], 6) ; 
		x3 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3], 2) ; 
		x4 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3], 1) ; 
		x5 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3], 7) ; 
		x6 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3], 5) ; 
		x7 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7872(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4], 6) ; 
		x3 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4], 2) ; 
		x4 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4], 1) ; 
		x5 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4], 7) ; 
		x6 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4], 5) ; 
		x7 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7873(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5], 6) ; 
		x3 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5], 2) ; 
		x4 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5], 1) ; 
		x5 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5], 7) ; 
		x6 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5], 5) ; 
		x7 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7874(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6], 6) ; 
		x3 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6], 2) ; 
		x4 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6], 1) ; 
		x5 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6], 7) ; 
		x6 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6], 5) ; 
		x7 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7875(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7], 6) ; 
		x3 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7], 2) ; 
		x4 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7], 1) ; 
		x5 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7], 7) ; 
		x6 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7], 5) ; 
		x7 = peek_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7866() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7867() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, pop_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_7696(){
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_7696_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_7696_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_7696_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_7696_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_7696_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_7696_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_7696_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_7696_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_7696_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_join[2], iDCT8x8_1D_col_fast_7696_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_7744() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2880, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_7669DUPLICATE_Splitter_7744);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7745() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2880, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_7745AnonFilter_a2_7697, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_7697(){
	FOR(uint32_t, __iter_steady_, 0, <, 2880, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7745AnonFilter_a2_7697) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7745AnonFilter_a2_7697) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7745AnonFilter_a2_7697) ; 
		AnonFilter_a2_7697_s.count = (AnonFilter_a2_7697_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_7697_s.errors = (AnonFilter_a2_7697_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_7697_s.errors / AnonFilter_a2_7697_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_7697_s.errors = (AnonFilter_a2_7697_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_7697_s.errors / AnonFilter_a2_7697_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_7745AnonFilter_a2_7697);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7742WEIGHTED_ROUND_ROBIN_Splitter_7799);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7878_7885_join[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_7669DUPLICATE_Splitter_7744);
	FOR(int, __iter_init_4_, 0, <, 45, __iter_init_4_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7753Pre_CollapsedDataParallel_1_7742);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7800Post_CollapsedDataParallel_2_7743);
	FOR(int, __iter_init_5_, 0, <, 45, __iter_init_5_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 45, __iter_init_6_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_7880_7887_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin114_iDCT8x8_1D_row_fast_Fiss_7881_7888_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_7743WEIGHTED_ROUND_ROBIN_Splitter_7809);
	FOR(int, __iter_init_9_, 0, <, 45, __iter_init_9_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_7877_7884_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7670_7746_7876_7883_split[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_7867iDCT8x8_1D_col_fast_7696);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7879_7886_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7810WEIGHTED_ROUND_ROBIN_Splitter_7819);
// --- init: iDCT_2D_reference_coarse_7672
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_7672_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7801
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7801_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7802
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7802_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7803
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7803_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7804
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7804_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7805
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7805_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7806
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7806_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7807
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7807_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7808
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7808_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7811
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7811_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7812
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7812_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7813
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7813_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7814
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7814_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7815
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7815_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7816
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7816_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7817
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7817_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7818
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7818_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_7697
	 {
	AnonFilter_a2_7697_s.count = 0.0 ; 
	AnonFilter_a2_7697_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_7669();
		DUPLICATE_Splitter_7744();
			iDCT_2D_reference_coarse_7672();
			WEIGHTED_ROUND_ROBIN_Splitter_7752();
				AnonFilter_a3_7754();
				AnonFilter_a3_7755();
				AnonFilter_a3_7756();
				AnonFilter_a3_7757();
				AnonFilter_a3_7758();
				AnonFilter_a3_7759();
				AnonFilter_a3_7760();
				AnonFilter_a3_7761();
				AnonFilter_a3_7762();
				AnonFilter_a3_7763();
				AnonFilter_a3_7764();
				AnonFilter_a3_7765();
				AnonFilter_a3_7766();
				AnonFilter_a3_7767();
				AnonFilter_a3_7768();
				AnonFilter_a3_7769();
				AnonFilter_a3_7770();
				AnonFilter_a3_7771();
				AnonFilter_a3_7772();
				AnonFilter_a3_7773();
				AnonFilter_a3_7774();
				AnonFilter_a3_7775();
				AnonFilter_a3_7776();
				AnonFilter_a3_7777();
				AnonFilter_a3_7778();
				AnonFilter_a3_7779();
				AnonFilter_a3_7780();
				AnonFilter_a3_7781();
				AnonFilter_a3_7782();
				AnonFilter_a3_7783();
				AnonFilter_a3_7784();
				AnonFilter_a3_7785();
				AnonFilter_a3_7786();
				AnonFilter_a3_7787();
				AnonFilter_a3_7788();
				AnonFilter_a3_7789();
				AnonFilter_a3_7790();
				AnonFilter_a3_7791();
				AnonFilter_a3_7792();
				AnonFilter_a3_7793();
				AnonFilter_a3_7794();
				AnonFilter_a3_7795();
				AnonFilter_a3_7796();
				AnonFilter_a3_7797();
				AnonFilter_a3_7798();
			WEIGHTED_ROUND_ROBIN_Joiner_7753();
			Pre_CollapsedDataParallel_1_7742();
			WEIGHTED_ROUND_ROBIN_Splitter_7799();
				iDCT_1D_reference_fine_7801();
				iDCT_1D_reference_fine_7802();
				iDCT_1D_reference_fine_7803();
				iDCT_1D_reference_fine_7804();
				iDCT_1D_reference_fine_7805();
				iDCT_1D_reference_fine_7806();
				iDCT_1D_reference_fine_7807();
				iDCT_1D_reference_fine_7808();
			WEIGHTED_ROUND_ROBIN_Joiner_7800();
			Post_CollapsedDataParallel_2_7743();
			WEIGHTED_ROUND_ROBIN_Splitter_7809();
				iDCT_1D_reference_fine_7811();
				iDCT_1D_reference_fine_7812();
				iDCT_1D_reference_fine_7813();
				iDCT_1D_reference_fine_7814();
				iDCT_1D_reference_fine_7815();
				iDCT_1D_reference_fine_7816();
				iDCT_1D_reference_fine_7817();
				iDCT_1D_reference_fine_7818();
			WEIGHTED_ROUND_ROBIN_Joiner_7810();
			WEIGHTED_ROUND_ROBIN_Splitter_7819();
				AnonFilter_a4_7821();
				AnonFilter_a4_7822();
				AnonFilter_a4_7823();
				AnonFilter_a4_7824();
				AnonFilter_a4_7825();
				AnonFilter_a4_7826();
				AnonFilter_a4_7827();
				AnonFilter_a4_7828();
				AnonFilter_a4_7829();
				AnonFilter_a4_7830();
				AnonFilter_a4_7831();
				AnonFilter_a4_7832();
				AnonFilter_a4_7833();
				AnonFilter_a4_7834();
				AnonFilter_a4_7835();
				AnonFilter_a4_7836();
				AnonFilter_a4_7837();
				AnonFilter_a4_7838();
				AnonFilter_a4_7839();
				AnonFilter_a4_7840();
				AnonFilter_a4_7841();
				AnonFilter_a4_7842();
				AnonFilter_a4_7843();
				AnonFilter_a4_7844();
				AnonFilter_a4_7845();
				AnonFilter_a4_7846();
				AnonFilter_a4_7847();
				AnonFilter_a4_7848();
				AnonFilter_a4_7849();
				AnonFilter_a4_7850();
				AnonFilter_a4_7851();
				AnonFilter_a4_7852();
				AnonFilter_a4_7853();
				AnonFilter_a4_7854();
				AnonFilter_a4_7855();
				AnonFilter_a4_7856();
				AnonFilter_a4_7857();
				AnonFilter_a4_7858();
				AnonFilter_a4_7859();
				AnonFilter_a4_7860();
				AnonFilter_a4_7861();
				AnonFilter_a4_7862();
				AnonFilter_a4_7863();
				AnonFilter_a4_7864();
				AnonFilter_a4_7865();
			WEIGHTED_ROUND_ROBIN_Joiner_7820();
			WEIGHTED_ROUND_ROBIN_Splitter_7866();
				iDCT8x8_1D_row_fast_7868();
				iDCT8x8_1D_row_fast_7869();
				iDCT8x8_1D_row_fast_7870();
				iDCT8x8_1D_row_fast_7871();
				iDCT8x8_1D_row_fast_7872();
				iDCT8x8_1D_row_fast_7873();
				iDCT8x8_1D_row_fast_7874();
				iDCT8x8_1D_row_fast_7875();
			WEIGHTED_ROUND_ROBIN_Joiner_7867();
			iDCT8x8_1D_col_fast_7696();
		WEIGHTED_ROUND_ROBIN_Joiner_7745();
		AnonFilter_a2_7697();
	ENDFOR
	return EXIT_SUCCESS;
}
