#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=18816 on the compile command line
#else
#if BUF_SIZEMAX < 18816
#error BUF_SIZEMAX too small, it must be at least 18816
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_6264_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_6288_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_6289_t;
void AnonFilter_a0_6261();
void DUPLICATE_Splitter_6336();
void iDCT_2D_reference_coarse_6264();
void WEIGHTED_ROUND_ROBIN_Splitter_6344();
void AnonFilter_a3_6346();
void AnonFilter_a3_6347();
void AnonFilter_a3_6348();
void AnonFilter_a3_6349();
void AnonFilter_a3_6350();
void AnonFilter_a3_6351();
void AnonFilter_a3_6352();
void AnonFilter_a3_6353();
void AnonFilter_a3_6354();
void AnonFilter_a3_6355();
void AnonFilter_a3_6356();
void AnonFilter_a3_6357();
void AnonFilter_a3_6358();
void AnonFilter_a3_6359();
void AnonFilter_a3_6360();
void AnonFilter_a3_6361();
void AnonFilter_a3_6362();
void AnonFilter_a3_6363();
void AnonFilter_a3_6364();
void AnonFilter_a3_6365();
void AnonFilter_a3_6366();
void AnonFilter_a3_6367();
void AnonFilter_a3_6368();
void AnonFilter_a3_6369();
void AnonFilter_a3_6370();
void AnonFilter_a3_6371();
void AnonFilter_a3_6372();
void AnonFilter_a3_6373();
void AnonFilter_a3_6374();
void AnonFilter_a3_6375();
void AnonFilter_a3_6376();
void AnonFilter_a3_6377();
void AnonFilter_a3_6378();
void AnonFilter_a3_6379();
void AnonFilter_a3_6380();
void AnonFilter_a3_6381();
void AnonFilter_a3_6382();
void AnonFilter_a3_6383();
void AnonFilter_a3_6384();
void AnonFilter_a3_6385();
void AnonFilter_a3_6386();
void AnonFilter_a3_6387();
void AnonFilter_a3_6388();
void AnonFilter_a3_6389();
void AnonFilter_a3_6390();
void AnonFilter_a3_6391();
void AnonFilter_a3_6392();
void AnonFilter_a3_6393();
void AnonFilter_a3_6394();
void WEIGHTED_ROUND_ROBIN_Joiner_6345();
void Pre_CollapsedDataParallel_1_6334();
void WEIGHTED_ROUND_ROBIN_Splitter_6395();
void iDCT_1D_reference_fine_6397();
void iDCT_1D_reference_fine_6398();
void iDCT_1D_reference_fine_6399();
void iDCT_1D_reference_fine_6400();
void iDCT_1D_reference_fine_6401();
void iDCT_1D_reference_fine_6402();
void iDCT_1D_reference_fine_6403();
void iDCT_1D_reference_fine_6404();
void WEIGHTED_ROUND_ROBIN_Joiner_6396();
void Post_CollapsedDataParallel_2_6335();
void WEIGHTED_ROUND_ROBIN_Splitter_6405();
void iDCT_1D_reference_fine_6407();
void iDCT_1D_reference_fine_6408();
void iDCT_1D_reference_fine_6409();
void iDCT_1D_reference_fine_6410();
void iDCT_1D_reference_fine_6411();
void iDCT_1D_reference_fine_6412();
void iDCT_1D_reference_fine_6413();
void iDCT_1D_reference_fine_6414();
void WEIGHTED_ROUND_ROBIN_Joiner_6406();
void WEIGHTED_ROUND_ROBIN_Splitter_6415();
void AnonFilter_a4_6417();
void AnonFilter_a4_6418();
void AnonFilter_a4_6419();
void AnonFilter_a4_6420();
void AnonFilter_a4_6421();
void AnonFilter_a4_6422();
void AnonFilter_a4_6423();
void AnonFilter_a4_6424();
void AnonFilter_a4_6425();
void AnonFilter_a4_6426();
void AnonFilter_a4_6427();
void AnonFilter_a4_6428();
void AnonFilter_a4_6429();
void AnonFilter_a4_6430();
void AnonFilter_a4_6431();
void AnonFilter_a4_6432();
void AnonFilter_a4_6433();
void AnonFilter_a4_6434();
void AnonFilter_a4_6435();
void AnonFilter_a4_6436();
void AnonFilter_a4_6437();
void AnonFilter_a4_6438();
void AnonFilter_a4_6439();
void AnonFilter_a4_6440();
void AnonFilter_a4_6441();
void AnonFilter_a4_6442();
void AnonFilter_a4_6443();
void AnonFilter_a4_6444();
void AnonFilter_a4_6445();
void AnonFilter_a4_6446();
void AnonFilter_a4_6447();
void AnonFilter_a4_6448();
void AnonFilter_a4_6449();
void AnonFilter_a4_6450();
void AnonFilter_a4_6451();
void AnonFilter_a4_6452();
void AnonFilter_a4_6453();
void AnonFilter_a4_6454();
void AnonFilter_a4_6455();
void AnonFilter_a4_6456();
void AnonFilter_a4_6457();
void AnonFilter_a4_6458();
void AnonFilter_a4_6459();
void AnonFilter_a4_6460();
void AnonFilter_a4_6461();
void AnonFilter_a4_6462();
void AnonFilter_a4_6463();
void AnonFilter_a4_6464();
void AnonFilter_a4_6465();
void WEIGHTED_ROUND_ROBIN_Joiner_6416();
void WEIGHTED_ROUND_ROBIN_Splitter_6466();
void iDCT8x8_1D_row_fast_6468();
void iDCT8x8_1D_row_fast_6469();
void iDCT8x8_1D_row_fast_6470();
void iDCT8x8_1D_row_fast_6471();
void iDCT8x8_1D_row_fast_6472();
void iDCT8x8_1D_row_fast_6473();
void iDCT8x8_1D_row_fast_6474();
void iDCT8x8_1D_row_fast_6475();
void WEIGHTED_ROUND_ROBIN_Joiner_6467();
void iDCT8x8_1D_col_fast_6288();
void WEIGHTED_ROUND_ROBIN_Joiner_6337();
void AnonFilter_a2_6289();

#ifdef __cplusplus
}
#endif
#endif
