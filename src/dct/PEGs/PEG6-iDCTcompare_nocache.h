#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1152 on the compile command line
#else
#if BUF_SIZEMAX < 1152
#error BUF_SIZEMAX too small, it must be at least 1152
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_18040_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_18064_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_18065_t;
void AnonFilter_a0_18037();
void DUPLICATE_Splitter_18112();
void iDCT_2D_reference_coarse_18040();
void WEIGHTED_ROUND_ROBIN_Splitter_18120();
void AnonFilter_a3_18122();
void AnonFilter_a3_18123();
void AnonFilter_a3_18124();
void AnonFilter_a3_18125();
void AnonFilter_a3_18126();
void AnonFilter_a3_18127();
void WEIGHTED_ROUND_ROBIN_Joiner_18121();
void Pre_CollapsedDataParallel_1_18110();
void WEIGHTED_ROUND_ROBIN_Splitter_18128();
void iDCT_1D_reference_fine_18130();
void iDCT_1D_reference_fine_18131();
void iDCT_1D_reference_fine_18132();
void iDCT_1D_reference_fine_18133();
void iDCT_1D_reference_fine_18134();
void iDCT_1D_reference_fine_18135();
void WEIGHTED_ROUND_ROBIN_Joiner_18129();
void Post_CollapsedDataParallel_2_18111();
void WEIGHTED_ROUND_ROBIN_Splitter_18136();
void iDCT_1D_reference_fine_18138();
void iDCT_1D_reference_fine_18139();
void iDCT_1D_reference_fine_18140();
void iDCT_1D_reference_fine_18141();
void iDCT_1D_reference_fine_18142();
void iDCT_1D_reference_fine_18143();
void WEIGHTED_ROUND_ROBIN_Joiner_18137();
void WEIGHTED_ROUND_ROBIN_Splitter_18144();
void AnonFilter_a4_18146();
void AnonFilter_a4_18147();
void AnonFilter_a4_18148();
void AnonFilter_a4_18149();
void AnonFilter_a4_18150();
void AnonFilter_a4_18151();
void WEIGHTED_ROUND_ROBIN_Joiner_18145();
void WEIGHTED_ROUND_ROBIN_Splitter_18152();
void iDCT8x8_1D_row_fast_18154();
void iDCT8x8_1D_row_fast_18155();
void iDCT8x8_1D_row_fast_18156();
void iDCT8x8_1D_row_fast_18157();
void iDCT8x8_1D_row_fast_18158();
void iDCT8x8_1D_row_fast_18159();
void WEIGHTED_ROUND_ROBIN_Joiner_18153();
void iDCT8x8_1D_col_fast_18064();
void WEIGHTED_ROUND_ROBIN_Joiner_18113();
void AnonFilter_a2_18065();

#ifdef __cplusplus
}
#endif
#endif
