#include "PEG27-iDCTcompare.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_split[3];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[27];
buffer_float_t Pre_CollapsedDataParallel_1_13286WEIGHTED_ROUND_ROBIN_Splitter_13325;
buffer_float_t Post_CollapsedDataParallel_2_13287WEIGHTED_ROUND_ROBIN_Splitter_13335;
buffer_int_t SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[27];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13297Pre_CollapsedDataParallel_1_13286;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[27];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13326Post_CollapsedDataParallel_2_13287;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_13289AnonFilter_a2_13241;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[27];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13336WEIGHTED_ROUND_ROBIN_Splitter_13345;
buffer_int_t SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_13375iDCT8x8_1D_col_fast_13240;
buffer_int_t AnonFilter_a0_13213DUPLICATE_Splitter_13288;


iDCT_2D_reference_coarse_13216_t iDCT_2D_reference_coarse_13216_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13327_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13328_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13329_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13330_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13331_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13332_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13333_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13334_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13337_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13338_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13339_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13340_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13341_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13342_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13343_s;
iDCT_2D_reference_coarse_13216_t iDCT_1D_reference_fine_13344_s;
iDCT8x8_1D_col_fast_13240_t iDCT8x8_1D_col_fast_13240_s;
AnonFilter_a2_13241_t AnonFilter_a2_13241_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_13213() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_13213DUPLICATE_Splitter_13288));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_13216_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_13216_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_13216() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_13298() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[0]));
	ENDFOR
}

void AnonFilter_a3_13299() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[1]));
	ENDFOR
}

void AnonFilter_a3_13300() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[2]));
	ENDFOR
}

void AnonFilter_a3_13301() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[3]));
	ENDFOR
}

void AnonFilter_a3_13302() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[4]));
	ENDFOR
}

void AnonFilter_a3_13303() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[5]));
	ENDFOR
}

void AnonFilter_a3_13304() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[6]));
	ENDFOR
}

void AnonFilter_a3_13305() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[7]));
	ENDFOR
}

void AnonFilter_a3_13306() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[8]));
	ENDFOR
}

void AnonFilter_a3_13307() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[9]));
	ENDFOR
}

void AnonFilter_a3_13308() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[10]));
	ENDFOR
}

void AnonFilter_a3_13309() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[11]));
	ENDFOR
}

void AnonFilter_a3_13310() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[12]));
	ENDFOR
}

void AnonFilter_a3_13311() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[13]));
	ENDFOR
}

void AnonFilter_a3_13312() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[14]));
	ENDFOR
}

void AnonFilter_a3_13313() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[15]));
	ENDFOR
}

void AnonFilter_a3_13314() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[16]));
	ENDFOR
}

void AnonFilter_a3_13315() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[17]));
	ENDFOR
}

void AnonFilter_a3_13316() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[18]));
	ENDFOR
}

void AnonFilter_a3_13317() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[19]));
	ENDFOR
}

void AnonFilter_a3_13318() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[20]));
	ENDFOR
}

void AnonFilter_a3_13319() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[21]));
	ENDFOR
}

void AnonFilter_a3_13320() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[22]));
	ENDFOR
}

void AnonFilter_a3_13321() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[23]));
	ENDFOR
}

void AnonFilter_a3_13322() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[24]));
	ENDFOR
}

void AnonFilter_a3_13323() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[25]));
	ENDFOR
}

void AnonFilter_a3_13324() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[26]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13296() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13297() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13297Pre_CollapsedDataParallel_1_13286, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_13286() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_13297Pre_CollapsedDataParallel_1_13286), &(Pre_CollapsedDataParallel_1_13286WEIGHTED_ROUND_ROBIN_Splitter_13325));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13327_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_13327() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_13328() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_13329() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_13330() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_13331() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_13332() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_13333() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_13334() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13325() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_13286WEIGHTED_ROUND_ROBIN_Splitter_13325));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13326() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13326Post_CollapsedDataParallel_2_13287, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_13287() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_13326Post_CollapsedDataParallel_2_13287), &(Post_CollapsedDataParallel_2_13287WEIGHTED_ROUND_ROBIN_Splitter_13335));
	ENDFOR
}

void iDCT_1D_reference_fine_13337() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_13338() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_13339() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_13340() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_13341() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_13342() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_13343() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_13344() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13335() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_13287WEIGHTED_ROUND_ROBIN_Splitter_13335));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13336() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13336WEIGHTED_ROUND_ROBIN_Splitter_13345, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_13347() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[0]));
	ENDFOR
}

void AnonFilter_a4_13348() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[1]));
	ENDFOR
}

void AnonFilter_a4_13349() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[2]));
	ENDFOR
}

void AnonFilter_a4_13350() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[3]));
	ENDFOR
}

void AnonFilter_a4_13351() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[4]));
	ENDFOR
}

void AnonFilter_a4_13352() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[5]));
	ENDFOR
}

void AnonFilter_a4_13353() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[6]));
	ENDFOR
}

void AnonFilter_a4_13354() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[7]));
	ENDFOR
}

void AnonFilter_a4_13355() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[8]));
	ENDFOR
}

void AnonFilter_a4_13356() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[9]));
	ENDFOR
}

void AnonFilter_a4_13357() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[10]));
	ENDFOR
}

void AnonFilter_a4_13358() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[11]));
	ENDFOR
}

void AnonFilter_a4_13359() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[12]));
	ENDFOR
}

void AnonFilter_a4_13360() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[13]));
	ENDFOR
}

void AnonFilter_a4_13361() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[14]));
	ENDFOR
}

void AnonFilter_a4_13362() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[15]));
	ENDFOR
}

void AnonFilter_a4_13363() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[16]));
	ENDFOR
}

void AnonFilter_a4_13364() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[17]));
	ENDFOR
}

void AnonFilter_a4_13365() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[18]));
	ENDFOR
}

void AnonFilter_a4_13366() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[19]));
	ENDFOR
}

void AnonFilter_a4_13367() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[20]));
	ENDFOR
}

void AnonFilter_a4_13368() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[21]));
	ENDFOR
}

void AnonFilter_a4_13369() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[22]));
	ENDFOR
}

void AnonFilter_a4_13370() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[23]));
	ENDFOR
}

void AnonFilter_a4_13371() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[24]));
	ENDFOR
}

void AnonFilter_a4_13372() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[25]));
	ENDFOR
}

void AnonFilter_a4_13373() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[26]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13345() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_13336WEIGHTED_ROUND_ROBIN_Splitter_13345));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13346() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_13376() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[0]), &(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13377() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[1]), &(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13378() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[2]), &(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13379() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[3]), &(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13380() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[4]), &(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13381() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[5]), &(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13382() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[6]), &(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13383() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[7]), &(SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13374() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_13375iDCT8x8_1D_col_fast_13240, pop_int(&SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_13240_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_13240_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_13240_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_13240_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_13240_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_13240_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_13240_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_13240_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_13240_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_13240_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_13240() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_13375iDCT8x8_1D_col_fast_13240), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_13288() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_13213DUPLICATE_Splitter_13288);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13289() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_13289AnonFilter_a2_13241, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_13241_s.count = (AnonFilter_a2_13241_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_13241_s.errors = (AnonFilter_a2_13241_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_13241_s.errors / AnonFilter_a2_13241_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_13241_s.errors = (AnonFilter_a2_13241_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_13241_s.errors / AnonFilter_a2_13241_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_13241() {
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_13289AnonFilter_a2_13241));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 27, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_13388_13395_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_13286WEIGHTED_ROUND_ROBIN_Splitter_13325);
	init_buffer_float(&Post_CollapsedDataParallel_2_13287WEIGHTED_ROUND_ROBIN_Splitter_13335);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 27, __iter_init_3_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_13388_13395_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13386_13393_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_13214_13290_13384_13391_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13297Pre_CollapsedDataParallel_1_13286);
	FOR(int, __iter_init_8_, 0, <, 27, __iter_init_8_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_13385_13392_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13326Post_CollapsedDataParallel_2_13287);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13387_13394_join[__iter_init_9_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_13289AnonFilter_a2_13241);
	FOR(int, __iter_init_10_, 0, <, 27, __iter_init_10_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_13385_13392_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13336WEIGHTED_ROUND_ROBIN_Splitter_13345);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin78_iDCT8x8_1D_row_fast_Fiss_13389_13396_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_13375iDCT8x8_1D_col_fast_13240);
	init_buffer_int(&AnonFilter_a0_13213DUPLICATE_Splitter_13288);
// --- init: iDCT_2D_reference_coarse_13216
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_13216_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13327
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13327_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13328
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13328_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13329
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13329_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13330
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13330_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13331
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13331_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13332
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13332_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13333
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13333_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13334
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13334_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13337
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13337_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13338
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13338_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13339
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13339_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13340
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13340_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13341
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13341_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13342
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13342_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13343
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13343_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13344
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13344_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_13241
	 {
	AnonFilter_a2_13241_s.count = 0.0 ; 
	AnonFilter_a2_13241_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_13213();
		DUPLICATE_Splitter_13288();
			iDCT_2D_reference_coarse_13216();
			WEIGHTED_ROUND_ROBIN_Splitter_13296();
				AnonFilter_a3_13298();
				AnonFilter_a3_13299();
				AnonFilter_a3_13300();
				AnonFilter_a3_13301();
				AnonFilter_a3_13302();
				AnonFilter_a3_13303();
				AnonFilter_a3_13304();
				AnonFilter_a3_13305();
				AnonFilter_a3_13306();
				AnonFilter_a3_13307();
				AnonFilter_a3_13308();
				AnonFilter_a3_13309();
				AnonFilter_a3_13310();
				AnonFilter_a3_13311();
				AnonFilter_a3_13312();
				AnonFilter_a3_13313();
				AnonFilter_a3_13314();
				AnonFilter_a3_13315();
				AnonFilter_a3_13316();
				AnonFilter_a3_13317();
				AnonFilter_a3_13318();
				AnonFilter_a3_13319();
				AnonFilter_a3_13320();
				AnonFilter_a3_13321();
				AnonFilter_a3_13322();
				AnonFilter_a3_13323();
				AnonFilter_a3_13324();
			WEIGHTED_ROUND_ROBIN_Joiner_13297();
			Pre_CollapsedDataParallel_1_13286();
			WEIGHTED_ROUND_ROBIN_Splitter_13325();
				iDCT_1D_reference_fine_13327();
				iDCT_1D_reference_fine_13328();
				iDCT_1D_reference_fine_13329();
				iDCT_1D_reference_fine_13330();
				iDCT_1D_reference_fine_13331();
				iDCT_1D_reference_fine_13332();
				iDCT_1D_reference_fine_13333();
				iDCT_1D_reference_fine_13334();
			WEIGHTED_ROUND_ROBIN_Joiner_13326();
			Post_CollapsedDataParallel_2_13287();
			WEIGHTED_ROUND_ROBIN_Splitter_13335();
				iDCT_1D_reference_fine_13337();
				iDCT_1D_reference_fine_13338();
				iDCT_1D_reference_fine_13339();
				iDCT_1D_reference_fine_13340();
				iDCT_1D_reference_fine_13341();
				iDCT_1D_reference_fine_13342();
				iDCT_1D_reference_fine_13343();
				iDCT_1D_reference_fine_13344();
			WEIGHTED_ROUND_ROBIN_Joiner_13336();
			WEIGHTED_ROUND_ROBIN_Splitter_13345();
				AnonFilter_a4_13347();
				AnonFilter_a4_13348();
				AnonFilter_a4_13349();
				AnonFilter_a4_13350();
				AnonFilter_a4_13351();
				AnonFilter_a4_13352();
				AnonFilter_a4_13353();
				AnonFilter_a4_13354();
				AnonFilter_a4_13355();
				AnonFilter_a4_13356();
				AnonFilter_a4_13357();
				AnonFilter_a4_13358();
				AnonFilter_a4_13359();
				AnonFilter_a4_13360();
				AnonFilter_a4_13361();
				AnonFilter_a4_13362();
				AnonFilter_a4_13363();
				AnonFilter_a4_13364();
				AnonFilter_a4_13365();
				AnonFilter_a4_13366();
				AnonFilter_a4_13367();
				AnonFilter_a4_13368();
				AnonFilter_a4_13369();
				AnonFilter_a4_13370();
				AnonFilter_a4_13371();
				AnonFilter_a4_13372();
				AnonFilter_a4_13373();
			WEIGHTED_ROUND_ROBIN_Joiner_13346();
			WEIGHTED_ROUND_ROBIN_Splitter_13374();
				iDCT8x8_1D_row_fast_13376();
				iDCT8x8_1D_row_fast_13377();
				iDCT8x8_1D_row_fast_13378();
				iDCT8x8_1D_row_fast_13379();
				iDCT8x8_1D_row_fast_13380();
				iDCT8x8_1D_row_fast_13381();
				iDCT8x8_1D_row_fast_13382();
				iDCT8x8_1D_row_fast_13383();
			WEIGHTED_ROUND_ROBIN_Joiner_13375();
			iDCT8x8_1D_col_fast_13240();
		WEIGHTED_ROUND_ROBIN_Joiner_13289();
		AnonFilter_a2_13241();
	ENDFOR
	return EXIT_SUCCESS;
}
