#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=11136 on the compile command line
#else
#if BUF_SIZEMAX < 11136
#error BUF_SIZEMAX too small, it must be at least 11136
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_2862_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_2886_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_2887_t;
void AnonFilter_a0_2859();
void DUPLICATE_Splitter_2934();
void iDCT_2D_reference_coarse_2862();
void WEIGHTED_ROUND_ROBIN_Splitter_2942();
void AnonFilter_a3_2944();
void AnonFilter_a3_2945();
void AnonFilter_a3_2946();
void AnonFilter_a3_2947();
void AnonFilter_a3_2948();
void AnonFilter_a3_2949();
void AnonFilter_a3_2950();
void AnonFilter_a3_2951();
void AnonFilter_a3_2952();
void AnonFilter_a3_2953();
void AnonFilter_a3_2954();
void AnonFilter_a3_2955();
void AnonFilter_a3_2956();
void AnonFilter_a3_2957();
void AnonFilter_a3_2958();
void AnonFilter_a3_2959();
void AnonFilter_a3_2960();
void AnonFilter_a3_2961();
void AnonFilter_a3_2962();
void AnonFilter_a3_2963();
void AnonFilter_a3_2964();
void AnonFilter_a3_2965();
void AnonFilter_a3_2966();
void AnonFilter_a3_2967();
void AnonFilter_a3_2968();
void AnonFilter_a3_2969();
void AnonFilter_a3_2970();
void AnonFilter_a3_2971();
void AnonFilter_a3_2972();
void AnonFilter_a3_2973();
void AnonFilter_a3_2974();
void AnonFilter_a3_2975();
void AnonFilter_a3_2976();
void AnonFilter_a3_2977();
void AnonFilter_a3_2978();
void AnonFilter_a3_2979();
void AnonFilter_a3_2980();
void AnonFilter_a3_2981();
void AnonFilter_a3_2982();
void AnonFilter_a3_2983();
void AnonFilter_a3_2984();
void AnonFilter_a3_2985();
void AnonFilter_a3_2986();
void AnonFilter_a3_2987();
void AnonFilter_a3_2988();
void AnonFilter_a3_2989();
void AnonFilter_a3_2990();
void AnonFilter_a3_2991();
void AnonFilter_a3_2992();
void AnonFilter_a3_2993();
void AnonFilter_a3_2994();
void AnonFilter_a3_2995();
void AnonFilter_a3_2996();
void AnonFilter_a3_2997();
void AnonFilter_a3_2998();
void AnonFilter_a3_2999();
void AnonFilter_a3_3000();
void AnonFilter_a3_3001();
void WEIGHTED_ROUND_ROBIN_Joiner_2943();
void Pre_CollapsedDataParallel_1_2932();
void WEIGHTED_ROUND_ROBIN_Splitter_3002();
void iDCT_1D_reference_fine_3004();
void iDCT_1D_reference_fine_3005();
void iDCT_1D_reference_fine_3006();
void iDCT_1D_reference_fine_3007();
void iDCT_1D_reference_fine_3008();
void iDCT_1D_reference_fine_3009();
void iDCT_1D_reference_fine_3010();
void iDCT_1D_reference_fine_3011();
void WEIGHTED_ROUND_ROBIN_Joiner_3003();
void Post_CollapsedDataParallel_2_2933();
void WEIGHTED_ROUND_ROBIN_Splitter_3012();
void iDCT_1D_reference_fine_3014();
void iDCT_1D_reference_fine_3015();
void iDCT_1D_reference_fine_3016();
void iDCT_1D_reference_fine_3017();
void iDCT_1D_reference_fine_3018();
void iDCT_1D_reference_fine_3019();
void iDCT_1D_reference_fine_3020();
void iDCT_1D_reference_fine_3021();
void WEIGHTED_ROUND_ROBIN_Joiner_3013();
void WEIGHTED_ROUND_ROBIN_Splitter_3022();
void AnonFilter_a4_3024();
void AnonFilter_a4_3025();
void AnonFilter_a4_3026();
void AnonFilter_a4_3027();
void AnonFilter_a4_3028();
void AnonFilter_a4_3029();
void AnonFilter_a4_3030();
void AnonFilter_a4_3031();
void AnonFilter_a4_3032();
void AnonFilter_a4_3033();
void AnonFilter_a4_3034();
void AnonFilter_a4_3035();
void AnonFilter_a4_3036();
void AnonFilter_a4_3037();
void AnonFilter_a4_3038();
void AnonFilter_a4_3039();
void AnonFilter_a4_3040();
void AnonFilter_a4_3041();
void AnonFilter_a4_3042();
void AnonFilter_a4_3043();
void AnonFilter_a4_3044();
void AnonFilter_a4_3045();
void AnonFilter_a4_3046();
void AnonFilter_a4_3047();
void AnonFilter_a4_3048();
void AnonFilter_a4_3049();
void AnonFilter_a4_3050();
void AnonFilter_a4_3051();
void AnonFilter_a4_3052();
void AnonFilter_a4_3053();
void AnonFilter_a4_3054();
void AnonFilter_a4_3055();
void AnonFilter_a4_3056();
void AnonFilter_a4_3057();
void AnonFilter_a4_3058();
void AnonFilter_a4_3059();
void AnonFilter_a4_3060();
void AnonFilter_a4_3061();
void AnonFilter_a4_3062();
void AnonFilter_a4_3063();
void AnonFilter_a4_3064();
void AnonFilter_a4_3065();
void AnonFilter_a4_3066();
void AnonFilter_a4_3067();
void AnonFilter_a4_3068();
void AnonFilter_a4_3069();
void AnonFilter_a4_3070();
void AnonFilter_a4_3071();
void AnonFilter_a4_3072();
void AnonFilter_a4_3073();
void AnonFilter_a4_3074();
void AnonFilter_a4_3075();
void AnonFilter_a4_3076();
void AnonFilter_a4_3077();
void AnonFilter_a4_3078();
void AnonFilter_a4_3079();
void AnonFilter_a4_3080();
void AnonFilter_a4_3081();
void WEIGHTED_ROUND_ROBIN_Joiner_3023();
void WEIGHTED_ROUND_ROBIN_Splitter_3082();
void iDCT8x8_1D_row_fast_3084();
void iDCT8x8_1D_row_fast_3085();
void iDCT8x8_1D_row_fast_3086();
void iDCT8x8_1D_row_fast_3087();
void iDCT8x8_1D_row_fast_3088();
void iDCT8x8_1D_row_fast_3089();
void iDCT8x8_1D_row_fast_3090();
void iDCT8x8_1D_row_fast_3091();
void WEIGHTED_ROUND_ROBIN_Joiner_3083();
void iDCT8x8_1D_col_fast_2886();
void WEIGHTED_ROUND_ROBIN_Joiner_2935();
void AnonFilter_a2_2887();

#ifdef __cplusplus
}
#endif
#endif
