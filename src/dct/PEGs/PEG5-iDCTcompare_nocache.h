#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1920 on the compile command line
#else
#if BUF_SIZEMAX < 1920
#error BUF_SIZEMAX too small, it must be at least 1920
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_18214_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_18238_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_18239_t;
void AnonFilter_a0_18211();
void DUPLICATE_Splitter_18286();
void iDCT_2D_reference_coarse_18214();
void WEIGHTED_ROUND_ROBIN_Splitter_18294();
void AnonFilter_a3_18296();
void AnonFilter_a3_18297();
void AnonFilter_a3_18298();
void AnonFilter_a3_18299();
void AnonFilter_a3_18300();
void WEIGHTED_ROUND_ROBIN_Joiner_18295();
void Pre_CollapsedDataParallel_1_18284();
void WEIGHTED_ROUND_ROBIN_Splitter_18301();
void iDCT_1D_reference_fine_18303();
void iDCT_1D_reference_fine_18304();
void iDCT_1D_reference_fine_18305();
void iDCT_1D_reference_fine_18306();
void iDCT_1D_reference_fine_18307();
void WEIGHTED_ROUND_ROBIN_Joiner_18302();
void Post_CollapsedDataParallel_2_18285();
void WEIGHTED_ROUND_ROBIN_Splitter_18308();
void iDCT_1D_reference_fine_18310();
void iDCT_1D_reference_fine_18311();
void iDCT_1D_reference_fine_18312();
void iDCT_1D_reference_fine_18313();
void iDCT_1D_reference_fine_18314();
void WEIGHTED_ROUND_ROBIN_Joiner_18309();
void WEIGHTED_ROUND_ROBIN_Splitter_18315();
void AnonFilter_a4_18317();
void AnonFilter_a4_18318();
void AnonFilter_a4_18319();
void AnonFilter_a4_18320();
void AnonFilter_a4_18321();
void WEIGHTED_ROUND_ROBIN_Joiner_18316();
void WEIGHTED_ROUND_ROBIN_Splitter_18322();
void iDCT8x8_1D_row_fast_18324();
void iDCT8x8_1D_row_fast_18325();
void iDCT8x8_1D_row_fast_18326();
void iDCT8x8_1D_row_fast_18327();
void iDCT8x8_1D_row_fast_18328();
void WEIGHTED_ROUND_ROBIN_Joiner_18323();
void iDCT8x8_1D_col_fast_18238();
void WEIGHTED_ROUND_ROBIN_Joiner_18287();
void AnonFilter_a2_18239();

#ifdef __cplusplus
}
#endif
#endif
