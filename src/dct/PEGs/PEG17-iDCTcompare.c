#include "PEG17-iDCTcompare.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_split[3];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[17];
buffer_int_t SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_15809AnonFilter_a2_15761;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15817Pre_CollapsedDataParallel_1_15806;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_15875iDCT8x8_1D_col_fast_15760;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[8];
buffer_int_t AnonFilter_a0_15733DUPLICATE_Splitter_15808;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15846WEIGHTED_ROUND_ROBIN_Splitter_15855;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[17];
buffer_float_t Post_CollapsedDataParallel_2_15807WEIGHTED_ROUND_ROBIN_Splitter_15845;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[17];
buffer_float_t Pre_CollapsedDataParallel_1_15806WEIGHTED_ROUND_ROBIN_Splitter_15835;
buffer_int_t SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[17];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_15836Post_CollapsedDataParallel_2_15807;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_join[3];


iDCT_2D_reference_coarse_15736_t iDCT_2D_reference_coarse_15736_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15837_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15838_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15839_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15840_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15841_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15842_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15843_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15844_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15847_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15848_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15849_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15850_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15851_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15852_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15853_s;
iDCT_2D_reference_coarse_15736_t iDCT_1D_reference_fine_15854_s;
iDCT8x8_1D_col_fast_15760_t iDCT8x8_1D_col_fast_15760_s;
AnonFilter_a2_15761_t AnonFilter_a2_15761_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_15733() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_15733DUPLICATE_Splitter_15808));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_15736_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_15736_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_15736() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_15818() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[0]));
	ENDFOR
}

void AnonFilter_a3_15819() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[1]));
	ENDFOR
}

void AnonFilter_a3_15820() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[2]));
	ENDFOR
}

void AnonFilter_a3_15821() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[3]));
	ENDFOR
}

void AnonFilter_a3_15822() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[4]));
	ENDFOR
}

void AnonFilter_a3_15823() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[5]));
	ENDFOR
}

void AnonFilter_a3_15824() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[6]));
	ENDFOR
}

void AnonFilter_a3_15825() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[7]));
	ENDFOR
}

void AnonFilter_a3_15826() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[8]));
	ENDFOR
}

void AnonFilter_a3_15827() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[9]));
	ENDFOR
}

void AnonFilter_a3_15828() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[10]));
	ENDFOR
}

void AnonFilter_a3_15829() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[11]));
	ENDFOR
}

void AnonFilter_a3_15830() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[12]));
	ENDFOR
}

void AnonFilter_a3_15831() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[13]));
	ENDFOR
}

void AnonFilter_a3_15832() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[14]));
	ENDFOR
}

void AnonFilter_a3_15833() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[15]));
	ENDFOR
}

void AnonFilter_a3_15834() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[16]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15816() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15817() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15817Pre_CollapsedDataParallel_1_15806, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_15806() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_15817Pre_CollapsedDataParallel_1_15806), &(Pre_CollapsedDataParallel_1_15806WEIGHTED_ROUND_ROBIN_Splitter_15835));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_15837_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_15837() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_15838() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_15839() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_15840() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_15841() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_15842() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_15843() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_15844() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15835() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_15806WEIGHTED_ROUND_ROBIN_Splitter_15835));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15836() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15836Post_CollapsedDataParallel_2_15807, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_15807() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_15836Post_CollapsedDataParallel_2_15807), &(Post_CollapsedDataParallel_2_15807WEIGHTED_ROUND_ROBIN_Splitter_15845));
	ENDFOR
}

void iDCT_1D_reference_fine_15847() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_15848() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_15849() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_15850() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_15851() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_15852() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_15853() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_15854() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15845() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_15807WEIGHTED_ROUND_ROBIN_Splitter_15845));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15846() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_15846WEIGHTED_ROUND_ROBIN_Splitter_15855, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_15857() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[0]));
	ENDFOR
}

void AnonFilter_a4_15858() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[1]));
	ENDFOR
}

void AnonFilter_a4_15859() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[2]));
	ENDFOR
}

void AnonFilter_a4_15860() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[3]));
	ENDFOR
}

void AnonFilter_a4_15861() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[4]));
	ENDFOR
}

void AnonFilter_a4_15862() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[5]));
	ENDFOR
}

void AnonFilter_a4_15863() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[6]));
	ENDFOR
}

void AnonFilter_a4_15864() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[7]));
	ENDFOR
}

void AnonFilter_a4_15865() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[8]));
	ENDFOR
}

void AnonFilter_a4_15866() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[9]));
	ENDFOR
}

void AnonFilter_a4_15867() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[10]));
	ENDFOR
}

void AnonFilter_a4_15868() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[11]));
	ENDFOR
}

void AnonFilter_a4_15869() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[12]));
	ENDFOR
}

void AnonFilter_a4_15870() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[13]));
	ENDFOR
}

void AnonFilter_a4_15871() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[14]));
	ENDFOR
}

void AnonFilter_a4_15872() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[15]));
	ENDFOR
}

void AnonFilter_a4_15873() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[16]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15855() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_15846WEIGHTED_ROUND_ROBIN_Splitter_15855));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15856() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_15876() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[0]), &(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15877() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[1]), &(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15878() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[2]), &(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15879() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[3]), &(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15880() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[4]), &(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15881() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[5]), &(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15882() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[6]), &(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_15883() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[7]), &(SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_15874() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15875() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_15875iDCT8x8_1D_col_fast_15760, pop_int(&SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_15760_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_15760_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_15760_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_15760_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_15760_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_15760_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_15760_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_15760_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_15760_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_15760_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_15760() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_15875iDCT8x8_1D_col_fast_15760), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_15808() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_15733DUPLICATE_Splitter_15808);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_15809() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_15809AnonFilter_a2_15761, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_15761_s.count = (AnonFilter_a2_15761_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_15761_s.errors = (AnonFilter_a2_15761_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15761_s.errors / AnonFilter_a2_15761_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_15761_s.errors = (AnonFilter_a2_15761_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_15761_s.errors / AnonFilter_a2_15761_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_15761() {
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_15809AnonFilter_a2_15761));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 17, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_15888_15895_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_split[__iter_init_2_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_15809AnonFilter_a2_15761);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15817Pre_CollapsedDataParallel_1_15806);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_15875iDCT8x8_1D_col_fast_15760);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_15886_15893_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_split[__iter_init_5_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_15733DUPLICATE_Splitter_15808);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15846WEIGHTED_ROUND_ROBIN_Splitter_15855);
	FOR(int, __iter_init_6_, 0, <, 17, __iter_init_6_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_15888_15895_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_15807WEIGHTED_ROUND_ROBIN_Splitter_15845);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_15887_15894_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 17, __iter_init_8_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_15885_15892_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_15806WEIGHTED_ROUND_ROBIN_Splitter_15835);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin58_iDCT8x8_1D_row_fast_Fiss_15889_15896_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 17, __iter_init_10_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_15885_15892_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_15836Post_CollapsedDataParallel_2_15807);
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_15734_15810_15884_15891_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_15736
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_15736_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15837
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15837_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15838
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15838_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15839
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15839_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15840
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15840_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15841
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15841_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15842
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15842_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15843
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15843_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15844
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15844_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15847
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15847_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15848
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15848_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15849
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15849_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15850
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15850_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15851
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15851_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15852
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15852_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15853
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15853_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_15854
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_15854_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_15761
	 {
	AnonFilter_a2_15761_s.count = 0.0 ; 
	AnonFilter_a2_15761_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_15733();
		DUPLICATE_Splitter_15808();
			iDCT_2D_reference_coarse_15736();
			WEIGHTED_ROUND_ROBIN_Splitter_15816();
				AnonFilter_a3_15818();
				AnonFilter_a3_15819();
				AnonFilter_a3_15820();
				AnonFilter_a3_15821();
				AnonFilter_a3_15822();
				AnonFilter_a3_15823();
				AnonFilter_a3_15824();
				AnonFilter_a3_15825();
				AnonFilter_a3_15826();
				AnonFilter_a3_15827();
				AnonFilter_a3_15828();
				AnonFilter_a3_15829();
				AnonFilter_a3_15830();
				AnonFilter_a3_15831();
				AnonFilter_a3_15832();
				AnonFilter_a3_15833();
				AnonFilter_a3_15834();
			WEIGHTED_ROUND_ROBIN_Joiner_15817();
			Pre_CollapsedDataParallel_1_15806();
			WEIGHTED_ROUND_ROBIN_Splitter_15835();
				iDCT_1D_reference_fine_15837();
				iDCT_1D_reference_fine_15838();
				iDCT_1D_reference_fine_15839();
				iDCT_1D_reference_fine_15840();
				iDCT_1D_reference_fine_15841();
				iDCT_1D_reference_fine_15842();
				iDCT_1D_reference_fine_15843();
				iDCT_1D_reference_fine_15844();
			WEIGHTED_ROUND_ROBIN_Joiner_15836();
			Post_CollapsedDataParallel_2_15807();
			WEIGHTED_ROUND_ROBIN_Splitter_15845();
				iDCT_1D_reference_fine_15847();
				iDCT_1D_reference_fine_15848();
				iDCT_1D_reference_fine_15849();
				iDCT_1D_reference_fine_15850();
				iDCT_1D_reference_fine_15851();
				iDCT_1D_reference_fine_15852();
				iDCT_1D_reference_fine_15853();
				iDCT_1D_reference_fine_15854();
			WEIGHTED_ROUND_ROBIN_Joiner_15846();
			WEIGHTED_ROUND_ROBIN_Splitter_15855();
				AnonFilter_a4_15857();
				AnonFilter_a4_15858();
				AnonFilter_a4_15859();
				AnonFilter_a4_15860();
				AnonFilter_a4_15861();
				AnonFilter_a4_15862();
				AnonFilter_a4_15863();
				AnonFilter_a4_15864();
				AnonFilter_a4_15865();
				AnonFilter_a4_15866();
				AnonFilter_a4_15867();
				AnonFilter_a4_15868();
				AnonFilter_a4_15869();
				AnonFilter_a4_15870();
				AnonFilter_a4_15871();
				AnonFilter_a4_15872();
				AnonFilter_a4_15873();
			WEIGHTED_ROUND_ROBIN_Joiner_15856();
			WEIGHTED_ROUND_ROBIN_Splitter_15874();
				iDCT8x8_1D_row_fast_15876();
				iDCT8x8_1D_row_fast_15877();
				iDCT8x8_1D_row_fast_15878();
				iDCT8x8_1D_row_fast_15879();
				iDCT8x8_1D_row_fast_15880();
				iDCT8x8_1D_row_fast_15881();
				iDCT8x8_1D_row_fast_15882();
				iDCT8x8_1D_row_fast_15883();
			WEIGHTED_ROUND_ROBIN_Joiner_15875();
			iDCT8x8_1D_col_fast_15760();
		WEIGHTED_ROUND_ROBIN_Joiner_15809();
		AnonFilter_a2_15761();
	ENDFOR
	return EXIT_SUCCESS;
}
