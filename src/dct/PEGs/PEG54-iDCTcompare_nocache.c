#include "PEG54-iDCTcompare_nocache.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4551Post_CollapsedDataParallel_2_4485;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[54];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_split[3];
buffer_int_t SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[8];
buffer_int_t AnonFilter_a0_4411DUPLICATE_Splitter_4486;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[54];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4561WEIGHTED_ROUND_ROBIN_Splitter_4570;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4495Pre_CollapsedDataParallel_1_4484;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[54];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[54];
buffer_float_t Pre_CollapsedDataParallel_1_4484WEIGHTED_ROUND_ROBIN_Splitter_4550;
buffer_float_t Post_CollapsedDataParallel_2_4485WEIGHTED_ROUND_ROBIN_Splitter_4560;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438;
buffer_int_t SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_4487AnonFilter_a2_4439;


iDCT_2D_reference_coarse_4414_t iDCT_2D_reference_coarse_4414_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4552_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4553_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4554_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4555_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4556_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4557_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4558_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4559_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4562_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4563_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4564_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4565_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4566_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4567_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4568_s;
iDCT_2D_reference_coarse_4414_t iDCT_1D_reference_fine_4569_s;
iDCT8x8_1D_col_fast_4438_t iDCT8x8_1D_col_fast_4438_s;
AnonFilter_a2_4439_t AnonFilter_a2_4439_s;

void AnonFilter_a0_4411(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_4411DUPLICATE_Splitter_4486, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_4414(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_4414_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_4414_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_4496(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4497(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4498(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4499(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4500(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4501(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4502(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4503(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4504(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4505(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4506(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4507(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4508(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4509(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4510(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4511(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4512(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4513(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4514(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4515(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4516(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4517(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4518(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4519(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4520(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4521(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4522(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4523(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4524(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4525(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4526(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4527(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4528(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4529(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4530(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4531(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4532(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4533(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4534(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4535(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4536(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4537(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4538(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4539(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4540(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4541(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[45])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4542(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[46], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[46])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4543(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[47], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[47])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4544(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[48], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[48])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4545(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[49], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[49])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4546(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[50], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[50])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4547(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[51], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[51])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4548(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[52], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[52])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_4549(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[53], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[53])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4494() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 54, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4495() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 54, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4495Pre_CollapsedDataParallel_1_4484, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4484(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_4484WEIGHTED_ROUND_ROBIN_Splitter_4550, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4495Pre_CollapsedDataParallel_1_4484, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4495Pre_CollapsedDataParallel_1_4484) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4552(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4552_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4553(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4553_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4554(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4554_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4555(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4555_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4556(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4556_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4557(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4557_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4558(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4558_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4559(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4559_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4550() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4484WEIGHTED_ROUND_ROBIN_Splitter_4550));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4551() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4551Post_CollapsedDataParallel_2_4485, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4485(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_4485WEIGHTED_ROUND_ROBIN_Splitter_4560, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4551Post_CollapsedDataParallel_2_4485, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4551Post_CollapsedDataParallel_2_4485) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4562(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4562_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4563(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4563_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4564(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4564_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4565(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4565_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4566(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4566_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4567(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4567_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4568(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4568_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_4569(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_4569_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4560() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_4485WEIGHTED_ROUND_ROBIN_Splitter_4560));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4561() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4561WEIGHTED_ROUND_ROBIN_Splitter_4570, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_4572(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4573(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4574(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4575(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4576(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4577(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4578(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4579(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4580(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4581(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4582(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4583(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4584(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4585(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4586(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4587(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4588(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4589(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4590(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4591(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4592(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4593(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4594(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4595(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4596(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4597(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4598(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4599(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4600(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4601(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4602(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4603(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4604(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4605(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4606(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4607(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4608(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4609(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4610(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4611(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4612(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4613(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4614(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4615(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4616(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4617(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4618(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[46], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[46]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4619(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[47], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[47]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4620(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[48], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[48]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4621(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[49], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[49]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4622(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[50], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[50]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4623(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[51], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[51]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4624(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[52], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[52]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_4625(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[53], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[53]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4570() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 54, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4561WEIGHTED_ROUND_ROBIN_Splitter_4570));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4571() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 54, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_4628(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0], 6) ; 
		x3 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0], 2) ; 
		x4 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0], 1) ; 
		x5 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0], 7) ; 
		x6 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0], 5) ; 
		x7 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_4629(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1], 6) ; 
		x3 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1], 2) ; 
		x4 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1], 1) ; 
		x5 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1], 7) ; 
		x6 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1], 5) ; 
		x7 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_4630(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2], 6) ; 
		x3 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2], 2) ; 
		x4 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2], 1) ; 
		x5 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2], 7) ; 
		x6 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2], 5) ; 
		x7 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_4631(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3], 6) ; 
		x3 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3], 2) ; 
		x4 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3], 1) ; 
		x5 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3], 7) ; 
		x6 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3], 5) ; 
		x7 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_4632(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4], 6) ; 
		x3 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4], 2) ; 
		x4 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4], 1) ; 
		x5 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4], 7) ; 
		x6 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4], 5) ; 
		x7 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_4633(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5], 6) ; 
		x3 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5], 2) ; 
		x4 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5], 1) ; 
		x5 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5], 7) ; 
		x6 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5], 5) ; 
		x7 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_4634(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6], 6) ; 
		x3 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6], 2) ; 
		x4 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6], 1) ; 
		x5 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6], 7) ; 
		x6 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6], 5) ; 
		x7 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_4635(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7], 6) ; 
		x3 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7], 2) ; 
		x4 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7], 1) ; 
		x5 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7], 7) ; 
		x6 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7], 5) ; 
		x7 = peek_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4626() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4627() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, pop_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_4438(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_4438_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_4438_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_4438_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_4438_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_4438_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_4438_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_4438_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_4438_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_4438_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_join[2], iDCT8x8_1D_col_fast_4438_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_4486() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_4411DUPLICATE_Splitter_4486);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4487() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_4487AnonFilter_a2_4439, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_4439(){
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_4487AnonFilter_a2_4439) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_4487AnonFilter_a2_4439) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_4487AnonFilter_a2_4439) ; 
		AnonFilter_a2_4439_s.count = (AnonFilter_a2_4439_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_4439_s.errors = (AnonFilter_a2_4439_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_4439_s.errors / AnonFilter_a2_4439_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_4439_s.errors = (AnonFilter_a2_4439_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_4439_s.errors / AnonFilter_a2_4439_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4551Post_CollapsedDataParallel_2_4485);
	FOR(int, __iter_init_0_, 0, <, 54, __iter_init_0_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_join[__iter_init_2_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_4411DUPLICATE_Splitter_4486);
	FOR(int, __iter_init_3_, 0, <, 54, __iter_init_3_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4561WEIGHTED_ROUND_ROBIN_Splitter_4570);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4495Pre_CollapsedDataParallel_1_4484);
	FOR(int, __iter_init_4_, 0, <, 54, __iter_init_4_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_4640_4647_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 54, __iter_init_5_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_4637_4644_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4484WEIGHTED_ROUND_ROBIN_Splitter_4550);
	init_buffer_float(&Post_CollapsedDataParallel_2_4485WEIGHTED_ROUND_ROBIN_Splitter_4560);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_4627iDCT8x8_1D_col_fast_4438);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_int(&SplitJoin132_iDCT8x8_1D_row_fast_Fiss_4641_4648_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_4639_4646_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_4412_4488_4636_4643_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_4638_4645_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_4487AnonFilter_a2_4439);
// --- init: iDCT_2D_reference_coarse_4414
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_4414_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4552
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4552_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4553
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4553_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4554
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4554_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4555
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4555_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4556
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4556_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4557
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4557_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4558
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4558_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4559
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4559_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4562
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4562_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4563
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4563_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4564
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4564_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4565
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4565_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4566
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4566_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4567
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4567_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4568
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4568_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_4569
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_4569_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_4439
	 {
	AnonFilter_a2_4439_s.count = 0.0 ; 
	AnonFilter_a2_4439_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_4411();
		DUPLICATE_Splitter_4486();
			iDCT_2D_reference_coarse_4414();
			WEIGHTED_ROUND_ROBIN_Splitter_4494();
				AnonFilter_a3_4496();
				AnonFilter_a3_4497();
				AnonFilter_a3_4498();
				AnonFilter_a3_4499();
				AnonFilter_a3_4500();
				AnonFilter_a3_4501();
				AnonFilter_a3_4502();
				AnonFilter_a3_4503();
				AnonFilter_a3_4504();
				AnonFilter_a3_4505();
				AnonFilter_a3_4506();
				AnonFilter_a3_4507();
				AnonFilter_a3_4508();
				AnonFilter_a3_4509();
				AnonFilter_a3_4510();
				AnonFilter_a3_4511();
				AnonFilter_a3_4512();
				AnonFilter_a3_4513();
				AnonFilter_a3_4514();
				AnonFilter_a3_4515();
				AnonFilter_a3_4516();
				AnonFilter_a3_4517();
				AnonFilter_a3_4518();
				AnonFilter_a3_4519();
				AnonFilter_a3_4520();
				AnonFilter_a3_4521();
				AnonFilter_a3_4522();
				AnonFilter_a3_4523();
				AnonFilter_a3_4524();
				AnonFilter_a3_4525();
				AnonFilter_a3_4526();
				AnonFilter_a3_4527();
				AnonFilter_a3_4528();
				AnonFilter_a3_4529();
				AnonFilter_a3_4530();
				AnonFilter_a3_4531();
				AnonFilter_a3_4532();
				AnonFilter_a3_4533();
				AnonFilter_a3_4534();
				AnonFilter_a3_4535();
				AnonFilter_a3_4536();
				AnonFilter_a3_4537();
				AnonFilter_a3_4538();
				AnonFilter_a3_4539();
				AnonFilter_a3_4540();
				AnonFilter_a3_4541();
				AnonFilter_a3_4542();
				AnonFilter_a3_4543();
				AnonFilter_a3_4544();
				AnonFilter_a3_4545();
				AnonFilter_a3_4546();
				AnonFilter_a3_4547();
				AnonFilter_a3_4548();
				AnonFilter_a3_4549();
			WEIGHTED_ROUND_ROBIN_Joiner_4495();
			Pre_CollapsedDataParallel_1_4484();
			WEIGHTED_ROUND_ROBIN_Splitter_4550();
				iDCT_1D_reference_fine_4552();
				iDCT_1D_reference_fine_4553();
				iDCT_1D_reference_fine_4554();
				iDCT_1D_reference_fine_4555();
				iDCT_1D_reference_fine_4556();
				iDCT_1D_reference_fine_4557();
				iDCT_1D_reference_fine_4558();
				iDCT_1D_reference_fine_4559();
			WEIGHTED_ROUND_ROBIN_Joiner_4551();
			Post_CollapsedDataParallel_2_4485();
			WEIGHTED_ROUND_ROBIN_Splitter_4560();
				iDCT_1D_reference_fine_4562();
				iDCT_1D_reference_fine_4563();
				iDCT_1D_reference_fine_4564();
				iDCT_1D_reference_fine_4565();
				iDCT_1D_reference_fine_4566();
				iDCT_1D_reference_fine_4567();
				iDCT_1D_reference_fine_4568();
				iDCT_1D_reference_fine_4569();
			WEIGHTED_ROUND_ROBIN_Joiner_4561();
			WEIGHTED_ROUND_ROBIN_Splitter_4570();
				AnonFilter_a4_4572();
				AnonFilter_a4_4573();
				AnonFilter_a4_4574();
				AnonFilter_a4_4575();
				AnonFilter_a4_4576();
				AnonFilter_a4_4577();
				AnonFilter_a4_4578();
				AnonFilter_a4_4579();
				AnonFilter_a4_4580();
				AnonFilter_a4_4581();
				AnonFilter_a4_4582();
				AnonFilter_a4_4583();
				AnonFilter_a4_4584();
				AnonFilter_a4_4585();
				AnonFilter_a4_4586();
				AnonFilter_a4_4587();
				AnonFilter_a4_4588();
				AnonFilter_a4_4589();
				AnonFilter_a4_4590();
				AnonFilter_a4_4591();
				AnonFilter_a4_4592();
				AnonFilter_a4_4593();
				AnonFilter_a4_4594();
				AnonFilter_a4_4595();
				AnonFilter_a4_4596();
				AnonFilter_a4_4597();
				AnonFilter_a4_4598();
				AnonFilter_a4_4599();
				AnonFilter_a4_4600();
				AnonFilter_a4_4601();
				AnonFilter_a4_4602();
				AnonFilter_a4_4603();
				AnonFilter_a4_4604();
				AnonFilter_a4_4605();
				AnonFilter_a4_4606();
				AnonFilter_a4_4607();
				AnonFilter_a4_4608();
				AnonFilter_a4_4609();
				AnonFilter_a4_4610();
				AnonFilter_a4_4611();
				AnonFilter_a4_4612();
				AnonFilter_a4_4613();
				AnonFilter_a4_4614();
				AnonFilter_a4_4615();
				AnonFilter_a4_4616();
				AnonFilter_a4_4617();
				AnonFilter_a4_4618();
				AnonFilter_a4_4619();
				AnonFilter_a4_4620();
				AnonFilter_a4_4621();
				AnonFilter_a4_4622();
				AnonFilter_a4_4623();
				AnonFilter_a4_4624();
				AnonFilter_a4_4625();
			WEIGHTED_ROUND_ROBIN_Joiner_4571();
			WEIGHTED_ROUND_ROBIN_Splitter_4626();
				iDCT8x8_1D_row_fast_4628();
				iDCT8x8_1D_row_fast_4629();
				iDCT8x8_1D_row_fast_4630();
				iDCT8x8_1D_row_fast_4631();
				iDCT8x8_1D_row_fast_4632();
				iDCT8x8_1D_row_fast_4633();
				iDCT8x8_1D_row_fast_4634();
				iDCT8x8_1D_row_fast_4635();
			WEIGHTED_ROUND_ROBIN_Joiner_4627();
			iDCT8x8_1D_col_fast_4438();
		WEIGHTED_ROUND_ROBIN_Joiner_4487();
		AnonFilter_a2_4439();
	ENDFOR
	return EXIT_SUCCESS;
}
