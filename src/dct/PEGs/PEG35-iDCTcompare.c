#include "PEG35-iDCTcompare.h"

buffer_int_t AnonFilter_a0_10909DUPLICATE_Splitter_10984;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_10985AnonFilter_a2_10937;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[8];
buffer_float_t Post_CollapsedDataParallel_2_10983WEIGHTED_ROUND_ROBIN_Splitter_11039;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[35];
buffer_float_t Pre_CollapsedDataParallel_1_10982WEIGHTED_ROUND_ROBIN_Splitter_11029;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11030Post_CollapsedDataParallel_2_10983;
buffer_int_t SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10993Pre_CollapsedDataParallel_1_10982;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[8];
buffer_int_t SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[35];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11040WEIGHTED_ROUND_ROBIN_Splitter_11049;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_11087iDCT8x8_1D_col_fast_10936;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_join[3];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[35];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_split[3];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[35];


iDCT_2D_reference_coarse_10912_t iDCT_2D_reference_coarse_10912_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11031_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11032_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11033_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11034_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11035_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11036_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11037_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11038_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11041_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11042_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11043_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11044_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11045_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11046_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11047_s;
iDCT_2D_reference_coarse_10912_t iDCT_1D_reference_fine_11048_s;
iDCT8x8_1D_col_fast_10936_t iDCT8x8_1D_col_fast_10936_s;
AnonFilter_a2_10937_t AnonFilter_a2_10937_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_10909() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_10909DUPLICATE_Splitter_10984));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_10912_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_10912_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_10912() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_10994() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[0]));
	ENDFOR
}

void AnonFilter_a3_10995() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[1]));
	ENDFOR
}

void AnonFilter_a3_10996() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[2]));
	ENDFOR
}

void AnonFilter_a3_10997() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[3]));
	ENDFOR
}

void AnonFilter_a3_10998() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[4]));
	ENDFOR
}

void AnonFilter_a3_10999() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[5]));
	ENDFOR
}

void AnonFilter_a3_11000() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[6]));
	ENDFOR
}

void AnonFilter_a3_11001() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[7]));
	ENDFOR
}

void AnonFilter_a3_11002() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[8]));
	ENDFOR
}

void AnonFilter_a3_11003() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[9]));
	ENDFOR
}

void AnonFilter_a3_11004() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[10]));
	ENDFOR
}

void AnonFilter_a3_11005() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[11]));
	ENDFOR
}

void AnonFilter_a3_11006() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[12]));
	ENDFOR
}

void AnonFilter_a3_11007() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[13]));
	ENDFOR
}

void AnonFilter_a3_11008() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[14]));
	ENDFOR
}

void AnonFilter_a3_11009() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[15]));
	ENDFOR
}

void AnonFilter_a3_11010() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[16]));
	ENDFOR
}

void AnonFilter_a3_11011() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[17]));
	ENDFOR
}

void AnonFilter_a3_11012() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[18]));
	ENDFOR
}

void AnonFilter_a3_11013() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[19]));
	ENDFOR
}

void AnonFilter_a3_11014() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[20]));
	ENDFOR
}

void AnonFilter_a3_11015() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[21]));
	ENDFOR
}

void AnonFilter_a3_11016() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[22]));
	ENDFOR
}

void AnonFilter_a3_11017() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[23]));
	ENDFOR
}

void AnonFilter_a3_11018() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[24]));
	ENDFOR
}

void AnonFilter_a3_11019() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[25]));
	ENDFOR
}

void AnonFilter_a3_11020() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[26]));
	ENDFOR
}

void AnonFilter_a3_11021() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[27]));
	ENDFOR
}

void AnonFilter_a3_11022() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[28]));
	ENDFOR
}

void AnonFilter_a3_11023() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[29]));
	ENDFOR
}

void AnonFilter_a3_11024() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[30]));
	ENDFOR
}

void AnonFilter_a3_11025() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[31]));
	ENDFOR
}

void AnonFilter_a3_11026() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[32]));
	ENDFOR
}

void AnonFilter_a3_11027() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[33]));
	ENDFOR
}

void AnonFilter_a3_11028() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[34]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10992() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 35, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10993() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 35, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10993Pre_CollapsedDataParallel_1_10982, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_10982() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_10993Pre_CollapsedDataParallel_1_10982), &(Pre_CollapsedDataParallel_1_10982WEIGHTED_ROUND_ROBIN_Splitter_11029));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_11031_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_11031() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_11032() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_11033() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_11034() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_11035() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_11036() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_11037() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_11038() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11029() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10982WEIGHTED_ROUND_ROBIN_Splitter_11029));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11030() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11030Post_CollapsedDataParallel_2_10983, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_10983() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11030Post_CollapsedDataParallel_2_10983), &(Post_CollapsedDataParallel_2_10983WEIGHTED_ROUND_ROBIN_Splitter_11039));
	ENDFOR
}

void iDCT_1D_reference_fine_11041() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_11042() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_11043() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_11044() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_11045() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_11046() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_11047() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_11048() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_10983WEIGHTED_ROUND_ROBIN_Splitter_11039));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11040() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11040WEIGHTED_ROUND_ROBIN_Splitter_11049, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_11051() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[0]));
	ENDFOR
}

void AnonFilter_a4_11052() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[1]));
	ENDFOR
}

void AnonFilter_a4_11053() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[2]));
	ENDFOR
}

void AnonFilter_a4_11054() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[3]));
	ENDFOR
}

void AnonFilter_a4_11055() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[4]));
	ENDFOR
}

void AnonFilter_a4_11056() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[5]));
	ENDFOR
}

void AnonFilter_a4_11057() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[6]));
	ENDFOR
}

void AnonFilter_a4_11058() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[7]));
	ENDFOR
}

void AnonFilter_a4_11059() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[8]));
	ENDFOR
}

void AnonFilter_a4_11060() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[9]));
	ENDFOR
}

void AnonFilter_a4_11061() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[10]));
	ENDFOR
}

void AnonFilter_a4_11062() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[11]));
	ENDFOR
}

void AnonFilter_a4_11063() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[12]));
	ENDFOR
}

void AnonFilter_a4_11064() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[13]));
	ENDFOR
}

void AnonFilter_a4_11065() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[14]));
	ENDFOR
}

void AnonFilter_a4_11066() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[15]));
	ENDFOR
}

void AnonFilter_a4_11067() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[16]));
	ENDFOR
}

void AnonFilter_a4_11068() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[17]));
	ENDFOR
}

void AnonFilter_a4_11069() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[18]));
	ENDFOR
}

void AnonFilter_a4_11070() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[19]));
	ENDFOR
}

void AnonFilter_a4_11071() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[20]));
	ENDFOR
}

void AnonFilter_a4_11072() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[21]));
	ENDFOR
}

void AnonFilter_a4_11073() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[22]));
	ENDFOR
}

void AnonFilter_a4_11074() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[23]));
	ENDFOR
}

void AnonFilter_a4_11075() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[24]));
	ENDFOR
}

void AnonFilter_a4_11076() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[25]));
	ENDFOR
}

void AnonFilter_a4_11077() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[26]));
	ENDFOR
}

void AnonFilter_a4_11078() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[27]));
	ENDFOR
}

void AnonFilter_a4_11079() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[28]));
	ENDFOR
}

void AnonFilter_a4_11080() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[29]));
	ENDFOR
}

void AnonFilter_a4_11081() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[30]));
	ENDFOR
}

void AnonFilter_a4_11082() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[31]));
	ENDFOR
}

void AnonFilter_a4_11083() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[32]));
	ENDFOR
}

void AnonFilter_a4_11084() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[33]));
	ENDFOR
}

void AnonFilter_a4_11085() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[34]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11049() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 35, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11040WEIGHTED_ROUND_ROBIN_Splitter_11049));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11050() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 35, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_11088() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[0]), &(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11089() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[1]), &(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11090() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[2]), &(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11091() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[3]), &(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11092() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[4]), &(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11093() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[5]), &(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11094() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[6]), &(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_11095() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[7]), &(SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11086() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11087() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_11087iDCT8x8_1D_col_fast_10936, pop_int(&SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_10936_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_10936_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_10936_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_10936_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_10936_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_10936_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_10936_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_10936_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_10936_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_10936_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_10936() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_11087iDCT8x8_1D_col_fast_10936), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_10984() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2240, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_10909DUPLICATE_Splitter_10984);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10985() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2240, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_10985AnonFilter_a2_10937, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_10937_s.count = (AnonFilter_a2_10937_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_10937_s.errors = (AnonFilter_a2_10937_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_10937_s.errors / AnonFilter_a2_10937_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_10937_s.errors = (AnonFilter_a2_10937_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_10937_s.errors / AnonFilter_a2_10937_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_10937() {
	FOR(uint32_t, __iter_steady_, 0, <, 2240, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_10985AnonFilter_a2_10937));
	ENDFOR
}

void __stream_init__() {
	init_buffer_int(&AnonFilter_a0_10909DUPLICATE_Splitter_10984);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_10985AnonFilter_a2_10937);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_10983WEIGHTED_ROUND_ROBIN_Splitter_11039);
	FOR(int, __iter_init_1_, 0, <, 35, __iter_init_1_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_11097_11104_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10982WEIGHTED_ROUND_ROBIN_Splitter_11029);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11030Post_CollapsedDataParallel_2_10983);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_int(&SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_11099_11106_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10993Pre_CollapsedDataParallel_1_10982);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_11098_11105_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_int(&SplitJoin94_iDCT8x8_1D_row_fast_Fiss_11101_11108_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 35, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_11100_11107_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11040WEIGHTED_ROUND_ROBIN_Splitter_11049);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_11087iDCT8x8_1D_col_fast_10936);
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 35, __iter_init_9_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_11100_11107_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10910_10986_11096_11103_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 35, __iter_init_11_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_11097_11104_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_10912
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_10912_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11031
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11031_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11032
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11032_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11033
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11033_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11034
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11034_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11035
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11035_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11036
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11036_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11037
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11037_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11038
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11038_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11041
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11041_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11042
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11042_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11043
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11043_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11044
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11044_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11045
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11045_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11046
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11046_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11047
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11047_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_11048
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_11048_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_10937
	 {
	AnonFilter_a2_10937_s.count = 0.0 ; 
	AnonFilter_a2_10937_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_10909();
		DUPLICATE_Splitter_10984();
			iDCT_2D_reference_coarse_10912();
			WEIGHTED_ROUND_ROBIN_Splitter_10992();
				AnonFilter_a3_10994();
				AnonFilter_a3_10995();
				AnonFilter_a3_10996();
				AnonFilter_a3_10997();
				AnonFilter_a3_10998();
				AnonFilter_a3_10999();
				AnonFilter_a3_11000();
				AnonFilter_a3_11001();
				AnonFilter_a3_11002();
				AnonFilter_a3_11003();
				AnonFilter_a3_11004();
				AnonFilter_a3_11005();
				AnonFilter_a3_11006();
				AnonFilter_a3_11007();
				AnonFilter_a3_11008();
				AnonFilter_a3_11009();
				AnonFilter_a3_11010();
				AnonFilter_a3_11011();
				AnonFilter_a3_11012();
				AnonFilter_a3_11013();
				AnonFilter_a3_11014();
				AnonFilter_a3_11015();
				AnonFilter_a3_11016();
				AnonFilter_a3_11017();
				AnonFilter_a3_11018();
				AnonFilter_a3_11019();
				AnonFilter_a3_11020();
				AnonFilter_a3_11021();
				AnonFilter_a3_11022();
				AnonFilter_a3_11023();
				AnonFilter_a3_11024();
				AnonFilter_a3_11025();
				AnonFilter_a3_11026();
				AnonFilter_a3_11027();
				AnonFilter_a3_11028();
			WEIGHTED_ROUND_ROBIN_Joiner_10993();
			Pre_CollapsedDataParallel_1_10982();
			WEIGHTED_ROUND_ROBIN_Splitter_11029();
				iDCT_1D_reference_fine_11031();
				iDCT_1D_reference_fine_11032();
				iDCT_1D_reference_fine_11033();
				iDCT_1D_reference_fine_11034();
				iDCT_1D_reference_fine_11035();
				iDCT_1D_reference_fine_11036();
				iDCT_1D_reference_fine_11037();
				iDCT_1D_reference_fine_11038();
			WEIGHTED_ROUND_ROBIN_Joiner_11030();
			Post_CollapsedDataParallel_2_10983();
			WEIGHTED_ROUND_ROBIN_Splitter_11039();
				iDCT_1D_reference_fine_11041();
				iDCT_1D_reference_fine_11042();
				iDCT_1D_reference_fine_11043();
				iDCT_1D_reference_fine_11044();
				iDCT_1D_reference_fine_11045();
				iDCT_1D_reference_fine_11046();
				iDCT_1D_reference_fine_11047();
				iDCT_1D_reference_fine_11048();
			WEIGHTED_ROUND_ROBIN_Joiner_11040();
			WEIGHTED_ROUND_ROBIN_Splitter_11049();
				AnonFilter_a4_11051();
				AnonFilter_a4_11052();
				AnonFilter_a4_11053();
				AnonFilter_a4_11054();
				AnonFilter_a4_11055();
				AnonFilter_a4_11056();
				AnonFilter_a4_11057();
				AnonFilter_a4_11058();
				AnonFilter_a4_11059();
				AnonFilter_a4_11060();
				AnonFilter_a4_11061();
				AnonFilter_a4_11062();
				AnonFilter_a4_11063();
				AnonFilter_a4_11064();
				AnonFilter_a4_11065();
				AnonFilter_a4_11066();
				AnonFilter_a4_11067();
				AnonFilter_a4_11068();
				AnonFilter_a4_11069();
				AnonFilter_a4_11070();
				AnonFilter_a4_11071();
				AnonFilter_a4_11072();
				AnonFilter_a4_11073();
				AnonFilter_a4_11074();
				AnonFilter_a4_11075();
				AnonFilter_a4_11076();
				AnonFilter_a4_11077();
				AnonFilter_a4_11078();
				AnonFilter_a4_11079();
				AnonFilter_a4_11080();
				AnonFilter_a4_11081();
				AnonFilter_a4_11082();
				AnonFilter_a4_11083();
				AnonFilter_a4_11084();
				AnonFilter_a4_11085();
			WEIGHTED_ROUND_ROBIN_Joiner_11050();
			WEIGHTED_ROUND_ROBIN_Splitter_11086();
				iDCT8x8_1D_row_fast_11088();
				iDCT8x8_1D_row_fast_11089();
				iDCT8x8_1D_row_fast_11090();
				iDCT8x8_1D_row_fast_11091();
				iDCT8x8_1D_row_fast_11092();
				iDCT8x8_1D_row_fast_11093();
				iDCT8x8_1D_row_fast_11094();
				iDCT8x8_1D_row_fast_11095();
			WEIGHTED_ROUND_ROBIN_Joiner_11087();
			iDCT8x8_1D_col_fast_10936();
		WEIGHTED_ROUND_ROBIN_Joiner_10985();
		AnonFilter_a2_10937();
	ENDFOR
	return EXIT_SUCCESS;
}
