#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=5760 on the compile command line
#else
#if BUF_SIZEMAX < 5760
#error BUF_SIZEMAX too small, it must be at least 5760
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_16192_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_16216_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_16217_t;
void AnonFilter_a0_16189();
void DUPLICATE_Splitter_16264();
void iDCT_2D_reference_coarse_16192();
void WEIGHTED_ROUND_ROBIN_Splitter_16272();
void AnonFilter_a3_16274();
void AnonFilter_a3_16275();
void AnonFilter_a3_16276();
void AnonFilter_a3_16277();
void AnonFilter_a3_16278();
void AnonFilter_a3_16279();
void AnonFilter_a3_16280();
void AnonFilter_a3_16281();
void AnonFilter_a3_16282();
void AnonFilter_a3_16283();
void AnonFilter_a3_16284();
void AnonFilter_a3_16285();
void AnonFilter_a3_16286();
void AnonFilter_a3_16287();
void AnonFilter_a3_16288();
void WEIGHTED_ROUND_ROBIN_Joiner_16273();
void Pre_CollapsedDataParallel_1_16262();
void WEIGHTED_ROUND_ROBIN_Splitter_16289();
void iDCT_1D_reference_fine_16291();
void iDCT_1D_reference_fine_16292();
void iDCT_1D_reference_fine_16293();
void iDCT_1D_reference_fine_16294();
void iDCT_1D_reference_fine_16295();
void iDCT_1D_reference_fine_16296();
void iDCT_1D_reference_fine_16297();
void iDCT_1D_reference_fine_16298();
void WEIGHTED_ROUND_ROBIN_Joiner_16290();
void Post_CollapsedDataParallel_2_16263();
void WEIGHTED_ROUND_ROBIN_Splitter_16299();
void iDCT_1D_reference_fine_16301();
void iDCT_1D_reference_fine_16302();
void iDCT_1D_reference_fine_16303();
void iDCT_1D_reference_fine_16304();
void iDCT_1D_reference_fine_16305();
void iDCT_1D_reference_fine_16306();
void iDCT_1D_reference_fine_16307();
void iDCT_1D_reference_fine_16308();
void WEIGHTED_ROUND_ROBIN_Joiner_16300();
void WEIGHTED_ROUND_ROBIN_Splitter_16309();
void AnonFilter_a4_16311();
void AnonFilter_a4_16312();
void AnonFilter_a4_16313();
void AnonFilter_a4_16314();
void AnonFilter_a4_16315();
void AnonFilter_a4_16316();
void AnonFilter_a4_16317();
void AnonFilter_a4_16318();
void AnonFilter_a4_16319();
void AnonFilter_a4_16320();
void AnonFilter_a4_16321();
void AnonFilter_a4_16322();
void AnonFilter_a4_16323();
void AnonFilter_a4_16324();
void AnonFilter_a4_16325();
void WEIGHTED_ROUND_ROBIN_Joiner_16310();
void WEIGHTED_ROUND_ROBIN_Splitter_16326();
void iDCT8x8_1D_row_fast_16328();
void iDCT8x8_1D_row_fast_16329();
void iDCT8x8_1D_row_fast_16330();
void iDCT8x8_1D_row_fast_16331();
void iDCT8x8_1D_row_fast_16332();
void iDCT8x8_1D_row_fast_16333();
void iDCT8x8_1D_row_fast_16334();
void iDCT8x8_1D_row_fast_16335();
void WEIGHTED_ROUND_ROBIN_Joiner_16327();
void iDCT8x8_1D_col_fast_16216();
void WEIGHTED_ROUND_ROBIN_Joiner_16265();
void AnonFilter_a2_16217();

#ifdef __cplusplus
}
#endif
#endif
