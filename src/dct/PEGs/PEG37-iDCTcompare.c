#include "PEG37-iDCTcompare.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_split[3];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[37];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[8];
buffer_int_t AnonFilter_a0_10293DUPLICATE_Splitter_10368;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[8];
buffer_int_t SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[8];
buffer_int_t SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[8];
buffer_float_t Post_CollapsedDataParallel_2_10367WEIGHTED_ROUND_ROBIN_Splitter_10425;
buffer_float_t Pre_CollapsedDataParallel_1_10366WEIGHTED_ROUND_ROBIN_Splitter_10415;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[37];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10426WEIGHTED_ROUND_ROBIN_Splitter_10435;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[37];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10377Pre_CollapsedDataParallel_1_10366;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[37];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10416Post_CollapsedDataParallel_2_10367;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_10369AnonFilter_a2_10321;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_10475iDCT8x8_1D_col_fast_10320;


iDCT_2D_reference_coarse_10296_t iDCT_2D_reference_coarse_10296_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10417_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10418_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10419_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10420_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10421_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10422_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10423_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10424_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10427_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10428_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10429_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10430_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10431_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10432_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10433_s;
iDCT_2D_reference_coarse_10296_t iDCT_1D_reference_fine_10434_s;
iDCT8x8_1D_col_fast_10320_t iDCT8x8_1D_col_fast_10320_s;
AnonFilter_a2_10321_t AnonFilter_a2_10321_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_10293() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_10293DUPLICATE_Splitter_10368));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_10296_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_10296_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_10296() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_10378() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[0]));
	ENDFOR
}

void AnonFilter_a3_10379() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[1]));
	ENDFOR
}

void AnonFilter_a3_10380() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[2]));
	ENDFOR
}

void AnonFilter_a3_10381() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[3]));
	ENDFOR
}

void AnonFilter_a3_10382() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[4]));
	ENDFOR
}

void AnonFilter_a3_10383() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[5]));
	ENDFOR
}

void AnonFilter_a3_10384() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[6]));
	ENDFOR
}

void AnonFilter_a3_10385() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[7]));
	ENDFOR
}

void AnonFilter_a3_10386() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[8]));
	ENDFOR
}

void AnonFilter_a3_10387() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[9]));
	ENDFOR
}

void AnonFilter_a3_10388() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[10]));
	ENDFOR
}

void AnonFilter_a3_10389() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[11]));
	ENDFOR
}

void AnonFilter_a3_10390() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[12]));
	ENDFOR
}

void AnonFilter_a3_10391() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[13]));
	ENDFOR
}

void AnonFilter_a3_10392() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[14]));
	ENDFOR
}

void AnonFilter_a3_10393() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[15]));
	ENDFOR
}

void AnonFilter_a3_10394() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[16]));
	ENDFOR
}

void AnonFilter_a3_10395() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[17]));
	ENDFOR
}

void AnonFilter_a3_10396() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[18]));
	ENDFOR
}

void AnonFilter_a3_10397() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[19]));
	ENDFOR
}

void AnonFilter_a3_10398() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[20]));
	ENDFOR
}

void AnonFilter_a3_10399() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[21]));
	ENDFOR
}

void AnonFilter_a3_10400() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[22]));
	ENDFOR
}

void AnonFilter_a3_10401() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[23]));
	ENDFOR
}

void AnonFilter_a3_10402() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[24]));
	ENDFOR
}

void AnonFilter_a3_10403() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[25]));
	ENDFOR
}

void AnonFilter_a3_10404() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[26]));
	ENDFOR
}

void AnonFilter_a3_10405() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[27]));
	ENDFOR
}

void AnonFilter_a3_10406() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[28]));
	ENDFOR
}

void AnonFilter_a3_10407() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[29]));
	ENDFOR
}

void AnonFilter_a3_10408() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[30]));
	ENDFOR
}

void AnonFilter_a3_10409() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[31]));
	ENDFOR
}

void AnonFilter_a3_10410() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[32]));
	ENDFOR
}

void AnonFilter_a3_10411() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[33]));
	ENDFOR
}

void AnonFilter_a3_10412() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[34]));
	ENDFOR
}

void AnonFilter_a3_10413() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[35]));
	ENDFOR
}

void AnonFilter_a3_10414() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[36]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10376() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 37, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10377() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 37, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10377Pre_CollapsedDataParallel_1_10366, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_10366() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_10377Pre_CollapsedDataParallel_1_10366), &(Pre_CollapsedDataParallel_1_10366WEIGHTED_ROUND_ROBIN_Splitter_10415));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_10417_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_10417() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_10418() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_10419() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_10420() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_10421() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_10422() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_10423() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_10424() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10415() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10366WEIGHTED_ROUND_ROBIN_Splitter_10415));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10416() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10416Post_CollapsedDataParallel_2_10367, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_10367() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_10416Post_CollapsedDataParallel_2_10367), &(Post_CollapsedDataParallel_2_10367WEIGHTED_ROUND_ROBIN_Splitter_10425));
	ENDFOR
}

void iDCT_1D_reference_fine_10427() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_10428() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_10429() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_10430() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_10431() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_10432() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_10433() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_10434() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10425() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_10367WEIGHTED_ROUND_ROBIN_Splitter_10425));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10426() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10426WEIGHTED_ROUND_ROBIN_Splitter_10435, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_10437() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[0]));
	ENDFOR
}

void AnonFilter_a4_10438() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[1]));
	ENDFOR
}

void AnonFilter_a4_10439() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[2]));
	ENDFOR
}

void AnonFilter_a4_10440() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[3]));
	ENDFOR
}

void AnonFilter_a4_10441() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[4]));
	ENDFOR
}

void AnonFilter_a4_10442() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[5]));
	ENDFOR
}

void AnonFilter_a4_10443() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[6]));
	ENDFOR
}

void AnonFilter_a4_10444() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[7]));
	ENDFOR
}

void AnonFilter_a4_10445() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[8]));
	ENDFOR
}

void AnonFilter_a4_10446() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[9]));
	ENDFOR
}

void AnonFilter_a4_10447() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[10]));
	ENDFOR
}

void AnonFilter_a4_10448() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[11]));
	ENDFOR
}

void AnonFilter_a4_10449() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[12]));
	ENDFOR
}

void AnonFilter_a4_10450() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[13]));
	ENDFOR
}

void AnonFilter_a4_10451() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[14]));
	ENDFOR
}

void AnonFilter_a4_10452() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[15]));
	ENDFOR
}

void AnonFilter_a4_10453() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[16]));
	ENDFOR
}

void AnonFilter_a4_10454() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[17]));
	ENDFOR
}

void AnonFilter_a4_10455() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[18]));
	ENDFOR
}

void AnonFilter_a4_10456() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[19]));
	ENDFOR
}

void AnonFilter_a4_10457() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[20]));
	ENDFOR
}

void AnonFilter_a4_10458() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[21]));
	ENDFOR
}

void AnonFilter_a4_10459() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[22]));
	ENDFOR
}

void AnonFilter_a4_10460() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[23]));
	ENDFOR
}

void AnonFilter_a4_10461() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[24]));
	ENDFOR
}

void AnonFilter_a4_10462() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[25]));
	ENDFOR
}

void AnonFilter_a4_10463() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[26]));
	ENDFOR
}

void AnonFilter_a4_10464() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[27]));
	ENDFOR
}

void AnonFilter_a4_10465() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[28]));
	ENDFOR
}

void AnonFilter_a4_10466() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[29]));
	ENDFOR
}

void AnonFilter_a4_10467() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[30]));
	ENDFOR
}

void AnonFilter_a4_10468() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[31]));
	ENDFOR
}

void AnonFilter_a4_10469() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[32]));
	ENDFOR
}

void AnonFilter_a4_10470() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[33]));
	ENDFOR
}

void AnonFilter_a4_10471() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[34]));
	ENDFOR
}

void AnonFilter_a4_10472() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[35]));
	ENDFOR
}

void AnonFilter_a4_10473() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[36]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10435() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 37, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10426WEIGHTED_ROUND_ROBIN_Splitter_10435));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10436() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 37, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_10476() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[0]), &(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10477() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[1]), &(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10478() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[2]), &(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10479() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[3]), &(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10480() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[4]), &(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10481() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[5]), &(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10482() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[6]), &(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_10483() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[7]), &(SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10474() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10475() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_10475iDCT8x8_1D_col_fast_10320, pop_int(&SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_10320_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_10320_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_10320_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_10320_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_10320_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_10320_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_10320_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_10320_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_10320_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_10320_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_10320() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_10475iDCT8x8_1D_col_fast_10320), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_10368() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2368, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_10293DUPLICATE_Splitter_10368);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10369() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2368, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_10369AnonFilter_a2_10321, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_10321_s.count = (AnonFilter_a2_10321_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_10321_s.errors = (AnonFilter_a2_10321_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_10321_s.errors / AnonFilter_a2_10321_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_10321_s.errors = (AnonFilter_a2_10321_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_10321_s.errors / AnonFilter_a2_10321_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_10321() {
	FOR(uint32_t, __iter_steady_, 0, <, 2368, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_10369AnonFilter_a2_10321));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 37, __iter_init_1_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_10488_10495_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_split[__iter_init_2_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_10293DUPLICATE_Splitter_10368);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_int(&SplitJoin98_iDCT8x8_1D_row_fast_Fiss_10489_10496_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_10367WEIGHTED_ROUND_ROBIN_Splitter_10425);
	init_buffer_float(&Pre_CollapsedDataParallel_1_10366WEIGHTED_ROUND_ROBIN_Splitter_10415);
	FOR(int, __iter_init_6_, 0, <, 37, __iter_init_6_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_10488_10495_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_10487_10494_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_10294_10370_10484_10491_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_10486_10493_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10426WEIGHTED_ROUND_ROBIN_Splitter_10435);
	FOR(int, __iter_init_10_, 0, <, 37, __iter_init_10_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_10485_10492_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10377Pre_CollapsedDataParallel_1_10366);
	FOR(int, __iter_init_11_, 0, <, 37, __iter_init_11_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_10485_10492_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10416Post_CollapsedDataParallel_2_10367);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_10369AnonFilter_a2_10321);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_10475iDCT8x8_1D_col_fast_10320);
// --- init: iDCT_2D_reference_coarse_10296
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_10296_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10417
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10417_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10418
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10418_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10419
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10419_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10420
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10420_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10421
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10421_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10422
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10422_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10423
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10423_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10424
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10424_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10427
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10427_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10428
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10428_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10429
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10429_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10430
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10430_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10431
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10431_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10432
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10432_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10433
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10433_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_10434
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_10434_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_10321
	 {
	AnonFilter_a2_10321_s.count = 0.0 ; 
	AnonFilter_a2_10321_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_10293();
		DUPLICATE_Splitter_10368();
			iDCT_2D_reference_coarse_10296();
			WEIGHTED_ROUND_ROBIN_Splitter_10376();
				AnonFilter_a3_10378();
				AnonFilter_a3_10379();
				AnonFilter_a3_10380();
				AnonFilter_a3_10381();
				AnonFilter_a3_10382();
				AnonFilter_a3_10383();
				AnonFilter_a3_10384();
				AnonFilter_a3_10385();
				AnonFilter_a3_10386();
				AnonFilter_a3_10387();
				AnonFilter_a3_10388();
				AnonFilter_a3_10389();
				AnonFilter_a3_10390();
				AnonFilter_a3_10391();
				AnonFilter_a3_10392();
				AnonFilter_a3_10393();
				AnonFilter_a3_10394();
				AnonFilter_a3_10395();
				AnonFilter_a3_10396();
				AnonFilter_a3_10397();
				AnonFilter_a3_10398();
				AnonFilter_a3_10399();
				AnonFilter_a3_10400();
				AnonFilter_a3_10401();
				AnonFilter_a3_10402();
				AnonFilter_a3_10403();
				AnonFilter_a3_10404();
				AnonFilter_a3_10405();
				AnonFilter_a3_10406();
				AnonFilter_a3_10407();
				AnonFilter_a3_10408();
				AnonFilter_a3_10409();
				AnonFilter_a3_10410();
				AnonFilter_a3_10411();
				AnonFilter_a3_10412();
				AnonFilter_a3_10413();
				AnonFilter_a3_10414();
			WEIGHTED_ROUND_ROBIN_Joiner_10377();
			Pre_CollapsedDataParallel_1_10366();
			WEIGHTED_ROUND_ROBIN_Splitter_10415();
				iDCT_1D_reference_fine_10417();
				iDCT_1D_reference_fine_10418();
				iDCT_1D_reference_fine_10419();
				iDCT_1D_reference_fine_10420();
				iDCT_1D_reference_fine_10421();
				iDCT_1D_reference_fine_10422();
				iDCT_1D_reference_fine_10423();
				iDCT_1D_reference_fine_10424();
			WEIGHTED_ROUND_ROBIN_Joiner_10416();
			Post_CollapsedDataParallel_2_10367();
			WEIGHTED_ROUND_ROBIN_Splitter_10425();
				iDCT_1D_reference_fine_10427();
				iDCT_1D_reference_fine_10428();
				iDCT_1D_reference_fine_10429();
				iDCT_1D_reference_fine_10430();
				iDCT_1D_reference_fine_10431();
				iDCT_1D_reference_fine_10432();
				iDCT_1D_reference_fine_10433();
				iDCT_1D_reference_fine_10434();
			WEIGHTED_ROUND_ROBIN_Joiner_10426();
			WEIGHTED_ROUND_ROBIN_Splitter_10435();
				AnonFilter_a4_10437();
				AnonFilter_a4_10438();
				AnonFilter_a4_10439();
				AnonFilter_a4_10440();
				AnonFilter_a4_10441();
				AnonFilter_a4_10442();
				AnonFilter_a4_10443();
				AnonFilter_a4_10444();
				AnonFilter_a4_10445();
				AnonFilter_a4_10446();
				AnonFilter_a4_10447();
				AnonFilter_a4_10448();
				AnonFilter_a4_10449();
				AnonFilter_a4_10450();
				AnonFilter_a4_10451();
				AnonFilter_a4_10452();
				AnonFilter_a4_10453();
				AnonFilter_a4_10454();
				AnonFilter_a4_10455();
				AnonFilter_a4_10456();
				AnonFilter_a4_10457();
				AnonFilter_a4_10458();
				AnonFilter_a4_10459();
				AnonFilter_a4_10460();
				AnonFilter_a4_10461();
				AnonFilter_a4_10462();
				AnonFilter_a4_10463();
				AnonFilter_a4_10464();
				AnonFilter_a4_10465();
				AnonFilter_a4_10466();
				AnonFilter_a4_10467();
				AnonFilter_a4_10468();
				AnonFilter_a4_10469();
				AnonFilter_a4_10470();
				AnonFilter_a4_10471();
				AnonFilter_a4_10472();
				AnonFilter_a4_10473();
			WEIGHTED_ROUND_ROBIN_Joiner_10436();
			WEIGHTED_ROUND_ROBIN_Splitter_10474();
				iDCT8x8_1D_row_fast_10476();
				iDCT8x8_1D_row_fast_10477();
				iDCT8x8_1D_row_fast_10478();
				iDCT8x8_1D_row_fast_10479();
				iDCT8x8_1D_row_fast_10480();
				iDCT8x8_1D_row_fast_10481();
				iDCT8x8_1D_row_fast_10482();
				iDCT8x8_1D_row_fast_10483();
			WEIGHTED_ROUND_ROBIN_Joiner_10475();
			iDCT8x8_1D_col_fast_10320();
		WEIGHTED_ROUND_ROBIN_Joiner_10369();
		AnonFilter_a2_10321();
	ENDFOR
	return EXIT_SUCCESS;
}
