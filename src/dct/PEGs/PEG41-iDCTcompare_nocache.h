#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=15744 on the compile command line
#else
#if BUF_SIZEMAX < 15744
#error BUF_SIZEMAX too small, it must be at least 15744
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_9016_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_9040_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_9041_t;
void AnonFilter_a0_9013();
void DUPLICATE_Splitter_9088();
void iDCT_2D_reference_coarse_9016();
void WEIGHTED_ROUND_ROBIN_Splitter_9096();
void AnonFilter_a3_9098();
void AnonFilter_a3_9099();
void AnonFilter_a3_9100();
void AnonFilter_a3_9101();
void AnonFilter_a3_9102();
void AnonFilter_a3_9103();
void AnonFilter_a3_9104();
void AnonFilter_a3_9105();
void AnonFilter_a3_9106();
void AnonFilter_a3_9107();
void AnonFilter_a3_9108();
void AnonFilter_a3_9109();
void AnonFilter_a3_9110();
void AnonFilter_a3_9111();
void AnonFilter_a3_9112();
void AnonFilter_a3_9113();
void AnonFilter_a3_9114();
void AnonFilter_a3_9115();
void AnonFilter_a3_9116();
void AnonFilter_a3_9117();
void AnonFilter_a3_9118();
void AnonFilter_a3_9119();
void AnonFilter_a3_9120();
void AnonFilter_a3_9121();
void AnonFilter_a3_9122();
void AnonFilter_a3_9123();
void AnonFilter_a3_9124();
void AnonFilter_a3_9125();
void AnonFilter_a3_9126();
void AnonFilter_a3_9127();
void AnonFilter_a3_9128();
void AnonFilter_a3_9129();
void AnonFilter_a3_9130();
void AnonFilter_a3_9131();
void AnonFilter_a3_9132();
void AnonFilter_a3_9133();
void AnonFilter_a3_9134();
void AnonFilter_a3_9135();
void AnonFilter_a3_9136();
void AnonFilter_a3_9137();
void AnonFilter_a3_9138();
void WEIGHTED_ROUND_ROBIN_Joiner_9097();
void Pre_CollapsedDataParallel_1_9086();
void WEIGHTED_ROUND_ROBIN_Splitter_9139();
void iDCT_1D_reference_fine_9141();
void iDCT_1D_reference_fine_9142();
void iDCT_1D_reference_fine_9143();
void iDCT_1D_reference_fine_9144();
void iDCT_1D_reference_fine_9145();
void iDCT_1D_reference_fine_9146();
void iDCT_1D_reference_fine_9147();
void iDCT_1D_reference_fine_9148();
void WEIGHTED_ROUND_ROBIN_Joiner_9140();
void Post_CollapsedDataParallel_2_9087();
void WEIGHTED_ROUND_ROBIN_Splitter_9149();
void iDCT_1D_reference_fine_9151();
void iDCT_1D_reference_fine_9152();
void iDCT_1D_reference_fine_9153();
void iDCT_1D_reference_fine_9154();
void iDCT_1D_reference_fine_9155();
void iDCT_1D_reference_fine_9156();
void iDCT_1D_reference_fine_9157();
void iDCT_1D_reference_fine_9158();
void WEIGHTED_ROUND_ROBIN_Joiner_9150();
void WEIGHTED_ROUND_ROBIN_Splitter_9159();
void AnonFilter_a4_9161();
void AnonFilter_a4_9162();
void AnonFilter_a4_9163();
void AnonFilter_a4_9164();
void AnonFilter_a4_9165();
void AnonFilter_a4_9166();
void AnonFilter_a4_9167();
void AnonFilter_a4_9168();
void AnonFilter_a4_9169();
void AnonFilter_a4_9170();
void AnonFilter_a4_9171();
void AnonFilter_a4_9172();
void AnonFilter_a4_9173();
void AnonFilter_a4_9174();
void AnonFilter_a4_9175();
void AnonFilter_a4_9176();
void AnonFilter_a4_9177();
void AnonFilter_a4_9178();
void AnonFilter_a4_9179();
void AnonFilter_a4_9180();
void AnonFilter_a4_9181();
void AnonFilter_a4_9182();
void AnonFilter_a4_9183();
void AnonFilter_a4_9184();
void AnonFilter_a4_9185();
void AnonFilter_a4_9186();
void AnonFilter_a4_9187();
void AnonFilter_a4_9188();
void AnonFilter_a4_9189();
void AnonFilter_a4_9190();
void AnonFilter_a4_9191();
void AnonFilter_a4_9192();
void AnonFilter_a4_9193();
void AnonFilter_a4_9194();
void AnonFilter_a4_9195();
void AnonFilter_a4_9196();
void AnonFilter_a4_9197();
void AnonFilter_a4_9198();
void AnonFilter_a4_9199();
void AnonFilter_a4_9200();
void AnonFilter_a4_9201();
void WEIGHTED_ROUND_ROBIN_Joiner_9160();
void WEIGHTED_ROUND_ROBIN_Splitter_9202();
void iDCT8x8_1D_row_fast_9204();
void iDCT8x8_1D_row_fast_9205();
void iDCT8x8_1D_row_fast_9206();
void iDCT8x8_1D_row_fast_9207();
void iDCT8x8_1D_row_fast_9208();
void iDCT8x8_1D_row_fast_9209();
void iDCT8x8_1D_row_fast_9210();
void iDCT8x8_1D_row_fast_9211();
void WEIGHTED_ROUND_ROBIN_Joiner_9203();
void iDCT8x8_1D_col_fast_9040();
void WEIGHTED_ROUND_ROBIN_Joiner_9089();
void AnonFilter_a2_9041();

#ifdef __cplusplus
}
#endif
#endif
