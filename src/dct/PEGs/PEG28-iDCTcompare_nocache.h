#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2688 on the compile command line
#else
#if BUF_SIZEMAX < 2688
#error BUF_SIZEMAX too small, it must be at least 2688
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_12942_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_12966_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_12967_t;
void AnonFilter_a0_12939();
void DUPLICATE_Splitter_13014();
void iDCT_2D_reference_coarse_12942();
void WEIGHTED_ROUND_ROBIN_Splitter_13022();
void AnonFilter_a3_13024();
void AnonFilter_a3_13025();
void AnonFilter_a3_13026();
void AnonFilter_a3_13027();
void AnonFilter_a3_13028();
void AnonFilter_a3_13029();
void AnonFilter_a3_13030();
void AnonFilter_a3_13031();
void AnonFilter_a3_13032();
void AnonFilter_a3_13033();
void AnonFilter_a3_13034();
void AnonFilter_a3_13035();
void AnonFilter_a3_13036();
void AnonFilter_a3_13037();
void AnonFilter_a3_13038();
void AnonFilter_a3_13039();
void AnonFilter_a3_13040();
void AnonFilter_a3_13041();
void AnonFilter_a3_13042();
void AnonFilter_a3_13043();
void AnonFilter_a3_13044();
void AnonFilter_a3_13045();
void AnonFilter_a3_13046();
void AnonFilter_a3_13047();
void AnonFilter_a3_13048();
void AnonFilter_a3_13049();
void AnonFilter_a3_13050();
void AnonFilter_a3_13051();
void WEIGHTED_ROUND_ROBIN_Joiner_13023();
void Pre_CollapsedDataParallel_1_13012();
void WEIGHTED_ROUND_ROBIN_Splitter_13052();
void iDCT_1D_reference_fine_13054();
void iDCT_1D_reference_fine_13055();
void iDCT_1D_reference_fine_13056();
void iDCT_1D_reference_fine_13057();
void iDCT_1D_reference_fine_13058();
void iDCT_1D_reference_fine_13059();
void iDCT_1D_reference_fine_13060();
void iDCT_1D_reference_fine_13061();
void WEIGHTED_ROUND_ROBIN_Joiner_13053();
void Post_CollapsedDataParallel_2_13013();
void WEIGHTED_ROUND_ROBIN_Splitter_13062();
void iDCT_1D_reference_fine_13064();
void iDCT_1D_reference_fine_13065();
void iDCT_1D_reference_fine_13066();
void iDCT_1D_reference_fine_13067();
void iDCT_1D_reference_fine_13068();
void iDCT_1D_reference_fine_13069();
void iDCT_1D_reference_fine_13070();
void iDCT_1D_reference_fine_13071();
void WEIGHTED_ROUND_ROBIN_Joiner_13063();
void WEIGHTED_ROUND_ROBIN_Splitter_13072();
void AnonFilter_a4_13074();
void AnonFilter_a4_13075();
void AnonFilter_a4_13076();
void AnonFilter_a4_13077();
void AnonFilter_a4_13078();
void AnonFilter_a4_13079();
void AnonFilter_a4_13080();
void AnonFilter_a4_13081();
void AnonFilter_a4_13082();
void AnonFilter_a4_13083();
void AnonFilter_a4_13084();
void AnonFilter_a4_13085();
void AnonFilter_a4_13086();
void AnonFilter_a4_13087();
void AnonFilter_a4_13088();
void AnonFilter_a4_13089();
void AnonFilter_a4_13090();
void AnonFilter_a4_13091();
void AnonFilter_a4_13092();
void AnonFilter_a4_13093();
void AnonFilter_a4_13094();
void AnonFilter_a4_13095();
void AnonFilter_a4_13096();
void AnonFilter_a4_13097();
void AnonFilter_a4_13098();
void AnonFilter_a4_13099();
void AnonFilter_a4_13100();
void AnonFilter_a4_13101();
void WEIGHTED_ROUND_ROBIN_Joiner_13073();
void WEIGHTED_ROUND_ROBIN_Splitter_13102();
void iDCT8x8_1D_row_fast_13104();
void iDCT8x8_1D_row_fast_13105();
void iDCT8x8_1D_row_fast_13106();
void iDCT8x8_1D_row_fast_13107();
void iDCT8x8_1D_row_fast_13108();
void iDCT8x8_1D_row_fast_13109();
void iDCT8x8_1D_row_fast_13110();
void iDCT8x8_1D_row_fast_13111();
void WEIGHTED_ROUND_ROBIN_Joiner_13103();
void iDCT8x8_1D_col_fast_12966();
void WEIGHTED_ROUND_ROBIN_Joiner_13015();
void AnonFilter_a2_12967();

#ifdef __cplusplus
}
#endif
#endif
