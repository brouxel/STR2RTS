#include "PEG8-iDCTcompare.h"

buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[8];
buffer_int_t AnonFilter_a0_17659DUPLICATE_Splitter_17734;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_17783iDCT8x8_1D_col_fast_17686;
buffer_float_t Post_CollapsedDataParallel_2_17733WEIGHTED_ROUND_ROBIN_Splitter_17762;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_17732WEIGHTED_ROUND_ROBIN_Splitter_17752;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17763WEIGHTED_ROUND_ROBIN_Splitter_17772;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_join[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[8];
buffer_int_t SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17743Pre_CollapsedDataParallel_1_17732;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_split[3];
buffer_int_t SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_17735AnonFilter_a2_17687;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17753Post_CollapsedDataParallel_2_17733;


iDCT_2D_reference_coarse_17662_t iDCT_2D_reference_coarse_17662_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17754_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17755_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17756_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17757_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17758_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17759_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17760_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17761_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17764_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17765_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17766_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17767_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17768_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17769_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17770_s;
iDCT_2D_reference_coarse_17662_t iDCT_1D_reference_fine_17771_s;
iDCT8x8_1D_col_fast_17686_t iDCT8x8_1D_col_fast_17686_s;
AnonFilter_a2_17687_t AnonFilter_a2_17687_s;

void AnonFilter_a0(buffer_int_t *chanout) {
	FOR(int, i, 0,  < , 64, i++) {
		push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
	}
	ENDFOR
}


void AnonFilter_a0_17659() {
	AnonFilter_a0(&(AnonFilter_a0_17659DUPLICATE_Splitter_17734));
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
	float block_x[8][8];
	FOR(int, i, 0,  < , 8, i++) {
		FOR(int, j, 0,  < , 8, j++) {
			block_x[i][j] = 0.0 ; 
			FOR(int, k, 0,  < , 8, k++) {
				block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_17662_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
			}
			ENDFOR
		}
		ENDFOR
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		FOR(int, j, 0,  < , 8, j++) {
			float block_y = 0.0;
			FOR(int, k, 0,  < , 8, k++) {
				block_y = (block_y + (iDCT_2D_reference_coarse_17662_s.coeff[k][i] * block_x[k][j])) ; 
			}
			ENDFOR
			block_y = ((float) floor((block_y + 0.5))) ; 
			push_int(&(*chanout), ((int) block_y)) ; 
		}
		ENDFOR
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_int(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT_2D_reference_coarse_17662() {
	iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_join[0]));
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_17744() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[0]));
	ENDFOR
}

void AnonFilter_a3_17745() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[1]));
	ENDFOR
}

void AnonFilter_a3_17746() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[2]));
	ENDFOR
}

void AnonFilter_a3_17747() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[3]));
	ENDFOR
}

void AnonFilter_a3_17748() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[4]));
	ENDFOR
}

void AnonFilter_a3_17749() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[5]));
	ENDFOR
}

void AnonFilter_a3_17750() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[6]));
	ENDFOR
}

void AnonFilter_a3_17751() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17742() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17743() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17743Pre_CollapsedDataParallel_1_17732, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
		FOR(int, _i, 0,  < , 8, _i++) {
			push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}
}
	pop_float(&(*chanin)) ; 
}


void Pre_CollapsedDataParallel_1_17732() {
	Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_17743Pre_CollapsedDataParallel_1_17732), &(Pre_CollapsedDataParallel_1_17732WEIGHTED_ROUND_ROBIN_Splitter_17752));
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
	FOR(int, x, 0,  < , 8, x++) {
		float tempsum = 0.0;
		FOR(int, u, 0,  < , 8, u++) {
			tempsum = (tempsum + (iDCT_1D_reference_fine_17754_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
		}
		ENDFOR
		push_float(&(*chanout), tempsum) ; 
	}
	ENDFOR
	FOR(int, u, 0,  < , 8, u++) {
		pop_float(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT_1D_reference_fine_17754() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[0]));
}

void iDCT_1D_reference_fine_17755() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[1]));
}

void iDCT_1D_reference_fine_17756() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[2]));
}

void iDCT_1D_reference_fine_17757() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[3]));
}

void iDCT_1D_reference_fine_17758() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[4]));
}

void iDCT_1D_reference_fine_17759() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[5]));
}

void iDCT_1D_reference_fine_17760() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[6]));
}

void iDCT_1D_reference_fine_17761() {
	iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_17752() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_17732WEIGHTED_ROUND_ROBIN_Splitter_17752));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_17753() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17753Post_CollapsedDataParallel_2_17733, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 8, _i++) {
			push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
			partialSum_i = (partialSum_i + 8) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}
}
	pop_float(&(*chanin)) ; 
}


void Post_CollapsedDataParallel_2_17733() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_17753Post_CollapsedDataParallel_2_17733), &(Post_CollapsedDataParallel_2_17733WEIGHTED_ROUND_ROBIN_Splitter_17762));
}

void iDCT_1D_reference_fine_17764() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[0]));
}

void iDCT_1D_reference_fine_17765() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[1]));
}

void iDCT_1D_reference_fine_17766() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[2]));
}

void iDCT_1D_reference_fine_17767() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[3]));
}

void iDCT_1D_reference_fine_17768() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[4]));
}

void iDCT_1D_reference_fine_17769() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[5]));
}

void iDCT_1D_reference_fine_17770() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[6]));
}

void iDCT_1D_reference_fine_17771() {
	iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_17762() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_17733WEIGHTED_ROUND_ROBIN_Splitter_17762));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_17763() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17763WEIGHTED_ROUND_ROBIN_Splitter_17772, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_17774() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[0]));
	ENDFOR
}

void AnonFilter_a4_17775() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[1]));
	ENDFOR
}

void AnonFilter_a4_17776() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[2]));
	ENDFOR
}

void AnonFilter_a4_17777() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[3]));
	ENDFOR
}

void AnonFilter_a4_17778() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[4]));
	ENDFOR
}

void AnonFilter_a4_17779() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[5]));
	ENDFOR
}

void AnonFilter_a4_17780() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[6]));
	ENDFOR
}

void AnonFilter_a4_17781() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17772() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_17763WEIGHTED_ROUND_ROBIN_Splitter_17772));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17773() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
	int x0 = 0;
	int x1 = 0;
	int x2 = 0;
	int x3 = 0;
	int x4 = 0;
	int x5 = 0;
	int x6 = 0;
	int x7 = 0;
	int x8 = 0;
	x0 = peek_int(&(*chanin), 0) ; 
	x1 = (peek_int(&(*chanin), 4) << 11) ; 
	x2 = peek_int(&(*chanin), 6) ; 
	x3 = peek_int(&(*chanin), 2) ; 
	x4 = peek_int(&(*chanin), 1) ; 
	x5 = peek_int(&(*chanin), 7) ; 
	x6 = peek_int(&(*chanin), 5) ; 
	x7 = peek_int(&(*chanin), 3) ; 
	if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
		x0 = (x0 << 3) ; 
		FOR(int, i, 0,  < , 8, i++) {
			push_int(&(*chanout), x0) ; 
		}
		ENDFOR
	}
	else {
		x0 = ((x0 << 11) + 128) ; 
		x8 = (565 * (x4 + x5)) ; 
		x4 = (x8 + (2276 * x4)) ; 
		x5 = (x8 - (3406 * x5)) ; 
		x8 = (2408 * (x6 + x7)) ; 
		x6 = (x8 - (799 * x6)) ; 
		x7 = (x8 - (4017 * x7)) ; 
		x8 = (x0 + x1) ; 
		x0 = (x0 - x1) ; 
		x1 = (1108 * (x3 + x2)) ; 
		x2 = (x1 - (3784 * x2)) ; 
		x3 = (x1 + (1568 * x3)) ; 
		x1 = (x4 + x6) ; 
		x4 = (x4 - x6) ; 
		x6 = (x5 + x7) ; 
		x5 = (x5 - x7) ; 
		x7 = (x8 + x3) ; 
		x8 = (x8 - x3) ; 
		x3 = (x0 + x2) ; 
		x0 = (x0 - x2) ; 
		x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
		x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
		push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
		push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
		push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
		push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
		push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
		push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
		push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
		push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
	}
	FOR(int, i, 0,  < , 8, i++) {
		pop_int(&(*chanin)) ; 
	}
	ENDFOR
}


void iDCT8x8_1D_row_fast_17784() {
	iDCT8x8_1D_row_fast(&(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[0]), &(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[0]));
}

void iDCT8x8_1D_row_fast_17785() {
	iDCT8x8_1D_row_fast(&(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[1]), &(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[1]));
}

void iDCT8x8_1D_row_fast_17786() {
	iDCT8x8_1D_row_fast(&(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[2]), &(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[2]));
}

void iDCT8x8_1D_row_fast_17787() {
	iDCT8x8_1D_row_fast(&(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[3]), &(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[3]));
}

void iDCT8x8_1D_row_fast_17788() {
	iDCT8x8_1D_row_fast(&(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[4]), &(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[4]));
}

void iDCT8x8_1D_row_fast_17789() {
	iDCT8x8_1D_row_fast(&(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[5]), &(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[5]));
}

void iDCT8x8_1D_row_fast_17790() {
	iDCT8x8_1D_row_fast(&(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[6]), &(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[6]));
}

void iDCT8x8_1D_row_fast_17791() {
	iDCT8x8_1D_row_fast(&(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[7]), &(SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_17782() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_int(&SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_split[2]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_17783() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_17783iDCT8x8_1D_col_fast_17686, pop_int(&SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
	FOR(int, c, 0,  < , 8, c++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), (c + 0)) ; 
		x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
		x2 = peek_int(&(*chanin), (c + 48)) ; 
		x3 = peek_int(&(*chanin), (c + 16)) ; 
		x4 = peek_int(&(*chanin), (c + 8)) ; 
		x5 = peek_int(&(*chanin), (c + 56)) ; 
		x6 = peek_int(&(*chanin), (c + 40)) ; 
		x7 = peek_int(&(*chanin), (c + 24)) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = ((x0 + 32) >> 6) ; 
			FOR(int, i, 0,  < , 8, i++) {
				iDCT8x8_1D_col_fast_17686_s.buffer[(c + (8 * i))] = x0 ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 8) + 8192) ; 
			x8 = ((565 * (x4 + x5)) + 4) ; 
			x4 = ((x8 + (2276 * x4)) >> 3) ; 
			x5 = ((x8 - (3406 * x5)) >> 3) ; 
			x8 = ((2408 * (x6 + x7)) + 4) ; 
			x6 = ((x8 - (799 * x6)) >> 3) ; 
			x7 = ((x8 - (4017 * x7)) >> 3) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = ((1108 * (x3 + x2)) + 4) ; 
			x2 = ((x1 - (3784 * x2)) >> 3) ; 
			x3 = ((x1 + (1568 * x3)) >> 3) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			iDCT8x8_1D_col_fast_17686_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
			iDCT8x8_1D_col_fast_17686_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
			iDCT8x8_1D_col_fast_17686_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
			iDCT8x8_1D_col_fast_17686_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
			iDCT8x8_1D_col_fast_17686_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
			iDCT8x8_1D_col_fast_17686_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
			iDCT8x8_1D_col_fast_17686_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
			iDCT8x8_1D_col_fast_17686_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
		}
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_int(&(*chanin)) ; 
		push_int(&(*chanout), iDCT8x8_1D_col_fast_17686_s.buffer[i]) ; 
	}
	ENDFOR
}


void iDCT8x8_1D_col_fast_17686() {
	iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_17783iDCT8x8_1D_col_fast_17686), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_join[2]));
}

void DUPLICATE_Splitter_17734() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_17659DUPLICATE_Splitter_17734);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17735() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_17735AnonFilter_a2_17687, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_17687_s.count = (AnonFilter_a2_17687_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_17687_s.errors = (AnonFilter_a2_17687_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_17687_s.errors / AnonFilter_a2_17687_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_17687_s.errors = (AnonFilter_a2_17687_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_17687_s.errors / AnonFilter_a2_17687_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_17687() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_17735AnonFilter_a2_17687));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_17796_17803_join[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_17659DUPLICATE_Splitter_17734);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_17783iDCT8x8_1D_col_fast_17686);
	init_buffer_float(&Post_CollapsedDataParallel_2_17733WEIGHTED_ROUND_ROBIN_Splitter_17762);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_17793_17800_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_17732WEIGHTED_ROUND_ROBIN_Splitter_17752);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17763WEIGHTED_ROUND_ROBIN_Splitter_17772);
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17794_17801_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_17793_17800_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_int(&SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17743Pre_CollapsedDataParallel_1_17732);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17795_17802_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_17796_17803_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17660_17736_17792_17799_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin40_iDCT8x8_1D_row_fast_Fiss_17797_17804_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_17735AnonFilter_a2_17687);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17753Post_CollapsedDataParallel_2_17733);
// --- init: iDCT_2D_reference_coarse_17662
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_17662_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17754
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17754_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17755
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17755_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17756
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17756_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17757
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17757_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17758
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17758_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17759
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17759_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17760
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17760_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17761
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17761_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17764
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17764_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17765
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17765_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17766
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17766_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17767
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17767_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17768
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17768_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17769
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17769_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17770
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17770_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17771
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17771_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_17687
	 {
	AnonFilter_a2_17687_s.count = 0.0 ; 
	AnonFilter_a2_17687_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_17659();
		DUPLICATE_Splitter_17734();
			iDCT_2D_reference_coarse_17662();
			WEIGHTED_ROUND_ROBIN_Splitter_17742();
				AnonFilter_a3_17744();
				AnonFilter_a3_17745();
				AnonFilter_a3_17746();
				AnonFilter_a3_17747();
				AnonFilter_a3_17748();
				AnonFilter_a3_17749();
				AnonFilter_a3_17750();
				AnonFilter_a3_17751();
			WEIGHTED_ROUND_ROBIN_Joiner_17743();
			Pre_CollapsedDataParallel_1_17732();
			WEIGHTED_ROUND_ROBIN_Splitter_17752();
				iDCT_1D_reference_fine_17754();
				iDCT_1D_reference_fine_17755();
				iDCT_1D_reference_fine_17756();
				iDCT_1D_reference_fine_17757();
				iDCT_1D_reference_fine_17758();
				iDCT_1D_reference_fine_17759();
				iDCT_1D_reference_fine_17760();
				iDCT_1D_reference_fine_17761();
			WEIGHTED_ROUND_ROBIN_Joiner_17753();
			Post_CollapsedDataParallel_2_17733();
			WEIGHTED_ROUND_ROBIN_Splitter_17762();
				iDCT_1D_reference_fine_17764();
				iDCT_1D_reference_fine_17765();
				iDCT_1D_reference_fine_17766();
				iDCT_1D_reference_fine_17767();
				iDCT_1D_reference_fine_17768();
				iDCT_1D_reference_fine_17769();
				iDCT_1D_reference_fine_17770();
				iDCT_1D_reference_fine_17771();
			WEIGHTED_ROUND_ROBIN_Joiner_17763();
			WEIGHTED_ROUND_ROBIN_Splitter_17772();
				AnonFilter_a4_17774();
				AnonFilter_a4_17775();
				AnonFilter_a4_17776();
				AnonFilter_a4_17777();
				AnonFilter_a4_17778();
				AnonFilter_a4_17779();
				AnonFilter_a4_17780();
				AnonFilter_a4_17781();
			WEIGHTED_ROUND_ROBIN_Joiner_17773();
			WEIGHTED_ROUND_ROBIN_Splitter_17782();
				iDCT8x8_1D_row_fast_17784();
				iDCT8x8_1D_row_fast_17785();
				iDCT8x8_1D_row_fast_17786();
				iDCT8x8_1D_row_fast_17787();
				iDCT8x8_1D_row_fast_17788();
				iDCT8x8_1D_row_fast_17789();
				iDCT8x8_1D_row_fast_17790();
				iDCT8x8_1D_row_fast_17791();
			WEIGHTED_ROUND_ROBIN_Joiner_17783();
			iDCT8x8_1D_col_fast_17686();
		WEIGHTED_ROUND_ROBIN_Joiner_17735();
		AnonFilter_a2_17687();
	ENDFOR
	return EXIT_SUCCESS;
}
