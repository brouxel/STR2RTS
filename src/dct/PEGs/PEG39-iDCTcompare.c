#include "PEG39-iDCTcompare.h"

buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[39];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[39];
buffer_float_t Pre_CollapsedDataParallel_1_9734WEIGHTED_ROUND_ROBIN_Splitter_9785;
buffer_float_t Post_CollapsedDataParallel_2_9735WEIGHTED_ROUND_ROBIN_Splitter_9795;
buffer_int_t AnonFilter_a0_9661DUPLICATE_Splitter_9736;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_9737AnonFilter_a2_9689;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[39];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_9847iDCT8x8_1D_col_fast_9688;
buffer_int_t SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_join[3];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_split[3];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9786Post_CollapsedDataParallel_2_9735;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[39];
buffer_int_t SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9796WEIGHTED_ROUND_ROBIN_Splitter_9805;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9745Pre_CollapsedDataParallel_1_9734;


iDCT_2D_reference_coarse_9664_t iDCT_2D_reference_coarse_9664_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9787_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9788_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9789_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9790_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9791_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9792_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9793_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9794_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9797_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9798_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9799_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9800_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9801_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9802_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9803_s;
iDCT_2D_reference_coarse_9664_t iDCT_1D_reference_fine_9804_s;
iDCT8x8_1D_col_fast_9688_t iDCT8x8_1D_col_fast_9688_s;
AnonFilter_a2_9689_t AnonFilter_a2_9689_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_9661() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_9661DUPLICATE_Splitter_9736));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_9664_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_9664_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_9664() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_9746() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[0]));
	ENDFOR
}

void AnonFilter_a3_9747() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[1]));
	ENDFOR
}

void AnonFilter_a3_9748() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[2]));
	ENDFOR
}

void AnonFilter_a3_9749() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[3]));
	ENDFOR
}

void AnonFilter_a3_9750() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[4]));
	ENDFOR
}

void AnonFilter_a3_9751() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[5]));
	ENDFOR
}

void AnonFilter_a3_9752() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[6]));
	ENDFOR
}

void AnonFilter_a3_9753() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[7]));
	ENDFOR
}

void AnonFilter_a3_9754() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[8]));
	ENDFOR
}

void AnonFilter_a3_9755() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[9]));
	ENDFOR
}

void AnonFilter_a3_9756() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[10]));
	ENDFOR
}

void AnonFilter_a3_9757() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[11]));
	ENDFOR
}

void AnonFilter_a3_9758() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[12]));
	ENDFOR
}

void AnonFilter_a3_9759() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[13]));
	ENDFOR
}

void AnonFilter_a3_9760() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[14]));
	ENDFOR
}

void AnonFilter_a3_9761() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[15]));
	ENDFOR
}

void AnonFilter_a3_9762() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[16]));
	ENDFOR
}

void AnonFilter_a3_9763() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[17]));
	ENDFOR
}

void AnonFilter_a3_9764() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[18]));
	ENDFOR
}

void AnonFilter_a3_9765() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[19]));
	ENDFOR
}

void AnonFilter_a3_9766() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[20]));
	ENDFOR
}

void AnonFilter_a3_9767() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[21]));
	ENDFOR
}

void AnonFilter_a3_9768() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[22]));
	ENDFOR
}

void AnonFilter_a3_9769() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[23]));
	ENDFOR
}

void AnonFilter_a3_9770() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[24]));
	ENDFOR
}

void AnonFilter_a3_9771() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[25]));
	ENDFOR
}

void AnonFilter_a3_9772() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[26]));
	ENDFOR
}

void AnonFilter_a3_9773() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[27]));
	ENDFOR
}

void AnonFilter_a3_9774() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[28]));
	ENDFOR
}

void AnonFilter_a3_9775() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[29]));
	ENDFOR
}

void AnonFilter_a3_9776() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[30]));
	ENDFOR
}

void AnonFilter_a3_9777() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[31]));
	ENDFOR
}

void AnonFilter_a3_9778() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[32]));
	ENDFOR
}

void AnonFilter_a3_9779() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[33]));
	ENDFOR
}

void AnonFilter_a3_9780() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[34]));
	ENDFOR
}

void AnonFilter_a3_9781() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[35]));
	ENDFOR
}

void AnonFilter_a3_9782() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[36]));
	ENDFOR
}

void AnonFilter_a3_9783() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[37]));
	ENDFOR
}

void AnonFilter_a3_9784() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[38]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9744() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 39, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9745() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 39, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9745Pre_CollapsedDataParallel_1_9734, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_9734() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_9745Pre_CollapsedDataParallel_1_9734), &(Pre_CollapsedDataParallel_1_9734WEIGHTED_ROUND_ROBIN_Splitter_9785));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_9787_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_9787() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_9788() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_9789() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_9790() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_9791() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_9792() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_9793() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_9794() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9785() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9734WEIGHTED_ROUND_ROBIN_Splitter_9785));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9786() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9786Post_CollapsedDataParallel_2_9735, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_9735() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9786Post_CollapsedDataParallel_2_9735), &(Post_CollapsedDataParallel_2_9735WEIGHTED_ROUND_ROBIN_Splitter_9795));
	ENDFOR
}

void iDCT_1D_reference_fine_9797() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_9798() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_9799() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_9800() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_9801() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_9802() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_9803() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_9804() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9795() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_9735WEIGHTED_ROUND_ROBIN_Splitter_9795));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9796() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9796WEIGHTED_ROUND_ROBIN_Splitter_9805, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_9807() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[0]));
	ENDFOR
}

void AnonFilter_a4_9808() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[1]));
	ENDFOR
}

void AnonFilter_a4_9809() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[2]));
	ENDFOR
}

void AnonFilter_a4_9810() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[3]));
	ENDFOR
}

void AnonFilter_a4_9811() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[4]));
	ENDFOR
}

void AnonFilter_a4_9812() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[5]));
	ENDFOR
}

void AnonFilter_a4_9813() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[6]));
	ENDFOR
}

void AnonFilter_a4_9814() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[7]));
	ENDFOR
}

void AnonFilter_a4_9815() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[8]));
	ENDFOR
}

void AnonFilter_a4_9816() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[9]));
	ENDFOR
}

void AnonFilter_a4_9817() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[10]));
	ENDFOR
}

void AnonFilter_a4_9818() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[11]));
	ENDFOR
}

void AnonFilter_a4_9819() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[12]));
	ENDFOR
}

void AnonFilter_a4_9820() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[13]));
	ENDFOR
}

void AnonFilter_a4_9821() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[14]));
	ENDFOR
}

void AnonFilter_a4_9822() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[15]));
	ENDFOR
}

void AnonFilter_a4_9823() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[16]));
	ENDFOR
}

void AnonFilter_a4_9824() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[17]));
	ENDFOR
}

void AnonFilter_a4_9825() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[18]));
	ENDFOR
}

void AnonFilter_a4_9826() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[19]));
	ENDFOR
}

void AnonFilter_a4_9827() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[20]));
	ENDFOR
}

void AnonFilter_a4_9828() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[21]));
	ENDFOR
}

void AnonFilter_a4_9829() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[22]));
	ENDFOR
}

void AnonFilter_a4_9830() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[23]));
	ENDFOR
}

void AnonFilter_a4_9831() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[24]));
	ENDFOR
}

void AnonFilter_a4_9832() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[25]));
	ENDFOR
}

void AnonFilter_a4_9833() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[26]));
	ENDFOR
}

void AnonFilter_a4_9834() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[27]));
	ENDFOR
}

void AnonFilter_a4_9835() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[28]));
	ENDFOR
}

void AnonFilter_a4_9836() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[29]));
	ENDFOR
}

void AnonFilter_a4_9837() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[30]));
	ENDFOR
}

void AnonFilter_a4_9838() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[31]));
	ENDFOR
}

void AnonFilter_a4_9839() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[32]));
	ENDFOR
}

void AnonFilter_a4_9840() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[33]));
	ENDFOR
}

void AnonFilter_a4_9841() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[34]));
	ENDFOR
}

void AnonFilter_a4_9842() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[35]));
	ENDFOR
}

void AnonFilter_a4_9843() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[36]));
	ENDFOR
}

void AnonFilter_a4_9844() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[37]));
	ENDFOR
}

void AnonFilter_a4_9845() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[38]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9805() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 39, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9796WEIGHTED_ROUND_ROBIN_Splitter_9805));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9806() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 39, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_9848() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[0]), &(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9849() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[1]), &(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9850() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[2]), &(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9851() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[3]), &(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9852() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[4]), &(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9853() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[5]), &(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9854() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[6]), &(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_9855() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[7]), &(SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9846() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9847() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_9847iDCT8x8_1D_col_fast_9688, pop_int(&SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_9688_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_9688_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_9688_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_9688_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_9688_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_9688_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_9688_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_9688_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_9688_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_9688_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_9688() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_9847iDCT8x8_1D_col_fast_9688), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_9736() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2496, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_9661DUPLICATE_Splitter_9736);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9737() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2496, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_9737AnonFilter_a2_9689, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_9689_s.count = (AnonFilter_a2_9689_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_9689_s.errors = (AnonFilter_a2_9689_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_9689_s.errors / AnonFilter_a2_9689_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_9689_s.errors = (AnonFilter_a2_9689_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_9689_s.errors / AnonFilter_a2_9689_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_9689() {
	FOR(uint32_t, __iter_steady_, 0, <, 2496, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_9737AnonFilter_a2_9689));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 39, __iter_init_0_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_9857_9864_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 39, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_9860_9867_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_9734WEIGHTED_ROUND_ROBIN_Splitter_9785);
	init_buffer_float(&Post_CollapsedDataParallel_2_9735WEIGHTED_ROUND_ROBIN_Splitter_9795);
	init_buffer_int(&AnonFilter_a0_9661DUPLICATE_Splitter_9736);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_9737AnonFilter_a2_9689);
	FOR(int, __iter_init_2_, 0, <, 39, __iter_init_2_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_9857_9864_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_split[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_9847iDCT8x8_1D_col_fast_9688);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_int(&SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_9662_9738_9856_9863_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9786Post_CollapsedDataParallel_2_9735);
	FOR(int, __iter_init_8_, 0, <, 39, __iter_init_8_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_9860_9867_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin102_iDCT8x8_1D_row_fast_Fiss_9861_9868_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_9858_9865_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_9859_9866_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9796WEIGHTED_ROUND_ROBIN_Splitter_9805);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9745Pre_CollapsedDataParallel_1_9734);
// --- init: iDCT_2D_reference_coarse_9664
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_9664_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9787
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9787_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9788
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9788_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9789
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9789_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9790
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9790_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9791
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9791_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9792
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9792_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9793
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9793_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9794
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9794_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9797
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9797_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9798
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9798_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9799
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9799_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9800
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9800_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9801
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9801_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9802
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9802_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9803
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9803_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_9804
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_9804_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_9689
	 {
	AnonFilter_a2_9689_s.count = 0.0 ; 
	AnonFilter_a2_9689_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_9661();
		DUPLICATE_Splitter_9736();
			iDCT_2D_reference_coarse_9664();
			WEIGHTED_ROUND_ROBIN_Splitter_9744();
				AnonFilter_a3_9746();
				AnonFilter_a3_9747();
				AnonFilter_a3_9748();
				AnonFilter_a3_9749();
				AnonFilter_a3_9750();
				AnonFilter_a3_9751();
				AnonFilter_a3_9752();
				AnonFilter_a3_9753();
				AnonFilter_a3_9754();
				AnonFilter_a3_9755();
				AnonFilter_a3_9756();
				AnonFilter_a3_9757();
				AnonFilter_a3_9758();
				AnonFilter_a3_9759();
				AnonFilter_a3_9760();
				AnonFilter_a3_9761();
				AnonFilter_a3_9762();
				AnonFilter_a3_9763();
				AnonFilter_a3_9764();
				AnonFilter_a3_9765();
				AnonFilter_a3_9766();
				AnonFilter_a3_9767();
				AnonFilter_a3_9768();
				AnonFilter_a3_9769();
				AnonFilter_a3_9770();
				AnonFilter_a3_9771();
				AnonFilter_a3_9772();
				AnonFilter_a3_9773();
				AnonFilter_a3_9774();
				AnonFilter_a3_9775();
				AnonFilter_a3_9776();
				AnonFilter_a3_9777();
				AnonFilter_a3_9778();
				AnonFilter_a3_9779();
				AnonFilter_a3_9780();
				AnonFilter_a3_9781();
				AnonFilter_a3_9782();
				AnonFilter_a3_9783();
				AnonFilter_a3_9784();
			WEIGHTED_ROUND_ROBIN_Joiner_9745();
			Pre_CollapsedDataParallel_1_9734();
			WEIGHTED_ROUND_ROBIN_Splitter_9785();
				iDCT_1D_reference_fine_9787();
				iDCT_1D_reference_fine_9788();
				iDCT_1D_reference_fine_9789();
				iDCT_1D_reference_fine_9790();
				iDCT_1D_reference_fine_9791();
				iDCT_1D_reference_fine_9792();
				iDCT_1D_reference_fine_9793();
				iDCT_1D_reference_fine_9794();
			WEIGHTED_ROUND_ROBIN_Joiner_9786();
			Post_CollapsedDataParallel_2_9735();
			WEIGHTED_ROUND_ROBIN_Splitter_9795();
				iDCT_1D_reference_fine_9797();
				iDCT_1D_reference_fine_9798();
				iDCT_1D_reference_fine_9799();
				iDCT_1D_reference_fine_9800();
				iDCT_1D_reference_fine_9801();
				iDCT_1D_reference_fine_9802();
				iDCT_1D_reference_fine_9803();
				iDCT_1D_reference_fine_9804();
			WEIGHTED_ROUND_ROBIN_Joiner_9796();
			WEIGHTED_ROUND_ROBIN_Splitter_9805();
				AnonFilter_a4_9807();
				AnonFilter_a4_9808();
				AnonFilter_a4_9809();
				AnonFilter_a4_9810();
				AnonFilter_a4_9811();
				AnonFilter_a4_9812();
				AnonFilter_a4_9813();
				AnonFilter_a4_9814();
				AnonFilter_a4_9815();
				AnonFilter_a4_9816();
				AnonFilter_a4_9817();
				AnonFilter_a4_9818();
				AnonFilter_a4_9819();
				AnonFilter_a4_9820();
				AnonFilter_a4_9821();
				AnonFilter_a4_9822();
				AnonFilter_a4_9823();
				AnonFilter_a4_9824();
				AnonFilter_a4_9825();
				AnonFilter_a4_9826();
				AnonFilter_a4_9827();
				AnonFilter_a4_9828();
				AnonFilter_a4_9829();
				AnonFilter_a4_9830();
				AnonFilter_a4_9831();
				AnonFilter_a4_9832();
				AnonFilter_a4_9833();
				AnonFilter_a4_9834();
				AnonFilter_a4_9835();
				AnonFilter_a4_9836();
				AnonFilter_a4_9837();
				AnonFilter_a4_9838();
				AnonFilter_a4_9839();
				AnonFilter_a4_9840();
				AnonFilter_a4_9841();
				AnonFilter_a4_9842();
				AnonFilter_a4_9843();
				AnonFilter_a4_9844();
				AnonFilter_a4_9845();
			WEIGHTED_ROUND_ROBIN_Joiner_9806();
			WEIGHTED_ROUND_ROBIN_Splitter_9846();
				iDCT8x8_1D_row_fast_9848();
				iDCT8x8_1D_row_fast_9849();
				iDCT8x8_1D_row_fast_9850();
				iDCT8x8_1D_row_fast_9851();
				iDCT8x8_1D_row_fast_9852();
				iDCT8x8_1D_row_fast_9853();
				iDCT8x8_1D_row_fast_9854();
				iDCT8x8_1D_row_fast_9855();
			WEIGHTED_ROUND_ROBIN_Joiner_9847();
			iDCT8x8_1D_col_fast_9688();
		WEIGHTED_ROUND_ROBIN_Joiner_9737();
		AnonFilter_a2_9689();
	ENDFOR
	return EXIT_SUCCESS;
}
