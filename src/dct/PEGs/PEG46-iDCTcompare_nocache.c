#include "PEG46-iDCTcompare_nocache.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7455Post_CollapsedDataParallel_2_7397;
buffer_int_t AnonFilter_a0_7323DUPLICATE_Splitter_7398;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[8];
buffer_int_t SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[46];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_7396WEIGHTED_ROUND_ROBIN_Splitter_7454;
buffer_float_t Post_CollapsedDataParallel_2_7397WEIGHTED_ROUND_ROBIN_Splitter_7464;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7465WEIGHTED_ROUND_ROBIN_Splitter_7474;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[46];
buffer_int_t SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7407Pre_CollapsedDataParallel_1_7396;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_7399AnonFilter_a2_7351;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_split[3];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[46];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[46];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_join[3];


iDCT_2D_reference_coarse_7326_t iDCT_2D_reference_coarse_7326_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7456_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7457_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7458_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7459_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7460_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7461_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7462_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7463_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7466_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7467_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7468_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7469_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7470_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7471_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7472_s;
iDCT_2D_reference_coarse_7326_t iDCT_1D_reference_fine_7473_s;
iDCT8x8_1D_col_fast_7350_t iDCT8x8_1D_col_fast_7350_s;
AnonFilter_a2_7351_t AnonFilter_a2_7351_s;

void AnonFilter_a0_7323(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_7323DUPLICATE_Splitter_7398, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_7326(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_7326_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_7326_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_7408(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7409(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7410(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7411(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7412(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7413(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7414(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7415(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7416(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7417(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7418(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7419(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7420(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7421(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7422(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7423(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7424(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7425(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7426(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7427(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7428(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7429(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7430(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7431(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7432(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7433(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7434(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7435(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7436(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7437(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7438(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7439(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7440(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7441(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7442(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7443(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7444(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7445(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7446(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7447(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7448(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7449(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7450(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7451(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7452(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_7453(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[45])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7406() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 46, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7407() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 46, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7407Pre_CollapsedDataParallel_1_7396, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_7396(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_7396WEIGHTED_ROUND_ROBIN_Splitter_7454, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7407Pre_CollapsedDataParallel_1_7396, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7407Pre_CollapsedDataParallel_1_7396) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7456(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7456_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7457(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7457_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7458(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7458_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7459(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7459_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7460(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7460_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7461(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7461_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7462(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7462_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7463(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7463_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7454() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7396WEIGHTED_ROUND_ROBIN_Splitter_7454));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7455() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7455Post_CollapsedDataParallel_2_7397, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_7397(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_7397WEIGHTED_ROUND_ROBIN_Splitter_7464, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7455Post_CollapsedDataParallel_2_7397, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7455Post_CollapsedDataParallel_2_7397) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7466(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7466_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7467(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7467_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7468(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7468_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7469(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7469_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7470(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7470_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7471(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7471_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7472(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7472_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_7473(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_7473_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_7397WEIGHTED_ROUND_ROBIN_Splitter_7464));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7465() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7465WEIGHTED_ROUND_ROBIN_Splitter_7474, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_7476(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7477(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7478(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7479(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7480(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7481(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7482(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7483(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7484(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7485(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7486(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7487(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7488(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7489(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7490(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7491(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7492(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7493(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7494(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7495(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7496(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7497(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7498(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7499(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7500(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7501(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7502(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7503(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7504(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7505(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7506(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7507(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7508(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7509(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7510(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7511(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7512(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7513(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7514(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7515(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7516(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7517(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7518(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7519(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7520(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_7521(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7474() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 46, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7465WEIGHTED_ROUND_ROBIN_Splitter_7474));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7475() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 46, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_7524(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0], 6) ; 
		x3 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0], 2) ; 
		x4 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0], 1) ; 
		x5 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0], 7) ; 
		x6 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0], 5) ; 
		x7 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7525(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1], 6) ; 
		x3 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1], 2) ; 
		x4 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1], 1) ; 
		x5 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1], 7) ; 
		x6 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1], 5) ; 
		x7 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7526(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2], 6) ; 
		x3 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2], 2) ; 
		x4 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2], 1) ; 
		x5 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2], 7) ; 
		x6 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2], 5) ; 
		x7 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7527(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3], 6) ; 
		x3 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3], 2) ; 
		x4 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3], 1) ; 
		x5 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3], 7) ; 
		x6 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3], 5) ; 
		x7 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7528(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4], 6) ; 
		x3 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4], 2) ; 
		x4 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4], 1) ; 
		x5 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4], 7) ; 
		x6 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4], 5) ; 
		x7 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7529(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5], 6) ; 
		x3 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5], 2) ; 
		x4 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5], 1) ; 
		x5 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5], 7) ; 
		x6 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5], 5) ; 
		x7 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7530(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6], 6) ; 
		x3 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6], 2) ; 
		x4 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6], 1) ; 
		x5 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6], 7) ; 
		x6 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6], 5) ; 
		x7 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_7531(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7], 6) ; 
		x3 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7], 2) ; 
		x4 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7], 1) ; 
		x5 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7], 7) ; 
		x6 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7], 5) ; 
		x7 = peek_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7522() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7523() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, pop_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_7350(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_7350_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_7350_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_7350_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_7350_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_7350_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_7350_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_7350_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_7350_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_7350_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_join[2], iDCT8x8_1D_col_fast_7350_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_7398() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_7323DUPLICATE_Splitter_7398);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7399() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_7399AnonFilter_a2_7351, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_7351(){
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7399AnonFilter_a2_7351) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7399AnonFilter_a2_7351) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_7399AnonFilter_a2_7351) ; 
		AnonFilter_a2_7351_s.count = (AnonFilter_a2_7351_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_7351_s.errors = (AnonFilter_a2_7351_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_7351_s.errors / AnonFilter_a2_7351_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_7351_s.errors = (AnonFilter_a2_7351_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_7351_s.errors / AnonFilter_a2_7351_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7455Post_CollapsedDataParallel_2_7397);
	init_buffer_int(&AnonFilter_a0_7323DUPLICATE_Splitter_7398);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 46, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7396WEIGHTED_ROUND_ROBIN_Splitter_7454);
	init_buffer_float(&Post_CollapsedDataParallel_2_7397WEIGHTED_ROUND_ROBIN_Splitter_7464);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7465WEIGHTED_ROUND_ROBIN_Splitter_7474);
	FOR(int, __iter_init_4_, 0, <, 46, __iter_init_4_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_int(&SplitJoin116_iDCT8x8_1D_row_fast_Fiss_7537_7544_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_7534_7541_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7407Pre_CollapsedDataParallel_1_7396);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_7535_7542_join[__iter_init_7_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_7523iDCT8x8_1D_col_fast_7350);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_7399AnonFilter_a2_7351);
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 46, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_7533_7540_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 46, __iter_init_10_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_7536_7543_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_7324_7400_7532_7539_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_7326
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_7326_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7456
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7456_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7457
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7457_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7458
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7458_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7459
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7459_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7460
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7460_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7461
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7461_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7462
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7462_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7463
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7463_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7466
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7466_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7467
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7467_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7468
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7468_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7469
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7469_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7470
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7470_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7471
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7471_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7472
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7472_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_7473
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_7473_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_7351
	 {
	AnonFilter_a2_7351_s.count = 0.0 ; 
	AnonFilter_a2_7351_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_7323();
		DUPLICATE_Splitter_7398();
			iDCT_2D_reference_coarse_7326();
			WEIGHTED_ROUND_ROBIN_Splitter_7406();
				AnonFilter_a3_7408();
				AnonFilter_a3_7409();
				AnonFilter_a3_7410();
				AnonFilter_a3_7411();
				AnonFilter_a3_7412();
				AnonFilter_a3_7413();
				AnonFilter_a3_7414();
				AnonFilter_a3_7415();
				AnonFilter_a3_7416();
				AnonFilter_a3_7417();
				AnonFilter_a3_7418();
				AnonFilter_a3_7419();
				AnonFilter_a3_7420();
				AnonFilter_a3_7421();
				AnonFilter_a3_7422();
				AnonFilter_a3_7423();
				AnonFilter_a3_7424();
				AnonFilter_a3_7425();
				AnonFilter_a3_7426();
				AnonFilter_a3_7427();
				AnonFilter_a3_7428();
				AnonFilter_a3_7429();
				AnonFilter_a3_7430();
				AnonFilter_a3_7431();
				AnonFilter_a3_7432();
				AnonFilter_a3_7433();
				AnonFilter_a3_7434();
				AnonFilter_a3_7435();
				AnonFilter_a3_7436();
				AnonFilter_a3_7437();
				AnonFilter_a3_7438();
				AnonFilter_a3_7439();
				AnonFilter_a3_7440();
				AnonFilter_a3_7441();
				AnonFilter_a3_7442();
				AnonFilter_a3_7443();
				AnonFilter_a3_7444();
				AnonFilter_a3_7445();
				AnonFilter_a3_7446();
				AnonFilter_a3_7447();
				AnonFilter_a3_7448();
				AnonFilter_a3_7449();
				AnonFilter_a3_7450();
				AnonFilter_a3_7451();
				AnonFilter_a3_7452();
				AnonFilter_a3_7453();
			WEIGHTED_ROUND_ROBIN_Joiner_7407();
			Pre_CollapsedDataParallel_1_7396();
			WEIGHTED_ROUND_ROBIN_Splitter_7454();
				iDCT_1D_reference_fine_7456();
				iDCT_1D_reference_fine_7457();
				iDCT_1D_reference_fine_7458();
				iDCT_1D_reference_fine_7459();
				iDCT_1D_reference_fine_7460();
				iDCT_1D_reference_fine_7461();
				iDCT_1D_reference_fine_7462();
				iDCT_1D_reference_fine_7463();
			WEIGHTED_ROUND_ROBIN_Joiner_7455();
			Post_CollapsedDataParallel_2_7397();
			WEIGHTED_ROUND_ROBIN_Splitter_7464();
				iDCT_1D_reference_fine_7466();
				iDCT_1D_reference_fine_7467();
				iDCT_1D_reference_fine_7468();
				iDCT_1D_reference_fine_7469();
				iDCT_1D_reference_fine_7470();
				iDCT_1D_reference_fine_7471();
				iDCT_1D_reference_fine_7472();
				iDCT_1D_reference_fine_7473();
			WEIGHTED_ROUND_ROBIN_Joiner_7465();
			WEIGHTED_ROUND_ROBIN_Splitter_7474();
				AnonFilter_a4_7476();
				AnonFilter_a4_7477();
				AnonFilter_a4_7478();
				AnonFilter_a4_7479();
				AnonFilter_a4_7480();
				AnonFilter_a4_7481();
				AnonFilter_a4_7482();
				AnonFilter_a4_7483();
				AnonFilter_a4_7484();
				AnonFilter_a4_7485();
				AnonFilter_a4_7486();
				AnonFilter_a4_7487();
				AnonFilter_a4_7488();
				AnonFilter_a4_7489();
				AnonFilter_a4_7490();
				AnonFilter_a4_7491();
				AnonFilter_a4_7492();
				AnonFilter_a4_7493();
				AnonFilter_a4_7494();
				AnonFilter_a4_7495();
				AnonFilter_a4_7496();
				AnonFilter_a4_7497();
				AnonFilter_a4_7498();
				AnonFilter_a4_7499();
				AnonFilter_a4_7500();
				AnonFilter_a4_7501();
				AnonFilter_a4_7502();
				AnonFilter_a4_7503();
				AnonFilter_a4_7504();
				AnonFilter_a4_7505();
				AnonFilter_a4_7506();
				AnonFilter_a4_7507();
				AnonFilter_a4_7508();
				AnonFilter_a4_7509();
				AnonFilter_a4_7510();
				AnonFilter_a4_7511();
				AnonFilter_a4_7512();
				AnonFilter_a4_7513();
				AnonFilter_a4_7514();
				AnonFilter_a4_7515();
				AnonFilter_a4_7516();
				AnonFilter_a4_7517();
				AnonFilter_a4_7518();
				AnonFilter_a4_7519();
				AnonFilter_a4_7520();
				AnonFilter_a4_7521();
			WEIGHTED_ROUND_ROBIN_Joiner_7475();
			WEIGHTED_ROUND_ROBIN_Splitter_7522();
				iDCT8x8_1D_row_fast_7524();
				iDCT8x8_1D_row_fast_7525();
				iDCT8x8_1D_row_fast_7526();
				iDCT8x8_1D_row_fast_7527();
				iDCT8x8_1D_row_fast_7528();
				iDCT8x8_1D_row_fast_7529();
				iDCT8x8_1D_row_fast_7530();
				iDCT8x8_1D_row_fast_7531();
			WEIGHTED_ROUND_ROBIN_Joiner_7523();
			iDCT8x8_1D_col_fast_7350();
		WEIGHTED_ROUND_ROBIN_Joiner_7399();
		AnonFilter_a2_7351();
	ENDFOR
	return EXIT_SUCCESS;
}
