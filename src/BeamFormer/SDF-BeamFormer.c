#include "SDF-BeamFormer.h"

buffer_float_t Magnitude_109FloatPrinter_110;
buffer_float_t BeamFirFilter_60BeamFirFilter_61;
buffer_float_t BeamFirFilter_100BeamFirFilter_101;
buffer_float_t BeamFirFilter_68BeamFirFilter_69;
buffer_float_t InputGenerate_67BeamFirFilter_68;
buffer_float_t BeamFirFilter_104BeamFirFilter_105;
buffer_float_t InputGenerate_75BeamFirFilter_76;
buffer_float_t BeamForm_107BeamFirFilter_108;
buffer_float_t BeamFirFilter_92BeamFirFilter_93;
buffer_float_t SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_join[4];
buffer_float_t InputGenerate_99BeamFirFilter_100;
buffer_float_t InputGenerate_79BeamFirFilter_80;
buffer_float_t BeamFirFilter_80BeamFirFilter_81;
buffer_float_t BeamFirFilter_84BeamFirFilter_85;
buffer_float_t BeamFirFilter_123Magnitude_124;
buffer_float_t BeamFirFilter_76BeamFirFilter_77;
buffer_float_t BeamFirFilter_113Magnitude_114;
buffer_float_t Magnitude_124FloatPrinter_125;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_127DUPLICATE_Splitter_128;
buffer_float_t SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_split[4];
buffer_float_t InputGenerate_71BeamFirFilter_72;
buffer_float_t BeamForm_112BeamFirFilter_113;
buffer_float_t Magnitude_114FloatPrinter_115;
buffer_float_t BeamFirFilter_118Magnitude_119;
buffer_float_t BeamFirFilter_64BeamFirFilter_65;
buffer_float_t BeamFirFilter_108Magnitude_109;
buffer_float_t BeamForm_117BeamFirFilter_118;
buffer_float_t InputGenerate_63BeamFirFilter_64;
buffer_float_t InputGenerate_103BeamFirFilter_104;
buffer_float_t BeamFirFilter_72BeamFirFilter_73;
buffer_float_t InputGenerate_59BeamFirFilter_60;
buffer_float_t InputGenerate_95BeamFirFilter_96;
buffer_float_t Magnitude_119FloatPrinter_120;
buffer_float_t InputGenerate_87BeamFirFilter_88;
buffer_float_t BeamFirFilter_88BeamFirFilter_89;
buffer_float_t InputGenerate_83BeamFirFilter_84;
buffer_float_t BeamForm_122BeamFirFilter_123;
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[12];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[12];
buffer_float_t BeamFirFilter_96BeamFirFilter_97;
buffer_float_t InputGenerate_91BeamFirFilter_92;


InputGenerate_59_t InputGenerate_59_s;
BeamFirFilter_60_t BeamFirFilter_60_s;
BeamFirFilter_60_t BeamFirFilter_61_s;
InputGenerate_59_t InputGenerate_63_s;
BeamFirFilter_60_t BeamFirFilter_64_s;
BeamFirFilter_60_t BeamFirFilter_65_s;
InputGenerate_59_t InputGenerate_67_s;
BeamFirFilter_60_t BeamFirFilter_68_s;
BeamFirFilter_60_t BeamFirFilter_69_s;
InputGenerate_59_t InputGenerate_71_s;
BeamFirFilter_60_t BeamFirFilter_72_s;
BeamFirFilter_60_t BeamFirFilter_73_s;
InputGenerate_59_t InputGenerate_75_s;
BeamFirFilter_60_t BeamFirFilter_76_s;
BeamFirFilter_60_t BeamFirFilter_77_s;
InputGenerate_59_t InputGenerate_79_s;
BeamFirFilter_60_t BeamFirFilter_80_s;
BeamFirFilter_60_t BeamFirFilter_81_s;
InputGenerate_59_t InputGenerate_83_s;
BeamFirFilter_60_t BeamFirFilter_84_s;
BeamFirFilter_60_t BeamFirFilter_85_s;
InputGenerate_59_t InputGenerate_87_s;
BeamFirFilter_60_t BeamFirFilter_88_s;
BeamFirFilter_60_t BeamFirFilter_89_s;
InputGenerate_59_t InputGenerate_91_s;
BeamFirFilter_60_t BeamFirFilter_92_s;
BeamFirFilter_60_t BeamFirFilter_93_s;
InputGenerate_59_t InputGenerate_95_s;
BeamFirFilter_60_t BeamFirFilter_96_s;
BeamFirFilter_60_t BeamFirFilter_97_s;
InputGenerate_59_t InputGenerate_99_s;
BeamFirFilter_60_t BeamFirFilter_100_s;
BeamFirFilter_60_t BeamFirFilter_101_s;
InputGenerate_59_t InputGenerate_103_s;
BeamFirFilter_60_t BeamFirFilter_104_s;
BeamFirFilter_60_t BeamFirFilter_105_s;
BeamForm_107_t BeamForm_107_s;
BeamFirFilter_108_t BeamFirFilter_108_s;
BeamForm_107_t BeamForm_112_s;
BeamFirFilter_108_t BeamFirFilter_113_s;
BeamForm_107_t BeamForm_117_s;
BeamFirFilter_108_t BeamFirFilter_118_s;
BeamForm_107_t BeamForm_122_s;
BeamFirFilter_108_t BeamFirFilter_123_s;

void InputGenerate(buffer_float_t *chanin, buffer_float_t *chanout) {
		if((FALSE && InputGenerate_59_s.curSample == 256)) {
			push_float(&(*chanout), ((float) sqrt((InputGenerate_59_s.curSample * 0)))) ; 
			push_float(&(*chanout), (((float) sqrt((InputGenerate_59_s.curSample * 0))) + 1.0)) ; 
		}
		else {
			push_float(&(*chanout), -((float) sqrt((InputGenerate_59_s.curSample * 0)))) ; 
			push_float(&(*chanout), -(((float) sqrt((InputGenerate_59_s.curSample * 0))) + 1.0)) ; 
		}
		InputGenerate_59_s.curSample++ ; 
		if((InputGenerate_59_s.curSample >= 1024)) {
			InputGenerate_59_s.curSample = 0 ; 
		}
	}


void InputGenerate_59() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[0]), &(InputGenerate_59BeamFirFilter_60));
	ENDFOR
}

void BeamFirFilter(buffer_float_t *chanin, buffer_float_t *chanout) {
		float real_curr = 0.0;
		float imag_curr = 0.0;
		int modPos = 0;
		BeamFirFilter_60_s.realBuffer[(63 - BeamFirFilter_60_s.pos)] = pop_float(&(*chanin)) ; 
		BeamFirFilter_60_s.imagBuffer[(63 - BeamFirFilter_60_s.pos)] = pop_float(&(*chanin)) ; 
		modPos = (63 - BeamFirFilter_60_s.pos) ; 
		FOR(int, i, 0,  < , 64, i++) {
			real_curr = (real_curr + ((BeamFirFilter_60_s.realBuffer[modPos] * BeamFirFilter_60_s.real_weight[i]) + (BeamFirFilter_60_s.imagBuffer[modPos] * BeamFirFilter_60_s.imag_weight[i]))) ; 
			imag_curr = (imag_curr + ((BeamFirFilter_60_s.imagBuffer[modPos] * BeamFirFilter_60_s.real_weight[i]) + (BeamFirFilter_60_s.realBuffer[modPos] * BeamFirFilter_60_s.imag_weight[i]))) ; 
			modPos = ((modPos + 1) & 63) ; 
		}
		ENDFOR
		BeamFirFilter_60_s.pos = ((BeamFirFilter_60_s.pos + 1) & 63) ; 
		push_float(&(*chanout), real_curr) ; 
		push_float(&(*chanout), imag_curr) ; 
		FOR(int, i, 2,  < , 2, i++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
		BeamFirFilter_60_s.count = (BeamFirFilter_60_s.count + 1) ; 
		if(BeamFirFilter_60_s.count == 1024) {
			BeamFirFilter_60_s.count = 0 ; 
			BeamFirFilter_60_s.pos = 0 ; 
			FOR(int, i, 0,  < , 64, i++) {
				BeamFirFilter_60_s.realBuffer[i] = 0.0 ; 
				BeamFirFilter_60_s.imagBuffer[i] = 0.0 ; 
			}
			ENDFOR
		}
	}


void BeamFirFilter_60() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_59BeamFirFilter_60), &(BeamFirFilter_60BeamFirFilter_61));
	ENDFOR
}

void BeamFirFilter_61() {
	BeamFirFilter(&(BeamFirFilter_60BeamFirFilter_61), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[0]));
}

void InputGenerate_63() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[1]), &(InputGenerate_63BeamFirFilter_64));
	ENDFOR
}

void BeamFirFilter_64() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_63BeamFirFilter_64), &(BeamFirFilter_64BeamFirFilter_65));
	ENDFOR
}

void BeamFirFilter_65() {
	BeamFirFilter(&(BeamFirFilter_64BeamFirFilter_65), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[1]));
}

void InputGenerate_67() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[2]), &(InputGenerate_67BeamFirFilter_68));
	ENDFOR
}

void BeamFirFilter_68() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_67BeamFirFilter_68), &(BeamFirFilter_68BeamFirFilter_69));
	ENDFOR
}

void BeamFirFilter_69() {
	BeamFirFilter(&(BeamFirFilter_68BeamFirFilter_69), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[2]));
}

void InputGenerate_71() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[3]), &(InputGenerate_71BeamFirFilter_72));
	ENDFOR
}

void BeamFirFilter_72() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_71BeamFirFilter_72), &(BeamFirFilter_72BeamFirFilter_73));
	ENDFOR
}

void BeamFirFilter_73() {
	BeamFirFilter(&(BeamFirFilter_72BeamFirFilter_73), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[3]));
}

void InputGenerate_75() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[4]), &(InputGenerate_75BeamFirFilter_76));
	ENDFOR
}

void BeamFirFilter_76() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_75BeamFirFilter_76), &(BeamFirFilter_76BeamFirFilter_77));
	ENDFOR
}

void BeamFirFilter_77() {
	BeamFirFilter(&(BeamFirFilter_76BeamFirFilter_77), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[4]));
}

void InputGenerate_79() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[5]), &(InputGenerate_79BeamFirFilter_80));
	ENDFOR
}

void BeamFirFilter_80() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_79BeamFirFilter_80), &(BeamFirFilter_80BeamFirFilter_81));
	ENDFOR
}

void BeamFirFilter_81() {
	BeamFirFilter(&(BeamFirFilter_80BeamFirFilter_81), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[5]));
}

void InputGenerate_83() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[6]), &(InputGenerate_83BeamFirFilter_84));
	ENDFOR
}

void BeamFirFilter_84() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_83BeamFirFilter_84), &(BeamFirFilter_84BeamFirFilter_85));
	ENDFOR
}

void BeamFirFilter_85() {
	BeamFirFilter(&(BeamFirFilter_84BeamFirFilter_85), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[6]));
}

void InputGenerate_87() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[7]), &(InputGenerate_87BeamFirFilter_88));
	ENDFOR
}

void BeamFirFilter_88() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_87BeamFirFilter_88), &(BeamFirFilter_88BeamFirFilter_89));
	ENDFOR
}

void BeamFirFilter_89() {
	BeamFirFilter(&(BeamFirFilter_88BeamFirFilter_89), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[7]));
}

void InputGenerate_91() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[8]), &(InputGenerate_91BeamFirFilter_92));
	ENDFOR
}

void BeamFirFilter_92() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_91BeamFirFilter_92), &(BeamFirFilter_92BeamFirFilter_93));
	ENDFOR
}

void BeamFirFilter_93() {
	BeamFirFilter(&(BeamFirFilter_92BeamFirFilter_93), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[8]));
}

void InputGenerate_95() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[9]), &(InputGenerate_95BeamFirFilter_96));
	ENDFOR
}

void BeamFirFilter_96() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_95BeamFirFilter_96), &(BeamFirFilter_96BeamFirFilter_97));
	ENDFOR
}

void BeamFirFilter_97() {
	BeamFirFilter(&(BeamFirFilter_96BeamFirFilter_97), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[9]));
}

void InputGenerate_99() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[10]), &(InputGenerate_99BeamFirFilter_100));
	ENDFOR
}

void BeamFirFilter_100() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_99BeamFirFilter_100), &(BeamFirFilter_100BeamFirFilter_101));
	ENDFOR
}

void BeamFirFilter_101() {
	BeamFirFilter(&(BeamFirFilter_100BeamFirFilter_101), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[10]));
}

void InputGenerate_103() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		InputGenerate(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[11]), &(InputGenerate_103BeamFirFilter_104));
	ENDFOR
}

void BeamFirFilter_104() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		BeamFirFilter(&(InputGenerate_103BeamFirFilter_104), &(BeamFirFilter_104BeamFirFilter_105));
	ENDFOR
}

void BeamFirFilter_105() {
	BeamFirFilter(&(BeamFirFilter_104BeamFirFilter_105), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[11]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_126() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_127() {
	FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_127DUPLICATE_Splitter_128, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[__iter_]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_127DUPLICATE_Splitter_128, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[__iter_]));
	ENDFOR
}

void BeamForm(buffer_float_t *chanin, buffer_float_t *chanout) {
	float real_curr = 0.0;
	float imag_curr = 0.0;
	FOR(int, i, 0,  < , 12, i++) {
		float real_pop = 0.0;
		float imag_pop = 0.0;
		real_pop = pop_float(&(*chanin)) ; 
		imag_pop = pop_float(&(*chanin)) ; 
		real_curr = (real_curr + ((BeamForm_107_s.real_weight[i] * real_pop) - (BeamForm_107_s.imag_weight[i] * imag_pop))) ; 
		imag_curr = (imag_curr + ((BeamForm_107_s.real_weight[i] * imag_pop) + (BeamForm_107_s.imag_weight[i] * real_pop))) ; 
	}
	ENDFOR
	push_float(&(*chanout), real_curr) ; 
	push_float(&(*chanout), imag_curr) ; 
}


void BeamForm_107() {
	BeamForm(&(SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_split[0]), &(BeamForm_107BeamFirFilter_108));
}

void BeamFirFilter_108() {
	BeamFirFilter(&(BeamForm_107BeamFirFilter_108), &(BeamFirFilter_108Magnitude_109));
}

float Magnitude_109_mag(float real, float imag) {
	return ((float) sqrt(((real * real) + (imag * imag))));
}
void Magnitude(buffer_float_t *chanin, buffer_float_t *chanout) {
	float f1 = 0.0;
	float f2 = 0.0;
	f1 = pop_float(&(*chanin)) ; 
	f2 = pop_float(&(*chanin)) ; 
	push_float(&(*chanout), Magnitude_109_mag(f1, f2)) ; 
}


void Magnitude_109() {
	Magnitude(&(BeamFirFilter_108Magnitude_109), &(Magnitude_109FloatPrinter_110));
}

void FloatPrinter(buffer_float_t *chanin, buffer_float_t *chanout) {
	printf("%.10f", pop_float(&(*chanin)));
	printf("\n");
}


void FloatPrinter_110() {
	FloatPrinter(&(Magnitude_109FloatPrinter_110), &(SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_join[0]));
}

void BeamForm_112() {
	BeamForm(&(SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_split[1]), &(BeamForm_112BeamFirFilter_113));
}

void BeamFirFilter_113() {
	BeamFirFilter(&(BeamForm_112BeamFirFilter_113), &(BeamFirFilter_113Magnitude_114));
}

float Magnitude_114_mag(float real, float imag) {
	return ((float) sqrt(((real * real) + (imag * imag))));
}
void Magnitude_114() {
	Magnitude(&(BeamFirFilter_113Magnitude_114), &(Magnitude_114FloatPrinter_115));
}

void FloatPrinter_115() {
	FloatPrinter(&(Magnitude_114FloatPrinter_115), &(SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_join[1]));
}

void BeamForm_117() {
	BeamForm(&(SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_split[2]), &(BeamForm_117BeamFirFilter_118));
}

void BeamFirFilter_118() {
	BeamFirFilter(&(BeamForm_117BeamFirFilter_118), &(BeamFirFilter_118Magnitude_119));
}

float Magnitude_119_mag(float real, float imag) {
	return ((float) sqrt(((real * real) + (imag * imag))));
}
void Magnitude_119() {
	Magnitude(&(BeamFirFilter_118Magnitude_119), &(Magnitude_119FloatPrinter_120));
}

void FloatPrinter_120() {
	FloatPrinter(&(Magnitude_119FloatPrinter_120), &(SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_join[2]));
}

void BeamForm_122() {
	BeamForm(&(SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_split[3]), &(BeamForm_122BeamFirFilter_123));
}

void BeamFirFilter_123() {
	BeamFirFilter(&(BeamForm_122BeamFirFilter_123), &(BeamFirFilter_123Magnitude_124));
}

float Magnitude_124_mag(float real, float imag) {
	return ((float) sqrt(((real * real) + (imag * imag))));
}
void Magnitude_124() {
	Magnitude(&(BeamFirFilter_123Magnitude_124), &(Magnitude_124FloatPrinter_125));
}

void FloatPrinter_125() {
	FloatPrinter(&(Magnitude_124FloatPrinter_125), &(SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_join[3]));
}

void DUPLICATE_Splitter_128() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_127DUPLICATE_Splitter_128);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_129() {
}

void __stream_init__() {
	init_buffer_float(&Magnitude_109FloatPrinter_110);
	init_buffer_float(&BeamFirFilter_60BeamFirFilter_61);
	init_buffer_float(&BeamFirFilter_100BeamFirFilter_101);
	init_buffer_float(&BeamFirFilter_68BeamFirFilter_69);
	init_buffer_float(&InputGenerate_67BeamFirFilter_68);
	init_buffer_float(&BeamFirFilter_104BeamFirFilter_105);
	init_buffer_float(&InputGenerate_75BeamFirFilter_76);
	init_buffer_float(&BeamForm_107BeamFirFilter_108);
	init_buffer_float(&BeamFirFilter_92BeamFirFilter_93);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&InputGenerate_99BeamFirFilter_100);
	init_buffer_float(&InputGenerate_79BeamFirFilter_80);
	init_buffer_float(&BeamFirFilter_80BeamFirFilter_81);
	init_buffer_float(&BeamFirFilter_84BeamFirFilter_85);
	init_buffer_float(&BeamFirFilter_123Magnitude_124);
	init_buffer_float(&BeamFirFilter_76BeamFirFilter_77);
	init_buffer_float(&BeamFirFilter_113Magnitude_114);
	init_buffer_float(&Magnitude_124FloatPrinter_125);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_127DUPLICATE_Splitter_128);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a3_56_132_150_168_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&InputGenerate_71BeamFirFilter_72);
	init_buffer_float(&BeamForm_112BeamFirFilter_113);
	init_buffer_float(&Magnitude_114FloatPrinter_115);
	init_buffer_float(&BeamFirFilter_118Magnitude_119);
	init_buffer_float(&BeamFirFilter_64BeamFirFilter_65);
	init_buffer_float(&BeamFirFilter_108Magnitude_109);
	init_buffer_float(&BeamForm_117BeamFirFilter_118);
	init_buffer_float(&InputGenerate_63BeamFirFilter_64);
	init_buffer_float(&InputGenerate_103BeamFirFilter_104);
	init_buffer_float(&BeamFirFilter_72BeamFirFilter_73);
	init_buffer_float(&InputGenerate_59BeamFirFilter_60);
	init_buffer_float(&InputGenerate_95BeamFirFilter_96);
	init_buffer_float(&Magnitude_119FloatPrinter_120);
	init_buffer_float(&InputGenerate_87BeamFirFilter_88);
	init_buffer_float(&BeamFirFilter_88BeamFirFilter_89);
	init_buffer_float(&InputGenerate_83BeamFirFilter_84);
	init_buffer_float(&BeamForm_122BeamFirFilter_123);
	FOR(int, __iter_init_2_, 0, <, 12, __iter_init_2_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 12, __iter_init_3_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_50_130_148_167_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&BeamFirFilter_96BeamFirFilter_97);
	init_buffer_float(&InputGenerate_91BeamFirFilter_92);
// --- init: InputGenerate_59
	 {
	InputGenerate_59_s.curSample = 0 ; 
}
//--------------------------------
// --- init: BeamFirFilter_60
	 {
	BeamFirFilter_60_s.pos = 0 ; 
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_60_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_60_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_61
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_61_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_61_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_64
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_64_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_64_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_65
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_65_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_65_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_68
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_68_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_68_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_69
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_69_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_69_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_72
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_72_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_72_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_73
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_73_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_73_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_76
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_76_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_76_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_77
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_77_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_77_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_80
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_80_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_80_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_81
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_81_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_81_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_84
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_84_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_84_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_85
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_85_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_85_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_88
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_88_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_88_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_89
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_89_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_89_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_92
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_92_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_92_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_93
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_93_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_93_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_96
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_96_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_96_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_97
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_97_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_97_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_100
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_100_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_100_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_101
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_101_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_101_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_104
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_104_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_104_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_105
	 {
	FOR(int, j, 0,  < , 64, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_105_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_105_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamForm_107
	 {
	FOR(int, j, 0,  < , 12, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamForm_107_s.real_weight[j] = (((float) sin(idx)) / (0 + idx)) ; 
		BeamForm_107_s.imag_weight[j] = (((float) cos(idx)) / (0 + idx)) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_108
	 {
	FOR(int, j, 0,  < , 512, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_108_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_108_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamForm_112
	 {
	FOR(int, j, 0,  < , 12, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamForm_112_s.real_weight[j] = (((float) sin(idx)) / (1 + idx)) ; 
		BeamForm_112_s.imag_weight[j] = (((float) cos(idx)) / (1 + idx)) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_113
	 {
	FOR(int, j, 0,  < , 512, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_113_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_113_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamForm_117
	 {
	FOR(int, j, 0,  < , 12, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamForm_117_s.real_weight[j] = (((float) sin(idx)) / (2 + idx)) ; 
		BeamForm_117_s.imag_weight[j] = (((float) cos(idx)) / (2 + idx)) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_118
	 {
	FOR(int, j, 0,  < , 512, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_118_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_118_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamForm_122
	 {
	FOR(int, j, 0,  < , 12, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamForm_122_s.real_weight[j] = (((float) sin(idx)) / (3 + idx)) ; 
		BeamForm_122_s.imag_weight[j] = (((float) cos(idx)) / (3 + idx)) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: BeamFirFilter_123
	 {
	BeamFirFilter_123_s.pos = 0 ; 
	FOR(int, j, 0,  < , 512, j++) {
		int idx = 0;
		idx = (j + 1) ; 
		BeamFirFilter_123_s.real_weight[j] = (((float) sin(idx)) / idx) ; 
		BeamFirFilter_123_s.imag_weight[j] = (((float) cos(idx)) / idx) ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_126();
			InputGenerate_59();
			BeamFirFilter_60();
			BeamFirFilter_61();
			InputGenerate_63();
			BeamFirFilter_64();
			BeamFirFilter_65();
			InputGenerate_67();
			BeamFirFilter_68();
			BeamFirFilter_69();
			InputGenerate_71();
			BeamFirFilter_72();
			BeamFirFilter_73();
			InputGenerate_75();
			BeamFirFilter_76();
			BeamFirFilter_77();
			InputGenerate_79();
			BeamFirFilter_80();
			BeamFirFilter_81();
			InputGenerate_83();
			BeamFirFilter_84();
			BeamFirFilter_85();
			InputGenerate_87();
			BeamFirFilter_88();
			BeamFirFilter_89();
			InputGenerate_91();
			BeamFirFilter_92();
			BeamFirFilter_93();
			InputGenerate_95();
			BeamFirFilter_96();
			BeamFirFilter_97();
			InputGenerate_99();
			BeamFirFilter_100();
			BeamFirFilter_101();
			InputGenerate_103();
			BeamFirFilter_104();
			BeamFirFilter_105();
		WEIGHTED_ROUND_ROBIN_Joiner_127();
		DUPLICATE_Splitter_128();
			BeamForm_107();
			BeamFirFilter_108();
			Magnitude_109();
			FloatPrinter_110();
			BeamForm_112();
			BeamFirFilter_113();
			Magnitude_114();
			FloatPrinter_115();
			BeamForm_117();
			BeamFirFilter_118();
			Magnitude_119();
			FloatPrinter_120();
			BeamForm_122();
			BeamFirFilter_123();
			Magnitude_124();
			FloatPrinter_125();
		WEIGHTED_ROUND_ROBIN_Joiner_129();
	ENDFOR
	return EXIT_SUCCESS;
}
