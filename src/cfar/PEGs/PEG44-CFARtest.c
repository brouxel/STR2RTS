#include "PEG44-CFARtest.h"

buffer_complex_t ComplexSource_2453WEIGHTED_ROUND_ROBIN_Splitter_2459;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2505_2507_split[44];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_2505_2507_join[44];
buffer_float_t CFAR_gather_2456AnonFilter_a0_2457;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2460CFAR_gather_2456;


ComplexSource_2453_t ComplexSource_2453_s;
CFAR_gather_2456_t CFAR_gather_2456_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2453_s.theta = (ComplexSource_2453_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2453_s.theta)) * (((float) cos(ComplexSource_2453_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2453_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2453_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2453_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2453_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2453_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2453_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2453() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		ComplexSource(&(ComplexSource_2453WEIGHTED_ROUND_ROBIN_Splitter_2459));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2461() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[0]));
	ENDFOR
}

void SquareAndScale_2462() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[1]));
	ENDFOR
}

void SquareAndScale_2463() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[2]));
	ENDFOR
}

void SquareAndScale_2464() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[3]));
	ENDFOR
}

void SquareAndScale_2465() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[4]));
	ENDFOR
}

void SquareAndScale_2466() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[5]));
	ENDFOR
}

void SquareAndScale_2467() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[6]));
	ENDFOR
}

void SquareAndScale_2468() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[7]));
	ENDFOR
}

void SquareAndScale_2469() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[8]));
	ENDFOR
}

void SquareAndScale_2470() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[9]));
	ENDFOR
}

void SquareAndScale_2471() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[10]));
	ENDFOR
}

void SquareAndScale_2472() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[11]));
	ENDFOR
}

void SquareAndScale_2473() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[12]));
	ENDFOR
}

void SquareAndScale_2474() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[13]));
	ENDFOR
}

void SquareAndScale_2475() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[14]));
	ENDFOR
}

void SquareAndScale_2476() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[15]));
	ENDFOR
}

void SquareAndScale_2477() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[16]));
	ENDFOR
}

void SquareAndScale_2478() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[17]));
	ENDFOR
}

void SquareAndScale_2479() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[18]));
	ENDFOR
}

void SquareAndScale_2480() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[19]));
	ENDFOR
}

void SquareAndScale_2481() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[20]));
	ENDFOR
}

void SquareAndScale_2482() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[21]));
	ENDFOR
}

void SquareAndScale_2483() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[22]));
	ENDFOR
}

void SquareAndScale_2484() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[23]));
	ENDFOR
}

void SquareAndScale_2485() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[24]));
	ENDFOR
}

void SquareAndScale_2486() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[25]));
	ENDFOR
}

void SquareAndScale_2487() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[26]));
	ENDFOR
}

void SquareAndScale_2488() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[27]));
	ENDFOR
}

void SquareAndScale_2489() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[28]));
	ENDFOR
}

void SquareAndScale_2490() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[29]));
	ENDFOR
}

void SquareAndScale_2491() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[30]));
	ENDFOR
}

void SquareAndScale_2492() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[31]));
	ENDFOR
}

void SquareAndScale_2493() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[32]));
	ENDFOR
}

void SquareAndScale_2494() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[33]));
	ENDFOR
}

void SquareAndScale_2495() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[34]));
	ENDFOR
}

void SquareAndScale_2496() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[35]));
	ENDFOR
}

void SquareAndScale_2497() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[36]));
	ENDFOR
}

void SquareAndScale_2498() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[37]));
	ENDFOR
}

void SquareAndScale_2499() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[38]));
	ENDFOR
}

void SquareAndScale_2500() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[39]));
	ENDFOR
}

void SquareAndScale_2501() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[40]));
	ENDFOR
}

void SquareAndScale_2502() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[41]));
	ENDFOR
}

void SquareAndScale_2503() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[42]));
	ENDFOR
}

void SquareAndScale_2504() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[43]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2459() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 44, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2505_2507_split[__iter_], pop_complex(&ComplexSource_2453WEIGHTED_ROUND_ROBIN_Splitter_2459));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2460() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 44, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2460CFAR_gather_2456, pop_float(&SplitJoin0_SquareAndScale_Fiss_2505_2507_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2456_s.pos) - 5) >= 0)), __DEFLOOPBOUND__128__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2456_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2456_s.pos) < 64)), __DEFLOOPBOUND__129__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2456_s.poke[(i - 1)] = CFAR_gather_2456_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2456_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2456_s.pos++ ; 
		if(CFAR_gather_2456_s.pos == 64) {
			CFAR_gather_2456_s.pos = 0 ; 
		}
	}


void CFAR_gather_2456() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2460CFAR_gather_2456), &(CFAR_gather_2456AnonFilter_a0_2457));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2457() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2456AnonFilter_a0_2457));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_2453WEIGHTED_ROUND_ROBIN_Splitter_2459);
	FOR(int, __iter_init_0_, 0, <, 44, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2505_2507_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 44, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2505_2507_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_2456AnonFilter_a0_2457);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2460CFAR_gather_2456);
// --- init: ComplexSource_2453
	 {
	ComplexSource_2453_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2453WEIGHTED_ROUND_ROBIN_Splitter_2459));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2459
	
	FOR(uint32_t, __iter_, 0, <, 44, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2505_2507_split[__iter_], pop_complex(&ComplexSource_2453WEIGHTED_ROUND_ROBIN_Splitter_2459));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2461
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[0]));
//--------------------------------
// --- init: SquareAndScale_2462
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[1]));
//--------------------------------
// --- init: SquareAndScale_2463
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[2]));
//--------------------------------
// --- init: SquareAndScale_2464
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[3]));
//--------------------------------
// --- init: SquareAndScale_2465
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[4]));
//--------------------------------
// --- init: SquareAndScale_2466
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[5]));
//--------------------------------
// --- init: SquareAndScale_2467
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[6]));
//--------------------------------
// --- init: SquareAndScale_2468
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[7]));
//--------------------------------
// --- init: SquareAndScale_2469
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[8]));
//--------------------------------
// --- init: SquareAndScale_2470
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[9]));
//--------------------------------
// --- init: SquareAndScale_2471
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[10]));
//--------------------------------
// --- init: SquareAndScale_2472
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[11]));
//--------------------------------
// --- init: SquareAndScale_2473
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[12]));
//--------------------------------
// --- init: SquareAndScale_2474
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[13]));
//--------------------------------
// --- init: SquareAndScale_2475
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[14]));
//--------------------------------
// --- init: SquareAndScale_2476
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[15]));
//--------------------------------
// --- init: SquareAndScale_2477
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[16]));
//--------------------------------
// --- init: SquareAndScale_2478
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[17]));
//--------------------------------
// --- init: SquareAndScale_2479
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[18]));
//--------------------------------
// --- init: SquareAndScale_2480
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[19]));
//--------------------------------
// --- init: SquareAndScale_2481
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[20]));
//--------------------------------
// --- init: SquareAndScale_2482
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[21]));
//--------------------------------
// --- init: SquareAndScale_2483
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[22]));
//--------------------------------
// --- init: SquareAndScale_2484
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[23]));
//--------------------------------
// --- init: SquareAndScale_2485
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[24]));
//--------------------------------
// --- init: SquareAndScale_2486
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[25]));
//--------------------------------
// --- init: SquareAndScale_2487
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[26]));
//--------------------------------
// --- init: SquareAndScale_2488
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[27]));
//--------------------------------
// --- init: SquareAndScale_2489
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[28]));
//--------------------------------
// --- init: SquareAndScale_2490
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[29]));
//--------------------------------
// --- init: SquareAndScale_2491
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[30]));
//--------------------------------
// --- init: SquareAndScale_2492
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[31]));
//--------------------------------
// --- init: SquareAndScale_2493
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[32]));
//--------------------------------
// --- init: SquareAndScale_2494
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[33]));
//--------------------------------
// --- init: SquareAndScale_2495
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[34]));
//--------------------------------
// --- init: SquareAndScale_2496
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[35]));
//--------------------------------
// --- init: SquareAndScale_2497
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[36]));
//--------------------------------
// --- init: SquareAndScale_2498
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[37]));
//--------------------------------
// --- init: SquareAndScale_2499
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[38]));
//--------------------------------
// --- init: SquareAndScale_2500
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[39]));
//--------------------------------
// --- init: SquareAndScale_2501
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[40]));
//--------------------------------
// --- init: SquareAndScale_2502
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[41]));
//--------------------------------
// --- init: SquareAndScale_2503
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[42]));
//--------------------------------
// --- init: SquareAndScale_2504
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2505_2507_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2505_2507_join[43]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2460
	
	FOR(uint32_t, __iter_, 0, <, 44, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2460CFAR_gather_2456, pop_float(&SplitJoin0_SquareAndScale_Fiss_2505_2507_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2456
	 {
	CFAR_gather_2456_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2460CFAR_gather_2456), &(CFAR_gather_2456AnonFilter_a0_2457));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2457
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2456AnonFilter_a0_2457));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2453();
		WEIGHTED_ROUND_ROBIN_Splitter_2459();
			SquareAndScale_2461();
			SquareAndScale_2462();
			SquareAndScale_2463();
			SquareAndScale_2464();
			SquareAndScale_2465();
			SquareAndScale_2466();
			SquareAndScale_2467();
			SquareAndScale_2468();
			SquareAndScale_2469();
			SquareAndScale_2470();
			SquareAndScale_2471();
			SquareAndScale_2472();
			SquareAndScale_2473();
			SquareAndScale_2474();
			SquareAndScale_2475();
			SquareAndScale_2476();
			SquareAndScale_2477();
			SquareAndScale_2478();
			SquareAndScale_2479();
			SquareAndScale_2480();
			SquareAndScale_2481();
			SquareAndScale_2482();
			SquareAndScale_2483();
			SquareAndScale_2484();
			SquareAndScale_2485();
			SquareAndScale_2486();
			SquareAndScale_2487();
			SquareAndScale_2488();
			SquareAndScale_2489();
			SquareAndScale_2490();
			SquareAndScale_2491();
			SquareAndScale_2492();
			SquareAndScale_2493();
			SquareAndScale_2494();
			SquareAndScale_2495();
			SquareAndScale_2496();
			SquareAndScale_2497();
			SquareAndScale_2498();
			SquareAndScale_2499();
			SquareAndScale_2500();
			SquareAndScale_2501();
			SquareAndScale_2502();
			SquareAndScale_2503();
			SquareAndScale_2504();
		WEIGHTED_ROUND_ROBIN_Joiner_2460();
		CFAR_gather_2456();
		AnonFilter_a0_2457();
	ENDFOR
	return EXIT_SUCCESS;
}
