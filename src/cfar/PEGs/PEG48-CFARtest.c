#include "PEG48-CFARtest.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2089_2091_split[48];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2040CFAR_gather_2036;
buffer_float_t CFAR_gather_2036AnonFilter_a0_2037;
buffer_complex_t ComplexSource_2033WEIGHTED_ROUND_ROBIN_Splitter_2039;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_2089_2091_join[48];


ComplexSource_2033_t ComplexSource_2033_s;
CFAR_gather_2036_t CFAR_gather_2036_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2033_s.theta = (ComplexSource_2033_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2033_s.theta)) * (((float) cos(ComplexSource_2033_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2033_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2033_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2033_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2033_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2033_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2033_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2033() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		ComplexSource(&(ComplexSource_2033WEIGHTED_ROUND_ROBIN_Splitter_2039));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2041() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[0]));
	ENDFOR
}

void SquareAndScale_2042() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[1]));
	ENDFOR
}

void SquareAndScale_2043() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[2]));
	ENDFOR
}

void SquareAndScale_2044() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[3]));
	ENDFOR
}

void SquareAndScale_2045() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[4]));
	ENDFOR
}

void SquareAndScale_2046() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[5]));
	ENDFOR
}

void SquareAndScale_2047() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[6]));
	ENDFOR
}

void SquareAndScale_2048() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[7]));
	ENDFOR
}

void SquareAndScale_2049() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[8]));
	ENDFOR
}

void SquareAndScale_2050() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[9]));
	ENDFOR
}

void SquareAndScale_2051() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[10]));
	ENDFOR
}

void SquareAndScale_2052() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[11]));
	ENDFOR
}

void SquareAndScale_2053() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[12]));
	ENDFOR
}

void SquareAndScale_2054() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[13]));
	ENDFOR
}

void SquareAndScale_2055() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[14]));
	ENDFOR
}

void SquareAndScale_2056() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[15]));
	ENDFOR
}

void SquareAndScale_2057() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[16]));
	ENDFOR
}

void SquareAndScale_2058() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[17]));
	ENDFOR
}

void SquareAndScale_2059() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[18]));
	ENDFOR
}

void SquareAndScale_2060() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[19]));
	ENDFOR
}

void SquareAndScale_2061() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[20]));
	ENDFOR
}

void SquareAndScale_2062() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[21]));
	ENDFOR
}

void SquareAndScale_2063() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[22]));
	ENDFOR
}

void SquareAndScale_2064() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[23]));
	ENDFOR
}

void SquareAndScale_2065() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[24]));
	ENDFOR
}

void SquareAndScale_2066() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[25]));
	ENDFOR
}

void SquareAndScale_2067() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[26]));
	ENDFOR
}

void SquareAndScale_2068() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[27]));
	ENDFOR
}

void SquareAndScale_2069() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[28]));
	ENDFOR
}

void SquareAndScale_2070() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[29]));
	ENDFOR
}

void SquareAndScale_2071() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[30]));
	ENDFOR
}

void SquareAndScale_2072() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[31]));
	ENDFOR
}

void SquareAndScale_2073() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[32]));
	ENDFOR
}

void SquareAndScale_2074() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[33]));
	ENDFOR
}

void SquareAndScale_2075() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[34]));
	ENDFOR
}

void SquareAndScale_2076() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[35]));
	ENDFOR
}

void SquareAndScale_2077() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[36]));
	ENDFOR
}

void SquareAndScale_2078() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[37]));
	ENDFOR
}

void SquareAndScale_2079() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[38]));
	ENDFOR
}

void SquareAndScale_2080() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[39]));
	ENDFOR
}

void SquareAndScale_2081() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[40]));
	ENDFOR
}

void SquareAndScale_2082() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[41]));
	ENDFOR
}

void SquareAndScale_2083() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[42]));
	ENDFOR
}

void SquareAndScale_2084() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[43]));
	ENDFOR
}

void SquareAndScale_2085() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[44]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[44]));
	ENDFOR
}

void SquareAndScale_2086() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[45]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[45]));
	ENDFOR
}

void SquareAndScale_2087() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[46]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[46]));
	ENDFOR
}

void SquareAndScale_2088() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[47]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[47]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 48, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2089_2091_split[__iter_], pop_complex(&ComplexSource_2033WEIGHTED_ROUND_ROBIN_Splitter_2039));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2040() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 48, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2040CFAR_gather_2036, pop_float(&SplitJoin0_SquareAndScale_Fiss_2089_2091_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2036_s.pos) - 5) >= 0)), __DEFLOOPBOUND__104__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2036_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2036_s.pos) < 64)), __DEFLOOPBOUND__105__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2036_s.poke[(i - 1)] = CFAR_gather_2036_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2036_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2036_s.pos++ ; 
		if(CFAR_gather_2036_s.pos == 64) {
			CFAR_gather_2036_s.pos = 0 ; 
		}
	}


void CFAR_gather_2036() {
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2040CFAR_gather_2036), &(CFAR_gather_2036AnonFilter_a0_2037));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2037() {
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2036AnonFilter_a0_2037));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 48, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2089_2091_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2040CFAR_gather_2036);
	init_buffer_float(&CFAR_gather_2036AnonFilter_a0_2037);
	init_buffer_complex(&ComplexSource_2033WEIGHTED_ROUND_ROBIN_Splitter_2039);
	FOR(int, __iter_init_1_, 0, <, 48, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2089_2091_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_2033
	 {
	ComplexSource_2033_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2033WEIGHTED_ROUND_ROBIN_Splitter_2039));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2039
	
	FOR(uint32_t, __iter_, 0, <, 48, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2089_2091_split[__iter_], pop_complex(&ComplexSource_2033WEIGHTED_ROUND_ROBIN_Splitter_2039));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2041
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[0]));
//--------------------------------
// --- init: SquareAndScale_2042
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[1]));
//--------------------------------
// --- init: SquareAndScale_2043
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[2]));
//--------------------------------
// --- init: SquareAndScale_2044
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[3]));
//--------------------------------
// --- init: SquareAndScale_2045
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[4]));
//--------------------------------
// --- init: SquareAndScale_2046
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[5]));
//--------------------------------
// --- init: SquareAndScale_2047
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[6]));
//--------------------------------
// --- init: SquareAndScale_2048
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[7]));
//--------------------------------
// --- init: SquareAndScale_2049
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[8]));
//--------------------------------
// --- init: SquareAndScale_2050
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[9]));
//--------------------------------
// --- init: SquareAndScale_2051
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[10]));
//--------------------------------
// --- init: SquareAndScale_2052
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[11]));
//--------------------------------
// --- init: SquareAndScale_2053
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[12]));
//--------------------------------
// --- init: SquareAndScale_2054
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[13]));
//--------------------------------
// --- init: SquareAndScale_2055
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[14]));
//--------------------------------
// --- init: SquareAndScale_2056
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[15]));
//--------------------------------
// --- init: SquareAndScale_2057
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[16]));
//--------------------------------
// --- init: SquareAndScale_2058
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[17]));
//--------------------------------
// --- init: SquareAndScale_2059
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[18]));
//--------------------------------
// --- init: SquareAndScale_2060
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[19]));
//--------------------------------
// --- init: SquareAndScale_2061
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[20]));
//--------------------------------
// --- init: SquareAndScale_2062
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[21]));
//--------------------------------
// --- init: SquareAndScale_2063
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[22]));
//--------------------------------
// --- init: SquareAndScale_2064
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[23]));
//--------------------------------
// --- init: SquareAndScale_2065
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[24]));
//--------------------------------
// --- init: SquareAndScale_2066
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[25]));
//--------------------------------
// --- init: SquareAndScale_2067
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[26]));
//--------------------------------
// --- init: SquareAndScale_2068
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[27]));
//--------------------------------
// --- init: SquareAndScale_2069
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[28]));
//--------------------------------
// --- init: SquareAndScale_2070
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[29]));
//--------------------------------
// --- init: SquareAndScale_2071
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[30]));
//--------------------------------
// --- init: SquareAndScale_2072
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[31]));
//--------------------------------
// --- init: SquareAndScale_2073
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[32]));
//--------------------------------
// --- init: SquareAndScale_2074
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[33]));
//--------------------------------
// --- init: SquareAndScale_2075
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[34]));
//--------------------------------
// --- init: SquareAndScale_2076
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[35]));
//--------------------------------
// --- init: SquareAndScale_2077
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[36]));
//--------------------------------
// --- init: SquareAndScale_2078
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[37]));
//--------------------------------
// --- init: SquareAndScale_2079
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[38]));
//--------------------------------
// --- init: SquareAndScale_2080
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[39]));
//--------------------------------
// --- init: SquareAndScale_2081
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[40]));
//--------------------------------
// --- init: SquareAndScale_2082
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[41]));
//--------------------------------
// --- init: SquareAndScale_2083
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[42]));
//--------------------------------
// --- init: SquareAndScale_2084
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[43]));
//--------------------------------
// --- init: SquareAndScale_2085
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[44]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[44]));
//--------------------------------
// --- init: SquareAndScale_2086
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[45]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[45]));
//--------------------------------
// --- init: SquareAndScale_2087
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[46]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[46]));
//--------------------------------
// --- init: SquareAndScale_2088
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2089_2091_split[47]), &(SplitJoin0_SquareAndScale_Fiss_2089_2091_join[47]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2040
	
	FOR(uint32_t, __iter_, 0, <, 48, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2040CFAR_gather_2036, pop_float(&SplitJoin0_SquareAndScale_Fiss_2089_2091_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2036
	 {
	CFAR_gather_2036_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 39, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2040CFAR_gather_2036), &(CFAR_gather_2036AnonFilter_a0_2037));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2037
	FOR(uint32_t, __iter_init_, 0, <, 39, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2036AnonFilter_a0_2037));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2033();
		WEIGHTED_ROUND_ROBIN_Splitter_2039();
			SquareAndScale_2041();
			SquareAndScale_2042();
			SquareAndScale_2043();
			SquareAndScale_2044();
			SquareAndScale_2045();
			SquareAndScale_2046();
			SquareAndScale_2047();
			SquareAndScale_2048();
			SquareAndScale_2049();
			SquareAndScale_2050();
			SquareAndScale_2051();
			SquareAndScale_2052();
			SquareAndScale_2053();
			SquareAndScale_2054();
			SquareAndScale_2055();
			SquareAndScale_2056();
			SquareAndScale_2057();
			SquareAndScale_2058();
			SquareAndScale_2059();
			SquareAndScale_2060();
			SquareAndScale_2061();
			SquareAndScale_2062();
			SquareAndScale_2063();
			SquareAndScale_2064();
			SquareAndScale_2065();
			SquareAndScale_2066();
			SquareAndScale_2067();
			SquareAndScale_2068();
			SquareAndScale_2069();
			SquareAndScale_2070();
			SquareAndScale_2071();
			SquareAndScale_2072();
			SquareAndScale_2073();
			SquareAndScale_2074();
			SquareAndScale_2075();
			SquareAndScale_2076();
			SquareAndScale_2077();
			SquareAndScale_2078();
			SquareAndScale_2079();
			SquareAndScale_2080();
			SquareAndScale_2081();
			SquareAndScale_2082();
			SquareAndScale_2083();
			SquareAndScale_2084();
			SquareAndScale_2085();
			SquareAndScale_2086();
			SquareAndScale_2087();
			SquareAndScale_2088();
		WEIGHTED_ROUND_ROBIN_Joiner_2040();
		CFAR_gather_2036();
		AnonFilter_a0_2037();
	ENDFOR
	return EXIT_SUCCESS;
}
