#include "PEG41-CFARtest_nocache.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_2796_2798_join[41];
buffer_complex_t ComplexSource_2747WEIGHTED_ROUND_ROBIN_Splitter_2753;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2796_2798_split[41];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2754CFAR_gather_2750;
buffer_float_t CFAR_gather_2750AnonFilter_a0_2751;


ComplexSource_2747_t ComplexSource_2747_s;
CFAR_gather_2750_t CFAR_gather_2750_s;

void ComplexSource_2747(){
	FOR(uint32_t, __iter_steady_, 0, <, 41, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2747_s.theta = (ComplexSource_2747_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2747_s.theta)) * (((float) cos(ComplexSource_2747_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2747_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2747_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2747_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2747_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2747_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2747_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_2747WEIGHTED_ROUND_ROBIN_Splitter_2753, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_2755(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2756(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2757(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2758(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2759(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2760(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2761(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2762(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2763(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2764(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2765(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2766(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2767(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2768(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2769(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2770(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2771(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2772(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2773(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2774(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2775(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2776(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[21]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[21], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2777(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[22]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[22], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2778(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[23]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[23], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2779(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[24]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[24], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2780(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[25]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[25], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2781(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[26]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[26], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2782(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[27]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[27], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2783(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[28]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[28], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2784(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[29]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[29], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2785(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[30]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[30], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2786(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[31]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[31], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2787(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[32]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[32], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2788(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[33]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[33], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2789(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[34]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[34], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2790(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[35]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[35], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2791(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[36]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[36], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2792(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[37]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[37], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2793(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[38]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[38], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2794(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[39]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[39], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2795(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[40]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[40], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2753() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 41, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[__iter_], pop_complex(&ComplexSource_2747WEIGHTED_ROUND_ROBIN_Splitter_2753));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2754() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 41, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2754CFAR_gather_2750, pop_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_2750(){
	FOR(uint32_t, __iter_steady_, 0, <, 2624, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2750_s.pos) - 5) >= 0)), __DEFLOOPBOUND__146__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2750_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2750_s.pos) < 64)), __DEFLOOPBOUND__147__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_2754CFAR_gather_2750, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_2750AnonFilter_a0_2751, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2750_s.poke[(i - 1)] = CFAR_gather_2750_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2750_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2754CFAR_gather_2750) ; 
		CFAR_gather_2750_s.pos++ ; 
		if(CFAR_gather_2750_s.pos == 64) {
			CFAR_gather_2750_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_2751(){
	FOR(uint32_t, __iter_steady_, 0, <, 2624, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_2750AnonFilter_a0_2751));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 41, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_2747WEIGHTED_ROUND_ROBIN_Splitter_2753);
	FOR(int, __iter_init_1_, 0, <, 41, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2754CFAR_gather_2750);
	init_buffer_float(&CFAR_gather_2750AnonFilter_a0_2751);
// --- init: ComplexSource_2747
	 {
	ComplexSource_2747_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_2747_s.theta = (ComplexSource_2747_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_2747_s.theta)) * (((float) cos(ComplexSource_2747_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2747_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2747_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_2747_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2747_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2747_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2747_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_2747WEIGHTED_ROUND_ROBIN_Splitter_2753, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2753
	
	FOR(uint32_t, __iter_, 0, <, 41, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[__iter_], pop_complex(&ComplexSource_2747WEIGHTED_ROUND_ROBIN_Splitter_2753));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2755
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[0]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[0], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2756
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[1]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[1], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2757
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[2]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[2], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2758
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[3]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[3], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2759
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[4]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[4], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2760
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[5]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[5], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2761
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[6]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[6], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2762
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[7]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[7], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2763
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[8]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[8], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2764
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[9]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[9], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2765
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[10]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[10], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2766
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[11]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[11], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2767
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[12]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[12], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2768
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[13]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[13], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2769
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[14]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[14], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2770
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[15]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[15], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2771
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[16]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[16], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2772
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[17]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[17], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2773
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[18]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[18], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2774
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[19]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[19], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2775
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[20]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[20], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2776
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[21]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[21], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2777
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[22]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[22], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2778
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[23]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[23], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2779
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[24]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[24], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2780
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[25]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[25], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2781
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[26]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[26], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2782
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[27]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[27], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2783
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[28]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[28], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2784
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[29]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[29], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2785
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[30]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[30], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2786
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[31]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[31], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2787
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[32]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[32], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2788
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[33]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[33], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2789
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[34]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[34], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2790
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[35]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[35], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2791
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[36]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[36], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2792
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[37]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[37], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2793
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[38]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[38], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2794
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[39]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[39], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2795
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2796_2798_split[40]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[40], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2754
	
	FOR(uint32_t, __iter_, 0, <, 41, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2754CFAR_gather_2750, pop_float(&SplitJoin0_SquareAndScale_Fiss_2796_2798_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2750
	 {
	CFAR_gather_2750_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 32, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2750_s.pos) - 5) >= 0)), __DEFLOOPBOUND__148__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2750_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2750_s.pos) < 64)), __DEFLOOPBOUND__149__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_2754CFAR_gather_2750, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_2750AnonFilter_a0_2751, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2750_s.poke[(i - 1)] = CFAR_gather_2750_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2750_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2754CFAR_gather_2750) ; 
		CFAR_gather_2750_s.pos++ ; 
		if(CFAR_gather_2750_s.pos == 64) {
			CFAR_gather_2750_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2751
	FOR(uint32_t, __iter_init_, 0, <, 32, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_2750AnonFilter_a0_2751));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2747();
		WEIGHTED_ROUND_ROBIN_Splitter_2753();
			SquareAndScale_2755();
			SquareAndScale_2756();
			SquareAndScale_2757();
			SquareAndScale_2758();
			SquareAndScale_2759();
			SquareAndScale_2760();
			SquareAndScale_2761();
			SquareAndScale_2762();
			SquareAndScale_2763();
			SquareAndScale_2764();
			SquareAndScale_2765();
			SquareAndScale_2766();
			SquareAndScale_2767();
			SquareAndScale_2768();
			SquareAndScale_2769();
			SquareAndScale_2770();
			SquareAndScale_2771();
			SquareAndScale_2772();
			SquareAndScale_2773();
			SquareAndScale_2774();
			SquareAndScale_2775();
			SquareAndScale_2776();
			SquareAndScale_2777();
			SquareAndScale_2778();
			SquareAndScale_2779();
			SquareAndScale_2780();
			SquareAndScale_2781();
			SquareAndScale_2782();
			SquareAndScale_2783();
			SquareAndScale_2784();
			SquareAndScale_2785();
			SquareAndScale_2786();
			SquareAndScale_2787();
			SquareAndScale_2788();
			SquareAndScale_2789();
			SquareAndScale_2790();
			SquareAndScale_2791();
			SquareAndScale_2792();
			SquareAndScale_2793();
			SquareAndScale_2794();
			SquareAndScale_2795();
		WEIGHTED_ROUND_ROBIN_Joiner_2754();
		CFAR_gather_2750();
		AnonFilter_a0_2751();
	ENDFOR
	return EXIT_SUCCESS;
}
