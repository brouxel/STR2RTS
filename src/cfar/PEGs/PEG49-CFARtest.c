#include "PEG49-CFARtest.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_1980_1982_join[49];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1980_1982_split[49];
buffer_complex_t ComplexSource_1923WEIGHTED_ROUND_ROBIN_Splitter_1929;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1930CFAR_gather_1926;
buffer_float_t CFAR_gather_1926AnonFilter_a0_1927;


ComplexSource_1923_t ComplexSource_1923_s;
CFAR_gather_1926_t CFAR_gather_1926_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_1923_s.theta = (ComplexSource_1923_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_1923_s.theta)) * (((float) cos(ComplexSource_1923_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1923_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1923_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_1923_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1923_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1923_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1923_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_1923() {
	FOR(uint32_t, __iter_steady_, 0, <, 49, __iter_steady_++)
		ComplexSource(&(ComplexSource_1923WEIGHTED_ROUND_ROBIN_Splitter_1929));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_1931() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[0]));
	ENDFOR
}

void SquareAndScale_1932() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[1]));
	ENDFOR
}

void SquareAndScale_1933() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[2]));
	ENDFOR
}

void SquareAndScale_1934() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[3]));
	ENDFOR
}

void SquareAndScale_1935() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[4]));
	ENDFOR
}

void SquareAndScale_1936() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[5]));
	ENDFOR
}

void SquareAndScale_1937() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[6]));
	ENDFOR
}

void SquareAndScale_1938() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[7]));
	ENDFOR
}

void SquareAndScale_1939() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[8]));
	ENDFOR
}

void SquareAndScale_1940() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[9]));
	ENDFOR
}

void SquareAndScale_1941() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[10]));
	ENDFOR
}

void SquareAndScale_1942() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[11]));
	ENDFOR
}

void SquareAndScale_1943() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[12]));
	ENDFOR
}

void SquareAndScale_1944() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[13]));
	ENDFOR
}

void SquareAndScale_1945() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[14]));
	ENDFOR
}

void SquareAndScale_1946() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[15]));
	ENDFOR
}

void SquareAndScale_1947() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[16]));
	ENDFOR
}

void SquareAndScale_1948() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[17]));
	ENDFOR
}

void SquareAndScale_1949() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[18]));
	ENDFOR
}

void SquareAndScale_1950() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[19]));
	ENDFOR
}

void SquareAndScale_1951() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[20]));
	ENDFOR
}

void SquareAndScale_1952() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[21]));
	ENDFOR
}

void SquareAndScale_1953() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[22]));
	ENDFOR
}

void SquareAndScale_1954() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[23]));
	ENDFOR
}

void SquareAndScale_1955() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[24]));
	ENDFOR
}

void SquareAndScale_1956() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[25]));
	ENDFOR
}

void SquareAndScale_1957() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[26]));
	ENDFOR
}

void SquareAndScale_1958() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[27]));
	ENDFOR
}

void SquareAndScale_1959() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[28]));
	ENDFOR
}

void SquareAndScale_1960() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[29]));
	ENDFOR
}

void SquareAndScale_1961() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[30]));
	ENDFOR
}

void SquareAndScale_1962() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[31]));
	ENDFOR
}

void SquareAndScale_1963() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[32]));
	ENDFOR
}

void SquareAndScale_1964() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[33]));
	ENDFOR
}

void SquareAndScale_1965() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[34]));
	ENDFOR
}

void SquareAndScale_1966() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[35]));
	ENDFOR
}

void SquareAndScale_1967() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[36]));
	ENDFOR
}

void SquareAndScale_1968() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[37]));
	ENDFOR
}

void SquareAndScale_1969() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[38]));
	ENDFOR
}

void SquareAndScale_1970() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[39]));
	ENDFOR
}

void SquareAndScale_1971() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[40]));
	ENDFOR
}

void SquareAndScale_1972() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[41]));
	ENDFOR
}

void SquareAndScale_1973() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[42]));
	ENDFOR
}

void SquareAndScale_1974() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[43]));
	ENDFOR
}

void SquareAndScale_1975() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[44]));
	ENDFOR
}

void SquareAndScale_1976() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[45]));
	ENDFOR
}

void SquareAndScale_1977() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[46]));
	ENDFOR
}

void SquareAndScale_1978() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[47]));
	ENDFOR
}

void SquareAndScale_1979() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[48]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1929() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 49, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1980_1982_split[__iter_], pop_complex(&ComplexSource_1923WEIGHTED_ROUND_ROBIN_Splitter_1929));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1930() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 49, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1930CFAR_gather_1926, pop_float(&SplitJoin0_SquareAndScale_Fiss_1980_1982_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1926_s.pos) - 5) >= 0)), __DEFLOOPBOUND__98__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1926_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1926_s.pos) < 64)), __DEFLOOPBOUND__99__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1926_s.poke[(i - 1)] = CFAR_gather_1926_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1926_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_1926_s.pos++ ; 
		if(CFAR_gather_1926_s.pos == 64) {
			CFAR_gather_1926_s.pos = 0 ; 
		}
	}


void CFAR_gather_1926() {
	FOR(uint32_t, __iter_steady_, 0, <, 3136, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1930CFAR_gather_1926), &(CFAR_gather_1926AnonFilter_a0_1927));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_1927() {
	FOR(uint32_t, __iter_steady_, 0, <, 3136, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_1926AnonFilter_a0_1927));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 49, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1980_1982_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 49, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1980_1982_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_1923WEIGHTED_ROUND_ROBIN_Splitter_1929);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1930CFAR_gather_1926);
	init_buffer_float(&CFAR_gather_1926AnonFilter_a0_1927);
// --- init: ComplexSource_1923
	 {
	ComplexSource_1923_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_1923WEIGHTED_ROUND_ROBIN_Splitter_1929));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1929
	
	FOR(uint32_t, __iter_, 0, <, 49, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1980_1982_split[__iter_], pop_complex(&ComplexSource_1923WEIGHTED_ROUND_ROBIN_Splitter_1929));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_1931
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[0]));
//--------------------------------
// --- init: SquareAndScale_1932
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[1]));
//--------------------------------
// --- init: SquareAndScale_1933
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[2]));
//--------------------------------
// --- init: SquareAndScale_1934
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[3]));
//--------------------------------
// --- init: SquareAndScale_1935
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[4]));
//--------------------------------
// --- init: SquareAndScale_1936
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[5]));
//--------------------------------
// --- init: SquareAndScale_1937
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[6]));
//--------------------------------
// --- init: SquareAndScale_1938
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[7]));
//--------------------------------
// --- init: SquareAndScale_1939
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[8]));
//--------------------------------
// --- init: SquareAndScale_1940
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[9]));
//--------------------------------
// --- init: SquareAndScale_1941
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[10]));
//--------------------------------
// --- init: SquareAndScale_1942
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[11]));
//--------------------------------
// --- init: SquareAndScale_1943
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[12]));
//--------------------------------
// --- init: SquareAndScale_1944
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[13]));
//--------------------------------
// --- init: SquareAndScale_1945
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[14]));
//--------------------------------
// --- init: SquareAndScale_1946
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[15]));
//--------------------------------
// --- init: SquareAndScale_1947
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[16]));
//--------------------------------
// --- init: SquareAndScale_1948
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[17]));
//--------------------------------
// --- init: SquareAndScale_1949
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[18]));
//--------------------------------
// --- init: SquareAndScale_1950
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[19]));
//--------------------------------
// --- init: SquareAndScale_1951
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[20]));
//--------------------------------
// --- init: SquareAndScale_1952
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[21]));
//--------------------------------
// --- init: SquareAndScale_1953
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[22]));
//--------------------------------
// --- init: SquareAndScale_1954
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[23]));
//--------------------------------
// --- init: SquareAndScale_1955
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[24]));
//--------------------------------
// --- init: SquareAndScale_1956
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[25]));
//--------------------------------
// --- init: SquareAndScale_1957
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[26]));
//--------------------------------
// --- init: SquareAndScale_1958
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[27]));
//--------------------------------
// --- init: SquareAndScale_1959
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[28]));
//--------------------------------
// --- init: SquareAndScale_1960
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[29]));
//--------------------------------
// --- init: SquareAndScale_1961
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[30]));
//--------------------------------
// --- init: SquareAndScale_1962
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[31]));
//--------------------------------
// --- init: SquareAndScale_1963
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[32]));
//--------------------------------
// --- init: SquareAndScale_1964
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[33]));
//--------------------------------
// --- init: SquareAndScale_1965
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[34]));
//--------------------------------
// --- init: SquareAndScale_1966
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[35]));
//--------------------------------
// --- init: SquareAndScale_1967
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[36]));
//--------------------------------
// --- init: SquareAndScale_1968
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[37]));
//--------------------------------
// --- init: SquareAndScale_1969
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[38]));
//--------------------------------
// --- init: SquareAndScale_1970
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[39]));
//--------------------------------
// --- init: SquareAndScale_1971
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[40]));
//--------------------------------
// --- init: SquareAndScale_1972
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[41]));
//--------------------------------
// --- init: SquareAndScale_1973
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[42]));
//--------------------------------
// --- init: SquareAndScale_1974
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[43]));
//--------------------------------
// --- init: SquareAndScale_1975
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[44]));
//--------------------------------
// --- init: SquareAndScale_1976
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[45]));
//--------------------------------
// --- init: SquareAndScale_1977
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[46]));
//--------------------------------
// --- init: SquareAndScale_1978
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[47]));
//--------------------------------
// --- init: SquareAndScale_1979
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1980_1982_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1980_1982_join[48]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1930
	
	FOR(uint32_t, __iter_, 0, <, 49, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1930CFAR_gather_1926, pop_float(&SplitJoin0_SquareAndScale_Fiss_1980_1982_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_1926
	 {
	CFAR_gather_1926_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1930CFAR_gather_1926), &(CFAR_gather_1926AnonFilter_a0_1927));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_1927
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_1926AnonFilter_a0_1927));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_1923();
		WEIGHTED_ROUND_ROBIN_Splitter_1929();
			SquareAndScale_1931();
			SquareAndScale_1932();
			SquareAndScale_1933();
			SquareAndScale_1934();
			SquareAndScale_1935();
			SquareAndScale_1936();
			SquareAndScale_1937();
			SquareAndScale_1938();
			SquareAndScale_1939();
			SquareAndScale_1940();
			SquareAndScale_1941();
			SquareAndScale_1942();
			SquareAndScale_1943();
			SquareAndScale_1944();
			SquareAndScale_1945();
			SquareAndScale_1946();
			SquareAndScale_1947();
			SquareAndScale_1948();
			SquareAndScale_1949();
			SquareAndScale_1950();
			SquareAndScale_1951();
			SquareAndScale_1952();
			SquareAndScale_1953();
			SquareAndScale_1954();
			SquareAndScale_1955();
			SquareAndScale_1956();
			SquareAndScale_1957();
			SquareAndScale_1958();
			SquareAndScale_1959();
			SquareAndScale_1960();
			SquareAndScale_1961();
			SquareAndScale_1962();
			SquareAndScale_1963();
			SquareAndScale_1964();
			SquareAndScale_1965();
			SquareAndScale_1966();
			SquareAndScale_1967();
			SquareAndScale_1968();
			SquareAndScale_1969();
			SquareAndScale_1970();
			SquareAndScale_1971();
			SquareAndScale_1972();
			SquareAndScale_1973();
			SquareAndScale_1974();
			SquareAndScale_1975();
			SquareAndScale_1976();
			SquareAndScale_1977();
			SquareAndScale_1978();
			SquareAndScale_1979();
		WEIGHTED_ROUND_ROBIN_Joiner_1930();
		CFAR_gather_1926();
		AnonFilter_a0_1927();
	ENDFOR
	return EXIT_SUCCESS;
}
