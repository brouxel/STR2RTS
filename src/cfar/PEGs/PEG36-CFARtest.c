#include "PEG36-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3204CFAR_gather_3200;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3241_3243_join[36];
buffer_float_t CFAR_gather_3200AnonFilter_a0_3201;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3241_3243_split[36];
buffer_complex_t ComplexSource_3197WEIGHTED_ROUND_ROBIN_Splitter_3203;


ComplexSource_3197_t ComplexSource_3197_s;
CFAR_gather_3200_t CFAR_gather_3200_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3197_s.theta = (ComplexSource_3197_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3197_s.theta)) * (((float) cos(ComplexSource_3197_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3197_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3197_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3197_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3197_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3197_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3197_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3197() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		ComplexSource(&(ComplexSource_3197WEIGHTED_ROUND_ROBIN_Splitter_3203));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3205() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[0]));
	ENDFOR
}

void SquareAndScale_3206() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[1]));
	ENDFOR
}

void SquareAndScale_3207() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[2]));
	ENDFOR
}

void SquareAndScale_3208() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[3]));
	ENDFOR
}

void SquareAndScale_3209() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[4]));
	ENDFOR
}

void SquareAndScale_3210() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[5]));
	ENDFOR
}

void SquareAndScale_3211() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[6]));
	ENDFOR
}

void SquareAndScale_3212() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[7]));
	ENDFOR
}

void SquareAndScale_3213() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[8]));
	ENDFOR
}

void SquareAndScale_3214() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[9]));
	ENDFOR
}

void SquareAndScale_3215() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[10]));
	ENDFOR
}

void SquareAndScale_3216() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[11]));
	ENDFOR
}

void SquareAndScale_3217() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[12]));
	ENDFOR
}

void SquareAndScale_3218() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[13]));
	ENDFOR
}

void SquareAndScale_3219() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[14]));
	ENDFOR
}

void SquareAndScale_3220() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[15]));
	ENDFOR
}

void SquareAndScale_3221() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[16]));
	ENDFOR
}

void SquareAndScale_3222() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[17]));
	ENDFOR
}

void SquareAndScale_3223() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[18]));
	ENDFOR
}

void SquareAndScale_3224() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[19]));
	ENDFOR
}

void SquareAndScale_3225() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[20]));
	ENDFOR
}

void SquareAndScale_3226() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[21]));
	ENDFOR
}

void SquareAndScale_3227() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[22]));
	ENDFOR
}

void SquareAndScale_3228() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[23]));
	ENDFOR
}

void SquareAndScale_3229() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[24]));
	ENDFOR
}

void SquareAndScale_3230() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[25]));
	ENDFOR
}

void SquareAndScale_3231() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[26]));
	ENDFOR
}

void SquareAndScale_3232() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[27]));
	ENDFOR
}

void SquareAndScale_3233() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[28]));
	ENDFOR
}

void SquareAndScale_3234() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[29]));
	ENDFOR
}

void SquareAndScale_3235() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[30]));
	ENDFOR
}

void SquareAndScale_3236() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[31]));
	ENDFOR
}

void SquareAndScale_3237() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[32]));
	ENDFOR
}

void SquareAndScale_3238() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[33]));
	ENDFOR
}

void SquareAndScale_3239() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[34]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[34]));
	ENDFOR
}

void SquareAndScale_3240() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[35]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[35]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3203() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 36, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3241_3243_split[__iter_], pop_complex(&ComplexSource_3197WEIGHTED_ROUND_ROBIN_Splitter_3203));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3204() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 36, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3204CFAR_gather_3200, pop_float(&SplitJoin0_SquareAndScale_Fiss_3241_3243_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3200_s.pos) - 5) >= 0)), __DEFLOOPBOUND__176__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3200_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3200_s.pos) < 64)), __DEFLOOPBOUND__177__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3200_s.poke[(i - 1)] = CFAR_gather_3200_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3200_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3200_s.pos++ ; 
		if(CFAR_gather_3200_s.pos == 64) {
			CFAR_gather_3200_s.pos = 0 ; 
		}
	}


void CFAR_gather_3200() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3204CFAR_gather_3200), &(CFAR_gather_3200AnonFilter_a0_3201));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3201() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3200AnonFilter_a0_3201));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3204CFAR_gather_3200);
	FOR(int, __iter_init_0_, 0, <, 36, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3241_3243_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_3200AnonFilter_a0_3201);
	FOR(int, __iter_init_1_, 0, <, 36, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3241_3243_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_3197WEIGHTED_ROUND_ROBIN_Splitter_3203);
// --- init: ComplexSource_3197
	 {
	ComplexSource_3197_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3197WEIGHTED_ROUND_ROBIN_Splitter_3203));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3203
	
	FOR(uint32_t, __iter_, 0, <, 36, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_3241_3243_split[__iter_], pop_complex(&ComplexSource_3197WEIGHTED_ROUND_ROBIN_Splitter_3203));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3205
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[0]));
//--------------------------------
// --- init: SquareAndScale_3206
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[1]));
//--------------------------------
// --- init: SquareAndScale_3207
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[2]));
//--------------------------------
// --- init: SquareAndScale_3208
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[3]));
//--------------------------------
// --- init: SquareAndScale_3209
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[4]));
//--------------------------------
// --- init: SquareAndScale_3210
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[5]));
//--------------------------------
// --- init: SquareAndScale_3211
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[6]));
//--------------------------------
// --- init: SquareAndScale_3212
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[7]));
//--------------------------------
// --- init: SquareAndScale_3213
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[8]));
//--------------------------------
// --- init: SquareAndScale_3214
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[9]));
//--------------------------------
// --- init: SquareAndScale_3215
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[10]));
//--------------------------------
// --- init: SquareAndScale_3216
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[11]));
//--------------------------------
// --- init: SquareAndScale_3217
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[12]));
//--------------------------------
// --- init: SquareAndScale_3218
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[13]));
//--------------------------------
// --- init: SquareAndScale_3219
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[14]));
//--------------------------------
// --- init: SquareAndScale_3220
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[15]));
//--------------------------------
// --- init: SquareAndScale_3221
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[16]));
//--------------------------------
// --- init: SquareAndScale_3222
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[17]));
//--------------------------------
// --- init: SquareAndScale_3223
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[18]));
//--------------------------------
// --- init: SquareAndScale_3224
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[19]));
//--------------------------------
// --- init: SquareAndScale_3225
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[20]));
//--------------------------------
// --- init: SquareAndScale_3226
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[21]));
//--------------------------------
// --- init: SquareAndScale_3227
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[22]));
//--------------------------------
// --- init: SquareAndScale_3228
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[23]));
//--------------------------------
// --- init: SquareAndScale_3229
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[24]));
//--------------------------------
// --- init: SquareAndScale_3230
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[25]));
//--------------------------------
// --- init: SquareAndScale_3231
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[26]));
//--------------------------------
// --- init: SquareAndScale_3232
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[27]));
//--------------------------------
// --- init: SquareAndScale_3233
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[28]));
//--------------------------------
// --- init: SquareAndScale_3234
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[29]));
//--------------------------------
// --- init: SquareAndScale_3235
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[30]));
//--------------------------------
// --- init: SquareAndScale_3236
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[31]));
//--------------------------------
// --- init: SquareAndScale_3237
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[32]));
//--------------------------------
// --- init: SquareAndScale_3238
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[33]));
//--------------------------------
// --- init: SquareAndScale_3239
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[34]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[34]));
//--------------------------------
// --- init: SquareAndScale_3240
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3241_3243_split[35]), &(SplitJoin0_SquareAndScale_Fiss_3241_3243_join[35]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3204
	
	FOR(uint32_t, __iter_, 0, <, 36, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3204CFAR_gather_3200, pop_float(&SplitJoin0_SquareAndScale_Fiss_3241_3243_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3200
	 {
	CFAR_gather_3200_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 27, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3204CFAR_gather_3200), &(CFAR_gather_3200AnonFilter_a0_3201));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3201
	FOR(uint32_t, __iter_init_, 0, <, 27, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3200AnonFilter_a0_3201));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3197();
		WEIGHTED_ROUND_ROBIN_Splitter_3203();
			SquareAndScale_3205();
			SquareAndScale_3206();
			SquareAndScale_3207();
			SquareAndScale_3208();
			SquareAndScale_3209();
			SquareAndScale_3210();
			SquareAndScale_3211();
			SquareAndScale_3212();
			SquareAndScale_3213();
			SquareAndScale_3214();
			SquareAndScale_3215();
			SquareAndScale_3216();
			SquareAndScale_3217();
			SquareAndScale_3218();
			SquareAndScale_3219();
			SquareAndScale_3220();
			SquareAndScale_3221();
			SquareAndScale_3222();
			SquareAndScale_3223();
			SquareAndScale_3224();
			SquareAndScale_3225();
			SquareAndScale_3226();
			SquareAndScale_3227();
			SquareAndScale_3228();
			SquareAndScale_3229();
			SquareAndScale_3230();
			SquareAndScale_3231();
			SquareAndScale_3232();
			SquareAndScale_3233();
			SquareAndScale_3234();
			SquareAndScale_3235();
			SquareAndScale_3236();
			SquareAndScale_3237();
			SquareAndScale_3238();
			SquareAndScale_3239();
			SquareAndScale_3240();
		WEIGHTED_ROUND_ROBIN_Joiner_3204();
		CFAR_gather_3200();
		AnonFilter_a0_3201();
	ENDFOR
	return EXIT_SUCCESS;
}
