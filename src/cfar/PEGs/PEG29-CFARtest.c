#include "PEG29-CFARtest.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_3780_3782_join[29];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3780_3782_split[29];
buffer_complex_t ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749;
buffer_float_t CFAR_gather_3746AnonFilter_a0_3747;


ComplexSource_3743_t ComplexSource_3743_s;
CFAR_gather_3746_t CFAR_gather_3746_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3743_s.theta = (ComplexSource_3743_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3743_s.theta)) * (((float) cos(ComplexSource_3743_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3743_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3743_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3743_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3743_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3743_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3743_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3743() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		ComplexSource(&(ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3751() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[0]));
	ENDFOR
}

void SquareAndScale_3752() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[1]));
	ENDFOR
}

void SquareAndScale_3753() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[2]));
	ENDFOR
}

void SquareAndScale_3754() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[3]));
	ENDFOR
}

void SquareAndScale_3755() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[4]));
	ENDFOR
}

void SquareAndScale_3756() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[5]));
	ENDFOR
}

void SquareAndScale_3757() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[6]));
	ENDFOR
}

void SquareAndScale_3758() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[7]));
	ENDFOR
}

void SquareAndScale_3759() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[8]));
	ENDFOR
}

void SquareAndScale_3760() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[9]));
	ENDFOR
}

void SquareAndScale_3761() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[10]));
	ENDFOR
}

void SquareAndScale_3762() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[11]));
	ENDFOR
}

void SquareAndScale_3763() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[12]));
	ENDFOR
}

void SquareAndScale_3764() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[13]));
	ENDFOR
}

void SquareAndScale_3765() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[14]));
	ENDFOR
}

void SquareAndScale_3766() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[15]));
	ENDFOR
}

void SquareAndScale_3767() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[16]));
	ENDFOR
}

void SquareAndScale_3768() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[17]));
	ENDFOR
}

void SquareAndScale_3769() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[18]));
	ENDFOR
}

void SquareAndScale_3770() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[19]));
	ENDFOR
}

void SquareAndScale_3771() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[20]));
	ENDFOR
}

void SquareAndScale_3772() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[21]));
	ENDFOR
}

void SquareAndScale_3773() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[22]));
	ENDFOR
}

void SquareAndScale_3774() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[23]));
	ENDFOR
}

void SquareAndScale_3775() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[24]));
	ENDFOR
}

void SquareAndScale_3776() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[25]));
	ENDFOR
}

void SquareAndScale_3777() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[26]));
	ENDFOR
}

void SquareAndScale_3778() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[27]));
	ENDFOR
}

void SquareAndScale_3779() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[28]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3749() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[__iter_], pop_complex(&ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3750() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746, pop_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3746_s.pos) - 5) >= 0)), __DEFLOOPBOUND__218__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3746_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3746_s.pos) < 64)), __DEFLOOPBOUND__219__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3746_s.poke[(i - 1)] = CFAR_gather_3746_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3746_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3746_s.pos++ ; 
		if(CFAR_gather_3746_s.pos == 64) {
			CFAR_gather_3746_s.pos = 0 ; 
		}
	}


void CFAR_gather_3746() {
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746), &(CFAR_gather_3746AnonFilter_a0_3747));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3747() {
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3746AnonFilter_a0_3747));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 29, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746);
	FOR(int, __iter_init_1_, 0, <, 29, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749);
	init_buffer_float(&CFAR_gather_3746AnonFilter_a0_3747);
// --- init: ComplexSource_3743
	 {
	ComplexSource_3743_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3749
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[__iter_], pop_complex(&ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3751
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3752
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3753
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3754
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3755
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3756
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3757
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3758
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3759
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3760
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3761
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3762
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3763
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3764
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3765
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3766
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3767
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3768
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3769
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3770
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3771
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3772
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3773
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[22]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3774
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[23]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3775
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[24]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3776
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[25]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3777
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[26]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3778
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[27]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3779
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3780_3782_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3780_3782_join[28]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3750
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746, pop_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3746
	 {
	CFAR_gather_3746_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 49, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746), &(CFAR_gather_3746AnonFilter_a0_3747));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3747
	FOR(uint32_t, __iter_init_, 0, <, 49, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3746AnonFilter_a0_3747));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3743();
		WEIGHTED_ROUND_ROBIN_Splitter_3749();
			SquareAndScale_3751();
			SquareAndScale_3752();
			SquareAndScale_3753();
			SquareAndScale_3754();
			SquareAndScale_3755();
			SquareAndScale_3756();
			SquareAndScale_3757();
			SquareAndScale_3758();
			SquareAndScale_3759();
			SquareAndScale_3760();
			SquareAndScale_3761();
			SquareAndScale_3762();
			SquareAndScale_3763();
			SquareAndScale_3764();
			SquareAndScale_3765();
			SquareAndScale_3766();
			SquareAndScale_3767();
			SquareAndScale_3768();
			SquareAndScale_3769();
			SquareAndScale_3770();
			SquareAndScale_3771();
			SquareAndScale_3772();
			SquareAndScale_3773();
			SquareAndScale_3774();
			SquareAndScale_3775();
			SquareAndScale_3776();
			SquareAndScale_3777();
			SquareAndScale_3778();
			SquareAndScale_3779();
		WEIGHTED_ROUND_ROBIN_Joiner_3750();
		CFAR_gather_3746();
		AnonFilter_a0_3747();
	ENDFOR
	return EXIT_SUCCESS;
}
