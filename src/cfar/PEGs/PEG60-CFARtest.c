#include "PEG60-CFARtest.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_649_651_split[60];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_649_651_join[60];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_588CFAR_gather_584;
buffer_complex_t ComplexSource_581WEIGHTED_ROUND_ROBIN_Splitter_587;
buffer_float_t CFAR_gather_584AnonFilter_a0_585;


ComplexSource_581_t ComplexSource_581_s;
CFAR_gather_584_t CFAR_gather_584_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_581_s.theta = (ComplexSource_581_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_581_s.theta)) * (((float) cos(ComplexSource_581_s.theta)) + ((0.0 * ((float) sin(ComplexSource_581_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_581_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_581_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_581_s.theta))))) + (0.0 * (((float) cos(ComplexSource_581_s.theta)) + ((0.0 * ((float) sin(ComplexSource_581_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_581() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		ComplexSource(&(ComplexSource_581WEIGHTED_ROUND_ROBIN_Splitter_587));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_589() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[0]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[0]));
	ENDFOR
}

void SquareAndScale_590() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[1]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[1]));
	ENDFOR
}

void SquareAndScale_591() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[2]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[2]));
	ENDFOR
}

void SquareAndScale_592() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[3]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[3]));
	ENDFOR
}

void SquareAndScale_593() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[4]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[4]));
	ENDFOR
}

void SquareAndScale_594() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[5]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[5]));
	ENDFOR
}

void SquareAndScale_595() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[6]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[6]));
	ENDFOR
}

void SquareAndScale_596() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[7]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[7]));
	ENDFOR
}

void SquareAndScale_597() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[8]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[8]));
	ENDFOR
}

void SquareAndScale_598() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[9]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[9]));
	ENDFOR
}

void SquareAndScale_599() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[10]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[10]));
	ENDFOR
}

void SquareAndScale_600() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[11]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[11]));
	ENDFOR
}

void SquareAndScale_601() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[12]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[12]));
	ENDFOR
}

void SquareAndScale_602() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[13]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[13]));
	ENDFOR
}

void SquareAndScale_603() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[14]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[14]));
	ENDFOR
}

void SquareAndScale_604() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[15]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[15]));
	ENDFOR
}

void SquareAndScale_605() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[16]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[16]));
	ENDFOR
}

void SquareAndScale_606() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[17]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[17]));
	ENDFOR
}

void SquareAndScale_607() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[18]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[18]));
	ENDFOR
}

void SquareAndScale_608() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[19]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[19]));
	ENDFOR
}

void SquareAndScale_609() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[20]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[20]));
	ENDFOR
}

void SquareAndScale_610() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[21]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[21]));
	ENDFOR
}

void SquareAndScale_611() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[22]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[22]));
	ENDFOR
}

void SquareAndScale_612() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[23]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[23]));
	ENDFOR
}

void SquareAndScale_613() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[24]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[24]));
	ENDFOR
}

void SquareAndScale_614() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[25]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[25]));
	ENDFOR
}

void SquareAndScale_615() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[26]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[26]));
	ENDFOR
}

void SquareAndScale_616() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[27]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[27]));
	ENDFOR
}

void SquareAndScale_617() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[28]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[28]));
	ENDFOR
}

void SquareAndScale_618() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[29]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[29]));
	ENDFOR
}

void SquareAndScale_619() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[30]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[30]));
	ENDFOR
}

void SquareAndScale_620() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[31]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[31]));
	ENDFOR
}

void SquareAndScale_621() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[32]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[32]));
	ENDFOR
}

void SquareAndScale_622() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[33]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[33]));
	ENDFOR
}

void SquareAndScale_623() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[34]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[34]));
	ENDFOR
}

void SquareAndScale_624() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[35]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[35]));
	ENDFOR
}

void SquareAndScale_625() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[36]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[36]));
	ENDFOR
}

void SquareAndScale_626() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[37]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[37]));
	ENDFOR
}

void SquareAndScale_627() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[38]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[38]));
	ENDFOR
}

void SquareAndScale_628() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[39]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[39]));
	ENDFOR
}

void SquareAndScale_629() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[40]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[40]));
	ENDFOR
}

void SquareAndScale_630() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[41]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[41]));
	ENDFOR
}

void SquareAndScale_631() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[42]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[42]));
	ENDFOR
}

void SquareAndScale_632() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[43]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[43]));
	ENDFOR
}

void SquareAndScale_633() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[44]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[44]));
	ENDFOR
}

void SquareAndScale_634() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[45]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[45]));
	ENDFOR
}

void SquareAndScale_635() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[46]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[46]));
	ENDFOR
}

void SquareAndScale_636() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[47]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[47]));
	ENDFOR
}

void SquareAndScale_637() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[48]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[48]));
	ENDFOR
}

void SquareAndScale_638() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[49]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[49]));
	ENDFOR
}

void SquareAndScale_639() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[50]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[50]));
	ENDFOR
}

void SquareAndScale_640() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[51]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[51]));
	ENDFOR
}

void SquareAndScale_641() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[52]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[52]));
	ENDFOR
}

void SquareAndScale_642() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[53]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[53]));
	ENDFOR
}

void SquareAndScale_643() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[54]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[54]));
	ENDFOR
}

void SquareAndScale_644() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[55]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[55]));
	ENDFOR
}

void SquareAndScale_645() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[56]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[56]));
	ENDFOR
}

void SquareAndScale_646() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[57]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[57]));
	ENDFOR
}

void SquareAndScale_647() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[58]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[58]));
	ENDFOR
}

void SquareAndScale_648() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[59]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[59]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_587() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 60, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_649_651_split[__iter_], pop_complex(&ComplexSource_581WEIGHTED_ROUND_ROBIN_Splitter_587));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_588() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 60, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_588CFAR_gather_584, pop_float(&SplitJoin0_SquareAndScale_Fiss_649_651_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_584_s.pos) - 5) >= 0)), __DEFLOOPBOUND__32__, i__conflict__1++) {
			sum = (sum + CFAR_gather_584_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_584_s.pos) < 64)), __DEFLOOPBOUND__33__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_584_s.poke[(i - 1)] = CFAR_gather_584_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_584_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_584_s.pos++ ; 
		if(CFAR_gather_584_s.pos == 64) {
			CFAR_gather_584_s.pos = 0 ; 
		}
	}


void CFAR_gather_584() {
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_588CFAR_gather_584), &(CFAR_gather_584AnonFilter_a0_585));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_585() {
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_584AnonFilter_a0_585));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 60, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_649_651_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 60, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_649_651_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_588CFAR_gather_584);
	init_buffer_complex(&ComplexSource_581WEIGHTED_ROUND_ROBIN_Splitter_587);
	init_buffer_float(&CFAR_gather_584AnonFilter_a0_585);
// --- init: ComplexSource_581
	 {
	ComplexSource_581_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_581WEIGHTED_ROUND_ROBIN_Splitter_587));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_587
	
	FOR(uint32_t, __iter_, 0, <, 60, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_649_651_split[__iter_], pop_complex(&ComplexSource_581WEIGHTED_ROUND_ROBIN_Splitter_587));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_589
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[0]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[0]));
//--------------------------------
// --- init: SquareAndScale_590
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[1]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[1]));
//--------------------------------
// --- init: SquareAndScale_591
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[2]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[2]));
//--------------------------------
// --- init: SquareAndScale_592
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[3]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[3]));
//--------------------------------
// --- init: SquareAndScale_593
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[4]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[4]));
//--------------------------------
// --- init: SquareAndScale_594
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[5]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[5]));
//--------------------------------
// --- init: SquareAndScale_595
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[6]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[6]));
//--------------------------------
// --- init: SquareAndScale_596
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[7]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[7]));
//--------------------------------
// --- init: SquareAndScale_597
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[8]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[8]));
//--------------------------------
// --- init: SquareAndScale_598
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[9]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[9]));
//--------------------------------
// --- init: SquareAndScale_599
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[10]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[10]));
//--------------------------------
// --- init: SquareAndScale_600
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[11]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[11]));
//--------------------------------
// --- init: SquareAndScale_601
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[12]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[12]));
//--------------------------------
// --- init: SquareAndScale_602
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[13]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[13]));
//--------------------------------
// --- init: SquareAndScale_603
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[14]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[14]));
//--------------------------------
// --- init: SquareAndScale_604
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[15]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[15]));
//--------------------------------
// --- init: SquareAndScale_605
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[16]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[16]));
//--------------------------------
// --- init: SquareAndScale_606
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[17]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[17]));
//--------------------------------
// --- init: SquareAndScale_607
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[18]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[18]));
//--------------------------------
// --- init: SquareAndScale_608
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[19]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[19]));
//--------------------------------
// --- init: SquareAndScale_609
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[20]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[20]));
//--------------------------------
// --- init: SquareAndScale_610
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[21]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[21]));
//--------------------------------
// --- init: SquareAndScale_611
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[22]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[22]));
//--------------------------------
// --- init: SquareAndScale_612
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[23]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[23]));
//--------------------------------
// --- init: SquareAndScale_613
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[24]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[24]));
//--------------------------------
// --- init: SquareAndScale_614
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[25]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[25]));
//--------------------------------
// --- init: SquareAndScale_615
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[26]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[26]));
//--------------------------------
// --- init: SquareAndScale_616
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[27]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[27]));
//--------------------------------
// --- init: SquareAndScale_617
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[28]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[28]));
//--------------------------------
// --- init: SquareAndScale_618
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[29]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[29]));
//--------------------------------
// --- init: SquareAndScale_619
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[30]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[30]));
//--------------------------------
// --- init: SquareAndScale_620
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[31]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[31]));
//--------------------------------
// --- init: SquareAndScale_621
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[32]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[32]));
//--------------------------------
// --- init: SquareAndScale_622
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[33]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[33]));
//--------------------------------
// --- init: SquareAndScale_623
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[34]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[34]));
//--------------------------------
// --- init: SquareAndScale_624
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[35]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[35]));
//--------------------------------
// --- init: SquareAndScale_625
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[36]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[36]));
//--------------------------------
// --- init: SquareAndScale_626
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[37]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[37]));
//--------------------------------
// --- init: SquareAndScale_627
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[38]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[38]));
//--------------------------------
// --- init: SquareAndScale_628
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[39]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[39]));
//--------------------------------
// --- init: SquareAndScale_629
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[40]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[40]));
//--------------------------------
// --- init: SquareAndScale_630
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[41]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[41]));
//--------------------------------
// --- init: SquareAndScale_631
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[42]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[42]));
//--------------------------------
// --- init: SquareAndScale_632
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[43]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[43]));
//--------------------------------
// --- init: SquareAndScale_633
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[44]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[44]));
//--------------------------------
// --- init: SquareAndScale_634
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[45]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[45]));
//--------------------------------
// --- init: SquareAndScale_635
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[46]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[46]));
//--------------------------------
// --- init: SquareAndScale_636
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[47]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[47]));
//--------------------------------
// --- init: SquareAndScale_637
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[48]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[48]));
//--------------------------------
// --- init: SquareAndScale_638
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[49]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[49]));
//--------------------------------
// --- init: SquareAndScale_639
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[50]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[50]));
//--------------------------------
// --- init: SquareAndScale_640
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[51]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[51]));
//--------------------------------
// --- init: SquareAndScale_641
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[52]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[52]));
//--------------------------------
// --- init: SquareAndScale_642
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[53]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[53]));
//--------------------------------
// --- init: SquareAndScale_643
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[54]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[54]));
//--------------------------------
// --- init: SquareAndScale_644
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[55]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[55]));
//--------------------------------
// --- init: SquareAndScale_645
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[56]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[56]));
//--------------------------------
// --- init: SquareAndScale_646
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[57]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[57]));
//--------------------------------
// --- init: SquareAndScale_647
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[58]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[58]));
//--------------------------------
// --- init: SquareAndScale_648
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_649_651_split[59]), &(SplitJoin0_SquareAndScale_Fiss_649_651_join[59]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_588
	
	FOR(uint32_t, __iter_, 0, <, 60, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_588CFAR_gather_584, pop_float(&SplitJoin0_SquareAndScale_Fiss_649_651_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_584
	 {
	CFAR_gather_584_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_588CFAR_gather_584), &(CFAR_gather_584AnonFilter_a0_585));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_585
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_584AnonFilter_a0_585));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_581();
		WEIGHTED_ROUND_ROBIN_Splitter_587();
			SquareAndScale_589();
			SquareAndScale_590();
			SquareAndScale_591();
			SquareAndScale_592();
			SquareAndScale_593();
			SquareAndScale_594();
			SquareAndScale_595();
			SquareAndScale_596();
			SquareAndScale_597();
			SquareAndScale_598();
			SquareAndScale_599();
			SquareAndScale_600();
			SquareAndScale_601();
			SquareAndScale_602();
			SquareAndScale_603();
			SquareAndScale_604();
			SquareAndScale_605();
			SquareAndScale_606();
			SquareAndScale_607();
			SquareAndScale_608();
			SquareAndScale_609();
			SquareAndScale_610();
			SquareAndScale_611();
			SquareAndScale_612();
			SquareAndScale_613();
			SquareAndScale_614();
			SquareAndScale_615();
			SquareAndScale_616();
			SquareAndScale_617();
			SquareAndScale_618();
			SquareAndScale_619();
			SquareAndScale_620();
			SquareAndScale_621();
			SquareAndScale_622();
			SquareAndScale_623();
			SquareAndScale_624();
			SquareAndScale_625();
			SquareAndScale_626();
			SquareAndScale_627();
			SquareAndScale_628();
			SquareAndScale_629();
			SquareAndScale_630();
			SquareAndScale_631();
			SquareAndScale_632();
			SquareAndScale_633();
			SquareAndScale_634();
			SquareAndScale_635();
			SquareAndScale_636();
			SquareAndScale_637();
			SquareAndScale_638();
			SquareAndScale_639();
			SquareAndScale_640();
			SquareAndScale_641();
			SquareAndScale_642();
			SquareAndScale_643();
			SquareAndScale_644();
			SquareAndScale_645();
			SquareAndScale_646();
			SquareAndScale_647();
			SquareAndScale_648();
		WEIGHTED_ROUND_ROBIN_Joiner_588();
		CFAR_gather_584();
		AnonFilter_a0_585();
	ENDFOR
	return EXIT_SUCCESS;
}
