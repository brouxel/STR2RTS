#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3712 on the compile command line
#else
#if BUF_SIZEMAX < 3712
#error BUF_SIZEMAX too small, it must be at least 3712
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_971_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_974_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_971();
void WEIGHTED_ROUND_ROBIN_Splitter_977();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_979();
void SquareAndScale_980();
void SquareAndScale_981();
void SquareAndScale_982();
void SquareAndScale_983();
void SquareAndScale_984();
void SquareAndScale_985();
void SquareAndScale_986();
void SquareAndScale_987();
void SquareAndScale_988();
void SquareAndScale_989();
void SquareAndScale_990();
void SquareAndScale_991();
void SquareAndScale_992();
void SquareAndScale_993();
void SquareAndScale_994();
void SquareAndScale_995();
void SquareAndScale_996();
void SquareAndScale_997();
void SquareAndScale_998();
void SquareAndScale_999();
void SquareAndScale_1000();
void SquareAndScale_1001();
void SquareAndScale_1002();
void SquareAndScale_1003();
void SquareAndScale_1004();
void SquareAndScale_1005();
void SquareAndScale_1006();
void SquareAndScale_1007();
void SquareAndScale_1008();
void SquareAndScale_1009();
void SquareAndScale_1010();
void SquareAndScale_1011();
void SquareAndScale_1012();
void SquareAndScale_1013();
void SquareAndScale_1014();
void SquareAndScale_1015();
void SquareAndScale_1016();
void SquareAndScale_1017();
void SquareAndScale_1018();
void SquareAndScale_1019();
void SquareAndScale_1020();
void SquareAndScale_1021();
void SquareAndScale_1022();
void SquareAndScale_1023();
void SquareAndScale_1024();
void SquareAndScale_1025();
void SquareAndScale_1026();
void SquareAndScale_1027();
void SquareAndScale_1028();
void SquareAndScale_1029();
void SquareAndScale_1030();
void SquareAndScale_1031();
void SquareAndScale_1032();
void SquareAndScale_1033();
void SquareAndScale_1034();
void SquareAndScale_1035();
void WEIGHTED_ROUND_ROBIN_Joiner_978();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_974();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_975();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1


#ifdef __cplusplus
}
#endif
#endif
