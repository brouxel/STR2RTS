#include "PEG34-CFARtest.h"

buffer_complex_t ComplexSource_3363WEIGHTED_ROUND_ROBIN_Splitter_3369;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3405_3407_join[34];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3405_3407_split[34];
buffer_float_t CFAR_gather_3366AnonFilter_a0_3367;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3370CFAR_gather_3366;


ComplexSource_3363_t ComplexSource_3363_s;
CFAR_gather_3366_t CFAR_gather_3366_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3363_s.theta = (ComplexSource_3363_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3363_s.theta)) * (((float) cos(ComplexSource_3363_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3363_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3363_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3363_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3363_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3363_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3363_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3363() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		ComplexSource(&(ComplexSource_3363WEIGHTED_ROUND_ROBIN_Splitter_3369));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3371() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[0]));
	ENDFOR
}

void SquareAndScale_3372() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[1]));
	ENDFOR
}

void SquareAndScale_3373() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[2]));
	ENDFOR
}

void SquareAndScale_3374() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[3]));
	ENDFOR
}

void SquareAndScale_3375() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[4]));
	ENDFOR
}

void SquareAndScale_3376() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[5]));
	ENDFOR
}

void SquareAndScale_3377() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[6]));
	ENDFOR
}

void SquareAndScale_3378() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[7]));
	ENDFOR
}

void SquareAndScale_3379() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[8]));
	ENDFOR
}

void SquareAndScale_3380() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[9]));
	ENDFOR
}

void SquareAndScale_3381() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[10]));
	ENDFOR
}

void SquareAndScale_3382() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[11]));
	ENDFOR
}

void SquareAndScale_3383() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[12]));
	ENDFOR
}

void SquareAndScale_3384() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[13]));
	ENDFOR
}

void SquareAndScale_3385() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[14]));
	ENDFOR
}

void SquareAndScale_3386() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[15]));
	ENDFOR
}

void SquareAndScale_3387() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[16]));
	ENDFOR
}

void SquareAndScale_3388() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[17]));
	ENDFOR
}

void SquareAndScale_3389() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[18]));
	ENDFOR
}

void SquareAndScale_3390() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[19]));
	ENDFOR
}

void SquareAndScale_3391() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[20]));
	ENDFOR
}

void SquareAndScale_3392() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[21]));
	ENDFOR
}

void SquareAndScale_3393() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[22]));
	ENDFOR
}

void SquareAndScale_3394() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[23]));
	ENDFOR
}

void SquareAndScale_3395() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[24]));
	ENDFOR
}

void SquareAndScale_3396() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[25]));
	ENDFOR
}

void SquareAndScale_3397() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[26]));
	ENDFOR
}

void SquareAndScale_3398() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[27]));
	ENDFOR
}

void SquareAndScale_3399() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[28]));
	ENDFOR
}

void SquareAndScale_3400() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[29]));
	ENDFOR
}

void SquareAndScale_3401() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[30]));
	ENDFOR
}

void SquareAndScale_3402() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[31]));
	ENDFOR
}

void SquareAndScale_3403() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[32]));
	ENDFOR
}

void SquareAndScale_3404() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[33]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3369() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 34, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3405_3407_split[__iter_], pop_complex(&ComplexSource_3363WEIGHTED_ROUND_ROBIN_Splitter_3369));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3370() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 34, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3370CFAR_gather_3366, pop_float(&SplitJoin0_SquareAndScale_Fiss_3405_3407_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3366_s.pos) - 5) >= 0)), __DEFLOOPBOUND__188__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3366_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3366_s.pos) < 64)), __DEFLOOPBOUND__189__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3366_s.poke[(i - 1)] = CFAR_gather_3366_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3366_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3366_s.pos++ ; 
		if(CFAR_gather_3366_s.pos == 64) {
			CFAR_gather_3366_s.pos = 0 ; 
		}
	}


void CFAR_gather_3366() {
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3370CFAR_gather_3366), &(CFAR_gather_3366AnonFilter_a0_3367));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3367() {
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3366AnonFilter_a0_3367));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_3363WEIGHTED_ROUND_ROBIN_Splitter_3369);
	FOR(int, __iter_init_0_, 0, <, 34, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3405_3407_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 34, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3405_3407_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_3366AnonFilter_a0_3367);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3370CFAR_gather_3366);
// --- init: ComplexSource_3363
	 {
	ComplexSource_3363_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3363WEIGHTED_ROUND_ROBIN_Splitter_3369));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3369
	
	FOR(uint32_t, __iter_, 0, <, 34, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_3405_3407_split[__iter_], pop_complex(&ComplexSource_3363WEIGHTED_ROUND_ROBIN_Splitter_3369));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3371
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[0]));
//--------------------------------
// --- init: SquareAndScale_3372
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[1]));
//--------------------------------
// --- init: SquareAndScale_3373
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[2]));
//--------------------------------
// --- init: SquareAndScale_3374
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[3]));
//--------------------------------
// --- init: SquareAndScale_3375
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[4]));
//--------------------------------
// --- init: SquareAndScale_3376
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[5]));
//--------------------------------
// --- init: SquareAndScale_3377
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[6]));
//--------------------------------
// --- init: SquareAndScale_3378
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[7]));
//--------------------------------
// --- init: SquareAndScale_3379
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[8]));
//--------------------------------
// --- init: SquareAndScale_3380
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[9]));
//--------------------------------
// --- init: SquareAndScale_3381
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[10]));
//--------------------------------
// --- init: SquareAndScale_3382
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[11]));
//--------------------------------
// --- init: SquareAndScale_3383
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[12]));
//--------------------------------
// --- init: SquareAndScale_3384
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[13]));
//--------------------------------
// --- init: SquareAndScale_3385
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[14]));
//--------------------------------
// --- init: SquareAndScale_3386
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[15]));
//--------------------------------
// --- init: SquareAndScale_3387
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[16]));
//--------------------------------
// --- init: SquareAndScale_3388
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[17]));
//--------------------------------
// --- init: SquareAndScale_3389
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[18]));
//--------------------------------
// --- init: SquareAndScale_3390
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[19]));
//--------------------------------
// --- init: SquareAndScale_3391
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[20]));
//--------------------------------
// --- init: SquareAndScale_3392
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[21]));
//--------------------------------
// --- init: SquareAndScale_3393
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[22]));
//--------------------------------
// --- init: SquareAndScale_3394
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[23]));
//--------------------------------
// --- init: SquareAndScale_3395
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[24]));
//--------------------------------
// --- init: SquareAndScale_3396
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[25]));
//--------------------------------
// --- init: SquareAndScale_3397
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[26]));
//--------------------------------
// --- init: SquareAndScale_3398
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[27]));
//--------------------------------
// --- init: SquareAndScale_3399
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[28]));
//--------------------------------
// --- init: SquareAndScale_3400
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[29]));
//--------------------------------
// --- init: SquareAndScale_3401
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[30]));
//--------------------------------
// --- init: SquareAndScale_3402
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[31]));
//--------------------------------
// --- init: SquareAndScale_3403
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[32]));
//--------------------------------
// --- init: SquareAndScale_3404
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3405_3407_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3405_3407_join[33]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3370
	
	FOR(uint32_t, __iter_, 0, <, 34, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3370CFAR_gather_3366, pop_float(&SplitJoin0_SquareAndScale_Fiss_3405_3407_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3366
	 {
	CFAR_gather_3366_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 25, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3370CFAR_gather_3366), &(CFAR_gather_3366AnonFilter_a0_3367));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3367
	FOR(uint32_t, __iter_init_, 0, <, 25, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3366AnonFilter_a0_3367));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3363();
		WEIGHTED_ROUND_ROBIN_Splitter_3369();
			SquareAndScale_3371();
			SquareAndScale_3372();
			SquareAndScale_3373();
			SquareAndScale_3374();
			SquareAndScale_3375();
			SquareAndScale_3376();
			SquareAndScale_3377();
			SquareAndScale_3378();
			SquareAndScale_3379();
			SquareAndScale_3380();
			SquareAndScale_3381();
			SquareAndScale_3382();
			SquareAndScale_3383();
			SquareAndScale_3384();
			SquareAndScale_3385();
			SquareAndScale_3386();
			SquareAndScale_3387();
			SquareAndScale_3388();
			SquareAndScale_3389();
			SquareAndScale_3390();
			SquareAndScale_3391();
			SquareAndScale_3392();
			SquareAndScale_3393();
			SquareAndScale_3394();
			SquareAndScale_3395();
			SquareAndScale_3396();
			SquareAndScale_3397();
			SquareAndScale_3398();
			SquareAndScale_3399();
			SquareAndScale_3400();
			SquareAndScale_3401();
			SquareAndScale_3402();
			SquareAndScale_3403();
			SquareAndScale_3404();
		WEIGHTED_ROUND_ROBIN_Joiner_3370();
		CFAR_gather_3366();
		AnonFilter_a0_3367();
	ENDFOR
	return EXIT_SUCCESS;
}
