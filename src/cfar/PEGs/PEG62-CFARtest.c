#include "PEG62-CFARtest.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_381_383_split[62];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_381_383_join[62];
buffer_float_t CFAR_gather_314AnonFilter_a0_315;
buffer_complex_t ComplexSource_311WEIGHTED_ROUND_ROBIN_Splitter_317;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_318CFAR_gather_314;


ComplexSource_311_t ComplexSource_311_s;
CFAR_gather_314_t CFAR_gather_314_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_311_s.theta = (ComplexSource_311_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_311_s.theta)) * (((float) cos(ComplexSource_311_s.theta)) + ((0.0 * ((float) sin(ComplexSource_311_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_311_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_311_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_311_s.theta))))) + (0.0 * (((float) cos(ComplexSource_311_s.theta)) + ((0.0 * ((float) sin(ComplexSource_311_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_311() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		ComplexSource(&(ComplexSource_311WEIGHTED_ROUND_ROBIN_Splitter_317));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_319() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[0]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[0]));
	ENDFOR
}

void SquareAndScale_320() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[1]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[1]));
	ENDFOR
}

void SquareAndScale_321() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[2]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[2]));
	ENDFOR
}

void SquareAndScale_322() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[3]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[3]));
	ENDFOR
}

void SquareAndScale_323() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[4]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[4]));
	ENDFOR
}

void SquareAndScale_324() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[5]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[5]));
	ENDFOR
}

void SquareAndScale_325() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[6]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[6]));
	ENDFOR
}

void SquareAndScale_326() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[7]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[7]));
	ENDFOR
}

void SquareAndScale_327() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[8]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[8]));
	ENDFOR
}

void SquareAndScale_328() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[9]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[9]));
	ENDFOR
}

void SquareAndScale_329() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[10]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[10]));
	ENDFOR
}

void SquareAndScale_330() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[11]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[11]));
	ENDFOR
}

void SquareAndScale_331() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[12]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[12]));
	ENDFOR
}

void SquareAndScale_332() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[13]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[13]));
	ENDFOR
}

void SquareAndScale_333() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[14]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[14]));
	ENDFOR
}

void SquareAndScale_334() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[15]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[15]));
	ENDFOR
}

void SquareAndScale_335() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[16]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[16]));
	ENDFOR
}

void SquareAndScale_336() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[17]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[17]));
	ENDFOR
}

void SquareAndScale_337() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[18]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[18]));
	ENDFOR
}

void SquareAndScale_338() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[19]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[19]));
	ENDFOR
}

void SquareAndScale_339() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[20]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[20]));
	ENDFOR
}

void SquareAndScale_340() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[21]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[21]));
	ENDFOR
}

void SquareAndScale_341() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[22]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[22]));
	ENDFOR
}

void SquareAndScale_342() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[23]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[23]));
	ENDFOR
}

void SquareAndScale_343() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[24]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[24]));
	ENDFOR
}

void SquareAndScale_344() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[25]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[25]));
	ENDFOR
}

void SquareAndScale_345() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[26]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[26]));
	ENDFOR
}

void SquareAndScale_346() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[27]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[27]));
	ENDFOR
}

void SquareAndScale_347() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[28]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[28]));
	ENDFOR
}

void SquareAndScale_348() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[29]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[29]));
	ENDFOR
}

void SquareAndScale_349() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[30]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[30]));
	ENDFOR
}

void SquareAndScale_350() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[31]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[31]));
	ENDFOR
}

void SquareAndScale_351() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[32]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[32]));
	ENDFOR
}

void SquareAndScale_352() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[33]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[33]));
	ENDFOR
}

void SquareAndScale_353() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[34]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[34]));
	ENDFOR
}

void SquareAndScale_354() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[35]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[35]));
	ENDFOR
}

void SquareAndScale_355() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[36]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[36]));
	ENDFOR
}

void SquareAndScale_356() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[37]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[37]));
	ENDFOR
}

void SquareAndScale_357() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[38]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[38]));
	ENDFOR
}

void SquareAndScale_358() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[39]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[39]));
	ENDFOR
}

void SquareAndScale_359() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[40]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[40]));
	ENDFOR
}

void SquareAndScale_360() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[41]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[41]));
	ENDFOR
}

void SquareAndScale_361() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[42]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[42]));
	ENDFOR
}

void SquareAndScale_362() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[43]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[43]));
	ENDFOR
}

void SquareAndScale_363() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[44]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[44]));
	ENDFOR
}

void SquareAndScale_364() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[45]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[45]));
	ENDFOR
}

void SquareAndScale_365() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[46]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[46]));
	ENDFOR
}

void SquareAndScale_366() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[47]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[47]));
	ENDFOR
}

void SquareAndScale_367() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[48]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[48]));
	ENDFOR
}

void SquareAndScale_368() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[49]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[49]));
	ENDFOR
}

void SquareAndScale_369() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[50]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[50]));
	ENDFOR
}

void SquareAndScale_370() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[51]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[51]));
	ENDFOR
}

void SquareAndScale_371() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[52]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[52]));
	ENDFOR
}

void SquareAndScale_372() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[53]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[53]));
	ENDFOR
}

void SquareAndScale_373() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[54]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[54]));
	ENDFOR
}

void SquareAndScale_374() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[55]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[55]));
	ENDFOR
}

void SquareAndScale_375() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[56]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[56]));
	ENDFOR
}

void SquareAndScale_376() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[57]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[57]));
	ENDFOR
}

void SquareAndScale_377() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[58]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[58]));
	ENDFOR
}

void SquareAndScale_378() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[59]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[59]));
	ENDFOR
}

void SquareAndScale_379() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[60]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[60]));
	ENDFOR
}

void SquareAndScale_380() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[61]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[61]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_317() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 62, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_381_383_split[__iter_], pop_complex(&ComplexSource_311WEIGHTED_ROUND_ROBIN_Splitter_317));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_318() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 62, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_318CFAR_gather_314, pop_float(&SplitJoin0_SquareAndScale_Fiss_381_383_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_314_s.pos) - 5) >= 0)), __DEFLOOPBOUND__20__, i__conflict__1++) {
			sum = (sum + CFAR_gather_314_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_314_s.pos) < 64)), __DEFLOOPBOUND__21__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_314_s.poke[(i - 1)] = CFAR_gather_314_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_314_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_314_s.pos++ ; 
		if(CFAR_gather_314_s.pos == 64) {
			CFAR_gather_314_s.pos = 0 ; 
		}
	}


void CFAR_gather_314() {
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_318CFAR_gather_314), &(CFAR_gather_314AnonFilter_a0_315));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_315() {
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_314AnonFilter_a0_315));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 62, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_381_383_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 62, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_381_383_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_314AnonFilter_a0_315);
	init_buffer_complex(&ComplexSource_311WEIGHTED_ROUND_ROBIN_Splitter_317);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_318CFAR_gather_314);
// --- init: ComplexSource_311
	 {
	ComplexSource_311_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_311WEIGHTED_ROUND_ROBIN_Splitter_317));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_317
	
	FOR(uint32_t, __iter_, 0, <, 62, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_381_383_split[__iter_], pop_complex(&ComplexSource_311WEIGHTED_ROUND_ROBIN_Splitter_317));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_319
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[0]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[0]));
//--------------------------------
// --- init: SquareAndScale_320
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[1]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[1]));
//--------------------------------
// --- init: SquareAndScale_321
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[2]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[2]));
//--------------------------------
// --- init: SquareAndScale_322
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[3]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[3]));
//--------------------------------
// --- init: SquareAndScale_323
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[4]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[4]));
//--------------------------------
// --- init: SquareAndScale_324
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[5]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[5]));
//--------------------------------
// --- init: SquareAndScale_325
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[6]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[6]));
//--------------------------------
// --- init: SquareAndScale_326
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[7]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[7]));
//--------------------------------
// --- init: SquareAndScale_327
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[8]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[8]));
//--------------------------------
// --- init: SquareAndScale_328
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[9]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[9]));
//--------------------------------
// --- init: SquareAndScale_329
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[10]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[10]));
//--------------------------------
// --- init: SquareAndScale_330
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[11]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[11]));
//--------------------------------
// --- init: SquareAndScale_331
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[12]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[12]));
//--------------------------------
// --- init: SquareAndScale_332
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[13]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[13]));
//--------------------------------
// --- init: SquareAndScale_333
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[14]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[14]));
//--------------------------------
// --- init: SquareAndScale_334
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[15]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[15]));
//--------------------------------
// --- init: SquareAndScale_335
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[16]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[16]));
//--------------------------------
// --- init: SquareAndScale_336
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[17]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[17]));
//--------------------------------
// --- init: SquareAndScale_337
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[18]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[18]));
//--------------------------------
// --- init: SquareAndScale_338
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[19]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[19]));
//--------------------------------
// --- init: SquareAndScale_339
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[20]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[20]));
//--------------------------------
// --- init: SquareAndScale_340
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[21]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[21]));
//--------------------------------
// --- init: SquareAndScale_341
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[22]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[22]));
//--------------------------------
// --- init: SquareAndScale_342
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[23]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[23]));
//--------------------------------
// --- init: SquareAndScale_343
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[24]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[24]));
//--------------------------------
// --- init: SquareAndScale_344
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[25]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[25]));
//--------------------------------
// --- init: SquareAndScale_345
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[26]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[26]));
//--------------------------------
// --- init: SquareAndScale_346
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[27]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[27]));
//--------------------------------
// --- init: SquareAndScale_347
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[28]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[28]));
//--------------------------------
// --- init: SquareAndScale_348
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[29]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[29]));
//--------------------------------
// --- init: SquareAndScale_349
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[30]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[30]));
//--------------------------------
// --- init: SquareAndScale_350
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[31]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[31]));
//--------------------------------
// --- init: SquareAndScale_351
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[32]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[32]));
//--------------------------------
// --- init: SquareAndScale_352
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[33]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[33]));
//--------------------------------
// --- init: SquareAndScale_353
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[34]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[34]));
//--------------------------------
// --- init: SquareAndScale_354
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[35]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[35]));
//--------------------------------
// --- init: SquareAndScale_355
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[36]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[36]));
//--------------------------------
// --- init: SquareAndScale_356
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[37]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[37]));
//--------------------------------
// --- init: SquareAndScale_357
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[38]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[38]));
//--------------------------------
// --- init: SquareAndScale_358
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[39]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[39]));
//--------------------------------
// --- init: SquareAndScale_359
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[40]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[40]));
//--------------------------------
// --- init: SquareAndScale_360
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[41]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[41]));
//--------------------------------
// --- init: SquareAndScale_361
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[42]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[42]));
//--------------------------------
// --- init: SquareAndScale_362
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[43]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[43]));
//--------------------------------
// --- init: SquareAndScale_363
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[44]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[44]));
//--------------------------------
// --- init: SquareAndScale_364
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[45]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[45]));
//--------------------------------
// --- init: SquareAndScale_365
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[46]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[46]));
//--------------------------------
// --- init: SquareAndScale_366
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[47]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[47]));
//--------------------------------
// --- init: SquareAndScale_367
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[48]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[48]));
//--------------------------------
// --- init: SquareAndScale_368
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[49]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[49]));
//--------------------------------
// --- init: SquareAndScale_369
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[50]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[50]));
//--------------------------------
// --- init: SquareAndScale_370
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[51]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[51]));
//--------------------------------
// --- init: SquareAndScale_371
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[52]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[52]));
//--------------------------------
// --- init: SquareAndScale_372
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[53]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[53]));
//--------------------------------
// --- init: SquareAndScale_373
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[54]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[54]));
//--------------------------------
// --- init: SquareAndScale_374
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[55]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[55]));
//--------------------------------
// --- init: SquareAndScale_375
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[56]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[56]));
//--------------------------------
// --- init: SquareAndScale_376
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[57]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[57]));
//--------------------------------
// --- init: SquareAndScale_377
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[58]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[58]));
//--------------------------------
// --- init: SquareAndScale_378
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[59]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[59]));
//--------------------------------
// --- init: SquareAndScale_379
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[60]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[60]));
//--------------------------------
// --- init: SquareAndScale_380
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_381_383_split[61]), &(SplitJoin0_SquareAndScale_Fiss_381_383_join[61]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_318
	
	FOR(uint32_t, __iter_, 0, <, 62, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_318CFAR_gather_314, pop_float(&SplitJoin0_SquareAndScale_Fiss_381_383_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_314
	 {
	CFAR_gather_314_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 53, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_318CFAR_gather_314), &(CFAR_gather_314AnonFilter_a0_315));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_315
	FOR(uint32_t, __iter_init_, 0, <, 53, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_314AnonFilter_a0_315));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_311();
		WEIGHTED_ROUND_ROBIN_Splitter_317();
			SquareAndScale_319();
			SquareAndScale_320();
			SquareAndScale_321();
			SquareAndScale_322();
			SquareAndScale_323();
			SquareAndScale_324();
			SquareAndScale_325();
			SquareAndScale_326();
			SquareAndScale_327();
			SquareAndScale_328();
			SquareAndScale_329();
			SquareAndScale_330();
			SquareAndScale_331();
			SquareAndScale_332();
			SquareAndScale_333();
			SquareAndScale_334();
			SquareAndScale_335();
			SquareAndScale_336();
			SquareAndScale_337();
			SquareAndScale_338();
			SquareAndScale_339();
			SquareAndScale_340();
			SquareAndScale_341();
			SquareAndScale_342();
			SquareAndScale_343();
			SquareAndScale_344();
			SquareAndScale_345();
			SquareAndScale_346();
			SquareAndScale_347();
			SquareAndScale_348();
			SquareAndScale_349();
			SquareAndScale_350();
			SquareAndScale_351();
			SquareAndScale_352();
			SquareAndScale_353();
			SquareAndScale_354();
			SquareAndScale_355();
			SquareAndScale_356();
			SquareAndScale_357();
			SquareAndScale_358();
			SquareAndScale_359();
			SquareAndScale_360();
			SquareAndScale_361();
			SquareAndScale_362();
			SquareAndScale_363();
			SquareAndScale_364();
			SquareAndScale_365();
			SquareAndScale_366();
			SquareAndScale_367();
			SquareAndScale_368();
			SquareAndScale_369();
			SquareAndScale_370();
			SquareAndScale_371();
			SquareAndScale_372();
			SquareAndScale_373();
			SquareAndScale_374();
			SquareAndScale_375();
			SquareAndScale_376();
			SquareAndScale_377();
			SquareAndScale_378();
			SquareAndScale_379();
			SquareAndScale_380();
		WEIGHTED_ROUND_ROBIN_Joiner_318();
		CFAR_gather_314();
		AnonFilter_a0_315();
	ENDFOR
	return EXIT_SUCCESS;
}
