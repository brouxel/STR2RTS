#include "PEG10-CFARtest_nocache.h"

buffer_float_t CFAR_gather_4734AnonFilter_a0_4735;
buffer_complex_t ComplexSource_4731WEIGHTED_ROUND_ROBIN_Splitter_4737;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4749_4751_join[10];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4738CFAR_gather_4734;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4749_4751_split[10];


ComplexSource_4731_t ComplexSource_4731_s;
CFAR_gather_4734_t CFAR_gather_4734_s;

void ComplexSource_4731(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4731_s.theta = (ComplexSource_4731_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4731_s.theta)) * (((float) cos(ComplexSource_4731_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4731_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4731_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4731_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4731_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4731_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4731_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_4731WEIGHTED_ROUND_ROBIN_Splitter_4737, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_4739(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4740(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4741(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4742(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4743(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4744(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4745(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4746(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4747(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4748(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4737() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[__iter_], pop_complex(&ComplexSource_4731WEIGHTED_ROUND_ROBIN_Splitter_4737));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4738() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4738CFAR_gather_4734, pop_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4734(){
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4734_s.pos) - 5) >= 0)), __DEFLOOPBOUND__332__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4734_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4734_s.pos) < 64)), __DEFLOOPBOUND__333__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4738CFAR_gather_4734, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4734AnonFilter_a0_4735, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4734_s.poke[(i - 1)] = CFAR_gather_4734_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4734_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4738CFAR_gather_4734) ; 
		CFAR_gather_4734_s.pos++ ; 
		if(CFAR_gather_4734_s.pos == 64) {
			CFAR_gather_4734_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4735(){
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4734AnonFilter_a0_4735));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&CFAR_gather_4734AnonFilter_a0_4735);
	init_buffer_complex(&ComplexSource_4731WEIGHTED_ROUND_ROBIN_Splitter_4737);
	FOR(int, __iter_init_0_, 0, <, 10, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4738CFAR_gather_4734);
	FOR(int, __iter_init_1_, 0, <, 10, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4731
	 {
	ComplexSource_4731_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4731_s.theta = (ComplexSource_4731_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4731_s.theta)) * (((float) cos(ComplexSource_4731_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4731_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4731_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4731_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4731_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4731_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4731_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4731WEIGHTED_ROUND_ROBIN_Splitter_4737, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4737
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[__iter_], pop_complex(&ComplexSource_4731WEIGHTED_ROUND_ROBIN_Splitter_4737));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4739
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4740
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4741
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4742
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4743
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4744
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4745
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4746
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4747
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4748
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4749_4751_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4738
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4738CFAR_gather_4734, pop_float(&SplitJoin0_SquareAndScale_Fiss_4749_4751_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4734
	 {
	CFAR_gather_4734_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4734_s.pos) - 5) >= 0)), __DEFLOOPBOUND__334__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4734_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4734_s.pos) < 64)), __DEFLOOPBOUND__335__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4738CFAR_gather_4734, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4734AnonFilter_a0_4735, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4734_s.poke[(i - 1)] = CFAR_gather_4734_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4734_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4738CFAR_gather_4734) ; 
		CFAR_gather_4734_s.pos++ ; 
		if(CFAR_gather_4734_s.pos == 64) {
			CFAR_gather_4734_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4735
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4734AnonFilter_a0_4735));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4731();
		WEIGHTED_ROUND_ROBIN_Splitter_4737();
			SquareAndScale_4739();
			SquareAndScale_4740();
			SquareAndScale_4741();
			SquareAndScale_4742();
			SquareAndScale_4743();
			SquareAndScale_4744();
			SquareAndScale_4745();
			SquareAndScale_4746();
			SquareAndScale_4747();
			SquareAndScale_4748();
		WEIGHTED_ROUND_ROBIN_Joiner_4738();
		CFAR_gather_4734();
		AnonFilter_a0_4735();
	ENDFOR
	return EXIT_SUCCESS;
}
