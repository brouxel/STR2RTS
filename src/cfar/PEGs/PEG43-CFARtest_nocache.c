#include "PEG43-CFARtest_nocache.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_2604_2606_join[43];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556;
buffer_float_t CFAR_gather_2556AnonFilter_a0_2557;
buffer_complex_t ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2604_2606_split[43];


ComplexSource_2553_t ComplexSource_2553_s;
CFAR_gather_2556_t CFAR_gather_2556_s;

void ComplexSource_2553(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2553_s.theta = (ComplexSource_2553_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2553_s.theta)) * (((float) cos(ComplexSource_2553_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2553_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2553_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2553_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2553_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2553_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2553_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_2561(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2562(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2563(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2564(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2565(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2566(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2567(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2568(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2569(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2570(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2571(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2572(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2573(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2574(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2575(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2576(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2577(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2578(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2579(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2580(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2581(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2582(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[21]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[21], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2583(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[22]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[22], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2584(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[23]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[23], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2585(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[24]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[24], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2586(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[25]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[25], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2587(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[26]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[26], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2588(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[27]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[27], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2589(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[28]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[28], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2590(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[29]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[29], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2591(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[30]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[30], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2592(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[31]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[31], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2593(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[32]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[32], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2594(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[33]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[33], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2595(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[34]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[34], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2596(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[35]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[35], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2597(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[36]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[36], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2598(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[37]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[37], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2599(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[38]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[38], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2600(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[39]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[39], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2601(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[40]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[40], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2602(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[41]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[41], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_2603(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[42]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[42], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2559() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[__iter_], pop_complex(&ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2560() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556, pop_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_2556(){
	FOR(uint32_t, __iter_steady_, 0, <, 2752, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2556_s.pos) - 5) >= 0)), __DEFLOOPBOUND__134__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2556_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2556_s.pos) < 64)), __DEFLOOPBOUND__135__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_2556AnonFilter_a0_2557, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2556_s.poke[(i - 1)] = CFAR_gather_2556_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2556_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556) ; 
		CFAR_gather_2556_s.pos++ ; 
		if(CFAR_gather_2556_s.pos == 64) {
			CFAR_gather_2556_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_2557(){
	FOR(uint32_t, __iter_steady_, 0, <, 2752, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_2556AnonFilter_a0_2557));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 43, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556);
	init_buffer_float(&CFAR_gather_2556AnonFilter_a0_2557);
	init_buffer_complex(&ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559);
	FOR(int, __iter_init_1_, 0, <, 43, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_2553
	 {
	ComplexSource_2553_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_2553_s.theta = (ComplexSource_2553_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_2553_s.theta)) * (((float) cos(ComplexSource_2553_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2553_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2553_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_2553_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2553_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2553_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2553_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2559
	
	FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[__iter_], pop_complex(&ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2561
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[0]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[0], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2562
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[1]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[1], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2563
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[2]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[2], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2564
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[3]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[3], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2565
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[4]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[4], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2566
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[5]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[5], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2567
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[6]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[6], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2568
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[7]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[7], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2569
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[8]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[8], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2570
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[9]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[9], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2571
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[10]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[10], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2572
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[11]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[11], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2573
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[12]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[12], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2574
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[13]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[13], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2575
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[14]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[14], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2576
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[15]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[15], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2577
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[16]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[16], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2578
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[17]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[17], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2579
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[18]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[18], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2580
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[19]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[19], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2581
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[20]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[20], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2582
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[21]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[21], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2583
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[22]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[22], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2584
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[23]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[23], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2585
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[24]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[24], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2586
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[25]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[25], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2587
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[26]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[26], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2588
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[27]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[27], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2589
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[28]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[28], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2590
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[29]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[29], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2591
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[30]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[30], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2592
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[31]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[31], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2593
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[32]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[32], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2594
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[33]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[33], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2595
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[34]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[34], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2596
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[35]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[35], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2597
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[36]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[36], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2598
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[37]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[37], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2599
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[38]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[38], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2600
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[39]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[39], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2601
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[40]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[40], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2602
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[41]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[41], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_2603
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[42]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[42], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2560
	
	FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556, pop_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2556
	 {
	CFAR_gather_2556_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2556_s.pos) - 5) >= 0)), __DEFLOOPBOUND__136__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2556_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2556_s.pos) < 64)), __DEFLOOPBOUND__137__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_2556AnonFilter_a0_2557, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2556_s.poke[(i - 1)] = CFAR_gather_2556_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2556_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556) ; 
		CFAR_gather_2556_s.pos++ ; 
		if(CFAR_gather_2556_s.pos == 64) {
			CFAR_gather_2556_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2557
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_2556AnonFilter_a0_2557));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2553();
		WEIGHTED_ROUND_ROBIN_Splitter_2559();
			SquareAndScale_2561();
			SquareAndScale_2562();
			SquareAndScale_2563();
			SquareAndScale_2564();
			SquareAndScale_2565();
			SquareAndScale_2566();
			SquareAndScale_2567();
			SquareAndScale_2568();
			SquareAndScale_2569();
			SquareAndScale_2570();
			SquareAndScale_2571();
			SquareAndScale_2572();
			SquareAndScale_2573();
			SquareAndScale_2574();
			SquareAndScale_2575();
			SquareAndScale_2576();
			SquareAndScale_2577();
			SquareAndScale_2578();
			SquareAndScale_2579();
			SquareAndScale_2580();
			SquareAndScale_2581();
			SquareAndScale_2582();
			SquareAndScale_2583();
			SquareAndScale_2584();
			SquareAndScale_2585();
			SquareAndScale_2586();
			SquareAndScale_2587();
			SquareAndScale_2588();
			SquareAndScale_2589();
			SquareAndScale_2590();
			SquareAndScale_2591();
			SquareAndScale_2592();
			SquareAndScale_2593();
			SquareAndScale_2594();
			SquareAndScale_2595();
			SquareAndScale_2596();
			SquareAndScale_2597();
			SquareAndScale_2598();
			SquareAndScale_2599();
			SquareAndScale_2600();
			SquareAndScale_2601();
			SquareAndScale_2602();
			SquareAndScale_2603();
		WEIGHTED_ROUND_ROBIN_Joiner_2560();
		CFAR_gather_2556();
		AnonFilter_a0_2557();
	ENDFOR
	return EXIT_SUCCESS;
}
