#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2048 on the compile command line
#else
#if BUF_SIZEMAX < 2048
#error BUF_SIZEMAX too small, it must be at least 2048
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_311_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_314_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_311();
void WEIGHTED_ROUND_ROBIN_Splitter_317();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_319();
void SquareAndScale_320();
void SquareAndScale_321();
void SquareAndScale_322();
void SquareAndScale_323();
void SquareAndScale_324();
void SquareAndScale_325();
void SquareAndScale_326();
void SquareAndScale_327();
void SquareAndScale_328();
void SquareAndScale_329();
void SquareAndScale_330();
void SquareAndScale_331();
void SquareAndScale_332();
void SquareAndScale_333();
void SquareAndScale_334();
void SquareAndScale_335();
void SquareAndScale_336();
void SquareAndScale_337();
void SquareAndScale_338();
void SquareAndScale_339();
void SquareAndScale_340();
void SquareAndScale_341();
void SquareAndScale_342();
void SquareAndScale_343();
void SquareAndScale_344();
void SquareAndScale_345();
void SquareAndScale_346();
void SquareAndScale_347();
void SquareAndScale_348();
void SquareAndScale_349();
void SquareAndScale_350();
void SquareAndScale_351();
void SquareAndScale_352();
void SquareAndScale_353();
void SquareAndScale_354();
void SquareAndScale_355();
void SquareAndScale_356();
void SquareAndScale_357();
void SquareAndScale_358();
void SquareAndScale_359();
void SquareAndScale_360();
void SquareAndScale_361();
void SquareAndScale_362();
void SquareAndScale_363();
void SquareAndScale_364();
void SquareAndScale_365();
void SquareAndScale_366();
void SquareAndScale_367();
void SquareAndScale_368();
void SquareAndScale_369();
void SquareAndScale_370();
void SquareAndScale_371();
void SquareAndScale_372();
void SquareAndScale_373();
void SquareAndScale_374();
void SquareAndScale_375();
void SquareAndScale_376();
void SquareAndScale_377();
void SquareAndScale_378();
void SquareAndScale_379();
void SquareAndScale_380();
void WEIGHTED_ROUND_ROBIN_Joiner_318();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_314();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_315();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1


#ifdef __cplusplus
}
#endif
#endif
