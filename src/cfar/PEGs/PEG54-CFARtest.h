#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1792 on the compile command line
#else
#if BUF_SIZEMAX < 1792
#error BUF_SIZEMAX too small, it must be at least 1792
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_1343_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_1346_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_1343();
void WEIGHTED_ROUND_ROBIN_Splitter_1349();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_1351();
void SquareAndScale_1352();
void SquareAndScale_1353();
void SquareAndScale_1354();
void SquareAndScale_1355();
void SquareAndScale_1356();
void SquareAndScale_1357();
void SquareAndScale_1358();
void SquareAndScale_1359();
void SquareAndScale_1360();
void SquareAndScale_1361();
void SquareAndScale_1362();
void SquareAndScale_1363();
void SquareAndScale_1364();
void SquareAndScale_1365();
void SquareAndScale_1366();
void SquareAndScale_1367();
void SquareAndScale_1368();
void SquareAndScale_1369();
void SquareAndScale_1370();
void SquareAndScale_1371();
void SquareAndScale_1372();
void SquareAndScale_1373();
void SquareAndScale_1374();
void SquareAndScale_1375();
void SquareAndScale_1376();
void SquareAndScale_1377();
void SquareAndScale_1378();
void SquareAndScale_1379();
void SquareAndScale_1380();
void SquareAndScale_1381();
void SquareAndScale_1382();
void SquareAndScale_1383();
void SquareAndScale_1384();
void SquareAndScale_1385();
void SquareAndScale_1386();
void SquareAndScale_1387();
void SquareAndScale_1388();
void SquareAndScale_1389();
void SquareAndScale_1390();
void SquareAndScale_1391();
void SquareAndScale_1392();
void SquareAndScale_1393();
void SquareAndScale_1394();
void SquareAndScale_1395();
void SquareAndScale_1396();
void SquareAndScale_1397();
void SquareAndScale_1398();
void SquareAndScale_1399();
void SquareAndScale_1400();
void SquareAndScale_1401();
void SquareAndScale_1402();
void SquareAndScale_1403();
void SquareAndScale_1404();
void WEIGHTED_ROUND_ROBIN_Joiner_1350();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_1346();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_1347();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1


#ifdef __cplusplus
}
#endif
#endif
