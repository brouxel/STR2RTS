#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=512 on the compile command line
#else
#if BUF_SIZEMAX < 512
#error BUF_SIZEMAX too small, it must be at least 512
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_1097_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_1100_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_1097();
void WEIGHTED_ROUND_ROBIN_Splitter_1103();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_1105();
void SquareAndScale_1106();
void SquareAndScale_1107();
void SquareAndScale_1108();
void SquareAndScale_1109();
void SquareAndScale_1110();
void SquareAndScale_1111();
void SquareAndScale_1112();
void SquareAndScale_1113();
void SquareAndScale_1114();
void SquareAndScale_1115();
void SquareAndScale_1116();
void SquareAndScale_1117();
void SquareAndScale_1118();
void SquareAndScale_1119();
void SquareAndScale_1120();
void SquareAndScale_1121();
void SquareAndScale_1122();
void SquareAndScale_1123();
void SquareAndScale_1124();
void SquareAndScale_1125();
void SquareAndScale_1126();
void SquareAndScale_1127();
void SquareAndScale_1128();
void SquareAndScale_1129();
void SquareAndScale_1130();
void SquareAndScale_1131();
void SquareAndScale_1132();
void SquareAndScale_1133();
void SquareAndScale_1134();
void SquareAndScale_1135();
void SquareAndScale_1136();
void SquareAndScale_1137();
void SquareAndScale_1138();
void SquareAndScale_1139();
void SquareAndScale_1140();
void SquareAndScale_1141();
void SquareAndScale_1142();
void SquareAndScale_1143();
void SquareAndScale_1144();
void SquareAndScale_1145();
void SquareAndScale_1146();
void SquareAndScale_1147();
void SquareAndScale_1148();
void SquareAndScale_1149();
void SquareAndScale_1150();
void SquareAndScale_1151();
void SquareAndScale_1152();
void SquareAndScale_1153();
void SquareAndScale_1154();
void SquareAndScale_1155();
void SquareAndScale_1156();
void SquareAndScale_1157();
void SquareAndScale_1158();
void SquareAndScale_1159();
void SquareAndScale_1160();
void WEIGHTED_ROUND_ROBIN_Joiner_1104();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_1100();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_1101();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1


#ifdef __cplusplus
}
#endif
#endif
