#include "PEG46-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2254CFAR_gather_2250;
buffer_complex_t ComplexSource_2247WEIGHTED_ROUND_ROBIN_Splitter_2253;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2301_2303_split[46];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_2301_2303_join[46];
buffer_float_t CFAR_gather_2250AnonFilter_a0_2251;


ComplexSource_2247_t ComplexSource_2247_s;
CFAR_gather_2250_t CFAR_gather_2250_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2247_s.theta = (ComplexSource_2247_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2247_s.theta)) * (((float) cos(ComplexSource_2247_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2247_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2247_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2247_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2247_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2247_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2247_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2247() {
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		ComplexSource(&(ComplexSource_2247WEIGHTED_ROUND_ROBIN_Splitter_2253));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2255() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[0]));
	ENDFOR
}

void SquareAndScale_2256() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[1]));
	ENDFOR
}

void SquareAndScale_2257() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[2]));
	ENDFOR
}

void SquareAndScale_2258() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[3]));
	ENDFOR
}

void SquareAndScale_2259() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[4]));
	ENDFOR
}

void SquareAndScale_2260() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[5]));
	ENDFOR
}

void SquareAndScale_2261() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[6]));
	ENDFOR
}

void SquareAndScale_2262() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[7]));
	ENDFOR
}

void SquareAndScale_2263() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[8]));
	ENDFOR
}

void SquareAndScale_2264() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[9]));
	ENDFOR
}

void SquareAndScale_2265() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[10]));
	ENDFOR
}

void SquareAndScale_2266() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[11]));
	ENDFOR
}

void SquareAndScale_2267() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[12]));
	ENDFOR
}

void SquareAndScale_2268() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[13]));
	ENDFOR
}

void SquareAndScale_2269() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[14]));
	ENDFOR
}

void SquareAndScale_2270() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[15]));
	ENDFOR
}

void SquareAndScale_2271() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[16]));
	ENDFOR
}

void SquareAndScale_2272() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[17]));
	ENDFOR
}

void SquareAndScale_2273() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[18]));
	ENDFOR
}

void SquareAndScale_2274() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[19]));
	ENDFOR
}

void SquareAndScale_2275() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[20]));
	ENDFOR
}

void SquareAndScale_2276() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[21]));
	ENDFOR
}

void SquareAndScale_2277() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[22]));
	ENDFOR
}

void SquareAndScale_2278() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[23]));
	ENDFOR
}

void SquareAndScale_2279() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[24]));
	ENDFOR
}

void SquareAndScale_2280() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[25]));
	ENDFOR
}

void SquareAndScale_2281() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[26]));
	ENDFOR
}

void SquareAndScale_2282() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[27]));
	ENDFOR
}

void SquareAndScale_2283() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[28]));
	ENDFOR
}

void SquareAndScale_2284() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[29]));
	ENDFOR
}

void SquareAndScale_2285() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[30]));
	ENDFOR
}

void SquareAndScale_2286() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[31]));
	ENDFOR
}

void SquareAndScale_2287() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[32]));
	ENDFOR
}

void SquareAndScale_2288() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[33]));
	ENDFOR
}

void SquareAndScale_2289() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[34]));
	ENDFOR
}

void SquareAndScale_2290() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[35]));
	ENDFOR
}

void SquareAndScale_2291() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[36]));
	ENDFOR
}

void SquareAndScale_2292() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[37]));
	ENDFOR
}

void SquareAndScale_2293() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[38]));
	ENDFOR
}

void SquareAndScale_2294() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[39]));
	ENDFOR
}

void SquareAndScale_2295() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[40]));
	ENDFOR
}

void SquareAndScale_2296() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[41]));
	ENDFOR
}

void SquareAndScale_2297() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[42]));
	ENDFOR
}

void SquareAndScale_2298() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[43]));
	ENDFOR
}

void SquareAndScale_2299() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[44]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[44]));
	ENDFOR
}

void SquareAndScale_2300() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[45]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[45]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2253() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 46, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2301_2303_split[__iter_], pop_complex(&ComplexSource_2247WEIGHTED_ROUND_ROBIN_Splitter_2253));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2254() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 46, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2254CFAR_gather_2250, pop_float(&SplitJoin0_SquareAndScale_Fiss_2301_2303_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2250_s.pos) - 5) >= 0)), __DEFLOOPBOUND__116__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2250_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2250_s.pos) < 64)), __DEFLOOPBOUND__117__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2250_s.poke[(i - 1)] = CFAR_gather_2250_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2250_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2250_s.pos++ ; 
		if(CFAR_gather_2250_s.pos == 64) {
			CFAR_gather_2250_s.pos = 0 ; 
		}
	}


void CFAR_gather_2250() {
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2254CFAR_gather_2250), &(CFAR_gather_2250AnonFilter_a0_2251));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2251() {
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2250AnonFilter_a0_2251));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2254CFAR_gather_2250);
	init_buffer_complex(&ComplexSource_2247WEIGHTED_ROUND_ROBIN_Splitter_2253);
	FOR(int, __iter_init_0_, 0, <, 46, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2301_2303_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 46, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2301_2303_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_2250AnonFilter_a0_2251);
// --- init: ComplexSource_2247
	 {
	ComplexSource_2247_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2247WEIGHTED_ROUND_ROBIN_Splitter_2253));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2253
	
	FOR(uint32_t, __iter_, 0, <, 46, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2301_2303_split[__iter_], pop_complex(&ComplexSource_2247WEIGHTED_ROUND_ROBIN_Splitter_2253));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2255
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[0]));
//--------------------------------
// --- init: SquareAndScale_2256
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[1]));
//--------------------------------
// --- init: SquareAndScale_2257
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[2]));
//--------------------------------
// --- init: SquareAndScale_2258
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[3]));
//--------------------------------
// --- init: SquareAndScale_2259
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[4]));
//--------------------------------
// --- init: SquareAndScale_2260
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[5]));
//--------------------------------
// --- init: SquareAndScale_2261
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[6]));
//--------------------------------
// --- init: SquareAndScale_2262
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[7]));
//--------------------------------
// --- init: SquareAndScale_2263
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[8]));
//--------------------------------
// --- init: SquareAndScale_2264
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[9]));
//--------------------------------
// --- init: SquareAndScale_2265
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[10]));
//--------------------------------
// --- init: SquareAndScale_2266
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[11]));
//--------------------------------
// --- init: SquareAndScale_2267
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[12]));
//--------------------------------
// --- init: SquareAndScale_2268
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[13]));
//--------------------------------
// --- init: SquareAndScale_2269
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[14]));
//--------------------------------
// --- init: SquareAndScale_2270
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[15]));
//--------------------------------
// --- init: SquareAndScale_2271
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[16]));
//--------------------------------
// --- init: SquareAndScale_2272
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[17]));
//--------------------------------
// --- init: SquareAndScale_2273
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[18]));
//--------------------------------
// --- init: SquareAndScale_2274
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[19]));
//--------------------------------
// --- init: SquareAndScale_2275
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[20]));
//--------------------------------
// --- init: SquareAndScale_2276
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[21]));
//--------------------------------
// --- init: SquareAndScale_2277
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[22]));
//--------------------------------
// --- init: SquareAndScale_2278
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[23]));
//--------------------------------
// --- init: SquareAndScale_2279
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[24]));
//--------------------------------
// --- init: SquareAndScale_2280
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[25]));
//--------------------------------
// --- init: SquareAndScale_2281
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[26]));
//--------------------------------
// --- init: SquareAndScale_2282
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[27]));
//--------------------------------
// --- init: SquareAndScale_2283
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[28]));
//--------------------------------
// --- init: SquareAndScale_2284
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[29]));
//--------------------------------
// --- init: SquareAndScale_2285
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[30]));
//--------------------------------
// --- init: SquareAndScale_2286
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[31]));
//--------------------------------
// --- init: SquareAndScale_2287
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[32]));
//--------------------------------
// --- init: SquareAndScale_2288
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[33]));
//--------------------------------
// --- init: SquareAndScale_2289
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[34]));
//--------------------------------
// --- init: SquareAndScale_2290
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[35]));
//--------------------------------
// --- init: SquareAndScale_2291
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[36]));
//--------------------------------
// --- init: SquareAndScale_2292
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[37]));
//--------------------------------
// --- init: SquareAndScale_2293
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[38]));
//--------------------------------
// --- init: SquareAndScale_2294
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[39]));
//--------------------------------
// --- init: SquareAndScale_2295
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[40]));
//--------------------------------
// --- init: SquareAndScale_2296
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[41]));
//--------------------------------
// --- init: SquareAndScale_2297
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[42]));
//--------------------------------
// --- init: SquareAndScale_2298
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[43]));
//--------------------------------
// --- init: SquareAndScale_2299
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[44]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[44]));
//--------------------------------
// --- init: SquareAndScale_2300
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2301_2303_split[45]), &(SplitJoin0_SquareAndScale_Fiss_2301_2303_join[45]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2254
	
	FOR(uint32_t, __iter_, 0, <, 46, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2254CFAR_gather_2250, pop_float(&SplitJoin0_SquareAndScale_Fiss_2301_2303_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2250
	 {
	CFAR_gather_2250_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2254CFAR_gather_2250), &(CFAR_gather_2250AnonFilter_a0_2251));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2251
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2250AnonFilter_a0_2251));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2247();
		WEIGHTED_ROUND_ROBIN_Splitter_2253();
			SquareAndScale_2255();
			SquareAndScale_2256();
			SquareAndScale_2257();
			SquareAndScale_2258();
			SquareAndScale_2259();
			SquareAndScale_2260();
			SquareAndScale_2261();
			SquareAndScale_2262();
			SquareAndScale_2263();
			SquareAndScale_2264();
			SquareAndScale_2265();
			SquareAndScale_2266();
			SquareAndScale_2267();
			SquareAndScale_2268();
			SquareAndScale_2269();
			SquareAndScale_2270();
			SquareAndScale_2271();
			SquareAndScale_2272();
			SquareAndScale_2273();
			SquareAndScale_2274();
			SquareAndScale_2275();
			SquareAndScale_2276();
			SquareAndScale_2277();
			SquareAndScale_2278();
			SquareAndScale_2279();
			SquareAndScale_2280();
			SquareAndScale_2281();
			SquareAndScale_2282();
			SquareAndScale_2283();
			SquareAndScale_2284();
			SquareAndScale_2285();
			SquareAndScale_2286();
			SquareAndScale_2287();
			SquareAndScale_2288();
			SquareAndScale_2289();
			SquareAndScale_2290();
			SquareAndScale_2291();
			SquareAndScale_2292();
			SquareAndScale_2293();
			SquareAndScale_2294();
			SquareAndScale_2295();
			SquareAndScale_2296();
			SquareAndScale_2297();
			SquareAndScale_2298();
			SquareAndScale_2299();
			SquareAndScale_2300();
		WEIGHTED_ROUND_ROBIN_Joiner_2254();
		CFAR_gather_2250();
		AnonFilter_a0_2251();
	ENDFOR
	return EXIT_SUCCESS;
}
