#include "PEG27-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3888CFAR_gather_3884;
buffer_float_t CFAR_gather_3884AnonFilter_a0_3885;
buffer_complex_t ComplexSource_3881WEIGHTED_ROUND_ROBIN_Splitter_3887;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3916_3918_join[27];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3916_3918_split[27];


ComplexSource_3881_t ComplexSource_3881_s;
CFAR_gather_3884_t CFAR_gather_3884_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3881_s.theta = (ComplexSource_3881_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3881_s.theta)) * (((float) cos(ComplexSource_3881_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3881_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3881_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3881_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3881_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3881_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3881_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3881() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		ComplexSource(&(ComplexSource_3881WEIGHTED_ROUND_ROBIN_Splitter_3887));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3889() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[0]));
	ENDFOR
}

void SquareAndScale_3890() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[1]));
	ENDFOR
}

void SquareAndScale_3891() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[2]));
	ENDFOR
}

void SquareAndScale_3892() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[3]));
	ENDFOR
}

void SquareAndScale_3893() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[4]));
	ENDFOR
}

void SquareAndScale_3894() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[5]));
	ENDFOR
}

void SquareAndScale_3895() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[6]));
	ENDFOR
}

void SquareAndScale_3896() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[7]));
	ENDFOR
}

void SquareAndScale_3897() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[8]));
	ENDFOR
}

void SquareAndScale_3898() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[9]));
	ENDFOR
}

void SquareAndScale_3899() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[10]));
	ENDFOR
}

void SquareAndScale_3900() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[11]));
	ENDFOR
}

void SquareAndScale_3901() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[12]));
	ENDFOR
}

void SquareAndScale_3902() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[13]));
	ENDFOR
}

void SquareAndScale_3903() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[14]));
	ENDFOR
}

void SquareAndScale_3904() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[15]));
	ENDFOR
}

void SquareAndScale_3905() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[16]));
	ENDFOR
}

void SquareAndScale_3906() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[17]));
	ENDFOR
}

void SquareAndScale_3907() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[18]));
	ENDFOR
}

void SquareAndScale_3908() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[19]));
	ENDFOR
}

void SquareAndScale_3909() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[20]));
	ENDFOR
}

void SquareAndScale_3910() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[21]));
	ENDFOR
}

void SquareAndScale_3911() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[22]));
	ENDFOR
}

void SquareAndScale_3912() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[23]));
	ENDFOR
}

void SquareAndScale_3913() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[24]));
	ENDFOR
}

void SquareAndScale_3914() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[25]));
	ENDFOR
}

void SquareAndScale_3915() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[26]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3887() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3916_3918_split[__iter_], pop_complex(&ComplexSource_3881WEIGHTED_ROUND_ROBIN_Splitter_3887));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3888() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3888CFAR_gather_3884, pop_float(&SplitJoin0_SquareAndScale_Fiss_3916_3918_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3884_s.pos) - 5) >= 0)), __DEFLOOPBOUND__230__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3884_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3884_s.pos) < 64)), __DEFLOOPBOUND__231__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3884_s.poke[(i - 1)] = CFAR_gather_3884_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3884_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3884_s.pos++ ; 
		if(CFAR_gather_3884_s.pos == 64) {
			CFAR_gather_3884_s.pos = 0 ; 
		}
	}


void CFAR_gather_3884() {
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3888CFAR_gather_3884), &(CFAR_gather_3884AnonFilter_a0_3885));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3885() {
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3884AnonFilter_a0_3885));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3888CFAR_gather_3884);
	init_buffer_float(&CFAR_gather_3884AnonFilter_a0_3885);
	init_buffer_complex(&ComplexSource_3881WEIGHTED_ROUND_ROBIN_Splitter_3887);
	FOR(int, __iter_init_0_, 0, <, 27, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3916_3918_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 27, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3916_3918_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_3881
	 {
	ComplexSource_3881_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3881WEIGHTED_ROUND_ROBIN_Splitter_3887));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3887
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3916_3918_split[__iter_], pop_complex(&ComplexSource_3881WEIGHTED_ROUND_ROBIN_Splitter_3887));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3889
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3890
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3891
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3892
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3893
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3894
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3895
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3896
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3897
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3898
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3899
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3900
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3901
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3902
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3903
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3904
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3905
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3906
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3907
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3908
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3909
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3910
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3911
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[22]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3912
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[23]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3913
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[24]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3914
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[25]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3915
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3916_3918_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3916_3918_join[26]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3888
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3888CFAR_gather_3884, pop_float(&SplitJoin0_SquareAndScale_Fiss_3916_3918_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3884
	 {
	CFAR_gather_3884_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 45, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3888CFAR_gather_3884), &(CFAR_gather_3884AnonFilter_a0_3885));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3885
	FOR(uint32_t, __iter_init_, 0, <, 45, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3884AnonFilter_a0_3885));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3881();
		WEIGHTED_ROUND_ROBIN_Splitter_3887();
			SquareAndScale_3889();
			SquareAndScale_3890();
			SquareAndScale_3891();
			SquareAndScale_3892();
			SquareAndScale_3893();
			SquareAndScale_3894();
			SquareAndScale_3895();
			SquareAndScale_3896();
			SquareAndScale_3897();
			SquareAndScale_3898();
			SquareAndScale_3899();
			SquareAndScale_3900();
			SquareAndScale_3901();
			SquareAndScale_3902();
			SquareAndScale_3903();
			SquareAndScale_3904();
			SquareAndScale_3905();
			SquareAndScale_3906();
			SquareAndScale_3907();
			SquareAndScale_3908();
			SquareAndScale_3909();
			SquareAndScale_3910();
			SquareAndScale_3911();
			SquareAndScale_3912();
			SquareAndScale_3913();
			SquareAndScale_3914();
			SquareAndScale_3915();
		WEIGHTED_ROUND_ROBIN_Joiner_3888();
		CFAR_gather_3884();
		AnonFilter_a0_3885();
	ENDFOR
	return EXIT_SUCCESS;
}
