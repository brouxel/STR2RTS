#include "PEG37-CFARtest.h"

buffer_complex_t ComplexSource_3111WEIGHTED_ROUND_ROBIN_Splitter_3117;
buffer_float_t CFAR_gather_3114AnonFilter_a0_3115;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3156_3158_join[37];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3118CFAR_gather_3114;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3156_3158_split[37];


ComplexSource_3111_t ComplexSource_3111_s;
CFAR_gather_3114_t CFAR_gather_3114_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3111_s.theta = (ComplexSource_3111_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3111_s.theta)) * (((float) cos(ComplexSource_3111_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3111_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3111_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3111_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3111_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3111_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3111_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3111() {
	FOR(uint32_t, __iter_steady_, 0, <, 37, __iter_steady_++)
		ComplexSource(&(ComplexSource_3111WEIGHTED_ROUND_ROBIN_Splitter_3117));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3119() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[0]));
	ENDFOR
}

void SquareAndScale_3120() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[1]));
	ENDFOR
}

void SquareAndScale_3121() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[2]));
	ENDFOR
}

void SquareAndScale_3122() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[3]));
	ENDFOR
}

void SquareAndScale_3123() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[4]));
	ENDFOR
}

void SquareAndScale_3124() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[5]));
	ENDFOR
}

void SquareAndScale_3125() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[6]));
	ENDFOR
}

void SquareAndScale_3126() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[7]));
	ENDFOR
}

void SquareAndScale_3127() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[8]));
	ENDFOR
}

void SquareAndScale_3128() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[9]));
	ENDFOR
}

void SquareAndScale_3129() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[10]));
	ENDFOR
}

void SquareAndScale_3130() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[11]));
	ENDFOR
}

void SquareAndScale_3131() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[12]));
	ENDFOR
}

void SquareAndScale_3132() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[13]));
	ENDFOR
}

void SquareAndScale_3133() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[14]));
	ENDFOR
}

void SquareAndScale_3134() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[15]));
	ENDFOR
}

void SquareAndScale_3135() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[16]));
	ENDFOR
}

void SquareAndScale_3136() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[17]));
	ENDFOR
}

void SquareAndScale_3137() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[18]));
	ENDFOR
}

void SquareAndScale_3138() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[19]));
	ENDFOR
}

void SquareAndScale_3139() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[20]));
	ENDFOR
}

void SquareAndScale_3140() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[21]));
	ENDFOR
}

void SquareAndScale_3141() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[22]));
	ENDFOR
}

void SquareAndScale_3142() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[23]));
	ENDFOR
}

void SquareAndScale_3143() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[24]));
	ENDFOR
}

void SquareAndScale_3144() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[25]));
	ENDFOR
}

void SquareAndScale_3145() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[26]));
	ENDFOR
}

void SquareAndScale_3146() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[27]));
	ENDFOR
}

void SquareAndScale_3147() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[28]));
	ENDFOR
}

void SquareAndScale_3148() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[29]));
	ENDFOR
}

void SquareAndScale_3149() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[30]));
	ENDFOR
}

void SquareAndScale_3150() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[31]));
	ENDFOR
}

void SquareAndScale_3151() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[32]));
	ENDFOR
}

void SquareAndScale_3152() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[33]));
	ENDFOR
}

void SquareAndScale_3153() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[34]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[34]));
	ENDFOR
}

void SquareAndScale_3154() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[35]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[35]));
	ENDFOR
}

void SquareAndScale_3155() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[36]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[36]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3117() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 37, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3156_3158_split[__iter_], pop_complex(&ComplexSource_3111WEIGHTED_ROUND_ROBIN_Splitter_3117));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3118() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 37, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3118CFAR_gather_3114, pop_float(&SplitJoin0_SquareAndScale_Fiss_3156_3158_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3114_s.pos) - 5) >= 0)), __DEFLOOPBOUND__170__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3114_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3114_s.pos) < 64)), __DEFLOOPBOUND__171__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3114_s.poke[(i - 1)] = CFAR_gather_3114_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3114_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3114_s.pos++ ; 
		if(CFAR_gather_3114_s.pos == 64) {
			CFAR_gather_3114_s.pos = 0 ; 
		}
	}


void CFAR_gather_3114() {
	FOR(uint32_t, __iter_steady_, 0, <, 2368, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3118CFAR_gather_3114), &(CFAR_gather_3114AnonFilter_a0_3115));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3115() {
	FOR(uint32_t, __iter_steady_, 0, <, 2368, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3114AnonFilter_a0_3115));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_3111WEIGHTED_ROUND_ROBIN_Splitter_3117);
	init_buffer_float(&CFAR_gather_3114AnonFilter_a0_3115);
	FOR(int, __iter_init_0_, 0, <, 37, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3156_3158_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3118CFAR_gather_3114);
	FOR(int, __iter_init_1_, 0, <, 37, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3156_3158_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_3111
	 {
	ComplexSource_3111_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3111WEIGHTED_ROUND_ROBIN_Splitter_3117));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3117
	
	FOR(uint32_t, __iter_, 0, <, 37, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_3156_3158_split[__iter_], pop_complex(&ComplexSource_3111WEIGHTED_ROUND_ROBIN_Splitter_3117));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3119
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[0]));
//--------------------------------
// --- init: SquareAndScale_3120
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[1]));
//--------------------------------
// --- init: SquareAndScale_3121
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[2]));
//--------------------------------
// --- init: SquareAndScale_3122
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[3]));
//--------------------------------
// --- init: SquareAndScale_3123
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[4]));
//--------------------------------
// --- init: SquareAndScale_3124
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[5]));
//--------------------------------
// --- init: SquareAndScale_3125
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[6]));
//--------------------------------
// --- init: SquareAndScale_3126
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[7]));
//--------------------------------
// --- init: SquareAndScale_3127
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[8]));
//--------------------------------
// --- init: SquareAndScale_3128
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[9]));
//--------------------------------
// --- init: SquareAndScale_3129
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[10]));
//--------------------------------
// --- init: SquareAndScale_3130
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[11]));
//--------------------------------
// --- init: SquareAndScale_3131
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[12]));
//--------------------------------
// --- init: SquareAndScale_3132
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[13]));
//--------------------------------
// --- init: SquareAndScale_3133
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[14]));
//--------------------------------
// --- init: SquareAndScale_3134
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[15]));
//--------------------------------
// --- init: SquareAndScale_3135
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[16]));
//--------------------------------
// --- init: SquareAndScale_3136
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[17]));
//--------------------------------
// --- init: SquareAndScale_3137
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[18]));
//--------------------------------
// --- init: SquareAndScale_3138
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[19]));
//--------------------------------
// --- init: SquareAndScale_3139
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[20]));
//--------------------------------
// --- init: SquareAndScale_3140
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[21]));
//--------------------------------
// --- init: SquareAndScale_3141
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[22]));
//--------------------------------
// --- init: SquareAndScale_3142
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[23]));
//--------------------------------
// --- init: SquareAndScale_3143
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[24]));
//--------------------------------
// --- init: SquareAndScale_3144
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[25]));
//--------------------------------
// --- init: SquareAndScale_3145
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[26]));
//--------------------------------
// --- init: SquareAndScale_3146
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[27]));
//--------------------------------
// --- init: SquareAndScale_3147
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[28]));
//--------------------------------
// --- init: SquareAndScale_3148
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[29]));
//--------------------------------
// --- init: SquareAndScale_3149
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[30]));
//--------------------------------
// --- init: SquareAndScale_3150
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[31]));
//--------------------------------
// --- init: SquareAndScale_3151
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[32]));
//--------------------------------
// --- init: SquareAndScale_3152
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[33]));
//--------------------------------
// --- init: SquareAndScale_3153
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[34]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[34]));
//--------------------------------
// --- init: SquareAndScale_3154
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[35]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[35]));
//--------------------------------
// --- init: SquareAndScale_3155
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3156_3158_split[36]), &(SplitJoin0_SquareAndScale_Fiss_3156_3158_join[36]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3118
	
	FOR(uint32_t, __iter_, 0, <, 37, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3118CFAR_gather_3114, pop_float(&SplitJoin0_SquareAndScale_Fiss_3156_3158_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3114
	 {
	CFAR_gather_3114_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 28, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3118CFAR_gather_3114), &(CFAR_gather_3114AnonFilter_a0_3115));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3115
	FOR(uint32_t, __iter_init_, 0, <, 28, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3114AnonFilter_a0_3115));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3111();
		WEIGHTED_ROUND_ROBIN_Splitter_3117();
			SquareAndScale_3119();
			SquareAndScale_3120();
			SquareAndScale_3121();
			SquareAndScale_3122();
			SquareAndScale_3123();
			SquareAndScale_3124();
			SquareAndScale_3125();
			SquareAndScale_3126();
			SquareAndScale_3127();
			SquareAndScale_3128();
			SquareAndScale_3129();
			SquareAndScale_3130();
			SquareAndScale_3131();
			SquareAndScale_3132();
			SquareAndScale_3133();
			SquareAndScale_3134();
			SquareAndScale_3135();
			SquareAndScale_3136();
			SquareAndScale_3137();
			SquareAndScale_3138();
			SquareAndScale_3139();
			SquareAndScale_3140();
			SquareAndScale_3141();
			SquareAndScale_3142();
			SquareAndScale_3143();
			SquareAndScale_3144();
			SquareAndScale_3145();
			SquareAndScale_3146();
			SquareAndScale_3147();
			SquareAndScale_3148();
			SquareAndScale_3149();
			SquareAndScale_3150();
			SquareAndScale_3151();
			SquareAndScale_3152();
			SquareAndScale_3153();
			SquareAndScale_3154();
			SquareAndScale_3155();
		WEIGHTED_ROUND_ROBIN_Joiner_3118();
		CFAR_gather_3114();
		AnonFilter_a0_3115();
	ENDFOR
	return EXIT_SUCCESS;
}
