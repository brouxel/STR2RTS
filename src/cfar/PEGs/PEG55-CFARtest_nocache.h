#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=39226 on the compile command line
#else
#if BUF_SIZEMAX < 39226
#error BUF_SIZEMAX too small, it must be at least 39226
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_1221_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_1224_t;
void ComplexSource_1221();
void WEIGHTED_ROUND_ROBIN_Splitter_1227();
void SquareAndScale_1229();
void SquareAndScale_1230();
void SquareAndScale_1231();
void SquareAndScale_1232();
void SquareAndScale_1233();
void SquareAndScale_1234();
void SquareAndScale_1235();
void SquareAndScale_1236();
void SquareAndScale_1237();
void SquareAndScale_1238();
void SquareAndScale_1239();
void SquareAndScale_1240();
void SquareAndScale_1241();
void SquareAndScale_1242();
void SquareAndScale_1243();
void SquareAndScale_1244();
void SquareAndScale_1245();
void SquareAndScale_1246();
void SquareAndScale_1247();
void SquareAndScale_1248();
void SquareAndScale_1249();
void SquareAndScale_1250();
void SquareAndScale_1251();
void SquareAndScale_1252();
void SquareAndScale_1253();
void SquareAndScale_1254();
void SquareAndScale_1255();
void SquareAndScale_1256();
void SquareAndScale_1257();
void SquareAndScale_1258();
void SquareAndScale_1259();
void SquareAndScale_1260();
void SquareAndScale_1261();
void SquareAndScale_1262();
void SquareAndScale_1263();
void SquareAndScale_1264();
void SquareAndScale_1265();
void SquareAndScale_1266();
void SquareAndScale_1267();
void SquareAndScale_1268();
void SquareAndScale_1269();
void SquareAndScale_1270();
void SquareAndScale_1271();
void SquareAndScale_1272();
void SquareAndScale_1273();
void SquareAndScale_1274();
void SquareAndScale_1275();
void SquareAndScale_1276();
void SquareAndScale_1277();
void SquareAndScale_1278();
void SquareAndScale_1279();
void SquareAndScale_1280();
void SquareAndScale_1281();
void SquareAndScale_1282();
void SquareAndScale_1283();
void WEIGHTED_ROUND_ROBIN_Joiner_1228();
void CFAR_gather_1224();
void AnonFilter_a0_1225();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1


#ifdef __cplusplus
}
#endif
#endif
