#include "PEG50-CFARtest.h"

buffer_complex_t ComplexSource_1811WEIGHTED_ROUND_ROBIN_Splitter_1817;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1818CFAR_gather_1814;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1869_1871_split[50];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_1869_1871_join[50];
buffer_float_t CFAR_gather_1814AnonFilter_a0_1815;


ComplexSource_1811_t ComplexSource_1811_s;
CFAR_gather_1814_t CFAR_gather_1814_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_1811_s.theta = (ComplexSource_1811_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_1811_s.theta)) * (((float) cos(ComplexSource_1811_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1811_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1811_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_1811_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1811_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1811_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1811_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_1811() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		ComplexSource(&(ComplexSource_1811WEIGHTED_ROUND_ROBIN_Splitter_1817));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_1819() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[0]));
	ENDFOR
}

void SquareAndScale_1820() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[1]));
	ENDFOR
}

void SquareAndScale_1821() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[2]));
	ENDFOR
}

void SquareAndScale_1822() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[3]));
	ENDFOR
}

void SquareAndScale_1823() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[4]));
	ENDFOR
}

void SquareAndScale_1824() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[5]));
	ENDFOR
}

void SquareAndScale_1825() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[6]));
	ENDFOR
}

void SquareAndScale_1826() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[7]));
	ENDFOR
}

void SquareAndScale_1827() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[8]));
	ENDFOR
}

void SquareAndScale_1828() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[9]));
	ENDFOR
}

void SquareAndScale_1829() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[10]));
	ENDFOR
}

void SquareAndScale_1830() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[11]));
	ENDFOR
}

void SquareAndScale_1831() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[12]));
	ENDFOR
}

void SquareAndScale_1832() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[13]));
	ENDFOR
}

void SquareAndScale_1833() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[14]));
	ENDFOR
}

void SquareAndScale_1834() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[15]));
	ENDFOR
}

void SquareAndScale_1835() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[16]));
	ENDFOR
}

void SquareAndScale_1836() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[17]));
	ENDFOR
}

void SquareAndScale_1837() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[18]));
	ENDFOR
}

void SquareAndScale_1838() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[19]));
	ENDFOR
}

void SquareAndScale_1839() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[20]));
	ENDFOR
}

void SquareAndScale_1840() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[21]));
	ENDFOR
}

void SquareAndScale_1841() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[22]));
	ENDFOR
}

void SquareAndScale_1842() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[23]));
	ENDFOR
}

void SquareAndScale_1843() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[24]));
	ENDFOR
}

void SquareAndScale_1844() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[25]));
	ENDFOR
}

void SquareAndScale_1845() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[26]));
	ENDFOR
}

void SquareAndScale_1846() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[27]));
	ENDFOR
}

void SquareAndScale_1847() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[28]));
	ENDFOR
}

void SquareAndScale_1848() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[29]));
	ENDFOR
}

void SquareAndScale_1849() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[30]));
	ENDFOR
}

void SquareAndScale_1850() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[31]));
	ENDFOR
}

void SquareAndScale_1851() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[32]));
	ENDFOR
}

void SquareAndScale_1852() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[33]));
	ENDFOR
}

void SquareAndScale_1853() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[34]));
	ENDFOR
}

void SquareAndScale_1854() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[35]));
	ENDFOR
}

void SquareAndScale_1855() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[36]));
	ENDFOR
}

void SquareAndScale_1856() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[37]));
	ENDFOR
}

void SquareAndScale_1857() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[38]));
	ENDFOR
}

void SquareAndScale_1858() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[39]));
	ENDFOR
}

void SquareAndScale_1859() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[40]));
	ENDFOR
}

void SquareAndScale_1860() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[41]));
	ENDFOR
}

void SquareAndScale_1861() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[42]));
	ENDFOR
}

void SquareAndScale_1862() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[43]));
	ENDFOR
}

void SquareAndScale_1863() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[44]));
	ENDFOR
}

void SquareAndScale_1864() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[45]));
	ENDFOR
}

void SquareAndScale_1865() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[46]));
	ENDFOR
}

void SquareAndScale_1866() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[47]));
	ENDFOR
}

void SquareAndScale_1867() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[48]));
	ENDFOR
}

void SquareAndScale_1868() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[49]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1817() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 50, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1869_1871_split[__iter_], pop_complex(&ComplexSource_1811WEIGHTED_ROUND_ROBIN_Splitter_1817));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1818() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 50, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1818CFAR_gather_1814, pop_float(&SplitJoin0_SquareAndScale_Fiss_1869_1871_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1814_s.pos) - 5) >= 0)), __DEFLOOPBOUND__92__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1814_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1814_s.pos) < 64)), __DEFLOOPBOUND__93__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1814_s.poke[(i - 1)] = CFAR_gather_1814_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1814_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_1814_s.pos++ ; 
		if(CFAR_gather_1814_s.pos == 64) {
			CFAR_gather_1814_s.pos = 0 ; 
		}
	}


void CFAR_gather_1814() {
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1818CFAR_gather_1814), &(CFAR_gather_1814AnonFilter_a0_1815));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_1815() {
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_1814AnonFilter_a0_1815));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_1811WEIGHTED_ROUND_ROBIN_Splitter_1817);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1818CFAR_gather_1814);
	FOR(int, __iter_init_0_, 0, <, 50, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1869_1871_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 50, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1869_1871_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_1814AnonFilter_a0_1815);
// --- init: ComplexSource_1811
	 {
	ComplexSource_1811_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_1811WEIGHTED_ROUND_ROBIN_Splitter_1817));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1817
	
	FOR(uint32_t, __iter_, 0, <, 50, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1869_1871_split[__iter_], pop_complex(&ComplexSource_1811WEIGHTED_ROUND_ROBIN_Splitter_1817));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_1819
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[0]));
//--------------------------------
// --- init: SquareAndScale_1820
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[1]));
//--------------------------------
// --- init: SquareAndScale_1821
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[2]));
//--------------------------------
// --- init: SquareAndScale_1822
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[3]));
//--------------------------------
// --- init: SquareAndScale_1823
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[4]));
//--------------------------------
// --- init: SquareAndScale_1824
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[5]));
//--------------------------------
// --- init: SquareAndScale_1825
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[6]));
//--------------------------------
// --- init: SquareAndScale_1826
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[7]));
//--------------------------------
// --- init: SquareAndScale_1827
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[8]));
//--------------------------------
// --- init: SquareAndScale_1828
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[9]));
//--------------------------------
// --- init: SquareAndScale_1829
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[10]));
//--------------------------------
// --- init: SquareAndScale_1830
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[11]));
//--------------------------------
// --- init: SquareAndScale_1831
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[12]));
//--------------------------------
// --- init: SquareAndScale_1832
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[13]));
//--------------------------------
// --- init: SquareAndScale_1833
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[14]));
//--------------------------------
// --- init: SquareAndScale_1834
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[15]));
//--------------------------------
// --- init: SquareAndScale_1835
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[16]));
//--------------------------------
// --- init: SquareAndScale_1836
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[17]));
//--------------------------------
// --- init: SquareAndScale_1837
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[18]));
//--------------------------------
// --- init: SquareAndScale_1838
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[19]));
//--------------------------------
// --- init: SquareAndScale_1839
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[20]));
//--------------------------------
// --- init: SquareAndScale_1840
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[21]));
//--------------------------------
// --- init: SquareAndScale_1841
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[22]));
//--------------------------------
// --- init: SquareAndScale_1842
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[23]));
//--------------------------------
// --- init: SquareAndScale_1843
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[24]));
//--------------------------------
// --- init: SquareAndScale_1844
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[25]));
//--------------------------------
// --- init: SquareAndScale_1845
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[26]));
//--------------------------------
// --- init: SquareAndScale_1846
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[27]));
//--------------------------------
// --- init: SquareAndScale_1847
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[28]));
//--------------------------------
// --- init: SquareAndScale_1848
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[29]));
//--------------------------------
// --- init: SquareAndScale_1849
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[30]));
//--------------------------------
// --- init: SquareAndScale_1850
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[31]));
//--------------------------------
// --- init: SquareAndScale_1851
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[32]));
//--------------------------------
// --- init: SquareAndScale_1852
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[33]));
//--------------------------------
// --- init: SquareAndScale_1853
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[34]));
//--------------------------------
// --- init: SquareAndScale_1854
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[35]));
//--------------------------------
// --- init: SquareAndScale_1855
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[36]));
//--------------------------------
// --- init: SquareAndScale_1856
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[37]));
//--------------------------------
// --- init: SquareAndScale_1857
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[38]));
//--------------------------------
// --- init: SquareAndScale_1858
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[39]));
//--------------------------------
// --- init: SquareAndScale_1859
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[40]));
//--------------------------------
// --- init: SquareAndScale_1860
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[41]));
//--------------------------------
// --- init: SquareAndScale_1861
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[42]));
//--------------------------------
// --- init: SquareAndScale_1862
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[43]));
//--------------------------------
// --- init: SquareAndScale_1863
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[44]));
//--------------------------------
// --- init: SquareAndScale_1864
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[45]));
//--------------------------------
// --- init: SquareAndScale_1865
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[46]));
//--------------------------------
// --- init: SquareAndScale_1866
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[47]));
//--------------------------------
// --- init: SquareAndScale_1867
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[48]));
//--------------------------------
// --- init: SquareAndScale_1868
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1869_1871_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1869_1871_join[49]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1818
	
	FOR(uint32_t, __iter_, 0, <, 50, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1818CFAR_gather_1814, pop_float(&SplitJoin0_SquareAndScale_Fiss_1869_1871_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_1814
	 {
	CFAR_gather_1814_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1818CFAR_gather_1814), &(CFAR_gather_1814AnonFilter_a0_1815));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_1815
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_1814AnonFilter_a0_1815));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_1811();
		WEIGHTED_ROUND_ROBIN_Splitter_1817();
			SquareAndScale_1819();
			SquareAndScale_1820();
			SquareAndScale_1821();
			SquareAndScale_1822();
			SquareAndScale_1823();
			SquareAndScale_1824();
			SquareAndScale_1825();
			SquareAndScale_1826();
			SquareAndScale_1827();
			SquareAndScale_1828();
			SquareAndScale_1829();
			SquareAndScale_1830();
			SquareAndScale_1831();
			SquareAndScale_1832();
			SquareAndScale_1833();
			SquareAndScale_1834();
			SquareAndScale_1835();
			SquareAndScale_1836();
			SquareAndScale_1837();
			SquareAndScale_1838();
			SquareAndScale_1839();
			SquareAndScale_1840();
			SquareAndScale_1841();
			SquareAndScale_1842();
			SquareAndScale_1843();
			SquareAndScale_1844();
			SquareAndScale_1845();
			SquareAndScale_1846();
			SquareAndScale_1847();
			SquareAndScale_1848();
			SquareAndScale_1849();
			SquareAndScale_1850();
			SquareAndScale_1851();
			SquareAndScale_1852();
			SquareAndScale_1853();
			SquareAndScale_1854();
			SquareAndScale_1855();
			SquareAndScale_1856();
			SquareAndScale_1857();
			SquareAndScale_1858();
			SquareAndScale_1859();
			SquareAndScale_1860();
			SquareAndScale_1861();
			SquareAndScale_1862();
			SquareAndScale_1863();
			SquareAndScale_1864();
			SquareAndScale_1865();
			SquareAndScale_1866();
			SquareAndScale_1867();
			SquareAndScale_1868();
		WEIGHTED_ROUND_ROBIN_Joiner_1818();
		CFAR_gather_1814();
		AnonFilter_a0_1815();
	ENDFOR
	return EXIT_SUCCESS;
}
