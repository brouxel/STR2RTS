#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3072 on the compile command line
#else
#if BUF_SIZEMAX < 3072
#error BUF_SIZEMAX too small, it must be at least 3072
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_2141_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_2144_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_2141();
void WEIGHTED_ROUND_ROBIN_Splitter_2147();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_2149();
void SquareAndScale_2150();
void SquareAndScale_2151();
void SquareAndScale_2152();
void SquareAndScale_2153();
void SquareAndScale_2154();
void SquareAndScale_2155();
void SquareAndScale_2156();
void SquareAndScale_2157();
void SquareAndScale_2158();
void SquareAndScale_2159();
void SquareAndScale_2160();
void SquareAndScale_2161();
void SquareAndScale_2162();
void SquareAndScale_2163();
void SquareAndScale_2164();
void SquareAndScale_2165();
void SquareAndScale_2166();
void SquareAndScale_2167();
void SquareAndScale_2168();
void SquareAndScale_2169();
void SquareAndScale_2170();
void SquareAndScale_2171();
void SquareAndScale_2172();
void SquareAndScale_2173();
void SquareAndScale_2174();
void SquareAndScale_2175();
void SquareAndScale_2176();
void SquareAndScale_2177();
void SquareAndScale_2178();
void SquareAndScale_2179();
void SquareAndScale_2180();
void SquareAndScale_2181();
void SquareAndScale_2182();
void SquareAndScale_2183();
void SquareAndScale_2184();
void SquareAndScale_2185();
void SquareAndScale_2186();
void SquareAndScale_2187();
void SquareAndScale_2188();
void SquareAndScale_2189();
void SquareAndScale_2190();
void SquareAndScale_2191();
void SquareAndScale_2192();
void SquareAndScale_2193();
void SquareAndScale_2194();
void SquareAndScale_2195();
void WEIGHTED_ROUND_ROBIN_Joiner_2148();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_2144();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_2145();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1

#define __DEFLOOPBOUND__72__ -1

#define __DEFLOOPBOUND__73__ -1

#define __DEFLOOPBOUND__74__ -1

#define __DEFLOOPBOUND__75__ -1

#define __DEFLOOPBOUND__76__ -1

#define __DEFLOOPBOUND__77__ -1

#define __DEFLOOPBOUND__78__ -1

#define __DEFLOOPBOUND__79__ -1

#define __DEFLOOPBOUND__80__ -1

#define __DEFLOOPBOUND__81__ -1

#define __DEFLOOPBOUND__82__ -1

#define __DEFLOOPBOUND__83__ -1

#define __DEFLOOPBOUND__84__ -1

#define __DEFLOOPBOUND__85__ -1

#define __DEFLOOPBOUND__86__ -1

#define __DEFLOOPBOUND__87__ -1

#define __DEFLOOPBOUND__88__ -1

#define __DEFLOOPBOUND__89__ -1

#define __DEFLOOPBOUND__90__ -1

#define __DEFLOOPBOUND__91__ -1

#define __DEFLOOPBOUND__92__ -1

#define __DEFLOOPBOUND__93__ -1

#define __DEFLOOPBOUND__94__ -1

#define __DEFLOOPBOUND__95__ -1

#define __DEFLOOPBOUND__96__ -1

#define __DEFLOOPBOUND__97__ -1

#define __DEFLOOPBOUND__98__ -1

#define __DEFLOOPBOUND__99__ -1

#define __DEFLOOPBOUND__100__ -1

#define __DEFLOOPBOUND__101__ -1

#define __DEFLOOPBOUND__102__ -1

#define __DEFLOOPBOUND__103__ -1

#define __DEFLOOPBOUND__104__ -1

#define __DEFLOOPBOUND__105__ -1

#define __DEFLOOPBOUND__106__ -1

#define __DEFLOOPBOUND__107__ -1

#define __DEFLOOPBOUND__108__ -1

#define __DEFLOOPBOUND__109__ -1

#define __DEFLOOPBOUND__110__ -1

#define __DEFLOOPBOUND__111__ -1

#define __DEFLOOPBOUND__112__ -1

#define __DEFLOOPBOUND__113__ -1


#ifdef __cplusplus
}
#endif
#endif
