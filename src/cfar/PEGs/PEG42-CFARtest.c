#include "PEG42-CFARtest.h"

buffer_float_t CFAR_gather_2654AnonFilter_a0_2655;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2701_2703_split[42];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2658CFAR_gather_2654;
buffer_complex_t ComplexSource_2651WEIGHTED_ROUND_ROBIN_Splitter_2657;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_2701_2703_join[42];


ComplexSource_2651_t ComplexSource_2651_s;
CFAR_gather_2654_t CFAR_gather_2654_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2651_s.theta = (ComplexSource_2651_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2651_s.theta)) * (((float) cos(ComplexSource_2651_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2651_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2651_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2651_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2651_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2651_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2651_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2651() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		ComplexSource(&(ComplexSource_2651WEIGHTED_ROUND_ROBIN_Splitter_2657));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2659() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[0]));
	ENDFOR
}

void SquareAndScale_2660() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[1]));
	ENDFOR
}

void SquareAndScale_2661() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[2]));
	ENDFOR
}

void SquareAndScale_2662() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[3]));
	ENDFOR
}

void SquareAndScale_2663() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[4]));
	ENDFOR
}

void SquareAndScale_2664() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[5]));
	ENDFOR
}

void SquareAndScale_2665() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[6]));
	ENDFOR
}

void SquareAndScale_2666() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[7]));
	ENDFOR
}

void SquareAndScale_2667() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[8]));
	ENDFOR
}

void SquareAndScale_2668() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[9]));
	ENDFOR
}

void SquareAndScale_2669() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[10]));
	ENDFOR
}

void SquareAndScale_2670() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[11]));
	ENDFOR
}

void SquareAndScale_2671() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[12]));
	ENDFOR
}

void SquareAndScale_2672() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[13]));
	ENDFOR
}

void SquareAndScale_2673() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[14]));
	ENDFOR
}

void SquareAndScale_2674() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[15]));
	ENDFOR
}

void SquareAndScale_2675() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[16]));
	ENDFOR
}

void SquareAndScale_2676() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[17]));
	ENDFOR
}

void SquareAndScale_2677() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[18]));
	ENDFOR
}

void SquareAndScale_2678() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[19]));
	ENDFOR
}

void SquareAndScale_2679() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[20]));
	ENDFOR
}

void SquareAndScale_2680() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[21]));
	ENDFOR
}

void SquareAndScale_2681() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[22]));
	ENDFOR
}

void SquareAndScale_2682() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[23]));
	ENDFOR
}

void SquareAndScale_2683() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[24]));
	ENDFOR
}

void SquareAndScale_2684() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[25]));
	ENDFOR
}

void SquareAndScale_2685() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[26]));
	ENDFOR
}

void SquareAndScale_2686() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[27]));
	ENDFOR
}

void SquareAndScale_2687() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[28]));
	ENDFOR
}

void SquareAndScale_2688() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[29]));
	ENDFOR
}

void SquareAndScale_2689() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[30]));
	ENDFOR
}

void SquareAndScale_2690() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[31]));
	ENDFOR
}

void SquareAndScale_2691() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[32]));
	ENDFOR
}

void SquareAndScale_2692() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[33]));
	ENDFOR
}

void SquareAndScale_2693() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[34]));
	ENDFOR
}

void SquareAndScale_2694() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[35]));
	ENDFOR
}

void SquareAndScale_2695() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[36]));
	ENDFOR
}

void SquareAndScale_2696() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[37]));
	ENDFOR
}

void SquareAndScale_2697() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[38]));
	ENDFOR
}

void SquareAndScale_2698() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[39]));
	ENDFOR
}

void SquareAndScale_2699() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[40]));
	ENDFOR
}

void SquareAndScale_2700() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[41]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2657() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 42, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2701_2703_split[__iter_], pop_complex(&ComplexSource_2651WEIGHTED_ROUND_ROBIN_Splitter_2657));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2658() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 42, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2658CFAR_gather_2654, pop_float(&SplitJoin0_SquareAndScale_Fiss_2701_2703_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2654_s.pos) - 5) >= 0)), __DEFLOOPBOUND__140__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2654_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2654_s.pos) < 64)), __DEFLOOPBOUND__141__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2654_s.poke[(i - 1)] = CFAR_gather_2654_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2654_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2654_s.pos++ ; 
		if(CFAR_gather_2654_s.pos == 64) {
			CFAR_gather_2654_s.pos = 0 ; 
		}
	}


void CFAR_gather_2654() {
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2658CFAR_gather_2654), &(CFAR_gather_2654AnonFilter_a0_2655));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2655() {
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2654AnonFilter_a0_2655));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&CFAR_gather_2654AnonFilter_a0_2655);
	FOR(int, __iter_init_0_, 0, <, 42, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2701_2703_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2658CFAR_gather_2654);
	init_buffer_complex(&ComplexSource_2651WEIGHTED_ROUND_ROBIN_Splitter_2657);
	FOR(int, __iter_init_1_, 0, <, 42, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2701_2703_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_2651
	 {
	ComplexSource_2651_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2651WEIGHTED_ROUND_ROBIN_Splitter_2657));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2657
	
	FOR(uint32_t, __iter_, 0, <, 42, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2701_2703_split[__iter_], pop_complex(&ComplexSource_2651WEIGHTED_ROUND_ROBIN_Splitter_2657));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2659
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[0]));
//--------------------------------
// --- init: SquareAndScale_2660
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[1]));
//--------------------------------
// --- init: SquareAndScale_2661
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[2]));
//--------------------------------
// --- init: SquareAndScale_2662
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[3]));
//--------------------------------
// --- init: SquareAndScale_2663
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[4]));
//--------------------------------
// --- init: SquareAndScale_2664
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[5]));
//--------------------------------
// --- init: SquareAndScale_2665
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[6]));
//--------------------------------
// --- init: SquareAndScale_2666
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[7]));
//--------------------------------
// --- init: SquareAndScale_2667
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[8]));
//--------------------------------
// --- init: SquareAndScale_2668
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[9]));
//--------------------------------
// --- init: SquareAndScale_2669
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[10]));
//--------------------------------
// --- init: SquareAndScale_2670
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[11]));
//--------------------------------
// --- init: SquareAndScale_2671
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[12]));
//--------------------------------
// --- init: SquareAndScale_2672
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[13]));
//--------------------------------
// --- init: SquareAndScale_2673
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[14]));
//--------------------------------
// --- init: SquareAndScale_2674
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[15]));
//--------------------------------
// --- init: SquareAndScale_2675
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[16]));
//--------------------------------
// --- init: SquareAndScale_2676
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[17]));
//--------------------------------
// --- init: SquareAndScale_2677
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[18]));
//--------------------------------
// --- init: SquareAndScale_2678
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[19]));
//--------------------------------
// --- init: SquareAndScale_2679
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[20]));
//--------------------------------
// --- init: SquareAndScale_2680
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[21]));
//--------------------------------
// --- init: SquareAndScale_2681
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[22]));
//--------------------------------
// --- init: SquareAndScale_2682
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[23]));
//--------------------------------
// --- init: SquareAndScale_2683
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[24]));
//--------------------------------
// --- init: SquareAndScale_2684
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[25]));
//--------------------------------
// --- init: SquareAndScale_2685
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[26]));
//--------------------------------
// --- init: SquareAndScale_2686
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[27]));
//--------------------------------
// --- init: SquareAndScale_2687
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[28]));
//--------------------------------
// --- init: SquareAndScale_2688
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[29]));
//--------------------------------
// --- init: SquareAndScale_2689
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[30]));
//--------------------------------
// --- init: SquareAndScale_2690
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[31]));
//--------------------------------
// --- init: SquareAndScale_2691
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[32]));
//--------------------------------
// --- init: SquareAndScale_2692
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[33]));
//--------------------------------
// --- init: SquareAndScale_2693
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[34]));
//--------------------------------
// --- init: SquareAndScale_2694
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[35]));
//--------------------------------
// --- init: SquareAndScale_2695
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[36]));
//--------------------------------
// --- init: SquareAndScale_2696
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[37]));
//--------------------------------
// --- init: SquareAndScale_2697
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[38]));
//--------------------------------
// --- init: SquareAndScale_2698
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[39]));
//--------------------------------
// --- init: SquareAndScale_2699
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[40]));
//--------------------------------
// --- init: SquareAndScale_2700
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2701_2703_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2701_2703_join[41]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2658
	
	FOR(uint32_t, __iter_, 0, <, 42, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2658CFAR_gather_2654, pop_float(&SplitJoin0_SquareAndScale_Fiss_2701_2703_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2654
	 {
	CFAR_gather_2654_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2658CFAR_gather_2654), &(CFAR_gather_2654AnonFilter_a0_2655));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2655
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2654AnonFilter_a0_2655));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2651();
		WEIGHTED_ROUND_ROBIN_Splitter_2657();
			SquareAndScale_2659();
			SquareAndScale_2660();
			SquareAndScale_2661();
			SquareAndScale_2662();
			SquareAndScale_2663();
			SquareAndScale_2664();
			SquareAndScale_2665();
			SquareAndScale_2666();
			SquareAndScale_2667();
			SquareAndScale_2668();
			SquareAndScale_2669();
			SquareAndScale_2670();
			SquareAndScale_2671();
			SquareAndScale_2672();
			SquareAndScale_2673();
			SquareAndScale_2674();
			SquareAndScale_2675();
			SquareAndScale_2676();
			SquareAndScale_2677();
			SquareAndScale_2678();
			SquareAndScale_2679();
			SquareAndScale_2680();
			SquareAndScale_2681();
			SquareAndScale_2682();
			SquareAndScale_2683();
			SquareAndScale_2684();
			SquareAndScale_2685();
			SquareAndScale_2686();
			SquareAndScale_2687();
			SquareAndScale_2688();
			SquareAndScale_2689();
			SquareAndScale_2690();
			SquareAndScale_2691();
			SquareAndScale_2692();
			SquareAndScale_2693();
			SquareAndScale_2694();
			SquareAndScale_2695();
			SquareAndScale_2696();
			SquareAndScale_2697();
			SquareAndScale_2698();
			SquareAndScale_2699();
			SquareAndScale_2700();
		WEIGHTED_ROUND_ROBIN_Joiner_2658();
		CFAR_gather_2654();
		AnonFilter_a0_2655();
	ENDFOR
	return EXIT_SUCCESS;
}
