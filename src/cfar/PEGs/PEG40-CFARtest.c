#include "PEG40-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2848CFAR_gather_2844;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2889_2891_split[40];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_2889_2891_join[40];
buffer_complex_t ComplexSource_2841WEIGHTED_ROUND_ROBIN_Splitter_2847;
buffer_float_t CFAR_gather_2844AnonFilter_a0_2845;


ComplexSource_2841_t ComplexSource_2841_s;
CFAR_gather_2844_t CFAR_gather_2844_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2841_s.theta = (ComplexSource_2841_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2841_s.theta)) * (((float) cos(ComplexSource_2841_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2841_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2841_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2841_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2841_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2841_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2841_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2841() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		ComplexSource(&(ComplexSource_2841WEIGHTED_ROUND_ROBIN_Splitter_2847));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2849() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[0]));
	ENDFOR
}

void SquareAndScale_2850() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[1]));
	ENDFOR
}

void SquareAndScale_2851() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[2]));
	ENDFOR
}

void SquareAndScale_2852() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[3]));
	ENDFOR
}

void SquareAndScale_2853() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[4]));
	ENDFOR
}

void SquareAndScale_2854() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[5]));
	ENDFOR
}

void SquareAndScale_2855() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[6]));
	ENDFOR
}

void SquareAndScale_2856() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[7]));
	ENDFOR
}

void SquareAndScale_2857() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[8]));
	ENDFOR
}

void SquareAndScale_2858() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[9]));
	ENDFOR
}

void SquareAndScale_2859() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[10]));
	ENDFOR
}

void SquareAndScale_2860() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[11]));
	ENDFOR
}

void SquareAndScale_2861() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[12]));
	ENDFOR
}

void SquareAndScale_2862() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[13]));
	ENDFOR
}

void SquareAndScale_2863() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[14]));
	ENDFOR
}

void SquareAndScale_2864() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[15]));
	ENDFOR
}

void SquareAndScale_2865() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[16]));
	ENDFOR
}

void SquareAndScale_2866() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[17]));
	ENDFOR
}

void SquareAndScale_2867() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[18]));
	ENDFOR
}

void SquareAndScale_2868() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[19]));
	ENDFOR
}

void SquareAndScale_2869() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[20]));
	ENDFOR
}

void SquareAndScale_2870() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[21]));
	ENDFOR
}

void SquareAndScale_2871() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[22]));
	ENDFOR
}

void SquareAndScale_2872() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[23]));
	ENDFOR
}

void SquareAndScale_2873() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[24]));
	ENDFOR
}

void SquareAndScale_2874() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[25]));
	ENDFOR
}

void SquareAndScale_2875() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[26]));
	ENDFOR
}

void SquareAndScale_2876() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[27]));
	ENDFOR
}

void SquareAndScale_2877() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[28]));
	ENDFOR
}

void SquareAndScale_2878() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[29]));
	ENDFOR
}

void SquareAndScale_2879() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[30]));
	ENDFOR
}

void SquareAndScale_2880() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[31]));
	ENDFOR
}

void SquareAndScale_2881() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[32]));
	ENDFOR
}

void SquareAndScale_2882() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[33]));
	ENDFOR
}

void SquareAndScale_2883() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[34]));
	ENDFOR
}

void SquareAndScale_2884() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[35]));
	ENDFOR
}

void SquareAndScale_2885() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[36]));
	ENDFOR
}

void SquareAndScale_2886() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[37]));
	ENDFOR
}

void SquareAndScale_2887() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[38]));
	ENDFOR
}

void SquareAndScale_2888() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[39]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2847() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2889_2891_split[__iter_], pop_complex(&ComplexSource_2841WEIGHTED_ROUND_ROBIN_Splitter_2847));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2848() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2848CFAR_gather_2844, pop_float(&SplitJoin0_SquareAndScale_Fiss_2889_2891_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2844_s.pos) - 5) >= 0)), __DEFLOOPBOUND__152__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2844_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2844_s.pos) < 64)), __DEFLOOPBOUND__153__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2844_s.poke[(i - 1)] = CFAR_gather_2844_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2844_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2844_s.pos++ ; 
		if(CFAR_gather_2844_s.pos == 64) {
			CFAR_gather_2844_s.pos = 0 ; 
		}
	}


void CFAR_gather_2844() {
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2848CFAR_gather_2844), &(CFAR_gather_2844AnonFilter_a0_2845));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2845() {
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2844AnonFilter_a0_2845));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2848CFAR_gather_2844);
	FOR(int, __iter_init_0_, 0, <, 40, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2889_2891_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 40, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2889_2891_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_2841WEIGHTED_ROUND_ROBIN_Splitter_2847);
	init_buffer_float(&CFAR_gather_2844AnonFilter_a0_2845);
// --- init: ComplexSource_2841
	 {
	ComplexSource_2841_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2841WEIGHTED_ROUND_ROBIN_Splitter_2847));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2847
	
	FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2889_2891_split[__iter_], pop_complex(&ComplexSource_2841WEIGHTED_ROUND_ROBIN_Splitter_2847));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2849
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[0]));
//--------------------------------
// --- init: SquareAndScale_2850
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[1]));
//--------------------------------
// --- init: SquareAndScale_2851
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[2]));
//--------------------------------
// --- init: SquareAndScale_2852
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[3]));
//--------------------------------
// --- init: SquareAndScale_2853
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[4]));
//--------------------------------
// --- init: SquareAndScale_2854
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[5]));
//--------------------------------
// --- init: SquareAndScale_2855
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[6]));
//--------------------------------
// --- init: SquareAndScale_2856
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[7]));
//--------------------------------
// --- init: SquareAndScale_2857
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[8]));
//--------------------------------
// --- init: SquareAndScale_2858
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[9]));
//--------------------------------
// --- init: SquareAndScale_2859
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[10]));
//--------------------------------
// --- init: SquareAndScale_2860
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[11]));
//--------------------------------
// --- init: SquareAndScale_2861
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[12]));
//--------------------------------
// --- init: SquareAndScale_2862
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[13]));
//--------------------------------
// --- init: SquareAndScale_2863
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[14]));
//--------------------------------
// --- init: SquareAndScale_2864
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[15]));
//--------------------------------
// --- init: SquareAndScale_2865
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[16]));
//--------------------------------
// --- init: SquareAndScale_2866
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[17]));
//--------------------------------
// --- init: SquareAndScale_2867
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[18]));
//--------------------------------
// --- init: SquareAndScale_2868
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[19]));
//--------------------------------
// --- init: SquareAndScale_2869
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[20]));
//--------------------------------
// --- init: SquareAndScale_2870
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[21]));
//--------------------------------
// --- init: SquareAndScale_2871
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[22]));
//--------------------------------
// --- init: SquareAndScale_2872
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[23]));
//--------------------------------
// --- init: SquareAndScale_2873
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[24]));
//--------------------------------
// --- init: SquareAndScale_2874
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[25]));
//--------------------------------
// --- init: SquareAndScale_2875
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[26]));
//--------------------------------
// --- init: SquareAndScale_2876
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[27]));
//--------------------------------
// --- init: SquareAndScale_2877
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[28]));
//--------------------------------
// --- init: SquareAndScale_2878
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[29]));
//--------------------------------
// --- init: SquareAndScale_2879
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[30]));
//--------------------------------
// --- init: SquareAndScale_2880
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[31]));
//--------------------------------
// --- init: SquareAndScale_2881
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[32]));
//--------------------------------
// --- init: SquareAndScale_2882
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[33]));
//--------------------------------
// --- init: SquareAndScale_2883
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[34]));
//--------------------------------
// --- init: SquareAndScale_2884
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[35]));
//--------------------------------
// --- init: SquareAndScale_2885
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[36]));
//--------------------------------
// --- init: SquareAndScale_2886
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[37]));
//--------------------------------
// --- init: SquareAndScale_2887
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[38]));
//--------------------------------
// --- init: SquareAndScale_2888
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2889_2891_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2889_2891_join[39]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2848
	
	FOR(uint32_t, __iter_, 0, <, 40, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2848CFAR_gather_2844, pop_float(&SplitJoin0_SquareAndScale_Fiss_2889_2891_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2844
	 {
	CFAR_gather_2844_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2848CFAR_gather_2844), &(CFAR_gather_2844AnonFilter_a0_2845));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2845
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2844AnonFilter_a0_2845));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2841();
		WEIGHTED_ROUND_ROBIN_Splitter_2847();
			SquareAndScale_2849();
			SquareAndScale_2850();
			SquareAndScale_2851();
			SquareAndScale_2852();
			SquareAndScale_2853();
			SquareAndScale_2854();
			SquareAndScale_2855();
			SquareAndScale_2856();
			SquareAndScale_2857();
			SquareAndScale_2858();
			SquareAndScale_2859();
			SquareAndScale_2860();
			SquareAndScale_2861();
			SquareAndScale_2862();
			SquareAndScale_2863();
			SquareAndScale_2864();
			SquareAndScale_2865();
			SquareAndScale_2866();
			SquareAndScale_2867();
			SquareAndScale_2868();
			SquareAndScale_2869();
			SquareAndScale_2870();
			SquareAndScale_2871();
			SquareAndScale_2872();
			SquareAndScale_2873();
			SquareAndScale_2874();
			SquareAndScale_2875();
			SquareAndScale_2876();
			SquareAndScale_2877();
			SquareAndScale_2878();
			SquareAndScale_2879();
			SquareAndScale_2880();
			SquareAndScale_2881();
			SquareAndScale_2882();
			SquareAndScale_2883();
			SquareAndScale_2884();
			SquareAndScale_2885();
			SquareAndScale_2886();
			SquareAndScale_2887();
			SquareAndScale_2888();
		WEIGHTED_ROUND_ROBIN_Joiner_2848();
		CFAR_gather_2844();
		AnonFilter_a0_2845();
	ENDFOR
	return EXIT_SUCCESS;
}
