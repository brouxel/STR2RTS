#include "PEG22-CFARtest.h"

buffer_complex_t ComplexSource_4191WEIGHTED_ROUND_ROBIN_Splitter_4197;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4198CFAR_gather_4194;
buffer_float_t CFAR_gather_4194AnonFilter_a0_4195;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4221_4223_split[22];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4221_4223_join[22];


ComplexSource_4191_t ComplexSource_4191_s;
CFAR_gather_4194_t CFAR_gather_4194_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4191_s.theta = (ComplexSource_4191_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4191_s.theta)) * (((float) cos(ComplexSource_4191_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4191_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4191_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4191_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4191_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4191_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4191_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4191() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		ComplexSource(&(ComplexSource_4191WEIGHTED_ROUND_ROBIN_Splitter_4197));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4199() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[0]));
	ENDFOR
}

void SquareAndScale_4200() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[1]));
	ENDFOR
}

void SquareAndScale_4201() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[2]));
	ENDFOR
}

void SquareAndScale_4202() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[3]));
	ENDFOR
}

void SquareAndScale_4203() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[4]));
	ENDFOR
}

void SquareAndScale_4204() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[5]));
	ENDFOR
}

void SquareAndScale_4205() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[6]));
	ENDFOR
}

void SquareAndScale_4206() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[7]));
	ENDFOR
}

void SquareAndScale_4207() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[8]));
	ENDFOR
}

void SquareAndScale_4208() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[9]));
	ENDFOR
}

void SquareAndScale_4209() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[10]));
	ENDFOR
}

void SquareAndScale_4210() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[11]));
	ENDFOR
}

void SquareAndScale_4211() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[12]));
	ENDFOR
}

void SquareAndScale_4212() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[13]));
	ENDFOR
}

void SquareAndScale_4213() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[14]));
	ENDFOR
}

void SquareAndScale_4214() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[15]));
	ENDFOR
}

void SquareAndScale_4215() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[16]));
	ENDFOR
}

void SquareAndScale_4216() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[17]));
	ENDFOR
}

void SquareAndScale_4217() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[18]));
	ENDFOR
}

void SquareAndScale_4218() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[19]));
	ENDFOR
}

void SquareAndScale_4219() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[20]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[20]));
	ENDFOR
}

void SquareAndScale_4220() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[21]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[21]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4197() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4221_4223_split[__iter_], pop_complex(&ComplexSource_4191WEIGHTED_ROUND_ROBIN_Splitter_4197));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4198() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4198CFAR_gather_4194, pop_float(&SplitJoin0_SquareAndScale_Fiss_4221_4223_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4194_s.pos) - 5) >= 0)), __DEFLOOPBOUND__260__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4194_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4194_s.pos) < 64)), __DEFLOOPBOUND__261__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4194_s.poke[(i - 1)] = CFAR_gather_4194_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4194_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4194_s.pos++ ; 
		if(CFAR_gather_4194_s.pos == 64) {
			CFAR_gather_4194_s.pos = 0 ; 
		}
	}


void CFAR_gather_4194() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4198CFAR_gather_4194), &(CFAR_gather_4194AnonFilter_a0_4195));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4195() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4194AnonFilter_a0_4195));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_4191WEIGHTED_ROUND_ROBIN_Splitter_4197);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4198CFAR_gather_4194);
	init_buffer_float(&CFAR_gather_4194AnonFilter_a0_4195);
	FOR(int, __iter_init_0_, 0, <, 22, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4221_4223_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 22, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4221_4223_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4191
	 {
	ComplexSource_4191_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4191WEIGHTED_ROUND_ROBIN_Splitter_4197));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4197
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4221_4223_split[__iter_], pop_complex(&ComplexSource_4191WEIGHTED_ROUND_ROBIN_Splitter_4197));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4199
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4200
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4201
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4202
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4203
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4204
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4205
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4206
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4207
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4208
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4209
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4210
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4211
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4212
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4213
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4214
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4215
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4216
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4217
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4218
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4219
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[20]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4220
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4221_4223_split[21]), &(SplitJoin0_SquareAndScale_Fiss_4221_4223_join[21]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4198
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4198CFAR_gather_4194, pop_float(&SplitJoin0_SquareAndScale_Fiss_4221_4223_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4194
	 {
	CFAR_gather_4194_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4198CFAR_gather_4194), &(CFAR_gather_4194AnonFilter_a0_4195));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4195
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4194AnonFilter_a0_4195));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4191();
		WEIGHTED_ROUND_ROBIN_Splitter_4197();
			SquareAndScale_4199();
			SquareAndScale_4200();
			SquareAndScale_4201();
			SquareAndScale_4202();
			SquareAndScale_4203();
			SquareAndScale_4204();
			SquareAndScale_4205();
			SquareAndScale_4206();
			SquareAndScale_4207();
			SquareAndScale_4208();
			SquareAndScale_4209();
			SquareAndScale_4210();
			SquareAndScale_4211();
			SquareAndScale_4212();
			SquareAndScale_4213();
			SquareAndScale_4214();
			SquareAndScale_4215();
			SquareAndScale_4216();
			SquareAndScale_4217();
			SquareAndScale_4218();
			SquareAndScale_4219();
			SquareAndScale_4220();
		WEIGHTED_ROUND_ROBIN_Joiner_4198();
		CFAR_gather_4194();
		AnonFilter_a0_4195();
	ENDFOR
	return EXIT_SUCCESS;
}
