#include "PEG21-CFARtest.h"

buffer_complex_t ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253;
buffer_float_t CFAR_gather_4250AnonFilter_a0_4251;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4276_4278_split[21];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4276_4278_join[21];


ComplexSource_4247_t ComplexSource_4247_s;
CFAR_gather_4250_t CFAR_gather_4250_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4247_s.theta = (ComplexSource_4247_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4247_s.theta)) * (((float) cos(ComplexSource_4247_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4247_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4247_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4247_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4247_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4247_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4247_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4247() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		ComplexSource(&(ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4255() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[0]));
	ENDFOR
}

void SquareAndScale_4256() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[1]));
	ENDFOR
}

void SquareAndScale_4257() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[2]));
	ENDFOR
}

void SquareAndScale_4258() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[3]));
	ENDFOR
}

void SquareAndScale_4259() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[4]));
	ENDFOR
}

void SquareAndScale_4260() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[5]));
	ENDFOR
}

void SquareAndScale_4261() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[6]));
	ENDFOR
}

void SquareAndScale_4262() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[7]));
	ENDFOR
}

void SquareAndScale_4263() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[8]));
	ENDFOR
}

void SquareAndScale_4264() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[9]));
	ENDFOR
}

void SquareAndScale_4265() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[10]));
	ENDFOR
}

void SquareAndScale_4266() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[11]));
	ENDFOR
}

void SquareAndScale_4267() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[12]));
	ENDFOR
}

void SquareAndScale_4268() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[13]));
	ENDFOR
}

void SquareAndScale_4269() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[14]));
	ENDFOR
}

void SquareAndScale_4270() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[15]));
	ENDFOR
}

void SquareAndScale_4271() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[16]));
	ENDFOR
}

void SquareAndScale_4272() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[17]));
	ENDFOR
}

void SquareAndScale_4273() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[18]));
	ENDFOR
}

void SquareAndScale_4274() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[19]));
	ENDFOR
}

void SquareAndScale_4275() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[20]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[20]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4253() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[__iter_], pop_complex(&ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4254() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250, pop_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4250_s.pos) - 5) >= 0)), __DEFLOOPBOUND__266__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4250_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4250_s.pos) < 64)), __DEFLOOPBOUND__267__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4250_s.poke[(i - 1)] = CFAR_gather_4250_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4250_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4250_s.pos++ ; 
		if(CFAR_gather_4250_s.pos == 64) {
			CFAR_gather_4250_s.pos = 0 ; 
		}
	}


void CFAR_gather_4250() {
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250), &(CFAR_gather_4250AnonFilter_a0_4251));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4251() {
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4250AnonFilter_a0_4251));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253);
	init_buffer_float(&CFAR_gather_4250AnonFilter_a0_4251);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250);
	FOR(int, __iter_init_0_, 0, <, 21, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 21, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4247
	 {
	ComplexSource_4247_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4253
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[__iter_], pop_complex(&ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4255
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4256
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4257
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4258
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4259
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4260
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4261
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4262
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4263
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4264
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4265
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4266
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4267
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4268
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4269
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4270
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4271
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4272
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4273
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4274
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4275
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4276_4278_split[20]), &(SplitJoin0_SquareAndScale_Fiss_4276_4278_join[20]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4254
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250, pop_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4250
	 {
	CFAR_gather_4250_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250), &(CFAR_gather_4250AnonFilter_a0_4251));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4251
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4250AnonFilter_a0_4251));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4247();
		WEIGHTED_ROUND_ROBIN_Splitter_4253();
			SquareAndScale_4255();
			SquareAndScale_4256();
			SquareAndScale_4257();
			SquareAndScale_4258();
			SquareAndScale_4259();
			SquareAndScale_4260();
			SquareAndScale_4261();
			SquareAndScale_4262();
			SquareAndScale_4263();
			SquareAndScale_4264();
			SquareAndScale_4265();
			SquareAndScale_4266();
			SquareAndScale_4267();
			SquareAndScale_4268();
			SquareAndScale_4269();
			SquareAndScale_4270();
			SquareAndScale_4271();
			SquareAndScale_4272();
			SquareAndScale_4273();
			SquareAndScale_4274();
			SquareAndScale_4275();
		WEIGHTED_ROUND_ROBIN_Joiner_4254();
		CFAR_gather_4250();
		AnonFilter_a0_4251();
	ENDFOR
	return EXIT_SUCCESS;
}
