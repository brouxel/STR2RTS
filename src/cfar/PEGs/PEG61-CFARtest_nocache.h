#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=43516 on the compile command line
#else
#if BUF_SIZEMAX < 43516
#error BUF_SIZEMAX too small, it must be at least 43516
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_447_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_450_t;
void ComplexSource_447();
void WEIGHTED_ROUND_ROBIN_Splitter_453();
void SquareAndScale_455();
void SquareAndScale_456();
void SquareAndScale_457();
void SquareAndScale_458();
void SquareAndScale_459();
void SquareAndScale_460();
void SquareAndScale_461();
void SquareAndScale_462();
void SquareAndScale_463();
void SquareAndScale_464();
void SquareAndScale_465();
void SquareAndScale_466();
void SquareAndScale_467();
void SquareAndScale_468();
void SquareAndScale_469();
void SquareAndScale_470();
void SquareAndScale_471();
void SquareAndScale_472();
void SquareAndScale_473();
void SquareAndScale_474();
void SquareAndScale_475();
void SquareAndScale_476();
void SquareAndScale_477();
void SquareAndScale_478();
void SquareAndScale_479();
void SquareAndScale_480();
void SquareAndScale_481();
void SquareAndScale_482();
void SquareAndScale_483();
void SquareAndScale_484();
void SquareAndScale_485();
void SquareAndScale_486();
void SquareAndScale_487();
void SquareAndScale_488();
void SquareAndScale_489();
void SquareAndScale_490();
void SquareAndScale_491();
void SquareAndScale_492();
void SquareAndScale_493();
void SquareAndScale_494();
void SquareAndScale_495();
void SquareAndScale_496();
void SquareAndScale_497();
void SquareAndScale_498();
void SquareAndScale_499();
void SquareAndScale_500();
void SquareAndScale_501();
void SquareAndScale_502();
void SquareAndScale_503();
void SquareAndScale_504();
void SquareAndScale_505();
void SquareAndScale_506();
void SquareAndScale_507();
void SquareAndScale_508();
void SquareAndScale_509();
void SquareAndScale_510();
void SquareAndScale_511();
void SquareAndScale_512();
void SquareAndScale_513();
void SquareAndScale_514();
void SquareAndScale_515();
void WEIGHTED_ROUND_ROBIN_Joiner_454();
void CFAR_gather_450();
void AnonFilter_a0_451();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1


#ifdef __cplusplus
}
#endif
#endif
