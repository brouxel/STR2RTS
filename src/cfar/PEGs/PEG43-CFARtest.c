#include "PEG43-CFARtest.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_2604_2606_join[43];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556;
buffer_float_t CFAR_gather_2556AnonFilter_a0_2557;
buffer_complex_t ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2604_2606_split[43];


ComplexSource_2553_t ComplexSource_2553_s;
CFAR_gather_2556_t CFAR_gather_2556_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2553_s.theta = (ComplexSource_2553_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2553_s.theta)) * (((float) cos(ComplexSource_2553_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2553_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2553_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2553_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2553_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2553_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2553_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2553() {
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++)
		ComplexSource(&(ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2561() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[0]));
	ENDFOR
}

void SquareAndScale_2562() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[1]));
	ENDFOR
}

void SquareAndScale_2563() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[2]));
	ENDFOR
}

void SquareAndScale_2564() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[3]));
	ENDFOR
}

void SquareAndScale_2565() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[4]));
	ENDFOR
}

void SquareAndScale_2566() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[5]));
	ENDFOR
}

void SquareAndScale_2567() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[6]));
	ENDFOR
}

void SquareAndScale_2568() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[7]));
	ENDFOR
}

void SquareAndScale_2569() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[8]));
	ENDFOR
}

void SquareAndScale_2570() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[9]));
	ENDFOR
}

void SquareAndScale_2571() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[10]));
	ENDFOR
}

void SquareAndScale_2572() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[11]));
	ENDFOR
}

void SquareAndScale_2573() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[12]));
	ENDFOR
}

void SquareAndScale_2574() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[13]));
	ENDFOR
}

void SquareAndScale_2575() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[14]));
	ENDFOR
}

void SquareAndScale_2576() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[15]));
	ENDFOR
}

void SquareAndScale_2577() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[16]));
	ENDFOR
}

void SquareAndScale_2578() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[17]));
	ENDFOR
}

void SquareAndScale_2579() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[18]));
	ENDFOR
}

void SquareAndScale_2580() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[19]));
	ENDFOR
}

void SquareAndScale_2581() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[20]));
	ENDFOR
}

void SquareAndScale_2582() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[21]));
	ENDFOR
}

void SquareAndScale_2583() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[22]));
	ENDFOR
}

void SquareAndScale_2584() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[23]));
	ENDFOR
}

void SquareAndScale_2585() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[24]));
	ENDFOR
}

void SquareAndScale_2586() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[25]));
	ENDFOR
}

void SquareAndScale_2587() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[26]));
	ENDFOR
}

void SquareAndScale_2588() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[27]));
	ENDFOR
}

void SquareAndScale_2589() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[28]));
	ENDFOR
}

void SquareAndScale_2590() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[29]));
	ENDFOR
}

void SquareAndScale_2591() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[30]));
	ENDFOR
}

void SquareAndScale_2592() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[31]));
	ENDFOR
}

void SquareAndScale_2593() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[32]));
	ENDFOR
}

void SquareAndScale_2594() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[33]));
	ENDFOR
}

void SquareAndScale_2595() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[34]));
	ENDFOR
}

void SquareAndScale_2596() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[35]));
	ENDFOR
}

void SquareAndScale_2597() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[36]));
	ENDFOR
}

void SquareAndScale_2598() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[37]));
	ENDFOR
}

void SquareAndScale_2599() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[38]));
	ENDFOR
}

void SquareAndScale_2600() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[39]));
	ENDFOR
}

void SquareAndScale_2601() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[40]));
	ENDFOR
}

void SquareAndScale_2602() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[41]));
	ENDFOR
}

void SquareAndScale_2603() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[42]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2559() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[__iter_], pop_complex(&ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2560() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556, pop_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2556_s.pos) - 5) >= 0)), __DEFLOOPBOUND__134__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2556_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2556_s.pos) < 64)), __DEFLOOPBOUND__135__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2556_s.poke[(i - 1)] = CFAR_gather_2556_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2556_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2556_s.pos++ ; 
		if(CFAR_gather_2556_s.pos == 64) {
			CFAR_gather_2556_s.pos = 0 ; 
		}
	}


void CFAR_gather_2556() {
	FOR(uint32_t, __iter_steady_, 0, <, 2752, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556), &(CFAR_gather_2556AnonFilter_a0_2557));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2557() {
	FOR(uint32_t, __iter_steady_, 0, <, 2752, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2556AnonFilter_a0_2557));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 43, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556);
	init_buffer_float(&CFAR_gather_2556AnonFilter_a0_2557);
	init_buffer_complex(&ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559);
	FOR(int, __iter_init_1_, 0, <, 43, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_2553
	 {
	ComplexSource_2553_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2559
	
	FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2604_2606_split[__iter_], pop_complex(&ComplexSource_2553WEIGHTED_ROUND_ROBIN_Splitter_2559));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2561
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[0]));
//--------------------------------
// --- init: SquareAndScale_2562
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[1]));
//--------------------------------
// --- init: SquareAndScale_2563
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[2]));
//--------------------------------
// --- init: SquareAndScale_2564
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[3]));
//--------------------------------
// --- init: SquareAndScale_2565
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[4]));
//--------------------------------
// --- init: SquareAndScale_2566
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[5]));
//--------------------------------
// --- init: SquareAndScale_2567
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[6]));
//--------------------------------
// --- init: SquareAndScale_2568
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[7]));
//--------------------------------
// --- init: SquareAndScale_2569
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[8]));
//--------------------------------
// --- init: SquareAndScale_2570
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[9]));
//--------------------------------
// --- init: SquareAndScale_2571
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[10]));
//--------------------------------
// --- init: SquareAndScale_2572
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[11]));
//--------------------------------
// --- init: SquareAndScale_2573
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[12]));
//--------------------------------
// --- init: SquareAndScale_2574
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[13]));
//--------------------------------
// --- init: SquareAndScale_2575
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[14]));
//--------------------------------
// --- init: SquareAndScale_2576
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[15]));
//--------------------------------
// --- init: SquareAndScale_2577
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[16]));
//--------------------------------
// --- init: SquareAndScale_2578
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[17]));
//--------------------------------
// --- init: SquareAndScale_2579
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[18]));
//--------------------------------
// --- init: SquareAndScale_2580
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[19]));
//--------------------------------
// --- init: SquareAndScale_2581
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[20]));
//--------------------------------
// --- init: SquareAndScale_2582
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[21]));
//--------------------------------
// --- init: SquareAndScale_2583
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[22]));
//--------------------------------
// --- init: SquareAndScale_2584
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[23]));
//--------------------------------
// --- init: SquareAndScale_2585
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[24]));
//--------------------------------
// --- init: SquareAndScale_2586
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[25]));
//--------------------------------
// --- init: SquareAndScale_2587
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[26]));
//--------------------------------
// --- init: SquareAndScale_2588
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[27]));
//--------------------------------
// --- init: SquareAndScale_2589
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[28]));
//--------------------------------
// --- init: SquareAndScale_2590
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[29]));
//--------------------------------
// --- init: SquareAndScale_2591
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[30]));
//--------------------------------
// --- init: SquareAndScale_2592
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[31]));
//--------------------------------
// --- init: SquareAndScale_2593
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[32]));
//--------------------------------
// --- init: SquareAndScale_2594
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[33]));
//--------------------------------
// --- init: SquareAndScale_2595
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[34]));
//--------------------------------
// --- init: SquareAndScale_2596
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[35]));
//--------------------------------
// --- init: SquareAndScale_2597
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[36]));
//--------------------------------
// --- init: SquareAndScale_2598
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[37]));
//--------------------------------
// --- init: SquareAndScale_2599
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[38]));
//--------------------------------
// --- init: SquareAndScale_2600
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[39]));
//--------------------------------
// --- init: SquareAndScale_2601
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[40]));
//--------------------------------
// --- init: SquareAndScale_2602
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[41]));
//--------------------------------
// --- init: SquareAndScale_2603
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2604_2606_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2604_2606_join[42]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2560
	
	FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556, pop_float(&SplitJoin0_SquareAndScale_Fiss_2604_2606_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2556
	 {
	CFAR_gather_2556_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2560CFAR_gather_2556), &(CFAR_gather_2556AnonFilter_a0_2557));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2557
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2556AnonFilter_a0_2557));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2553();
		WEIGHTED_ROUND_ROBIN_Splitter_2559();
			SquareAndScale_2561();
			SquareAndScale_2562();
			SquareAndScale_2563();
			SquareAndScale_2564();
			SquareAndScale_2565();
			SquareAndScale_2566();
			SquareAndScale_2567();
			SquareAndScale_2568();
			SquareAndScale_2569();
			SquareAndScale_2570();
			SquareAndScale_2571();
			SquareAndScale_2572();
			SquareAndScale_2573();
			SquareAndScale_2574();
			SquareAndScale_2575();
			SquareAndScale_2576();
			SquareAndScale_2577();
			SquareAndScale_2578();
			SquareAndScale_2579();
			SquareAndScale_2580();
			SquareAndScale_2581();
			SquareAndScale_2582();
			SquareAndScale_2583();
			SquareAndScale_2584();
			SquareAndScale_2585();
			SquareAndScale_2586();
			SquareAndScale_2587();
			SquareAndScale_2588();
			SquareAndScale_2589();
			SquareAndScale_2590();
			SquareAndScale_2591();
			SquareAndScale_2592();
			SquareAndScale_2593();
			SquareAndScale_2594();
			SquareAndScale_2595();
			SquareAndScale_2596();
			SquareAndScale_2597();
			SquareAndScale_2598();
			SquareAndScale_2599();
			SquareAndScale_2600();
			SquareAndScale_2601();
			SquareAndScale_2602();
			SquareAndScale_2603();
		WEIGHTED_ROUND_ROBIN_Joiner_2560();
		CFAR_gather_2556();
		AnonFilter_a0_2557();
	ENDFOR
	return EXIT_SUCCESS;
}
