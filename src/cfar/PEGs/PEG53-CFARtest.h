#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3456 on the compile command line
#else
#if BUF_SIZEMAX < 3456
#error BUF_SIZEMAX too small, it must be at least 3456
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_1463_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_1466_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_1463();
void WEIGHTED_ROUND_ROBIN_Splitter_1469();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_1471();
void SquareAndScale_1472();
void SquareAndScale_1473();
void SquareAndScale_1474();
void SquareAndScale_1475();
void SquareAndScale_1476();
void SquareAndScale_1477();
void SquareAndScale_1478();
void SquareAndScale_1479();
void SquareAndScale_1480();
void SquareAndScale_1481();
void SquareAndScale_1482();
void SquareAndScale_1483();
void SquareAndScale_1484();
void SquareAndScale_1485();
void SquareAndScale_1486();
void SquareAndScale_1487();
void SquareAndScale_1488();
void SquareAndScale_1489();
void SquareAndScale_1490();
void SquareAndScale_1491();
void SquareAndScale_1492();
void SquareAndScale_1493();
void SquareAndScale_1494();
void SquareAndScale_1495();
void SquareAndScale_1496();
void SquareAndScale_1497();
void SquareAndScale_1498();
void SquareAndScale_1499();
void SquareAndScale_1500();
void SquareAndScale_1501();
void SquareAndScale_1502();
void SquareAndScale_1503();
void SquareAndScale_1504();
void SquareAndScale_1505();
void SquareAndScale_1506();
void SquareAndScale_1507();
void SquareAndScale_1508();
void SquareAndScale_1509();
void SquareAndScale_1510();
void SquareAndScale_1511();
void SquareAndScale_1512();
void SquareAndScale_1513();
void SquareAndScale_1514();
void SquareAndScale_1515();
void SquareAndScale_1516();
void SquareAndScale_1517();
void SquareAndScale_1518();
void SquareAndScale_1519();
void SquareAndScale_1520();
void SquareAndScale_1521();
void SquareAndScale_1522();
void SquareAndScale_1523();
void WEIGHTED_ROUND_ROBIN_Joiner_1470();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_1466();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_1467();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1

#define __DEFLOOPBOUND__72__ -1

#define __DEFLOOPBOUND__73__ -1

#define __DEFLOOPBOUND__74__ -1

#define __DEFLOOPBOUND__75__ -1

#define __DEFLOOPBOUND__76__ -1

#define __DEFLOOPBOUND__77__ -1


#ifdef __cplusplus
}
#endif
#endif
