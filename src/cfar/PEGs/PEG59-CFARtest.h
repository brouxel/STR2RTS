#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3840 on the compile command line
#else
#if BUF_SIZEMAX < 3840
#error BUF_SIZEMAX too small, it must be at least 3840
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_713_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_716_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_713();
void WEIGHTED_ROUND_ROBIN_Splitter_719();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_721();
void SquareAndScale_722();
void SquareAndScale_723();
void SquareAndScale_724();
void SquareAndScale_725();
void SquareAndScale_726();
void SquareAndScale_727();
void SquareAndScale_728();
void SquareAndScale_729();
void SquareAndScale_730();
void SquareAndScale_731();
void SquareAndScale_732();
void SquareAndScale_733();
void SquareAndScale_734();
void SquareAndScale_735();
void SquareAndScale_736();
void SquareAndScale_737();
void SquareAndScale_738();
void SquareAndScale_739();
void SquareAndScale_740();
void SquareAndScale_741();
void SquareAndScale_742();
void SquareAndScale_743();
void SquareAndScale_744();
void SquareAndScale_745();
void SquareAndScale_746();
void SquareAndScale_747();
void SquareAndScale_748();
void SquareAndScale_749();
void SquareAndScale_750();
void SquareAndScale_751();
void SquareAndScale_752();
void SquareAndScale_753();
void SquareAndScale_754();
void SquareAndScale_755();
void SquareAndScale_756();
void SquareAndScale_757();
void SquareAndScale_758();
void SquareAndScale_759();
void SquareAndScale_760();
void SquareAndScale_761();
void SquareAndScale_762();
void SquareAndScale_763();
void SquareAndScale_764();
void SquareAndScale_765();
void SquareAndScale_766();
void SquareAndScale_767();
void SquareAndScale_768();
void SquareAndScale_769();
void SquareAndScale_770();
void SquareAndScale_771();
void SquareAndScale_772();
void SquareAndScale_773();
void SquareAndScale_774();
void SquareAndScale_775();
void SquareAndScale_776();
void SquareAndScale_777();
void SquareAndScale_778();
void SquareAndScale_779();
void WEIGHTED_ROUND_ROBIN_Joiner_720();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_716();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_717();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1


#ifdef __cplusplus
}
#endif
#endif
