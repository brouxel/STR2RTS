#include "PEG19-CFARtest_nocache.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_4380_4382_join[19];
buffer_float_t CFAR_gather_4356AnonFilter_a0_4357;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4380_4382_split[19];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4360CFAR_gather_4356;
buffer_complex_t ComplexSource_4353WEIGHTED_ROUND_ROBIN_Splitter_4359;


ComplexSource_4353_t ComplexSource_4353_s;
CFAR_gather_4356_t CFAR_gather_4356_s;

void ComplexSource_4353(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4353_s.theta = (ComplexSource_4353_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4353_s.theta)) * (((float) cos(ComplexSource_4353_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4353_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4353_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4353_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4353_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4353_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4353_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_4353WEIGHTED_ROUND_ROBIN_Splitter_4359, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_4361(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4362(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4363(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4364(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4365(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4366(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4367(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4368(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4369(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4370(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4371(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4372(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4373(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4374(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4375(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4376(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4377(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4378(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4379(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4359() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[__iter_], pop_complex(&ComplexSource_4353WEIGHTED_ROUND_ROBIN_Splitter_4359));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4360() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4360CFAR_gather_4356, pop_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4356(){
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4356_s.pos) - 5) >= 0)), __DEFLOOPBOUND__278__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4356_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4356_s.pos) < 64)), __DEFLOOPBOUND__279__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4360CFAR_gather_4356, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4356AnonFilter_a0_4357, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4356_s.poke[(i - 1)] = CFAR_gather_4356_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4356_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4360CFAR_gather_4356) ; 
		CFAR_gather_4356_s.pos++ ; 
		if(CFAR_gather_4356_s.pos == 64) {
			CFAR_gather_4356_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4357(){
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4356AnonFilter_a0_4357));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 19, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4356AnonFilter_a0_4357);
	FOR(int, __iter_init_1_, 0, <, 19, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4360CFAR_gather_4356);
	init_buffer_complex(&ComplexSource_4353WEIGHTED_ROUND_ROBIN_Splitter_4359);
// --- init: ComplexSource_4353
	 {
	ComplexSource_4353_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4353_s.theta = (ComplexSource_4353_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4353_s.theta)) * (((float) cos(ComplexSource_4353_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4353_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4353_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4353_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4353_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4353_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4353_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4353WEIGHTED_ROUND_ROBIN_Splitter_4359, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4359
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[__iter_], pop_complex(&ComplexSource_4353WEIGHTED_ROUND_ROBIN_Splitter_4359));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4361
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4362
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4363
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4364
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4365
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4366
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4367
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4368
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4369
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4370
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4371
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4372
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4373
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4374
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4375
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4376
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4377
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4378
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4379
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4380_4382_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4360
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4360CFAR_gather_4356, pop_float(&SplitJoin0_SquareAndScale_Fiss_4380_4382_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4356
	 {
	CFAR_gather_4356_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4356_s.pos) - 5) >= 0)), __DEFLOOPBOUND__280__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4356_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4356_s.pos) < 64)), __DEFLOOPBOUND__281__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4360CFAR_gather_4356, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4356AnonFilter_a0_4357, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4356_s.poke[(i - 1)] = CFAR_gather_4356_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4356_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4360CFAR_gather_4356) ; 
		CFAR_gather_4356_s.pos++ ; 
		if(CFAR_gather_4356_s.pos == 64) {
			CFAR_gather_4356_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4357
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4356AnonFilter_a0_4357));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4353();
		WEIGHTED_ROUND_ROBIN_Splitter_4359();
			SquareAndScale_4361();
			SquareAndScale_4362();
			SquareAndScale_4363();
			SquareAndScale_4364();
			SquareAndScale_4365();
			SquareAndScale_4366();
			SquareAndScale_4367();
			SquareAndScale_4368();
			SquareAndScale_4369();
			SquareAndScale_4370();
			SquareAndScale_4371();
			SquareAndScale_4372();
			SquareAndScale_4373();
			SquareAndScale_4374();
			SquareAndScale_4375();
			SquareAndScale_4376();
			SquareAndScale_4377();
			SquareAndScale_4378();
			SquareAndScale_4379();
		WEIGHTED_ROUND_ROBIN_Joiner_4360();
		CFAR_gather_4356();
		AnonFilter_a0_4357();
	ENDFOR
	return EXIT_SUCCESS;
}
