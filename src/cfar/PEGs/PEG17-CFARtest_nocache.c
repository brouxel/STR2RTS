#include "PEG17-CFARtest_nocache.h"

buffer_float_t CFAR_gather_4454AnonFilter_a0_4455;
buffer_complex_t ComplexSource_4451WEIGHTED_ROUND_ROBIN_Splitter_4457;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4476_4478_split[17];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4476_4478_join[17];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4458CFAR_gather_4454;


ComplexSource_4451_t ComplexSource_4451_s;
CFAR_gather_4454_t CFAR_gather_4454_s;

void ComplexSource_4451(){
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4451_s.theta = (ComplexSource_4451_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4451_s.theta)) * (((float) cos(ComplexSource_4451_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4451_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4451_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4451_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4451_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4451_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4451_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_4451WEIGHTED_ROUND_ROBIN_Splitter_4457, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_4459(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4460(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4461(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4462(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4463(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4464(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4465(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4466(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4467(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4468(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4469(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4470(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4471(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4472(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4473(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4474(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4475(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4457() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[__iter_], pop_complex(&ComplexSource_4451WEIGHTED_ROUND_ROBIN_Splitter_4457));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4458() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4458CFAR_gather_4454, pop_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4454(){
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4454_s.pos) - 5) >= 0)), __DEFLOOPBOUND__290__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4454_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4454_s.pos) < 64)), __DEFLOOPBOUND__291__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4458CFAR_gather_4454, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4454AnonFilter_a0_4455, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4454_s.poke[(i - 1)] = CFAR_gather_4454_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4454_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4458CFAR_gather_4454) ; 
		CFAR_gather_4454_s.pos++ ; 
		if(CFAR_gather_4454_s.pos == 64) {
			CFAR_gather_4454_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4455(){
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4454AnonFilter_a0_4455));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&CFAR_gather_4454AnonFilter_a0_4455);
	init_buffer_complex(&ComplexSource_4451WEIGHTED_ROUND_ROBIN_Splitter_4457);
	FOR(int, __iter_init_0_, 0, <, 17, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 17, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4458CFAR_gather_4454);
// --- init: ComplexSource_4451
	 {
	ComplexSource_4451_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4451_s.theta = (ComplexSource_4451_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4451_s.theta)) * (((float) cos(ComplexSource_4451_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4451_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4451_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4451_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4451_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4451_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4451_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4451WEIGHTED_ROUND_ROBIN_Splitter_4457, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4457
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[__iter_], pop_complex(&ComplexSource_4451WEIGHTED_ROUND_ROBIN_Splitter_4457));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4459
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4460
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4461
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4462
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4463
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4464
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4465
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4466
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4467
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4468
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4469
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4470
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4471
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4472
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4473
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4474
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4475
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4476_4478_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4458
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4458CFAR_gather_4454, pop_float(&SplitJoin0_SquareAndScale_Fiss_4476_4478_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4454
	 {
	CFAR_gather_4454_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 42, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4454_s.pos) - 5) >= 0)), __DEFLOOPBOUND__292__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4454_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4454_s.pos) < 64)), __DEFLOOPBOUND__293__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4458CFAR_gather_4454, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4454AnonFilter_a0_4455, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4454_s.poke[(i - 1)] = CFAR_gather_4454_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4454_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4458CFAR_gather_4454) ; 
		CFAR_gather_4454_s.pos++ ; 
		if(CFAR_gather_4454_s.pos == 64) {
			CFAR_gather_4454_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4455
	FOR(uint32_t, __iter_init_, 0, <, 42, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4454AnonFilter_a0_4455));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4451();
		WEIGHTED_ROUND_ROBIN_Splitter_4457();
			SquareAndScale_4459();
			SquareAndScale_4460();
			SquareAndScale_4461();
			SquareAndScale_4462();
			SquareAndScale_4463();
			SquareAndScale_4464();
			SquareAndScale_4465();
			SquareAndScale_4466();
			SquareAndScale_4467();
			SquareAndScale_4468();
			SquareAndScale_4469();
			SquareAndScale_4470();
			SquareAndScale_4471();
			SquareAndScale_4472();
			SquareAndScale_4473();
			SquareAndScale_4474();
			SquareAndScale_4475();
		WEIGHTED_ROUND_ROBIN_Joiner_4458();
		CFAR_gather_4454();
		AnonFilter_a0_4455();
	ENDFOR
	return EXIT_SUCCESS;
}
