#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=36366 on the compile command line
#else
#if BUF_SIZEMAX < 36366
#error BUF_SIZEMAX too small, it must be at least 36366
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_1697_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_1700_t;
void ComplexSource_1697();
void WEIGHTED_ROUND_ROBIN_Splitter_1703();
void SquareAndScale_1705();
void SquareAndScale_1706();
void SquareAndScale_1707();
void SquareAndScale_1708();
void SquareAndScale_1709();
void SquareAndScale_1710();
void SquareAndScale_1711();
void SquareAndScale_1712();
void SquareAndScale_1713();
void SquareAndScale_1714();
void SquareAndScale_1715();
void SquareAndScale_1716();
void SquareAndScale_1717();
void SquareAndScale_1718();
void SquareAndScale_1719();
void SquareAndScale_1720();
void SquareAndScale_1721();
void SquareAndScale_1722();
void SquareAndScale_1723();
void SquareAndScale_1724();
void SquareAndScale_1725();
void SquareAndScale_1726();
void SquareAndScale_1727();
void SquareAndScale_1728();
void SquareAndScale_1729();
void SquareAndScale_1730();
void SquareAndScale_1731();
void SquareAndScale_1732();
void SquareAndScale_1733();
void SquareAndScale_1734();
void SquareAndScale_1735();
void SquareAndScale_1736();
void SquareAndScale_1737();
void SquareAndScale_1738();
void SquareAndScale_1739();
void SquareAndScale_1740();
void SquareAndScale_1741();
void SquareAndScale_1742();
void SquareAndScale_1743();
void SquareAndScale_1744();
void SquareAndScale_1745();
void SquareAndScale_1746();
void SquareAndScale_1747();
void SquareAndScale_1748();
void SquareAndScale_1749();
void SquareAndScale_1750();
void SquareAndScale_1751();
void SquareAndScale_1752();
void SquareAndScale_1753();
void SquareAndScale_1754();
void SquareAndScale_1755();
void WEIGHTED_ROUND_ROBIN_Joiner_1704();
void CFAR_gather_1700();
void AnonFilter_a0_1701();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1

#define __DEFLOOPBOUND__72__ -1

#define __DEFLOOPBOUND__73__ -1

#define __DEFLOOPBOUND__74__ -1

#define __DEFLOOPBOUND__75__ -1

#define __DEFLOOPBOUND__76__ -1

#define __DEFLOOPBOUND__77__ -1

#define __DEFLOOPBOUND__78__ -1

#define __DEFLOOPBOUND__79__ -1

#define __DEFLOOPBOUND__80__ -1

#define __DEFLOOPBOUND__81__ -1

#define __DEFLOOPBOUND__82__ -1

#define __DEFLOOPBOUND__83__ -1

#define __DEFLOOPBOUND__84__ -1

#define __DEFLOOPBOUND__85__ -1

#define __DEFLOOPBOUND__86__ -1

#define __DEFLOOPBOUND__87__ -1

#define __DEFLOOPBOUND__88__ -1

#define __DEFLOOPBOUND__89__ -1


#ifdef __cplusplus
}
#endif
#endif
