#include "PEG31-CFARtest_nocache.h"

buffer_complex_t ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600;
buffer_float_t CFAR_gather_3600AnonFilter_a0_3601;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3636_3638_join[31];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3636_3638_split[31];


ComplexSource_3597_t ComplexSource_3597_s;
CFAR_gather_3600_t CFAR_gather_3600_s;

void ComplexSource_3597(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3597_s.theta = (ComplexSource_3597_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3597_s.theta)) * (((float) cos(ComplexSource_3597_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3597_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3597_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3597_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3597_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3597_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3597_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_3605(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3606(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3607(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3608(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3609(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3610(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3611(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3612(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3613(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3614(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3615(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3616(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3617(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3618(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3619(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3620(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3621(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3622(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3623(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3624(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3625(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3626(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[21]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[21], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3627(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[22]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[22], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3628(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[23]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[23], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3629(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[24]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[24], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3630(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[25]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[25], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3631(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[26]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[26], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3632(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[27]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[27], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3633(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[28]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[28], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3634(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[29]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[29], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3635(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[30]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[30], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3603() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[__iter_], pop_complex(&ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3604() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600, pop_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_3600(){
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3600_s.pos) - 5) >= 0)), __DEFLOOPBOUND__206__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3600_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3600_s.pos) < 64)), __DEFLOOPBOUND__207__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_3600AnonFilter_a0_3601, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3600_s.poke[(i - 1)] = CFAR_gather_3600_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3600_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600) ; 
		CFAR_gather_3600_s.pos++ ; 
		if(CFAR_gather_3600_s.pos == 64) {
			CFAR_gather_3600_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_3601(){
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_3600AnonFilter_a0_3601));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600);
	init_buffer_float(&CFAR_gather_3600AnonFilter_a0_3601);
	FOR(int, __iter_init_0_, 0, <, 31, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 31, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_3597
	 {
	ComplexSource_3597_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_3597_s.theta = (ComplexSource_3597_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_3597_s.theta)) * (((float) cos(ComplexSource_3597_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3597_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3597_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_3597_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3597_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3597_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3597_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3603
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[__iter_], pop_complex(&ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3605
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3606
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3607
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3608
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3609
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3610
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3611
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3612
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3613
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3614
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3615
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3616
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3617
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3618
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3619
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3620
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3621
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3622
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3623
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3624
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3625
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3626
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[21]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[21], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3627
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[22]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[22], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3628
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[23]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[23], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3629
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[24]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[24], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3630
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[25]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[25], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3631
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[26]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[26], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3632
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[27]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[27], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3633
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[28]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[28], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3634
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[29]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[29], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3635
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[30]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[30], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3604
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600, pop_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3600
	 {
	CFAR_gather_3600_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 53, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3600_s.pos) - 5) >= 0)), __DEFLOOPBOUND__208__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3600_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3600_s.pos) < 64)), __DEFLOOPBOUND__209__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_3600AnonFilter_a0_3601, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3600_s.poke[(i - 1)] = CFAR_gather_3600_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3600_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600) ; 
		CFAR_gather_3600_s.pos++ ; 
		if(CFAR_gather_3600_s.pos == 64) {
			CFAR_gather_3600_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3601
	FOR(uint32_t, __iter_init_, 0, <, 53, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_3600AnonFilter_a0_3601));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3597();
		WEIGHTED_ROUND_ROBIN_Splitter_3603();
			SquareAndScale_3605();
			SquareAndScale_3606();
			SquareAndScale_3607();
			SquareAndScale_3608();
			SquareAndScale_3609();
			SquareAndScale_3610();
			SquareAndScale_3611();
			SquareAndScale_3612();
			SquareAndScale_3613();
			SquareAndScale_3614();
			SquareAndScale_3615();
			SquareAndScale_3616();
			SquareAndScale_3617();
			SquareAndScale_3618();
			SquareAndScale_3619();
			SquareAndScale_3620();
			SquareAndScale_3621();
			SquareAndScale_3622();
			SquareAndScale_3623();
			SquareAndScale_3624();
			SquareAndScale_3625();
			SquareAndScale_3626();
			SquareAndScale_3627();
			SquareAndScale_3628();
			SquareAndScale_3629();
			SquareAndScale_3630();
			SquareAndScale_3631();
			SquareAndScale_3632();
			SquareAndScale_3633();
			SquareAndScale_3634();
			SquareAndScale_3635();
		WEIGHTED_ROUND_ROBIN_Joiner_3604();
		CFAR_gather_3600();
		AnonFilter_a0_3601();
	ENDFOR
	return EXIT_SUCCESS;
}
