#include "PEG6-CFARtest_nocache.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_4861_4863_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4854CFAR_gather_4850;
buffer_float_t CFAR_gather_4850AnonFilter_a0_4851;
buffer_complex_t ComplexSource_4847WEIGHTED_ROUND_ROBIN_Splitter_4853;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4861_4863_split[6];


ComplexSource_4847_t ComplexSource_4847_s;
CFAR_gather_4850_t CFAR_gather_4850_s;

void ComplexSource_4847(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4847_s.theta = (ComplexSource_4847_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4847_s.theta)) * (((float) cos(ComplexSource_4847_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4847_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4847_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4847_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4847_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4847_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4847_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_4847WEIGHTED_ROUND_ROBIN_Splitter_4853, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_4855(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4856(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4857(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4858(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4859(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4860(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4853() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[__iter_], pop_complex(&ComplexSource_4847WEIGHTED_ROUND_ROBIN_Splitter_4853));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4854() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854CFAR_gather_4850, pop_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4850(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4850_s.pos) - 5) >= 0)), __DEFLOOPBOUND__356__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4850_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4850_s.pos) < 64)), __DEFLOOPBOUND__357__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854CFAR_gather_4850, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4850AnonFilter_a0_4851, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4850_s.poke[(i - 1)] = CFAR_gather_4850_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4850_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854CFAR_gather_4850) ; 
		CFAR_gather_4850_s.pos++ ; 
		if(CFAR_gather_4850_s.pos == 64) {
			CFAR_gather_4850_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4851(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4850AnonFilter_a0_4851));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 6, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854CFAR_gather_4850);
	init_buffer_float(&CFAR_gather_4850AnonFilter_a0_4851);
	init_buffer_complex(&ComplexSource_4847WEIGHTED_ROUND_ROBIN_Splitter_4853);
	FOR(int, __iter_init_1_, 0, <, 6, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4847
	 {
	ComplexSource_4847_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4847_s.theta = (ComplexSource_4847_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4847_s.theta)) * (((float) cos(ComplexSource_4847_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4847_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4847_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4847_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4847_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4847_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4847_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4847WEIGHTED_ROUND_ROBIN_Splitter_4853, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4853
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[__iter_], pop_complex(&ComplexSource_4847WEIGHTED_ROUND_ROBIN_Splitter_4853));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4855
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4856
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4857
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4858
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4859
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4860
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4861_4863_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4854
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854CFAR_gather_4850, pop_float(&SplitJoin0_SquareAndScale_Fiss_4861_4863_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4850
	 {
	CFAR_gather_4850_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4850_s.pos) - 5) >= 0)), __DEFLOOPBOUND__358__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4850_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4850_s.pos) < 64)), __DEFLOOPBOUND__359__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854CFAR_gather_4850, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4850AnonFilter_a0_4851, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4850_s.poke[(i - 1)] = CFAR_gather_4850_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4850_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854CFAR_gather_4850) ; 
		CFAR_gather_4850_s.pos++ ; 
		if(CFAR_gather_4850_s.pos == 64) {
			CFAR_gather_4850_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4851
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4850AnonFilter_a0_4851));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4847();
		WEIGHTED_ROUND_ROBIN_Splitter_4853();
			SquareAndScale_4855();
			SquareAndScale_4856();
			SquareAndScale_4857();
			SquareAndScale_4858();
			SquareAndScale_4859();
			SquareAndScale_4860();
		WEIGHTED_ROUND_ROBIN_Joiner_4854();
		CFAR_gather_4850();
		AnonFilter_a0_4851();
	ENDFOR
	return EXIT_SUCCESS;
}
