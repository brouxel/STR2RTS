#include "PEG35-CFARtest.h"

buffer_complex_t ComplexSource_3281WEIGHTED_ROUND_ROBIN_Splitter_3287;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3288CFAR_gather_3284;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3324_3326_split[35];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3324_3326_join[35];
buffer_float_t CFAR_gather_3284AnonFilter_a0_3285;


ComplexSource_3281_t ComplexSource_3281_s;
CFAR_gather_3284_t CFAR_gather_3284_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3281_s.theta = (ComplexSource_3281_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3281_s.theta)) * (((float) cos(ComplexSource_3281_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3281_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3281_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3281_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3281_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3281_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3281_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3281() {
	FOR(uint32_t, __iter_steady_, 0, <, 35, __iter_steady_++)
		ComplexSource(&(ComplexSource_3281WEIGHTED_ROUND_ROBIN_Splitter_3287));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3289() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[0]));
	ENDFOR
}

void SquareAndScale_3290() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[1]));
	ENDFOR
}

void SquareAndScale_3291() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[2]));
	ENDFOR
}

void SquareAndScale_3292() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[3]));
	ENDFOR
}

void SquareAndScale_3293() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[4]));
	ENDFOR
}

void SquareAndScale_3294() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[5]));
	ENDFOR
}

void SquareAndScale_3295() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[6]));
	ENDFOR
}

void SquareAndScale_3296() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[7]));
	ENDFOR
}

void SquareAndScale_3297() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[8]));
	ENDFOR
}

void SquareAndScale_3298() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[9]));
	ENDFOR
}

void SquareAndScale_3299() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[10]));
	ENDFOR
}

void SquareAndScale_3300() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[11]));
	ENDFOR
}

void SquareAndScale_3301() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[12]));
	ENDFOR
}

void SquareAndScale_3302() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[13]));
	ENDFOR
}

void SquareAndScale_3303() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[14]));
	ENDFOR
}

void SquareAndScale_3304() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[15]));
	ENDFOR
}

void SquareAndScale_3305() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[16]));
	ENDFOR
}

void SquareAndScale_3306() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[17]));
	ENDFOR
}

void SquareAndScale_3307() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[18]));
	ENDFOR
}

void SquareAndScale_3308() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[19]));
	ENDFOR
}

void SquareAndScale_3309() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[20]));
	ENDFOR
}

void SquareAndScale_3310() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[21]));
	ENDFOR
}

void SquareAndScale_3311() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[22]));
	ENDFOR
}

void SquareAndScale_3312() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[23]));
	ENDFOR
}

void SquareAndScale_3313() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[24]));
	ENDFOR
}

void SquareAndScale_3314() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[25]));
	ENDFOR
}

void SquareAndScale_3315() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[26]));
	ENDFOR
}

void SquareAndScale_3316() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[27]));
	ENDFOR
}

void SquareAndScale_3317() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[28]));
	ENDFOR
}

void SquareAndScale_3318() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[29]));
	ENDFOR
}

void SquareAndScale_3319() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[30]));
	ENDFOR
}

void SquareAndScale_3320() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[31]));
	ENDFOR
}

void SquareAndScale_3321() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[32]));
	ENDFOR
}

void SquareAndScale_3322() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[33]));
	ENDFOR
}

void SquareAndScale_3323() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[34]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[34]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3287() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 35, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3324_3326_split[__iter_], pop_complex(&ComplexSource_3281WEIGHTED_ROUND_ROBIN_Splitter_3287));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3288() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 35, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3288CFAR_gather_3284, pop_float(&SplitJoin0_SquareAndScale_Fiss_3324_3326_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3284_s.pos) - 5) >= 0)), __DEFLOOPBOUND__182__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3284_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3284_s.pos) < 64)), __DEFLOOPBOUND__183__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3284_s.poke[(i - 1)] = CFAR_gather_3284_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3284_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3284_s.pos++ ; 
		if(CFAR_gather_3284_s.pos == 64) {
			CFAR_gather_3284_s.pos = 0 ; 
		}
	}


void CFAR_gather_3284() {
	FOR(uint32_t, __iter_steady_, 0, <, 2240, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3288CFAR_gather_3284), &(CFAR_gather_3284AnonFilter_a0_3285));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3285() {
	FOR(uint32_t, __iter_steady_, 0, <, 2240, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3284AnonFilter_a0_3285));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_3281WEIGHTED_ROUND_ROBIN_Splitter_3287);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3288CFAR_gather_3284);
	FOR(int, __iter_init_0_, 0, <, 35, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3324_3326_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 35, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3324_3326_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_3284AnonFilter_a0_3285);
// --- init: ComplexSource_3281
	 {
	ComplexSource_3281_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3281WEIGHTED_ROUND_ROBIN_Splitter_3287));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3287
	
	FOR(uint32_t, __iter_, 0, <, 35, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_3324_3326_split[__iter_], pop_complex(&ComplexSource_3281WEIGHTED_ROUND_ROBIN_Splitter_3287));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3289
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[0]));
//--------------------------------
// --- init: SquareAndScale_3290
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[1]));
//--------------------------------
// --- init: SquareAndScale_3291
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[2]));
//--------------------------------
// --- init: SquareAndScale_3292
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[3]));
//--------------------------------
// --- init: SquareAndScale_3293
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[4]));
//--------------------------------
// --- init: SquareAndScale_3294
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[5]));
//--------------------------------
// --- init: SquareAndScale_3295
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[6]));
//--------------------------------
// --- init: SquareAndScale_3296
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[7]));
//--------------------------------
// --- init: SquareAndScale_3297
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[8]));
//--------------------------------
// --- init: SquareAndScale_3298
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[9]));
//--------------------------------
// --- init: SquareAndScale_3299
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[10]));
//--------------------------------
// --- init: SquareAndScale_3300
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[11]));
//--------------------------------
// --- init: SquareAndScale_3301
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[12]));
//--------------------------------
// --- init: SquareAndScale_3302
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[13]));
//--------------------------------
// --- init: SquareAndScale_3303
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[14]));
//--------------------------------
// --- init: SquareAndScale_3304
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[15]));
//--------------------------------
// --- init: SquareAndScale_3305
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[16]));
//--------------------------------
// --- init: SquareAndScale_3306
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[17]));
//--------------------------------
// --- init: SquareAndScale_3307
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[18]));
//--------------------------------
// --- init: SquareAndScale_3308
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[19]));
//--------------------------------
// --- init: SquareAndScale_3309
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[20]));
//--------------------------------
// --- init: SquareAndScale_3310
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[21]));
//--------------------------------
// --- init: SquareAndScale_3311
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[22]));
//--------------------------------
// --- init: SquareAndScale_3312
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[23]));
//--------------------------------
// --- init: SquareAndScale_3313
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[24]));
//--------------------------------
// --- init: SquareAndScale_3314
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[25]));
//--------------------------------
// --- init: SquareAndScale_3315
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[26]));
//--------------------------------
// --- init: SquareAndScale_3316
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[27]));
//--------------------------------
// --- init: SquareAndScale_3317
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[28]));
//--------------------------------
// --- init: SquareAndScale_3318
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[29]));
//--------------------------------
// --- init: SquareAndScale_3319
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[30]));
//--------------------------------
// --- init: SquareAndScale_3320
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[31]));
//--------------------------------
// --- init: SquareAndScale_3321
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[32]));
//--------------------------------
// --- init: SquareAndScale_3322
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[33]));
//--------------------------------
// --- init: SquareAndScale_3323
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3324_3326_split[34]), &(SplitJoin0_SquareAndScale_Fiss_3324_3326_join[34]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3288
	
	FOR(uint32_t, __iter_, 0, <, 35, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3288CFAR_gather_3284, pop_float(&SplitJoin0_SquareAndScale_Fiss_3324_3326_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3284
	 {
	CFAR_gather_3284_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 26, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3288CFAR_gather_3284), &(CFAR_gather_3284AnonFilter_a0_3285));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3285
	FOR(uint32_t, __iter_init_, 0, <, 26, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3284AnonFilter_a0_3285));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3281();
		WEIGHTED_ROUND_ROBIN_Splitter_3287();
			SquareAndScale_3289();
			SquareAndScale_3290();
			SquareAndScale_3291();
			SquareAndScale_3292();
			SquareAndScale_3293();
			SquareAndScale_3294();
			SquareAndScale_3295();
			SquareAndScale_3296();
			SquareAndScale_3297();
			SquareAndScale_3298();
			SquareAndScale_3299();
			SquareAndScale_3300();
			SquareAndScale_3301();
			SquareAndScale_3302();
			SquareAndScale_3303();
			SquareAndScale_3304();
			SquareAndScale_3305();
			SquareAndScale_3306();
			SquareAndScale_3307();
			SquareAndScale_3308();
			SquareAndScale_3309();
			SquareAndScale_3310();
			SquareAndScale_3311();
			SquareAndScale_3312();
			SquareAndScale_3313();
			SquareAndScale_3314();
			SquareAndScale_3315();
			SquareAndScale_3316();
			SquareAndScale_3317();
			SquareAndScale_3318();
			SquareAndScale_3319();
			SquareAndScale_3320();
			SquareAndScale_3321();
			SquareAndScale_3322();
			SquareAndScale_3323();
		WEIGHTED_ROUND_ROBIN_Joiner_3288();
		CFAR_gather_3284();
		AnonFilter_a0_3285();
	ENDFOR
	return EXIT_SUCCESS;
}
