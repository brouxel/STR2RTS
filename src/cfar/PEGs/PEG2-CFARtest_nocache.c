#include "PEG2-CFARtest_nocache.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4941_4943_split[2];
buffer_float_t CFAR_gather_4934AnonFilter_a0_4935;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4941_4943_join[2];
buffer_complex_t ComplexSource_4931WEIGHTED_ROUND_ROBIN_Splitter_4937;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934;


ComplexSource_4931_t ComplexSource_4931_s;
CFAR_gather_4934_t CFAR_gather_4934_s;

void ComplexSource_4931() {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4931_s.theta = (ComplexSource_4931_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4931_s.theta)) * (((float) cos(ComplexSource_4931_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4931_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4931_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4931_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4931_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4931_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4931_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4931WEIGHTED_ROUND_ROBIN_Splitter_4937, c) ; 
	}
	ENDFOR
}


void SquareAndScale_4939(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4940(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4937() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[0], pop_complex(&ComplexSource_4931WEIGHTED_ROUND_ROBIN_Splitter_4937));
		push_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[1], pop_complex(&ComplexSource_4931WEIGHTED_ROUND_ROBIN_Splitter_4937));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4938() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934, pop_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934, pop_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[1]));
	ENDFOR
}}

void CFAR_gather_4934(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4934_s.pos) - 5) >= 0)), __DEFLOOPBOUND__380__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4934_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4934_s.pos) < 64)), __DEFLOOPBOUND__381__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4934AnonFilter_a0_4935, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4934_s.poke[(i - 1)] = CFAR_gather_4934_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4934_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934) ; 
		CFAR_gather_4934_s.pos++ ; 
		if(CFAR_gather_4934_s.pos == 64) {
			CFAR_gather_4934_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4935(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4934AnonFilter_a0_4935));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4934AnonFilter_a0_4935);
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_4931WEIGHTED_ROUND_ROBIN_Splitter_4937);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934);
// --- init: ComplexSource_4931
	 {
	ComplexSource_4931_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4931_s.theta = (ComplexSource_4931_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4931_s.theta)) * (((float) cos(ComplexSource_4931_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4931_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4931_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4931_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4931_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4931_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4931_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4931WEIGHTED_ROUND_ROBIN_Splitter_4937, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4937
	FOR(uint32_t, __iter_init_, 0, <, 32, __iter_init_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[0], pop_complex(&ComplexSource_4931WEIGHTED_ROUND_ROBIN_Splitter_4937));
		push_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[1], pop_complex(&ComplexSource_4931WEIGHTED_ROUND_ROBIN_Splitter_4937));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4939
	FOR(uint32_t, __iter_init_, 0, <, 32, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4940
	FOR(uint32_t, __iter_init_, 0, <, 32, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4941_4943_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4938
	FOR(uint32_t, __iter_init_, 0, <, 32, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934, pop_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934, pop_float(&SplitJoin0_SquareAndScale_Fiss_4941_4943_join[1]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4934
	 {
	CFAR_gather_4934_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4934_s.pos) - 5) >= 0)), __DEFLOOPBOUND__382__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4934_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4934_s.pos) < 64)), __DEFLOOPBOUND__383__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4934AnonFilter_a0_4935, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4934_s.poke[(i - 1)] = CFAR_gather_4934_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4934_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4938CFAR_gather_4934) ; 
		CFAR_gather_4934_s.pos++ ; 
		if(CFAR_gather_4934_s.pos == 64) {
			CFAR_gather_4934_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4935
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4934AnonFilter_a0_4935));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4931();
		WEIGHTED_ROUND_ROBIN_Splitter_4937();
			SquareAndScale_4939();
			SquareAndScale_4940();
		WEIGHTED_ROUND_ROBIN_Joiner_4938();
		CFAR_gather_4934();
		AnonFilter_a0_4935();
	ENDFOR
	return EXIT_SUCCESS;
}
