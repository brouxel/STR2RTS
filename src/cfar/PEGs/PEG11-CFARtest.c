#include "PEG11-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4704CFAR_gather_4700;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4716_4718_join[11];
buffer_float_t CFAR_gather_4700AnonFilter_a0_4701;
buffer_complex_t ComplexSource_4697WEIGHTED_ROUND_ROBIN_Splitter_4703;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4716_4718_split[11];


ComplexSource_4697_t ComplexSource_4697_s;
CFAR_gather_4700_t CFAR_gather_4700_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4697_s.theta = (ComplexSource_4697_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4697_s.theta)) * (((float) cos(ComplexSource_4697_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4697_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4697_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4697_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4697_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4697_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4697_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4697() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		ComplexSource(&(ComplexSource_4697WEIGHTED_ROUND_ROBIN_Splitter_4703));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4705() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[0]));
	ENDFOR
}

void SquareAndScale_4706() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[1]));
	ENDFOR
}

void SquareAndScale_4707() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[2]));
	ENDFOR
}

void SquareAndScale_4708() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[3]));
	ENDFOR
}

void SquareAndScale_4709() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[4]));
	ENDFOR
}

void SquareAndScale_4710() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[5]));
	ENDFOR
}

void SquareAndScale_4711() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[6]));
	ENDFOR
}

void SquareAndScale_4712() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[7]));
	ENDFOR
}

void SquareAndScale_4713() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[8]));
	ENDFOR
}

void SquareAndScale_4714() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[9]));
	ENDFOR
}

void SquareAndScale_4715() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4703() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4716_4718_split[__iter_], pop_complex(&ComplexSource_4697WEIGHTED_ROUND_ROBIN_Splitter_4703));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4704() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4704CFAR_gather_4700, pop_float(&SplitJoin0_SquareAndScale_Fiss_4716_4718_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4700_s.pos) - 5) >= 0)), __DEFLOOPBOUND__326__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4700_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4700_s.pos) < 64)), __DEFLOOPBOUND__327__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4700_s.poke[(i - 1)] = CFAR_gather_4700_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4700_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4700_s.pos++ ; 
		if(CFAR_gather_4700_s.pos == 64) {
			CFAR_gather_4700_s.pos = 0 ; 
		}
	}


void CFAR_gather_4700() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4704CFAR_gather_4700), &(CFAR_gather_4700AnonFilter_a0_4701));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4701() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4700AnonFilter_a0_4701));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4704CFAR_gather_4700);
	FOR(int, __iter_init_0_, 0, <, 11, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4716_4718_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4700AnonFilter_a0_4701);
	init_buffer_complex(&ComplexSource_4697WEIGHTED_ROUND_ROBIN_Splitter_4703);
	FOR(int, __iter_init_1_, 0, <, 11, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4716_4718_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4697
	 {
	ComplexSource_4697_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4697WEIGHTED_ROUND_ROBIN_Splitter_4703));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4703
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4716_4718_split[__iter_], pop_complex(&ComplexSource_4697WEIGHTED_ROUND_ROBIN_Splitter_4703));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4705
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4706
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4707
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4708
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4709
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4710
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4711
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4712
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4713
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4714
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4715
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4716_4718_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4716_4718_join[10]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4704
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4704CFAR_gather_4700, pop_float(&SplitJoin0_SquareAndScale_Fiss_4716_4718_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4700
	 {
	CFAR_gather_4700_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4704CFAR_gather_4700), &(CFAR_gather_4700AnonFilter_a0_4701));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4701
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4700AnonFilter_a0_4701));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4697();
		WEIGHTED_ROUND_ROBIN_Splitter_4703();
			SquareAndScale_4705();
			SquareAndScale_4706();
			SquareAndScale_4707();
			SquareAndScale_4708();
			SquareAndScale_4709();
			SquareAndScale_4710();
			SquareAndScale_4711();
			SquareAndScale_4712();
			SquareAndScale_4713();
			SquareAndScale_4714();
			SquareAndScale_4715();
		WEIGHTED_ROUND_ROBIN_Joiner_4704();
		CFAR_gather_4700();
		AnonFilter_a0_4701();
	ENDFOR
	return EXIT_SUCCESS;
}
