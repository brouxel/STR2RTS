#include "PEG20-CFARtest.h"

buffer_complex_t ComplexSource_4301WEIGHTED_ROUND_ROBIN_Splitter_4307;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4329_4331_join[20];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4308CFAR_gather_4304;
buffer_float_t CFAR_gather_4304AnonFilter_a0_4305;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4329_4331_split[20];


ComplexSource_4301_t ComplexSource_4301_s;
CFAR_gather_4304_t CFAR_gather_4304_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4301_s.theta = (ComplexSource_4301_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4301_s.theta)) * (((float) cos(ComplexSource_4301_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4301_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4301_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4301_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4301_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4301_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4301_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4301() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		ComplexSource(&(ComplexSource_4301WEIGHTED_ROUND_ROBIN_Splitter_4307));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4309() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[0]));
	ENDFOR
}

void SquareAndScale_4310() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[1]));
	ENDFOR
}

void SquareAndScale_4311() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[2]));
	ENDFOR
}

void SquareAndScale_4312() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[3]));
	ENDFOR
}

void SquareAndScale_4313() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[4]));
	ENDFOR
}

void SquareAndScale_4314() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[5]));
	ENDFOR
}

void SquareAndScale_4315() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[6]));
	ENDFOR
}

void SquareAndScale_4316() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[7]));
	ENDFOR
}

void SquareAndScale_4317() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[8]));
	ENDFOR
}

void SquareAndScale_4318() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[9]));
	ENDFOR
}

void SquareAndScale_4319() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[10]));
	ENDFOR
}

void SquareAndScale_4320() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[11]));
	ENDFOR
}

void SquareAndScale_4321() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[12]));
	ENDFOR
}

void SquareAndScale_4322() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[13]));
	ENDFOR
}

void SquareAndScale_4323() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[14]));
	ENDFOR
}

void SquareAndScale_4324() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[15]));
	ENDFOR
}

void SquareAndScale_4325() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[16]));
	ENDFOR
}

void SquareAndScale_4326() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[17]));
	ENDFOR
}

void SquareAndScale_4327() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[18]));
	ENDFOR
}

void SquareAndScale_4328() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[19]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4307() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4329_4331_split[__iter_], pop_complex(&ComplexSource_4301WEIGHTED_ROUND_ROBIN_Splitter_4307));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4308() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4308CFAR_gather_4304, pop_float(&SplitJoin0_SquareAndScale_Fiss_4329_4331_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4304_s.pos) - 5) >= 0)), __DEFLOOPBOUND__272__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4304_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4304_s.pos) < 64)), __DEFLOOPBOUND__273__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4304_s.poke[(i - 1)] = CFAR_gather_4304_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4304_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4304_s.pos++ ; 
		if(CFAR_gather_4304_s.pos == 64) {
			CFAR_gather_4304_s.pos = 0 ; 
		}
	}


void CFAR_gather_4304() {
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4308CFAR_gather_4304), &(CFAR_gather_4304AnonFilter_a0_4305));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4305() {
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4304AnonFilter_a0_4305));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_4301WEIGHTED_ROUND_ROBIN_Splitter_4307);
	FOR(int, __iter_init_0_, 0, <, 20, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4329_4331_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4308CFAR_gather_4304);
	init_buffer_float(&CFAR_gather_4304AnonFilter_a0_4305);
	FOR(int, __iter_init_1_, 0, <, 20, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4329_4331_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4301
	 {
	ComplexSource_4301_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4301WEIGHTED_ROUND_ROBIN_Splitter_4307));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4307
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4329_4331_split[__iter_], pop_complex(&ComplexSource_4301WEIGHTED_ROUND_ROBIN_Splitter_4307));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4309
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4310
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4311
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4312
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4313
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4314
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4315
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4316
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4317
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4318
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4319
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4320
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4321
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4322
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4323
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4324
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4325
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4326
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4327
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4328
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4329_4331_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4329_4331_join[19]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4308
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4308CFAR_gather_4304, pop_float(&SplitJoin0_SquareAndScale_Fiss_4329_4331_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4304
	 {
	CFAR_gather_4304_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4308CFAR_gather_4304), &(CFAR_gather_4304AnonFilter_a0_4305));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4305
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4304AnonFilter_a0_4305));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4301();
		WEIGHTED_ROUND_ROBIN_Splitter_4307();
			SquareAndScale_4309();
			SquareAndScale_4310();
			SquareAndScale_4311();
			SquareAndScale_4312();
			SquareAndScale_4313();
			SquareAndScale_4314();
			SquareAndScale_4315();
			SquareAndScale_4316();
			SquareAndScale_4317();
			SquareAndScale_4318();
			SquareAndScale_4319();
			SquareAndScale_4320();
			SquareAndScale_4321();
			SquareAndScale_4322();
			SquareAndScale_4323();
			SquareAndScale_4324();
			SquareAndScale_4325();
			SquareAndScale_4326();
			SquareAndScale_4327();
			SquareAndScale_4328();
		WEIGHTED_ROUND_ROBIN_Joiner_4308();
		CFAR_gather_4304();
		AnonFilter_a0_4305();
	ENDFOR
	return EXIT_SUCCESS;
}
