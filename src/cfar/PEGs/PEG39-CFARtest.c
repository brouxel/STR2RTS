#include "PEG39-CFARtest.h"

buffer_float_t CFAR_gather_2936AnonFilter_a0_2937;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2980_2982_split[39];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_2980_2982_join[39];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2940CFAR_gather_2936;
buffer_complex_t ComplexSource_2933WEIGHTED_ROUND_ROBIN_Splitter_2939;


ComplexSource_2933_t ComplexSource_2933_s;
CFAR_gather_2936_t CFAR_gather_2936_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2933_s.theta = (ComplexSource_2933_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2933_s.theta)) * (((float) cos(ComplexSource_2933_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2933_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2933_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2933_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2933_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2933_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2933_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2933() {
	FOR(uint32_t, __iter_steady_, 0, <, 39, __iter_steady_++)
		ComplexSource(&(ComplexSource_2933WEIGHTED_ROUND_ROBIN_Splitter_2939));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2941() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[0]));
	ENDFOR
}

void SquareAndScale_2942() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[1]));
	ENDFOR
}

void SquareAndScale_2943() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[2]));
	ENDFOR
}

void SquareAndScale_2944() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[3]));
	ENDFOR
}

void SquareAndScale_2945() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[4]));
	ENDFOR
}

void SquareAndScale_2946() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[5]));
	ENDFOR
}

void SquareAndScale_2947() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[6]));
	ENDFOR
}

void SquareAndScale_2948() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[7]));
	ENDFOR
}

void SquareAndScale_2949() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[8]));
	ENDFOR
}

void SquareAndScale_2950() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[9]));
	ENDFOR
}

void SquareAndScale_2951() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[10]));
	ENDFOR
}

void SquareAndScale_2952() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[11]));
	ENDFOR
}

void SquareAndScale_2953() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[12]));
	ENDFOR
}

void SquareAndScale_2954() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[13]));
	ENDFOR
}

void SquareAndScale_2955() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[14]));
	ENDFOR
}

void SquareAndScale_2956() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[15]));
	ENDFOR
}

void SquareAndScale_2957() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[16]));
	ENDFOR
}

void SquareAndScale_2958() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[17]));
	ENDFOR
}

void SquareAndScale_2959() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[18]));
	ENDFOR
}

void SquareAndScale_2960() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[19]));
	ENDFOR
}

void SquareAndScale_2961() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[20]));
	ENDFOR
}

void SquareAndScale_2962() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[21]));
	ENDFOR
}

void SquareAndScale_2963() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[22]));
	ENDFOR
}

void SquareAndScale_2964() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[23]));
	ENDFOR
}

void SquareAndScale_2965() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[24]));
	ENDFOR
}

void SquareAndScale_2966() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[25]));
	ENDFOR
}

void SquareAndScale_2967() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[26]));
	ENDFOR
}

void SquareAndScale_2968() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[27]));
	ENDFOR
}

void SquareAndScale_2969() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[28]));
	ENDFOR
}

void SquareAndScale_2970() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[29]));
	ENDFOR
}

void SquareAndScale_2971() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[30]));
	ENDFOR
}

void SquareAndScale_2972() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[31]));
	ENDFOR
}

void SquareAndScale_2973() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[32]));
	ENDFOR
}

void SquareAndScale_2974() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[33]));
	ENDFOR
}

void SquareAndScale_2975() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[34]));
	ENDFOR
}

void SquareAndScale_2976() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[35]));
	ENDFOR
}

void SquareAndScale_2977() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[36]));
	ENDFOR
}

void SquareAndScale_2978() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[37]));
	ENDFOR
}

void SquareAndScale_2979() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[38]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2939() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 39, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2980_2982_split[__iter_], pop_complex(&ComplexSource_2933WEIGHTED_ROUND_ROBIN_Splitter_2939));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2940() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 39, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2940CFAR_gather_2936, pop_float(&SplitJoin0_SquareAndScale_Fiss_2980_2982_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2936_s.pos) - 5) >= 0)), __DEFLOOPBOUND__158__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2936_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2936_s.pos) < 64)), __DEFLOOPBOUND__159__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2936_s.poke[(i - 1)] = CFAR_gather_2936_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2936_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2936_s.pos++ ; 
		if(CFAR_gather_2936_s.pos == 64) {
			CFAR_gather_2936_s.pos = 0 ; 
		}
	}


void CFAR_gather_2936() {
	FOR(uint32_t, __iter_steady_, 0, <, 2496, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2940CFAR_gather_2936), &(CFAR_gather_2936AnonFilter_a0_2937));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2937() {
	FOR(uint32_t, __iter_steady_, 0, <, 2496, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2936AnonFilter_a0_2937));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&CFAR_gather_2936AnonFilter_a0_2937);
	FOR(int, __iter_init_0_, 0, <, 39, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2980_2982_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 39, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2980_2982_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2940CFAR_gather_2936);
	init_buffer_complex(&ComplexSource_2933WEIGHTED_ROUND_ROBIN_Splitter_2939);
// --- init: ComplexSource_2933
	 {
	ComplexSource_2933_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2933WEIGHTED_ROUND_ROBIN_Splitter_2939));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2939
	
	FOR(uint32_t, __iter_, 0, <, 39, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2980_2982_split[__iter_], pop_complex(&ComplexSource_2933WEIGHTED_ROUND_ROBIN_Splitter_2939));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2941
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[0]));
//--------------------------------
// --- init: SquareAndScale_2942
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[1]));
//--------------------------------
// --- init: SquareAndScale_2943
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[2]));
//--------------------------------
// --- init: SquareAndScale_2944
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[3]));
//--------------------------------
// --- init: SquareAndScale_2945
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[4]));
//--------------------------------
// --- init: SquareAndScale_2946
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[5]));
//--------------------------------
// --- init: SquareAndScale_2947
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[6]));
//--------------------------------
// --- init: SquareAndScale_2948
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[7]));
//--------------------------------
// --- init: SquareAndScale_2949
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[8]));
//--------------------------------
// --- init: SquareAndScale_2950
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[9]));
//--------------------------------
// --- init: SquareAndScale_2951
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[10]));
//--------------------------------
// --- init: SquareAndScale_2952
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[11]));
//--------------------------------
// --- init: SquareAndScale_2953
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[12]));
//--------------------------------
// --- init: SquareAndScale_2954
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[13]));
//--------------------------------
// --- init: SquareAndScale_2955
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[14]));
//--------------------------------
// --- init: SquareAndScale_2956
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[15]));
//--------------------------------
// --- init: SquareAndScale_2957
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[16]));
//--------------------------------
// --- init: SquareAndScale_2958
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[17]));
//--------------------------------
// --- init: SquareAndScale_2959
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[18]));
//--------------------------------
// --- init: SquareAndScale_2960
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[19]));
//--------------------------------
// --- init: SquareAndScale_2961
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[20]));
//--------------------------------
// --- init: SquareAndScale_2962
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[21]));
//--------------------------------
// --- init: SquareAndScale_2963
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[22]));
//--------------------------------
// --- init: SquareAndScale_2964
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[23]));
//--------------------------------
// --- init: SquareAndScale_2965
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[24]));
//--------------------------------
// --- init: SquareAndScale_2966
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[25]));
//--------------------------------
// --- init: SquareAndScale_2967
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[26]));
//--------------------------------
// --- init: SquareAndScale_2968
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[27]));
//--------------------------------
// --- init: SquareAndScale_2969
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[28]));
//--------------------------------
// --- init: SquareAndScale_2970
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[29]));
//--------------------------------
// --- init: SquareAndScale_2971
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[30]));
//--------------------------------
// --- init: SquareAndScale_2972
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[31]));
//--------------------------------
// --- init: SquareAndScale_2973
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[32]));
//--------------------------------
// --- init: SquareAndScale_2974
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[33]));
//--------------------------------
// --- init: SquareAndScale_2975
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[34]));
//--------------------------------
// --- init: SquareAndScale_2976
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[35]));
//--------------------------------
// --- init: SquareAndScale_2977
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[36]));
//--------------------------------
// --- init: SquareAndScale_2978
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[37]));
//--------------------------------
// --- init: SquareAndScale_2979
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2980_2982_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2980_2982_join[38]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2940
	
	FOR(uint32_t, __iter_, 0, <, 39, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2940CFAR_gather_2936, pop_float(&SplitJoin0_SquareAndScale_Fiss_2980_2982_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2936
	 {
	CFAR_gather_2936_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2940CFAR_gather_2936), &(CFAR_gather_2936AnonFilter_a0_2937));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2937
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2936AnonFilter_a0_2937));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2933();
		WEIGHTED_ROUND_ROBIN_Splitter_2939();
			SquareAndScale_2941();
			SquareAndScale_2942();
			SquareAndScale_2943();
			SquareAndScale_2944();
			SquareAndScale_2945();
			SquareAndScale_2946();
			SquareAndScale_2947();
			SquareAndScale_2948();
			SquareAndScale_2949();
			SquareAndScale_2950();
			SquareAndScale_2951();
			SquareAndScale_2952();
			SquareAndScale_2953();
			SquareAndScale_2954();
			SquareAndScale_2955();
			SquareAndScale_2956();
			SquareAndScale_2957();
			SquareAndScale_2958();
			SquareAndScale_2959();
			SquareAndScale_2960();
			SquareAndScale_2961();
			SquareAndScale_2962();
			SquareAndScale_2963();
			SquareAndScale_2964();
			SquareAndScale_2965();
			SquareAndScale_2966();
			SquareAndScale_2967();
			SquareAndScale_2968();
			SquareAndScale_2969();
			SquareAndScale_2970();
			SquareAndScale_2971();
			SquareAndScale_2972();
			SquareAndScale_2973();
			SquareAndScale_2974();
			SquareAndScale_2975();
			SquareAndScale_2976();
			SquareAndScale_2977();
			SquareAndScale_2978();
			SquareAndScale_2979();
		WEIGHTED_ROUND_ROBIN_Joiner_2940();
		CFAR_gather_2936();
		AnonFilter_a0_2937();
	ENDFOR
	return EXIT_SUCCESS;
}
