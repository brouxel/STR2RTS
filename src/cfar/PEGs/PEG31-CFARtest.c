#include "PEG31-CFARtest.h"

buffer_complex_t ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600;
buffer_float_t CFAR_gather_3600AnonFilter_a0_3601;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3636_3638_join[31];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3636_3638_split[31];


ComplexSource_3597_t ComplexSource_3597_s;
CFAR_gather_3600_t CFAR_gather_3600_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3597_s.theta = (ComplexSource_3597_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3597_s.theta)) * (((float) cos(ComplexSource_3597_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3597_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3597_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3597_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3597_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3597_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3597_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3597() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		ComplexSource(&(ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3605() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[0]));
	ENDFOR
}

void SquareAndScale_3606() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[1]));
	ENDFOR
}

void SquareAndScale_3607() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[2]));
	ENDFOR
}

void SquareAndScale_3608() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[3]));
	ENDFOR
}

void SquareAndScale_3609() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[4]));
	ENDFOR
}

void SquareAndScale_3610() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[5]));
	ENDFOR
}

void SquareAndScale_3611() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[6]));
	ENDFOR
}

void SquareAndScale_3612() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[7]));
	ENDFOR
}

void SquareAndScale_3613() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[8]));
	ENDFOR
}

void SquareAndScale_3614() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[9]));
	ENDFOR
}

void SquareAndScale_3615() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[10]));
	ENDFOR
}

void SquareAndScale_3616() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[11]));
	ENDFOR
}

void SquareAndScale_3617() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[12]));
	ENDFOR
}

void SquareAndScale_3618() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[13]));
	ENDFOR
}

void SquareAndScale_3619() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[14]));
	ENDFOR
}

void SquareAndScale_3620() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[15]));
	ENDFOR
}

void SquareAndScale_3621() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[16]));
	ENDFOR
}

void SquareAndScale_3622() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[17]));
	ENDFOR
}

void SquareAndScale_3623() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[18]));
	ENDFOR
}

void SquareAndScale_3624() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[19]));
	ENDFOR
}

void SquareAndScale_3625() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[20]));
	ENDFOR
}

void SquareAndScale_3626() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[21]));
	ENDFOR
}

void SquareAndScale_3627() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[22]));
	ENDFOR
}

void SquareAndScale_3628() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[23]));
	ENDFOR
}

void SquareAndScale_3629() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[24]));
	ENDFOR
}

void SquareAndScale_3630() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[25]));
	ENDFOR
}

void SquareAndScale_3631() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[26]));
	ENDFOR
}

void SquareAndScale_3632() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[27]));
	ENDFOR
}

void SquareAndScale_3633() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[28]));
	ENDFOR
}

void SquareAndScale_3634() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[29]));
	ENDFOR
}

void SquareAndScale_3635() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[30]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3603() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[__iter_], pop_complex(&ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3604() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600, pop_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3600_s.pos) - 5) >= 0)), __DEFLOOPBOUND__206__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3600_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3600_s.pos) < 64)), __DEFLOOPBOUND__207__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3600_s.poke[(i - 1)] = CFAR_gather_3600_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3600_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3600_s.pos++ ; 
		if(CFAR_gather_3600_s.pos == 64) {
			CFAR_gather_3600_s.pos = 0 ; 
		}
	}


void CFAR_gather_3600() {
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600), &(CFAR_gather_3600AnonFilter_a0_3601));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3601() {
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3600AnonFilter_a0_3601));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600);
	init_buffer_float(&CFAR_gather_3600AnonFilter_a0_3601);
	FOR(int, __iter_init_0_, 0, <, 31, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 31, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_3597
	 {
	ComplexSource_3597_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3603
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3636_3638_split[__iter_], pop_complex(&ComplexSource_3597WEIGHTED_ROUND_ROBIN_Splitter_3603));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3605
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3606
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3607
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3608
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3609
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3610
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3611
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3612
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3613
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3614
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3615
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3616
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3617
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3618
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3619
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3620
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3621
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3622
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3623
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3624
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3625
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3626
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3627
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[22]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3628
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[23]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3629
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[24]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3630
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[25]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3631
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[26]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3632
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[27]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3633
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[28]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3634
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[29]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3635
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3636_3638_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3636_3638_join[30]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3604
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600, pop_float(&SplitJoin0_SquareAndScale_Fiss_3636_3638_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3600
	 {
	CFAR_gather_3600_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 53, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3604CFAR_gather_3600), &(CFAR_gather_3600AnonFilter_a0_3601));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3601
	FOR(uint32_t, __iter_init_, 0, <, 53, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3600AnonFilter_a0_3601));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3597();
		WEIGHTED_ROUND_ROBIN_Splitter_3603();
			SquareAndScale_3605();
			SquareAndScale_3606();
			SquareAndScale_3607();
			SquareAndScale_3608();
			SquareAndScale_3609();
			SquareAndScale_3610();
			SquareAndScale_3611();
			SquareAndScale_3612();
			SquareAndScale_3613();
			SquareAndScale_3614();
			SquareAndScale_3615();
			SquareAndScale_3616();
			SquareAndScale_3617();
			SquareAndScale_3618();
			SquareAndScale_3619();
			SquareAndScale_3620();
			SquareAndScale_3621();
			SquareAndScale_3622();
			SquareAndScale_3623();
			SquareAndScale_3624();
			SquareAndScale_3625();
			SquareAndScale_3626();
			SquareAndScale_3627();
			SquareAndScale_3628();
			SquareAndScale_3629();
			SquareAndScale_3630();
			SquareAndScale_3631();
			SquareAndScale_3632();
			SquareAndScale_3633();
			SquareAndScale_3634();
			SquareAndScale_3635();
		WEIGHTED_ROUND_ROBIN_Joiner_3604();
		CFAR_gather_3600();
		AnonFilter_a0_3601();
	ENDFOR
	return EXIT_SUCCESS;
}
