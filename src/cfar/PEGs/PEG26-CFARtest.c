#include "PEG26-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3954CFAR_gather_3950;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3981_3983_join[26];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3981_3983_split[26];
buffer_complex_t ComplexSource_3947WEIGHTED_ROUND_ROBIN_Splitter_3953;
buffer_float_t CFAR_gather_3950AnonFilter_a0_3951;


ComplexSource_3947_t ComplexSource_3947_s;
CFAR_gather_3950_t CFAR_gather_3950_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3947_s.theta = (ComplexSource_3947_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3947_s.theta)) * (((float) cos(ComplexSource_3947_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3947_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3947_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3947_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3947_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3947_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3947_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3947() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		ComplexSource(&(ComplexSource_3947WEIGHTED_ROUND_ROBIN_Splitter_3953));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3955() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[0]));
	ENDFOR
}

void SquareAndScale_3956() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[1]));
	ENDFOR
}

void SquareAndScale_3957() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[2]));
	ENDFOR
}

void SquareAndScale_3958() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[3]));
	ENDFOR
}

void SquareAndScale_3959() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[4]));
	ENDFOR
}

void SquareAndScale_3960() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[5]));
	ENDFOR
}

void SquareAndScale_3961() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[6]));
	ENDFOR
}

void SquareAndScale_3962() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[7]));
	ENDFOR
}

void SquareAndScale_3963() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[8]));
	ENDFOR
}

void SquareAndScale_3964() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[9]));
	ENDFOR
}

void SquareAndScale_3965() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[10]));
	ENDFOR
}

void SquareAndScale_3966() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[11]));
	ENDFOR
}

void SquareAndScale_3967() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[12]));
	ENDFOR
}

void SquareAndScale_3968() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[13]));
	ENDFOR
}

void SquareAndScale_3969() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[14]));
	ENDFOR
}

void SquareAndScale_3970() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[15]));
	ENDFOR
}

void SquareAndScale_3971() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[16]));
	ENDFOR
}

void SquareAndScale_3972() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[17]));
	ENDFOR
}

void SquareAndScale_3973() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[18]));
	ENDFOR
}

void SquareAndScale_3974() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[19]));
	ENDFOR
}

void SquareAndScale_3975() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[20]));
	ENDFOR
}

void SquareAndScale_3976() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[21]));
	ENDFOR
}

void SquareAndScale_3977() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[22]));
	ENDFOR
}

void SquareAndScale_3978() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[23]));
	ENDFOR
}

void SquareAndScale_3979() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[24]));
	ENDFOR
}

void SquareAndScale_3980() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[25]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3953() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3981_3983_split[__iter_], pop_complex(&ComplexSource_3947WEIGHTED_ROUND_ROBIN_Splitter_3953));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3954() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3954CFAR_gather_3950, pop_float(&SplitJoin0_SquareAndScale_Fiss_3981_3983_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3950_s.pos) - 5) >= 0)), __DEFLOOPBOUND__236__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3950_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3950_s.pos) < 64)), __DEFLOOPBOUND__237__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3950_s.poke[(i - 1)] = CFAR_gather_3950_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3950_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3950_s.pos++ ; 
		if(CFAR_gather_3950_s.pos == 64) {
			CFAR_gather_3950_s.pos = 0 ; 
		}
	}


void CFAR_gather_3950() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3954CFAR_gather_3950), &(CFAR_gather_3950AnonFilter_a0_3951));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3951() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3950AnonFilter_a0_3951));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3954CFAR_gather_3950);
	FOR(int, __iter_init_0_, 0, <, 26, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3981_3983_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 26, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3981_3983_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_3947WEIGHTED_ROUND_ROBIN_Splitter_3953);
	init_buffer_float(&CFAR_gather_3950AnonFilter_a0_3951);
// --- init: ComplexSource_3947
	 {
	ComplexSource_3947_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3947WEIGHTED_ROUND_ROBIN_Splitter_3953));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3953
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3981_3983_split[__iter_], pop_complex(&ComplexSource_3947WEIGHTED_ROUND_ROBIN_Splitter_3953));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3955
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3956
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3957
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3958
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3959
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3960
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3961
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3962
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3963
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3964
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3965
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3966
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3967
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3968
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3969
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3970
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3971
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3972
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3973
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3974
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3975
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3976
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3977
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[22]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3978
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[23]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3979
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[24]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3980
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3981_3983_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3981_3983_join[25]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3954
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3954CFAR_gather_3950, pop_float(&SplitJoin0_SquareAndScale_Fiss_3981_3983_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3950
	 {
	CFAR_gather_3950_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 43, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3954CFAR_gather_3950), &(CFAR_gather_3950AnonFilter_a0_3951));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3951
	FOR(uint32_t, __iter_init_, 0, <, 43, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3950AnonFilter_a0_3951));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3947();
		WEIGHTED_ROUND_ROBIN_Splitter_3953();
			SquareAndScale_3955();
			SquareAndScale_3956();
			SquareAndScale_3957();
			SquareAndScale_3958();
			SquareAndScale_3959();
			SquareAndScale_3960();
			SquareAndScale_3961();
			SquareAndScale_3962();
			SquareAndScale_3963();
			SquareAndScale_3964();
			SquareAndScale_3965();
			SquareAndScale_3966();
			SquareAndScale_3967();
			SquareAndScale_3968();
			SquareAndScale_3969();
			SquareAndScale_3970();
			SquareAndScale_3971();
			SquareAndScale_3972();
			SquareAndScale_3973();
			SquareAndScale_3974();
			SquareAndScale_3975();
			SquareAndScale_3976();
			SquareAndScale_3977();
			SquareAndScale_3978();
			SquareAndScale_3979();
			SquareAndScale_3980();
		WEIGHTED_ROUND_ROBIN_Joiner_3954();
		CFAR_gather_3950();
		AnonFilter_a0_3951();
	ENDFOR
	return EXIT_SUCCESS;
}
