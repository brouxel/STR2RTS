#include "PEG23-CFARtest.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_4164_4166_join[23];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4140CFAR_gather_4136;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4164_4166_split[23];
buffer_float_t CFAR_gather_4136AnonFilter_a0_4137;
buffer_complex_t ComplexSource_4133WEIGHTED_ROUND_ROBIN_Splitter_4139;


ComplexSource_4133_t ComplexSource_4133_s;
CFAR_gather_4136_t CFAR_gather_4136_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4133_s.theta = (ComplexSource_4133_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4133_s.theta)) * (((float) cos(ComplexSource_4133_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4133_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4133_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4133_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4133_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4133_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4133_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4133() {
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		ComplexSource(&(ComplexSource_4133WEIGHTED_ROUND_ROBIN_Splitter_4139));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4141() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[0]));
	ENDFOR
}

void SquareAndScale_4142() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[1]));
	ENDFOR
}

void SquareAndScale_4143() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[2]));
	ENDFOR
}

void SquareAndScale_4144() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[3]));
	ENDFOR
}

void SquareAndScale_4145() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[4]));
	ENDFOR
}

void SquareAndScale_4146() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[5]));
	ENDFOR
}

void SquareAndScale_4147() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[6]));
	ENDFOR
}

void SquareAndScale_4148() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[7]));
	ENDFOR
}

void SquareAndScale_4149() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[8]));
	ENDFOR
}

void SquareAndScale_4150() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[9]));
	ENDFOR
}

void SquareAndScale_4151() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[10]));
	ENDFOR
}

void SquareAndScale_4152() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[11]));
	ENDFOR
}

void SquareAndScale_4153() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[12]));
	ENDFOR
}

void SquareAndScale_4154() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[13]));
	ENDFOR
}

void SquareAndScale_4155() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[14]));
	ENDFOR
}

void SquareAndScale_4156() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[15]));
	ENDFOR
}

void SquareAndScale_4157() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[16]));
	ENDFOR
}

void SquareAndScale_4158() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[17]));
	ENDFOR
}

void SquareAndScale_4159() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[18]));
	ENDFOR
}

void SquareAndScale_4160() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[19]));
	ENDFOR
}

void SquareAndScale_4161() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[20]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[20]));
	ENDFOR
}

void SquareAndScale_4162() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[21]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[21]));
	ENDFOR
}

void SquareAndScale_4163() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[22]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[22]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4139() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4164_4166_split[__iter_], pop_complex(&ComplexSource_4133WEIGHTED_ROUND_ROBIN_Splitter_4139));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4140() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4140CFAR_gather_4136, pop_float(&SplitJoin0_SquareAndScale_Fiss_4164_4166_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4136_s.pos) - 5) >= 0)), __DEFLOOPBOUND__254__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4136_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4136_s.pos) < 64)), __DEFLOOPBOUND__255__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4136_s.poke[(i - 1)] = CFAR_gather_4136_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4136_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4136_s.pos++ ; 
		if(CFAR_gather_4136_s.pos == 64) {
			CFAR_gather_4136_s.pos = 0 ; 
		}
	}


void CFAR_gather_4136() {
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4140CFAR_gather_4136), &(CFAR_gather_4136AnonFilter_a0_4137));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4137() {
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4136AnonFilter_a0_4137));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 23, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4164_4166_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4140CFAR_gather_4136);
	FOR(int, __iter_init_1_, 0, <, 23, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4164_4166_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4136AnonFilter_a0_4137);
	init_buffer_complex(&ComplexSource_4133WEIGHTED_ROUND_ROBIN_Splitter_4139);
// --- init: ComplexSource_4133
	 {
	ComplexSource_4133_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4133WEIGHTED_ROUND_ROBIN_Splitter_4139));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4139
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4164_4166_split[__iter_], pop_complex(&ComplexSource_4133WEIGHTED_ROUND_ROBIN_Splitter_4139));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4141
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4142
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4143
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4144
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4145
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4146
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4147
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4148
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4149
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4150
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4151
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4152
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4153
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4154
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4155
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4156
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4157
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4158
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4159
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4160
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4161
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[20]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4162
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[21]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4163
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4164_4166_split[22]), &(SplitJoin0_SquareAndScale_Fiss_4164_4166_join[22]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4140
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4140CFAR_gather_4136, pop_float(&SplitJoin0_SquareAndScale_Fiss_4164_4166_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4136
	 {
	CFAR_gather_4136_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4140CFAR_gather_4136), &(CFAR_gather_4136AnonFilter_a0_4137));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4137
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4136AnonFilter_a0_4137));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4133();
		WEIGHTED_ROUND_ROBIN_Splitter_4139();
			SquareAndScale_4141();
			SquareAndScale_4142();
			SquareAndScale_4143();
			SquareAndScale_4144();
			SquareAndScale_4145();
			SquareAndScale_4146();
			SquareAndScale_4147();
			SquareAndScale_4148();
			SquareAndScale_4149();
			SquareAndScale_4150();
			SquareAndScale_4151();
			SquareAndScale_4152();
			SquareAndScale_4153();
			SquareAndScale_4154();
			SquareAndScale_4155();
			SquareAndScale_4156();
			SquareAndScale_4157();
			SquareAndScale_4158();
			SquareAndScale_4159();
			SquareAndScale_4160();
			SquareAndScale_4161();
			SquareAndScale_4162();
			SquareAndScale_4163();
		WEIGHTED_ROUND_ROBIN_Joiner_4140();
		CFAR_gather_4136();
		AnonFilter_a0_4137();
	ENDFOR
	return EXIT_SUCCESS;
}
