#include "PEG13-CFARtest.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4644_4646_split[13];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4644_4646_join[13];
buffer_complex_t ComplexSource_4623WEIGHTED_ROUND_ROBIN_Splitter_4629;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4630CFAR_gather_4626;
buffer_float_t CFAR_gather_4626AnonFilter_a0_4627;


ComplexSource_4623_t ComplexSource_4623_s;
CFAR_gather_4626_t CFAR_gather_4626_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4623_s.theta = (ComplexSource_4623_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4623_s.theta)) * (((float) cos(ComplexSource_4623_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4623_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4623_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4623_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4623_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4623_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4623_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4623() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		ComplexSource(&(ComplexSource_4623WEIGHTED_ROUND_ROBIN_Splitter_4629));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4631() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[0]));
	ENDFOR
}

void SquareAndScale_4632() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[1]));
	ENDFOR
}

void SquareAndScale_4633() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[2]));
	ENDFOR
}

void SquareAndScale_4634() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[3]));
	ENDFOR
}

void SquareAndScale_4635() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[4]));
	ENDFOR
}

void SquareAndScale_4636() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[5]));
	ENDFOR
}

void SquareAndScale_4637() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[6]));
	ENDFOR
}

void SquareAndScale_4638() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[7]));
	ENDFOR
}

void SquareAndScale_4639() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[8]));
	ENDFOR
}

void SquareAndScale_4640() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[9]));
	ENDFOR
}

void SquareAndScale_4641() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[10]));
	ENDFOR
}

void SquareAndScale_4642() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[11]));
	ENDFOR
}

void SquareAndScale_4643() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[12]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4629() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4644_4646_split[__iter_], pop_complex(&ComplexSource_4623WEIGHTED_ROUND_ROBIN_Splitter_4629));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4630() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4630CFAR_gather_4626, pop_float(&SplitJoin0_SquareAndScale_Fiss_4644_4646_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4626_s.pos) - 5) >= 0)), __DEFLOOPBOUND__314__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4626_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4626_s.pos) < 64)), __DEFLOOPBOUND__315__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4626_s.poke[(i - 1)] = CFAR_gather_4626_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4626_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4626_s.pos++ ; 
		if(CFAR_gather_4626_s.pos == 64) {
			CFAR_gather_4626_s.pos = 0 ; 
		}
	}


void CFAR_gather_4626() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4630CFAR_gather_4626), &(CFAR_gather_4626AnonFilter_a0_4627));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4627() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4626AnonFilter_a0_4627));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 13, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4644_4646_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 13, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4644_4646_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_4623WEIGHTED_ROUND_ROBIN_Splitter_4629);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4630CFAR_gather_4626);
	init_buffer_float(&CFAR_gather_4626AnonFilter_a0_4627);
// --- init: ComplexSource_4623
	 {
	ComplexSource_4623_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4623WEIGHTED_ROUND_ROBIN_Splitter_4629));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4629
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4644_4646_split[__iter_], pop_complex(&ComplexSource_4623WEIGHTED_ROUND_ROBIN_Splitter_4629));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4631
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4632
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4633
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4634
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4635
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4636
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4637
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4638
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4639
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4640
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4641
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4642
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4643
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4644_4646_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4644_4646_join[12]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4630
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4630CFAR_gather_4626, pop_float(&SplitJoin0_SquareAndScale_Fiss_4644_4646_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4626
	 {
	CFAR_gather_4626_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 43, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4630CFAR_gather_4626), &(CFAR_gather_4626AnonFilter_a0_4627));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4627
	FOR(uint32_t, __iter_init_, 0, <, 43, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4626AnonFilter_a0_4627));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4623();
		WEIGHTED_ROUND_ROBIN_Splitter_4629();
			SquareAndScale_4631();
			SquareAndScale_4632();
			SquareAndScale_4633();
			SquareAndScale_4634();
			SquareAndScale_4635();
			SquareAndScale_4636();
			SquareAndScale_4637();
			SquareAndScale_4638();
			SquareAndScale_4639();
			SquareAndScale_4640();
			SquareAndScale_4641();
			SquareAndScale_4642();
			SquareAndScale_4643();
		WEIGHTED_ROUND_ROBIN_Joiner_4630();
		CFAR_gather_4626();
		AnonFilter_a0_4627();
	ENDFOR
	return EXIT_SUCCESS;
}
