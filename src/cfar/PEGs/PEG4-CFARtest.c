#include "PEG4-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4900CFAR_gather_4896;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4905_4907_split[4];
buffer_complex_t ComplexSource_4893WEIGHTED_ROUND_ROBIN_Splitter_4899;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4905_4907_join[4];
buffer_float_t CFAR_gather_4896AnonFilter_a0_4897;


ComplexSource_4893_t ComplexSource_4893_s;
CFAR_gather_4896_t CFAR_gather_4896_s;

void ComplexSource(buffer_complex_t *chanout) {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4893_s.theta = (ComplexSource_4893_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4893_s.theta)) * (((float) cos(ComplexSource_4893_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4893_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4893_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4893_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4893_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4893_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4893_s.theta))) - 0.0)))) ; 
		push_complex(&(*chanout), c) ; 
	}
	ENDFOR
}


void ComplexSource_4893() {
	ComplexSource(&(ComplexSource_4893WEIGHTED_ROUND_ROBIN_Splitter_4899));
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4901() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4905_4907_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4905_4907_join[0]));
	ENDFOR
}

void SquareAndScale_4902() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4905_4907_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4905_4907_join[1]));
	ENDFOR
}

void SquareAndScale_4903() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4905_4907_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4905_4907_join[2]));
	ENDFOR
}

void SquareAndScale_4904() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4905_4907_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4905_4907_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4899() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4905_4907_split[__iter_], pop_complex(&ComplexSource_4893WEIGHTED_ROUND_ROBIN_Splitter_4899));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4900() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4900CFAR_gather_4896, pop_float(&SplitJoin0_SquareAndScale_Fiss_4905_4907_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4896_s.pos) - 5) >= 0)), __DEFLOOPBOUND__368__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4896_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4896_s.pos) < 64)), __DEFLOOPBOUND__369__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4896_s.poke[(i - 1)] = CFAR_gather_4896_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4896_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4896_s.pos++ ; 
		if(CFAR_gather_4896_s.pos == 64) {
			CFAR_gather_4896_s.pos = 0 ; 
		}
	}


void CFAR_gather_4896() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4900CFAR_gather_4896), &(CFAR_gather_4896AnonFilter_a0_4897));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4897() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4896AnonFilter_a0_4897));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4900CFAR_gather_4896);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4905_4907_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_4893WEIGHTED_ROUND_ROBIN_Splitter_4899);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4905_4907_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4896AnonFilter_a0_4897);
// --- init: ComplexSource_4893
	 {
	ComplexSource_4893_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4893WEIGHTED_ROUND_ROBIN_Splitter_4899));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4899
	FOR(uint32_t, __iter_init_, 0, <, 16, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4905_4907_split[__iter_], pop_complex(&ComplexSource_4893WEIGHTED_ROUND_ROBIN_Splitter_4899));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4901
	FOR(uint32_t, __iter_init_, 0, <, 16, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4905_4907_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4905_4907_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4902
	FOR(uint32_t, __iter_init_, 0, <, 16, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4905_4907_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4905_4907_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4903
	FOR(uint32_t, __iter_init_, 0, <, 16, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4905_4907_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4905_4907_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4904
	FOR(uint32_t, __iter_init_, 0, <, 16, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4905_4907_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4905_4907_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4900
	FOR(uint32_t, __iter_init_, 0, <, 16, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4900CFAR_gather_4896, pop_float(&SplitJoin0_SquareAndScale_Fiss_4905_4907_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4896
	 {
	CFAR_gather_4896_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4900CFAR_gather_4896), &(CFAR_gather_4896AnonFilter_a0_4897));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4897
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4896AnonFilter_a0_4897));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4893();
		WEIGHTED_ROUND_ROBIN_Splitter_4899();
			SquareAndScale_4901();
			SquareAndScale_4902();
			SquareAndScale_4903();
			SquareAndScale_4904();
		WEIGHTED_ROUND_ROBIN_Joiner_4900();
		CFAR_gather_4896();
		AnonFilter_a0_4897();
	ENDFOR
	return EXIT_SUCCESS;
}
