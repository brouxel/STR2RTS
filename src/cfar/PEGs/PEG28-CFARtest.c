#include "PEG28-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3820CFAR_gather_3816;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3849_3851_split[28];
buffer_complex_t ComplexSource_3813WEIGHTED_ROUND_ROBIN_Splitter_3819;
buffer_float_t CFAR_gather_3816AnonFilter_a0_3817;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3849_3851_join[28];


ComplexSource_3813_t ComplexSource_3813_s;
CFAR_gather_3816_t CFAR_gather_3816_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3813_s.theta = (ComplexSource_3813_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3813_s.theta)) * (((float) cos(ComplexSource_3813_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3813_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3813_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3813_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3813_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3813_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3813_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3813() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		ComplexSource(&(ComplexSource_3813WEIGHTED_ROUND_ROBIN_Splitter_3819));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3821() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[0]));
	ENDFOR
}

void SquareAndScale_3822() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[1]));
	ENDFOR
}

void SquareAndScale_3823() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[2]));
	ENDFOR
}

void SquareAndScale_3824() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[3]));
	ENDFOR
}

void SquareAndScale_3825() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[4]));
	ENDFOR
}

void SquareAndScale_3826() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[5]));
	ENDFOR
}

void SquareAndScale_3827() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[6]));
	ENDFOR
}

void SquareAndScale_3828() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[7]));
	ENDFOR
}

void SquareAndScale_3829() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[8]));
	ENDFOR
}

void SquareAndScale_3830() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[9]));
	ENDFOR
}

void SquareAndScale_3831() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[10]));
	ENDFOR
}

void SquareAndScale_3832() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[11]));
	ENDFOR
}

void SquareAndScale_3833() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[12]));
	ENDFOR
}

void SquareAndScale_3834() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[13]));
	ENDFOR
}

void SquareAndScale_3835() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[14]));
	ENDFOR
}

void SquareAndScale_3836() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[15]));
	ENDFOR
}

void SquareAndScale_3837() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[16]));
	ENDFOR
}

void SquareAndScale_3838() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[17]));
	ENDFOR
}

void SquareAndScale_3839() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[18]));
	ENDFOR
}

void SquareAndScale_3840() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[19]));
	ENDFOR
}

void SquareAndScale_3841() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[20]));
	ENDFOR
}

void SquareAndScale_3842() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[21]));
	ENDFOR
}

void SquareAndScale_3843() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[22]));
	ENDFOR
}

void SquareAndScale_3844() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[23]));
	ENDFOR
}

void SquareAndScale_3845() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[24]));
	ENDFOR
}

void SquareAndScale_3846() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[25]));
	ENDFOR
}

void SquareAndScale_3847() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[26]));
	ENDFOR
}

void SquareAndScale_3848() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[27]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3819() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3849_3851_split[__iter_], pop_complex(&ComplexSource_3813WEIGHTED_ROUND_ROBIN_Splitter_3819));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3820() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3820CFAR_gather_3816, pop_float(&SplitJoin0_SquareAndScale_Fiss_3849_3851_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3816_s.pos) - 5) >= 0)), __DEFLOOPBOUND__224__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3816_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3816_s.pos) < 64)), __DEFLOOPBOUND__225__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3816_s.poke[(i - 1)] = CFAR_gather_3816_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3816_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3816_s.pos++ ; 
		if(CFAR_gather_3816_s.pos == 64) {
			CFAR_gather_3816_s.pos = 0 ; 
		}
	}


void CFAR_gather_3816() {
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3820CFAR_gather_3816), &(CFAR_gather_3816AnonFilter_a0_3817));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3817() {
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3816AnonFilter_a0_3817));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3820CFAR_gather_3816);
	FOR(int, __iter_init_0_, 0, <, 28, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3849_3851_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_3813WEIGHTED_ROUND_ROBIN_Splitter_3819);
	init_buffer_float(&CFAR_gather_3816AnonFilter_a0_3817);
	FOR(int, __iter_init_1_, 0, <, 28, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3849_3851_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_3813
	 {
	ComplexSource_3813_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3813WEIGHTED_ROUND_ROBIN_Splitter_3819));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3819
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3849_3851_split[__iter_], pop_complex(&ComplexSource_3813WEIGHTED_ROUND_ROBIN_Splitter_3819));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3821
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3822
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3823
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3824
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3825
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3826
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3827
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3828
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3829
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3830
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3831
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3832
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3833
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3834
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3835
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3836
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3837
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3838
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3839
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3840
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3841
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3842
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3843
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[22]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3844
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[23]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3845
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[24]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3846
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[25]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3847
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[26]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3848
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3849_3851_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3849_3851_join[27]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3820
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3820CFAR_gather_3816, pop_float(&SplitJoin0_SquareAndScale_Fiss_3849_3851_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3816
	 {
	CFAR_gather_3816_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3820CFAR_gather_3816), &(CFAR_gather_3816AnonFilter_a0_3817));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3817
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3816AnonFilter_a0_3817));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3813();
		WEIGHTED_ROUND_ROBIN_Splitter_3819();
			SquareAndScale_3821();
			SquareAndScale_3822();
			SquareAndScale_3823();
			SquareAndScale_3824();
			SquareAndScale_3825();
			SquareAndScale_3826();
			SquareAndScale_3827();
			SquareAndScale_3828();
			SquareAndScale_3829();
			SquareAndScale_3830();
			SquareAndScale_3831();
			SquareAndScale_3832();
			SquareAndScale_3833();
			SquareAndScale_3834();
			SquareAndScale_3835();
			SquareAndScale_3836();
			SquareAndScale_3837();
			SquareAndScale_3838();
			SquareAndScale_3839();
			SquareAndScale_3840();
			SquareAndScale_3841();
			SquareAndScale_3842();
			SquareAndScale_3843();
			SquareAndScale_3844();
			SquareAndScale_3845();
			SquareAndScale_3846();
			SquareAndScale_3847();
			SquareAndScale_3848();
		WEIGHTED_ROUND_ROBIN_Joiner_3820();
		CFAR_gather_3816();
		AnonFilter_a0_3817();
	ENDFOR
	return EXIT_SUCCESS;
}
