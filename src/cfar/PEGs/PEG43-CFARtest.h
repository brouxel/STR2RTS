#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2816 on the compile command line
#else
#if BUF_SIZEMAX < 2816
#error BUF_SIZEMAX too small, it must be at least 2816
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_2553_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_2556_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_2553();
void WEIGHTED_ROUND_ROBIN_Splitter_2559();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_2561();
void SquareAndScale_2562();
void SquareAndScale_2563();
void SquareAndScale_2564();
void SquareAndScale_2565();
void SquareAndScale_2566();
void SquareAndScale_2567();
void SquareAndScale_2568();
void SquareAndScale_2569();
void SquareAndScale_2570();
void SquareAndScale_2571();
void SquareAndScale_2572();
void SquareAndScale_2573();
void SquareAndScale_2574();
void SquareAndScale_2575();
void SquareAndScale_2576();
void SquareAndScale_2577();
void SquareAndScale_2578();
void SquareAndScale_2579();
void SquareAndScale_2580();
void SquareAndScale_2581();
void SquareAndScale_2582();
void SquareAndScale_2583();
void SquareAndScale_2584();
void SquareAndScale_2585();
void SquareAndScale_2586();
void SquareAndScale_2587();
void SquareAndScale_2588();
void SquareAndScale_2589();
void SquareAndScale_2590();
void SquareAndScale_2591();
void SquareAndScale_2592();
void SquareAndScale_2593();
void SquareAndScale_2594();
void SquareAndScale_2595();
void SquareAndScale_2596();
void SquareAndScale_2597();
void SquareAndScale_2598();
void SquareAndScale_2599();
void SquareAndScale_2600();
void SquareAndScale_2601();
void SquareAndScale_2602();
void SquareAndScale_2603();
void WEIGHTED_ROUND_ROBIN_Joiner_2560();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_2556();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_2557();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1

#define __DEFLOOPBOUND__72__ -1

#define __DEFLOOPBOUND__73__ -1

#define __DEFLOOPBOUND__74__ -1

#define __DEFLOOPBOUND__75__ -1

#define __DEFLOOPBOUND__76__ -1

#define __DEFLOOPBOUND__77__ -1

#define __DEFLOOPBOUND__78__ -1

#define __DEFLOOPBOUND__79__ -1

#define __DEFLOOPBOUND__80__ -1

#define __DEFLOOPBOUND__81__ -1

#define __DEFLOOPBOUND__82__ -1

#define __DEFLOOPBOUND__83__ -1

#define __DEFLOOPBOUND__84__ -1

#define __DEFLOOPBOUND__85__ -1

#define __DEFLOOPBOUND__86__ -1

#define __DEFLOOPBOUND__87__ -1

#define __DEFLOOPBOUND__88__ -1

#define __DEFLOOPBOUND__89__ -1

#define __DEFLOOPBOUND__90__ -1

#define __DEFLOOPBOUND__91__ -1

#define __DEFLOOPBOUND__92__ -1

#define __DEFLOOPBOUND__93__ -1

#define __DEFLOOPBOUND__94__ -1

#define __DEFLOOPBOUND__95__ -1

#define __DEFLOOPBOUND__96__ -1

#define __DEFLOOPBOUND__97__ -1

#define __DEFLOOPBOUND__98__ -1

#define __DEFLOOPBOUND__99__ -1

#define __DEFLOOPBOUND__100__ -1

#define __DEFLOOPBOUND__101__ -1

#define __DEFLOOPBOUND__102__ -1

#define __DEFLOOPBOUND__103__ -1

#define __DEFLOOPBOUND__104__ -1

#define __DEFLOOPBOUND__105__ -1

#define __DEFLOOPBOUND__106__ -1

#define __DEFLOOPBOUND__107__ -1

#define __DEFLOOPBOUND__108__ -1

#define __DEFLOOPBOUND__109__ -1

#define __DEFLOOPBOUND__110__ -1

#define __DEFLOOPBOUND__111__ -1

#define __DEFLOOPBOUND__112__ -1

#define __DEFLOOPBOUND__113__ -1

#define __DEFLOOPBOUND__114__ -1

#define __DEFLOOPBOUND__115__ -1

#define __DEFLOOPBOUND__116__ -1

#define __DEFLOOPBOUND__117__ -1

#define __DEFLOOPBOUND__118__ -1

#define __DEFLOOPBOUND__119__ -1

#define __DEFLOOPBOUND__120__ -1

#define __DEFLOOPBOUND__121__ -1

#define __DEFLOOPBOUND__122__ -1

#define __DEFLOOPBOUND__123__ -1

#define __DEFLOOPBOUND__124__ -1

#define __DEFLOOPBOUND__125__ -1

#define __DEFLOOPBOUND__126__ -1

#define __DEFLOOPBOUND__127__ -1

#define __DEFLOOPBOUND__128__ -1

#define __DEFLOOPBOUND__129__ -1

#define __DEFLOOPBOUND__130__ -1

#define __DEFLOOPBOUND__131__ -1

#define __DEFLOOPBOUND__132__ -1

#define __DEFLOOPBOUND__133__ -1

#define __DEFLOOPBOUND__134__ -1

#define __DEFLOOPBOUND__135__ -1

#define __DEFLOOPBOUND__136__ -1

#define __DEFLOOPBOUND__137__ -1


#ifdef __cplusplus
}
#endif
#endif
