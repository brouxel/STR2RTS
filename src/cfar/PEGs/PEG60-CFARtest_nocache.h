#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=11121 on the compile command line
#else
#if BUF_SIZEMAX < 11121
#error BUF_SIZEMAX too small, it must be at least 11121
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_581_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_584_t;
void ComplexSource_581();
void WEIGHTED_ROUND_ROBIN_Splitter_587();
void SquareAndScale_589();
void SquareAndScale_590();
void SquareAndScale_591();
void SquareAndScale_592();
void SquareAndScale_593();
void SquareAndScale_594();
void SquareAndScale_595();
void SquareAndScale_596();
void SquareAndScale_597();
void SquareAndScale_598();
void SquareAndScale_599();
void SquareAndScale_600();
void SquareAndScale_601();
void SquareAndScale_602();
void SquareAndScale_603();
void SquareAndScale_604();
void SquareAndScale_605();
void SquareAndScale_606();
void SquareAndScale_607();
void SquareAndScale_608();
void SquareAndScale_609();
void SquareAndScale_610();
void SquareAndScale_611();
void SquareAndScale_612();
void SquareAndScale_613();
void SquareAndScale_614();
void SquareAndScale_615();
void SquareAndScale_616();
void SquareAndScale_617();
void SquareAndScale_618();
void SquareAndScale_619();
void SquareAndScale_620();
void SquareAndScale_621();
void SquareAndScale_622();
void SquareAndScale_623();
void SquareAndScale_624();
void SquareAndScale_625();
void SquareAndScale_626();
void SquareAndScale_627();
void SquareAndScale_628();
void SquareAndScale_629();
void SquareAndScale_630();
void SquareAndScale_631();
void SquareAndScale_632();
void SquareAndScale_633();
void SquareAndScale_634();
void SquareAndScale_635();
void SquareAndScale_636();
void SquareAndScale_637();
void SquareAndScale_638();
void SquareAndScale_639();
void SquareAndScale_640();
void SquareAndScale_641();
void SquareAndScale_642();
void SquareAndScale_643();
void SquareAndScale_644();
void SquareAndScale_645();
void SquareAndScale_646();
void SquareAndScale_647();
void SquareAndScale_648();
void WEIGHTED_ROUND_ROBIN_Joiner_588();
void CFAR_gather_584();
void AnonFilter_a0_585();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1


#ifdef __cplusplus
}
#endif
#endif
