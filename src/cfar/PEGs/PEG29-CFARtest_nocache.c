#include "PEG29-CFARtest_nocache.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_3780_3782_join[29];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3780_3782_split[29];
buffer_complex_t ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749;
buffer_float_t CFAR_gather_3746AnonFilter_a0_3747;


ComplexSource_3743_t ComplexSource_3743_s;
CFAR_gather_3746_t CFAR_gather_3746_s;

void ComplexSource_3743(){
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3743_s.theta = (ComplexSource_3743_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3743_s.theta)) * (((float) cos(ComplexSource_3743_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3743_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3743_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3743_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3743_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3743_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3743_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_3751(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3752(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3753(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3754(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3755(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3756(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3757(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3758(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3759(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3760(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3761(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3762(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3763(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3764(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3765(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3766(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3767(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3768(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3769(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3770(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3771(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3772(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[21]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[21], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3773(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[22]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[22], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3774(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[23]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[23], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3775(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[24]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[24], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3776(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[25]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[25], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3777(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[26]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[26], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3778(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[27]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[27], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_3779(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[28]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[28], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3749() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[__iter_], pop_complex(&ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3750() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746, pop_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_3746(){
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3746_s.pos) - 5) >= 0)), __DEFLOOPBOUND__218__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3746_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3746_s.pos) < 64)), __DEFLOOPBOUND__219__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_3746AnonFilter_a0_3747, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3746_s.poke[(i - 1)] = CFAR_gather_3746_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3746_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746) ; 
		CFAR_gather_3746_s.pos++ ; 
		if(CFAR_gather_3746_s.pos == 64) {
			CFAR_gather_3746_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_3747(){
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_3746AnonFilter_a0_3747));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 29, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746);
	FOR(int, __iter_init_1_, 0, <, 29, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749);
	init_buffer_float(&CFAR_gather_3746AnonFilter_a0_3747);
// --- init: ComplexSource_3743
	 {
	ComplexSource_3743_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_3743_s.theta = (ComplexSource_3743_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_3743_s.theta)) * (((float) cos(ComplexSource_3743_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3743_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3743_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_3743_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3743_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3743_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3743_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3749
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[__iter_], pop_complex(&ComplexSource_3743WEIGHTED_ROUND_ROBIN_Splitter_3749));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3751
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3752
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3753
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3754
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3755
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3756
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3757
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3758
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3759
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3760
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3761
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3762
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3763
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3764
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3765
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3766
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3767
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3768
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3769
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3770
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3771
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3772
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[21]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[21], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3773
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[22]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[22], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3774
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[23]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[23], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3775
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[24]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[24], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3776
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[25]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[25], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3777
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[26]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[26], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3778
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[27]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[27], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3779
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_3780_3782_split[28]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[28], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3750
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746, pop_float(&SplitJoin0_SquareAndScale_Fiss_3780_3782_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3746
	 {
	CFAR_gather_3746_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 49, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3746_s.pos) - 5) >= 0)), __DEFLOOPBOUND__220__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3746_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3746_s.pos) < 64)), __DEFLOOPBOUND__221__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_3746AnonFilter_a0_3747, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3746_s.poke[(i - 1)] = CFAR_gather_3746_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3746_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3750CFAR_gather_3746) ; 
		CFAR_gather_3746_s.pos++ ; 
		if(CFAR_gather_3746_s.pos == 64) {
			CFAR_gather_3746_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3747
	FOR(uint32_t, __iter_init_, 0, <, 49, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_3746AnonFilter_a0_3747));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3743();
		WEIGHTED_ROUND_ROBIN_Splitter_3749();
			SquareAndScale_3751();
			SquareAndScale_3752();
			SquareAndScale_3753();
			SquareAndScale_3754();
			SquareAndScale_3755();
			SquareAndScale_3756();
			SquareAndScale_3757();
			SquareAndScale_3758();
			SquareAndScale_3759();
			SquareAndScale_3760();
			SquareAndScale_3761();
			SquareAndScale_3762();
			SquareAndScale_3763();
			SquareAndScale_3764();
			SquareAndScale_3765();
			SquareAndScale_3766();
			SquareAndScale_3767();
			SquareAndScale_3768();
			SquareAndScale_3769();
			SquareAndScale_3770();
			SquareAndScale_3771();
			SquareAndScale_3772();
			SquareAndScale_3773();
			SquareAndScale_3774();
			SquareAndScale_3775();
			SquareAndScale_3776();
			SquareAndScale_3777();
			SquareAndScale_3778();
			SquareAndScale_3779();
		WEIGHTED_ROUND_ROBIN_Joiner_3750();
		CFAR_gather_3746();
		AnonFilter_a0_3747();
	ENDFOR
	return EXIT_SUCCESS;
}
