#include "PEG55-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1228CFAR_gather_1224;
buffer_float_t CFAR_gather_1224AnonFilter_a0_1225;
buffer_complex_t ComplexSource_1221WEIGHTED_ROUND_ROBIN_Splitter_1227;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1284_1286_split[55];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_1284_1286_join[55];


ComplexSource_1221_t ComplexSource_1221_s;
CFAR_gather_1224_t CFAR_gather_1224_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_1221_s.theta = (ComplexSource_1221_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_1221_s.theta)) * (((float) cos(ComplexSource_1221_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1221_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1221_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_1221_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1221_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1221_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1221_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_1221() {
	FOR(uint32_t, __iter_steady_, 0, <, 55, __iter_steady_++)
		ComplexSource(&(ComplexSource_1221WEIGHTED_ROUND_ROBIN_Splitter_1227));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_1229() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[0]));
	ENDFOR
}

void SquareAndScale_1230() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[1]));
	ENDFOR
}

void SquareAndScale_1231() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[2]));
	ENDFOR
}

void SquareAndScale_1232() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[3]));
	ENDFOR
}

void SquareAndScale_1233() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[4]));
	ENDFOR
}

void SquareAndScale_1234() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[5]));
	ENDFOR
}

void SquareAndScale_1235() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[6]));
	ENDFOR
}

void SquareAndScale_1236() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[7]));
	ENDFOR
}

void SquareAndScale_1237() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[8]));
	ENDFOR
}

void SquareAndScale_1238() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[9]));
	ENDFOR
}

void SquareAndScale_1239() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[10]));
	ENDFOR
}

void SquareAndScale_1240() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[11]));
	ENDFOR
}

void SquareAndScale_1241() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[12]));
	ENDFOR
}

void SquareAndScale_1242() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[13]));
	ENDFOR
}

void SquareAndScale_1243() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[14]));
	ENDFOR
}

void SquareAndScale_1244() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[15]));
	ENDFOR
}

void SquareAndScale_1245() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[16]));
	ENDFOR
}

void SquareAndScale_1246() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[17]));
	ENDFOR
}

void SquareAndScale_1247() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[18]));
	ENDFOR
}

void SquareAndScale_1248() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[19]));
	ENDFOR
}

void SquareAndScale_1249() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[20]));
	ENDFOR
}

void SquareAndScale_1250() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[21]));
	ENDFOR
}

void SquareAndScale_1251() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[22]));
	ENDFOR
}

void SquareAndScale_1252() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[23]));
	ENDFOR
}

void SquareAndScale_1253() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[24]));
	ENDFOR
}

void SquareAndScale_1254() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[25]));
	ENDFOR
}

void SquareAndScale_1255() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[26]));
	ENDFOR
}

void SquareAndScale_1256() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[27]));
	ENDFOR
}

void SquareAndScale_1257() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[28]));
	ENDFOR
}

void SquareAndScale_1258() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[29]));
	ENDFOR
}

void SquareAndScale_1259() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[30]));
	ENDFOR
}

void SquareAndScale_1260() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[31]));
	ENDFOR
}

void SquareAndScale_1261() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[32]));
	ENDFOR
}

void SquareAndScale_1262() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[33]));
	ENDFOR
}

void SquareAndScale_1263() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[34]));
	ENDFOR
}

void SquareAndScale_1264() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[35]));
	ENDFOR
}

void SquareAndScale_1265() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[36]));
	ENDFOR
}

void SquareAndScale_1266() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[37]));
	ENDFOR
}

void SquareAndScale_1267() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[38]));
	ENDFOR
}

void SquareAndScale_1268() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[39]));
	ENDFOR
}

void SquareAndScale_1269() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[40]));
	ENDFOR
}

void SquareAndScale_1270() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[41]));
	ENDFOR
}

void SquareAndScale_1271() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[42]));
	ENDFOR
}

void SquareAndScale_1272() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[43]));
	ENDFOR
}

void SquareAndScale_1273() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[44]));
	ENDFOR
}

void SquareAndScale_1274() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[45]));
	ENDFOR
}

void SquareAndScale_1275() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[46]));
	ENDFOR
}

void SquareAndScale_1276() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[47]));
	ENDFOR
}

void SquareAndScale_1277() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[48]));
	ENDFOR
}

void SquareAndScale_1278() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[49]));
	ENDFOR
}

void SquareAndScale_1279() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[50]));
	ENDFOR
}

void SquareAndScale_1280() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[51]));
	ENDFOR
}

void SquareAndScale_1281() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[52]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[52]));
	ENDFOR
}

void SquareAndScale_1282() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[53]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[53]));
	ENDFOR
}

void SquareAndScale_1283() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[54]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[54]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1227() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 55, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1284_1286_split[__iter_], pop_complex(&ComplexSource_1221WEIGHTED_ROUND_ROBIN_Splitter_1227));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1228() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 55, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1228CFAR_gather_1224, pop_float(&SplitJoin0_SquareAndScale_Fiss_1284_1286_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1224_s.pos) - 5) >= 0)), __DEFLOOPBOUND__62__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1224_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1224_s.pos) < 64)), __DEFLOOPBOUND__63__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1224_s.poke[(i - 1)] = CFAR_gather_1224_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1224_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_1224_s.pos++ ; 
		if(CFAR_gather_1224_s.pos == 64) {
			CFAR_gather_1224_s.pos = 0 ; 
		}
	}


void CFAR_gather_1224() {
	FOR(uint32_t, __iter_steady_, 0, <, 3520, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1228CFAR_gather_1224), &(CFAR_gather_1224AnonFilter_a0_1225));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_1225() {
	FOR(uint32_t, __iter_steady_, 0, <, 3520, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_1224AnonFilter_a0_1225));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1228CFAR_gather_1224);
	init_buffer_float(&CFAR_gather_1224AnonFilter_a0_1225);
	init_buffer_complex(&ComplexSource_1221WEIGHTED_ROUND_ROBIN_Splitter_1227);
	FOR(int, __iter_init_0_, 0, <, 55, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1284_1286_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 55, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1284_1286_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_1221
	 {
	ComplexSource_1221_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_1221WEIGHTED_ROUND_ROBIN_Splitter_1227));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1227
	
	FOR(uint32_t, __iter_, 0, <, 55, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1284_1286_split[__iter_], pop_complex(&ComplexSource_1221WEIGHTED_ROUND_ROBIN_Splitter_1227));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_1229
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[0]));
//--------------------------------
// --- init: SquareAndScale_1230
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[1]));
//--------------------------------
// --- init: SquareAndScale_1231
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[2]));
//--------------------------------
// --- init: SquareAndScale_1232
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[3]));
//--------------------------------
// --- init: SquareAndScale_1233
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[4]));
//--------------------------------
// --- init: SquareAndScale_1234
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[5]));
//--------------------------------
// --- init: SquareAndScale_1235
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[6]));
//--------------------------------
// --- init: SquareAndScale_1236
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[7]));
//--------------------------------
// --- init: SquareAndScale_1237
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[8]));
//--------------------------------
// --- init: SquareAndScale_1238
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[9]));
//--------------------------------
// --- init: SquareAndScale_1239
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[10]));
//--------------------------------
// --- init: SquareAndScale_1240
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[11]));
//--------------------------------
// --- init: SquareAndScale_1241
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[12]));
//--------------------------------
// --- init: SquareAndScale_1242
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[13]));
//--------------------------------
// --- init: SquareAndScale_1243
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[14]));
//--------------------------------
// --- init: SquareAndScale_1244
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[15]));
//--------------------------------
// --- init: SquareAndScale_1245
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[16]));
//--------------------------------
// --- init: SquareAndScale_1246
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[17]));
//--------------------------------
// --- init: SquareAndScale_1247
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[18]));
//--------------------------------
// --- init: SquareAndScale_1248
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[19]));
//--------------------------------
// --- init: SquareAndScale_1249
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[20]));
//--------------------------------
// --- init: SquareAndScale_1250
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[21]));
//--------------------------------
// --- init: SquareAndScale_1251
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[22]));
//--------------------------------
// --- init: SquareAndScale_1252
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[23]));
//--------------------------------
// --- init: SquareAndScale_1253
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[24]));
//--------------------------------
// --- init: SquareAndScale_1254
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[25]));
//--------------------------------
// --- init: SquareAndScale_1255
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[26]));
//--------------------------------
// --- init: SquareAndScale_1256
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[27]));
//--------------------------------
// --- init: SquareAndScale_1257
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[28]));
//--------------------------------
// --- init: SquareAndScale_1258
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[29]));
//--------------------------------
// --- init: SquareAndScale_1259
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[30]));
//--------------------------------
// --- init: SquareAndScale_1260
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[31]));
//--------------------------------
// --- init: SquareAndScale_1261
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[32]));
//--------------------------------
// --- init: SquareAndScale_1262
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[33]));
//--------------------------------
// --- init: SquareAndScale_1263
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[34]));
//--------------------------------
// --- init: SquareAndScale_1264
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[35]));
//--------------------------------
// --- init: SquareAndScale_1265
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[36]));
//--------------------------------
// --- init: SquareAndScale_1266
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[37]));
//--------------------------------
// --- init: SquareAndScale_1267
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[38]));
//--------------------------------
// --- init: SquareAndScale_1268
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[39]));
//--------------------------------
// --- init: SquareAndScale_1269
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[40]));
//--------------------------------
// --- init: SquareAndScale_1270
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[41]));
//--------------------------------
// --- init: SquareAndScale_1271
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[42]));
//--------------------------------
// --- init: SquareAndScale_1272
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[43]));
//--------------------------------
// --- init: SquareAndScale_1273
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[44]));
//--------------------------------
// --- init: SquareAndScale_1274
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[45]));
//--------------------------------
// --- init: SquareAndScale_1275
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[46]));
//--------------------------------
// --- init: SquareAndScale_1276
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[47]));
//--------------------------------
// --- init: SquareAndScale_1277
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[48]));
//--------------------------------
// --- init: SquareAndScale_1278
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[49]));
//--------------------------------
// --- init: SquareAndScale_1279
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[50]));
//--------------------------------
// --- init: SquareAndScale_1280
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[51]));
//--------------------------------
// --- init: SquareAndScale_1281
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[52]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[52]));
//--------------------------------
// --- init: SquareAndScale_1282
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[53]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[53]));
//--------------------------------
// --- init: SquareAndScale_1283
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1284_1286_split[54]), &(SplitJoin0_SquareAndScale_Fiss_1284_1286_join[54]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1228
	
	FOR(uint32_t, __iter_, 0, <, 55, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1228CFAR_gather_1224, pop_float(&SplitJoin0_SquareAndScale_Fiss_1284_1286_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_1224
	 {
	CFAR_gather_1224_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1228CFAR_gather_1224), &(CFAR_gather_1224AnonFilter_a0_1225));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_1225
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_1224AnonFilter_a0_1225));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_1221();
		WEIGHTED_ROUND_ROBIN_Splitter_1227();
			SquareAndScale_1229();
			SquareAndScale_1230();
			SquareAndScale_1231();
			SquareAndScale_1232();
			SquareAndScale_1233();
			SquareAndScale_1234();
			SquareAndScale_1235();
			SquareAndScale_1236();
			SquareAndScale_1237();
			SquareAndScale_1238();
			SquareAndScale_1239();
			SquareAndScale_1240();
			SquareAndScale_1241();
			SquareAndScale_1242();
			SquareAndScale_1243();
			SquareAndScale_1244();
			SquareAndScale_1245();
			SquareAndScale_1246();
			SquareAndScale_1247();
			SquareAndScale_1248();
			SquareAndScale_1249();
			SquareAndScale_1250();
			SquareAndScale_1251();
			SquareAndScale_1252();
			SquareAndScale_1253();
			SquareAndScale_1254();
			SquareAndScale_1255();
			SquareAndScale_1256();
			SquareAndScale_1257();
			SquareAndScale_1258();
			SquareAndScale_1259();
			SquareAndScale_1260();
			SquareAndScale_1261();
			SquareAndScale_1262();
			SquareAndScale_1263();
			SquareAndScale_1264();
			SquareAndScale_1265();
			SquareAndScale_1266();
			SquareAndScale_1267();
			SquareAndScale_1268();
			SquareAndScale_1269();
			SquareAndScale_1270();
			SquareAndScale_1271();
			SquareAndScale_1272();
			SquareAndScale_1273();
			SquareAndScale_1274();
			SquareAndScale_1275();
			SquareAndScale_1276();
			SquareAndScale_1277();
			SquareAndScale_1278();
			SquareAndScale_1279();
			SquareAndScale_1280();
			SquareAndScale_1281();
			SquareAndScale_1282();
			SquareAndScale_1283();
		WEIGHTED_ROUND_ROBIN_Joiner_1228();
		CFAR_gather_1224();
		AnonFilter_a0_1225();
	ENDFOR
	return EXIT_SUCCESS;
}
