#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=256 on the compile command line
#else
#if BUF_SIZEMAX < 256
#error BUF_SIZEMAX too small, it must be at least 256
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_2033_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_2036_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_2033();
void WEIGHTED_ROUND_ROBIN_Splitter_2039();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_2041();
void SquareAndScale_2042();
void SquareAndScale_2043();
void SquareAndScale_2044();
void SquareAndScale_2045();
void SquareAndScale_2046();
void SquareAndScale_2047();
void SquareAndScale_2048();
void SquareAndScale_2049();
void SquareAndScale_2050();
void SquareAndScale_2051();
void SquareAndScale_2052();
void SquareAndScale_2053();
void SquareAndScale_2054();
void SquareAndScale_2055();
void SquareAndScale_2056();
void SquareAndScale_2057();
void SquareAndScale_2058();
void SquareAndScale_2059();
void SquareAndScale_2060();
void SquareAndScale_2061();
void SquareAndScale_2062();
void SquareAndScale_2063();
void SquareAndScale_2064();
void SquareAndScale_2065();
void SquareAndScale_2066();
void SquareAndScale_2067();
void SquareAndScale_2068();
void SquareAndScale_2069();
void SquareAndScale_2070();
void SquareAndScale_2071();
void SquareAndScale_2072();
void SquareAndScale_2073();
void SquareAndScale_2074();
void SquareAndScale_2075();
void SquareAndScale_2076();
void SquareAndScale_2077();
void SquareAndScale_2078();
void SquareAndScale_2079();
void SquareAndScale_2080();
void SquareAndScale_2081();
void SquareAndScale_2082();
void SquareAndScale_2083();
void SquareAndScale_2084();
void SquareAndScale_2085();
void SquareAndScale_2086();
void SquareAndScale_2087();
void SquareAndScale_2088();
void WEIGHTED_ROUND_ROBIN_Joiner_2040();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_2036();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_2037();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1

#define __DEFLOOPBOUND__72__ -1

#define __DEFLOOPBOUND__73__ -1

#define __DEFLOOPBOUND__74__ -1

#define __DEFLOOPBOUND__75__ -1

#define __DEFLOOPBOUND__76__ -1

#define __DEFLOOPBOUND__77__ -1

#define __DEFLOOPBOUND__78__ -1

#define __DEFLOOPBOUND__79__ -1

#define __DEFLOOPBOUND__80__ -1

#define __DEFLOOPBOUND__81__ -1

#define __DEFLOOPBOUND__82__ -1

#define __DEFLOOPBOUND__83__ -1

#define __DEFLOOPBOUND__84__ -1

#define __DEFLOOPBOUND__85__ -1

#define __DEFLOOPBOUND__86__ -1

#define __DEFLOOPBOUND__87__ -1

#define __DEFLOOPBOUND__88__ -1

#define __DEFLOOPBOUND__89__ -1

#define __DEFLOOPBOUND__90__ -1

#define __DEFLOOPBOUND__91__ -1

#define __DEFLOOPBOUND__92__ -1

#define __DEFLOOPBOUND__93__ -1

#define __DEFLOOPBOUND__94__ -1

#define __DEFLOOPBOUND__95__ -1

#define __DEFLOOPBOUND__96__ -1

#define __DEFLOOPBOUND__97__ -1

#define __DEFLOOPBOUND__98__ -1

#define __DEFLOOPBOUND__99__ -1

#define __DEFLOOPBOUND__100__ -1

#define __DEFLOOPBOUND__101__ -1

#define __DEFLOOPBOUND__102__ -1

#define __DEFLOOPBOUND__103__ -1

#define __DEFLOOPBOUND__104__ -1

#define __DEFLOOPBOUND__105__ -1

#define __DEFLOOPBOUND__106__ -1

#define __DEFLOOPBOUND__107__ -1


#ifdef __cplusplus
}
#endif
#endif
