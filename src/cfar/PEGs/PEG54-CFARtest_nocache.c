#include "PEG54-CFARtest_nocache.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_1405_1407_join[54];
buffer_float_t CFAR_gather_1346AnonFilter_a0_1347;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1350CFAR_gather_1346;
buffer_complex_t ComplexSource_1343WEIGHTED_ROUND_ROBIN_Splitter_1349;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1405_1407_split[54];


ComplexSource_1343_t ComplexSource_1343_s;
CFAR_gather_1346_t CFAR_gather_1346_s;

void ComplexSource_1343(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_1343_s.theta = (ComplexSource_1343_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_1343_s.theta)) * (((float) cos(ComplexSource_1343_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1343_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1343_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_1343_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1343_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1343_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1343_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_1343WEIGHTED_ROUND_ROBIN_Splitter_1349, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_1351(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1352(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1353(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1354(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1355(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1356(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1357(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1358(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1359(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1360(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1361(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1362(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1363(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1364(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1365(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1366(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1367(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1368(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1369(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1370(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1371(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1372(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[21]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[21], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1373(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[22]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[22], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1374(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[23]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[23], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1375(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[24]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[24], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1376(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[25]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[25], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1377(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[26]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[26], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1378(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[27]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[27], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1379(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[28]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[28], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1380(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[29]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[29], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1381(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[30]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[30], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1382(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[31]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[31], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1383(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[32]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[32], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1384(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[33]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[33], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1385(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[34]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[34], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1386(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[35]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[35], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1387(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[36]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[36], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1388(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[37]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[37], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1389(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[38]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[38], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1390(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[39]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[39], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1391(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[40]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[40], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1392(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[41]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[41], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1393(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[42]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[42], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1394(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[43]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[43], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1395(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[44]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[44], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1396(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[45]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[45], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1397(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[46]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[46], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1398(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[47]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[47], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1399(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[48]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[48], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1400(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[49]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[49], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1401(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[50]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[50], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1402(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[51]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[51], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1403(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[52]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[52], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_1404(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[53]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[53], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1349() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 54, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[__iter_], pop_complex(&ComplexSource_1343WEIGHTED_ROUND_ROBIN_Splitter_1349));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1350() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 54, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1350CFAR_gather_1346, pop_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_1346(){
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1346_s.pos) - 5) >= 0)), __DEFLOOPBOUND__68__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1346_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1346_s.pos) < 64)), __DEFLOOPBOUND__69__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_1350CFAR_gather_1346, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_1346AnonFilter_a0_1347, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1346_s.poke[(i - 1)] = CFAR_gather_1346_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1346_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1350CFAR_gather_1346) ; 
		CFAR_gather_1346_s.pos++ ; 
		if(CFAR_gather_1346_s.pos == 64) {
			CFAR_gather_1346_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_1347(){
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_1346AnonFilter_a0_1347));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 54, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_1346AnonFilter_a0_1347);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1350CFAR_gather_1346);
	init_buffer_complex(&ComplexSource_1343WEIGHTED_ROUND_ROBIN_Splitter_1349);
	FOR(int, __iter_init_1_, 0, <, 54, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_1343
	 {
	ComplexSource_1343_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_1343_s.theta = (ComplexSource_1343_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_1343_s.theta)) * (((float) cos(ComplexSource_1343_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1343_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1343_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_1343_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1343_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1343_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1343_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_1343WEIGHTED_ROUND_ROBIN_Splitter_1349, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1349
	
	FOR(uint32_t, __iter_, 0, <, 54, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[__iter_], pop_complex(&ComplexSource_1343WEIGHTED_ROUND_ROBIN_Splitter_1349));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_1351
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[0]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[0], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1352
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[1]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[1], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1353
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[2]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[2], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1354
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[3]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[3], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1355
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[4]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[4], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1356
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[5]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[5], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1357
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[6]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[6], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1358
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[7]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[7], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1359
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[8]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[8], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1360
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[9]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[9], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1361
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[10]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[10], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1362
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[11]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[11], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1363
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[12]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[12], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1364
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[13]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[13], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1365
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[14]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[14], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1366
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[15]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[15], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1367
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[16]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[16], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1368
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[17]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[17], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1369
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[18]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[18], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1370
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[19]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[19], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1371
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[20]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[20], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1372
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[21]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[21], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1373
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[22]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[22], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1374
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[23]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[23], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1375
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[24]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[24], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1376
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[25]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[25], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1377
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[26]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[26], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1378
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[27]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[27], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1379
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[28]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[28], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1380
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[29]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[29], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1381
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[30]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[30], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1382
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[31]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[31], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1383
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[32]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[32], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1384
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[33]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[33], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1385
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[34]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[34], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1386
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[35]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[35], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1387
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[36]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[36], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1388
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[37]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[37], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1389
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[38]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[38], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1390
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[39]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[39], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1391
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[40]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[40], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1392
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[41]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[41], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1393
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[42]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[42], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1394
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[43]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[43], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1395
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[44]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[44], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1396
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[45]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[45], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1397
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[46]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[46], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1398
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[47]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[47], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1399
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[48]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[48], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1400
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[49]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[49], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1401
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[50]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[50], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1402
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[51]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[51], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1403
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[52]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[52], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_1404
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_1405_1407_split[53]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[53], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1350
	
	FOR(uint32_t, __iter_, 0, <, 54, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1350CFAR_gather_1346, pop_float(&SplitJoin0_SquareAndScale_Fiss_1405_1407_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_1346
	 {
	CFAR_gather_1346_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 45, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1346_s.pos) - 5) >= 0)), __DEFLOOPBOUND__70__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1346_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1346_s.pos) < 64)), __DEFLOOPBOUND__71__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_1350CFAR_gather_1346, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_1346AnonFilter_a0_1347, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1346_s.poke[(i - 1)] = CFAR_gather_1346_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1346_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1350CFAR_gather_1346) ; 
		CFAR_gather_1346_s.pos++ ; 
		if(CFAR_gather_1346_s.pos == 64) {
			CFAR_gather_1346_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_1347
	FOR(uint32_t, __iter_init_, 0, <, 45, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_1346AnonFilter_a0_1347));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_1343();
		WEIGHTED_ROUND_ROBIN_Splitter_1349();
			SquareAndScale_1351();
			SquareAndScale_1352();
			SquareAndScale_1353();
			SquareAndScale_1354();
			SquareAndScale_1355();
			SquareAndScale_1356();
			SquareAndScale_1357();
			SquareAndScale_1358();
			SquareAndScale_1359();
			SquareAndScale_1360();
			SquareAndScale_1361();
			SquareAndScale_1362();
			SquareAndScale_1363();
			SquareAndScale_1364();
			SquareAndScale_1365();
			SquareAndScale_1366();
			SquareAndScale_1367();
			SquareAndScale_1368();
			SquareAndScale_1369();
			SquareAndScale_1370();
			SquareAndScale_1371();
			SquareAndScale_1372();
			SquareAndScale_1373();
			SquareAndScale_1374();
			SquareAndScale_1375();
			SquareAndScale_1376();
			SquareAndScale_1377();
			SquareAndScale_1378();
			SquareAndScale_1379();
			SquareAndScale_1380();
			SquareAndScale_1381();
			SquareAndScale_1382();
			SquareAndScale_1383();
			SquareAndScale_1384();
			SquareAndScale_1385();
			SquareAndScale_1386();
			SquareAndScale_1387();
			SquareAndScale_1388();
			SquareAndScale_1389();
			SquareAndScale_1390();
			SquareAndScale_1391();
			SquareAndScale_1392();
			SquareAndScale_1393();
			SquareAndScale_1394();
			SquareAndScale_1395();
			SquareAndScale_1396();
			SquareAndScale_1397();
			SquareAndScale_1398();
			SquareAndScale_1399();
			SquareAndScale_1400();
			SquareAndScale_1401();
			SquareAndScale_1402();
			SquareAndScale_1403();
			SquareAndScale_1404();
		WEIGHTED_ROUND_ROBIN_Joiner_1350();
		CFAR_gather_1346();
		AnonFilter_a0_1347();
	ENDFOR
	return EXIT_SUCCESS;
}
