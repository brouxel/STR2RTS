#include "PEG63-CFARtest_nocache.h"

buffer_complex_t ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_244_246_split[63];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_244_246_join[63];
buffer_float_t CFAR_gather_176AnonFilter_a0_177;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176;


ComplexSource_173_t ComplexSource_173_s;
CFAR_gather_176_t CFAR_gather_176_s;

void ComplexSource_173(){
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_173_s.theta = (ComplexSource_173_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_173_s.theta)) * (((float) cos(ComplexSource_173_s.theta)) + ((0.0 * ((float) sin(ComplexSource_173_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_173_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_173_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_173_s.theta))))) + (0.0 * (((float) cos(ComplexSource_173_s.theta)) + ((0.0 * ((float) sin(ComplexSource_173_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_181(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_182(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_183(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_184(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_185(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_186(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_187(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_188(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_189(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_190(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_191(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_192(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_193(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_194(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_195(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_196(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_197(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_198(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_199(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_200(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_201(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_202(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[21]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[21], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_203(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[22]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[22], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_204(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[23]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[23], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_205(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[24]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[24], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_206(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[25]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[25], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_207(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[26]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[26], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_208(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[27]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[27], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_209(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[28]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[28], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_210(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[29]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[29], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_211(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[30]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[30], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_212(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[31]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[31], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_213(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[32]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[32], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_214(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[33]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[33], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_215(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[34]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[34], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_216(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[35]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[35], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_217(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[36]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[36], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_218(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[37]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[37], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_219(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[38]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[38], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_220(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[39]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[39], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_221(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[40]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[40], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_222(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[41]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[41], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_223(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[42]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[42], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_224(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[43]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[43], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_225(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[44]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[44], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_226(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[45]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[45], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_227(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[46]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[46], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_228(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[47]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[47], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_229(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[48]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[48], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_230(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[49]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[49], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_231(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[50]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[50], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_232(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[51]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[51], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_233(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[52]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[52], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_234(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[53]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[53], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_235(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[54]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[54], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_236(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[55]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[55], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_237(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[56]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[56], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_238(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[57]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[57], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_239(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[58]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[58], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_240(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[59]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[59], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_241(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[60]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[60], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_242(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[61]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[61], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_243(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[62]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[62], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[__iter_], pop_complex(&ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176, pop_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_176(){
	FOR(uint32_t, __iter_steady_, 0, <, 4032, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_176_s.pos) - 5) >= 0)), __DEFLOOPBOUND__14__, i__conflict__1++) {
			sum = (sum + CFAR_gather_176_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_176_s.pos) < 64)), __DEFLOOPBOUND__15__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_176AnonFilter_a0_177, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_176_s.poke[(i - 1)] = CFAR_gather_176_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_176_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176) ; 
		CFAR_gather_176_s.pos++ ; 
		if(CFAR_gather_176_s.pos == 64) {
			CFAR_gather_176_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_177(){
	FOR(uint32_t, __iter_steady_, 0, <, 4032, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_176AnonFilter_a0_177));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179);
	FOR(int, __iter_init_0_, 0, <, 63, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 63, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_176AnonFilter_a0_177);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176);
// --- init: ComplexSource_173
	 {
	ComplexSource_173_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_173_s.theta = (ComplexSource_173_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_173_s.theta)) * (((float) cos(ComplexSource_173_s.theta)) + ((0.0 * ((float) sin(ComplexSource_173_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_173_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_173_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_173_s.theta))))) + (0.0 * (((float) cos(ComplexSource_173_s.theta)) + ((0.0 * ((float) sin(ComplexSource_173_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_179
	
	FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[__iter_], pop_complex(&ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_181
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[0]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[0], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_182
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[1]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[1], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_183
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[2]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[2], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_184
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[3]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[3], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_185
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[4]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[4], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_186
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[5]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[5], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_187
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[6]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[6], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_188
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[7]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[7], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_189
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[8]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[8], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_190
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[9]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[9], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_191
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[10]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[10], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_192
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[11]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[11], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_193
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[12]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[12], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_194
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[13]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[13], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_195
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[14]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[14], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_196
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[15]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[15], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_197
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[16]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[16], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_198
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[17]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[17], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_199
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[18]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[18], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_200
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[19]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[19], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_201
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[20]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[20], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_202
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[21]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[21], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_203
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[22]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[22], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_204
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[23]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[23], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_205
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[24]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[24], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_206
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[25]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[25], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_207
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[26]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[26], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_208
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[27]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[27], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_209
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[28]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[28], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_210
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[29]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[29], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_211
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[30]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[30], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_212
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[31]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[31], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_213
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[32]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[32], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_214
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[33]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[33], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_215
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[34]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[34], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_216
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[35]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[35], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_217
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[36]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[36], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_218
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[37]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[37], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_219
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[38]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[38], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_220
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[39]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[39], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_221
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[40]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[40], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_222
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[41]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[41], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_223
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[42]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[42], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_224
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[43]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[43], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_225
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[44]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[44], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_226
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[45]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[45], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_227
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[46]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[46], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_228
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[47]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[47], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_229
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[48]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[48], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_230
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[49]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[49], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_231
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[50]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[50], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_232
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[51]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[51], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_233
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[52]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[52], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_234
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[53]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[53], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_235
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[54]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[54], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_236
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[55]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[55], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_237
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[56]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[56], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_238
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[57]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[57], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_239
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[58]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[58], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_240
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[59]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[59], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_241
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[60]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[60], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_242
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[61]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[61], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: SquareAndScale_243
	 {
	complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[62]));
	float mag = 0.0;
	mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
	push_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[62], ((mag * mag) / 10.0)) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_180
	
	FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176, pop_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_176
	 {
	CFAR_gather_176_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_176_s.pos) - 5) >= 0)), __DEFLOOPBOUND__16__, i__conflict__1++) {
			sum = (sum + CFAR_gather_176_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_176_s.pos) < 64)), __DEFLOOPBOUND__17__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_176AnonFilter_a0_177, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_176_s.poke[(i - 1)] = CFAR_gather_176_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_176_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176) ; 
		CFAR_gather_176_s.pos++ ; 
		if(CFAR_gather_176_s.pos == 64) {
			CFAR_gather_176_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_177
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_176AnonFilter_a0_177));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_173();
		WEIGHTED_ROUND_ROBIN_Splitter_179();
			SquareAndScale_181();
			SquareAndScale_182();
			SquareAndScale_183();
			SquareAndScale_184();
			SquareAndScale_185();
			SquareAndScale_186();
			SquareAndScale_187();
			SquareAndScale_188();
			SquareAndScale_189();
			SquareAndScale_190();
			SquareAndScale_191();
			SquareAndScale_192();
			SquareAndScale_193();
			SquareAndScale_194();
			SquareAndScale_195();
			SquareAndScale_196();
			SquareAndScale_197();
			SquareAndScale_198();
			SquareAndScale_199();
			SquareAndScale_200();
			SquareAndScale_201();
			SquareAndScale_202();
			SquareAndScale_203();
			SquareAndScale_204();
			SquareAndScale_205();
			SquareAndScale_206();
			SquareAndScale_207();
			SquareAndScale_208();
			SquareAndScale_209();
			SquareAndScale_210();
			SquareAndScale_211();
			SquareAndScale_212();
			SquareAndScale_213();
			SquareAndScale_214();
			SquareAndScale_215();
			SquareAndScale_216();
			SquareAndScale_217();
			SquareAndScale_218();
			SquareAndScale_219();
			SquareAndScale_220();
			SquareAndScale_221();
			SquareAndScale_222();
			SquareAndScale_223();
			SquareAndScale_224();
			SquareAndScale_225();
			SquareAndScale_226();
			SquareAndScale_227();
			SquareAndScale_228();
			SquareAndScale_229();
			SquareAndScale_230();
			SquareAndScale_231();
			SquareAndScale_232();
			SquareAndScale_233();
			SquareAndScale_234();
			SquareAndScale_235();
			SquareAndScale_236();
			SquareAndScale_237();
			SquareAndScale_238();
			SquareAndScale_239();
			SquareAndScale_240();
			SquareAndScale_241();
			SquareAndScale_242();
			SquareAndScale_243();
		WEIGHTED_ROUND_ROBIN_Joiner_180();
		CFAR_gather_176();
		AnonFilter_a0_177();
	ENDFOR
	return EXIT_SUCCESS;
}
