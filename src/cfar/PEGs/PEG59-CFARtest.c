#include "PEG59-CFARtest.h"

buffer_complex_t ComplexSource_713WEIGHTED_ROUND_ROBIN_Splitter_719;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_780_782_split[59];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_720CFAR_gather_716;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_780_782_join[59];
buffer_float_t CFAR_gather_716AnonFilter_a0_717;


ComplexSource_713_t ComplexSource_713_s;
CFAR_gather_716_t CFAR_gather_716_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_713_s.theta = (ComplexSource_713_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_713_s.theta)) * (((float) cos(ComplexSource_713_s.theta)) + ((0.0 * ((float) sin(ComplexSource_713_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_713_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_713_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_713_s.theta))))) + (0.0 * (((float) cos(ComplexSource_713_s.theta)) + ((0.0 * ((float) sin(ComplexSource_713_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_713() {
	FOR(uint32_t, __iter_steady_, 0, <, 59, __iter_steady_++)
		ComplexSource(&(ComplexSource_713WEIGHTED_ROUND_ROBIN_Splitter_719));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_721() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[0]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[0]));
	ENDFOR
}

void SquareAndScale_722() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[1]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[1]));
	ENDFOR
}

void SquareAndScale_723() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[2]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[2]));
	ENDFOR
}

void SquareAndScale_724() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[3]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[3]));
	ENDFOR
}

void SquareAndScale_725() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[4]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[4]));
	ENDFOR
}

void SquareAndScale_726() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[5]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[5]));
	ENDFOR
}

void SquareAndScale_727() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[6]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[6]));
	ENDFOR
}

void SquareAndScale_728() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[7]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[7]));
	ENDFOR
}

void SquareAndScale_729() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[8]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[8]));
	ENDFOR
}

void SquareAndScale_730() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[9]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[9]));
	ENDFOR
}

void SquareAndScale_731() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[10]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[10]));
	ENDFOR
}

void SquareAndScale_732() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[11]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[11]));
	ENDFOR
}

void SquareAndScale_733() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[12]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[12]));
	ENDFOR
}

void SquareAndScale_734() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[13]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[13]));
	ENDFOR
}

void SquareAndScale_735() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[14]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[14]));
	ENDFOR
}

void SquareAndScale_736() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[15]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[15]));
	ENDFOR
}

void SquareAndScale_737() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[16]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[16]));
	ENDFOR
}

void SquareAndScale_738() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[17]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[17]));
	ENDFOR
}

void SquareAndScale_739() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[18]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[18]));
	ENDFOR
}

void SquareAndScale_740() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[19]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[19]));
	ENDFOR
}

void SquareAndScale_741() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[20]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[20]));
	ENDFOR
}

void SquareAndScale_742() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[21]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[21]));
	ENDFOR
}

void SquareAndScale_743() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[22]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[22]));
	ENDFOR
}

void SquareAndScale_744() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[23]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[23]));
	ENDFOR
}

void SquareAndScale_745() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[24]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[24]));
	ENDFOR
}

void SquareAndScale_746() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[25]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[25]));
	ENDFOR
}

void SquareAndScale_747() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[26]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[26]));
	ENDFOR
}

void SquareAndScale_748() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[27]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[27]));
	ENDFOR
}

void SquareAndScale_749() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[28]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[28]));
	ENDFOR
}

void SquareAndScale_750() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[29]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[29]));
	ENDFOR
}

void SquareAndScale_751() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[30]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[30]));
	ENDFOR
}

void SquareAndScale_752() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[31]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[31]));
	ENDFOR
}

void SquareAndScale_753() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[32]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[32]));
	ENDFOR
}

void SquareAndScale_754() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[33]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[33]));
	ENDFOR
}

void SquareAndScale_755() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[34]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[34]));
	ENDFOR
}

void SquareAndScale_756() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[35]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[35]));
	ENDFOR
}

void SquareAndScale_757() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[36]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[36]));
	ENDFOR
}

void SquareAndScale_758() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[37]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[37]));
	ENDFOR
}

void SquareAndScale_759() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[38]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[38]));
	ENDFOR
}

void SquareAndScale_760() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[39]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[39]));
	ENDFOR
}

void SquareAndScale_761() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[40]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[40]));
	ENDFOR
}

void SquareAndScale_762() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[41]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[41]));
	ENDFOR
}

void SquareAndScale_763() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[42]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[42]));
	ENDFOR
}

void SquareAndScale_764() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[43]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[43]));
	ENDFOR
}

void SquareAndScale_765() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[44]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[44]));
	ENDFOR
}

void SquareAndScale_766() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[45]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[45]));
	ENDFOR
}

void SquareAndScale_767() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[46]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[46]));
	ENDFOR
}

void SquareAndScale_768() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[47]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[47]));
	ENDFOR
}

void SquareAndScale_769() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[48]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[48]));
	ENDFOR
}

void SquareAndScale_770() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[49]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[49]));
	ENDFOR
}

void SquareAndScale_771() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[50]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[50]));
	ENDFOR
}

void SquareAndScale_772() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[51]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[51]));
	ENDFOR
}

void SquareAndScale_773() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[52]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[52]));
	ENDFOR
}

void SquareAndScale_774() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[53]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[53]));
	ENDFOR
}

void SquareAndScale_775() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[54]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[54]));
	ENDFOR
}

void SquareAndScale_776() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[55]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[55]));
	ENDFOR
}

void SquareAndScale_777() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[56]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[56]));
	ENDFOR
}

void SquareAndScale_778() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[57]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[57]));
	ENDFOR
}

void SquareAndScale_779() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[58]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[58]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_719() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 59, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_780_782_split[__iter_], pop_complex(&ComplexSource_713WEIGHTED_ROUND_ROBIN_Splitter_719));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_720() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 59, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_720CFAR_gather_716, pop_float(&SplitJoin0_SquareAndScale_Fiss_780_782_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_716_s.pos) - 5) >= 0)), __DEFLOOPBOUND__38__, i__conflict__1++) {
			sum = (sum + CFAR_gather_716_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_716_s.pos) < 64)), __DEFLOOPBOUND__39__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_716_s.poke[(i - 1)] = CFAR_gather_716_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_716_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_716_s.pos++ ; 
		if(CFAR_gather_716_s.pos == 64) {
			CFAR_gather_716_s.pos = 0 ; 
		}
	}


void CFAR_gather_716() {
	FOR(uint32_t, __iter_steady_, 0, <, 3776, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_720CFAR_gather_716), &(CFAR_gather_716AnonFilter_a0_717));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_717() {
	FOR(uint32_t, __iter_steady_, 0, <, 3776, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_716AnonFilter_a0_717));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_713WEIGHTED_ROUND_ROBIN_Splitter_719);
	FOR(int, __iter_init_0_, 0, <, 59, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_780_782_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_720CFAR_gather_716);
	FOR(int, __iter_init_1_, 0, <, 59, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_780_782_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_716AnonFilter_a0_717);
// --- init: ComplexSource_713
	 {
	ComplexSource_713_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_713WEIGHTED_ROUND_ROBIN_Splitter_719));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_719
	
	FOR(uint32_t, __iter_, 0, <, 59, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_780_782_split[__iter_], pop_complex(&ComplexSource_713WEIGHTED_ROUND_ROBIN_Splitter_719));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_721
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[0]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[0]));
//--------------------------------
// --- init: SquareAndScale_722
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[1]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[1]));
//--------------------------------
// --- init: SquareAndScale_723
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[2]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[2]));
//--------------------------------
// --- init: SquareAndScale_724
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[3]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[3]));
//--------------------------------
// --- init: SquareAndScale_725
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[4]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[4]));
//--------------------------------
// --- init: SquareAndScale_726
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[5]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[5]));
//--------------------------------
// --- init: SquareAndScale_727
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[6]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[6]));
//--------------------------------
// --- init: SquareAndScale_728
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[7]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[7]));
//--------------------------------
// --- init: SquareAndScale_729
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[8]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[8]));
//--------------------------------
// --- init: SquareAndScale_730
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[9]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[9]));
//--------------------------------
// --- init: SquareAndScale_731
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[10]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[10]));
//--------------------------------
// --- init: SquareAndScale_732
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[11]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[11]));
//--------------------------------
// --- init: SquareAndScale_733
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[12]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[12]));
//--------------------------------
// --- init: SquareAndScale_734
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[13]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[13]));
//--------------------------------
// --- init: SquareAndScale_735
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[14]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[14]));
//--------------------------------
// --- init: SquareAndScale_736
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[15]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[15]));
//--------------------------------
// --- init: SquareAndScale_737
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[16]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[16]));
//--------------------------------
// --- init: SquareAndScale_738
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[17]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[17]));
//--------------------------------
// --- init: SquareAndScale_739
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[18]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[18]));
//--------------------------------
// --- init: SquareAndScale_740
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[19]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[19]));
//--------------------------------
// --- init: SquareAndScale_741
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[20]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[20]));
//--------------------------------
// --- init: SquareAndScale_742
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[21]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[21]));
//--------------------------------
// --- init: SquareAndScale_743
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[22]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[22]));
//--------------------------------
// --- init: SquareAndScale_744
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[23]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[23]));
//--------------------------------
// --- init: SquareAndScale_745
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[24]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[24]));
//--------------------------------
// --- init: SquareAndScale_746
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[25]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[25]));
//--------------------------------
// --- init: SquareAndScale_747
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[26]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[26]));
//--------------------------------
// --- init: SquareAndScale_748
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[27]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[27]));
//--------------------------------
// --- init: SquareAndScale_749
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[28]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[28]));
//--------------------------------
// --- init: SquareAndScale_750
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[29]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[29]));
//--------------------------------
// --- init: SquareAndScale_751
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[30]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[30]));
//--------------------------------
// --- init: SquareAndScale_752
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[31]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[31]));
//--------------------------------
// --- init: SquareAndScale_753
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[32]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[32]));
//--------------------------------
// --- init: SquareAndScale_754
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[33]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[33]));
//--------------------------------
// --- init: SquareAndScale_755
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[34]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[34]));
//--------------------------------
// --- init: SquareAndScale_756
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[35]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[35]));
//--------------------------------
// --- init: SquareAndScale_757
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[36]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[36]));
//--------------------------------
// --- init: SquareAndScale_758
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[37]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[37]));
//--------------------------------
// --- init: SquareAndScale_759
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[38]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[38]));
//--------------------------------
// --- init: SquareAndScale_760
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[39]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[39]));
//--------------------------------
// --- init: SquareAndScale_761
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[40]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[40]));
//--------------------------------
// --- init: SquareAndScale_762
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[41]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[41]));
//--------------------------------
// --- init: SquareAndScale_763
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[42]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[42]));
//--------------------------------
// --- init: SquareAndScale_764
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[43]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[43]));
//--------------------------------
// --- init: SquareAndScale_765
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[44]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[44]));
//--------------------------------
// --- init: SquareAndScale_766
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[45]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[45]));
//--------------------------------
// --- init: SquareAndScale_767
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[46]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[46]));
//--------------------------------
// --- init: SquareAndScale_768
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[47]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[47]));
//--------------------------------
// --- init: SquareAndScale_769
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[48]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[48]));
//--------------------------------
// --- init: SquareAndScale_770
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[49]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[49]));
//--------------------------------
// --- init: SquareAndScale_771
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[50]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[50]));
//--------------------------------
// --- init: SquareAndScale_772
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[51]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[51]));
//--------------------------------
// --- init: SquareAndScale_773
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[52]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[52]));
//--------------------------------
// --- init: SquareAndScale_774
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[53]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[53]));
//--------------------------------
// --- init: SquareAndScale_775
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[54]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[54]));
//--------------------------------
// --- init: SquareAndScale_776
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[55]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[55]));
//--------------------------------
// --- init: SquareAndScale_777
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[56]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[56]));
//--------------------------------
// --- init: SquareAndScale_778
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[57]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[57]));
//--------------------------------
// --- init: SquareAndScale_779
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_780_782_split[58]), &(SplitJoin0_SquareAndScale_Fiss_780_782_join[58]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_720
	
	FOR(uint32_t, __iter_, 0, <, 59, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_720CFAR_gather_716, pop_float(&SplitJoin0_SquareAndScale_Fiss_780_782_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_716
	 {
	CFAR_gather_716_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 50, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_720CFAR_gather_716), &(CFAR_gather_716AnonFilter_a0_717));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_717
	FOR(uint32_t, __iter_init_, 0, <, 50, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_716AnonFilter_a0_717));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_713();
		WEIGHTED_ROUND_ROBIN_Splitter_719();
			SquareAndScale_721();
			SquareAndScale_722();
			SquareAndScale_723();
			SquareAndScale_724();
			SquareAndScale_725();
			SquareAndScale_726();
			SquareAndScale_727();
			SquareAndScale_728();
			SquareAndScale_729();
			SquareAndScale_730();
			SquareAndScale_731();
			SquareAndScale_732();
			SquareAndScale_733();
			SquareAndScale_734();
			SquareAndScale_735();
			SquareAndScale_736();
			SquareAndScale_737();
			SquareAndScale_738();
			SquareAndScale_739();
			SquareAndScale_740();
			SquareAndScale_741();
			SquareAndScale_742();
			SquareAndScale_743();
			SquareAndScale_744();
			SquareAndScale_745();
			SquareAndScale_746();
			SquareAndScale_747();
			SquareAndScale_748();
			SquareAndScale_749();
			SquareAndScale_750();
			SquareAndScale_751();
			SquareAndScale_752();
			SquareAndScale_753();
			SquareAndScale_754();
			SquareAndScale_755();
			SquareAndScale_756();
			SquareAndScale_757();
			SquareAndScale_758();
			SquareAndScale_759();
			SquareAndScale_760();
			SquareAndScale_761();
			SquareAndScale_762();
			SquareAndScale_763();
			SquareAndScale_764();
			SquareAndScale_765();
			SquareAndScale_766();
			SquareAndScale_767();
			SquareAndScale_768();
			SquareAndScale_769();
			SquareAndScale_770();
			SquareAndScale_771();
			SquareAndScale_772();
			SquareAndScale_773();
			SquareAndScale_774();
			SquareAndScale_775();
			SquareAndScale_776();
			SquareAndScale_777();
			SquareAndScale_778();
			SquareAndScale_779();
		WEIGHTED_ROUND_ROBIN_Joiner_720();
		CFAR_gather_716();
		AnonFilter_a0_717();
	ENDFOR
	return EXIT_SUCCESS;
}
