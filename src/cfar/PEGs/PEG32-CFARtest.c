#include "PEG32-CFARtest.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_3561_3563_join[32];
buffer_complex_t ComplexSource_3521WEIGHTED_ROUND_ROBIN_Splitter_3527;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3561_3563_split[32];
buffer_float_t CFAR_gather_3524AnonFilter_a0_3525;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3528CFAR_gather_3524;


ComplexSource_3521_t ComplexSource_3521_s;
CFAR_gather_3524_t CFAR_gather_3524_s;

void ComplexSource(buffer_complex_t *chanout) {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_3521_s.theta = (ComplexSource_3521_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_3521_s.theta)) * (((float) cos(ComplexSource_3521_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3521_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3521_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_3521_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3521_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3521_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3521_s.theta))) - 0.0)))) ; 
		push_complex(&(*chanout), c) ; 
	}
	ENDFOR
}


void ComplexSource_3521() {
	ComplexSource(&(ComplexSource_3521WEIGHTED_ROUND_ROBIN_Splitter_3527));
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3529() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[0]));
	ENDFOR
}

void SquareAndScale_3530() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[1]));
	ENDFOR
}

void SquareAndScale_3531() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[2]));
	ENDFOR
}

void SquareAndScale_3532() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[3]));
	ENDFOR
}

void SquareAndScale_3533() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[4]));
	ENDFOR
}

void SquareAndScale_3534() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[5]));
	ENDFOR
}

void SquareAndScale_3535() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[6]));
	ENDFOR
}

void SquareAndScale_3536() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[7]));
	ENDFOR
}

void SquareAndScale_3537() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[8]));
	ENDFOR
}

void SquareAndScale_3538() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[9]));
	ENDFOR
}

void SquareAndScale_3539() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[10]));
	ENDFOR
}

void SquareAndScale_3540() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[11]));
	ENDFOR
}

void SquareAndScale_3541() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[12]));
	ENDFOR
}

void SquareAndScale_3542() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[13]));
	ENDFOR
}

void SquareAndScale_3543() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[14]));
	ENDFOR
}

void SquareAndScale_3544() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[15]));
	ENDFOR
}

void SquareAndScale_3545() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[16]));
	ENDFOR
}

void SquareAndScale_3546() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[17]));
	ENDFOR
}

void SquareAndScale_3547() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[18]));
	ENDFOR
}

void SquareAndScale_3548() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[19]));
	ENDFOR
}

void SquareAndScale_3549() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[20]));
	ENDFOR
}

void SquareAndScale_3550() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[21]));
	ENDFOR
}

void SquareAndScale_3551() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[22]));
	ENDFOR
}

void SquareAndScale_3552() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[23]));
	ENDFOR
}

void SquareAndScale_3553() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[24]));
	ENDFOR
}

void SquareAndScale_3554() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[25]));
	ENDFOR
}

void SquareAndScale_3555() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[26]));
	ENDFOR
}

void SquareAndScale_3556() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[27]));
	ENDFOR
}

void SquareAndScale_3557() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[28]));
	ENDFOR
}

void SquareAndScale_3558() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[29]));
	ENDFOR
}

void SquareAndScale_3559() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[30]));
	ENDFOR
}

void SquareAndScale_3560() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[31]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3527() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3561_3563_split[__iter_], pop_complex(&ComplexSource_3521WEIGHTED_ROUND_ROBIN_Splitter_3527));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3528() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3528CFAR_gather_3524, pop_float(&SplitJoin0_SquareAndScale_Fiss_3561_3563_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3524_s.pos) - 5) >= 0)), __DEFLOOPBOUND__200__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3524_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3524_s.pos) < 64)), __DEFLOOPBOUND__201__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3524_s.poke[(i - 1)] = CFAR_gather_3524_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3524_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3524_s.pos++ ; 
		if(CFAR_gather_3524_s.pos == 64) {
			CFAR_gather_3524_s.pos = 0 ; 
		}
	}


void CFAR_gather_3524() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3528CFAR_gather_3524), &(CFAR_gather_3524AnonFilter_a0_3525));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3525() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3524AnonFilter_a0_3525));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 32, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3561_3563_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_3521WEIGHTED_ROUND_ROBIN_Splitter_3527);
	FOR(int, __iter_init_1_, 0, <, 32, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3561_3563_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_3524AnonFilter_a0_3525);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3528CFAR_gather_3524);
// --- init: ComplexSource_3521
	 {
	ComplexSource_3521_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3521WEIGHTED_ROUND_ROBIN_Splitter_3527));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3527
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3561_3563_split[__iter_], pop_complex(&ComplexSource_3521WEIGHTED_ROUND_ROBIN_Splitter_3527));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3529
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3530
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3531
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3532
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3533
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3534
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3535
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3536
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3537
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3538
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3539
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3540
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3541
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3542
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3543
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3544
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3545
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3546
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3547
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3548
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3549
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3550
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3551
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[22]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3552
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[23]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3553
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[24]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3554
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[25]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3555
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[26]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3556
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[27]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3557
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[28]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3558
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[29]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3559
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[30]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3560
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3561_3563_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3561_3563_join[31]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3528
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3528CFAR_gather_3524, pop_float(&SplitJoin0_SquareAndScale_Fiss_3561_3563_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3524
	 {
	CFAR_gather_3524_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3528CFAR_gather_3524), &(CFAR_gather_3524AnonFilter_a0_3525));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3525
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3524AnonFilter_a0_3525));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3521();
		WEIGHTED_ROUND_ROBIN_Splitter_3527();
			SquareAndScale_3529();
			SquareAndScale_3530();
			SquareAndScale_3531();
			SquareAndScale_3532();
			SquareAndScale_3533();
			SquareAndScale_3534();
			SquareAndScale_3535();
			SquareAndScale_3536();
			SquareAndScale_3537();
			SquareAndScale_3538();
			SquareAndScale_3539();
			SquareAndScale_3540();
			SquareAndScale_3541();
			SquareAndScale_3542();
			SquareAndScale_3543();
			SquareAndScale_3544();
			SquareAndScale_3545();
			SquareAndScale_3546();
			SquareAndScale_3547();
			SquareAndScale_3548();
			SquareAndScale_3549();
			SquareAndScale_3550();
			SquareAndScale_3551();
			SquareAndScale_3552();
			SquareAndScale_3553();
			SquareAndScale_3554();
			SquareAndScale_3555();
			SquareAndScale_3556();
			SquareAndScale_3557();
			SquareAndScale_3558();
			SquareAndScale_3559();
			SquareAndScale_3560();
		WEIGHTED_ROUND_ROBIN_Joiner_3528();
		CFAR_gather_3524();
		AnonFilter_a0_3525();
	ENDFOR
	return EXIT_SUCCESS;
}
