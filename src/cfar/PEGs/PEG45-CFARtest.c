#include "PEG45-CFARtest.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2404_2406_split[45];
buffer_complex_t ComplexSource_2351WEIGHTED_ROUND_ROBIN_Splitter_2357;
buffer_float_t CFAR_gather_2354AnonFilter_a0_2355;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2358CFAR_gather_2354;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_2404_2406_join[45];


ComplexSource_2351_t ComplexSource_2351_s;
CFAR_gather_2354_t CFAR_gather_2354_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2351_s.theta = (ComplexSource_2351_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2351_s.theta)) * (((float) cos(ComplexSource_2351_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2351_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2351_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2351_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2351_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2351_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2351_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2351() {
	FOR(uint32_t, __iter_steady_, 0, <, 45, __iter_steady_++)
		ComplexSource(&(ComplexSource_2351WEIGHTED_ROUND_ROBIN_Splitter_2357));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2359() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[0]));
	ENDFOR
}

void SquareAndScale_2360() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[1]));
	ENDFOR
}

void SquareAndScale_2361() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[2]));
	ENDFOR
}

void SquareAndScale_2362() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[3]));
	ENDFOR
}

void SquareAndScale_2363() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[4]));
	ENDFOR
}

void SquareAndScale_2364() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[5]));
	ENDFOR
}

void SquareAndScale_2365() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[6]));
	ENDFOR
}

void SquareAndScale_2366() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[7]));
	ENDFOR
}

void SquareAndScale_2367() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[8]));
	ENDFOR
}

void SquareAndScale_2368() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[9]));
	ENDFOR
}

void SquareAndScale_2369() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[10]));
	ENDFOR
}

void SquareAndScale_2370() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[11]));
	ENDFOR
}

void SquareAndScale_2371() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[12]));
	ENDFOR
}

void SquareAndScale_2372() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[13]));
	ENDFOR
}

void SquareAndScale_2373() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[14]));
	ENDFOR
}

void SquareAndScale_2374() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[15]));
	ENDFOR
}

void SquareAndScale_2375() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[16]));
	ENDFOR
}

void SquareAndScale_2376() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[17]));
	ENDFOR
}

void SquareAndScale_2377() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[18]));
	ENDFOR
}

void SquareAndScale_2378() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[19]));
	ENDFOR
}

void SquareAndScale_2379() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[20]));
	ENDFOR
}

void SquareAndScale_2380() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[21]));
	ENDFOR
}

void SquareAndScale_2381() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[22]));
	ENDFOR
}

void SquareAndScale_2382() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[23]));
	ENDFOR
}

void SquareAndScale_2383() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[24]));
	ENDFOR
}

void SquareAndScale_2384() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[25]));
	ENDFOR
}

void SquareAndScale_2385() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[26]));
	ENDFOR
}

void SquareAndScale_2386() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[27]));
	ENDFOR
}

void SquareAndScale_2387() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[28]));
	ENDFOR
}

void SquareAndScale_2388() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[29]));
	ENDFOR
}

void SquareAndScale_2389() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[30]));
	ENDFOR
}

void SquareAndScale_2390() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[31]));
	ENDFOR
}

void SquareAndScale_2391() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[32]));
	ENDFOR
}

void SquareAndScale_2392() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[33]));
	ENDFOR
}

void SquareAndScale_2393() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[34]));
	ENDFOR
}

void SquareAndScale_2394() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[35]));
	ENDFOR
}

void SquareAndScale_2395() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[36]));
	ENDFOR
}

void SquareAndScale_2396() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[37]));
	ENDFOR
}

void SquareAndScale_2397() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[38]));
	ENDFOR
}

void SquareAndScale_2398() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[39]));
	ENDFOR
}

void SquareAndScale_2399() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[40]));
	ENDFOR
}

void SquareAndScale_2400() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[41]));
	ENDFOR
}

void SquareAndScale_2401() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[42]));
	ENDFOR
}

void SquareAndScale_2402() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[43]));
	ENDFOR
}

void SquareAndScale_2403() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[44]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[44]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2357() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 45, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2404_2406_split[__iter_], pop_complex(&ComplexSource_2351WEIGHTED_ROUND_ROBIN_Splitter_2357));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2358() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 45, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2358CFAR_gather_2354, pop_float(&SplitJoin0_SquareAndScale_Fiss_2404_2406_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2354_s.pos) - 5) >= 0)), __DEFLOOPBOUND__122__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2354_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2354_s.pos) < 64)), __DEFLOOPBOUND__123__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2354_s.poke[(i - 1)] = CFAR_gather_2354_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2354_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2354_s.pos++ ; 
		if(CFAR_gather_2354_s.pos == 64) {
			CFAR_gather_2354_s.pos = 0 ; 
		}
	}


void CFAR_gather_2354() {
	FOR(uint32_t, __iter_steady_, 0, <, 2880, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2358CFAR_gather_2354), &(CFAR_gather_2354AnonFilter_a0_2355));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2355() {
	FOR(uint32_t, __iter_steady_, 0, <, 2880, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2354AnonFilter_a0_2355));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 45, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2404_2406_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_2351WEIGHTED_ROUND_ROBIN_Splitter_2357);
	init_buffer_float(&CFAR_gather_2354AnonFilter_a0_2355);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2358CFAR_gather_2354);
	FOR(int, __iter_init_1_, 0, <, 45, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2404_2406_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_2351
	 {
	ComplexSource_2351_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2351WEIGHTED_ROUND_ROBIN_Splitter_2357));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2357
	
	FOR(uint32_t, __iter_, 0, <, 45, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2404_2406_split[__iter_], pop_complex(&ComplexSource_2351WEIGHTED_ROUND_ROBIN_Splitter_2357));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2359
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[0]));
//--------------------------------
// --- init: SquareAndScale_2360
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[1]));
//--------------------------------
// --- init: SquareAndScale_2361
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[2]));
//--------------------------------
// --- init: SquareAndScale_2362
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[3]));
//--------------------------------
// --- init: SquareAndScale_2363
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[4]));
//--------------------------------
// --- init: SquareAndScale_2364
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[5]));
//--------------------------------
// --- init: SquareAndScale_2365
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[6]));
//--------------------------------
// --- init: SquareAndScale_2366
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[7]));
//--------------------------------
// --- init: SquareAndScale_2367
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[8]));
//--------------------------------
// --- init: SquareAndScale_2368
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[9]));
//--------------------------------
// --- init: SquareAndScale_2369
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[10]));
//--------------------------------
// --- init: SquareAndScale_2370
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[11]));
//--------------------------------
// --- init: SquareAndScale_2371
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[12]));
//--------------------------------
// --- init: SquareAndScale_2372
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[13]));
//--------------------------------
// --- init: SquareAndScale_2373
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[14]));
//--------------------------------
// --- init: SquareAndScale_2374
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[15]));
//--------------------------------
// --- init: SquareAndScale_2375
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[16]));
//--------------------------------
// --- init: SquareAndScale_2376
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[17]));
//--------------------------------
// --- init: SquareAndScale_2377
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[18]));
//--------------------------------
// --- init: SquareAndScale_2378
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[19]));
//--------------------------------
// --- init: SquareAndScale_2379
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[20]));
//--------------------------------
// --- init: SquareAndScale_2380
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[21]));
//--------------------------------
// --- init: SquareAndScale_2381
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[22]));
//--------------------------------
// --- init: SquareAndScale_2382
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[23]));
//--------------------------------
// --- init: SquareAndScale_2383
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[24]));
//--------------------------------
// --- init: SquareAndScale_2384
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[25]));
//--------------------------------
// --- init: SquareAndScale_2385
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[26]));
//--------------------------------
// --- init: SquareAndScale_2386
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[27]));
//--------------------------------
// --- init: SquareAndScale_2387
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[28]));
//--------------------------------
// --- init: SquareAndScale_2388
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[29]));
//--------------------------------
// --- init: SquareAndScale_2389
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[30]));
//--------------------------------
// --- init: SquareAndScale_2390
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[31]));
//--------------------------------
// --- init: SquareAndScale_2391
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[32]));
//--------------------------------
// --- init: SquareAndScale_2392
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[33]));
//--------------------------------
// --- init: SquareAndScale_2393
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[34]));
//--------------------------------
// --- init: SquareAndScale_2394
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[35]));
//--------------------------------
// --- init: SquareAndScale_2395
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[36]));
//--------------------------------
// --- init: SquareAndScale_2396
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[37]));
//--------------------------------
// --- init: SquareAndScale_2397
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[38]));
//--------------------------------
// --- init: SquareAndScale_2398
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[39]));
//--------------------------------
// --- init: SquareAndScale_2399
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[40]));
//--------------------------------
// --- init: SquareAndScale_2400
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[41]));
//--------------------------------
// --- init: SquareAndScale_2401
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[42]));
//--------------------------------
// --- init: SquareAndScale_2402
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[43]));
//--------------------------------
// --- init: SquareAndScale_2403
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2404_2406_split[44]), &(SplitJoin0_SquareAndScale_Fiss_2404_2406_join[44]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2358
	
	FOR(uint32_t, __iter_, 0, <, 45, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2358CFAR_gather_2354, pop_float(&SplitJoin0_SquareAndScale_Fiss_2404_2406_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2354
	 {
	CFAR_gather_2354_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 36, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2358CFAR_gather_2354), &(CFAR_gather_2354AnonFilter_a0_2355));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2355
	FOR(uint32_t, __iter_init_, 0, <, 36, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2354AnonFilter_a0_2355));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2351();
		WEIGHTED_ROUND_ROBIN_Splitter_2357();
			SquareAndScale_2359();
			SquareAndScale_2360();
			SquareAndScale_2361();
			SquareAndScale_2362();
			SquareAndScale_2363();
			SquareAndScale_2364();
			SquareAndScale_2365();
			SquareAndScale_2366();
			SquareAndScale_2367();
			SquareAndScale_2368();
			SquareAndScale_2369();
			SquareAndScale_2370();
			SquareAndScale_2371();
			SquareAndScale_2372();
			SquareAndScale_2373();
			SquareAndScale_2374();
			SquareAndScale_2375();
			SquareAndScale_2376();
			SquareAndScale_2377();
			SquareAndScale_2378();
			SquareAndScale_2379();
			SquareAndScale_2380();
			SquareAndScale_2381();
			SquareAndScale_2382();
			SquareAndScale_2383();
			SquareAndScale_2384();
			SquareAndScale_2385();
			SquareAndScale_2386();
			SquareAndScale_2387();
			SquareAndScale_2388();
			SquareAndScale_2389();
			SquareAndScale_2390();
			SquareAndScale_2391();
			SquareAndScale_2392();
			SquareAndScale_2393();
			SquareAndScale_2394();
			SquareAndScale_2395();
			SquareAndScale_2396();
			SquareAndScale_2397();
			SquareAndScale_2398();
			SquareAndScale_2399();
			SquareAndScale_2400();
			SquareAndScale_2401();
			SquareAndScale_2402();
			SquareAndScale_2403();
		WEIGHTED_ROUND_ROBIN_Joiner_2358();
		CFAR_gather_2354();
		AnonFilter_a0_2355();
	ENDFOR
	return EXIT_SUCCESS;
}
