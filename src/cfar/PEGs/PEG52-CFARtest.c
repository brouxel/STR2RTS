#include "PEG52-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1588CFAR_gather_1584;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_1641_1643_join[52];
buffer_float_t CFAR_gather_1584AnonFilter_a0_1585;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1641_1643_split[52];
buffer_complex_t ComplexSource_1581WEIGHTED_ROUND_ROBIN_Splitter_1587;


ComplexSource_1581_t ComplexSource_1581_s;
CFAR_gather_1584_t CFAR_gather_1584_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_1581_s.theta = (ComplexSource_1581_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_1581_s.theta)) * (((float) cos(ComplexSource_1581_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1581_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1581_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_1581_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1581_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1581_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1581_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_1581() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		ComplexSource(&(ComplexSource_1581WEIGHTED_ROUND_ROBIN_Splitter_1587));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_1589() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[0]));
	ENDFOR
}

void SquareAndScale_1590() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[1]));
	ENDFOR
}

void SquareAndScale_1591() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[2]));
	ENDFOR
}

void SquareAndScale_1592() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[3]));
	ENDFOR
}

void SquareAndScale_1593() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[4]));
	ENDFOR
}

void SquareAndScale_1594() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[5]));
	ENDFOR
}

void SquareAndScale_1595() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[6]));
	ENDFOR
}

void SquareAndScale_1596() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[7]));
	ENDFOR
}

void SquareAndScale_1597() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[8]));
	ENDFOR
}

void SquareAndScale_1598() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[9]));
	ENDFOR
}

void SquareAndScale_1599() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[10]));
	ENDFOR
}

void SquareAndScale_1600() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[11]));
	ENDFOR
}

void SquareAndScale_1601() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[12]));
	ENDFOR
}

void SquareAndScale_1602() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[13]));
	ENDFOR
}

void SquareAndScale_1603() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[14]));
	ENDFOR
}

void SquareAndScale_1604() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[15]));
	ENDFOR
}

void SquareAndScale_1605() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[16]));
	ENDFOR
}

void SquareAndScale_1606() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[17]));
	ENDFOR
}

void SquareAndScale_1607() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[18]));
	ENDFOR
}

void SquareAndScale_1608() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[19]));
	ENDFOR
}

void SquareAndScale_1609() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[20]));
	ENDFOR
}

void SquareAndScale_1610() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[21]));
	ENDFOR
}

void SquareAndScale_1611() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[22]));
	ENDFOR
}

void SquareAndScale_1612() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[23]));
	ENDFOR
}

void SquareAndScale_1613() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[24]));
	ENDFOR
}

void SquareAndScale_1614() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[25]));
	ENDFOR
}

void SquareAndScale_1615() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[26]));
	ENDFOR
}

void SquareAndScale_1616() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[27]));
	ENDFOR
}

void SquareAndScale_1617() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[28]));
	ENDFOR
}

void SquareAndScale_1618() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[29]));
	ENDFOR
}

void SquareAndScale_1619() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[30]));
	ENDFOR
}

void SquareAndScale_1620() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[31]));
	ENDFOR
}

void SquareAndScale_1621() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[32]));
	ENDFOR
}

void SquareAndScale_1622() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[33]));
	ENDFOR
}

void SquareAndScale_1623() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[34]));
	ENDFOR
}

void SquareAndScale_1624() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[35]));
	ENDFOR
}

void SquareAndScale_1625() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[36]));
	ENDFOR
}

void SquareAndScale_1626() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[37]));
	ENDFOR
}

void SquareAndScale_1627() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[38]));
	ENDFOR
}

void SquareAndScale_1628() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[39]));
	ENDFOR
}

void SquareAndScale_1629() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[40]));
	ENDFOR
}

void SquareAndScale_1630() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[41]));
	ENDFOR
}

void SquareAndScale_1631() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[42]));
	ENDFOR
}

void SquareAndScale_1632() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[43]));
	ENDFOR
}

void SquareAndScale_1633() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[44]));
	ENDFOR
}

void SquareAndScale_1634() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[45]));
	ENDFOR
}

void SquareAndScale_1635() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[46]));
	ENDFOR
}

void SquareAndScale_1636() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[47]));
	ENDFOR
}

void SquareAndScale_1637() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[48]));
	ENDFOR
}

void SquareAndScale_1638() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[49]));
	ENDFOR
}

void SquareAndScale_1639() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[50]));
	ENDFOR
}

void SquareAndScale_1640() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[51]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1587() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 52, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1641_1643_split[__iter_], pop_complex(&ComplexSource_1581WEIGHTED_ROUND_ROBIN_Splitter_1587));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1588() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 52, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1588CFAR_gather_1584, pop_float(&SplitJoin0_SquareAndScale_Fiss_1641_1643_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1584_s.pos) - 5) >= 0)), __DEFLOOPBOUND__80__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1584_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1584_s.pos) < 64)), __DEFLOOPBOUND__81__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1584_s.poke[(i - 1)] = CFAR_gather_1584_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1584_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_1584_s.pos++ ; 
		if(CFAR_gather_1584_s.pos == 64) {
			CFAR_gather_1584_s.pos = 0 ; 
		}
	}


void CFAR_gather_1584() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1588CFAR_gather_1584), &(CFAR_gather_1584AnonFilter_a0_1585));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_1585() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_1584AnonFilter_a0_1585));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1588CFAR_gather_1584);
	FOR(int, __iter_init_0_, 0, <, 52, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1641_1643_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_1584AnonFilter_a0_1585);
	FOR(int, __iter_init_1_, 0, <, 52, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1641_1643_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_1581WEIGHTED_ROUND_ROBIN_Splitter_1587);
// --- init: ComplexSource_1581
	 {
	ComplexSource_1581_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_1581WEIGHTED_ROUND_ROBIN_Splitter_1587));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1587
	
	FOR(uint32_t, __iter_, 0, <, 52, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1641_1643_split[__iter_], pop_complex(&ComplexSource_1581WEIGHTED_ROUND_ROBIN_Splitter_1587));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_1589
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[0]));
//--------------------------------
// --- init: SquareAndScale_1590
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[1]));
//--------------------------------
// --- init: SquareAndScale_1591
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[2]));
//--------------------------------
// --- init: SquareAndScale_1592
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[3]));
//--------------------------------
// --- init: SquareAndScale_1593
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[4]));
//--------------------------------
// --- init: SquareAndScale_1594
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[5]));
//--------------------------------
// --- init: SquareAndScale_1595
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[6]));
//--------------------------------
// --- init: SquareAndScale_1596
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[7]));
//--------------------------------
// --- init: SquareAndScale_1597
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[8]));
//--------------------------------
// --- init: SquareAndScale_1598
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[9]));
//--------------------------------
// --- init: SquareAndScale_1599
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[10]));
//--------------------------------
// --- init: SquareAndScale_1600
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[11]));
//--------------------------------
// --- init: SquareAndScale_1601
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[12]));
//--------------------------------
// --- init: SquareAndScale_1602
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[13]));
//--------------------------------
// --- init: SquareAndScale_1603
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[14]));
//--------------------------------
// --- init: SquareAndScale_1604
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[15]));
//--------------------------------
// --- init: SquareAndScale_1605
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[16]));
//--------------------------------
// --- init: SquareAndScale_1606
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[17]));
//--------------------------------
// --- init: SquareAndScale_1607
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[18]));
//--------------------------------
// --- init: SquareAndScale_1608
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[19]));
//--------------------------------
// --- init: SquareAndScale_1609
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[20]));
//--------------------------------
// --- init: SquareAndScale_1610
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[21]));
//--------------------------------
// --- init: SquareAndScale_1611
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[22]));
//--------------------------------
// --- init: SquareAndScale_1612
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[23]));
//--------------------------------
// --- init: SquareAndScale_1613
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[24]));
//--------------------------------
// --- init: SquareAndScale_1614
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[25]));
//--------------------------------
// --- init: SquareAndScale_1615
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[26]));
//--------------------------------
// --- init: SquareAndScale_1616
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[27]));
//--------------------------------
// --- init: SquareAndScale_1617
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[28]));
//--------------------------------
// --- init: SquareAndScale_1618
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[29]));
//--------------------------------
// --- init: SquareAndScale_1619
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[30]));
//--------------------------------
// --- init: SquareAndScale_1620
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[31]));
//--------------------------------
// --- init: SquareAndScale_1621
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[32]));
//--------------------------------
// --- init: SquareAndScale_1622
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[33]));
//--------------------------------
// --- init: SquareAndScale_1623
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[34]));
//--------------------------------
// --- init: SquareAndScale_1624
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[35]));
//--------------------------------
// --- init: SquareAndScale_1625
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[36]));
//--------------------------------
// --- init: SquareAndScale_1626
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[37]));
//--------------------------------
// --- init: SquareAndScale_1627
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[38]));
//--------------------------------
// --- init: SquareAndScale_1628
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[39]));
//--------------------------------
// --- init: SquareAndScale_1629
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[40]));
//--------------------------------
// --- init: SquareAndScale_1630
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[41]));
//--------------------------------
// --- init: SquareAndScale_1631
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[42]));
//--------------------------------
// --- init: SquareAndScale_1632
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[43]));
//--------------------------------
// --- init: SquareAndScale_1633
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[44]));
//--------------------------------
// --- init: SquareAndScale_1634
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[45]));
//--------------------------------
// --- init: SquareAndScale_1635
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[46]));
//--------------------------------
// --- init: SquareAndScale_1636
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[47]));
//--------------------------------
// --- init: SquareAndScale_1637
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[48]));
//--------------------------------
// --- init: SquareAndScale_1638
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[49]));
//--------------------------------
// --- init: SquareAndScale_1639
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[50]));
//--------------------------------
// --- init: SquareAndScale_1640
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1641_1643_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1641_1643_join[51]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1588
	
	FOR(uint32_t, __iter_, 0, <, 52, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1588CFAR_gather_1584, pop_float(&SplitJoin0_SquareAndScale_Fiss_1641_1643_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_1584
	 {
	CFAR_gather_1584_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 43, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1588CFAR_gather_1584), &(CFAR_gather_1584AnonFilter_a0_1585));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_1585
	FOR(uint32_t, __iter_init_, 0, <, 43, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_1584AnonFilter_a0_1585));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_1581();
		WEIGHTED_ROUND_ROBIN_Splitter_1587();
			SquareAndScale_1589();
			SquareAndScale_1590();
			SquareAndScale_1591();
			SquareAndScale_1592();
			SquareAndScale_1593();
			SquareAndScale_1594();
			SquareAndScale_1595();
			SquareAndScale_1596();
			SquareAndScale_1597();
			SquareAndScale_1598();
			SquareAndScale_1599();
			SquareAndScale_1600();
			SquareAndScale_1601();
			SquareAndScale_1602();
			SquareAndScale_1603();
			SquareAndScale_1604();
			SquareAndScale_1605();
			SquareAndScale_1606();
			SquareAndScale_1607();
			SquareAndScale_1608();
			SquareAndScale_1609();
			SquareAndScale_1610();
			SquareAndScale_1611();
			SquareAndScale_1612();
			SquareAndScale_1613();
			SquareAndScale_1614();
			SquareAndScale_1615();
			SquareAndScale_1616();
			SquareAndScale_1617();
			SquareAndScale_1618();
			SquareAndScale_1619();
			SquareAndScale_1620();
			SquareAndScale_1621();
			SquareAndScale_1622();
			SquareAndScale_1623();
			SquareAndScale_1624();
			SquareAndScale_1625();
			SquareAndScale_1626();
			SquareAndScale_1627();
			SquareAndScale_1628();
			SquareAndScale_1629();
			SquareAndScale_1630();
			SquareAndScale_1631();
			SquareAndScale_1632();
			SquareAndScale_1633();
			SquareAndScale_1634();
			SquareAndScale_1635();
			SquareAndScale_1636();
			SquareAndScale_1637();
			SquareAndScale_1638();
			SquareAndScale_1639();
			SquareAndScale_1640();
		WEIGHTED_ROUND_ROBIN_Joiner_1588();
		CFAR_gather_1584();
		AnonFilter_a0_1585();
	ENDFOR
	return EXIT_SUCCESS;
}
