#include "PEG12-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4668CFAR_gather_4664;
buffer_complex_t ComplexSource_4661WEIGHTED_ROUND_ROBIN_Splitter_4667;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4681_4683_join[12];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4681_4683_split[12];
buffer_float_t CFAR_gather_4664AnonFilter_a0_4665;


ComplexSource_4661_t ComplexSource_4661_s;
CFAR_gather_4664_t CFAR_gather_4664_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4661_s.theta = (ComplexSource_4661_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4661_s.theta)) * (((float) cos(ComplexSource_4661_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4661_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4661_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4661_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4661_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4661_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4661_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4661() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		ComplexSource(&(ComplexSource_4661WEIGHTED_ROUND_ROBIN_Splitter_4667));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4669() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[0]));
	ENDFOR
}

void SquareAndScale_4670() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[1]));
	ENDFOR
}

void SquareAndScale_4671() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[2]));
	ENDFOR
}

void SquareAndScale_4672() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[3]));
	ENDFOR
}

void SquareAndScale_4673() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[4]));
	ENDFOR
}

void SquareAndScale_4674() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[5]));
	ENDFOR
}

void SquareAndScale_4675() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[6]));
	ENDFOR
}

void SquareAndScale_4676() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[7]));
	ENDFOR
}

void SquareAndScale_4677() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[8]));
	ENDFOR
}

void SquareAndScale_4678() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[9]));
	ENDFOR
}

void SquareAndScale_4679() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[10]));
	ENDFOR
}

void SquareAndScale_4680() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4667() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4681_4683_split[__iter_], pop_complex(&ComplexSource_4661WEIGHTED_ROUND_ROBIN_Splitter_4667));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4668() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4668CFAR_gather_4664, pop_float(&SplitJoin0_SquareAndScale_Fiss_4681_4683_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4664_s.pos) - 5) >= 0)), __DEFLOOPBOUND__320__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4664_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4664_s.pos) < 64)), __DEFLOOPBOUND__321__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4664_s.poke[(i - 1)] = CFAR_gather_4664_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4664_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4664_s.pos++ ; 
		if(CFAR_gather_4664_s.pos == 64) {
			CFAR_gather_4664_s.pos = 0 ; 
		}
	}


void CFAR_gather_4664() {
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4668CFAR_gather_4664), &(CFAR_gather_4664AnonFilter_a0_4665));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4665() {
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4664AnonFilter_a0_4665));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4668CFAR_gather_4664);
	init_buffer_complex(&ComplexSource_4661WEIGHTED_ROUND_ROBIN_Splitter_4667);
	FOR(int, __iter_init_0_, 0, <, 12, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4681_4683_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 12, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4681_4683_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4664AnonFilter_a0_4665);
// --- init: ComplexSource_4661
	 {
	ComplexSource_4661_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4661WEIGHTED_ROUND_ROBIN_Splitter_4667));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4667
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4681_4683_split[__iter_], pop_complex(&ComplexSource_4661WEIGHTED_ROUND_ROBIN_Splitter_4667));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4669
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4670
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4671
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4672
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4673
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4674
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4675
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4676
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4677
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4678
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4679
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4680
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4681_4683_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4681_4683_join[11]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4668
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4668CFAR_gather_4664, pop_float(&SplitJoin0_SquareAndScale_Fiss_4681_4683_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4664
	 {
	CFAR_gather_4664_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4668CFAR_gather_4664), &(CFAR_gather_4664AnonFilter_a0_4665));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4665
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4664AnonFilter_a0_4665));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4661();
		WEIGHTED_ROUND_ROBIN_Splitter_4667();
			SquareAndScale_4669();
			SquareAndScale_4670();
			SquareAndScale_4671();
			SquareAndScale_4672();
			SquareAndScale_4673();
			SquareAndScale_4674();
			SquareAndScale_4675();
			SquareAndScale_4676();
			SquareAndScale_4677();
			SquareAndScale_4678();
			SquareAndScale_4679();
			SquareAndScale_4680();
		WEIGHTED_ROUND_ROBIN_Joiner_4668();
		CFAR_gather_4664();
		AnonFilter_a0_4665();
	ENDFOR
	return EXIT_SUCCESS;
}
