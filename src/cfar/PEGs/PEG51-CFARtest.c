#include "PEG51-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1704CFAR_gather_1700;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1756_1758_split[51];
buffer_complex_t ComplexSource_1697WEIGHTED_ROUND_ROBIN_Splitter_1703;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_1756_1758_join[51];
buffer_float_t CFAR_gather_1700AnonFilter_a0_1701;


ComplexSource_1697_t ComplexSource_1697_s;
CFAR_gather_1700_t CFAR_gather_1700_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_1697_s.theta = (ComplexSource_1697_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_1697_s.theta)) * (((float) cos(ComplexSource_1697_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1697_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1697_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_1697_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1697_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1697_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1697_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_1697() {
	FOR(uint32_t, __iter_steady_, 0, <, 51, __iter_steady_++)
		ComplexSource(&(ComplexSource_1697WEIGHTED_ROUND_ROBIN_Splitter_1703));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_1705() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[0]));
	ENDFOR
}

void SquareAndScale_1706() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[1]));
	ENDFOR
}

void SquareAndScale_1707() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[2]));
	ENDFOR
}

void SquareAndScale_1708() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[3]));
	ENDFOR
}

void SquareAndScale_1709() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[4]));
	ENDFOR
}

void SquareAndScale_1710() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[5]));
	ENDFOR
}

void SquareAndScale_1711() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[6]));
	ENDFOR
}

void SquareAndScale_1712() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[7]));
	ENDFOR
}

void SquareAndScale_1713() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[8]));
	ENDFOR
}

void SquareAndScale_1714() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[9]));
	ENDFOR
}

void SquareAndScale_1715() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[10]));
	ENDFOR
}

void SquareAndScale_1716() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[11]));
	ENDFOR
}

void SquareAndScale_1717() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[12]));
	ENDFOR
}

void SquareAndScale_1718() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[13]));
	ENDFOR
}

void SquareAndScale_1719() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[14]));
	ENDFOR
}

void SquareAndScale_1720() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[15]));
	ENDFOR
}

void SquareAndScale_1721() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[16]));
	ENDFOR
}

void SquareAndScale_1722() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[17]));
	ENDFOR
}

void SquareAndScale_1723() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[18]));
	ENDFOR
}

void SquareAndScale_1724() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[19]));
	ENDFOR
}

void SquareAndScale_1725() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[20]));
	ENDFOR
}

void SquareAndScale_1726() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[21]));
	ENDFOR
}

void SquareAndScale_1727() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[22]));
	ENDFOR
}

void SquareAndScale_1728() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[23]));
	ENDFOR
}

void SquareAndScale_1729() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[24]));
	ENDFOR
}

void SquareAndScale_1730() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[25]));
	ENDFOR
}

void SquareAndScale_1731() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[26]));
	ENDFOR
}

void SquareAndScale_1732() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[27]));
	ENDFOR
}

void SquareAndScale_1733() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[28]));
	ENDFOR
}

void SquareAndScale_1734() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[29]));
	ENDFOR
}

void SquareAndScale_1735() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[30]));
	ENDFOR
}

void SquareAndScale_1736() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[31]));
	ENDFOR
}

void SquareAndScale_1737() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[32]));
	ENDFOR
}

void SquareAndScale_1738() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[33]));
	ENDFOR
}

void SquareAndScale_1739() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[34]));
	ENDFOR
}

void SquareAndScale_1740() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[35]));
	ENDFOR
}

void SquareAndScale_1741() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[36]));
	ENDFOR
}

void SquareAndScale_1742() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[37]));
	ENDFOR
}

void SquareAndScale_1743() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[38]));
	ENDFOR
}

void SquareAndScale_1744() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[39]));
	ENDFOR
}

void SquareAndScale_1745() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[40]));
	ENDFOR
}

void SquareAndScale_1746() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[41]));
	ENDFOR
}

void SquareAndScale_1747() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[42]));
	ENDFOR
}

void SquareAndScale_1748() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[43]));
	ENDFOR
}

void SquareAndScale_1749() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[44]));
	ENDFOR
}

void SquareAndScale_1750() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[45]));
	ENDFOR
}

void SquareAndScale_1751() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[46]));
	ENDFOR
}

void SquareAndScale_1752() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[47]));
	ENDFOR
}

void SquareAndScale_1753() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[48]));
	ENDFOR
}

void SquareAndScale_1754() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[49]));
	ENDFOR
}

void SquareAndScale_1755() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[50]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1703() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 51, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1756_1758_split[__iter_], pop_complex(&ComplexSource_1697WEIGHTED_ROUND_ROBIN_Splitter_1703));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1704() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 51, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1704CFAR_gather_1700, pop_float(&SplitJoin0_SquareAndScale_Fiss_1756_1758_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1700_s.pos) - 5) >= 0)), __DEFLOOPBOUND__86__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1700_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1700_s.pos) < 64)), __DEFLOOPBOUND__87__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1700_s.poke[(i - 1)] = CFAR_gather_1700_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1700_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_1700_s.pos++ ; 
		if(CFAR_gather_1700_s.pos == 64) {
			CFAR_gather_1700_s.pos = 0 ; 
		}
	}


void CFAR_gather_1700() {
	FOR(uint32_t, __iter_steady_, 0, <, 3264, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1704CFAR_gather_1700), &(CFAR_gather_1700AnonFilter_a0_1701));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_1701() {
	FOR(uint32_t, __iter_steady_, 0, <, 3264, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_1700AnonFilter_a0_1701));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1704CFAR_gather_1700);
	FOR(int, __iter_init_0_, 0, <, 51, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1756_1758_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_1697WEIGHTED_ROUND_ROBIN_Splitter_1703);
	FOR(int, __iter_init_1_, 0, <, 51, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1756_1758_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_1700AnonFilter_a0_1701);
// --- init: ComplexSource_1697
	 {
	ComplexSource_1697_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_1697WEIGHTED_ROUND_ROBIN_Splitter_1703));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1703
	
	FOR(uint32_t, __iter_, 0, <, 51, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1756_1758_split[__iter_], pop_complex(&ComplexSource_1697WEIGHTED_ROUND_ROBIN_Splitter_1703));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_1705
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[0]));
//--------------------------------
// --- init: SquareAndScale_1706
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[1]));
//--------------------------------
// --- init: SquareAndScale_1707
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[2]));
//--------------------------------
// --- init: SquareAndScale_1708
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[3]));
//--------------------------------
// --- init: SquareAndScale_1709
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[4]));
//--------------------------------
// --- init: SquareAndScale_1710
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[5]));
//--------------------------------
// --- init: SquareAndScale_1711
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[6]));
//--------------------------------
// --- init: SquareAndScale_1712
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[7]));
//--------------------------------
// --- init: SquareAndScale_1713
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[8]));
//--------------------------------
// --- init: SquareAndScale_1714
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[9]));
//--------------------------------
// --- init: SquareAndScale_1715
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[10]));
//--------------------------------
// --- init: SquareAndScale_1716
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[11]));
//--------------------------------
// --- init: SquareAndScale_1717
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[12]));
//--------------------------------
// --- init: SquareAndScale_1718
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[13]));
//--------------------------------
// --- init: SquareAndScale_1719
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[14]));
//--------------------------------
// --- init: SquareAndScale_1720
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[15]));
//--------------------------------
// --- init: SquareAndScale_1721
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[16]));
//--------------------------------
// --- init: SquareAndScale_1722
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[17]));
//--------------------------------
// --- init: SquareAndScale_1723
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[18]));
//--------------------------------
// --- init: SquareAndScale_1724
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[19]));
//--------------------------------
// --- init: SquareAndScale_1725
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[20]));
//--------------------------------
// --- init: SquareAndScale_1726
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[21]));
//--------------------------------
// --- init: SquareAndScale_1727
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[22]));
//--------------------------------
// --- init: SquareAndScale_1728
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[23]));
//--------------------------------
// --- init: SquareAndScale_1729
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[24]));
//--------------------------------
// --- init: SquareAndScale_1730
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[25]));
//--------------------------------
// --- init: SquareAndScale_1731
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[26]));
//--------------------------------
// --- init: SquareAndScale_1732
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[27]));
//--------------------------------
// --- init: SquareAndScale_1733
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[28]));
//--------------------------------
// --- init: SquareAndScale_1734
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[29]));
//--------------------------------
// --- init: SquareAndScale_1735
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[30]));
//--------------------------------
// --- init: SquareAndScale_1736
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[31]));
//--------------------------------
// --- init: SquareAndScale_1737
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[32]));
//--------------------------------
// --- init: SquareAndScale_1738
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[33]));
//--------------------------------
// --- init: SquareAndScale_1739
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[34]));
//--------------------------------
// --- init: SquareAndScale_1740
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[35]));
//--------------------------------
// --- init: SquareAndScale_1741
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[36]));
//--------------------------------
// --- init: SquareAndScale_1742
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[37]));
//--------------------------------
// --- init: SquareAndScale_1743
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[38]));
//--------------------------------
// --- init: SquareAndScale_1744
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[39]));
//--------------------------------
// --- init: SquareAndScale_1745
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[40]));
//--------------------------------
// --- init: SquareAndScale_1746
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[41]));
//--------------------------------
// --- init: SquareAndScale_1747
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[42]));
//--------------------------------
// --- init: SquareAndScale_1748
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[43]));
//--------------------------------
// --- init: SquareAndScale_1749
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[44]));
//--------------------------------
// --- init: SquareAndScale_1750
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[45]));
//--------------------------------
// --- init: SquareAndScale_1751
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[46]));
//--------------------------------
// --- init: SquareAndScale_1752
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[47]));
//--------------------------------
// --- init: SquareAndScale_1753
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[48]));
//--------------------------------
// --- init: SquareAndScale_1754
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[49]));
//--------------------------------
// --- init: SquareAndScale_1755
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1756_1758_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1756_1758_join[50]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1704
	
	FOR(uint32_t, __iter_, 0, <, 51, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1704CFAR_gather_1700, pop_float(&SplitJoin0_SquareAndScale_Fiss_1756_1758_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_1700
	 {
	CFAR_gather_1700_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 42, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1704CFAR_gather_1700), &(CFAR_gather_1700AnonFilter_a0_1701));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_1701
	FOR(uint32_t, __iter_init_, 0, <, 42, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_1700AnonFilter_a0_1701));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_1697();
		WEIGHTED_ROUND_ROBIN_Splitter_1703();
			SquareAndScale_1705();
			SquareAndScale_1706();
			SquareAndScale_1707();
			SquareAndScale_1708();
			SquareAndScale_1709();
			SquareAndScale_1710();
			SquareAndScale_1711();
			SquareAndScale_1712();
			SquareAndScale_1713();
			SquareAndScale_1714();
			SquareAndScale_1715();
			SquareAndScale_1716();
			SquareAndScale_1717();
			SquareAndScale_1718();
			SquareAndScale_1719();
			SquareAndScale_1720();
			SquareAndScale_1721();
			SquareAndScale_1722();
			SquareAndScale_1723();
			SquareAndScale_1724();
			SquareAndScale_1725();
			SquareAndScale_1726();
			SquareAndScale_1727();
			SquareAndScale_1728();
			SquareAndScale_1729();
			SquareAndScale_1730();
			SquareAndScale_1731();
			SquareAndScale_1732();
			SquareAndScale_1733();
			SquareAndScale_1734();
			SquareAndScale_1735();
			SquareAndScale_1736();
			SquareAndScale_1737();
			SquareAndScale_1738();
			SquareAndScale_1739();
			SquareAndScale_1740();
			SquareAndScale_1741();
			SquareAndScale_1742();
			SquareAndScale_1743();
			SquareAndScale_1744();
			SquareAndScale_1745();
			SquareAndScale_1746();
			SquareAndScale_1747();
			SquareAndScale_1748();
			SquareAndScale_1749();
			SquareAndScale_1750();
			SquareAndScale_1751();
			SquareAndScale_1752();
			SquareAndScale_1753();
			SquareAndScale_1754();
			SquareAndScale_1755();
		WEIGHTED_ROUND_ROBIN_Joiner_1704();
		CFAR_gather_1700();
		AnonFilter_a0_1701();
	ENDFOR
	return EXIT_SUCCESS;
}
