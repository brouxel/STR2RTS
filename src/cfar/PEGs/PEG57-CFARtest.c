#include "PEG57-CFARtest.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1036_1038_split[57];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_1036_1038_join[57];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_978CFAR_gather_974;
buffer_complex_t ComplexSource_971WEIGHTED_ROUND_ROBIN_Splitter_977;
buffer_float_t CFAR_gather_974AnonFilter_a0_975;


ComplexSource_971_t ComplexSource_971_s;
CFAR_gather_974_t CFAR_gather_974_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_971_s.theta = (ComplexSource_971_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_971_s.theta)) * (((float) cos(ComplexSource_971_s.theta)) + ((0.0 * ((float) sin(ComplexSource_971_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_971_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_971_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_971_s.theta))))) + (0.0 * (((float) cos(ComplexSource_971_s.theta)) + ((0.0 * ((float) sin(ComplexSource_971_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_971() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		ComplexSource(&(ComplexSource_971WEIGHTED_ROUND_ROBIN_Splitter_977));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_979() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[0]));
	ENDFOR
}

void SquareAndScale_980() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[1]));
	ENDFOR
}

void SquareAndScale_981() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[2]));
	ENDFOR
}

void SquareAndScale_982() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[3]));
	ENDFOR
}

void SquareAndScale_983() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[4]));
	ENDFOR
}

void SquareAndScale_984() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[5]));
	ENDFOR
}

void SquareAndScale_985() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[6]));
	ENDFOR
}

void SquareAndScale_986() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[7]));
	ENDFOR
}

void SquareAndScale_987() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[8]));
	ENDFOR
}

void SquareAndScale_988() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[9]));
	ENDFOR
}

void SquareAndScale_989() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[10]));
	ENDFOR
}

void SquareAndScale_990() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[11]));
	ENDFOR
}

void SquareAndScale_991() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[12]));
	ENDFOR
}

void SquareAndScale_992() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[13]));
	ENDFOR
}

void SquareAndScale_993() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[14]));
	ENDFOR
}

void SquareAndScale_994() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[15]));
	ENDFOR
}

void SquareAndScale_995() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[16]));
	ENDFOR
}

void SquareAndScale_996() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[17]));
	ENDFOR
}

void SquareAndScale_997() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[18]));
	ENDFOR
}

void SquareAndScale_998() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[19]));
	ENDFOR
}

void SquareAndScale_999() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[20]));
	ENDFOR
}

void SquareAndScale_1000() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[21]));
	ENDFOR
}

void SquareAndScale_1001() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[22]));
	ENDFOR
}

void SquareAndScale_1002() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[23]));
	ENDFOR
}

void SquareAndScale_1003() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[24]));
	ENDFOR
}

void SquareAndScale_1004() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[25]));
	ENDFOR
}

void SquareAndScale_1005() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[26]));
	ENDFOR
}

void SquareAndScale_1006() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[27]));
	ENDFOR
}

void SquareAndScale_1007() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[28]));
	ENDFOR
}

void SquareAndScale_1008() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[29]));
	ENDFOR
}

void SquareAndScale_1009() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[30]));
	ENDFOR
}

void SquareAndScale_1010() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[31]));
	ENDFOR
}

void SquareAndScale_1011() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[32]));
	ENDFOR
}

void SquareAndScale_1012() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[33]));
	ENDFOR
}

void SquareAndScale_1013() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[34]));
	ENDFOR
}

void SquareAndScale_1014() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[35]));
	ENDFOR
}

void SquareAndScale_1015() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[36]));
	ENDFOR
}

void SquareAndScale_1016() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[37]));
	ENDFOR
}

void SquareAndScale_1017() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[38]));
	ENDFOR
}

void SquareAndScale_1018() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[39]));
	ENDFOR
}

void SquareAndScale_1019() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[40]));
	ENDFOR
}

void SquareAndScale_1020() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[41]));
	ENDFOR
}

void SquareAndScale_1021() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[42]));
	ENDFOR
}

void SquareAndScale_1022() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[43]));
	ENDFOR
}

void SquareAndScale_1023() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[44]));
	ENDFOR
}

void SquareAndScale_1024() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[45]));
	ENDFOR
}

void SquareAndScale_1025() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[46]));
	ENDFOR
}

void SquareAndScale_1026() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[47]));
	ENDFOR
}

void SquareAndScale_1027() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[48]));
	ENDFOR
}

void SquareAndScale_1028() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[49]));
	ENDFOR
}

void SquareAndScale_1029() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[50]));
	ENDFOR
}

void SquareAndScale_1030() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[51]));
	ENDFOR
}

void SquareAndScale_1031() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[52]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[52]));
	ENDFOR
}

void SquareAndScale_1032() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[53]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[53]));
	ENDFOR
}

void SquareAndScale_1033() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[54]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[54]));
	ENDFOR
}

void SquareAndScale_1034() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[55]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[55]));
	ENDFOR
}

void SquareAndScale_1035() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[56]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[56]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_977() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 57, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1036_1038_split[__iter_], pop_complex(&ComplexSource_971WEIGHTED_ROUND_ROBIN_Splitter_977));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_978() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 57, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_978CFAR_gather_974, pop_float(&SplitJoin0_SquareAndScale_Fiss_1036_1038_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_974_s.pos) - 5) >= 0)), __DEFLOOPBOUND__50__, i__conflict__1++) {
			sum = (sum + CFAR_gather_974_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_974_s.pos) < 64)), __DEFLOOPBOUND__51__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_974_s.poke[(i - 1)] = CFAR_gather_974_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_974_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_974_s.pos++ ; 
		if(CFAR_gather_974_s.pos == 64) {
			CFAR_gather_974_s.pos = 0 ; 
		}
	}


void CFAR_gather_974() {
	FOR(uint32_t, __iter_steady_, 0, <, 3648, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_978CFAR_gather_974), &(CFAR_gather_974AnonFilter_a0_975));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_975() {
	FOR(uint32_t, __iter_steady_, 0, <, 3648, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_974AnonFilter_a0_975));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 57, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1036_1038_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 57, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1036_1038_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_978CFAR_gather_974);
	init_buffer_complex(&ComplexSource_971WEIGHTED_ROUND_ROBIN_Splitter_977);
	init_buffer_float(&CFAR_gather_974AnonFilter_a0_975);
// --- init: ComplexSource_971
	 {
	ComplexSource_971_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_971WEIGHTED_ROUND_ROBIN_Splitter_977));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_977
	
	FOR(uint32_t, __iter_, 0, <, 57, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1036_1038_split[__iter_], pop_complex(&ComplexSource_971WEIGHTED_ROUND_ROBIN_Splitter_977));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_979
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[0]));
//--------------------------------
// --- init: SquareAndScale_980
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[1]));
//--------------------------------
// --- init: SquareAndScale_981
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[2]));
//--------------------------------
// --- init: SquareAndScale_982
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[3]));
//--------------------------------
// --- init: SquareAndScale_983
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[4]));
//--------------------------------
// --- init: SquareAndScale_984
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[5]));
//--------------------------------
// --- init: SquareAndScale_985
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[6]));
//--------------------------------
// --- init: SquareAndScale_986
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[7]));
//--------------------------------
// --- init: SquareAndScale_987
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[8]));
//--------------------------------
// --- init: SquareAndScale_988
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[9]));
//--------------------------------
// --- init: SquareAndScale_989
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[10]));
//--------------------------------
// --- init: SquareAndScale_990
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[11]));
//--------------------------------
// --- init: SquareAndScale_991
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[12]));
//--------------------------------
// --- init: SquareAndScale_992
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[13]));
//--------------------------------
// --- init: SquareAndScale_993
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[14]));
//--------------------------------
// --- init: SquareAndScale_994
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[15]));
//--------------------------------
// --- init: SquareAndScale_995
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[16]));
//--------------------------------
// --- init: SquareAndScale_996
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[17]));
//--------------------------------
// --- init: SquareAndScale_997
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[18]));
//--------------------------------
// --- init: SquareAndScale_998
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[19]));
//--------------------------------
// --- init: SquareAndScale_999
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[20]));
//--------------------------------
// --- init: SquareAndScale_1000
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[21]));
//--------------------------------
// --- init: SquareAndScale_1001
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[22]));
//--------------------------------
// --- init: SquareAndScale_1002
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[23]));
//--------------------------------
// --- init: SquareAndScale_1003
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[24]));
//--------------------------------
// --- init: SquareAndScale_1004
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[25]));
//--------------------------------
// --- init: SquareAndScale_1005
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[26]));
//--------------------------------
// --- init: SquareAndScale_1006
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[27]));
//--------------------------------
// --- init: SquareAndScale_1007
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[28]));
//--------------------------------
// --- init: SquareAndScale_1008
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[29]));
//--------------------------------
// --- init: SquareAndScale_1009
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[30]));
//--------------------------------
// --- init: SquareAndScale_1010
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[31]));
//--------------------------------
// --- init: SquareAndScale_1011
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[32]));
//--------------------------------
// --- init: SquareAndScale_1012
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[33]));
//--------------------------------
// --- init: SquareAndScale_1013
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[34]));
//--------------------------------
// --- init: SquareAndScale_1014
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[35]));
//--------------------------------
// --- init: SquareAndScale_1015
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[36]));
//--------------------------------
// --- init: SquareAndScale_1016
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[37]));
//--------------------------------
// --- init: SquareAndScale_1017
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[38]));
//--------------------------------
// --- init: SquareAndScale_1018
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[39]));
//--------------------------------
// --- init: SquareAndScale_1019
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[40]));
//--------------------------------
// --- init: SquareAndScale_1020
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[41]));
//--------------------------------
// --- init: SquareAndScale_1021
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[42]));
//--------------------------------
// --- init: SquareAndScale_1022
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[43]));
//--------------------------------
// --- init: SquareAndScale_1023
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[44]));
//--------------------------------
// --- init: SquareAndScale_1024
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[45]));
//--------------------------------
// --- init: SquareAndScale_1025
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[46]));
//--------------------------------
// --- init: SquareAndScale_1026
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[47]));
//--------------------------------
// --- init: SquareAndScale_1027
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[48]));
//--------------------------------
// --- init: SquareAndScale_1028
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[49]));
//--------------------------------
// --- init: SquareAndScale_1029
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[50]));
//--------------------------------
// --- init: SquareAndScale_1030
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[51]));
//--------------------------------
// --- init: SquareAndScale_1031
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[52]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[52]));
//--------------------------------
// --- init: SquareAndScale_1032
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[53]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[53]));
//--------------------------------
// --- init: SquareAndScale_1033
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[54]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[54]));
//--------------------------------
// --- init: SquareAndScale_1034
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[55]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[55]));
//--------------------------------
// --- init: SquareAndScale_1035
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1036_1038_split[56]), &(SplitJoin0_SquareAndScale_Fiss_1036_1038_join[56]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_978
	
	FOR(uint32_t, __iter_, 0, <, 57, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_978CFAR_gather_974, pop_float(&SplitJoin0_SquareAndScale_Fiss_1036_1038_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_974
	 {
	CFAR_gather_974_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_978CFAR_gather_974), &(CFAR_gather_974AnonFilter_a0_975));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_975
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_974AnonFilter_a0_975));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_971();
		WEIGHTED_ROUND_ROBIN_Splitter_977();
			SquareAndScale_979();
			SquareAndScale_980();
			SquareAndScale_981();
			SquareAndScale_982();
			SquareAndScale_983();
			SquareAndScale_984();
			SquareAndScale_985();
			SquareAndScale_986();
			SquareAndScale_987();
			SquareAndScale_988();
			SquareAndScale_989();
			SquareAndScale_990();
			SquareAndScale_991();
			SquareAndScale_992();
			SquareAndScale_993();
			SquareAndScale_994();
			SquareAndScale_995();
			SquareAndScale_996();
			SquareAndScale_997();
			SquareAndScale_998();
			SquareAndScale_999();
			SquareAndScale_1000();
			SquareAndScale_1001();
			SquareAndScale_1002();
			SquareAndScale_1003();
			SquareAndScale_1004();
			SquareAndScale_1005();
			SquareAndScale_1006();
			SquareAndScale_1007();
			SquareAndScale_1008();
			SquareAndScale_1009();
			SquareAndScale_1010();
			SquareAndScale_1011();
			SquareAndScale_1012();
			SquareAndScale_1013();
			SquareAndScale_1014();
			SquareAndScale_1015();
			SquareAndScale_1016();
			SquareAndScale_1017();
			SquareAndScale_1018();
			SquareAndScale_1019();
			SquareAndScale_1020();
			SquareAndScale_1021();
			SquareAndScale_1022();
			SquareAndScale_1023();
			SquareAndScale_1024();
			SquareAndScale_1025();
			SquareAndScale_1026();
			SquareAndScale_1027();
			SquareAndScale_1028();
			SquareAndScale_1029();
			SquareAndScale_1030();
			SquareAndScale_1031();
			SquareAndScale_1032();
			SquareAndScale_1033();
			SquareAndScale_1034();
			SquareAndScale_1035();
		WEIGHTED_ROUND_ROBIN_Joiner_978();
		CFAR_gather_974();
		AnonFilter_a0_975();
	ENDFOR
	return EXIT_SUCCESS;
}
