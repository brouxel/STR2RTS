#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=18051 on the compile command line
#else
#if BUF_SIZEMAX < 18051
#error BUF_SIZEMAX too small, it must be at least 18051
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_1811_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_1814_t;
void ComplexSource_1811();
void WEIGHTED_ROUND_ROBIN_Splitter_1817();
void SquareAndScale_1819();
void SquareAndScale_1820();
void SquareAndScale_1821();
void SquareAndScale_1822();
void SquareAndScale_1823();
void SquareAndScale_1824();
void SquareAndScale_1825();
void SquareAndScale_1826();
void SquareAndScale_1827();
void SquareAndScale_1828();
void SquareAndScale_1829();
void SquareAndScale_1830();
void SquareAndScale_1831();
void SquareAndScale_1832();
void SquareAndScale_1833();
void SquareAndScale_1834();
void SquareAndScale_1835();
void SquareAndScale_1836();
void SquareAndScale_1837();
void SquareAndScale_1838();
void SquareAndScale_1839();
void SquareAndScale_1840();
void SquareAndScale_1841();
void SquareAndScale_1842();
void SquareAndScale_1843();
void SquareAndScale_1844();
void SquareAndScale_1845();
void SquareAndScale_1846();
void SquareAndScale_1847();
void SquareAndScale_1848();
void SquareAndScale_1849();
void SquareAndScale_1850();
void SquareAndScale_1851();
void SquareAndScale_1852();
void SquareAndScale_1853();
void SquareAndScale_1854();
void SquareAndScale_1855();
void SquareAndScale_1856();
void SquareAndScale_1857();
void SquareAndScale_1858();
void SquareAndScale_1859();
void SquareAndScale_1860();
void SquareAndScale_1861();
void SquareAndScale_1862();
void SquareAndScale_1863();
void SquareAndScale_1864();
void SquareAndScale_1865();
void SquareAndScale_1866();
void SquareAndScale_1867();
void SquareAndScale_1868();
void WEIGHTED_ROUND_ROBIN_Joiner_1818();
void CFAR_gather_1814();
void AnonFilter_a0_1815();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1

#define __DEFLOOPBOUND__72__ -1

#define __DEFLOOPBOUND__73__ -1

#define __DEFLOOPBOUND__74__ -1

#define __DEFLOOPBOUND__75__ -1

#define __DEFLOOPBOUND__76__ -1

#define __DEFLOOPBOUND__77__ -1

#define __DEFLOOPBOUND__78__ -1

#define __DEFLOOPBOUND__79__ -1

#define __DEFLOOPBOUND__80__ -1

#define __DEFLOOPBOUND__81__ -1

#define __DEFLOOPBOUND__82__ -1

#define __DEFLOOPBOUND__83__ -1

#define __DEFLOOPBOUND__84__ -1

#define __DEFLOOPBOUND__85__ -1

#define __DEFLOOPBOUND__86__ -1

#define __DEFLOOPBOUND__87__ -1

#define __DEFLOOPBOUND__88__ -1

#define __DEFLOOPBOUND__89__ -1

#define __DEFLOOPBOUND__90__ -1

#define __DEFLOOPBOUND__91__ -1

#define __DEFLOOPBOUND__92__ -1

#define __DEFLOOPBOUND__93__ -1

#define __DEFLOOPBOUND__94__ -1

#define __DEFLOOPBOUND__95__ -1


#ifdef __cplusplus
}
#endif
#endif
