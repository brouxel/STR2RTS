#include "PEG25-CFARtest.h"

buffer_complex_t ComplexSource_4011WEIGHTED_ROUND_ROBIN_Splitter_4017;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4044_4046_split[25];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4018CFAR_gather_4014;
buffer_float_t CFAR_gather_4014AnonFilter_a0_4015;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4044_4046_join[25];


ComplexSource_4011_t ComplexSource_4011_s;
CFAR_gather_4014_t CFAR_gather_4014_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4011_s.theta = (ComplexSource_4011_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4011_s.theta)) * (((float) cos(ComplexSource_4011_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4011_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4011_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4011_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4011_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4011_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4011_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4011() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		ComplexSource(&(ComplexSource_4011WEIGHTED_ROUND_ROBIN_Splitter_4017));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4019() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[0]));
	ENDFOR
}

void SquareAndScale_4020() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[1]));
	ENDFOR
}

void SquareAndScale_4021() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[2]));
	ENDFOR
}

void SquareAndScale_4022() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[3]));
	ENDFOR
}

void SquareAndScale_4023() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[4]));
	ENDFOR
}

void SquareAndScale_4024() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[5]));
	ENDFOR
}

void SquareAndScale_4025() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[6]));
	ENDFOR
}

void SquareAndScale_4026() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[7]));
	ENDFOR
}

void SquareAndScale_4027() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[8]));
	ENDFOR
}

void SquareAndScale_4028() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[9]));
	ENDFOR
}

void SquareAndScale_4029() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[10]));
	ENDFOR
}

void SquareAndScale_4030() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[11]));
	ENDFOR
}

void SquareAndScale_4031() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[12]));
	ENDFOR
}

void SquareAndScale_4032() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[13]));
	ENDFOR
}

void SquareAndScale_4033() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[14]));
	ENDFOR
}

void SquareAndScale_4034() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[15]));
	ENDFOR
}

void SquareAndScale_4035() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[16]));
	ENDFOR
}

void SquareAndScale_4036() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[17]));
	ENDFOR
}

void SquareAndScale_4037() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[18]));
	ENDFOR
}

void SquareAndScale_4038() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[19]));
	ENDFOR
}

void SquareAndScale_4039() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[20]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[20]));
	ENDFOR
}

void SquareAndScale_4040() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[21]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[21]));
	ENDFOR
}

void SquareAndScale_4041() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[22]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[22]));
	ENDFOR
}

void SquareAndScale_4042() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[23]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[23]));
	ENDFOR
}

void SquareAndScale_4043() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[24]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[24]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4017() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4044_4046_split[__iter_], pop_complex(&ComplexSource_4011WEIGHTED_ROUND_ROBIN_Splitter_4017));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4018() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4018CFAR_gather_4014, pop_float(&SplitJoin0_SquareAndScale_Fiss_4044_4046_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4014_s.pos) - 5) >= 0)), __DEFLOOPBOUND__242__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4014_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4014_s.pos) < 64)), __DEFLOOPBOUND__243__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4014_s.poke[(i - 1)] = CFAR_gather_4014_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4014_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4014_s.pos++ ; 
		if(CFAR_gather_4014_s.pos == 64) {
			CFAR_gather_4014_s.pos = 0 ; 
		}
	}


void CFAR_gather_4014() {
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4018CFAR_gather_4014), &(CFAR_gather_4014AnonFilter_a0_4015));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4015() {
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4014AnonFilter_a0_4015));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_4011WEIGHTED_ROUND_ROBIN_Splitter_4017);
	FOR(int, __iter_init_0_, 0, <, 25, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4044_4046_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4018CFAR_gather_4014);
	init_buffer_float(&CFAR_gather_4014AnonFilter_a0_4015);
	FOR(int, __iter_init_1_, 0, <, 25, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4044_4046_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4011
	 {
	ComplexSource_4011_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4011WEIGHTED_ROUND_ROBIN_Splitter_4017));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4017
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4044_4046_split[__iter_], pop_complex(&ComplexSource_4011WEIGHTED_ROUND_ROBIN_Splitter_4017));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4019
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4020
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4021
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4022
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4023
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4024
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4025
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4026
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4027
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4028
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4029
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4030
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4031
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4032
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4033
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4034
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4035
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4036
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4037
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[18]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4038
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[19]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4039
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[20]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4040
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[21]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4041
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[22]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[22]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4042
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[23]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[23]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4043
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4044_4046_split[24]), &(SplitJoin0_SquareAndScale_Fiss_4044_4046_join[24]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4018
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4018CFAR_gather_4014, pop_float(&SplitJoin0_SquareAndScale_Fiss_4044_4046_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4014
	 {
	CFAR_gather_4014_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4018CFAR_gather_4014), &(CFAR_gather_4014AnonFilter_a0_4015));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4015
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4014AnonFilter_a0_4015));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4011();
		WEIGHTED_ROUND_ROBIN_Splitter_4017();
			SquareAndScale_4019();
			SquareAndScale_4020();
			SquareAndScale_4021();
			SquareAndScale_4022();
			SquareAndScale_4023();
			SquareAndScale_4024();
			SquareAndScale_4025();
			SquareAndScale_4026();
			SquareAndScale_4027();
			SquareAndScale_4028();
			SquareAndScale_4029();
			SquareAndScale_4030();
			SquareAndScale_4031();
			SquareAndScale_4032();
			SquareAndScale_4033();
			SquareAndScale_4034();
			SquareAndScale_4035();
			SquareAndScale_4036();
			SquareAndScale_4037();
			SquareAndScale_4038();
			SquareAndScale_4039();
			SquareAndScale_4040();
			SquareAndScale_4041();
			SquareAndScale_4042();
			SquareAndScale_4043();
		WEIGHTED_ROUND_ROBIN_Joiner_4018();
		CFAR_gather_4014();
		AnonFilter_a0_4015();
	ENDFOR
	return EXIT_SUCCESS;
}
