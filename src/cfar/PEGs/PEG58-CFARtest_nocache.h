#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=20955 on the compile command line
#else
#if BUF_SIZEMAX < 20955
#error BUF_SIZEMAX too small, it must be at least 20955
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_843_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_846_t;
void ComplexSource_843();
void WEIGHTED_ROUND_ROBIN_Splitter_849();
void SquareAndScale_851();
void SquareAndScale_852();
void SquareAndScale_853();
void SquareAndScale_854();
void SquareAndScale_855();
void SquareAndScale_856();
void SquareAndScale_857();
void SquareAndScale_858();
void SquareAndScale_859();
void SquareAndScale_860();
void SquareAndScale_861();
void SquareAndScale_862();
void SquareAndScale_863();
void SquareAndScale_864();
void SquareAndScale_865();
void SquareAndScale_866();
void SquareAndScale_867();
void SquareAndScale_868();
void SquareAndScale_869();
void SquareAndScale_870();
void SquareAndScale_871();
void SquareAndScale_872();
void SquareAndScale_873();
void SquareAndScale_874();
void SquareAndScale_875();
void SquareAndScale_876();
void SquareAndScale_877();
void SquareAndScale_878();
void SquareAndScale_879();
void SquareAndScale_880();
void SquareAndScale_881();
void SquareAndScale_882();
void SquareAndScale_883();
void SquareAndScale_884();
void SquareAndScale_885();
void SquareAndScale_886();
void SquareAndScale_887();
void SquareAndScale_888();
void SquareAndScale_889();
void SquareAndScale_890();
void SquareAndScale_891();
void SquareAndScale_892();
void SquareAndScale_893();
void SquareAndScale_894();
void SquareAndScale_895();
void SquareAndScale_896();
void SquareAndScale_897();
void SquareAndScale_898();
void SquareAndScale_899();
void SquareAndScale_900();
void SquareAndScale_901();
void SquareAndScale_902();
void SquareAndScale_903();
void SquareAndScale_904();
void SquareAndScale_905();
void SquareAndScale_906();
void SquareAndScale_907();
void SquareAndScale_908();
void WEIGHTED_ROUND_ROBIN_Joiner_850();
void CFAR_gather_846();
void AnonFilter_a0_847();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1


#ifdef __cplusplus
}
#endif
#endif
