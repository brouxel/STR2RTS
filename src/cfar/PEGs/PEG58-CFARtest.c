#include "PEG58-CFARtest.h"

buffer_complex_t ComplexSource_843WEIGHTED_ROUND_ROBIN_Splitter_849;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_909_911_split[58];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_909_911_join[58];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_850CFAR_gather_846;
buffer_float_t CFAR_gather_846AnonFilter_a0_847;


ComplexSource_843_t ComplexSource_843_s;
CFAR_gather_846_t CFAR_gather_846_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_843_s.theta = (ComplexSource_843_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_843_s.theta)) * (((float) cos(ComplexSource_843_s.theta)) + ((0.0 * ((float) sin(ComplexSource_843_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_843_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_843_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_843_s.theta))))) + (0.0 * (((float) cos(ComplexSource_843_s.theta)) + ((0.0 * ((float) sin(ComplexSource_843_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_843() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		ComplexSource(&(ComplexSource_843WEIGHTED_ROUND_ROBIN_Splitter_849));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_851() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[0]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[0]));
	ENDFOR
}

void SquareAndScale_852() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[1]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[1]));
	ENDFOR
}

void SquareAndScale_853() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[2]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[2]));
	ENDFOR
}

void SquareAndScale_854() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[3]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[3]));
	ENDFOR
}

void SquareAndScale_855() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[4]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[4]));
	ENDFOR
}

void SquareAndScale_856() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[5]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[5]));
	ENDFOR
}

void SquareAndScale_857() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[6]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[6]));
	ENDFOR
}

void SquareAndScale_858() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[7]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[7]));
	ENDFOR
}

void SquareAndScale_859() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[8]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[8]));
	ENDFOR
}

void SquareAndScale_860() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[9]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[9]));
	ENDFOR
}

void SquareAndScale_861() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[10]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[10]));
	ENDFOR
}

void SquareAndScale_862() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[11]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[11]));
	ENDFOR
}

void SquareAndScale_863() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[12]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[12]));
	ENDFOR
}

void SquareAndScale_864() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[13]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[13]));
	ENDFOR
}

void SquareAndScale_865() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[14]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[14]));
	ENDFOR
}

void SquareAndScale_866() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[15]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[15]));
	ENDFOR
}

void SquareAndScale_867() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[16]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[16]));
	ENDFOR
}

void SquareAndScale_868() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[17]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[17]));
	ENDFOR
}

void SquareAndScale_869() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[18]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[18]));
	ENDFOR
}

void SquareAndScale_870() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[19]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[19]));
	ENDFOR
}

void SquareAndScale_871() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[20]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[20]));
	ENDFOR
}

void SquareAndScale_872() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[21]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[21]));
	ENDFOR
}

void SquareAndScale_873() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[22]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[22]));
	ENDFOR
}

void SquareAndScale_874() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[23]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[23]));
	ENDFOR
}

void SquareAndScale_875() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[24]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[24]));
	ENDFOR
}

void SquareAndScale_876() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[25]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[25]));
	ENDFOR
}

void SquareAndScale_877() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[26]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[26]));
	ENDFOR
}

void SquareAndScale_878() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[27]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[27]));
	ENDFOR
}

void SquareAndScale_879() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[28]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[28]));
	ENDFOR
}

void SquareAndScale_880() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[29]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[29]));
	ENDFOR
}

void SquareAndScale_881() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[30]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[30]));
	ENDFOR
}

void SquareAndScale_882() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[31]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[31]));
	ENDFOR
}

void SquareAndScale_883() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[32]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[32]));
	ENDFOR
}

void SquareAndScale_884() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[33]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[33]));
	ENDFOR
}

void SquareAndScale_885() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[34]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[34]));
	ENDFOR
}

void SquareAndScale_886() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[35]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[35]));
	ENDFOR
}

void SquareAndScale_887() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[36]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[36]));
	ENDFOR
}

void SquareAndScale_888() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[37]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[37]));
	ENDFOR
}

void SquareAndScale_889() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[38]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[38]));
	ENDFOR
}

void SquareAndScale_890() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[39]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[39]));
	ENDFOR
}

void SquareAndScale_891() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[40]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[40]));
	ENDFOR
}

void SquareAndScale_892() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[41]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[41]));
	ENDFOR
}

void SquareAndScale_893() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[42]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[42]));
	ENDFOR
}

void SquareAndScale_894() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[43]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[43]));
	ENDFOR
}

void SquareAndScale_895() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[44]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[44]));
	ENDFOR
}

void SquareAndScale_896() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[45]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[45]));
	ENDFOR
}

void SquareAndScale_897() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[46]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[46]));
	ENDFOR
}

void SquareAndScale_898() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[47]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[47]));
	ENDFOR
}

void SquareAndScale_899() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[48]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[48]));
	ENDFOR
}

void SquareAndScale_900() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[49]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[49]));
	ENDFOR
}

void SquareAndScale_901() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[50]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[50]));
	ENDFOR
}

void SquareAndScale_902() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[51]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[51]));
	ENDFOR
}

void SquareAndScale_903() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[52]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[52]));
	ENDFOR
}

void SquareAndScale_904() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[53]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[53]));
	ENDFOR
}

void SquareAndScale_905() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[54]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[54]));
	ENDFOR
}

void SquareAndScale_906() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[55]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[55]));
	ENDFOR
}

void SquareAndScale_907() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[56]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[56]));
	ENDFOR
}

void SquareAndScale_908() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[57]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[57]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_849() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 58, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_909_911_split[__iter_], pop_complex(&ComplexSource_843WEIGHTED_ROUND_ROBIN_Splitter_849));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_850() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 58, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_850CFAR_gather_846, pop_float(&SplitJoin0_SquareAndScale_Fiss_909_911_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_846_s.pos) - 5) >= 0)), __DEFLOOPBOUND__44__, i__conflict__1++) {
			sum = (sum + CFAR_gather_846_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_846_s.pos) < 64)), __DEFLOOPBOUND__45__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_846_s.poke[(i - 1)] = CFAR_gather_846_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_846_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_846_s.pos++ ; 
		if(CFAR_gather_846_s.pos == 64) {
			CFAR_gather_846_s.pos = 0 ; 
		}
	}


void CFAR_gather_846() {
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_850CFAR_gather_846), &(CFAR_gather_846AnonFilter_a0_847));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_847() {
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_846AnonFilter_a0_847));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_843WEIGHTED_ROUND_ROBIN_Splitter_849);
	FOR(int, __iter_init_0_, 0, <, 58, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_909_911_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 58, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_909_911_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_850CFAR_gather_846);
	init_buffer_float(&CFAR_gather_846AnonFilter_a0_847);
// --- init: ComplexSource_843
	 {
	ComplexSource_843_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_843WEIGHTED_ROUND_ROBIN_Splitter_849));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_849
	
	FOR(uint32_t, __iter_, 0, <, 58, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_909_911_split[__iter_], pop_complex(&ComplexSource_843WEIGHTED_ROUND_ROBIN_Splitter_849));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_851
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[0]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[0]));
//--------------------------------
// --- init: SquareAndScale_852
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[1]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[1]));
//--------------------------------
// --- init: SquareAndScale_853
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[2]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[2]));
//--------------------------------
// --- init: SquareAndScale_854
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[3]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[3]));
//--------------------------------
// --- init: SquareAndScale_855
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[4]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[4]));
//--------------------------------
// --- init: SquareAndScale_856
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[5]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[5]));
//--------------------------------
// --- init: SquareAndScale_857
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[6]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[6]));
//--------------------------------
// --- init: SquareAndScale_858
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[7]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[7]));
//--------------------------------
// --- init: SquareAndScale_859
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[8]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[8]));
//--------------------------------
// --- init: SquareAndScale_860
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[9]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[9]));
//--------------------------------
// --- init: SquareAndScale_861
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[10]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[10]));
//--------------------------------
// --- init: SquareAndScale_862
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[11]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[11]));
//--------------------------------
// --- init: SquareAndScale_863
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[12]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[12]));
//--------------------------------
// --- init: SquareAndScale_864
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[13]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[13]));
//--------------------------------
// --- init: SquareAndScale_865
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[14]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[14]));
//--------------------------------
// --- init: SquareAndScale_866
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[15]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[15]));
//--------------------------------
// --- init: SquareAndScale_867
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[16]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[16]));
//--------------------------------
// --- init: SquareAndScale_868
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[17]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[17]));
//--------------------------------
// --- init: SquareAndScale_869
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[18]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[18]));
//--------------------------------
// --- init: SquareAndScale_870
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[19]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[19]));
//--------------------------------
// --- init: SquareAndScale_871
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[20]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[20]));
//--------------------------------
// --- init: SquareAndScale_872
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[21]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[21]));
//--------------------------------
// --- init: SquareAndScale_873
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[22]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[22]));
//--------------------------------
// --- init: SquareAndScale_874
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[23]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[23]));
//--------------------------------
// --- init: SquareAndScale_875
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[24]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[24]));
//--------------------------------
// --- init: SquareAndScale_876
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[25]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[25]));
//--------------------------------
// --- init: SquareAndScale_877
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[26]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[26]));
//--------------------------------
// --- init: SquareAndScale_878
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[27]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[27]));
//--------------------------------
// --- init: SquareAndScale_879
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[28]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[28]));
//--------------------------------
// --- init: SquareAndScale_880
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[29]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[29]));
//--------------------------------
// --- init: SquareAndScale_881
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[30]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[30]));
//--------------------------------
// --- init: SquareAndScale_882
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[31]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[31]));
//--------------------------------
// --- init: SquareAndScale_883
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[32]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[32]));
//--------------------------------
// --- init: SquareAndScale_884
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[33]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[33]));
//--------------------------------
// --- init: SquareAndScale_885
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[34]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[34]));
//--------------------------------
// --- init: SquareAndScale_886
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[35]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[35]));
//--------------------------------
// --- init: SquareAndScale_887
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[36]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[36]));
//--------------------------------
// --- init: SquareAndScale_888
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[37]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[37]));
//--------------------------------
// --- init: SquareAndScale_889
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[38]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[38]));
//--------------------------------
// --- init: SquareAndScale_890
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[39]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[39]));
//--------------------------------
// --- init: SquareAndScale_891
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[40]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[40]));
//--------------------------------
// --- init: SquareAndScale_892
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[41]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[41]));
//--------------------------------
// --- init: SquareAndScale_893
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[42]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[42]));
//--------------------------------
// --- init: SquareAndScale_894
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[43]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[43]));
//--------------------------------
// --- init: SquareAndScale_895
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[44]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[44]));
//--------------------------------
// --- init: SquareAndScale_896
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[45]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[45]));
//--------------------------------
// --- init: SquareAndScale_897
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[46]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[46]));
//--------------------------------
// --- init: SquareAndScale_898
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[47]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[47]));
//--------------------------------
// --- init: SquareAndScale_899
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[48]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[48]));
//--------------------------------
// --- init: SquareAndScale_900
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[49]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[49]));
//--------------------------------
// --- init: SquareAndScale_901
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[50]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[50]));
//--------------------------------
// --- init: SquareAndScale_902
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[51]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[51]));
//--------------------------------
// --- init: SquareAndScale_903
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[52]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[52]));
//--------------------------------
// --- init: SquareAndScale_904
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[53]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[53]));
//--------------------------------
// --- init: SquareAndScale_905
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[54]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[54]));
//--------------------------------
// --- init: SquareAndScale_906
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[55]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[55]));
//--------------------------------
// --- init: SquareAndScale_907
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[56]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[56]));
//--------------------------------
// --- init: SquareAndScale_908
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_909_911_split[57]), &(SplitJoin0_SquareAndScale_Fiss_909_911_join[57]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_850
	
	FOR(uint32_t, __iter_, 0, <, 58, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_850CFAR_gather_846, pop_float(&SplitJoin0_SquareAndScale_Fiss_909_911_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_846
	 {
	CFAR_gather_846_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 49, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_850CFAR_gather_846), &(CFAR_gather_846AnonFilter_a0_847));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_847
	FOR(uint32_t, __iter_init_, 0, <, 49, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_846AnonFilter_a0_847));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_843();
		WEIGHTED_ROUND_ROBIN_Splitter_849();
			SquareAndScale_851();
			SquareAndScale_852();
			SquareAndScale_853();
			SquareAndScale_854();
			SquareAndScale_855();
			SquareAndScale_856();
			SquareAndScale_857();
			SquareAndScale_858();
			SquareAndScale_859();
			SquareAndScale_860();
			SquareAndScale_861();
			SquareAndScale_862();
			SquareAndScale_863();
			SquareAndScale_864();
			SquareAndScale_865();
			SquareAndScale_866();
			SquareAndScale_867();
			SquareAndScale_868();
			SquareAndScale_869();
			SquareAndScale_870();
			SquareAndScale_871();
			SquareAndScale_872();
			SquareAndScale_873();
			SquareAndScale_874();
			SquareAndScale_875();
			SquareAndScale_876();
			SquareAndScale_877();
			SquareAndScale_878();
			SquareAndScale_879();
			SquareAndScale_880();
			SquareAndScale_881();
			SquareAndScale_882();
			SquareAndScale_883();
			SquareAndScale_884();
			SquareAndScale_885();
			SquareAndScale_886();
			SquareAndScale_887();
			SquareAndScale_888();
			SquareAndScale_889();
			SquareAndScale_890();
			SquareAndScale_891();
			SquareAndScale_892();
			SquareAndScale_893();
			SquareAndScale_894();
			SquareAndScale_895();
			SquareAndScale_896();
			SquareAndScale_897();
			SquareAndScale_898();
			SquareAndScale_899();
			SquareAndScale_900();
			SquareAndScale_901();
			SquareAndScale_902();
			SquareAndScale_903();
			SquareAndScale_904();
			SquareAndScale_905();
			SquareAndScale_906();
			SquareAndScale_907();
			SquareAndScale_908();
		WEIGHTED_ROUND_ROBIN_Joiner_850();
		CFAR_gather_846();
		AnonFilter_a0_847();
	ENDFOR
	return EXIT_SUCCESS;
}
