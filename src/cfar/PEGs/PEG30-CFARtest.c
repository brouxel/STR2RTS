#include "PEG30-CFARtest.h"

buffer_float_t CFAR_gather_3674AnonFilter_a0_3675;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3709_3711_split[30];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3678CFAR_gather_3674;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3709_3711_join[30];
buffer_complex_t ComplexSource_3671WEIGHTED_ROUND_ROBIN_Splitter_3677;


ComplexSource_3671_t ComplexSource_3671_s;
CFAR_gather_3674_t CFAR_gather_3674_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3671_s.theta = (ComplexSource_3671_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3671_s.theta)) * (((float) cos(ComplexSource_3671_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3671_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3671_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3671_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3671_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3671_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3671_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3671() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		ComplexSource(&(ComplexSource_3671WEIGHTED_ROUND_ROBIN_Splitter_3677));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3679() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[0]));
	ENDFOR
}

void SquareAndScale_3680() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[1]));
	ENDFOR
}

void SquareAndScale_3681() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[2]));
	ENDFOR
}

void SquareAndScale_3682() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[3]));
	ENDFOR
}

void SquareAndScale_3683() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[4]));
	ENDFOR
}

void SquareAndScale_3684() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[5]));
	ENDFOR
}

void SquareAndScale_3685() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[6]));
	ENDFOR
}

void SquareAndScale_3686() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[7]));
	ENDFOR
}

void SquareAndScale_3687() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[8]));
	ENDFOR
}

void SquareAndScale_3688() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[9]));
	ENDFOR
}

void SquareAndScale_3689() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[10]));
	ENDFOR
}

void SquareAndScale_3690() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[11]));
	ENDFOR
}

void SquareAndScale_3691() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[12]));
	ENDFOR
}

void SquareAndScale_3692() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[13]));
	ENDFOR
}

void SquareAndScale_3693() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[14]));
	ENDFOR
}

void SquareAndScale_3694() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[15]));
	ENDFOR
}

void SquareAndScale_3695() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[16]));
	ENDFOR
}

void SquareAndScale_3696() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[17]));
	ENDFOR
}

void SquareAndScale_3697() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[18]));
	ENDFOR
}

void SquareAndScale_3698() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[19]));
	ENDFOR
}

void SquareAndScale_3699() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[20]));
	ENDFOR
}

void SquareAndScale_3700() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[21]));
	ENDFOR
}

void SquareAndScale_3701() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[22]));
	ENDFOR
}

void SquareAndScale_3702() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[23]));
	ENDFOR
}

void SquareAndScale_3703() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[24]));
	ENDFOR
}

void SquareAndScale_3704() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[25]));
	ENDFOR
}

void SquareAndScale_3705() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[26]));
	ENDFOR
}

void SquareAndScale_3706() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[27]));
	ENDFOR
}

void SquareAndScale_3707() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[28]));
	ENDFOR
}

void SquareAndScale_3708() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[29]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3677() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3709_3711_split[__iter_], pop_complex(&ComplexSource_3671WEIGHTED_ROUND_ROBIN_Splitter_3677));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3678() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3678CFAR_gather_3674, pop_float(&SplitJoin0_SquareAndScale_Fiss_3709_3711_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3674_s.pos) - 5) >= 0)), __DEFLOOPBOUND__212__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3674_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3674_s.pos) < 64)), __DEFLOOPBOUND__213__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3674_s.poke[(i - 1)] = CFAR_gather_3674_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3674_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3674_s.pos++ ; 
		if(CFAR_gather_3674_s.pos == 64) {
			CFAR_gather_3674_s.pos = 0 ; 
		}
	}


void CFAR_gather_3674() {
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3678CFAR_gather_3674), &(CFAR_gather_3674AnonFilter_a0_3675));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3675() {
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3674AnonFilter_a0_3675));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&CFAR_gather_3674AnonFilter_a0_3675);
	FOR(int, __iter_init_0_, 0, <, 30, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3709_3711_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3678CFAR_gather_3674);
	FOR(int, __iter_init_1_, 0, <, 30, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3709_3711_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_3671WEIGHTED_ROUND_ROBIN_Splitter_3677);
// --- init: ComplexSource_3671
	 {
	ComplexSource_3671_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3671WEIGHTED_ROUND_ROBIN_Splitter_3677));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3677
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3709_3711_split[__iter_], pop_complex(&ComplexSource_3671WEIGHTED_ROUND_ROBIN_Splitter_3677));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3679
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3680
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3681
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3682
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3683
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3684
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3685
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3686
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3687
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3688
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3689
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3690
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3691
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3692
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3693
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3694
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3695
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3696
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[17]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3697
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[18]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3698
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[19]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3699
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[20]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3700
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[21]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3701
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[22]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3702
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[23]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3703
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[24]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3704
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[25]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3705
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[26]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3706
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[27]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3707
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[28]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3708
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3709_3711_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3709_3711_join[29]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3678
	FOR(uint32_t, __iter_init_, 0, <, 2, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3678CFAR_gather_3674, pop_float(&SplitJoin0_SquareAndScale_Fiss_3709_3711_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3674
	 {
	CFAR_gather_3674_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3678CFAR_gather_3674), &(CFAR_gather_3674AnonFilter_a0_3675));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3675
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3674AnonFilter_a0_3675));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3671();
		WEIGHTED_ROUND_ROBIN_Splitter_3677();
			SquareAndScale_3679();
			SquareAndScale_3680();
			SquareAndScale_3681();
			SquareAndScale_3682();
			SquareAndScale_3683();
			SquareAndScale_3684();
			SquareAndScale_3685();
			SquareAndScale_3686();
			SquareAndScale_3687();
			SquareAndScale_3688();
			SquareAndScale_3689();
			SquareAndScale_3690();
			SquareAndScale_3691();
			SquareAndScale_3692();
			SquareAndScale_3693();
			SquareAndScale_3694();
			SquareAndScale_3695();
			SquareAndScale_3696();
			SquareAndScale_3697();
			SquareAndScale_3698();
			SquareAndScale_3699();
			SquareAndScale_3700();
			SquareAndScale_3701();
			SquareAndScale_3702();
			SquareAndScale_3703();
			SquareAndScale_3704();
			SquareAndScale_3705();
			SquareAndScale_3706();
			SquareAndScale_3707();
			SquareAndScale_3708();
		WEIGHTED_ROUND_ROBIN_Joiner_3678();
		CFAR_gather_3674();
		AnonFilter_a0_3675();
	ENDFOR
	return EXIT_SUCCESS;
}
