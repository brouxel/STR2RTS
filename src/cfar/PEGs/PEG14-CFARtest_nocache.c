#include "PEG14-CFARtest_nocache.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4605_4607_split[14];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4590CFAR_gather_4586;
buffer_complex_t ComplexSource_4583WEIGHTED_ROUND_ROBIN_Splitter_4589;
buffer_float_t CFAR_gather_4586AnonFilter_a0_4587;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4605_4607_join[14];


ComplexSource_4583_t ComplexSource_4583_s;
CFAR_gather_4586_t CFAR_gather_4586_s;

void ComplexSource_4583(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4583_s.theta = (ComplexSource_4583_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4583_s.theta)) * (((float) cos(ComplexSource_4583_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4583_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4583_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4583_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4583_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4583_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4583_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_4583WEIGHTED_ROUND_ROBIN_Splitter_4589, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_4591(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4592(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4593(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4594(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4595(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4596(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4597(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4598(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4599(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4600(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4601(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4602(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4603(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4604(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4589() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[__iter_], pop_complex(&ComplexSource_4583WEIGHTED_ROUND_ROBIN_Splitter_4589));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4590() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4590CFAR_gather_4586, pop_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4586(){
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4586_s.pos) - 5) >= 0)), __DEFLOOPBOUND__308__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4586_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4586_s.pos) < 64)), __DEFLOOPBOUND__309__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4590CFAR_gather_4586, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4586AnonFilter_a0_4587, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4586_s.poke[(i - 1)] = CFAR_gather_4586_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4586_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4590CFAR_gather_4586) ; 
		CFAR_gather_4586_s.pos++ ; 
		if(CFAR_gather_4586_s.pos == 64) {
			CFAR_gather_4586_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4587(){
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4586AnonFilter_a0_4587));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 14, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4590CFAR_gather_4586);
	init_buffer_complex(&ComplexSource_4583WEIGHTED_ROUND_ROBIN_Splitter_4589);
	init_buffer_float(&CFAR_gather_4586AnonFilter_a0_4587);
	FOR(int, __iter_init_1_, 0, <, 14, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4583
	 {
	ComplexSource_4583_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4583_s.theta = (ComplexSource_4583_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4583_s.theta)) * (((float) cos(ComplexSource_4583_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4583_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4583_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4583_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4583_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4583_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4583_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4583WEIGHTED_ROUND_ROBIN_Splitter_4589, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4589
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[__iter_], pop_complex(&ComplexSource_4583WEIGHTED_ROUND_ROBIN_Splitter_4589));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4591
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4592
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4593
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4594
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4595
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4596
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4597
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4598
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4599
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4600
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4601
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4602
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4603
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4604
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4605_4607_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4590
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4590CFAR_gather_4586, pop_float(&SplitJoin0_SquareAndScale_Fiss_4605_4607_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4586
	 {
	CFAR_gather_4586_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4586_s.pos) - 5) >= 0)), __DEFLOOPBOUND__310__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4586_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4586_s.pos) < 64)), __DEFLOOPBOUND__311__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4590CFAR_gather_4586, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4586AnonFilter_a0_4587, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4586_s.poke[(i - 1)] = CFAR_gather_4586_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4586_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4590CFAR_gather_4586) ; 
		CFAR_gather_4586_s.pos++ ; 
		if(CFAR_gather_4586_s.pos == 64) {
			CFAR_gather_4586_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4587
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4586AnonFilter_a0_4587));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4583();
		WEIGHTED_ROUND_ROBIN_Splitter_4589();
			SquareAndScale_4591();
			SquareAndScale_4592();
			SquareAndScale_4593();
			SquareAndScale_4594();
			SquareAndScale_4595();
			SquareAndScale_4596();
			SquareAndScale_4597();
			SquareAndScale_4598();
			SquareAndScale_4599();
			SquareAndScale_4600();
			SquareAndScale_4601();
			SquareAndScale_4602();
			SquareAndScale_4603();
			SquareAndScale_4604();
		WEIGHTED_ROUND_ROBIN_Joiner_4590();
		CFAR_gather_4586();
		AnonFilter_a0_4587();
	ENDFOR
	return EXIT_SUCCESS;
}
