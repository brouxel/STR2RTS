#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=11121 on the compile command line
#else
#if BUF_SIZEMAX < 11121
#error BUF_SIZEMAX too small, it must be at least 11121
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_3671_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_3674_t;
void ComplexSource_3671();
void WEIGHTED_ROUND_ROBIN_Splitter_3677();
void SquareAndScale_3679();
void SquareAndScale_3680();
void SquareAndScale_3681();
void SquareAndScale_3682();
void SquareAndScale_3683();
void SquareAndScale_3684();
void SquareAndScale_3685();
void SquareAndScale_3686();
void SquareAndScale_3687();
void SquareAndScale_3688();
void SquareAndScale_3689();
void SquareAndScale_3690();
void SquareAndScale_3691();
void SquareAndScale_3692();
void SquareAndScale_3693();
void SquareAndScale_3694();
void SquareAndScale_3695();
void SquareAndScale_3696();
void SquareAndScale_3697();
void SquareAndScale_3698();
void SquareAndScale_3699();
void SquareAndScale_3700();
void SquareAndScale_3701();
void SquareAndScale_3702();
void SquareAndScale_3703();
void SquareAndScale_3704();
void SquareAndScale_3705();
void SquareAndScale_3706();
void SquareAndScale_3707();
void SquareAndScale_3708();
void WEIGHTED_ROUND_ROBIN_Joiner_3678();
void CFAR_gather_3674();
void AnonFilter_a0_3675();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1

#define __DEFLOOPBOUND__72__ -1

#define __DEFLOOPBOUND__73__ -1

#define __DEFLOOPBOUND__74__ -1

#define __DEFLOOPBOUND__75__ -1

#define __DEFLOOPBOUND__76__ -1

#define __DEFLOOPBOUND__77__ -1

#define __DEFLOOPBOUND__78__ -1

#define __DEFLOOPBOUND__79__ -1

#define __DEFLOOPBOUND__80__ -1

#define __DEFLOOPBOUND__81__ -1

#define __DEFLOOPBOUND__82__ -1

#define __DEFLOOPBOUND__83__ -1

#define __DEFLOOPBOUND__84__ -1

#define __DEFLOOPBOUND__85__ -1

#define __DEFLOOPBOUND__86__ -1

#define __DEFLOOPBOUND__87__ -1

#define __DEFLOOPBOUND__88__ -1

#define __DEFLOOPBOUND__89__ -1

#define __DEFLOOPBOUND__90__ -1

#define __DEFLOOPBOUND__91__ -1

#define __DEFLOOPBOUND__92__ -1

#define __DEFLOOPBOUND__93__ -1

#define __DEFLOOPBOUND__94__ -1

#define __DEFLOOPBOUND__95__ -1

#define __DEFLOOPBOUND__96__ -1

#define __DEFLOOPBOUND__97__ -1

#define __DEFLOOPBOUND__98__ -1

#define __DEFLOOPBOUND__99__ -1

#define __DEFLOOPBOUND__100__ -1

#define __DEFLOOPBOUND__101__ -1

#define __DEFLOOPBOUND__102__ -1

#define __DEFLOOPBOUND__103__ -1

#define __DEFLOOPBOUND__104__ -1

#define __DEFLOOPBOUND__105__ -1

#define __DEFLOOPBOUND__106__ -1

#define __DEFLOOPBOUND__107__ -1

#define __DEFLOOPBOUND__108__ -1

#define __DEFLOOPBOUND__109__ -1

#define __DEFLOOPBOUND__110__ -1

#define __DEFLOOPBOUND__111__ -1

#define __DEFLOOPBOUND__112__ -1

#define __DEFLOOPBOUND__113__ -1

#define __DEFLOOPBOUND__114__ -1

#define __DEFLOOPBOUND__115__ -1

#define __DEFLOOPBOUND__116__ -1

#define __DEFLOOPBOUND__117__ -1

#define __DEFLOOPBOUND__118__ -1

#define __DEFLOOPBOUND__119__ -1

#define __DEFLOOPBOUND__120__ -1

#define __DEFLOOPBOUND__121__ -1

#define __DEFLOOPBOUND__122__ -1

#define __DEFLOOPBOUND__123__ -1

#define __DEFLOOPBOUND__124__ -1

#define __DEFLOOPBOUND__125__ -1

#define __DEFLOOPBOUND__126__ -1

#define __DEFLOOPBOUND__127__ -1

#define __DEFLOOPBOUND__128__ -1

#define __DEFLOOPBOUND__129__ -1

#define __DEFLOOPBOUND__130__ -1

#define __DEFLOOPBOUND__131__ -1

#define __DEFLOOPBOUND__132__ -1

#define __DEFLOOPBOUND__133__ -1

#define __DEFLOOPBOUND__134__ -1

#define __DEFLOOPBOUND__135__ -1

#define __DEFLOOPBOUND__136__ -1

#define __DEFLOOPBOUND__137__ -1

#define __DEFLOOPBOUND__138__ -1

#define __DEFLOOPBOUND__139__ -1

#define __DEFLOOPBOUND__140__ -1

#define __DEFLOOPBOUND__141__ -1

#define __DEFLOOPBOUND__142__ -1

#define __DEFLOOPBOUND__143__ -1

#define __DEFLOOPBOUND__144__ -1

#define __DEFLOOPBOUND__145__ -1

#define __DEFLOOPBOUND__146__ -1

#define __DEFLOOPBOUND__147__ -1

#define __DEFLOOPBOUND__148__ -1

#define __DEFLOOPBOUND__149__ -1

#define __DEFLOOPBOUND__150__ -1

#define __DEFLOOPBOUND__151__ -1

#define __DEFLOOPBOUND__152__ -1

#define __DEFLOOPBOUND__153__ -1

#define __DEFLOOPBOUND__154__ -1

#define __DEFLOOPBOUND__155__ -1

#define __DEFLOOPBOUND__156__ -1

#define __DEFLOOPBOUND__157__ -1

#define __DEFLOOPBOUND__158__ -1

#define __DEFLOOPBOUND__159__ -1

#define __DEFLOOPBOUND__160__ -1

#define __DEFLOOPBOUND__161__ -1

#define __DEFLOOPBOUND__162__ -1

#define __DEFLOOPBOUND__163__ -1

#define __DEFLOOPBOUND__164__ -1

#define __DEFLOOPBOUND__165__ -1

#define __DEFLOOPBOUND__166__ -1

#define __DEFLOOPBOUND__167__ -1

#define __DEFLOOPBOUND__168__ -1

#define __DEFLOOPBOUND__169__ -1

#define __DEFLOOPBOUND__170__ -1

#define __DEFLOOPBOUND__171__ -1

#define __DEFLOOPBOUND__172__ -1

#define __DEFLOOPBOUND__173__ -1

#define __DEFLOOPBOUND__174__ -1

#define __DEFLOOPBOUND__175__ -1

#define __DEFLOOPBOUND__176__ -1

#define __DEFLOOPBOUND__177__ -1

#define __DEFLOOPBOUND__178__ -1

#define __DEFLOOPBOUND__179__ -1

#define __DEFLOOPBOUND__180__ -1

#define __DEFLOOPBOUND__181__ -1

#define __DEFLOOPBOUND__182__ -1

#define __DEFLOOPBOUND__183__ -1

#define __DEFLOOPBOUND__184__ -1

#define __DEFLOOPBOUND__185__ -1

#define __DEFLOOPBOUND__186__ -1

#define __DEFLOOPBOUND__187__ -1

#define __DEFLOOPBOUND__188__ -1

#define __DEFLOOPBOUND__189__ -1

#define __DEFLOOPBOUND__190__ -1

#define __DEFLOOPBOUND__191__ -1

#define __DEFLOOPBOUND__192__ -1

#define __DEFLOOPBOUND__193__ -1

#define __DEFLOOPBOUND__194__ -1

#define __DEFLOOPBOUND__195__ -1

#define __DEFLOOPBOUND__196__ -1

#define __DEFLOOPBOUND__197__ -1

#define __DEFLOOPBOUND__198__ -1

#define __DEFLOOPBOUND__199__ -1

#define __DEFLOOPBOUND__200__ -1

#define __DEFLOOPBOUND__201__ -1

#define __DEFLOOPBOUND__202__ -1

#define __DEFLOOPBOUND__203__ -1

#define __DEFLOOPBOUND__204__ -1

#define __DEFLOOPBOUND__205__ -1

#define __DEFLOOPBOUND__206__ -1

#define __DEFLOOPBOUND__207__ -1

#define __DEFLOOPBOUND__208__ -1

#define __DEFLOOPBOUND__209__ -1

#define __DEFLOOPBOUND__210__ -1

#define __DEFLOOPBOUND__211__ -1

#define __DEFLOOPBOUND__212__ -1

#define __DEFLOOPBOUND__213__ -1

#define __DEFLOOPBOUND__214__ -1

#define __DEFLOOPBOUND__215__ -1


#ifdef __cplusplus
}
#endif
#endif
