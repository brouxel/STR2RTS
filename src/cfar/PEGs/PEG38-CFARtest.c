#include "PEG38-CFARtest.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_3069_3071_join[38];
buffer_float_t CFAR_gather_3026AnonFilter_a0_3027;
buffer_complex_t ComplexSource_3023WEIGHTED_ROUND_ROBIN_Splitter_3029;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3069_3071_split[38];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3030CFAR_gather_3026;


ComplexSource_3023_t ComplexSource_3023_s;
CFAR_gather_3026_t CFAR_gather_3026_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3023_s.theta = (ComplexSource_3023_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3023_s.theta)) * (((float) cos(ComplexSource_3023_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3023_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3023_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3023_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3023_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3023_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3023_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3023() {
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		ComplexSource(&(ComplexSource_3023WEIGHTED_ROUND_ROBIN_Splitter_3029));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3031() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[0]));
	ENDFOR
}

void SquareAndScale_3032() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[1]));
	ENDFOR
}

void SquareAndScale_3033() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[2]));
	ENDFOR
}

void SquareAndScale_3034() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[3]));
	ENDFOR
}

void SquareAndScale_3035() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[4]));
	ENDFOR
}

void SquareAndScale_3036() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[5]));
	ENDFOR
}

void SquareAndScale_3037() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[6]));
	ENDFOR
}

void SquareAndScale_3038() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[7]));
	ENDFOR
}

void SquareAndScale_3039() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[8]));
	ENDFOR
}

void SquareAndScale_3040() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[9]));
	ENDFOR
}

void SquareAndScale_3041() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[10]));
	ENDFOR
}

void SquareAndScale_3042() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[11]));
	ENDFOR
}

void SquareAndScale_3043() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[12]));
	ENDFOR
}

void SquareAndScale_3044() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[13]));
	ENDFOR
}

void SquareAndScale_3045() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[14]));
	ENDFOR
}

void SquareAndScale_3046() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[15]));
	ENDFOR
}

void SquareAndScale_3047() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[16]));
	ENDFOR
}

void SquareAndScale_3048() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[17]));
	ENDFOR
}

void SquareAndScale_3049() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[18]));
	ENDFOR
}

void SquareAndScale_3050() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[19]));
	ENDFOR
}

void SquareAndScale_3051() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[20]));
	ENDFOR
}

void SquareAndScale_3052() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[21]));
	ENDFOR
}

void SquareAndScale_3053() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[22]));
	ENDFOR
}

void SquareAndScale_3054() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[23]));
	ENDFOR
}

void SquareAndScale_3055() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[24]));
	ENDFOR
}

void SquareAndScale_3056() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[25]));
	ENDFOR
}

void SquareAndScale_3057() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[26]));
	ENDFOR
}

void SquareAndScale_3058() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[27]));
	ENDFOR
}

void SquareAndScale_3059() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[28]));
	ENDFOR
}

void SquareAndScale_3060() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[29]));
	ENDFOR
}

void SquareAndScale_3061() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[30]));
	ENDFOR
}

void SquareAndScale_3062() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[31]));
	ENDFOR
}

void SquareAndScale_3063() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[32]));
	ENDFOR
}

void SquareAndScale_3064() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[33]));
	ENDFOR
}

void SquareAndScale_3065() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[34]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[34]));
	ENDFOR
}

void SquareAndScale_3066() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[35]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[35]));
	ENDFOR
}

void SquareAndScale_3067() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[36]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[36]));
	ENDFOR
}

void SquareAndScale_3068() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[37]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[37]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3029() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 38, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3069_3071_split[__iter_], pop_complex(&ComplexSource_3023WEIGHTED_ROUND_ROBIN_Splitter_3029));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3030() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 38, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3030CFAR_gather_3026, pop_float(&SplitJoin0_SquareAndScale_Fiss_3069_3071_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3026_s.pos) - 5) >= 0)), __DEFLOOPBOUND__164__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3026_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3026_s.pos) < 64)), __DEFLOOPBOUND__165__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3026_s.poke[(i - 1)] = CFAR_gather_3026_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3026_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3026_s.pos++ ; 
		if(CFAR_gather_3026_s.pos == 64) {
			CFAR_gather_3026_s.pos = 0 ; 
		}
	}


void CFAR_gather_3026() {
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3030CFAR_gather_3026), &(CFAR_gather_3026AnonFilter_a0_3027));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3027() {
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3026AnonFilter_a0_3027));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 38, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3069_3071_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_3026AnonFilter_a0_3027);
	init_buffer_complex(&ComplexSource_3023WEIGHTED_ROUND_ROBIN_Splitter_3029);
	FOR(int, __iter_init_1_, 0, <, 38, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3069_3071_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3030CFAR_gather_3026);
// --- init: ComplexSource_3023
	 {
	ComplexSource_3023_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3023WEIGHTED_ROUND_ROBIN_Splitter_3029));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3029
	
	FOR(uint32_t, __iter_, 0, <, 38, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_3069_3071_split[__iter_], pop_complex(&ComplexSource_3023WEIGHTED_ROUND_ROBIN_Splitter_3029));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3031
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[0]));
//--------------------------------
// --- init: SquareAndScale_3032
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[1]));
//--------------------------------
// --- init: SquareAndScale_3033
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[2]));
//--------------------------------
// --- init: SquareAndScale_3034
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[3]));
//--------------------------------
// --- init: SquareAndScale_3035
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[4]));
//--------------------------------
// --- init: SquareAndScale_3036
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[5]));
//--------------------------------
// --- init: SquareAndScale_3037
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[6]));
//--------------------------------
// --- init: SquareAndScale_3038
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[7]));
//--------------------------------
// --- init: SquareAndScale_3039
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[8]));
//--------------------------------
// --- init: SquareAndScale_3040
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[9]));
//--------------------------------
// --- init: SquareAndScale_3041
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[10]));
//--------------------------------
// --- init: SquareAndScale_3042
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[11]));
//--------------------------------
// --- init: SquareAndScale_3043
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[12]));
//--------------------------------
// --- init: SquareAndScale_3044
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[13]));
//--------------------------------
// --- init: SquareAndScale_3045
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[14]));
//--------------------------------
// --- init: SquareAndScale_3046
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[15]));
//--------------------------------
// --- init: SquareAndScale_3047
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[16]));
//--------------------------------
// --- init: SquareAndScale_3048
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[17]));
//--------------------------------
// --- init: SquareAndScale_3049
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[18]));
//--------------------------------
// --- init: SquareAndScale_3050
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[19]));
//--------------------------------
// --- init: SquareAndScale_3051
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[20]));
//--------------------------------
// --- init: SquareAndScale_3052
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[21]));
//--------------------------------
// --- init: SquareAndScale_3053
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[22]));
//--------------------------------
// --- init: SquareAndScale_3054
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[23]));
//--------------------------------
// --- init: SquareAndScale_3055
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[24]));
//--------------------------------
// --- init: SquareAndScale_3056
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[25]));
//--------------------------------
// --- init: SquareAndScale_3057
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[26]));
//--------------------------------
// --- init: SquareAndScale_3058
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[27]));
//--------------------------------
// --- init: SquareAndScale_3059
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[28]));
//--------------------------------
// --- init: SquareAndScale_3060
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[29]));
//--------------------------------
// --- init: SquareAndScale_3061
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[30]));
//--------------------------------
// --- init: SquareAndScale_3062
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[31]));
//--------------------------------
// --- init: SquareAndScale_3063
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[32]));
//--------------------------------
// --- init: SquareAndScale_3064
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[33]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[33]));
//--------------------------------
// --- init: SquareAndScale_3065
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[34]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[34]));
//--------------------------------
// --- init: SquareAndScale_3066
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[35]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[35]));
//--------------------------------
// --- init: SquareAndScale_3067
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[36]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[36]));
//--------------------------------
// --- init: SquareAndScale_3068
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3069_3071_split[37]), &(SplitJoin0_SquareAndScale_Fiss_3069_3071_join[37]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3030
	
	FOR(uint32_t, __iter_, 0, <, 38, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3030CFAR_gather_3026, pop_float(&SplitJoin0_SquareAndScale_Fiss_3069_3071_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3026
	 {
	CFAR_gather_3026_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 29, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3030CFAR_gather_3026), &(CFAR_gather_3026AnonFilter_a0_3027));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3027
	FOR(uint32_t, __iter_init_, 0, <, 29, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3026AnonFilter_a0_3027));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3023();
		WEIGHTED_ROUND_ROBIN_Splitter_3029();
			SquareAndScale_3031();
			SquareAndScale_3032();
			SquareAndScale_3033();
			SquareAndScale_3034();
			SquareAndScale_3035();
			SquareAndScale_3036();
			SquareAndScale_3037();
			SquareAndScale_3038();
			SquareAndScale_3039();
			SquareAndScale_3040();
			SquareAndScale_3041();
			SquareAndScale_3042();
			SquareAndScale_3043();
			SquareAndScale_3044();
			SquareAndScale_3045();
			SquareAndScale_3046();
			SquareAndScale_3047();
			SquareAndScale_3048();
			SquareAndScale_3049();
			SquareAndScale_3050();
			SquareAndScale_3051();
			SquareAndScale_3052();
			SquareAndScale_3053();
			SquareAndScale_3054();
			SquareAndScale_3055();
			SquareAndScale_3056();
			SquareAndScale_3057();
			SquareAndScale_3058();
			SquareAndScale_3059();
			SquareAndScale_3060();
			SquareAndScale_3061();
			SquareAndScale_3062();
			SquareAndScale_3063();
			SquareAndScale_3064();
			SquareAndScale_3065();
			SquareAndScale_3066();
			SquareAndScale_3067();
			SquareAndScale_3068();
		WEIGHTED_ROUND_ROBIN_Joiner_3030();
		CFAR_gather_3026();
		AnonFilter_a0_3027();
	ENDFOR
	return EXIT_SUCCESS;
}
