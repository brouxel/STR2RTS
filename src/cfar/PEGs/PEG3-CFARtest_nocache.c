#include "PEG3-CFARtest_nocache.h"

buffer_float_t SplitJoin0_SquareAndScale_Fiss_4924_4926_join[3];
buffer_complex_t ComplexSource_4913WEIGHTED_ROUND_ROBIN_Splitter_4919;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4924_4926_split[3];
buffer_float_t CFAR_gather_4916AnonFilter_a0_4917;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4920CFAR_gather_4916;


ComplexSource_4913_t ComplexSource_4913_s;
CFAR_gather_4916_t CFAR_gather_4916_s;

void ComplexSource_4913(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4913_s.theta = (ComplexSource_4913_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4913_s.theta)) * (((float) cos(ComplexSource_4913_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4913_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4913_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4913_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4913_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4913_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4913_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_4913WEIGHTED_ROUND_ROBIN_Splitter_4919, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_4921(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4922(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4923(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4919() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[__iter_], pop_complex(&ComplexSource_4913WEIGHTED_ROUND_ROBIN_Splitter_4919));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4920() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920CFAR_gather_4916, pop_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4916(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4916_s.pos) - 5) >= 0)), __DEFLOOPBOUND__374__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4916_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4916_s.pos) < 64)), __DEFLOOPBOUND__375__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920CFAR_gather_4916, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4916AnonFilter_a0_4917, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4916_s.poke[(i - 1)] = CFAR_gather_4916_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4916_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920CFAR_gather_4916) ; 
		CFAR_gather_4916_s.pos++ ; 
		if(CFAR_gather_4916_s.pos == 64) {
			CFAR_gather_4916_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4917(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4916AnonFilter_a0_4917));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_4913WEIGHTED_ROUND_ROBIN_Splitter_4919);
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4916AnonFilter_a0_4917);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920CFAR_gather_4916);
// --- init: ComplexSource_4913
	 {
	ComplexSource_4913_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4913_s.theta = (ComplexSource_4913_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4913_s.theta)) * (((float) cos(ComplexSource_4913_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4913_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4913_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4913_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4913_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4913_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4913_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4913WEIGHTED_ROUND_ROBIN_Splitter_4919, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4919
	FOR(uint32_t, __iter_init_, 0, <, 21, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[__iter_], pop_complex(&ComplexSource_4913WEIGHTED_ROUND_ROBIN_Splitter_4919));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4921
	FOR(uint32_t, __iter_init_, 0, <, 21, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4922
	FOR(uint32_t, __iter_init_, 0, <, 21, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4923
	FOR(uint32_t, __iter_init_, 0, <, 21, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4924_4926_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4920
	FOR(uint32_t, __iter_init_, 0, <, 21, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920CFAR_gather_4916, pop_float(&SplitJoin0_SquareAndScale_Fiss_4924_4926_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4916
	 {
	CFAR_gather_4916_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4916_s.pos) - 5) >= 0)), __DEFLOOPBOUND__376__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4916_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4916_s.pos) < 64)), __DEFLOOPBOUND__377__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920CFAR_gather_4916, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4916AnonFilter_a0_4917, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4916_s.poke[(i - 1)] = CFAR_gather_4916_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4916_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920CFAR_gather_4916) ; 
		CFAR_gather_4916_s.pos++ ; 
		if(CFAR_gather_4916_s.pos == 64) {
			CFAR_gather_4916_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4917
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4916AnonFilter_a0_4917));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4913();
		WEIGHTED_ROUND_ROBIN_Splitter_4919();
			SquareAndScale_4921();
			SquareAndScale_4922();
			SquareAndScale_4923();
		WEIGHTED_ROUND_ROBIN_Joiner_4920();
		CFAR_gather_4916();
		AnonFilter_a0_4917();
	ENDFOR
	return EXIT_SUCCESS;
}
