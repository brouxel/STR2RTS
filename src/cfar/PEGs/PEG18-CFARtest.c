#include "PEG18-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4410CFAR_gather_4406;
buffer_float_t CFAR_gather_4406AnonFilter_a0_4407;
buffer_complex_t ComplexSource_4403WEIGHTED_ROUND_ROBIN_Splitter_4409;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4429_4431_join[18];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4429_4431_split[18];


ComplexSource_4403_t ComplexSource_4403_s;
CFAR_gather_4406_t CFAR_gather_4406_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4403_s.theta = (ComplexSource_4403_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4403_s.theta)) * (((float) cos(ComplexSource_4403_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4403_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4403_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4403_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4403_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4403_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4403_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4403() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		ComplexSource(&(ComplexSource_4403WEIGHTED_ROUND_ROBIN_Splitter_4409));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4411() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[0]));
	ENDFOR
}

void SquareAndScale_4412() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[1]));
	ENDFOR
}

void SquareAndScale_4413() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[2]));
	ENDFOR
}

void SquareAndScale_4414() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[3]));
	ENDFOR
}

void SquareAndScale_4415() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[4]));
	ENDFOR
}

void SquareAndScale_4416() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[5]));
	ENDFOR
}

void SquareAndScale_4417() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[6]));
	ENDFOR
}

void SquareAndScale_4418() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[7]));
	ENDFOR
}

void SquareAndScale_4419() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[8]));
	ENDFOR
}

void SquareAndScale_4420() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[9]));
	ENDFOR
}

void SquareAndScale_4421() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[10]));
	ENDFOR
}

void SquareAndScale_4422() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[11]));
	ENDFOR
}

void SquareAndScale_4423() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[12]));
	ENDFOR
}

void SquareAndScale_4424() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[13]));
	ENDFOR
}

void SquareAndScale_4425() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[14]));
	ENDFOR
}

void SquareAndScale_4426() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[15]));
	ENDFOR
}

void SquareAndScale_4427() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[16]));
	ENDFOR
}

void SquareAndScale_4428() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[17]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4409() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4429_4431_split[__iter_], pop_complex(&ComplexSource_4403WEIGHTED_ROUND_ROBIN_Splitter_4409));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4410() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4410CFAR_gather_4406, pop_float(&SplitJoin0_SquareAndScale_Fiss_4429_4431_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4406_s.pos) - 5) >= 0)), __DEFLOOPBOUND__284__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4406_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4406_s.pos) < 64)), __DEFLOOPBOUND__285__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4406_s.poke[(i - 1)] = CFAR_gather_4406_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4406_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4406_s.pos++ ; 
		if(CFAR_gather_4406_s.pos == 64) {
			CFAR_gather_4406_s.pos = 0 ; 
		}
	}


void CFAR_gather_4406() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4410CFAR_gather_4406), &(CFAR_gather_4406AnonFilter_a0_4407));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4407() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4406AnonFilter_a0_4407));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4410CFAR_gather_4406);
	init_buffer_float(&CFAR_gather_4406AnonFilter_a0_4407);
	init_buffer_complex(&ComplexSource_4403WEIGHTED_ROUND_ROBIN_Splitter_4409);
	FOR(int, __iter_init_0_, 0, <, 18, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4429_4431_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 18, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4429_4431_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4403
	 {
	ComplexSource_4403_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4403WEIGHTED_ROUND_ROBIN_Splitter_4409));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4409
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4429_4431_split[__iter_], pop_complex(&ComplexSource_4403WEIGHTED_ROUND_ROBIN_Splitter_4409));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4411
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4412
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4413
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4414
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4415
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4416
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4417
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4418
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4419
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4420
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4421
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4422
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4423
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4424
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4425
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[14]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4426
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[15]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[15]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4427
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[16]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[16]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4428
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4429_4431_split[17]), &(SplitJoin0_SquareAndScale_Fiss_4429_4431_join[17]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4410
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4410CFAR_gather_4406, pop_float(&SplitJoin0_SquareAndScale_Fiss_4429_4431_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4406
	 {
	CFAR_gather_4406_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 45, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4410CFAR_gather_4406), &(CFAR_gather_4406AnonFilter_a0_4407));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4407
	FOR(uint32_t, __iter_init_, 0, <, 45, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4406AnonFilter_a0_4407));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4403();
		WEIGHTED_ROUND_ROBIN_Splitter_4409();
			SquareAndScale_4411();
			SquareAndScale_4412();
			SquareAndScale_4413();
			SquareAndScale_4414();
			SquareAndScale_4415();
			SquareAndScale_4416();
			SquareAndScale_4417();
			SquareAndScale_4418();
			SquareAndScale_4419();
			SquareAndScale_4420();
			SquareAndScale_4421();
			SquareAndScale_4422();
			SquareAndScale_4423();
			SquareAndScale_4424();
			SquareAndScale_4425();
			SquareAndScale_4426();
			SquareAndScale_4427();
			SquareAndScale_4428();
		WEIGHTED_ROUND_ROBIN_Joiner_4410();
		CFAR_gather_4406();
		AnonFilter_a0_4407();
	ENDFOR
	return EXIT_SUCCESS;
}
