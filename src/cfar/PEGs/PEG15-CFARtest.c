#include "PEG15-CFARtest.h"

buffer_complex_t ComplexSource_4541WEIGHTED_ROUND_ROBIN_Splitter_4547;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4564_4566_join[15];
buffer_float_t CFAR_gather_4544AnonFilter_a0_4545;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4548CFAR_gather_4544;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4564_4566_split[15];


ComplexSource_4541_t ComplexSource_4541_s;
CFAR_gather_4544_t CFAR_gather_4544_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4541_s.theta = (ComplexSource_4541_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4541_s.theta)) * (((float) cos(ComplexSource_4541_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4541_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4541_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4541_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4541_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4541_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4541_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4541() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		ComplexSource(&(ComplexSource_4541WEIGHTED_ROUND_ROBIN_Splitter_4547));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4549() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[0]));
	ENDFOR
}

void SquareAndScale_4550() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[1]));
	ENDFOR
}

void SquareAndScale_4551() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[2]));
	ENDFOR
}

void SquareAndScale_4552() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[3]));
	ENDFOR
}

void SquareAndScale_4553() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[4]));
	ENDFOR
}

void SquareAndScale_4554() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[5]));
	ENDFOR
}

void SquareAndScale_4555() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[6]));
	ENDFOR
}

void SquareAndScale_4556() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[7]));
	ENDFOR
}

void SquareAndScale_4557() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[8]));
	ENDFOR
}

void SquareAndScale_4558() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[9]));
	ENDFOR
}

void SquareAndScale_4559() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[10]));
	ENDFOR
}

void SquareAndScale_4560() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[11]));
	ENDFOR
}

void SquareAndScale_4561() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[12]));
	ENDFOR
}

void SquareAndScale_4562() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[13]));
	ENDFOR
}

void SquareAndScale_4563() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[14]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4547() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4564_4566_split[__iter_], pop_complex(&ComplexSource_4541WEIGHTED_ROUND_ROBIN_Splitter_4547));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4548() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4548CFAR_gather_4544, pop_float(&SplitJoin0_SquareAndScale_Fiss_4564_4566_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4544_s.pos) - 5) >= 0)), __DEFLOOPBOUND__302__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4544_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4544_s.pos) < 64)), __DEFLOOPBOUND__303__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4544_s.poke[(i - 1)] = CFAR_gather_4544_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4544_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4544_s.pos++ ; 
		if(CFAR_gather_4544_s.pos == 64) {
			CFAR_gather_4544_s.pos = 0 ; 
		}
	}


void CFAR_gather_4544() {
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4548CFAR_gather_4544), &(CFAR_gather_4544AnonFilter_a0_4545));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4545() {
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4544AnonFilter_a0_4545));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_4541WEIGHTED_ROUND_ROBIN_Splitter_4547);
	FOR(int, __iter_init_0_, 0, <, 15, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4564_4566_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4544AnonFilter_a0_4545);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4548CFAR_gather_4544);
	FOR(int, __iter_init_1_, 0, <, 15, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4564_4566_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4541
	 {
	ComplexSource_4541_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4541WEIGHTED_ROUND_ROBIN_Splitter_4547));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4547
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4564_4566_split[__iter_], pop_complex(&ComplexSource_4541WEIGHTED_ROUND_ROBIN_Splitter_4547));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4549
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4550
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4551
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4552
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4553
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4554
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4555
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4556
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4557
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[8]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4558
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[9]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[9]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4559
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[10]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[10]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4560
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[11]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[11]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4561
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[12]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[12]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4562
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[13]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[13]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4563
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4564_4566_split[14]), &(SplitJoin0_SquareAndScale_Fiss_4564_4566_join[14]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4548
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4548CFAR_gather_4544, pop_float(&SplitJoin0_SquareAndScale_Fiss_4564_4566_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4544
	 {
	CFAR_gather_4544_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4548CFAR_gather_4544), &(CFAR_gather_4544AnonFilter_a0_4545));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4545
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4544AnonFilter_a0_4545));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4541();
		WEIGHTED_ROUND_ROBIN_Splitter_4547();
			SquareAndScale_4549();
			SquareAndScale_4550();
			SquareAndScale_4551();
			SquareAndScale_4552();
			SquareAndScale_4553();
			SquareAndScale_4554();
			SquareAndScale_4555();
			SquareAndScale_4556();
			SquareAndScale_4557();
			SquareAndScale_4558();
			SquareAndScale_4559();
			SquareAndScale_4560();
			SquareAndScale_4561();
			SquareAndScale_4562();
			SquareAndScale_4563();
		WEIGHTED_ROUND_ROBIN_Joiner_4548();
		CFAR_gather_4544();
		AnonFilter_a0_4545();
	ENDFOR
	return EXIT_SUCCESS;
}
