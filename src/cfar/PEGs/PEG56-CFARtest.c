#include "PEG56-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1104CFAR_gather_1100;
buffer_float_t CFAR_gather_1100AnonFilter_a0_1101;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1161_1163_split[56];
buffer_complex_t ComplexSource_1097WEIGHTED_ROUND_ROBIN_Splitter_1103;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_1161_1163_join[56];


ComplexSource_1097_t ComplexSource_1097_s;
CFAR_gather_1100_t CFAR_gather_1100_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_1097_s.theta = (ComplexSource_1097_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_1097_s.theta)) * (((float) cos(ComplexSource_1097_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1097_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1097_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_1097_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1097_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1097_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1097_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_1097() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		ComplexSource(&(ComplexSource_1097WEIGHTED_ROUND_ROBIN_Splitter_1103));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_1105() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[0]));
	ENDFOR
}

void SquareAndScale_1106() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[1]));
	ENDFOR
}

void SquareAndScale_1107() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[2]));
	ENDFOR
}

void SquareAndScale_1108() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[3]));
	ENDFOR
}

void SquareAndScale_1109() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[4]));
	ENDFOR
}

void SquareAndScale_1110() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[5]));
	ENDFOR
}

void SquareAndScale_1111() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[6]));
	ENDFOR
}

void SquareAndScale_1112() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[7]));
	ENDFOR
}

void SquareAndScale_1113() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[8]));
	ENDFOR
}

void SquareAndScale_1114() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[9]));
	ENDFOR
}

void SquareAndScale_1115() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[10]));
	ENDFOR
}

void SquareAndScale_1116() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[11]));
	ENDFOR
}

void SquareAndScale_1117() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[12]));
	ENDFOR
}

void SquareAndScale_1118() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[13]));
	ENDFOR
}

void SquareAndScale_1119() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[14]));
	ENDFOR
}

void SquareAndScale_1120() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[15]));
	ENDFOR
}

void SquareAndScale_1121() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[16]));
	ENDFOR
}

void SquareAndScale_1122() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[17]));
	ENDFOR
}

void SquareAndScale_1123() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[18]));
	ENDFOR
}

void SquareAndScale_1124() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[19]));
	ENDFOR
}

void SquareAndScale_1125() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[20]));
	ENDFOR
}

void SquareAndScale_1126() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[21]));
	ENDFOR
}

void SquareAndScale_1127() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[22]));
	ENDFOR
}

void SquareAndScale_1128() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[23]));
	ENDFOR
}

void SquareAndScale_1129() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[24]));
	ENDFOR
}

void SquareAndScale_1130() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[25]));
	ENDFOR
}

void SquareAndScale_1131() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[26]));
	ENDFOR
}

void SquareAndScale_1132() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[27]));
	ENDFOR
}

void SquareAndScale_1133() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[28]));
	ENDFOR
}

void SquareAndScale_1134() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[29]));
	ENDFOR
}

void SquareAndScale_1135() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[30]));
	ENDFOR
}

void SquareAndScale_1136() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[31]));
	ENDFOR
}

void SquareAndScale_1137() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[32]));
	ENDFOR
}

void SquareAndScale_1138() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[33]));
	ENDFOR
}

void SquareAndScale_1139() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[34]));
	ENDFOR
}

void SquareAndScale_1140() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[35]));
	ENDFOR
}

void SquareAndScale_1141() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[36]));
	ENDFOR
}

void SquareAndScale_1142() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[37]));
	ENDFOR
}

void SquareAndScale_1143() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[38]));
	ENDFOR
}

void SquareAndScale_1144() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[39]));
	ENDFOR
}

void SquareAndScale_1145() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[40]));
	ENDFOR
}

void SquareAndScale_1146() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[41]));
	ENDFOR
}

void SquareAndScale_1147() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[42]));
	ENDFOR
}

void SquareAndScale_1148() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[43]));
	ENDFOR
}

void SquareAndScale_1149() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[44]));
	ENDFOR
}

void SquareAndScale_1150() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[45]));
	ENDFOR
}

void SquareAndScale_1151() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[46]));
	ENDFOR
}

void SquareAndScale_1152() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[47]));
	ENDFOR
}

void SquareAndScale_1153() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[48]));
	ENDFOR
}

void SquareAndScale_1154() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[49]));
	ENDFOR
}

void SquareAndScale_1155() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[50]));
	ENDFOR
}

void SquareAndScale_1156() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[51]));
	ENDFOR
}

void SquareAndScale_1157() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[52]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[52]));
	ENDFOR
}

void SquareAndScale_1158() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[53]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[53]));
	ENDFOR
}

void SquareAndScale_1159() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[54]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[54]));
	ENDFOR
}

void SquareAndScale_1160() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[55]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[55]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1103() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 56, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1161_1163_split[__iter_], pop_complex(&ComplexSource_1097WEIGHTED_ROUND_ROBIN_Splitter_1103));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1104() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 56, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1104CFAR_gather_1100, pop_float(&SplitJoin0_SquareAndScale_Fiss_1161_1163_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1100_s.pos) - 5) >= 0)), __DEFLOOPBOUND__56__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1100_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1100_s.pos) < 64)), __DEFLOOPBOUND__57__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1100_s.poke[(i - 1)] = CFAR_gather_1100_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1100_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_1100_s.pos++ ; 
		if(CFAR_gather_1100_s.pos == 64) {
			CFAR_gather_1100_s.pos = 0 ; 
		}
	}


void CFAR_gather_1100() {
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1104CFAR_gather_1100), &(CFAR_gather_1100AnonFilter_a0_1101));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_1101() {
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_1100AnonFilter_a0_1101));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1104CFAR_gather_1100);
	init_buffer_float(&CFAR_gather_1100AnonFilter_a0_1101);
	FOR(int, __iter_init_0_, 0, <, 56, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1161_1163_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_1097WEIGHTED_ROUND_ROBIN_Splitter_1103);
	FOR(int, __iter_init_1_, 0, <, 56, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1161_1163_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_1097
	 {
	ComplexSource_1097_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_1097WEIGHTED_ROUND_ROBIN_Splitter_1103));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1103
	
	FOR(uint32_t, __iter_, 0, <, 56, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1161_1163_split[__iter_], pop_complex(&ComplexSource_1097WEIGHTED_ROUND_ROBIN_Splitter_1103));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_1105
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[0]));
//--------------------------------
// --- init: SquareAndScale_1106
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[1]));
//--------------------------------
// --- init: SquareAndScale_1107
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[2]));
//--------------------------------
// --- init: SquareAndScale_1108
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[3]));
//--------------------------------
// --- init: SquareAndScale_1109
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[4]));
//--------------------------------
// --- init: SquareAndScale_1110
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[5]));
//--------------------------------
// --- init: SquareAndScale_1111
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[6]));
//--------------------------------
// --- init: SquareAndScale_1112
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[7]));
//--------------------------------
// --- init: SquareAndScale_1113
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[8]));
//--------------------------------
// --- init: SquareAndScale_1114
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[9]));
//--------------------------------
// --- init: SquareAndScale_1115
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[10]));
//--------------------------------
// --- init: SquareAndScale_1116
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[11]));
//--------------------------------
// --- init: SquareAndScale_1117
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[12]));
//--------------------------------
// --- init: SquareAndScale_1118
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[13]));
//--------------------------------
// --- init: SquareAndScale_1119
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[14]));
//--------------------------------
// --- init: SquareAndScale_1120
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[15]));
//--------------------------------
// --- init: SquareAndScale_1121
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[16]));
//--------------------------------
// --- init: SquareAndScale_1122
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[17]));
//--------------------------------
// --- init: SquareAndScale_1123
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[18]));
//--------------------------------
// --- init: SquareAndScale_1124
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[19]));
//--------------------------------
// --- init: SquareAndScale_1125
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[20]));
//--------------------------------
// --- init: SquareAndScale_1126
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[21]));
//--------------------------------
// --- init: SquareAndScale_1127
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[22]));
//--------------------------------
// --- init: SquareAndScale_1128
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[23]));
//--------------------------------
// --- init: SquareAndScale_1129
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[24]));
//--------------------------------
// --- init: SquareAndScale_1130
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[25]));
//--------------------------------
// --- init: SquareAndScale_1131
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[26]));
//--------------------------------
// --- init: SquareAndScale_1132
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[27]));
//--------------------------------
// --- init: SquareAndScale_1133
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[28]));
//--------------------------------
// --- init: SquareAndScale_1134
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[29]));
//--------------------------------
// --- init: SquareAndScale_1135
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[30]));
//--------------------------------
// --- init: SquareAndScale_1136
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[31]));
//--------------------------------
// --- init: SquareAndScale_1137
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[32]));
//--------------------------------
// --- init: SquareAndScale_1138
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[33]));
//--------------------------------
// --- init: SquareAndScale_1139
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[34]));
//--------------------------------
// --- init: SquareAndScale_1140
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[35]));
//--------------------------------
// --- init: SquareAndScale_1141
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[36]));
//--------------------------------
// --- init: SquareAndScale_1142
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[37]));
//--------------------------------
// --- init: SquareAndScale_1143
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[38]));
//--------------------------------
// --- init: SquareAndScale_1144
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[39]));
//--------------------------------
// --- init: SquareAndScale_1145
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[40]));
//--------------------------------
// --- init: SquareAndScale_1146
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[41]));
//--------------------------------
// --- init: SquareAndScale_1147
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[42]));
//--------------------------------
// --- init: SquareAndScale_1148
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[43]));
//--------------------------------
// --- init: SquareAndScale_1149
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[44]));
//--------------------------------
// --- init: SquareAndScale_1150
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[45]));
//--------------------------------
// --- init: SquareAndScale_1151
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[46]));
//--------------------------------
// --- init: SquareAndScale_1152
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[47]));
//--------------------------------
// --- init: SquareAndScale_1153
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[48]));
//--------------------------------
// --- init: SquareAndScale_1154
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[49]));
//--------------------------------
// --- init: SquareAndScale_1155
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[50]));
//--------------------------------
// --- init: SquareAndScale_1156
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[51]));
//--------------------------------
// --- init: SquareAndScale_1157
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[52]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[52]));
//--------------------------------
// --- init: SquareAndScale_1158
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[53]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[53]));
//--------------------------------
// --- init: SquareAndScale_1159
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[54]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[54]));
//--------------------------------
// --- init: SquareAndScale_1160
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1161_1163_split[55]), &(SplitJoin0_SquareAndScale_Fiss_1161_1163_join[55]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1104
	
	FOR(uint32_t, __iter_, 0, <, 56, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1104CFAR_gather_1100, pop_float(&SplitJoin0_SquareAndScale_Fiss_1161_1163_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_1100
	 {
	CFAR_gather_1100_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1104CFAR_gather_1100), &(CFAR_gather_1100AnonFilter_a0_1101));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_1101
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_1100AnonFilter_a0_1101));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_1097();
		WEIGHTED_ROUND_ROBIN_Splitter_1103();
			SquareAndScale_1105();
			SquareAndScale_1106();
			SquareAndScale_1107();
			SquareAndScale_1108();
			SquareAndScale_1109();
			SquareAndScale_1110();
			SquareAndScale_1111();
			SquareAndScale_1112();
			SquareAndScale_1113();
			SquareAndScale_1114();
			SquareAndScale_1115();
			SquareAndScale_1116();
			SquareAndScale_1117();
			SquareAndScale_1118();
			SquareAndScale_1119();
			SquareAndScale_1120();
			SquareAndScale_1121();
			SquareAndScale_1122();
			SquareAndScale_1123();
			SquareAndScale_1124();
			SquareAndScale_1125();
			SquareAndScale_1126();
			SquareAndScale_1127();
			SquareAndScale_1128();
			SquareAndScale_1129();
			SquareAndScale_1130();
			SquareAndScale_1131();
			SquareAndScale_1132();
			SquareAndScale_1133();
			SquareAndScale_1134();
			SquareAndScale_1135();
			SquareAndScale_1136();
			SquareAndScale_1137();
			SquareAndScale_1138();
			SquareAndScale_1139();
			SquareAndScale_1140();
			SquareAndScale_1141();
			SquareAndScale_1142();
			SquareAndScale_1143();
			SquareAndScale_1144();
			SquareAndScale_1145();
			SquareAndScale_1146();
			SquareAndScale_1147();
			SquareAndScale_1148();
			SquareAndScale_1149();
			SquareAndScale_1150();
			SquareAndScale_1151();
			SquareAndScale_1152();
			SquareAndScale_1153();
			SquareAndScale_1154();
			SquareAndScale_1155();
			SquareAndScale_1156();
			SquareAndScale_1157();
			SquareAndScale_1158();
			SquareAndScale_1159();
			SquareAndScale_1160();
		WEIGHTED_ROUND_ROBIN_Joiner_1104();
		CFAR_gather_1100();
		AnonFilter_a0_1101();
	ENDFOR
	return EXIT_SUCCESS;
}
