#include "PEG16-CFARtest_nocache.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4504CFAR_gather_4500;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4521_4523_join[16];
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4521_4523_split[16];
buffer_float_t CFAR_gather_4500AnonFilter_a0_4501;
buffer_complex_t ComplexSource_4497WEIGHTED_ROUND_ROBIN_Splitter_4503;


ComplexSource_4497_t ComplexSource_4497_s;
CFAR_gather_4500_t CFAR_gather_4500_s;

void ComplexSource_4497() {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4497_s.theta = (ComplexSource_4497_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4497_s.theta)) * (((float) cos(ComplexSource_4497_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4497_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4497_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4497_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4497_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4497_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4497_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4497WEIGHTED_ROUND_ROBIN_Splitter_4503, c) ; 
	}
	ENDFOR
}


void SquareAndScale_4505(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4506(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4507(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4508(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4509(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4510(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4511(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4512(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4513(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4514(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4515(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4516(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4517(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4518(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4519(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4520(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4503() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[__iter_], pop_complex(&ComplexSource_4497WEIGHTED_ROUND_ROBIN_Splitter_4503));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4504() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4504CFAR_gather_4500, pop_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4500(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4500_s.pos) - 5) >= 0)), __DEFLOOPBOUND__296__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4500_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4500_s.pos) < 64)), __DEFLOOPBOUND__297__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4504CFAR_gather_4500, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4500AnonFilter_a0_4501, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4500_s.poke[(i - 1)] = CFAR_gather_4500_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4500_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4504CFAR_gather_4500) ; 
		CFAR_gather_4500_s.pos++ ; 
		if(CFAR_gather_4500_s.pos == 64) {
			CFAR_gather_4500_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4501(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4500AnonFilter_a0_4501));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4504CFAR_gather_4500);
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 16, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4500AnonFilter_a0_4501);
	init_buffer_complex(&ComplexSource_4497WEIGHTED_ROUND_ROBIN_Splitter_4503);
// --- init: ComplexSource_4497
	 {
	ComplexSource_4497_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4497_s.theta = (ComplexSource_4497_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4497_s.theta)) * (((float) cos(ComplexSource_4497_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4497_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4497_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4497_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4497_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4497_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4497_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4497WEIGHTED_ROUND_ROBIN_Splitter_4503, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4503
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[__iter_], pop_complex(&ComplexSource_4497WEIGHTED_ROUND_ROBIN_Splitter_4503));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4505
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4506
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4507
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4508
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4509
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4510
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4511
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4512
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4513
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4514
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4515
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4516
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4517
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4518
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4519
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4520
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4521_4523_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4504
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4504CFAR_gather_4500, pop_float(&SplitJoin0_SquareAndScale_Fiss_4521_4523_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4500
	 {
	CFAR_gather_4500_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4500_s.pos) - 5) >= 0)), __DEFLOOPBOUND__298__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4500_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4500_s.pos) < 64)), __DEFLOOPBOUND__299__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4504CFAR_gather_4500, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4500AnonFilter_a0_4501, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4500_s.poke[(i - 1)] = CFAR_gather_4500_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4500_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4504CFAR_gather_4500) ; 
		CFAR_gather_4500_s.pos++ ; 
		if(CFAR_gather_4500_s.pos == 64) {
			CFAR_gather_4500_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4501
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4500AnonFilter_a0_4501));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4497();
		WEIGHTED_ROUND_ROBIN_Splitter_4503();
			SquareAndScale_4505();
			SquareAndScale_4506();
			SquareAndScale_4507();
			SquareAndScale_4508();
			SquareAndScale_4509();
			SquareAndScale_4510();
			SquareAndScale_4511();
			SquareAndScale_4512();
			SquareAndScale_4513();
			SquareAndScale_4514();
			SquareAndScale_4515();
			SquareAndScale_4516();
			SquareAndScale_4517();
			SquareAndScale_4518();
			SquareAndScale_4519();
			SquareAndScale_4520();
		WEIGHTED_ROUND_ROBIN_Joiner_4504();
		CFAR_gather_4500();
		AnonFilter_a0_4501();
	ENDFOR
	return EXIT_SUCCESS;
}
