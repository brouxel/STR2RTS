#include "PEG53-CFARtest.h"

buffer_complex_t ComplexSource_1463WEIGHTED_ROUND_ROBIN_Splitter_1469;
buffer_float_t CFAR_gather_1466AnonFilter_a0_1467;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_1524_1526_join[53];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1470CFAR_gather_1466;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_1524_1526_split[53];


ComplexSource_1463_t ComplexSource_1463_s;
CFAR_gather_1466_t CFAR_gather_1466_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_1463_s.theta = (ComplexSource_1463_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_1463_s.theta)) * (((float) cos(ComplexSource_1463_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1463_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_1463_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_1463_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_1463_s.theta))))) + (0.0 * (((float) cos(ComplexSource_1463_s.theta)) + ((0.0 * ((float) sin(ComplexSource_1463_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_1463() {
	FOR(uint32_t, __iter_steady_, 0, <, 53, __iter_steady_++)
		ComplexSource(&(ComplexSource_1463WEIGHTED_ROUND_ROBIN_Splitter_1469));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_1471() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[0]));
	ENDFOR
}

void SquareAndScale_1472() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[1]));
	ENDFOR
}

void SquareAndScale_1473() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[2]));
	ENDFOR
}

void SquareAndScale_1474() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[3]));
	ENDFOR
}

void SquareAndScale_1475() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[4]));
	ENDFOR
}

void SquareAndScale_1476() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[5]));
	ENDFOR
}

void SquareAndScale_1477() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[6]));
	ENDFOR
}

void SquareAndScale_1478() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[7]));
	ENDFOR
}

void SquareAndScale_1479() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[8]));
	ENDFOR
}

void SquareAndScale_1480() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[9]));
	ENDFOR
}

void SquareAndScale_1481() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[10]));
	ENDFOR
}

void SquareAndScale_1482() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[11]));
	ENDFOR
}

void SquareAndScale_1483() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[12]));
	ENDFOR
}

void SquareAndScale_1484() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[13]));
	ENDFOR
}

void SquareAndScale_1485() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[14]));
	ENDFOR
}

void SquareAndScale_1486() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[15]));
	ENDFOR
}

void SquareAndScale_1487() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[16]));
	ENDFOR
}

void SquareAndScale_1488() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[17]));
	ENDFOR
}

void SquareAndScale_1489() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[18]));
	ENDFOR
}

void SquareAndScale_1490() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[19]));
	ENDFOR
}

void SquareAndScale_1491() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[20]));
	ENDFOR
}

void SquareAndScale_1492() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[21]));
	ENDFOR
}

void SquareAndScale_1493() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[22]));
	ENDFOR
}

void SquareAndScale_1494() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[23]));
	ENDFOR
}

void SquareAndScale_1495() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[24]));
	ENDFOR
}

void SquareAndScale_1496() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[25]));
	ENDFOR
}

void SquareAndScale_1497() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[26]));
	ENDFOR
}

void SquareAndScale_1498() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[27]));
	ENDFOR
}

void SquareAndScale_1499() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[28]));
	ENDFOR
}

void SquareAndScale_1500() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[29]));
	ENDFOR
}

void SquareAndScale_1501() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[30]));
	ENDFOR
}

void SquareAndScale_1502() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[31]));
	ENDFOR
}

void SquareAndScale_1503() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[32]));
	ENDFOR
}

void SquareAndScale_1504() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[33]));
	ENDFOR
}

void SquareAndScale_1505() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[34]));
	ENDFOR
}

void SquareAndScale_1506() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[35]));
	ENDFOR
}

void SquareAndScale_1507() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[36]));
	ENDFOR
}

void SquareAndScale_1508() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[37]));
	ENDFOR
}

void SquareAndScale_1509() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[38]));
	ENDFOR
}

void SquareAndScale_1510() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[39]));
	ENDFOR
}

void SquareAndScale_1511() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[40]));
	ENDFOR
}

void SquareAndScale_1512() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[41]));
	ENDFOR
}

void SquareAndScale_1513() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[42]));
	ENDFOR
}

void SquareAndScale_1514() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[43]));
	ENDFOR
}

void SquareAndScale_1515() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[44]));
	ENDFOR
}

void SquareAndScale_1516() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[45]));
	ENDFOR
}

void SquareAndScale_1517() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[46]));
	ENDFOR
}

void SquareAndScale_1518() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[47]));
	ENDFOR
}

void SquareAndScale_1519() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[48]));
	ENDFOR
}

void SquareAndScale_1520() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[49]));
	ENDFOR
}

void SquareAndScale_1521() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[50]));
	ENDFOR
}

void SquareAndScale_1522() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[51]));
	ENDFOR
}

void SquareAndScale_1523() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[52]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[52]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1469() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 53, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_1524_1526_split[__iter_], pop_complex(&ComplexSource_1463WEIGHTED_ROUND_ROBIN_Splitter_1469));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1470() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 53, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1470CFAR_gather_1466, pop_float(&SplitJoin0_SquareAndScale_Fiss_1524_1526_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_1466_s.pos) - 5) >= 0)), __DEFLOOPBOUND__74__, i__conflict__1++) {
			sum = (sum + CFAR_gather_1466_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_1466_s.pos) < 64)), __DEFLOOPBOUND__75__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_1466_s.poke[(i - 1)] = CFAR_gather_1466_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_1466_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_1466_s.pos++ ; 
		if(CFAR_gather_1466_s.pos == 64) {
			CFAR_gather_1466_s.pos = 0 ; 
		}
	}


void CFAR_gather_1466() {
	FOR(uint32_t, __iter_steady_, 0, <, 3392, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1470CFAR_gather_1466), &(CFAR_gather_1466AnonFilter_a0_1467));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_1467() {
	FOR(uint32_t, __iter_steady_, 0, <, 3392, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_1466AnonFilter_a0_1467));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_1463WEIGHTED_ROUND_ROBIN_Splitter_1469);
	init_buffer_float(&CFAR_gather_1466AnonFilter_a0_1467);
	FOR(int, __iter_init_0_, 0, <, 53, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_1524_1526_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1470CFAR_gather_1466);
	FOR(int, __iter_init_1_, 0, <, 53, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_1524_1526_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_1463
	 {
	ComplexSource_1463_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_1463WEIGHTED_ROUND_ROBIN_Splitter_1469));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1469
	
	FOR(uint32_t, __iter_, 0, <, 53, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_1524_1526_split[__iter_], pop_complex(&ComplexSource_1463WEIGHTED_ROUND_ROBIN_Splitter_1469));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_1471
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[0]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[0]));
//--------------------------------
// --- init: SquareAndScale_1472
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[1]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[1]));
//--------------------------------
// --- init: SquareAndScale_1473
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[2]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[2]));
//--------------------------------
// --- init: SquareAndScale_1474
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[3]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[3]));
//--------------------------------
// --- init: SquareAndScale_1475
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[4]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[4]));
//--------------------------------
// --- init: SquareAndScale_1476
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[5]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[5]));
//--------------------------------
// --- init: SquareAndScale_1477
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[6]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[6]));
//--------------------------------
// --- init: SquareAndScale_1478
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[7]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[7]));
//--------------------------------
// --- init: SquareAndScale_1479
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[8]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[8]));
//--------------------------------
// --- init: SquareAndScale_1480
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[9]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[9]));
//--------------------------------
// --- init: SquareAndScale_1481
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[10]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[10]));
//--------------------------------
// --- init: SquareAndScale_1482
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[11]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[11]));
//--------------------------------
// --- init: SquareAndScale_1483
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[12]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[12]));
//--------------------------------
// --- init: SquareAndScale_1484
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[13]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[13]));
//--------------------------------
// --- init: SquareAndScale_1485
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[14]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[14]));
//--------------------------------
// --- init: SquareAndScale_1486
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[15]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[15]));
//--------------------------------
// --- init: SquareAndScale_1487
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[16]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[16]));
//--------------------------------
// --- init: SquareAndScale_1488
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[17]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[17]));
//--------------------------------
// --- init: SquareAndScale_1489
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[18]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[18]));
//--------------------------------
// --- init: SquareAndScale_1490
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[19]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[19]));
//--------------------------------
// --- init: SquareAndScale_1491
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[20]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[20]));
//--------------------------------
// --- init: SquareAndScale_1492
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[21]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[21]));
//--------------------------------
// --- init: SquareAndScale_1493
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[22]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[22]));
//--------------------------------
// --- init: SquareAndScale_1494
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[23]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[23]));
//--------------------------------
// --- init: SquareAndScale_1495
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[24]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[24]));
//--------------------------------
// --- init: SquareAndScale_1496
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[25]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[25]));
//--------------------------------
// --- init: SquareAndScale_1497
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[26]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[26]));
//--------------------------------
// --- init: SquareAndScale_1498
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[27]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[27]));
//--------------------------------
// --- init: SquareAndScale_1499
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[28]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[28]));
//--------------------------------
// --- init: SquareAndScale_1500
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[29]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[29]));
//--------------------------------
// --- init: SquareAndScale_1501
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[30]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[30]));
//--------------------------------
// --- init: SquareAndScale_1502
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[31]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[31]));
//--------------------------------
// --- init: SquareAndScale_1503
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[32]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[32]));
//--------------------------------
// --- init: SquareAndScale_1504
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[33]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[33]));
//--------------------------------
// --- init: SquareAndScale_1505
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[34]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[34]));
//--------------------------------
// --- init: SquareAndScale_1506
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[35]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[35]));
//--------------------------------
// --- init: SquareAndScale_1507
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[36]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[36]));
//--------------------------------
// --- init: SquareAndScale_1508
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[37]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[37]));
//--------------------------------
// --- init: SquareAndScale_1509
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[38]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[38]));
//--------------------------------
// --- init: SquareAndScale_1510
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[39]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[39]));
//--------------------------------
// --- init: SquareAndScale_1511
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[40]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[40]));
//--------------------------------
// --- init: SquareAndScale_1512
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[41]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[41]));
//--------------------------------
// --- init: SquareAndScale_1513
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[42]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[42]));
//--------------------------------
// --- init: SquareAndScale_1514
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[43]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[43]));
//--------------------------------
// --- init: SquareAndScale_1515
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[44]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[44]));
//--------------------------------
// --- init: SquareAndScale_1516
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[45]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[45]));
//--------------------------------
// --- init: SquareAndScale_1517
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[46]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[46]));
//--------------------------------
// --- init: SquareAndScale_1518
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[47]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[47]));
//--------------------------------
// --- init: SquareAndScale_1519
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[48]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[48]));
//--------------------------------
// --- init: SquareAndScale_1520
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[49]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[49]));
//--------------------------------
// --- init: SquareAndScale_1521
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[50]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[50]));
//--------------------------------
// --- init: SquareAndScale_1522
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[51]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[51]));
//--------------------------------
// --- init: SquareAndScale_1523
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_1524_1526_split[52]), &(SplitJoin0_SquareAndScale_Fiss_1524_1526_join[52]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1470
	
	FOR(uint32_t, __iter_, 0, <, 53, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1470CFAR_gather_1466, pop_float(&SplitJoin0_SquareAndScale_Fiss_1524_1526_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_1466
	 {
	CFAR_gather_1466_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 44, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_1470CFAR_gather_1466), &(CFAR_gather_1466AnonFilter_a0_1467));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_1467
	FOR(uint32_t, __iter_init_, 0, <, 44, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_1466AnonFilter_a0_1467));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_1463();
		WEIGHTED_ROUND_ROBIN_Splitter_1469();
			SquareAndScale_1471();
			SquareAndScale_1472();
			SquareAndScale_1473();
			SquareAndScale_1474();
			SquareAndScale_1475();
			SquareAndScale_1476();
			SquareAndScale_1477();
			SquareAndScale_1478();
			SquareAndScale_1479();
			SquareAndScale_1480();
			SquareAndScale_1481();
			SquareAndScale_1482();
			SquareAndScale_1483();
			SquareAndScale_1484();
			SquareAndScale_1485();
			SquareAndScale_1486();
			SquareAndScale_1487();
			SquareAndScale_1488();
			SquareAndScale_1489();
			SquareAndScale_1490();
			SquareAndScale_1491();
			SquareAndScale_1492();
			SquareAndScale_1493();
			SquareAndScale_1494();
			SquareAndScale_1495();
			SquareAndScale_1496();
			SquareAndScale_1497();
			SquareAndScale_1498();
			SquareAndScale_1499();
			SquareAndScale_1500();
			SquareAndScale_1501();
			SquareAndScale_1502();
			SquareAndScale_1503();
			SquareAndScale_1504();
			SquareAndScale_1505();
			SquareAndScale_1506();
			SquareAndScale_1507();
			SquareAndScale_1508();
			SquareAndScale_1509();
			SquareAndScale_1510();
			SquareAndScale_1511();
			SquareAndScale_1512();
			SquareAndScale_1513();
			SquareAndScale_1514();
			SquareAndScale_1515();
			SquareAndScale_1516();
			SquareAndScale_1517();
			SquareAndScale_1518();
			SquareAndScale_1519();
			SquareAndScale_1520();
			SquareAndScale_1521();
			SquareAndScale_1522();
			SquareAndScale_1523();
		WEIGHTED_ROUND_ROBIN_Joiner_1470();
		CFAR_gather_1466();
		AnonFilter_a0_1467();
	ENDFOR
	return EXIT_SUCCESS;
}
