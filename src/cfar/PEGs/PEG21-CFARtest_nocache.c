#include "PEG21-CFARtest_nocache.h"

buffer_complex_t ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253;
buffer_float_t CFAR_gather_4250AnonFilter_a0_4251;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4276_4278_split[21];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4276_4278_join[21];


ComplexSource_4247_t ComplexSource_4247_s;
CFAR_gather_4250_t CFAR_gather_4250_s;

void ComplexSource_4247(){
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4247_s.theta = (ComplexSource_4247_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4247_s.theta)) * (((float) cos(ComplexSource_4247_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4247_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4247_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4247_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4247_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4247_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4247_s.theta))) - 0.0)))) ; 
			push_complex(&ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253, c) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void SquareAndScale_4255(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4256(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4257(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4258(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4259(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4260(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4261(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4262(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4263(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4264(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4265(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4266(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4267(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4268(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4269(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4270(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4271(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4272(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4273(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4274(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4275(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4253() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[__iter_], pop_complex(&ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4254() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250, pop_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4250(){
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4250_s.pos) - 5) >= 0)), __DEFLOOPBOUND__266__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4250_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4250_s.pos) < 64)), __DEFLOOPBOUND__267__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4250AnonFilter_a0_4251, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4250_s.poke[(i - 1)] = CFAR_gather_4250_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4250_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250) ; 
		CFAR_gather_4250_s.pos++ ; 
		if(CFAR_gather_4250_s.pos == 64) {
			CFAR_gather_4250_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4251(){
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4250AnonFilter_a0_4251));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253);
	init_buffer_float(&CFAR_gather_4250AnonFilter_a0_4251);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250);
	FOR(int, __iter_init_0_, 0, <, 21, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 21, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4247
	 {
	ComplexSource_4247_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4247_s.theta = (ComplexSource_4247_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4247_s.theta)) * (((float) cos(ComplexSource_4247_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4247_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4247_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4247_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4247_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4247_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4247_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4253
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[__iter_], pop_complex(&ComplexSource_4247WEIGHTED_ROUND_ROBIN_Splitter_4253));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4255
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4256
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4257
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4258
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4259
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4260
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4261
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4262
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4263
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[8]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[8], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4264
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[9]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[9], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4265
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[10]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[10], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4266
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[11]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[11], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4267
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[12]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[12], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4268
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[13]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[13], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4269
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[14]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[14], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4270
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[15]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[15], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4271
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[16]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[16], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4272
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[17]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[17], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4273
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[18]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[18], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4274
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[19]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[19], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4275
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4276_4278_split[20]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[20], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4254
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250, pop_float(&SplitJoin0_SquareAndScale_Fiss_4276_4278_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4250
	 {
	CFAR_gather_4250_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4250_s.pos) - 5) >= 0)), __DEFLOOPBOUND__268__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4250_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4250_s.pos) < 64)), __DEFLOOPBOUND__269__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4250AnonFilter_a0_4251, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4250_s.poke[(i - 1)] = CFAR_gather_4250_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4250_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4254CFAR_gather_4250) ; 
		CFAR_gather_4250_s.pos++ ; 
		if(CFAR_gather_4250_s.pos == 64) {
			CFAR_gather_4250_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4251
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4250AnonFilter_a0_4251));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4247();
		WEIGHTED_ROUND_ROBIN_Splitter_4253();
			SquareAndScale_4255();
			SquareAndScale_4256();
			SquareAndScale_4257();
			SquareAndScale_4258();
			SquareAndScale_4259();
			SquareAndScale_4260();
			SquareAndScale_4261();
			SquareAndScale_4262();
			SquareAndScale_4263();
			SquareAndScale_4264();
			SquareAndScale_4265();
			SquareAndScale_4266();
			SquareAndScale_4267();
			SquareAndScale_4268();
			SquareAndScale_4269();
			SquareAndScale_4270();
			SquareAndScale_4271();
			SquareAndScale_4272();
			SquareAndScale_4273();
			SquareAndScale_4274();
			SquareAndScale_4275();
		WEIGHTED_ROUND_ROBIN_Joiner_4254();
		CFAR_gather_4250();
		AnonFilter_a0_4251();
	ENDFOR
	return EXIT_SUCCESS;
}
