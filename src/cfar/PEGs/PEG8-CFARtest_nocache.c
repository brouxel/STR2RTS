#include "PEG8-CFARtest_nocache.h"

buffer_complex_t ComplexSource_4793WEIGHTED_ROUND_ROBIN_Splitter_4799;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4800CFAR_gather_4796;
buffer_float_t CFAR_gather_4796AnonFilter_a0_4797;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4809_4811_split[8];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4809_4811_join[8];


ComplexSource_4793_t ComplexSource_4793_s;
CFAR_gather_4796_t CFAR_gather_4796_s;

void ComplexSource_4793() {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4793_s.theta = (ComplexSource_4793_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4793_s.theta)) * (((float) cos(ComplexSource_4793_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4793_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4793_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4793_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4793_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4793_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4793_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4793WEIGHTED_ROUND_ROBIN_Splitter_4799, c) ; 
	}
	ENDFOR
}


void SquareAndScale_4801(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4802(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4803(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4804(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4805(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4806(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4807(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void SquareAndScale_4808(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4799() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[__iter_], pop_complex(&ComplexSource_4793WEIGHTED_ROUND_ROBIN_Splitter_4799));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4800() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4800CFAR_gather_4796, pop_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather_4796(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4796_s.pos) - 5) >= 0)), __DEFLOOPBOUND__344__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4796_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4796_s.pos) < 64)), __DEFLOOPBOUND__345__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4800CFAR_gather_4796, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4796AnonFilter_a0_4797, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4796_s.poke[(i - 1)] = CFAR_gather_4796_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4796_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4800CFAR_gather_4796) ; 
		CFAR_gather_4796_s.pos++ ; 
		if(CFAR_gather_4796_s.pos == 64) {
			CFAR_gather_4796_s.pos = 0 ; 
		}
	}
	ENDFOR
}

void AnonFilter_a0_4797(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		printf("%.10f", pop_float(&CFAR_gather_4796AnonFilter_a0_4797));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_4793WEIGHTED_ROUND_ROBIN_Splitter_4799);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4800CFAR_gather_4796);
	init_buffer_float(&CFAR_gather_4796AnonFilter_a0_4797);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4793
	 {
	ComplexSource_4793_s.theta = 0.0 ; 
}
	 {
	FOR(int, i, 0,  < , 64, i++) {
		complex_t c = {
			.real = 0,
			.imag = 0
		};
		ComplexSource_4793_s.theta = (ComplexSource_4793_s.theta + 0.19634955) ; 
		c.real = ((((float) sin(ComplexSource_4793_s.theta)) * (((float) cos(ComplexSource_4793_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4793_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4793_s.theta)))))) ; 
		c.imag = ((((float) sin(ComplexSource_4793_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4793_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4793_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4793_s.theta))) - 0.0)))) ; 
		push_complex(&ComplexSource_4793WEIGHTED_ROUND_ROBIN_Splitter_4799, c) ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4799
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[__iter_], pop_complex(&ComplexSource_4793WEIGHTED_ROUND_ROBIN_Splitter_4799));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4801
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[0]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[0], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4802
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[1]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[1], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4803
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[2]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[2], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4804
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[3]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[3], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4805
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[4]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[4], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4806
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[5]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[5], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4807
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[6]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[6], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4808
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin0_SquareAndScale_Fiss_4809_4811_split[7]));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[7], ((mag * mag) / 10.0)) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4800
	FOR(uint32_t, __iter_init_, 0, <, 8, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4800CFAR_gather_4796, pop_float(&SplitJoin0_SquareAndScale_Fiss_4809_4811_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4796
	 {
	CFAR_gather_4796_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4796_s.pos) - 5) >= 0)), __DEFLOOPBOUND__346__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4796_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4796_s.pos) < 64)), __DEFLOOPBOUND__347__, i__conflict__0++) {
			sum = (sum + peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_4800CFAR_gather_4796, i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&CFAR_gather_4796AnonFilter_a0_4797, sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4796_s.poke[(i - 1)] = CFAR_gather_4796_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4796_s.poke[8] = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4800CFAR_gather_4796) ; 
		CFAR_gather_4796_s.pos++ ; 
		if(CFAR_gather_4796_s.pos == 64) {
			CFAR_gather_4796_s.pos = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4797
	FOR(uint32_t, __iter_init_, 0, <, 55, __iter_init_++) {
		printf("%.10f", pop_float(&CFAR_gather_4796AnonFilter_a0_4797));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4793();
		WEIGHTED_ROUND_ROBIN_Splitter_4799();
			SquareAndScale_4801();
			SquareAndScale_4802();
			SquareAndScale_4803();
			SquareAndScale_4804();
			SquareAndScale_4805();
			SquareAndScale_4806();
			SquareAndScale_4807();
			SquareAndScale_4808();
		WEIGHTED_ROUND_ROBIN_Joiner_4800();
		CFAR_gather_4796();
		AnonFilter_a0_4797();
	ENDFOR
	return EXIT_SUCCESS;
}
