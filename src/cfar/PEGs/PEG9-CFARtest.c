#include "PEG9-CFARtest.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4770CFAR_gather_4766;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_4780_4782_join[9];
buffer_float_t CFAR_gather_4766AnonFilter_a0_4767;
buffer_complex_t ComplexSource_4763WEIGHTED_ROUND_ROBIN_Splitter_4769;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_4780_4782_split[9];


ComplexSource_4763_t ComplexSource_4763_s;
CFAR_gather_4766_t CFAR_gather_4766_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_4763_s.theta = (ComplexSource_4763_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_4763_s.theta)) * (((float) cos(ComplexSource_4763_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4763_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_4763_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_4763_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_4763_s.theta))))) + (0.0 * (((float) cos(ComplexSource_4763_s.theta)) + ((0.0 * ((float) sin(ComplexSource_4763_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_4763() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		ComplexSource(&(ComplexSource_4763WEIGHTED_ROUND_ROBIN_Splitter_4769));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_4771() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[0]));
	ENDFOR
}

void SquareAndScale_4772() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[1]));
	ENDFOR
}

void SquareAndScale_4773() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[2]));
	ENDFOR
}

void SquareAndScale_4774() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[3]));
	ENDFOR
}

void SquareAndScale_4775() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[4]));
	ENDFOR
}

void SquareAndScale_4776() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[5]));
	ENDFOR
}

void SquareAndScale_4777() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[6]));
	ENDFOR
}

void SquareAndScale_4778() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[7]));
	ENDFOR
}

void SquareAndScale_4779() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[8]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4769() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4780_4782_split[__iter_], pop_complex(&ComplexSource_4763WEIGHTED_ROUND_ROBIN_Splitter_4769));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4770() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4770CFAR_gather_4766, pop_float(&SplitJoin0_SquareAndScale_Fiss_4780_4782_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_4766_s.pos) - 5) >= 0)), __DEFLOOPBOUND__338__, i__conflict__1++) {
			sum = (sum + CFAR_gather_4766_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_4766_s.pos) < 64)), __DEFLOOPBOUND__339__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_4766_s.poke[(i - 1)] = CFAR_gather_4766_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_4766_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_4766_s.pos++ ; 
		if(CFAR_gather_4766_s.pos == 64) {
			CFAR_gather_4766_s.pos = 0 ; 
		}
	}


void CFAR_gather_4766() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4770CFAR_gather_4766), &(CFAR_gather_4766AnonFilter_a0_4767));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_4767() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_4766AnonFilter_a0_4767));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4770CFAR_gather_4766);
	FOR(int, __iter_init_0_, 0, <, 9, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_4780_4782_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_4766AnonFilter_a0_4767);
	init_buffer_complex(&ComplexSource_4763WEIGHTED_ROUND_ROBIN_Splitter_4769);
	FOR(int, __iter_init_1_, 0, <, 9, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_4780_4782_split[__iter_init_1_]);
	ENDFOR
// --- init: ComplexSource_4763
	 {
	ComplexSource_4763_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_4763WEIGHTED_ROUND_ROBIN_Splitter_4769));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4769
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_4780_4782_split[__iter_], pop_complex(&ComplexSource_4763WEIGHTED_ROUND_ROBIN_Splitter_4769));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4771
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[0]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[0]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4772
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[1]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[1]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4773
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[2]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[2]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4774
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[3]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[3]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4775
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[4]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[4]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4776
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[5]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[5]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4777
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[6]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[6]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4778
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[7]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[7]));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_4779
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_4780_4782_split[8]), &(SplitJoin0_SquareAndScale_Fiss_4780_4782_join[8]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4770
	FOR(uint32_t, __iter_init_, 0, <, 7, __iter_init_++)
		
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4770CFAR_gather_4766, pop_float(&SplitJoin0_SquareAndScale_Fiss_4780_4782_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_4766
	 {
	CFAR_gather_4766_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_4770CFAR_gather_4766), &(CFAR_gather_4766AnonFilter_a0_4767));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_4767
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_4766AnonFilter_a0_4767));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_4763();
		WEIGHTED_ROUND_ROBIN_Splitter_4769();
			SquareAndScale_4771();
			SquareAndScale_4772();
			SquareAndScale_4773();
			SquareAndScale_4774();
			SquareAndScale_4775();
			SquareAndScale_4776();
			SquareAndScale_4777();
			SquareAndScale_4778();
			SquareAndScale_4779();
		WEIGHTED_ROUND_ROBIN_Joiner_4770();
		CFAR_gather_4766();
		AnonFilter_a0_4767();
	ENDFOR
	return EXIT_SUCCESS;
}
