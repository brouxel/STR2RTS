#include "PEG63-CFARtest.h"

buffer_complex_t ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_244_246_split[63];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_244_246_join[63];
buffer_float_t CFAR_gather_176AnonFilter_a0_177;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176;


ComplexSource_173_t ComplexSource_173_s;
CFAR_gather_176_t CFAR_gather_176_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_173_s.theta = (ComplexSource_173_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_173_s.theta)) * (((float) cos(ComplexSource_173_s.theta)) + ((0.0 * ((float) sin(ComplexSource_173_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_173_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_173_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_173_s.theta))))) + (0.0 * (((float) cos(ComplexSource_173_s.theta)) + ((0.0 * ((float) sin(ComplexSource_173_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_173() {
	FOR(uint32_t, __iter_steady_, 0, <, 63, __iter_steady_++)
		ComplexSource(&(ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_181() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[0]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[0]));
	ENDFOR
}

void SquareAndScale_182() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[1]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[1]));
	ENDFOR
}

void SquareAndScale_183() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[2]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[2]));
	ENDFOR
}

void SquareAndScale_184() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[3]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[3]));
	ENDFOR
}

void SquareAndScale_185() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[4]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[4]));
	ENDFOR
}

void SquareAndScale_186() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[5]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[5]));
	ENDFOR
}

void SquareAndScale_187() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[6]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[6]));
	ENDFOR
}

void SquareAndScale_188() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[7]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[7]));
	ENDFOR
}

void SquareAndScale_189() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[8]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[8]));
	ENDFOR
}

void SquareAndScale_190() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[9]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[9]));
	ENDFOR
}

void SquareAndScale_191() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[10]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[10]));
	ENDFOR
}

void SquareAndScale_192() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[11]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[11]));
	ENDFOR
}

void SquareAndScale_193() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[12]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[12]));
	ENDFOR
}

void SquareAndScale_194() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[13]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[13]));
	ENDFOR
}

void SquareAndScale_195() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[14]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[14]));
	ENDFOR
}

void SquareAndScale_196() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[15]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[15]));
	ENDFOR
}

void SquareAndScale_197() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[16]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[16]));
	ENDFOR
}

void SquareAndScale_198() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[17]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[17]));
	ENDFOR
}

void SquareAndScale_199() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[18]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[18]));
	ENDFOR
}

void SquareAndScale_200() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[19]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[19]));
	ENDFOR
}

void SquareAndScale_201() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[20]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[20]));
	ENDFOR
}

void SquareAndScale_202() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[21]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[21]));
	ENDFOR
}

void SquareAndScale_203() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[22]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[22]));
	ENDFOR
}

void SquareAndScale_204() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[23]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[23]));
	ENDFOR
}

void SquareAndScale_205() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[24]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[24]));
	ENDFOR
}

void SquareAndScale_206() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[25]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[25]));
	ENDFOR
}

void SquareAndScale_207() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[26]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[26]));
	ENDFOR
}

void SquareAndScale_208() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[27]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[27]));
	ENDFOR
}

void SquareAndScale_209() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[28]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[28]));
	ENDFOR
}

void SquareAndScale_210() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[29]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[29]));
	ENDFOR
}

void SquareAndScale_211() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[30]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[30]));
	ENDFOR
}

void SquareAndScale_212() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[31]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[31]));
	ENDFOR
}

void SquareAndScale_213() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[32]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[32]));
	ENDFOR
}

void SquareAndScale_214() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[33]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[33]));
	ENDFOR
}

void SquareAndScale_215() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[34]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[34]));
	ENDFOR
}

void SquareAndScale_216() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[35]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[35]));
	ENDFOR
}

void SquareAndScale_217() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[36]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[36]));
	ENDFOR
}

void SquareAndScale_218() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[37]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[37]));
	ENDFOR
}

void SquareAndScale_219() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[38]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[38]));
	ENDFOR
}

void SquareAndScale_220() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[39]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[39]));
	ENDFOR
}

void SquareAndScale_221() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[40]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[40]));
	ENDFOR
}

void SquareAndScale_222() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[41]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[41]));
	ENDFOR
}

void SquareAndScale_223() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[42]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[42]));
	ENDFOR
}

void SquareAndScale_224() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[43]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[43]));
	ENDFOR
}

void SquareAndScale_225() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[44]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[44]));
	ENDFOR
}

void SquareAndScale_226() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[45]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[45]));
	ENDFOR
}

void SquareAndScale_227() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[46]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[46]));
	ENDFOR
}

void SquareAndScale_228() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[47]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[47]));
	ENDFOR
}

void SquareAndScale_229() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[48]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[48]));
	ENDFOR
}

void SquareAndScale_230() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[49]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[49]));
	ENDFOR
}

void SquareAndScale_231() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[50]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[50]));
	ENDFOR
}

void SquareAndScale_232() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[51]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[51]));
	ENDFOR
}

void SquareAndScale_233() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[52]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[52]));
	ENDFOR
}

void SquareAndScale_234() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[53]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[53]));
	ENDFOR
}

void SquareAndScale_235() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[54]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[54]));
	ENDFOR
}

void SquareAndScale_236() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[55]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[55]));
	ENDFOR
}

void SquareAndScale_237() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[56]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[56]));
	ENDFOR
}

void SquareAndScale_238() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[57]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[57]));
	ENDFOR
}

void SquareAndScale_239() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[58]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[58]));
	ENDFOR
}

void SquareAndScale_240() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[59]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[59]));
	ENDFOR
}

void SquareAndScale_241() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[60]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[60]));
	ENDFOR
}

void SquareAndScale_242() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[61]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[61]));
	ENDFOR
}

void SquareAndScale_243() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[62]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[62]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[__iter_], pop_complex(&ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176, pop_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_176_s.pos) - 5) >= 0)), __DEFLOOPBOUND__14__, i__conflict__1++) {
			sum = (sum + CFAR_gather_176_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_176_s.pos) < 64)), __DEFLOOPBOUND__15__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_176_s.poke[(i - 1)] = CFAR_gather_176_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_176_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_176_s.pos++ ; 
		if(CFAR_gather_176_s.pos == 64) {
			CFAR_gather_176_s.pos = 0 ; 
		}
	}


void CFAR_gather_176() {
	FOR(uint32_t, __iter_steady_, 0, <, 4032, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176), &(CFAR_gather_176AnonFilter_a0_177));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_177() {
	FOR(uint32_t, __iter_steady_, 0, <, 4032, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_176AnonFilter_a0_177));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179);
	FOR(int, __iter_init_0_, 0, <, 63, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 63, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_176AnonFilter_a0_177);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176);
// --- init: ComplexSource_173
	 {
	ComplexSource_173_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_179
	
	FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_244_246_split[__iter_], pop_complex(&ComplexSource_173WEIGHTED_ROUND_ROBIN_Splitter_179));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_181
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[0]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[0]));
//--------------------------------
// --- init: SquareAndScale_182
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[1]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[1]));
//--------------------------------
// --- init: SquareAndScale_183
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[2]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[2]));
//--------------------------------
// --- init: SquareAndScale_184
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[3]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[3]));
//--------------------------------
// --- init: SquareAndScale_185
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[4]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[4]));
//--------------------------------
// --- init: SquareAndScale_186
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[5]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[5]));
//--------------------------------
// --- init: SquareAndScale_187
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[6]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[6]));
//--------------------------------
// --- init: SquareAndScale_188
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[7]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[7]));
//--------------------------------
// --- init: SquareAndScale_189
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[8]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[8]));
//--------------------------------
// --- init: SquareAndScale_190
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[9]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[9]));
//--------------------------------
// --- init: SquareAndScale_191
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[10]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[10]));
//--------------------------------
// --- init: SquareAndScale_192
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[11]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[11]));
//--------------------------------
// --- init: SquareAndScale_193
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[12]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[12]));
//--------------------------------
// --- init: SquareAndScale_194
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[13]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[13]));
//--------------------------------
// --- init: SquareAndScale_195
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[14]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[14]));
//--------------------------------
// --- init: SquareAndScale_196
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[15]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[15]));
//--------------------------------
// --- init: SquareAndScale_197
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[16]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[16]));
//--------------------------------
// --- init: SquareAndScale_198
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[17]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[17]));
//--------------------------------
// --- init: SquareAndScale_199
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[18]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[18]));
//--------------------------------
// --- init: SquareAndScale_200
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[19]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[19]));
//--------------------------------
// --- init: SquareAndScale_201
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[20]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[20]));
//--------------------------------
// --- init: SquareAndScale_202
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[21]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[21]));
//--------------------------------
// --- init: SquareAndScale_203
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[22]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[22]));
//--------------------------------
// --- init: SquareAndScale_204
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[23]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[23]));
//--------------------------------
// --- init: SquareAndScale_205
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[24]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[24]));
//--------------------------------
// --- init: SquareAndScale_206
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[25]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[25]));
//--------------------------------
// --- init: SquareAndScale_207
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[26]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[26]));
//--------------------------------
// --- init: SquareAndScale_208
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[27]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[27]));
//--------------------------------
// --- init: SquareAndScale_209
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[28]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[28]));
//--------------------------------
// --- init: SquareAndScale_210
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[29]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[29]));
//--------------------------------
// --- init: SquareAndScale_211
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[30]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[30]));
//--------------------------------
// --- init: SquareAndScale_212
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[31]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[31]));
//--------------------------------
// --- init: SquareAndScale_213
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[32]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[32]));
//--------------------------------
// --- init: SquareAndScale_214
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[33]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[33]));
//--------------------------------
// --- init: SquareAndScale_215
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[34]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[34]));
//--------------------------------
// --- init: SquareAndScale_216
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[35]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[35]));
//--------------------------------
// --- init: SquareAndScale_217
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[36]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[36]));
//--------------------------------
// --- init: SquareAndScale_218
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[37]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[37]));
//--------------------------------
// --- init: SquareAndScale_219
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[38]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[38]));
//--------------------------------
// --- init: SquareAndScale_220
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[39]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[39]));
//--------------------------------
// --- init: SquareAndScale_221
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[40]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[40]));
//--------------------------------
// --- init: SquareAndScale_222
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[41]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[41]));
//--------------------------------
// --- init: SquareAndScale_223
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[42]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[42]));
//--------------------------------
// --- init: SquareAndScale_224
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[43]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[43]));
//--------------------------------
// --- init: SquareAndScale_225
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[44]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[44]));
//--------------------------------
// --- init: SquareAndScale_226
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[45]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[45]));
//--------------------------------
// --- init: SquareAndScale_227
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[46]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[46]));
//--------------------------------
// --- init: SquareAndScale_228
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[47]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[47]));
//--------------------------------
// --- init: SquareAndScale_229
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[48]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[48]));
//--------------------------------
// --- init: SquareAndScale_230
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[49]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[49]));
//--------------------------------
// --- init: SquareAndScale_231
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[50]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[50]));
//--------------------------------
// --- init: SquareAndScale_232
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[51]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[51]));
//--------------------------------
// --- init: SquareAndScale_233
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[52]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[52]));
//--------------------------------
// --- init: SquareAndScale_234
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[53]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[53]));
//--------------------------------
// --- init: SquareAndScale_235
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[54]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[54]));
//--------------------------------
// --- init: SquareAndScale_236
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[55]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[55]));
//--------------------------------
// --- init: SquareAndScale_237
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[56]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[56]));
//--------------------------------
// --- init: SquareAndScale_238
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[57]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[57]));
//--------------------------------
// --- init: SquareAndScale_239
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[58]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[58]));
//--------------------------------
// --- init: SquareAndScale_240
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[59]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[59]));
//--------------------------------
// --- init: SquareAndScale_241
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[60]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[60]));
//--------------------------------
// --- init: SquareAndScale_242
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[61]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[61]));
//--------------------------------
// --- init: SquareAndScale_243
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_244_246_split[62]), &(SplitJoin0_SquareAndScale_Fiss_244_246_join[62]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_180
	
	FOR(uint32_t, __iter_, 0, <, 63, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176, pop_float(&SplitJoin0_SquareAndScale_Fiss_244_246_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_176
	 {
	CFAR_gather_176_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_180CFAR_gather_176), &(CFAR_gather_176AnonFilter_a0_177));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_177
	FOR(uint32_t, __iter_init_, 0, <, 54, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_176AnonFilter_a0_177));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_173();
		WEIGHTED_ROUND_ROBIN_Splitter_179();
			SquareAndScale_181();
			SquareAndScale_182();
			SquareAndScale_183();
			SquareAndScale_184();
			SquareAndScale_185();
			SquareAndScale_186();
			SquareAndScale_187();
			SquareAndScale_188();
			SquareAndScale_189();
			SquareAndScale_190();
			SquareAndScale_191();
			SquareAndScale_192();
			SquareAndScale_193();
			SquareAndScale_194();
			SquareAndScale_195();
			SquareAndScale_196();
			SquareAndScale_197();
			SquareAndScale_198();
			SquareAndScale_199();
			SquareAndScale_200();
			SquareAndScale_201();
			SquareAndScale_202();
			SquareAndScale_203();
			SquareAndScale_204();
			SquareAndScale_205();
			SquareAndScale_206();
			SquareAndScale_207();
			SquareAndScale_208();
			SquareAndScale_209();
			SquareAndScale_210();
			SquareAndScale_211();
			SquareAndScale_212();
			SquareAndScale_213();
			SquareAndScale_214();
			SquareAndScale_215();
			SquareAndScale_216();
			SquareAndScale_217();
			SquareAndScale_218();
			SquareAndScale_219();
			SquareAndScale_220();
			SquareAndScale_221();
			SquareAndScale_222();
			SquareAndScale_223();
			SquareAndScale_224();
			SquareAndScale_225();
			SquareAndScale_226();
			SquareAndScale_227();
			SquareAndScale_228();
			SquareAndScale_229();
			SquareAndScale_230();
			SquareAndScale_231();
			SquareAndScale_232();
			SquareAndScale_233();
			SquareAndScale_234();
			SquareAndScale_235();
			SquareAndScale_236();
			SquareAndScale_237();
			SquareAndScale_238();
			SquareAndScale_239();
			SquareAndScale_240();
			SquareAndScale_241();
			SquareAndScale_242();
			SquareAndScale_243();
		WEIGHTED_ROUND_ROBIN_Joiner_180();
		CFAR_gather_176();
		AnonFilter_a0_177();
	ENDFOR
	return EXIT_SUCCESS;
}
