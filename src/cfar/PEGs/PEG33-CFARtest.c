#include "PEG33-CFARtest.h"

buffer_complex_t SplitJoin0_SquareAndScale_Fiss_3484_3486_split[33];
buffer_float_t CFAR_gather_3446AnonFilter_a0_3447;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_3484_3486_join[33];
buffer_complex_t ComplexSource_3443WEIGHTED_ROUND_ROBIN_Splitter_3449;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3450CFAR_gather_3446;


ComplexSource_3443_t ComplexSource_3443_s;
CFAR_gather_3446_t CFAR_gather_3446_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_3443_s.theta = (ComplexSource_3443_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_3443_s.theta)) * (((float) cos(ComplexSource_3443_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3443_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_3443_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_3443_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_3443_s.theta))))) + (0.0 * (((float) cos(ComplexSource_3443_s.theta)) + ((0.0 * ((float) sin(ComplexSource_3443_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_3443() {
	FOR(uint32_t, __iter_steady_, 0, <, 33, __iter_steady_++)
		ComplexSource(&(ComplexSource_3443WEIGHTED_ROUND_ROBIN_Splitter_3449));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_3451() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[0]));
	ENDFOR
}

void SquareAndScale_3452() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[1]));
	ENDFOR
}

void SquareAndScale_3453() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[2]));
	ENDFOR
}

void SquareAndScale_3454() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[3]));
	ENDFOR
}

void SquareAndScale_3455() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[4]));
	ENDFOR
}

void SquareAndScale_3456() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[5]));
	ENDFOR
}

void SquareAndScale_3457() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[6]));
	ENDFOR
}

void SquareAndScale_3458() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[7]));
	ENDFOR
}

void SquareAndScale_3459() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[8]));
	ENDFOR
}

void SquareAndScale_3460() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[9]));
	ENDFOR
}

void SquareAndScale_3461() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[10]));
	ENDFOR
}

void SquareAndScale_3462() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[11]));
	ENDFOR
}

void SquareAndScale_3463() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[12]));
	ENDFOR
}

void SquareAndScale_3464() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[13]));
	ENDFOR
}

void SquareAndScale_3465() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[14]));
	ENDFOR
}

void SquareAndScale_3466() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[15]));
	ENDFOR
}

void SquareAndScale_3467() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[16]));
	ENDFOR
}

void SquareAndScale_3468() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[17]));
	ENDFOR
}

void SquareAndScale_3469() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[18]));
	ENDFOR
}

void SquareAndScale_3470() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[19]));
	ENDFOR
}

void SquareAndScale_3471() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[20]));
	ENDFOR
}

void SquareAndScale_3472() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[21]));
	ENDFOR
}

void SquareAndScale_3473() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[22]));
	ENDFOR
}

void SquareAndScale_3474() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[23]));
	ENDFOR
}

void SquareAndScale_3475() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[24]));
	ENDFOR
}

void SquareAndScale_3476() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[25]));
	ENDFOR
}

void SquareAndScale_3477() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[26]));
	ENDFOR
}

void SquareAndScale_3478() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[27]));
	ENDFOR
}

void SquareAndScale_3479() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[28]));
	ENDFOR
}

void SquareAndScale_3480() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[29]));
	ENDFOR
}

void SquareAndScale_3481() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[30]));
	ENDFOR
}

void SquareAndScale_3482() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[31]));
	ENDFOR
}

void SquareAndScale_3483() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[32]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3449() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 33, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_3484_3486_split[__iter_], pop_complex(&ComplexSource_3443WEIGHTED_ROUND_ROBIN_Splitter_3449));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3450() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 33, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3450CFAR_gather_3446, pop_float(&SplitJoin0_SquareAndScale_Fiss_3484_3486_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_3446_s.pos) - 5) >= 0)), __DEFLOOPBOUND__194__, i__conflict__1++) {
			sum = (sum + CFAR_gather_3446_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_3446_s.pos) < 64)), __DEFLOOPBOUND__195__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_3446_s.poke[(i - 1)] = CFAR_gather_3446_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_3446_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_3446_s.pos++ ; 
		if(CFAR_gather_3446_s.pos == 64) {
			CFAR_gather_3446_s.pos = 0 ; 
		}
	}


void CFAR_gather_3446() {
	FOR(uint32_t, __iter_steady_, 0, <, 2112, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3450CFAR_gather_3446), &(CFAR_gather_3446AnonFilter_a0_3447));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_3447() {
	FOR(uint32_t, __iter_steady_, 0, <, 2112, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_3446AnonFilter_a0_3447));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 33, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_3484_3486_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_3446AnonFilter_a0_3447);
	FOR(int, __iter_init_1_, 0, <, 33, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_3484_3486_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&ComplexSource_3443WEIGHTED_ROUND_ROBIN_Splitter_3449);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3450CFAR_gather_3446);
// --- init: ComplexSource_3443
	 {
	ComplexSource_3443_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_3443WEIGHTED_ROUND_ROBIN_Splitter_3449));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3449
	
	FOR(uint32_t, __iter_, 0, <, 33, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_3484_3486_split[__iter_], pop_complex(&ComplexSource_3443WEIGHTED_ROUND_ROBIN_Splitter_3449));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_3451
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[0]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[0]));
//--------------------------------
// --- init: SquareAndScale_3452
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[1]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[1]));
//--------------------------------
// --- init: SquareAndScale_3453
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[2]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[2]));
//--------------------------------
// --- init: SquareAndScale_3454
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[3]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[3]));
//--------------------------------
// --- init: SquareAndScale_3455
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[4]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[4]));
//--------------------------------
// --- init: SquareAndScale_3456
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[5]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[5]));
//--------------------------------
// --- init: SquareAndScale_3457
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[6]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[6]));
//--------------------------------
// --- init: SquareAndScale_3458
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[7]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[7]));
//--------------------------------
// --- init: SquareAndScale_3459
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[8]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[8]));
//--------------------------------
// --- init: SquareAndScale_3460
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[9]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[9]));
//--------------------------------
// --- init: SquareAndScale_3461
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[10]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[10]));
//--------------------------------
// --- init: SquareAndScale_3462
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[11]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[11]));
//--------------------------------
// --- init: SquareAndScale_3463
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[12]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[12]));
//--------------------------------
// --- init: SquareAndScale_3464
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[13]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[13]));
//--------------------------------
// --- init: SquareAndScale_3465
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[14]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[14]));
//--------------------------------
// --- init: SquareAndScale_3466
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[15]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[15]));
//--------------------------------
// --- init: SquareAndScale_3467
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[16]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[16]));
//--------------------------------
// --- init: SquareAndScale_3468
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[17]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[17]));
//--------------------------------
// --- init: SquareAndScale_3469
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[18]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[18]));
//--------------------------------
// --- init: SquareAndScale_3470
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[19]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[19]));
//--------------------------------
// --- init: SquareAndScale_3471
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[20]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[20]));
//--------------------------------
// --- init: SquareAndScale_3472
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[21]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[21]));
//--------------------------------
// --- init: SquareAndScale_3473
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[22]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[22]));
//--------------------------------
// --- init: SquareAndScale_3474
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[23]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[23]));
//--------------------------------
// --- init: SquareAndScale_3475
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[24]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[24]));
//--------------------------------
// --- init: SquareAndScale_3476
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[25]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[25]));
//--------------------------------
// --- init: SquareAndScale_3477
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[26]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[26]));
//--------------------------------
// --- init: SquareAndScale_3478
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[27]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[27]));
//--------------------------------
// --- init: SquareAndScale_3479
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[28]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[28]));
//--------------------------------
// --- init: SquareAndScale_3480
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[29]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[29]));
//--------------------------------
// --- init: SquareAndScale_3481
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[30]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[30]));
//--------------------------------
// --- init: SquareAndScale_3482
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[31]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[31]));
//--------------------------------
// --- init: SquareAndScale_3483
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_3484_3486_split[32]), &(SplitJoin0_SquareAndScale_Fiss_3484_3486_join[32]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3450
	
	FOR(uint32_t, __iter_, 0, <, 33, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3450CFAR_gather_3446, pop_float(&SplitJoin0_SquareAndScale_Fiss_3484_3486_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_3446
	 {
	CFAR_gather_3446_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 24, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_3450CFAR_gather_3446), &(CFAR_gather_3446AnonFilter_a0_3447));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_3447
	FOR(uint32_t, __iter_init_, 0, <, 24, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_3446AnonFilter_a0_3447));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_3443();
		WEIGHTED_ROUND_ROBIN_Splitter_3449();
			SquareAndScale_3451();
			SquareAndScale_3452();
			SquareAndScale_3453();
			SquareAndScale_3454();
			SquareAndScale_3455();
			SquareAndScale_3456();
			SquareAndScale_3457();
			SquareAndScale_3458();
			SquareAndScale_3459();
			SquareAndScale_3460();
			SquareAndScale_3461();
			SquareAndScale_3462();
			SquareAndScale_3463();
			SquareAndScale_3464();
			SquareAndScale_3465();
			SquareAndScale_3466();
			SquareAndScale_3467();
			SquareAndScale_3468();
			SquareAndScale_3469();
			SquareAndScale_3470();
			SquareAndScale_3471();
			SquareAndScale_3472();
			SquareAndScale_3473();
			SquareAndScale_3474();
			SquareAndScale_3475();
			SquareAndScale_3476();
			SquareAndScale_3477();
			SquareAndScale_3478();
			SquareAndScale_3479();
			SquareAndScale_3480();
			SquareAndScale_3481();
			SquareAndScale_3482();
			SquareAndScale_3483();
		WEIGHTED_ROUND_ROBIN_Joiner_3450();
		CFAR_gather_3446();
		AnonFilter_a0_3447();
	ENDFOR
	return EXIT_SUCCESS;
}
