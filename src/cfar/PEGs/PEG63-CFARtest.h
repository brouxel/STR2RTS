#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4096 on the compile command line
#else
#if BUF_SIZEMAX < 4096
#error BUF_SIZEMAX too small, it must be at least 4096
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_173_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_176_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_173();
void WEIGHTED_ROUND_ROBIN_Splitter_179();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_181();
void SquareAndScale_182();
void SquareAndScale_183();
void SquareAndScale_184();
void SquareAndScale_185();
void SquareAndScale_186();
void SquareAndScale_187();
void SquareAndScale_188();
void SquareAndScale_189();
void SquareAndScale_190();
void SquareAndScale_191();
void SquareAndScale_192();
void SquareAndScale_193();
void SquareAndScale_194();
void SquareAndScale_195();
void SquareAndScale_196();
void SquareAndScale_197();
void SquareAndScale_198();
void SquareAndScale_199();
void SquareAndScale_200();
void SquareAndScale_201();
void SquareAndScale_202();
void SquareAndScale_203();
void SquareAndScale_204();
void SquareAndScale_205();
void SquareAndScale_206();
void SquareAndScale_207();
void SquareAndScale_208();
void SquareAndScale_209();
void SquareAndScale_210();
void SquareAndScale_211();
void SquareAndScale_212();
void SquareAndScale_213();
void SquareAndScale_214();
void SquareAndScale_215();
void SquareAndScale_216();
void SquareAndScale_217();
void SquareAndScale_218();
void SquareAndScale_219();
void SquareAndScale_220();
void SquareAndScale_221();
void SquareAndScale_222();
void SquareAndScale_223();
void SquareAndScale_224();
void SquareAndScale_225();
void SquareAndScale_226();
void SquareAndScale_227();
void SquareAndScale_228();
void SquareAndScale_229();
void SquareAndScale_230();
void SquareAndScale_231();
void SquareAndScale_232();
void SquareAndScale_233();
void SquareAndScale_234();
void SquareAndScale_235();
void SquareAndScale_236();
void SquareAndScale_237();
void SquareAndScale_238();
void SquareAndScale_239();
void SquareAndScale_240();
void SquareAndScale_241();
void SquareAndScale_242();
void SquareAndScale_243();
void WEIGHTED_ROUND_ROBIN_Joiner_180();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_176();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_177();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1


#ifdef __cplusplus
}
#endif
#endif
