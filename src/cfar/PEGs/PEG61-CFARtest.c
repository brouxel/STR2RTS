#include "PEG61-CFARtest.h"

buffer_complex_t ComplexSource_447WEIGHTED_ROUND_ROBIN_Splitter_453;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_516_518_split[61];
buffer_float_t CFAR_gather_450AnonFilter_a0_451;
buffer_float_t SplitJoin0_SquareAndScale_Fiss_516_518_join[61];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_454CFAR_gather_450;


ComplexSource_447_t ComplexSource_447_s;
CFAR_gather_450_t CFAR_gather_450_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_447_s.theta = (ComplexSource_447_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_447_s.theta)) * (((float) cos(ComplexSource_447_s.theta)) + ((0.0 * ((float) sin(ComplexSource_447_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_447_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_447_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_447_s.theta))))) + (0.0 * (((float) cos(ComplexSource_447_s.theta)) + ((0.0 * ((float) sin(ComplexSource_447_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_447() {
	FOR(uint32_t, __iter_steady_, 0, <, 61, __iter_steady_++)
		ComplexSource(&(ComplexSource_447WEIGHTED_ROUND_ROBIN_Splitter_453));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_455() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[0]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[0]));
	ENDFOR
}

void SquareAndScale_456() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[1]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[1]));
	ENDFOR
}

void SquareAndScale_457() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[2]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[2]));
	ENDFOR
}

void SquareAndScale_458() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[3]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[3]));
	ENDFOR
}

void SquareAndScale_459() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[4]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[4]));
	ENDFOR
}

void SquareAndScale_460() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[5]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[5]));
	ENDFOR
}

void SquareAndScale_461() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[6]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[6]));
	ENDFOR
}

void SquareAndScale_462() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[7]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[7]));
	ENDFOR
}

void SquareAndScale_463() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[8]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[8]));
	ENDFOR
}

void SquareAndScale_464() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[9]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[9]));
	ENDFOR
}

void SquareAndScale_465() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[10]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[10]));
	ENDFOR
}

void SquareAndScale_466() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[11]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[11]));
	ENDFOR
}

void SquareAndScale_467() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[12]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[12]));
	ENDFOR
}

void SquareAndScale_468() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[13]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[13]));
	ENDFOR
}

void SquareAndScale_469() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[14]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[14]));
	ENDFOR
}

void SquareAndScale_470() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[15]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[15]));
	ENDFOR
}

void SquareAndScale_471() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[16]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[16]));
	ENDFOR
}

void SquareAndScale_472() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[17]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[17]));
	ENDFOR
}

void SquareAndScale_473() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[18]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[18]));
	ENDFOR
}

void SquareAndScale_474() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[19]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[19]));
	ENDFOR
}

void SquareAndScale_475() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[20]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[20]));
	ENDFOR
}

void SquareAndScale_476() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[21]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[21]));
	ENDFOR
}

void SquareAndScale_477() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[22]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[22]));
	ENDFOR
}

void SquareAndScale_478() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[23]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[23]));
	ENDFOR
}

void SquareAndScale_479() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[24]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[24]));
	ENDFOR
}

void SquareAndScale_480() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[25]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[25]));
	ENDFOR
}

void SquareAndScale_481() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[26]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[26]));
	ENDFOR
}

void SquareAndScale_482() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[27]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[27]));
	ENDFOR
}

void SquareAndScale_483() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[28]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[28]));
	ENDFOR
}

void SquareAndScale_484() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[29]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[29]));
	ENDFOR
}

void SquareAndScale_485() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[30]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[30]));
	ENDFOR
}

void SquareAndScale_486() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[31]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[31]));
	ENDFOR
}

void SquareAndScale_487() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[32]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[32]));
	ENDFOR
}

void SquareAndScale_488() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[33]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[33]));
	ENDFOR
}

void SquareAndScale_489() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[34]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[34]));
	ENDFOR
}

void SquareAndScale_490() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[35]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[35]));
	ENDFOR
}

void SquareAndScale_491() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[36]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[36]));
	ENDFOR
}

void SquareAndScale_492() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[37]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[37]));
	ENDFOR
}

void SquareAndScale_493() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[38]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[38]));
	ENDFOR
}

void SquareAndScale_494() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[39]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[39]));
	ENDFOR
}

void SquareAndScale_495() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[40]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[40]));
	ENDFOR
}

void SquareAndScale_496() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[41]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[41]));
	ENDFOR
}

void SquareAndScale_497() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[42]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[42]));
	ENDFOR
}

void SquareAndScale_498() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[43]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[43]));
	ENDFOR
}

void SquareAndScale_499() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[44]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[44]));
	ENDFOR
}

void SquareAndScale_500() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[45]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[45]));
	ENDFOR
}

void SquareAndScale_501() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[46]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[46]));
	ENDFOR
}

void SquareAndScale_502() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[47]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[47]));
	ENDFOR
}

void SquareAndScale_503() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[48]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[48]));
	ENDFOR
}

void SquareAndScale_504() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[49]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[49]));
	ENDFOR
}

void SquareAndScale_505() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[50]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[50]));
	ENDFOR
}

void SquareAndScale_506() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[51]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[51]));
	ENDFOR
}

void SquareAndScale_507() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[52]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[52]));
	ENDFOR
}

void SquareAndScale_508() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[53]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[53]));
	ENDFOR
}

void SquareAndScale_509() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[54]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[54]));
	ENDFOR
}

void SquareAndScale_510() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[55]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[55]));
	ENDFOR
}

void SquareAndScale_511() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[56]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[56]));
	ENDFOR
}

void SquareAndScale_512() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[57]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[57]));
	ENDFOR
}

void SquareAndScale_513() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[58]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[58]));
	ENDFOR
}

void SquareAndScale_514() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[59]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[59]));
	ENDFOR
}

void SquareAndScale_515() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[60]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[60]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_453() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 61, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_516_518_split[__iter_], pop_complex(&ComplexSource_447WEIGHTED_ROUND_ROBIN_Splitter_453));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_454() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 61, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_454CFAR_gather_450, pop_float(&SplitJoin0_SquareAndScale_Fiss_516_518_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_450_s.pos) - 5) >= 0)), __DEFLOOPBOUND__26__, i__conflict__1++) {
			sum = (sum + CFAR_gather_450_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_450_s.pos) < 64)), __DEFLOOPBOUND__27__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_450_s.poke[(i - 1)] = CFAR_gather_450_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_450_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_450_s.pos++ ; 
		if(CFAR_gather_450_s.pos == 64) {
			CFAR_gather_450_s.pos = 0 ; 
		}
	}


void CFAR_gather_450() {
	FOR(uint32_t, __iter_steady_, 0, <, 3904, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_454CFAR_gather_450), &(CFAR_gather_450AnonFilter_a0_451));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_451() {
	FOR(uint32_t, __iter_steady_, 0, <, 3904, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_450AnonFilter_a0_451));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_447WEIGHTED_ROUND_ROBIN_Splitter_453);
	FOR(int, __iter_init_0_, 0, <, 61, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_516_518_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_450AnonFilter_a0_451);
	FOR(int, __iter_init_1_, 0, <, 61, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_516_518_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_454CFAR_gather_450);
// --- init: ComplexSource_447
	 {
	ComplexSource_447_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_447WEIGHTED_ROUND_ROBIN_Splitter_453));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_453
	
	FOR(uint32_t, __iter_, 0, <, 61, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_516_518_split[__iter_], pop_complex(&ComplexSource_447WEIGHTED_ROUND_ROBIN_Splitter_453));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_455
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[0]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[0]));
//--------------------------------
// --- init: SquareAndScale_456
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[1]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[1]));
//--------------------------------
// --- init: SquareAndScale_457
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[2]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[2]));
//--------------------------------
// --- init: SquareAndScale_458
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[3]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[3]));
//--------------------------------
// --- init: SquareAndScale_459
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[4]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[4]));
//--------------------------------
// --- init: SquareAndScale_460
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[5]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[5]));
//--------------------------------
// --- init: SquareAndScale_461
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[6]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[6]));
//--------------------------------
// --- init: SquareAndScale_462
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[7]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[7]));
//--------------------------------
// --- init: SquareAndScale_463
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[8]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[8]));
//--------------------------------
// --- init: SquareAndScale_464
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[9]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[9]));
//--------------------------------
// --- init: SquareAndScale_465
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[10]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[10]));
//--------------------------------
// --- init: SquareAndScale_466
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[11]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[11]));
//--------------------------------
// --- init: SquareAndScale_467
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[12]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[12]));
//--------------------------------
// --- init: SquareAndScale_468
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[13]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[13]));
//--------------------------------
// --- init: SquareAndScale_469
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[14]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[14]));
//--------------------------------
// --- init: SquareAndScale_470
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[15]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[15]));
//--------------------------------
// --- init: SquareAndScale_471
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[16]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[16]));
//--------------------------------
// --- init: SquareAndScale_472
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[17]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[17]));
//--------------------------------
// --- init: SquareAndScale_473
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[18]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[18]));
//--------------------------------
// --- init: SquareAndScale_474
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[19]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[19]));
//--------------------------------
// --- init: SquareAndScale_475
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[20]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[20]));
//--------------------------------
// --- init: SquareAndScale_476
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[21]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[21]));
//--------------------------------
// --- init: SquareAndScale_477
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[22]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[22]));
//--------------------------------
// --- init: SquareAndScale_478
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[23]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[23]));
//--------------------------------
// --- init: SquareAndScale_479
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[24]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[24]));
//--------------------------------
// --- init: SquareAndScale_480
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[25]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[25]));
//--------------------------------
// --- init: SquareAndScale_481
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[26]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[26]));
//--------------------------------
// --- init: SquareAndScale_482
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[27]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[27]));
//--------------------------------
// --- init: SquareAndScale_483
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[28]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[28]));
//--------------------------------
// --- init: SquareAndScale_484
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[29]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[29]));
//--------------------------------
// --- init: SquareAndScale_485
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[30]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[30]));
//--------------------------------
// --- init: SquareAndScale_486
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[31]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[31]));
//--------------------------------
// --- init: SquareAndScale_487
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[32]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[32]));
//--------------------------------
// --- init: SquareAndScale_488
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[33]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[33]));
//--------------------------------
// --- init: SquareAndScale_489
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[34]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[34]));
//--------------------------------
// --- init: SquareAndScale_490
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[35]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[35]));
//--------------------------------
// --- init: SquareAndScale_491
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[36]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[36]));
//--------------------------------
// --- init: SquareAndScale_492
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[37]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[37]));
//--------------------------------
// --- init: SquareAndScale_493
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[38]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[38]));
//--------------------------------
// --- init: SquareAndScale_494
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[39]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[39]));
//--------------------------------
// --- init: SquareAndScale_495
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[40]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[40]));
//--------------------------------
// --- init: SquareAndScale_496
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[41]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[41]));
//--------------------------------
// --- init: SquareAndScale_497
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[42]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[42]));
//--------------------------------
// --- init: SquareAndScale_498
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[43]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[43]));
//--------------------------------
// --- init: SquareAndScale_499
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[44]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[44]));
//--------------------------------
// --- init: SquareAndScale_500
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[45]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[45]));
//--------------------------------
// --- init: SquareAndScale_501
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[46]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[46]));
//--------------------------------
// --- init: SquareAndScale_502
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[47]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[47]));
//--------------------------------
// --- init: SquareAndScale_503
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[48]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[48]));
//--------------------------------
// --- init: SquareAndScale_504
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[49]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[49]));
//--------------------------------
// --- init: SquareAndScale_505
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[50]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[50]));
//--------------------------------
// --- init: SquareAndScale_506
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[51]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[51]));
//--------------------------------
// --- init: SquareAndScale_507
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[52]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[52]));
//--------------------------------
// --- init: SquareAndScale_508
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[53]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[53]));
//--------------------------------
// --- init: SquareAndScale_509
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[54]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[54]));
//--------------------------------
// --- init: SquareAndScale_510
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[55]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[55]));
//--------------------------------
// --- init: SquareAndScale_511
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[56]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[56]));
//--------------------------------
// --- init: SquareAndScale_512
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[57]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[57]));
//--------------------------------
// --- init: SquareAndScale_513
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[58]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[58]));
//--------------------------------
// --- init: SquareAndScale_514
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[59]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[59]));
//--------------------------------
// --- init: SquareAndScale_515
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_516_518_split[60]), &(SplitJoin0_SquareAndScale_Fiss_516_518_join[60]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_454
	
	FOR(uint32_t, __iter_, 0, <, 61, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_454CFAR_gather_450, pop_float(&SplitJoin0_SquareAndScale_Fiss_516_518_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_450
	 {
	CFAR_gather_450_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_454CFAR_gather_450), &(CFAR_gather_450AnonFilter_a0_451));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_451
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_450AnonFilter_a0_451));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_447();
		WEIGHTED_ROUND_ROBIN_Splitter_453();
			SquareAndScale_455();
			SquareAndScale_456();
			SquareAndScale_457();
			SquareAndScale_458();
			SquareAndScale_459();
			SquareAndScale_460();
			SquareAndScale_461();
			SquareAndScale_462();
			SquareAndScale_463();
			SquareAndScale_464();
			SquareAndScale_465();
			SquareAndScale_466();
			SquareAndScale_467();
			SquareAndScale_468();
			SquareAndScale_469();
			SquareAndScale_470();
			SquareAndScale_471();
			SquareAndScale_472();
			SquareAndScale_473();
			SquareAndScale_474();
			SquareAndScale_475();
			SquareAndScale_476();
			SquareAndScale_477();
			SquareAndScale_478();
			SquareAndScale_479();
			SquareAndScale_480();
			SquareAndScale_481();
			SquareAndScale_482();
			SquareAndScale_483();
			SquareAndScale_484();
			SquareAndScale_485();
			SquareAndScale_486();
			SquareAndScale_487();
			SquareAndScale_488();
			SquareAndScale_489();
			SquareAndScale_490();
			SquareAndScale_491();
			SquareAndScale_492();
			SquareAndScale_493();
			SquareAndScale_494();
			SquareAndScale_495();
			SquareAndScale_496();
			SquareAndScale_497();
			SquareAndScale_498();
			SquareAndScale_499();
			SquareAndScale_500();
			SquareAndScale_501();
			SquareAndScale_502();
			SquareAndScale_503();
			SquareAndScale_504();
			SquareAndScale_505();
			SquareAndScale_506();
			SquareAndScale_507();
			SquareAndScale_508();
			SquareAndScale_509();
			SquareAndScale_510();
			SquareAndScale_511();
			SquareAndScale_512();
			SquareAndScale_513();
			SquareAndScale_514();
			SquareAndScale_515();
		WEIGHTED_ROUND_ROBIN_Joiner_454();
		CFAR_gather_450();
		AnonFilter_a0_451();
	ENDFOR
	return EXIT_SUCCESS;
}
