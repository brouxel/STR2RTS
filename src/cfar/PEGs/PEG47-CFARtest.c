#include "PEG47-CFARtest.h"

buffer_complex_t ComplexSource_2141WEIGHTED_ROUND_ROBIN_Splitter_2147;
buffer_complex_t SplitJoin0_SquareAndScale_Fiss_2196_2198_split[47];
buffer_float_t SplitJoin0_SquareAndScale_Fiss_2196_2198_join[47];
buffer_float_t CFAR_gather_2144AnonFilter_a0_2145;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2148CFAR_gather_2144;


ComplexSource_2141_t ComplexSource_2141_s;
CFAR_gather_2144_t CFAR_gather_2144_s;

void ComplexSource(buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			complex_t c = {
				.real = 0,
				.imag = 0
			};
			ComplexSource_2141_s.theta = (ComplexSource_2141_s.theta + 0.19634955) ; 
			c.real = ((((float) sin(ComplexSource_2141_s.theta)) * (((float) cos(ComplexSource_2141_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2141_s.theta))) - 0.0))) - (0.0 * (0.0 + (1.0 * ((float) sin(ComplexSource_2141_s.theta)))))) ; 
			c.imag = ((((float) sin(ComplexSource_2141_s.theta)) * (0.0 + (1.0 * ((float) sin(ComplexSource_2141_s.theta))))) + (0.0 * (((float) cos(ComplexSource_2141_s.theta)) + ((0.0 * ((float) sin(ComplexSource_2141_s.theta))) - 0.0)))) ; 
			push_complex(&(*chanout), c) ; 
		}
		ENDFOR
	}


void ComplexSource_2141() {
	FOR(uint32_t, __iter_steady_, 0, <, 47, __iter_steady_++)
		ComplexSource(&(ComplexSource_2141WEIGHTED_ROUND_ROBIN_Splitter_2147));
	ENDFOR
}

void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		float mag = 0.0;
		mag = ((float) sqrt(((c.real * c.real) + (c.imag * c.imag)))) ; 
		push_float(&(*chanout), ((mag * mag) / 10.0)) ; 
	}


void SquareAndScale_2149() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[0]));
	ENDFOR
}

void SquareAndScale_2150() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[1]));
	ENDFOR
}

void SquareAndScale_2151() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[2]));
	ENDFOR
}

void SquareAndScale_2152() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[3]));
	ENDFOR
}

void SquareAndScale_2153() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[4]));
	ENDFOR
}

void SquareAndScale_2154() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[5]));
	ENDFOR
}

void SquareAndScale_2155() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[6]));
	ENDFOR
}

void SquareAndScale_2156() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[7]));
	ENDFOR
}

void SquareAndScale_2157() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[8]));
	ENDFOR
}

void SquareAndScale_2158() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[9]));
	ENDFOR
}

void SquareAndScale_2159() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[10]));
	ENDFOR
}

void SquareAndScale_2160() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[11]));
	ENDFOR
}

void SquareAndScale_2161() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[12]));
	ENDFOR
}

void SquareAndScale_2162() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[13]));
	ENDFOR
}

void SquareAndScale_2163() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[14]));
	ENDFOR
}

void SquareAndScale_2164() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[15]));
	ENDFOR
}

void SquareAndScale_2165() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[16]));
	ENDFOR
}

void SquareAndScale_2166() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[17]));
	ENDFOR
}

void SquareAndScale_2167() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[18]));
	ENDFOR
}

void SquareAndScale_2168() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[19]));
	ENDFOR
}

void SquareAndScale_2169() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[20]));
	ENDFOR
}

void SquareAndScale_2170() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[21]));
	ENDFOR
}

void SquareAndScale_2171() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[22]));
	ENDFOR
}

void SquareAndScale_2172() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[23]));
	ENDFOR
}

void SquareAndScale_2173() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[24]));
	ENDFOR
}

void SquareAndScale_2174() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[25]));
	ENDFOR
}

void SquareAndScale_2175() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[26]));
	ENDFOR
}

void SquareAndScale_2176() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[27]));
	ENDFOR
}

void SquareAndScale_2177() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[28]));
	ENDFOR
}

void SquareAndScale_2178() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[29]));
	ENDFOR
}

void SquareAndScale_2179() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[30]));
	ENDFOR
}

void SquareAndScale_2180() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[31]));
	ENDFOR
}

void SquareAndScale_2181() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[32]));
	ENDFOR
}

void SquareAndScale_2182() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[33]));
	ENDFOR
}

void SquareAndScale_2183() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[34]));
	ENDFOR
}

void SquareAndScale_2184() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[35]));
	ENDFOR
}

void SquareAndScale_2185() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[36]));
	ENDFOR
}

void SquareAndScale_2186() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[37]));
	ENDFOR
}

void SquareAndScale_2187() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[38]));
	ENDFOR
}

void SquareAndScale_2188() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[39]));
	ENDFOR
}

void SquareAndScale_2189() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[40]));
	ENDFOR
}

void SquareAndScale_2190() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[41]));
	ENDFOR
}

void SquareAndScale_2191() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[42]));
	ENDFOR
}

void SquareAndScale_2192() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[43]));
	ENDFOR
}

void SquareAndScale_2193() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[44]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[44]));
	ENDFOR
}

void SquareAndScale_2194() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[45]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[45]));
	ENDFOR
}

void SquareAndScale_2195() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[46]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[46]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2147() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 47, __iter_++)
			push_complex(&SplitJoin0_SquareAndScale_Fiss_2196_2198_split[__iter_], pop_complex(&ComplexSource_2141WEIGHTED_ROUND_ROBIN_Splitter_2147));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2148() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 47, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2148CFAR_gather_2144, pop_float(&SplitJoin0_SquareAndScale_Fiss_2196_2198_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR4(int, i__conflict__1, 0, ((i__conflict__1 < 5) && (((i__conflict__1 + CFAR_gather_2144_s.pos) - 5) >= 0)), __DEFLOOPBOUND__110__, i__conflict__1++) {
			sum = (sum + CFAR_gather_2144_s.poke[((5 - i__conflict__1) - 1)]) ; 
		}
		ENDFOR
		FOR4(int, i__conflict__0, 5, ((i__conflict__0 <= 9) && ((i__conflict__0 + CFAR_gather_2144_s.pos) < 64)), __DEFLOOPBOUND__111__, i__conflict__0++) {
			sum = (sum + peek_float(&(*chanin), i__conflict__0)) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
		FOR(int, i, 1,  < , 9, i++) {
			CFAR_gather_2144_s.poke[(i - 1)] = CFAR_gather_2144_s.poke[i] ; 
		}
		ENDFOR
		CFAR_gather_2144_s.poke[8] = pop_float(&(*chanin)) ; 
		CFAR_gather_2144_s.pos++ ; 
		if(CFAR_gather_2144_s.pos == 64) {
			CFAR_gather_2144_s.pos = 0 ; 
		}
	}


void CFAR_gather_2144() {
	FOR(uint32_t, __iter_steady_, 0, <, 3008, __iter_steady_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2148CFAR_gather_2144), &(CFAR_gather_2144AnonFilter_a0_2145));
	ENDFOR
}

void AnonFilter_a0(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void AnonFilter_a0_2145() {
	FOR(uint32_t, __iter_steady_, 0, <, 3008, __iter_steady_++)
		AnonFilter_a0(&(CFAR_gather_2144AnonFilter_a0_2145));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&ComplexSource_2141WEIGHTED_ROUND_ROBIN_Splitter_2147);
	FOR(int, __iter_init_0_, 0, <, 47, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_SquareAndScale_Fiss_2196_2198_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 47, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SquareAndScale_Fiss_2196_2198_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&CFAR_gather_2144AnonFilter_a0_2145);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2148CFAR_gather_2144);
// --- init: ComplexSource_2141
	 {
	ComplexSource_2141_s.theta = 0.0 ; 
}
	ComplexSource(&(ComplexSource_2141WEIGHTED_ROUND_ROBIN_Splitter_2147));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_2147
	
	FOR(uint32_t, __iter_, 0, <, 47, __iter_++)
		push_complex(&SplitJoin0_SquareAndScale_Fiss_2196_2198_split[__iter_], pop_complex(&ComplexSource_2141WEIGHTED_ROUND_ROBIN_Splitter_2147));
	ENDFOR
//--------------------------------
// --- init: SquareAndScale_2149
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[0]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[0]));
//--------------------------------
// --- init: SquareAndScale_2150
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[1]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[1]));
//--------------------------------
// --- init: SquareAndScale_2151
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[2]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[2]));
//--------------------------------
// --- init: SquareAndScale_2152
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[3]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[3]));
//--------------------------------
// --- init: SquareAndScale_2153
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[4]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[4]));
//--------------------------------
// --- init: SquareAndScale_2154
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[5]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[5]));
//--------------------------------
// --- init: SquareAndScale_2155
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[6]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[6]));
//--------------------------------
// --- init: SquareAndScale_2156
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[7]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[7]));
//--------------------------------
// --- init: SquareAndScale_2157
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[8]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[8]));
//--------------------------------
// --- init: SquareAndScale_2158
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[9]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[9]));
//--------------------------------
// --- init: SquareAndScale_2159
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[10]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[10]));
//--------------------------------
// --- init: SquareAndScale_2160
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[11]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[11]));
//--------------------------------
// --- init: SquareAndScale_2161
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[12]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[12]));
//--------------------------------
// --- init: SquareAndScale_2162
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[13]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[13]));
//--------------------------------
// --- init: SquareAndScale_2163
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[14]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[14]));
//--------------------------------
// --- init: SquareAndScale_2164
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[15]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[15]));
//--------------------------------
// --- init: SquareAndScale_2165
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[16]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[16]));
//--------------------------------
// --- init: SquareAndScale_2166
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[17]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[17]));
//--------------------------------
// --- init: SquareAndScale_2167
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[18]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[18]));
//--------------------------------
// --- init: SquareAndScale_2168
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[19]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[19]));
//--------------------------------
// --- init: SquareAndScale_2169
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[20]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[20]));
//--------------------------------
// --- init: SquareAndScale_2170
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[21]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[21]));
//--------------------------------
// --- init: SquareAndScale_2171
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[22]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[22]));
//--------------------------------
// --- init: SquareAndScale_2172
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[23]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[23]));
//--------------------------------
// --- init: SquareAndScale_2173
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[24]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[24]));
//--------------------------------
// --- init: SquareAndScale_2174
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[25]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[25]));
//--------------------------------
// --- init: SquareAndScale_2175
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[26]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[26]));
//--------------------------------
// --- init: SquareAndScale_2176
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[27]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[27]));
//--------------------------------
// --- init: SquareAndScale_2177
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[28]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[28]));
//--------------------------------
// --- init: SquareAndScale_2178
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[29]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[29]));
//--------------------------------
// --- init: SquareAndScale_2179
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[30]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[30]));
//--------------------------------
// --- init: SquareAndScale_2180
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[31]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[31]));
//--------------------------------
// --- init: SquareAndScale_2181
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[32]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[32]));
//--------------------------------
// --- init: SquareAndScale_2182
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[33]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[33]));
//--------------------------------
// --- init: SquareAndScale_2183
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[34]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[34]));
//--------------------------------
// --- init: SquareAndScale_2184
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[35]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[35]));
//--------------------------------
// --- init: SquareAndScale_2185
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[36]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[36]));
//--------------------------------
// --- init: SquareAndScale_2186
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[37]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[37]));
//--------------------------------
// --- init: SquareAndScale_2187
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[38]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[38]));
//--------------------------------
// --- init: SquareAndScale_2188
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[39]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[39]));
//--------------------------------
// --- init: SquareAndScale_2189
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[40]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[40]));
//--------------------------------
// --- init: SquareAndScale_2190
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[41]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[41]));
//--------------------------------
// --- init: SquareAndScale_2191
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[42]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[42]));
//--------------------------------
// --- init: SquareAndScale_2192
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[43]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[43]));
//--------------------------------
// --- init: SquareAndScale_2193
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[44]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[44]));
//--------------------------------
// --- init: SquareAndScale_2194
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[45]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[45]));
//--------------------------------
// --- init: SquareAndScale_2195
	SquareAndScale(&(SplitJoin0_SquareAndScale_Fiss_2196_2198_split[46]), &(SplitJoin0_SquareAndScale_Fiss_2196_2198_join[46]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_2148
	
	FOR(uint32_t, __iter_, 0, <, 47, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2148CFAR_gather_2144, pop_float(&SplitJoin0_SquareAndScale_Fiss_2196_2198_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: CFAR_gather_2144
	 {
	CFAR_gather_2144_s.pos = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		CFAR_gather(&(WEIGHTED_ROUND_ROBIN_Joiner_2148CFAR_gather_2144), &(CFAR_gather_2144AnonFilter_a0_2145));
	ENDFOR
//--------------------------------
// --- init: AnonFilter_a0_2145
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		AnonFilter_a0(&(CFAR_gather_2144AnonFilter_a0_2145));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		ComplexSource_2141();
		WEIGHTED_ROUND_ROBIN_Splitter_2147();
			SquareAndScale_2149();
			SquareAndScale_2150();
			SquareAndScale_2151();
			SquareAndScale_2152();
			SquareAndScale_2153();
			SquareAndScale_2154();
			SquareAndScale_2155();
			SquareAndScale_2156();
			SquareAndScale_2157();
			SquareAndScale_2158();
			SquareAndScale_2159();
			SquareAndScale_2160();
			SquareAndScale_2161();
			SquareAndScale_2162();
			SquareAndScale_2163();
			SquareAndScale_2164();
			SquareAndScale_2165();
			SquareAndScale_2166();
			SquareAndScale_2167();
			SquareAndScale_2168();
			SquareAndScale_2169();
			SquareAndScale_2170();
			SquareAndScale_2171();
			SquareAndScale_2172();
			SquareAndScale_2173();
			SquareAndScale_2174();
			SquareAndScale_2175();
			SquareAndScale_2176();
			SquareAndScale_2177();
			SquareAndScale_2178();
			SquareAndScale_2179();
			SquareAndScale_2180();
			SquareAndScale_2181();
			SquareAndScale_2182();
			SquareAndScale_2183();
			SquareAndScale_2184();
			SquareAndScale_2185();
			SquareAndScale_2186();
			SquareAndScale_2187();
			SquareAndScale_2188();
			SquareAndScale_2189();
			SquareAndScale_2190();
			SquareAndScale_2191();
			SquareAndScale_2192();
			SquareAndScale_2193();
			SquareAndScale_2194();
			SquareAndScale_2195();
		WEIGHTED_ROUND_ROBIN_Joiner_2148();
		CFAR_gather_2144();
		AnonFilter_a0_2145();
	ENDFOR
	return EXIT_SUCCESS;
}
