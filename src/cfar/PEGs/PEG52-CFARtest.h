#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=896 on the compile command line
#else
#if BUF_SIZEMAX < 896
#error BUF_SIZEMAX too small, it must be at least 896
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float theta;
} ComplexSource_1581_t;

typedef struct {
	int pos;
	float poke[9];
} CFAR_gather_1584_t;
void ComplexSource(buffer_complex_t *chanout);
void ComplexSource_1581();
void WEIGHTED_ROUND_ROBIN_Splitter_1587();
void SquareAndScale(buffer_complex_t *chanin, buffer_float_t *chanout);
void SquareAndScale_1589();
void SquareAndScale_1590();
void SquareAndScale_1591();
void SquareAndScale_1592();
void SquareAndScale_1593();
void SquareAndScale_1594();
void SquareAndScale_1595();
void SquareAndScale_1596();
void SquareAndScale_1597();
void SquareAndScale_1598();
void SquareAndScale_1599();
void SquareAndScale_1600();
void SquareAndScale_1601();
void SquareAndScale_1602();
void SquareAndScale_1603();
void SquareAndScale_1604();
void SquareAndScale_1605();
void SquareAndScale_1606();
void SquareAndScale_1607();
void SquareAndScale_1608();
void SquareAndScale_1609();
void SquareAndScale_1610();
void SquareAndScale_1611();
void SquareAndScale_1612();
void SquareAndScale_1613();
void SquareAndScale_1614();
void SquareAndScale_1615();
void SquareAndScale_1616();
void SquareAndScale_1617();
void SquareAndScale_1618();
void SquareAndScale_1619();
void SquareAndScale_1620();
void SquareAndScale_1621();
void SquareAndScale_1622();
void SquareAndScale_1623();
void SquareAndScale_1624();
void SquareAndScale_1625();
void SquareAndScale_1626();
void SquareAndScale_1627();
void SquareAndScale_1628();
void SquareAndScale_1629();
void SquareAndScale_1630();
void SquareAndScale_1631();
void SquareAndScale_1632();
void SquareAndScale_1633();
void SquareAndScale_1634();
void SquareAndScale_1635();
void SquareAndScale_1636();
void SquareAndScale_1637();
void SquareAndScale_1638();
void SquareAndScale_1639();
void SquareAndScale_1640();
void WEIGHTED_ROUND_ROBIN_Joiner_1588();
void CFAR_gather(buffer_float_t *chanin, buffer_float_t *chanout);
void CFAR_gather_1584();
void AnonFilter_a0(buffer_float_t *chanin);
void AnonFilter_a0_1585();
#define __DEFLOOPBOUND__0__ -1

#define __DEFLOOPBOUND__1__ -1

#define __DEFLOOPBOUND__2__ -1

#define __DEFLOOPBOUND__3__ -1

#define __DEFLOOPBOUND__4__ -1

#define __DEFLOOPBOUND__5__ -1

#define __DEFLOOPBOUND__6__ -1

#define __DEFLOOPBOUND__7__ -1

#define __DEFLOOPBOUND__8__ -1

#define __DEFLOOPBOUND__9__ -1

#define __DEFLOOPBOUND__10__ -1

#define __DEFLOOPBOUND__11__ -1

#define __DEFLOOPBOUND__12__ -1

#define __DEFLOOPBOUND__13__ -1

#define __DEFLOOPBOUND__14__ -1

#define __DEFLOOPBOUND__15__ -1

#define __DEFLOOPBOUND__16__ -1

#define __DEFLOOPBOUND__17__ -1

#define __DEFLOOPBOUND__18__ -1

#define __DEFLOOPBOUND__19__ -1

#define __DEFLOOPBOUND__20__ -1

#define __DEFLOOPBOUND__21__ -1

#define __DEFLOOPBOUND__22__ -1

#define __DEFLOOPBOUND__23__ -1

#define __DEFLOOPBOUND__24__ -1

#define __DEFLOOPBOUND__25__ -1

#define __DEFLOOPBOUND__26__ -1

#define __DEFLOOPBOUND__27__ -1

#define __DEFLOOPBOUND__28__ -1

#define __DEFLOOPBOUND__29__ -1

#define __DEFLOOPBOUND__30__ -1

#define __DEFLOOPBOUND__31__ -1

#define __DEFLOOPBOUND__32__ -1

#define __DEFLOOPBOUND__33__ -1

#define __DEFLOOPBOUND__34__ -1

#define __DEFLOOPBOUND__35__ -1

#define __DEFLOOPBOUND__36__ -1

#define __DEFLOOPBOUND__37__ -1

#define __DEFLOOPBOUND__38__ -1

#define __DEFLOOPBOUND__39__ -1

#define __DEFLOOPBOUND__40__ -1

#define __DEFLOOPBOUND__41__ -1

#define __DEFLOOPBOUND__42__ -1

#define __DEFLOOPBOUND__43__ -1

#define __DEFLOOPBOUND__44__ -1

#define __DEFLOOPBOUND__45__ -1

#define __DEFLOOPBOUND__46__ -1

#define __DEFLOOPBOUND__47__ -1

#define __DEFLOOPBOUND__48__ -1

#define __DEFLOOPBOUND__49__ -1

#define __DEFLOOPBOUND__50__ -1

#define __DEFLOOPBOUND__51__ -1

#define __DEFLOOPBOUND__52__ -1

#define __DEFLOOPBOUND__53__ -1

#define __DEFLOOPBOUND__54__ -1

#define __DEFLOOPBOUND__55__ -1

#define __DEFLOOPBOUND__56__ -1

#define __DEFLOOPBOUND__57__ -1

#define __DEFLOOPBOUND__58__ -1

#define __DEFLOOPBOUND__59__ -1

#define __DEFLOOPBOUND__60__ -1

#define __DEFLOOPBOUND__61__ -1

#define __DEFLOOPBOUND__62__ -1

#define __DEFLOOPBOUND__63__ -1

#define __DEFLOOPBOUND__64__ -1

#define __DEFLOOPBOUND__65__ -1

#define __DEFLOOPBOUND__66__ -1

#define __DEFLOOPBOUND__67__ -1

#define __DEFLOOPBOUND__68__ -1

#define __DEFLOOPBOUND__69__ -1

#define __DEFLOOPBOUND__70__ -1

#define __DEFLOOPBOUND__71__ -1

#define __DEFLOOPBOUND__72__ -1

#define __DEFLOOPBOUND__73__ -1

#define __DEFLOOPBOUND__74__ -1

#define __DEFLOOPBOUND__75__ -1

#define __DEFLOOPBOUND__76__ -1

#define __DEFLOOPBOUND__77__ -1

#define __DEFLOOPBOUND__78__ -1

#define __DEFLOOPBOUND__79__ -1

#define __DEFLOOPBOUND__80__ -1

#define __DEFLOOPBOUND__81__ -1

#define __DEFLOOPBOUND__82__ -1

#define __DEFLOOPBOUND__83__ -1


#ifdef __cplusplus
}
#endif
#endif
