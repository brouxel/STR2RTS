BENCH:=sdf_matmult_nocache
BENCHSRCFILE:=$(SRC_DIR)/matmult/SDF-MatrixMultBlock_nocache.c
BENCHHFILE:=$(SRC_DIR)/matmult/SDF-MatrixMultBlock_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=1512

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

BENCH:=hsdf_matmult_nocache
BENCHSRCFILE:=$(SRC_DIR)/matmult/HSDF-MatrixMultBlock_nocache.c
BENCHHFILE:=$(SRC_DIR)/matmult/HSDF-MatrixMultBlock_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=864

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

#$(foreach b, $(shell ls $(SRC_DIR)/matmult/PEGs/PEG*.h), \
   $(eval $(call BENCH_TEMPLATE, \
		 $(shell echo $$(basename -s'.h' $(b)) | tr A-Z a-z | tr - _), \
		 $(shell dirname $(b))/$(shell basename -s'.h' $(b)).c, \
		 $(b), \
		 $(shell grep "\-DBUF_SIZEMAX=" $(b) | sed -e 's/.*\(-DBUF_SIZEMAX=[0-9]*\).*/\1/')))\
)