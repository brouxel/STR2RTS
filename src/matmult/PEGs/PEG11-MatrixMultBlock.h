#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=9504 on the compile command line
#else
#if BUF_SIZEMAX < 9504
#error BUF_SIZEMAX too small, it must be at least 9504
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_84805_t;

typedef struct {
	float result[3][4];
} BlockAdd_84832_t;
void BlockFloatSource(buffer_float_t *chanout);
void BlockFloatSource_84805();
void WEIGHTED_ROUND_ROBIN_Splitter_84934();
void WEIGHTED_ROUND_ROBIN_Splitter_84942();
void Post_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_1_84944();
void Post_CollapsedDataParallel_1_84945();
void Post_CollapsedDataParallel_1_84946();
void Post_CollapsedDataParallel_1_84947();
void Post_CollapsedDataParallel_1_84948();
void Post_CollapsedDataParallel_1_84949();
void Post_CollapsedDataParallel_1_84950();
void Post_CollapsedDataParallel_1_84951();
void Post_CollapsedDataParallel_1_84952();
void Post_CollapsedDataParallel_1_84953();
void Post_CollapsedDataParallel_1_84954();
void WEIGHTED_ROUND_ROBIN_Joiner_84943();
void Identity(buffer_float_t *chanin, buffer_float_t *chanout);
void Identity_84809();
void WEIGHTED_ROUND_ROBIN_Splitter_84955();
void Pre_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_2_84957();
void Pre_CollapsedDataParallel_2_84958();
void Pre_CollapsedDataParallel_2_84959();
void WEIGHTED_ROUND_ROBIN_Joiner_84956();
void WEIGHTED_ROUND_ROBIN_Splitter_84960();
void AutoGeneratedExpander(buffer_float_t *chanin, buffer_float_t *chanout);
void AutoGeneratedExpander_84962();
void AutoGeneratedExpander_84963();
void AutoGeneratedExpander_84964();
void AutoGeneratedExpander_84965();
void AutoGeneratedExpander_84966();
void AutoGeneratedExpander_84967();
void AutoGeneratedExpander_84968();
void AutoGeneratedExpander_84969();
void AutoGeneratedExpander_84970();
void AutoGeneratedExpander_84971();
void AutoGeneratedExpander_84972();
void WEIGHTED_ROUND_ROBIN_Joiner_84961();
void Identity_84812();
void WEIGHTED_ROUND_ROBIN_Splitter_84973();
void Pre_CollapsedDataParallel_2_84975();
void Pre_CollapsedDataParallel_2_84976();
void Pre_CollapsedDataParallel_2_84977();
void WEIGHTED_ROUND_ROBIN_Joiner_84974();
void Identity_84816();
void Pre_CollapsedDataParallel_2_84928();
void WEIGHTED_ROUND_ROBIN_Splitter_84978();
void Post_CollapsedDataParallel_1_84980();
void Post_CollapsedDataParallel_1_84981();
void Post_CollapsedDataParallel_1_84982();
void Post_CollapsedDataParallel_1_84983();
void Post_CollapsedDataParallel_1_84984();
void Post_CollapsedDataParallel_1_84985();
void Post_CollapsedDataParallel_1_84986();
void Post_CollapsedDataParallel_1_84987();
void Post_CollapsedDataParallel_1_84988();
void WEIGHTED_ROUND_ROBIN_Joiner_84979();
void Identity_84825();
void WEIGHTED_ROUND_ROBIN_Splitter_84989();
void Pre_CollapsedDataParallel_2_84991();
void Pre_CollapsedDataParallel_2_84992();
void Pre_CollapsedDataParallel_2_84993();
void WEIGHTED_ROUND_ROBIN_Joiner_84990();
void WEIGHTED_ROUND_ROBIN_Splitter_84994();
void AutoGeneratedExpander_84996();
void AutoGeneratedExpander_84997();
void AutoGeneratedExpander_84998();
void AutoGeneratedExpander_84999();
void AutoGeneratedExpander_85000();
void AutoGeneratedExpander_85001();
void AutoGeneratedExpander_85002();
void AutoGeneratedExpander_85003();
void AutoGeneratedExpander_85004();
void AutoGeneratedExpander_85005();
void AutoGeneratedExpander_85006();
void WEIGHTED_ROUND_ROBIN_Joiner_84995();
void Identity_84828();
void Pre_CollapsedDataParallel_2_84931();
void WEIGHTED_ROUND_ROBIN_Joiner_84935();
void WEIGHTED_ROUND_ROBIN_Splitter_85007();
void BlockMultiply(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockMultiply_85009();
void BlockMultiply_85010();
void BlockMultiply_85011();
void BlockMultiply_85012();
void BlockMultiply_85013();
void BlockMultiply_85014();
void BlockMultiply_85015();
void BlockMultiply_85016();
void BlockMultiply_85017();
void BlockMultiply_85018();
void BlockMultiply_85019();
void WEIGHTED_ROUND_ROBIN_Joiner_85008();
void BlockAdd(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockAdd_84832();
void WEIGHTED_ROUND_ROBIN_Splitter_85020();
void Post_CollapsedDataParallel_1_85022();
void Post_CollapsedDataParallel_1_85023();
void Post_CollapsedDataParallel_1_85024();
void Post_CollapsedDataParallel_1_85025();
void Post_CollapsedDataParallel_1_85026();
void Post_CollapsedDataParallel_1_85027();
void Post_CollapsedDataParallel_1_85028();
void Post_CollapsedDataParallel_1_85029();
void Post_CollapsedDataParallel_1_85030();
void WEIGHTED_ROUND_ROBIN_Joiner_85021();
void Identity_84834();
void WEIGHTED_ROUND_ROBIN_Splitter_85031();
void Pre_CollapsedDataParallel_2_85033();
void Pre_CollapsedDataParallel_2_85034();
void Pre_CollapsedDataParallel_2_85035();
void WEIGHTED_ROUND_ROBIN_Joiner_85032();
void Printer(buffer_float_t *chanin);
void Printer_84838();

#ifdef __cplusplus
}
#endif
#endif
