#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=17280 on the compile command line
#else
#if BUF_SIZEMAX < 17280
#error BUF_SIZEMAX too small, it must be at least 17280
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_49374_t;

typedef struct {
	float result[3][4];
} BlockAdd_49401_t;
void BlockFloatSource(buffer_float_t *chanout);
void BlockFloatSource_49374();
void WEIGHTED_ROUND_ROBIN_Splitter_49503();
void WEIGHTED_ROUND_ROBIN_Splitter_49511();
void Post_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_1_49513();
void Post_CollapsedDataParallel_1_49514();
void Post_CollapsedDataParallel_1_49515();
void Post_CollapsedDataParallel_1_49516();
void Post_CollapsedDataParallel_1_49517();
void Post_CollapsedDataParallel_1_49518();
void Post_CollapsedDataParallel_1_49519();
void Post_CollapsedDataParallel_1_49520();
void Post_CollapsedDataParallel_1_49521();
void Post_CollapsedDataParallel_1_49522();
void Post_CollapsedDataParallel_1_49523();
void Post_CollapsedDataParallel_1_49524();
void WEIGHTED_ROUND_ROBIN_Joiner_49512();
void Identity(buffer_float_t *chanin, buffer_float_t *chanout);
void Identity_49378();
void WEIGHTED_ROUND_ROBIN_Splitter_49525();
void Pre_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_2_49527();
void Pre_CollapsedDataParallel_2_49528();
void Pre_CollapsedDataParallel_2_49529();
void WEIGHTED_ROUND_ROBIN_Joiner_49526();
void WEIGHTED_ROUND_ROBIN_Splitter_49530();
void AutoGeneratedExpander(buffer_float_t *chanin, buffer_float_t *chanout);
void AutoGeneratedExpander_49532();
void AutoGeneratedExpander_49533();
void AutoGeneratedExpander_49534();
void AutoGeneratedExpander_49535();
void AutoGeneratedExpander_49536();
void AutoGeneratedExpander_49537();
void AutoGeneratedExpander_49538();
void AutoGeneratedExpander_49539();
void AutoGeneratedExpander_49540();
void AutoGeneratedExpander_49541();
void AutoGeneratedExpander_49542();
void AutoGeneratedExpander_49543();
void AutoGeneratedExpander_49544();
void AutoGeneratedExpander_49545();
void AutoGeneratedExpander_49546();
void AutoGeneratedExpander_49547();
void AutoGeneratedExpander_49548();
void AutoGeneratedExpander_49549();
void AutoGeneratedExpander_49550();
void AutoGeneratedExpander_49551();
void AutoGeneratedExpander_49552();
void AutoGeneratedExpander_49553();
void AutoGeneratedExpander_49554();
void AutoGeneratedExpander_49555();
void AutoGeneratedExpander_49556();
void AutoGeneratedExpander_49557();
void AutoGeneratedExpander_49558();
void AutoGeneratedExpander_49559();
void AutoGeneratedExpander_49560();
void AutoGeneratedExpander_49561();
void AutoGeneratedExpander_49562();
void AutoGeneratedExpander_49563();
void AutoGeneratedExpander_49564();
void AutoGeneratedExpander_49565();
void AutoGeneratedExpander_49566();
void AutoGeneratedExpander_49567();
void AutoGeneratedExpander_49568();
void AutoGeneratedExpander_49569();
void AutoGeneratedExpander_49570();
void AutoGeneratedExpander_49571();
void AutoGeneratedExpander_49572();
void AutoGeneratedExpander_49573();
void AutoGeneratedExpander_49574();
void AutoGeneratedExpander_49575();
void AutoGeneratedExpander_49576();
void AutoGeneratedExpander_49577();
void AutoGeneratedExpander_49578();
void AutoGeneratedExpander_49579();
void AutoGeneratedExpander_49580();
void AutoGeneratedExpander_49581();
void AutoGeneratedExpander_49582();
void AutoGeneratedExpander_49583();
void AutoGeneratedExpander_49584();
void AutoGeneratedExpander_49585();
void AutoGeneratedExpander_49586();
void AutoGeneratedExpander_49587();
void AutoGeneratedExpander_49588();
void AutoGeneratedExpander_49589();
void AutoGeneratedExpander_49590();
void AutoGeneratedExpander_49591();
void AutoGeneratedExpander_49592();
void AutoGeneratedExpander_49593();
void AutoGeneratedExpander_49594();
void AutoGeneratedExpander_49595();
void AutoGeneratedExpander_49596();
void AutoGeneratedExpander_49597();
void AutoGeneratedExpander_49598();
void AutoGeneratedExpander_49599();
void AutoGeneratedExpander_49600();
void AutoGeneratedExpander_49601();
void AutoGeneratedExpander_49602();
void AutoGeneratedExpander_49603();
void AutoGeneratedExpander_49604();
void AutoGeneratedExpander_49605();
void AutoGeneratedExpander_49606();
void AutoGeneratedExpander_49607();
void AutoGeneratedExpander_49608();
void AutoGeneratedExpander_49609();
void AutoGeneratedExpander_49610();
void AutoGeneratedExpander_49611();
void WEIGHTED_ROUND_ROBIN_Joiner_49531();
void Identity_49381();
void WEIGHTED_ROUND_ROBIN_Splitter_49612();
void Pre_CollapsedDataParallel_2_49614();
void Pre_CollapsedDataParallel_2_49615();
void Pre_CollapsedDataParallel_2_49616();
void WEIGHTED_ROUND_ROBIN_Joiner_49613();
void Identity_49385();
void Pre_CollapsedDataParallel_2_49497();
void WEIGHTED_ROUND_ROBIN_Splitter_49617();
void Post_CollapsedDataParallel_1_49619();
void Post_CollapsedDataParallel_1_49620();
void Post_CollapsedDataParallel_1_49621();
void Post_CollapsedDataParallel_1_49622();
void Post_CollapsedDataParallel_1_49623();
void Post_CollapsedDataParallel_1_49624();
void Post_CollapsedDataParallel_1_49625();
void Post_CollapsedDataParallel_1_49626();
void Post_CollapsedDataParallel_1_49627();
void WEIGHTED_ROUND_ROBIN_Joiner_49618();
void Identity_49394();
void WEIGHTED_ROUND_ROBIN_Splitter_49628();
void Pre_CollapsedDataParallel_2_49630();
void Pre_CollapsedDataParallel_2_49631();
void Pre_CollapsedDataParallel_2_49632();
void WEIGHTED_ROUND_ROBIN_Joiner_49629();
void WEIGHTED_ROUND_ROBIN_Splitter_49633();
void AutoGeneratedExpander_49635();
void AutoGeneratedExpander_49636();
void AutoGeneratedExpander_49637();
void AutoGeneratedExpander_49638();
void AutoGeneratedExpander_49639();
void AutoGeneratedExpander_49640();
void AutoGeneratedExpander_49641();
void AutoGeneratedExpander_49642();
void AutoGeneratedExpander_49643();
void AutoGeneratedExpander_49644();
void AutoGeneratedExpander_49645();
void AutoGeneratedExpander_49646();
void AutoGeneratedExpander_49647();
void AutoGeneratedExpander_49648();
void AutoGeneratedExpander_49649();
void AutoGeneratedExpander_49650();
void AutoGeneratedExpander_49651();
void AutoGeneratedExpander_49652();
void AutoGeneratedExpander_49653();
void AutoGeneratedExpander_49654();
void AutoGeneratedExpander_49655();
void AutoGeneratedExpander_49656();
void AutoGeneratedExpander_49657();
void AutoGeneratedExpander_49658();
void AutoGeneratedExpander_49659();
void AutoGeneratedExpander_49660();
void AutoGeneratedExpander_49661();
void AutoGeneratedExpander_49662();
void AutoGeneratedExpander_49663();
void AutoGeneratedExpander_49664();
void AutoGeneratedExpander_49665();
void AutoGeneratedExpander_49666();
void AutoGeneratedExpander_49667();
void AutoGeneratedExpander_49668();
void AutoGeneratedExpander_49669();
void AutoGeneratedExpander_49670();
void AutoGeneratedExpander_49671();
void AutoGeneratedExpander_49672();
void AutoGeneratedExpander_49673();
void AutoGeneratedExpander_49674();
void AutoGeneratedExpander_49675();
void AutoGeneratedExpander_49676();
void AutoGeneratedExpander_49677();
void AutoGeneratedExpander_49678();
void AutoGeneratedExpander_49679();
void AutoGeneratedExpander_49680();
void AutoGeneratedExpander_49681();
void AutoGeneratedExpander_49682();
void AutoGeneratedExpander_49683();
void AutoGeneratedExpander_49684();
void AutoGeneratedExpander_49685();
void AutoGeneratedExpander_49686();
void AutoGeneratedExpander_49687();
void AutoGeneratedExpander_49688();
void AutoGeneratedExpander_49689();
void AutoGeneratedExpander_49690();
void AutoGeneratedExpander_49691();
void AutoGeneratedExpander_49692();
void AutoGeneratedExpander_49693();
void AutoGeneratedExpander_49694();
void AutoGeneratedExpander_49695();
void AutoGeneratedExpander_49696();
void AutoGeneratedExpander_49697();
void AutoGeneratedExpander_49698();
void AutoGeneratedExpander_49699();
void AutoGeneratedExpander_49700();
void AutoGeneratedExpander_49701();
void AutoGeneratedExpander_49702();
void AutoGeneratedExpander_49703();
void AutoGeneratedExpander_49704();
void AutoGeneratedExpander_49705();
void AutoGeneratedExpander_49706();
void AutoGeneratedExpander_49707();
void AutoGeneratedExpander_49708();
void AutoGeneratedExpander_49709();
void AutoGeneratedExpander_49710();
void AutoGeneratedExpander_49711();
void AutoGeneratedExpander_49712();
void AutoGeneratedExpander_49713();
void AutoGeneratedExpander_49714();
void WEIGHTED_ROUND_ROBIN_Joiner_49634();
void Identity_49397();
void Pre_CollapsedDataParallel_2_49500();
void WEIGHTED_ROUND_ROBIN_Joiner_49504();
void WEIGHTED_ROUND_ROBIN_Splitter_49715();
void BlockMultiply(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockMultiply_49717();
void BlockMultiply_49718();
void BlockMultiply_49719();
void BlockMultiply_49720();
void BlockMultiply_49721();
void BlockMultiply_49722();
void BlockMultiply_49723();
void BlockMultiply_49724();
void BlockMultiply_49725();
void BlockMultiply_49726();
void BlockMultiply_49727();
void BlockMultiply_49728();
void BlockMultiply_49729();
void BlockMultiply_49730();
void BlockMultiply_49731();
void BlockMultiply_49732();
void BlockMultiply_49733();
void BlockMultiply_49734();
void BlockMultiply_49735();
void BlockMultiply_49736();
void BlockMultiply_49737();
void BlockMultiply_49738();
void BlockMultiply_49739();
void BlockMultiply_49740();
void BlockMultiply_49741();
void BlockMultiply_49742();
void BlockMultiply_49743();
void WEIGHTED_ROUND_ROBIN_Joiner_49716();
void BlockAdd(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockAdd_49401();
void WEIGHTED_ROUND_ROBIN_Splitter_49744();
void Post_CollapsedDataParallel_1_49746();
void Post_CollapsedDataParallel_1_49747();
void Post_CollapsedDataParallel_1_49748();
void Post_CollapsedDataParallel_1_49749();
void Post_CollapsedDataParallel_1_49750();
void Post_CollapsedDataParallel_1_49751();
void Post_CollapsedDataParallel_1_49752();
void Post_CollapsedDataParallel_1_49753();
void Post_CollapsedDataParallel_1_49754();
void WEIGHTED_ROUND_ROBIN_Joiner_49745();
void Identity_49403();
void WEIGHTED_ROUND_ROBIN_Splitter_49755();
void Pre_CollapsedDataParallel_2_49757();
void Pre_CollapsedDataParallel_2_49758();
void Pre_CollapsedDataParallel_2_49759();
void WEIGHTED_ROUND_ROBIN_Joiner_49756();
void Printer(buffer_float_t *chanin);
void Printer_49407();

#ifdef __cplusplus
}
#endif
#endif
