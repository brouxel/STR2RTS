#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=52704 on the compile command line
#else
#if BUF_SIZEMAX < 52704
#error BUF_SIZEMAX too small, it must be at least 52704
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_61097_t;

typedef struct {
	float result[3][4];
} BlockAdd_61124_t;
void BlockFloatSource(buffer_float_t *chanout);
void BlockFloatSource_61097();
void WEIGHTED_ROUND_ROBIN_Splitter_61226();
void WEIGHTED_ROUND_ROBIN_Splitter_61234();
void Post_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_1_61236();
void Post_CollapsedDataParallel_1_61237();
void Post_CollapsedDataParallel_1_61238();
void Post_CollapsedDataParallel_1_61239();
void Post_CollapsedDataParallel_1_61240();
void Post_CollapsedDataParallel_1_61241();
void Post_CollapsedDataParallel_1_61242();
void Post_CollapsedDataParallel_1_61243();
void Post_CollapsedDataParallel_1_61244();
void Post_CollapsedDataParallel_1_61245();
void Post_CollapsedDataParallel_1_61246();
void Post_CollapsedDataParallel_1_61247();
void WEIGHTED_ROUND_ROBIN_Joiner_61235();
void Identity(buffer_float_t *chanin, buffer_float_t *chanout);
void Identity_61101();
void WEIGHTED_ROUND_ROBIN_Splitter_61248();
void Pre_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_2_61250();
void Pre_CollapsedDataParallel_2_61251();
void Pre_CollapsedDataParallel_2_61252();
void WEIGHTED_ROUND_ROBIN_Joiner_61249();
void WEIGHTED_ROUND_ROBIN_Splitter_61253();
void AutoGeneratedExpander(buffer_float_t *chanin, buffer_float_t *chanout);
void AutoGeneratedExpander_61255();
void AutoGeneratedExpander_61256();
void AutoGeneratedExpander_61257();
void AutoGeneratedExpander_61258();
void AutoGeneratedExpander_61259();
void AutoGeneratedExpander_61260();
void AutoGeneratedExpander_61261();
void AutoGeneratedExpander_61262();
void AutoGeneratedExpander_61263();
void AutoGeneratedExpander_61264();
void AutoGeneratedExpander_61265();
void AutoGeneratedExpander_61266();
void AutoGeneratedExpander_61267();
void AutoGeneratedExpander_61268();
void AutoGeneratedExpander_61269();
void AutoGeneratedExpander_61270();
void AutoGeneratedExpander_61271();
void AutoGeneratedExpander_61272();
void AutoGeneratedExpander_61273();
void AutoGeneratedExpander_61274();
void AutoGeneratedExpander_61275();
void AutoGeneratedExpander_61276();
void AutoGeneratedExpander_61277();
void AutoGeneratedExpander_61278();
void AutoGeneratedExpander_61279();
void AutoGeneratedExpander_61280();
void AutoGeneratedExpander_61281();
void AutoGeneratedExpander_61282();
void AutoGeneratedExpander_61283();
void AutoGeneratedExpander_61284();
void AutoGeneratedExpander_61285();
void AutoGeneratedExpander_61286();
void AutoGeneratedExpander_61287();
void AutoGeneratedExpander_61288();
void AutoGeneratedExpander_61289();
void AutoGeneratedExpander_61290();
void AutoGeneratedExpander_61291();
void AutoGeneratedExpander_61292();
void AutoGeneratedExpander_61293();
void AutoGeneratedExpander_61294();
void AutoGeneratedExpander_61295();
void AutoGeneratedExpander_61296();
void AutoGeneratedExpander_61297();
void AutoGeneratedExpander_61298();
void AutoGeneratedExpander_61299();
void AutoGeneratedExpander_61300();
void AutoGeneratedExpander_61301();
void AutoGeneratedExpander_61302();
void AutoGeneratedExpander_61303();
void AutoGeneratedExpander_61304();
void AutoGeneratedExpander_61305();
void AutoGeneratedExpander_61306();
void AutoGeneratedExpander_61307();
void AutoGeneratedExpander_61308();
void AutoGeneratedExpander_61309();
void AutoGeneratedExpander_61310();
void AutoGeneratedExpander_61311();
void AutoGeneratedExpander_61312();
void AutoGeneratedExpander_61313();
void AutoGeneratedExpander_61314();
void AutoGeneratedExpander_61315();
void WEIGHTED_ROUND_ROBIN_Joiner_61254();
void Identity_61104();
void WEIGHTED_ROUND_ROBIN_Splitter_61316();
void Pre_CollapsedDataParallel_2_61318();
void Pre_CollapsedDataParallel_2_61319();
void Pre_CollapsedDataParallel_2_61320();
void WEIGHTED_ROUND_ROBIN_Joiner_61317();
void Identity_61108();
void Pre_CollapsedDataParallel_2_61220();
void WEIGHTED_ROUND_ROBIN_Splitter_61321();
void Post_CollapsedDataParallel_1_61323();
void Post_CollapsedDataParallel_1_61324();
void Post_CollapsedDataParallel_1_61325();
void Post_CollapsedDataParallel_1_61326();
void Post_CollapsedDataParallel_1_61327();
void Post_CollapsedDataParallel_1_61328();
void Post_CollapsedDataParallel_1_61329();
void Post_CollapsedDataParallel_1_61330();
void Post_CollapsedDataParallel_1_61331();
void WEIGHTED_ROUND_ROBIN_Joiner_61322();
void Identity_61117();
void WEIGHTED_ROUND_ROBIN_Splitter_61332();
void Pre_CollapsedDataParallel_2_61334();
void Pre_CollapsedDataParallel_2_61335();
void Pre_CollapsedDataParallel_2_61336();
void WEIGHTED_ROUND_ROBIN_Joiner_61333();
void WEIGHTED_ROUND_ROBIN_Splitter_61337();
void AutoGeneratedExpander_61339();
void AutoGeneratedExpander_61340();
void AutoGeneratedExpander_61341();
void AutoGeneratedExpander_61342();
void AutoGeneratedExpander_61343();
void AutoGeneratedExpander_61344();
void AutoGeneratedExpander_61345();
void AutoGeneratedExpander_61346();
void AutoGeneratedExpander_61347();
void AutoGeneratedExpander_61348();
void AutoGeneratedExpander_61349();
void AutoGeneratedExpander_61350();
void AutoGeneratedExpander_61351();
void AutoGeneratedExpander_61352();
void AutoGeneratedExpander_61353();
void AutoGeneratedExpander_61354();
void AutoGeneratedExpander_61355();
void AutoGeneratedExpander_61356();
void AutoGeneratedExpander_61357();
void AutoGeneratedExpander_61358();
void AutoGeneratedExpander_61359();
void AutoGeneratedExpander_61360();
void AutoGeneratedExpander_61361();
void AutoGeneratedExpander_61362();
void AutoGeneratedExpander_61363();
void AutoGeneratedExpander_61364();
void AutoGeneratedExpander_61365();
void AutoGeneratedExpander_61366();
void AutoGeneratedExpander_61367();
void AutoGeneratedExpander_61368();
void AutoGeneratedExpander_61369();
void AutoGeneratedExpander_61370();
void AutoGeneratedExpander_61371();
void AutoGeneratedExpander_61372();
void AutoGeneratedExpander_61373();
void AutoGeneratedExpander_61374();
void AutoGeneratedExpander_61375();
void AutoGeneratedExpander_61376();
void AutoGeneratedExpander_61377();
void AutoGeneratedExpander_61378();
void AutoGeneratedExpander_61379();
void AutoGeneratedExpander_61380();
void AutoGeneratedExpander_61381();
void AutoGeneratedExpander_61382();
void AutoGeneratedExpander_61383();
void AutoGeneratedExpander_61384();
void AutoGeneratedExpander_61385();
void AutoGeneratedExpander_61386();
void AutoGeneratedExpander_61387();
void AutoGeneratedExpander_61388();
void AutoGeneratedExpander_61389();
void AutoGeneratedExpander_61390();
void AutoGeneratedExpander_61391();
void AutoGeneratedExpander_61392();
void AutoGeneratedExpander_61393();
void AutoGeneratedExpander_61394();
void AutoGeneratedExpander_61395();
void AutoGeneratedExpander_61396();
void AutoGeneratedExpander_61397();
void AutoGeneratedExpander_61398();
void AutoGeneratedExpander_61399();
void WEIGHTED_ROUND_ROBIN_Joiner_61338();
void Identity_61120();
void Pre_CollapsedDataParallel_2_61223();
void WEIGHTED_ROUND_ROBIN_Joiner_61227();
void WEIGHTED_ROUND_ROBIN_Splitter_61400();
void BlockMultiply(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockMultiply_61402();
void BlockMultiply_61403();
void BlockMultiply_61404();
void BlockMultiply_61405();
void BlockMultiply_61406();
void BlockMultiply_61407();
void BlockMultiply_61408();
void BlockMultiply_61409();
void BlockMultiply_61410();
void BlockMultiply_61411();
void BlockMultiply_61412();
void BlockMultiply_61413();
void BlockMultiply_61414();
void BlockMultiply_61415();
void BlockMultiply_61416();
void BlockMultiply_61417();
void BlockMultiply_61418();
void BlockMultiply_61419();
void BlockMultiply_61420();
void BlockMultiply_61421();
void BlockMultiply_61422();
void BlockMultiply_61423();
void BlockMultiply_61424();
void BlockMultiply_61425();
void BlockMultiply_61426();
void BlockMultiply_61427();
void BlockMultiply_61428();
void WEIGHTED_ROUND_ROBIN_Joiner_61401();
void BlockAdd(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockAdd_61124();
void WEIGHTED_ROUND_ROBIN_Splitter_61429();
void Post_CollapsedDataParallel_1_61431();
void Post_CollapsedDataParallel_1_61432();
void Post_CollapsedDataParallel_1_61433();
void Post_CollapsedDataParallel_1_61434();
void Post_CollapsedDataParallel_1_61435();
void Post_CollapsedDataParallel_1_61436();
void Post_CollapsedDataParallel_1_61437();
void Post_CollapsedDataParallel_1_61438();
void Post_CollapsedDataParallel_1_61439();
void WEIGHTED_ROUND_ROBIN_Joiner_61430();
void Identity_61126();
void WEIGHTED_ROUND_ROBIN_Splitter_61440();
void Pre_CollapsedDataParallel_2_61442();
void Pre_CollapsedDataParallel_2_61443();
void Pre_CollapsedDataParallel_2_61444();
void WEIGHTED_ROUND_ROBIN_Joiner_61441();
void Printer(buffer_float_t *chanin);
void Printer_61130();

#ifdef __cplusplus
}
#endif
#endif
