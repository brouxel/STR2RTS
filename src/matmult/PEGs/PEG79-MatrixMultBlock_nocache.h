#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=68256 on the compile command line
#else
#if BUF_SIZEMAX < 68256
#error BUF_SIZEMAX too small, it must be at least 68256
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_50027_t;

typedef struct {
	float result[3][4];
} BlockAdd_50054_t;
void BlockFloatSource_50027();
void WEIGHTED_ROUND_ROBIN_Splitter_50156();
void WEIGHTED_ROUND_ROBIN_Splitter_50164();
void Post_CollapsedDataParallel_1_50166();
void Post_CollapsedDataParallel_1_50167();
void Post_CollapsedDataParallel_1_50168();
void Post_CollapsedDataParallel_1_50169();
void Post_CollapsedDataParallel_1_50170();
void Post_CollapsedDataParallel_1_50171();
void Post_CollapsedDataParallel_1_50172();
void Post_CollapsedDataParallel_1_50173();
void Post_CollapsedDataParallel_1_50174();
void Post_CollapsedDataParallel_1_50175();
void Post_CollapsedDataParallel_1_50176();
void Post_CollapsedDataParallel_1_50177();
void WEIGHTED_ROUND_ROBIN_Joiner_50165();
void Identity_50031();
void WEIGHTED_ROUND_ROBIN_Splitter_50178();
void Pre_CollapsedDataParallel_2_50180();
void Pre_CollapsedDataParallel_2_50181();
void Pre_CollapsedDataParallel_2_50182();
void WEIGHTED_ROUND_ROBIN_Joiner_50179();
void WEIGHTED_ROUND_ROBIN_Splitter_50183();
void AutoGeneratedExpander_50185();
void AutoGeneratedExpander_50186();
void AutoGeneratedExpander_50187();
void AutoGeneratedExpander_50188();
void AutoGeneratedExpander_50189();
void AutoGeneratedExpander_50190();
void AutoGeneratedExpander_50191();
void AutoGeneratedExpander_50192();
void AutoGeneratedExpander_50193();
void AutoGeneratedExpander_50194();
void AutoGeneratedExpander_50195();
void AutoGeneratedExpander_50196();
void AutoGeneratedExpander_50197();
void AutoGeneratedExpander_50198();
void AutoGeneratedExpander_50199();
void AutoGeneratedExpander_50200();
void AutoGeneratedExpander_50201();
void AutoGeneratedExpander_50202();
void AutoGeneratedExpander_50203();
void AutoGeneratedExpander_50204();
void AutoGeneratedExpander_50205();
void AutoGeneratedExpander_50206();
void AutoGeneratedExpander_50207();
void AutoGeneratedExpander_50208();
void AutoGeneratedExpander_50209();
void AutoGeneratedExpander_50210();
void AutoGeneratedExpander_50211();
void AutoGeneratedExpander_50212();
void AutoGeneratedExpander_50213();
void AutoGeneratedExpander_50214();
void AutoGeneratedExpander_50215();
void AutoGeneratedExpander_50216();
void AutoGeneratedExpander_50217();
void AutoGeneratedExpander_50218();
void AutoGeneratedExpander_50219();
void AutoGeneratedExpander_50220();
void AutoGeneratedExpander_50221();
void AutoGeneratedExpander_50222();
void AutoGeneratedExpander_50223();
void AutoGeneratedExpander_50224();
void AutoGeneratedExpander_50225();
void AutoGeneratedExpander_50226();
void AutoGeneratedExpander_50227();
void AutoGeneratedExpander_50228();
void AutoGeneratedExpander_50229();
void AutoGeneratedExpander_50230();
void AutoGeneratedExpander_50231();
void AutoGeneratedExpander_50232();
void AutoGeneratedExpander_50233();
void AutoGeneratedExpander_50234();
void AutoGeneratedExpander_50235();
void AutoGeneratedExpander_50236();
void AutoGeneratedExpander_50237();
void AutoGeneratedExpander_50238();
void AutoGeneratedExpander_50239();
void AutoGeneratedExpander_50240();
void AutoGeneratedExpander_50241();
void AutoGeneratedExpander_50242();
void AutoGeneratedExpander_50243();
void AutoGeneratedExpander_50244();
void AutoGeneratedExpander_50245();
void AutoGeneratedExpander_50246();
void AutoGeneratedExpander_50247();
void AutoGeneratedExpander_50248();
void AutoGeneratedExpander_50249();
void AutoGeneratedExpander_50250();
void AutoGeneratedExpander_50251();
void AutoGeneratedExpander_50252();
void AutoGeneratedExpander_50253();
void AutoGeneratedExpander_50254();
void AutoGeneratedExpander_50255();
void AutoGeneratedExpander_50256();
void AutoGeneratedExpander_50257();
void AutoGeneratedExpander_50258();
void AutoGeneratedExpander_50259();
void AutoGeneratedExpander_50260();
void AutoGeneratedExpander_50261();
void AutoGeneratedExpander_50262();
void AutoGeneratedExpander_50263();
void WEIGHTED_ROUND_ROBIN_Joiner_50184();
void Identity_50034();
void WEIGHTED_ROUND_ROBIN_Splitter_50264();
void Pre_CollapsedDataParallel_2_50266();
void Pre_CollapsedDataParallel_2_50267();
void Pre_CollapsedDataParallel_2_50268();
void WEIGHTED_ROUND_ROBIN_Joiner_50265();
void Identity_50038();
void Pre_CollapsedDataParallel_2_50150();
void WEIGHTED_ROUND_ROBIN_Splitter_50269();
void Post_CollapsedDataParallel_1_50271();
void Post_CollapsedDataParallel_1_50272();
void Post_CollapsedDataParallel_1_50273();
void Post_CollapsedDataParallel_1_50274();
void Post_CollapsedDataParallel_1_50275();
void Post_CollapsedDataParallel_1_50276();
void Post_CollapsedDataParallel_1_50277();
void Post_CollapsedDataParallel_1_50278();
void Post_CollapsedDataParallel_1_50279();
void WEIGHTED_ROUND_ROBIN_Joiner_50270();
void Identity_50047();
void WEIGHTED_ROUND_ROBIN_Splitter_50280();
void Pre_CollapsedDataParallel_2_50282();
void Pre_CollapsedDataParallel_2_50283();
void Pre_CollapsedDataParallel_2_50284();
void WEIGHTED_ROUND_ROBIN_Joiner_50281();
void WEIGHTED_ROUND_ROBIN_Splitter_50285();
void AutoGeneratedExpander_50287();
void AutoGeneratedExpander_50288();
void AutoGeneratedExpander_50289();
void AutoGeneratedExpander_50290();
void AutoGeneratedExpander_50291();
void AutoGeneratedExpander_50292();
void AutoGeneratedExpander_50293();
void AutoGeneratedExpander_50294();
void AutoGeneratedExpander_50295();
void AutoGeneratedExpander_50296();
void AutoGeneratedExpander_50297();
void AutoGeneratedExpander_50298();
void AutoGeneratedExpander_50299();
void AutoGeneratedExpander_50300();
void AutoGeneratedExpander_50301();
void AutoGeneratedExpander_50302();
void AutoGeneratedExpander_50303();
void AutoGeneratedExpander_50304();
void AutoGeneratedExpander_50305();
void AutoGeneratedExpander_50306();
void AutoGeneratedExpander_50307();
void AutoGeneratedExpander_50308();
void AutoGeneratedExpander_50309();
void AutoGeneratedExpander_50310();
void AutoGeneratedExpander_50311();
void AutoGeneratedExpander_50312();
void AutoGeneratedExpander_50313();
void AutoGeneratedExpander_50314();
void AutoGeneratedExpander_50315();
void AutoGeneratedExpander_50316();
void AutoGeneratedExpander_50317();
void AutoGeneratedExpander_50318();
void AutoGeneratedExpander_50319();
void AutoGeneratedExpander_50320();
void AutoGeneratedExpander_50321();
void AutoGeneratedExpander_50322();
void AutoGeneratedExpander_50323();
void AutoGeneratedExpander_50324();
void AutoGeneratedExpander_50325();
void AutoGeneratedExpander_50326();
void AutoGeneratedExpander_50327();
void AutoGeneratedExpander_50328();
void AutoGeneratedExpander_50329();
void AutoGeneratedExpander_50330();
void AutoGeneratedExpander_50331();
void AutoGeneratedExpander_50332();
void AutoGeneratedExpander_50333();
void AutoGeneratedExpander_50334();
void AutoGeneratedExpander_50335();
void AutoGeneratedExpander_50336();
void AutoGeneratedExpander_50337();
void AutoGeneratedExpander_50338();
void AutoGeneratedExpander_50339();
void AutoGeneratedExpander_50340();
void AutoGeneratedExpander_50341();
void AutoGeneratedExpander_50342();
void AutoGeneratedExpander_50343();
void AutoGeneratedExpander_50344();
void AutoGeneratedExpander_50345();
void AutoGeneratedExpander_50346();
void AutoGeneratedExpander_50347();
void AutoGeneratedExpander_50348();
void AutoGeneratedExpander_50349();
void AutoGeneratedExpander_50350();
void AutoGeneratedExpander_50351();
void AutoGeneratedExpander_50352();
void AutoGeneratedExpander_50353();
void AutoGeneratedExpander_50354();
void AutoGeneratedExpander_50355();
void AutoGeneratedExpander_50356();
void AutoGeneratedExpander_50357();
void AutoGeneratedExpander_50358();
void AutoGeneratedExpander_50359();
void AutoGeneratedExpander_50360();
void AutoGeneratedExpander_50361();
void AutoGeneratedExpander_50362();
void AutoGeneratedExpander_50363();
void AutoGeneratedExpander_50364();
void AutoGeneratedExpander_50365();
void WEIGHTED_ROUND_ROBIN_Joiner_50286();
void Identity_50050();
void Pre_CollapsedDataParallel_2_50153();
void WEIGHTED_ROUND_ROBIN_Joiner_50157();
void WEIGHTED_ROUND_ROBIN_Splitter_50366();
void BlockMultiply_50368();
void BlockMultiply_50369();
void BlockMultiply_50370();
void BlockMultiply_50371();
void BlockMultiply_50372();
void BlockMultiply_50373();
void BlockMultiply_50374();
void BlockMultiply_50375();
void BlockMultiply_50376();
void BlockMultiply_50377();
void BlockMultiply_50378();
void BlockMultiply_50379();
void BlockMultiply_50380();
void BlockMultiply_50381();
void BlockMultiply_50382();
void BlockMultiply_50383();
void BlockMultiply_50384();
void BlockMultiply_50385();
void BlockMultiply_50386();
void BlockMultiply_50387();
void BlockMultiply_50388();
void BlockMultiply_50389();
void BlockMultiply_50390();
void BlockMultiply_50391();
void BlockMultiply_50392();
void BlockMultiply_50393();
void BlockMultiply_50394();
void WEIGHTED_ROUND_ROBIN_Joiner_50367();
void BlockAdd_50054();
void WEIGHTED_ROUND_ROBIN_Splitter_50395();
void Post_CollapsedDataParallel_1_50397();
void Post_CollapsedDataParallel_1_50398();
void Post_CollapsedDataParallel_1_50399();
void Post_CollapsedDataParallel_1_50400();
void Post_CollapsedDataParallel_1_50401();
void Post_CollapsedDataParallel_1_50402();
void Post_CollapsedDataParallel_1_50403();
void Post_CollapsedDataParallel_1_50404();
void Post_CollapsedDataParallel_1_50405();
void WEIGHTED_ROUND_ROBIN_Joiner_50396();
void Identity_50056();
void WEIGHTED_ROUND_ROBIN_Splitter_50406();
void Pre_CollapsedDataParallel_2_50408();
void Pre_CollapsedDataParallel_2_50409();
void Pre_CollapsedDataParallel_2_50410();
void WEIGHTED_ROUND_ROBIN_Joiner_50407();
void Printer_50060();

#ifdef __cplusplus
}
#endif
#endif
