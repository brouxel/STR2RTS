#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=6048 on the compile command line
#else
#if BUF_SIZEMAX < 6048
#error BUF_SIZEMAX too small, it must be at least 6048
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_59931_t;

typedef struct {
	float result[3][4];
} BlockAdd_59958_t;
void BlockFloatSource_59931();
void WEIGHTED_ROUND_ROBIN_Splitter_60060();
void WEIGHTED_ROUND_ROBIN_Splitter_60068();
void Post_CollapsedDataParallel_1_60070();
void Post_CollapsedDataParallel_1_60071();
void Post_CollapsedDataParallel_1_60072();
void Post_CollapsedDataParallel_1_60073();
void Post_CollapsedDataParallel_1_60074();
void Post_CollapsedDataParallel_1_60075();
void Post_CollapsedDataParallel_1_60076();
void Post_CollapsedDataParallel_1_60077();
void Post_CollapsedDataParallel_1_60078();
void Post_CollapsedDataParallel_1_60079();
void Post_CollapsedDataParallel_1_60080();
void Post_CollapsedDataParallel_1_60081();
void WEIGHTED_ROUND_ROBIN_Joiner_60069();
void Identity_59935();
void WEIGHTED_ROUND_ROBIN_Splitter_60082();
void Pre_CollapsedDataParallel_2_60084();
void Pre_CollapsedDataParallel_2_60085();
void Pre_CollapsedDataParallel_2_60086();
void WEIGHTED_ROUND_ROBIN_Joiner_60083();
void WEIGHTED_ROUND_ROBIN_Splitter_60087();
void AutoGeneratedExpander_60089();
void AutoGeneratedExpander_60090();
void AutoGeneratedExpander_60091();
void AutoGeneratedExpander_60092();
void AutoGeneratedExpander_60093();
void AutoGeneratedExpander_60094();
void AutoGeneratedExpander_60095();
void AutoGeneratedExpander_60096();
void AutoGeneratedExpander_60097();
void AutoGeneratedExpander_60098();
void AutoGeneratedExpander_60099();
void AutoGeneratedExpander_60100();
void AutoGeneratedExpander_60101();
void AutoGeneratedExpander_60102();
void AutoGeneratedExpander_60103();
void AutoGeneratedExpander_60104();
void AutoGeneratedExpander_60105();
void AutoGeneratedExpander_60106();
void AutoGeneratedExpander_60107();
void AutoGeneratedExpander_60108();
void AutoGeneratedExpander_60109();
void AutoGeneratedExpander_60110();
void AutoGeneratedExpander_60111();
void AutoGeneratedExpander_60112();
void AutoGeneratedExpander_60113();
void AutoGeneratedExpander_60114();
void AutoGeneratedExpander_60115();
void AutoGeneratedExpander_60116();
void AutoGeneratedExpander_60117();
void AutoGeneratedExpander_60118();
void AutoGeneratedExpander_60119();
void AutoGeneratedExpander_60120();
void AutoGeneratedExpander_60121();
void AutoGeneratedExpander_60122();
void AutoGeneratedExpander_60123();
void AutoGeneratedExpander_60124();
void AutoGeneratedExpander_60125();
void AutoGeneratedExpander_60126();
void AutoGeneratedExpander_60127();
void AutoGeneratedExpander_60128();
void AutoGeneratedExpander_60129();
void AutoGeneratedExpander_60130();
void AutoGeneratedExpander_60131();
void AutoGeneratedExpander_60132();
void AutoGeneratedExpander_60133();
void AutoGeneratedExpander_60134();
void AutoGeneratedExpander_60135();
void AutoGeneratedExpander_60136();
void AutoGeneratedExpander_60137();
void AutoGeneratedExpander_60138();
void AutoGeneratedExpander_60139();
void AutoGeneratedExpander_60140();
void AutoGeneratedExpander_60141();
void AutoGeneratedExpander_60142();
void AutoGeneratedExpander_60143();
void AutoGeneratedExpander_60144();
void AutoGeneratedExpander_60145();
void AutoGeneratedExpander_60146();
void AutoGeneratedExpander_60147();
void AutoGeneratedExpander_60148();
void AutoGeneratedExpander_60149();
void AutoGeneratedExpander_60150();
void AutoGeneratedExpander_60151();
void WEIGHTED_ROUND_ROBIN_Joiner_60088();
void Identity_59938();
void WEIGHTED_ROUND_ROBIN_Splitter_60152();
void Pre_CollapsedDataParallel_2_60154();
void Pre_CollapsedDataParallel_2_60155();
void Pre_CollapsedDataParallel_2_60156();
void WEIGHTED_ROUND_ROBIN_Joiner_60153();
void Identity_59942();
void Pre_CollapsedDataParallel_2_60054();
void WEIGHTED_ROUND_ROBIN_Splitter_60157();
void Post_CollapsedDataParallel_1_60159();
void Post_CollapsedDataParallel_1_60160();
void Post_CollapsedDataParallel_1_60161();
void Post_CollapsedDataParallel_1_60162();
void Post_CollapsedDataParallel_1_60163();
void Post_CollapsedDataParallel_1_60164();
void Post_CollapsedDataParallel_1_60165();
void Post_CollapsedDataParallel_1_60166();
void Post_CollapsedDataParallel_1_60167();
void WEIGHTED_ROUND_ROBIN_Joiner_60158();
void Identity_59951();
void WEIGHTED_ROUND_ROBIN_Splitter_60168();
void Pre_CollapsedDataParallel_2_60170();
void Pre_CollapsedDataParallel_2_60171();
void Pre_CollapsedDataParallel_2_60172();
void WEIGHTED_ROUND_ROBIN_Joiner_60169();
void WEIGHTED_ROUND_ROBIN_Splitter_60173();
void AutoGeneratedExpander_60175();
void AutoGeneratedExpander_60176();
void AutoGeneratedExpander_60177();
void AutoGeneratedExpander_60178();
void AutoGeneratedExpander_60179();
void AutoGeneratedExpander_60180();
void AutoGeneratedExpander_60181();
void AutoGeneratedExpander_60182();
void AutoGeneratedExpander_60183();
void AutoGeneratedExpander_60184();
void AutoGeneratedExpander_60185();
void AutoGeneratedExpander_60186();
void AutoGeneratedExpander_60187();
void AutoGeneratedExpander_60188();
void AutoGeneratedExpander_60189();
void AutoGeneratedExpander_60190();
void AutoGeneratedExpander_60191();
void AutoGeneratedExpander_60192();
void AutoGeneratedExpander_60193();
void AutoGeneratedExpander_60194();
void AutoGeneratedExpander_60195();
void AutoGeneratedExpander_60196();
void AutoGeneratedExpander_60197();
void AutoGeneratedExpander_60198();
void AutoGeneratedExpander_60199();
void AutoGeneratedExpander_60200();
void AutoGeneratedExpander_60201();
void AutoGeneratedExpander_60202();
void AutoGeneratedExpander_60203();
void AutoGeneratedExpander_60204();
void AutoGeneratedExpander_60205();
void AutoGeneratedExpander_60206();
void AutoGeneratedExpander_60207();
void AutoGeneratedExpander_60208();
void AutoGeneratedExpander_60209();
void AutoGeneratedExpander_60210();
void AutoGeneratedExpander_60211();
void AutoGeneratedExpander_60212();
void AutoGeneratedExpander_60213();
void AutoGeneratedExpander_60214();
void AutoGeneratedExpander_60215();
void AutoGeneratedExpander_60216();
void AutoGeneratedExpander_60217();
void AutoGeneratedExpander_60218();
void AutoGeneratedExpander_60219();
void AutoGeneratedExpander_60220();
void AutoGeneratedExpander_60221();
void AutoGeneratedExpander_60222();
void AutoGeneratedExpander_60223();
void AutoGeneratedExpander_60224();
void AutoGeneratedExpander_60225();
void AutoGeneratedExpander_60226();
void AutoGeneratedExpander_60227();
void AutoGeneratedExpander_60228();
void AutoGeneratedExpander_60229();
void AutoGeneratedExpander_60230();
void AutoGeneratedExpander_60231();
void AutoGeneratedExpander_60232();
void AutoGeneratedExpander_60233();
void AutoGeneratedExpander_60234();
void AutoGeneratedExpander_60235();
void AutoGeneratedExpander_60236();
void AutoGeneratedExpander_60237();
void WEIGHTED_ROUND_ROBIN_Joiner_60174();
void Identity_59954();
void Pre_CollapsedDataParallel_2_60057();
void WEIGHTED_ROUND_ROBIN_Joiner_60061();
void WEIGHTED_ROUND_ROBIN_Splitter_60238();
void BlockMultiply_60240();
void BlockMultiply_60241();
void BlockMultiply_60242();
void BlockMultiply_60243();
void BlockMultiply_60244();
void BlockMultiply_60245();
void BlockMultiply_60246();
void BlockMultiply_60247();
void BlockMultiply_60248();
void BlockMultiply_60249();
void BlockMultiply_60250();
void BlockMultiply_60251();
void BlockMultiply_60252();
void BlockMultiply_60253();
void BlockMultiply_60254();
void BlockMultiply_60255();
void BlockMultiply_60256();
void BlockMultiply_60257();
void BlockMultiply_60258();
void BlockMultiply_60259();
void BlockMultiply_60260();
void BlockMultiply_60261();
void BlockMultiply_60262();
void BlockMultiply_60263();
void BlockMultiply_60264();
void BlockMultiply_60265();
void BlockMultiply_60266();
void WEIGHTED_ROUND_ROBIN_Joiner_60239();
void BlockAdd_59958();
void WEIGHTED_ROUND_ROBIN_Splitter_60267();
void Post_CollapsedDataParallel_1_60269();
void Post_CollapsedDataParallel_1_60270();
void Post_CollapsedDataParallel_1_60271();
void Post_CollapsedDataParallel_1_60272();
void Post_CollapsedDataParallel_1_60273();
void Post_CollapsedDataParallel_1_60274();
void Post_CollapsedDataParallel_1_60275();
void Post_CollapsedDataParallel_1_60276();
void Post_CollapsedDataParallel_1_60277();
void WEIGHTED_ROUND_ROBIN_Joiner_60268();
void Identity_59960();
void WEIGHTED_ROUND_ROBIN_Splitter_60278();
void Pre_CollapsedDataParallel_2_60280();
void Pre_CollapsedDataParallel_2_60281();
void Pre_CollapsedDataParallel_2_60282();
void WEIGHTED_ROUND_ROBIN_Joiner_60279();
void Printer_59964();

#ifdef __cplusplus
}
#endif
#endif
