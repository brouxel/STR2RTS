#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=21600 on the compile command line
#else
#if BUF_SIZEMAX < 21600
#error BUF_SIZEMAX too small, it must be at least 21600
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_67224_t;

typedef struct {
	float result[3][4];
} BlockAdd_67251_t;
void BlockFloatSource_67224();
void WEIGHTED_ROUND_ROBIN_Splitter_67353();
void WEIGHTED_ROUND_ROBIN_Splitter_67361();
void Post_CollapsedDataParallel_1_67363();
void Post_CollapsedDataParallel_1_67364();
void Post_CollapsedDataParallel_1_67365();
void Post_CollapsedDataParallel_1_67366();
void Post_CollapsedDataParallel_1_67367();
void Post_CollapsedDataParallel_1_67368();
void Post_CollapsedDataParallel_1_67369();
void Post_CollapsedDataParallel_1_67370();
void Post_CollapsedDataParallel_1_67371();
void Post_CollapsedDataParallel_1_67372();
void Post_CollapsedDataParallel_1_67373();
void Post_CollapsedDataParallel_1_67374();
void WEIGHTED_ROUND_ROBIN_Joiner_67362();
void Identity_67228();
void WEIGHTED_ROUND_ROBIN_Splitter_67375();
void Pre_CollapsedDataParallel_2_67377();
void Pre_CollapsedDataParallel_2_67378();
void Pre_CollapsedDataParallel_2_67379();
void WEIGHTED_ROUND_ROBIN_Joiner_67376();
void WEIGHTED_ROUND_ROBIN_Splitter_67380();
void AutoGeneratedExpander_67382();
void AutoGeneratedExpander_67383();
void AutoGeneratedExpander_67384();
void AutoGeneratedExpander_67385();
void AutoGeneratedExpander_67386();
void AutoGeneratedExpander_67387();
void AutoGeneratedExpander_67388();
void AutoGeneratedExpander_67389();
void AutoGeneratedExpander_67390();
void AutoGeneratedExpander_67391();
void AutoGeneratedExpander_67392();
void AutoGeneratedExpander_67393();
void AutoGeneratedExpander_67394();
void AutoGeneratedExpander_67395();
void AutoGeneratedExpander_67396();
void AutoGeneratedExpander_67397();
void AutoGeneratedExpander_67398();
void AutoGeneratedExpander_67399();
void AutoGeneratedExpander_67400();
void AutoGeneratedExpander_67401();
void AutoGeneratedExpander_67402();
void AutoGeneratedExpander_67403();
void AutoGeneratedExpander_67404();
void AutoGeneratedExpander_67405();
void AutoGeneratedExpander_67406();
void AutoGeneratedExpander_67407();
void AutoGeneratedExpander_67408();
void AutoGeneratedExpander_67409();
void AutoGeneratedExpander_67410();
void AutoGeneratedExpander_67411();
void AutoGeneratedExpander_67412();
void AutoGeneratedExpander_67413();
void AutoGeneratedExpander_67414();
void AutoGeneratedExpander_67415();
void AutoGeneratedExpander_67416();
void AutoGeneratedExpander_67417();
void AutoGeneratedExpander_67418();
void AutoGeneratedExpander_67419();
void AutoGeneratedExpander_67420();
void AutoGeneratedExpander_67421();
void AutoGeneratedExpander_67422();
void AutoGeneratedExpander_67423();
void AutoGeneratedExpander_67424();
void AutoGeneratedExpander_67425();
void AutoGeneratedExpander_67426();
void AutoGeneratedExpander_67427();
void AutoGeneratedExpander_67428();
void AutoGeneratedExpander_67429();
void AutoGeneratedExpander_67430();
void AutoGeneratedExpander_67431();
void WEIGHTED_ROUND_ROBIN_Joiner_67381();
void Identity_67231();
void WEIGHTED_ROUND_ROBIN_Splitter_67432();
void Pre_CollapsedDataParallel_2_67434();
void Pre_CollapsedDataParallel_2_67435();
void Pre_CollapsedDataParallel_2_67436();
void WEIGHTED_ROUND_ROBIN_Joiner_67433();
void Identity_67235();
void Pre_CollapsedDataParallel_2_67347();
void WEIGHTED_ROUND_ROBIN_Splitter_67437();
void Post_CollapsedDataParallel_1_67439();
void Post_CollapsedDataParallel_1_67440();
void Post_CollapsedDataParallel_1_67441();
void Post_CollapsedDataParallel_1_67442();
void Post_CollapsedDataParallel_1_67443();
void Post_CollapsedDataParallel_1_67444();
void Post_CollapsedDataParallel_1_67445();
void Post_CollapsedDataParallel_1_67446();
void Post_CollapsedDataParallel_1_67447();
void WEIGHTED_ROUND_ROBIN_Joiner_67438();
void Identity_67244();
void WEIGHTED_ROUND_ROBIN_Splitter_67448();
void Pre_CollapsedDataParallel_2_67450();
void Pre_CollapsedDataParallel_2_67451();
void Pre_CollapsedDataParallel_2_67452();
void WEIGHTED_ROUND_ROBIN_Joiner_67449();
void WEIGHTED_ROUND_ROBIN_Splitter_67453();
void AutoGeneratedExpander_67455();
void AutoGeneratedExpander_67456();
void AutoGeneratedExpander_67457();
void AutoGeneratedExpander_67458();
void AutoGeneratedExpander_67459();
void AutoGeneratedExpander_67460();
void AutoGeneratedExpander_67461();
void AutoGeneratedExpander_67462();
void AutoGeneratedExpander_67463();
void AutoGeneratedExpander_67464();
void AutoGeneratedExpander_67465();
void AutoGeneratedExpander_67466();
void AutoGeneratedExpander_67467();
void AutoGeneratedExpander_67468();
void AutoGeneratedExpander_67469();
void AutoGeneratedExpander_67470();
void AutoGeneratedExpander_67471();
void AutoGeneratedExpander_67472();
void AutoGeneratedExpander_67473();
void AutoGeneratedExpander_67474();
void AutoGeneratedExpander_67475();
void AutoGeneratedExpander_67476();
void AutoGeneratedExpander_67477();
void AutoGeneratedExpander_67478();
void AutoGeneratedExpander_67479();
void AutoGeneratedExpander_67480();
void AutoGeneratedExpander_67481();
void AutoGeneratedExpander_67482();
void AutoGeneratedExpander_67483();
void AutoGeneratedExpander_67484();
void AutoGeneratedExpander_67485();
void AutoGeneratedExpander_67486();
void AutoGeneratedExpander_67487();
void AutoGeneratedExpander_67488();
void AutoGeneratedExpander_67489();
void AutoGeneratedExpander_67490();
void AutoGeneratedExpander_67491();
void AutoGeneratedExpander_67492();
void AutoGeneratedExpander_67493();
void AutoGeneratedExpander_67494();
void AutoGeneratedExpander_67495();
void AutoGeneratedExpander_67496();
void AutoGeneratedExpander_67497();
void AutoGeneratedExpander_67498();
void AutoGeneratedExpander_67499();
void AutoGeneratedExpander_67500();
void AutoGeneratedExpander_67501();
void AutoGeneratedExpander_67502();
void AutoGeneratedExpander_67503();
void AutoGeneratedExpander_67504();
void WEIGHTED_ROUND_ROBIN_Joiner_67454();
void Identity_67247();
void Pre_CollapsedDataParallel_2_67350();
void WEIGHTED_ROUND_ROBIN_Joiner_67354();
void WEIGHTED_ROUND_ROBIN_Splitter_67505();
void BlockMultiply_67507();
void BlockMultiply_67508();
void BlockMultiply_67509();
void BlockMultiply_67510();
void BlockMultiply_67511();
void BlockMultiply_67512();
void BlockMultiply_67513();
void BlockMultiply_67514();
void BlockMultiply_67515();
void BlockMultiply_67516();
void BlockMultiply_67517();
void BlockMultiply_67518();
void BlockMultiply_67519();
void BlockMultiply_67520();
void BlockMultiply_67521();
void BlockMultiply_67522();
void BlockMultiply_67523();
void BlockMultiply_67524();
void BlockMultiply_67525();
void BlockMultiply_67526();
void BlockMultiply_67527();
void BlockMultiply_67528();
void BlockMultiply_67529();
void BlockMultiply_67530();
void BlockMultiply_67531();
void BlockMultiply_67532();
void BlockMultiply_67533();
void WEIGHTED_ROUND_ROBIN_Joiner_67506();
void BlockAdd_67251();
void WEIGHTED_ROUND_ROBIN_Splitter_67534();
void Post_CollapsedDataParallel_1_67536();
void Post_CollapsedDataParallel_1_67537();
void Post_CollapsedDataParallel_1_67538();
void Post_CollapsedDataParallel_1_67539();
void Post_CollapsedDataParallel_1_67540();
void Post_CollapsedDataParallel_1_67541();
void Post_CollapsedDataParallel_1_67542();
void Post_CollapsedDataParallel_1_67543();
void Post_CollapsedDataParallel_1_67544();
void WEIGHTED_ROUND_ROBIN_Joiner_67535();
void Identity_67253();
void WEIGHTED_ROUND_ROBIN_Splitter_67545();
void Pre_CollapsedDataParallel_2_67547();
void Pre_CollapsedDataParallel_2_67548();
void Pre_CollapsedDataParallel_2_67549();
void WEIGHTED_ROUND_ROBIN_Joiner_67546();
void Printer_67257();

#ifdef __cplusplus
}
#endif
#endif
