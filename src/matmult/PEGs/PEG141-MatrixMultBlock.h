#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=40608 on the compile command line
#else
#if BUF_SIZEMAX < 40608
#error BUF_SIZEMAX too small, it must be at least 40608
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_3099_t;

typedef struct {
	float result[3][4];
} BlockAdd_3126_t;
void BlockFloatSource(buffer_float_t *chanout);
void BlockFloatSource_3099();
void WEIGHTED_ROUND_ROBIN_Splitter_3228();
void WEIGHTED_ROUND_ROBIN_Splitter_3236();
void Post_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_1_3238();
void Post_CollapsedDataParallel_1_3239();
void Post_CollapsedDataParallel_1_3240();
void Post_CollapsedDataParallel_1_3241();
void Post_CollapsedDataParallel_1_3242();
void Post_CollapsedDataParallel_1_3243();
void Post_CollapsedDataParallel_1_3244();
void Post_CollapsedDataParallel_1_3245();
void Post_CollapsedDataParallel_1_3246();
void Post_CollapsedDataParallel_1_3247();
void Post_CollapsedDataParallel_1_3248();
void Post_CollapsedDataParallel_1_3249();
void WEIGHTED_ROUND_ROBIN_Joiner_3237();
void Identity(buffer_float_t *chanin, buffer_float_t *chanout);
void Identity_3103();
void WEIGHTED_ROUND_ROBIN_Splitter_3250();
void Pre_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_2_3252();
void Pre_CollapsedDataParallel_2_3253();
void Pre_CollapsedDataParallel_2_3254();
void WEIGHTED_ROUND_ROBIN_Joiner_3251();
void WEIGHTED_ROUND_ROBIN_Splitter_3255();
void AutoGeneratedExpander(buffer_float_t *chanin, buffer_float_t *chanout);
void AutoGeneratedExpander_3257();
void AutoGeneratedExpander_3258();
void AutoGeneratedExpander_3259();
void AutoGeneratedExpander_3260();
void AutoGeneratedExpander_3261();
void AutoGeneratedExpander_3262();
void AutoGeneratedExpander_3263();
void AutoGeneratedExpander_3264();
void AutoGeneratedExpander_3265();
void AutoGeneratedExpander_3266();
void AutoGeneratedExpander_3267();
void AutoGeneratedExpander_3268();
void AutoGeneratedExpander_3269();
void AutoGeneratedExpander_3270();
void AutoGeneratedExpander_3271();
void AutoGeneratedExpander_3272();
void AutoGeneratedExpander_3273();
void AutoGeneratedExpander_3274();
void AutoGeneratedExpander_3275();
void AutoGeneratedExpander_3276();
void AutoGeneratedExpander_3277();
void AutoGeneratedExpander_3278();
void AutoGeneratedExpander_3279();
void AutoGeneratedExpander_3280();
void AutoGeneratedExpander_3281();
void AutoGeneratedExpander_3282();
void AutoGeneratedExpander_3283();
void AutoGeneratedExpander_3284();
void AutoGeneratedExpander_3285();
void AutoGeneratedExpander_3286();
void AutoGeneratedExpander_3287();
void AutoGeneratedExpander_3288();
void AutoGeneratedExpander_3289();
void AutoGeneratedExpander_3290();
void AutoGeneratedExpander_3291();
void AutoGeneratedExpander_3292();
void AutoGeneratedExpander_3293();
void AutoGeneratedExpander_3294();
void AutoGeneratedExpander_3295();
void AutoGeneratedExpander_3296();
void AutoGeneratedExpander_3297();
void AutoGeneratedExpander_3298();
void AutoGeneratedExpander_3299();
void AutoGeneratedExpander_3300();
void AutoGeneratedExpander_3301();
void AutoGeneratedExpander_3302();
void AutoGeneratedExpander_3303();
void AutoGeneratedExpander_3304();
void AutoGeneratedExpander_3305();
void AutoGeneratedExpander_3306();
void AutoGeneratedExpander_3307();
void AutoGeneratedExpander_3308();
void AutoGeneratedExpander_3309();
void AutoGeneratedExpander_3310();
void AutoGeneratedExpander_3311();
void AutoGeneratedExpander_3312();
void AutoGeneratedExpander_3313();
void AutoGeneratedExpander_3314();
void AutoGeneratedExpander_3315();
void AutoGeneratedExpander_3316();
void AutoGeneratedExpander_3317();
void AutoGeneratedExpander_3318();
void AutoGeneratedExpander_3319();
void AutoGeneratedExpander_3320();
void AutoGeneratedExpander_3321();
void AutoGeneratedExpander_3322();
void AutoGeneratedExpander_3323();
void AutoGeneratedExpander_3324();
void AutoGeneratedExpander_3325();
void AutoGeneratedExpander_3326();
void AutoGeneratedExpander_3327();
void AutoGeneratedExpander_3328();
void AutoGeneratedExpander_3329();
void AutoGeneratedExpander_3330();
void AutoGeneratedExpander_3331();
void AutoGeneratedExpander_3332();
void AutoGeneratedExpander_3333();
void AutoGeneratedExpander_3334();
void AutoGeneratedExpander_3335();
void AutoGeneratedExpander_3336();
void AutoGeneratedExpander_3337();
void AutoGeneratedExpander_3338();
void AutoGeneratedExpander_3339();
void AutoGeneratedExpander_3340();
void AutoGeneratedExpander_3341();
void AutoGeneratedExpander_3342();
void AutoGeneratedExpander_3343();
void AutoGeneratedExpander_3344();
void AutoGeneratedExpander_3345();
void AutoGeneratedExpander_3346();
void AutoGeneratedExpander_3347();
void AutoGeneratedExpander_3348();
void AutoGeneratedExpander_3349();
void AutoGeneratedExpander_3350();
void AutoGeneratedExpander_3351();
void AutoGeneratedExpander_3352();
void AutoGeneratedExpander_3353();
void AutoGeneratedExpander_3354();
void AutoGeneratedExpander_3355();
void AutoGeneratedExpander_3356();
void AutoGeneratedExpander_3357();
void AutoGeneratedExpander_3358();
void AutoGeneratedExpander_3359();
void AutoGeneratedExpander_3360();
void AutoGeneratedExpander_3361();
void AutoGeneratedExpander_3362();
void AutoGeneratedExpander_3363();
void AutoGeneratedExpander_3364();
void AutoGeneratedExpander_3365();
void AutoGeneratedExpander_3366();
void AutoGeneratedExpander_3367();
void AutoGeneratedExpander_3368();
void AutoGeneratedExpander_3369();
void AutoGeneratedExpander_3370();
void AutoGeneratedExpander_3371();
void AutoGeneratedExpander_3372();
void AutoGeneratedExpander_3373();
void AutoGeneratedExpander_3374();
void AutoGeneratedExpander_3375();
void AutoGeneratedExpander_3376();
void AutoGeneratedExpander_3377();
void AutoGeneratedExpander_3378();
void AutoGeneratedExpander_3379();
void AutoGeneratedExpander_3380();
void AutoGeneratedExpander_3381();
void AutoGeneratedExpander_3382();
void AutoGeneratedExpander_3383();
void AutoGeneratedExpander_3384();
void AutoGeneratedExpander_3385();
void AutoGeneratedExpander_3386();
void AutoGeneratedExpander_3387();
void AutoGeneratedExpander_3388();
void AutoGeneratedExpander_3389();
void AutoGeneratedExpander_3390();
void AutoGeneratedExpander_3391();
void AutoGeneratedExpander_3392();
void AutoGeneratedExpander_3393();
void AutoGeneratedExpander_3394();
void AutoGeneratedExpander_3395();
void AutoGeneratedExpander_3396();
void AutoGeneratedExpander_3397();
void WEIGHTED_ROUND_ROBIN_Joiner_3256();
void Identity_3106();
void WEIGHTED_ROUND_ROBIN_Splitter_3398();
void Pre_CollapsedDataParallel_2_3400();
void Pre_CollapsedDataParallel_2_3401();
void Pre_CollapsedDataParallel_2_3402();
void WEIGHTED_ROUND_ROBIN_Joiner_3399();
void Identity_3110();
void Pre_CollapsedDataParallel_2_3222();
void WEIGHTED_ROUND_ROBIN_Splitter_3403();
void Post_CollapsedDataParallel_1_3405();
void Post_CollapsedDataParallel_1_3406();
void Post_CollapsedDataParallel_1_3407();
void Post_CollapsedDataParallel_1_3408();
void Post_CollapsedDataParallel_1_3409();
void Post_CollapsedDataParallel_1_3410();
void Post_CollapsedDataParallel_1_3411();
void Post_CollapsedDataParallel_1_3412();
void Post_CollapsedDataParallel_1_3413();
void WEIGHTED_ROUND_ROBIN_Joiner_3404();
void Identity_3119();
void WEIGHTED_ROUND_ROBIN_Splitter_3414();
void Pre_CollapsedDataParallel_2_3416();
void Pre_CollapsedDataParallel_2_3417();
void Pre_CollapsedDataParallel_2_3418();
void WEIGHTED_ROUND_ROBIN_Joiner_3415();
void WEIGHTED_ROUND_ROBIN_Splitter_3419();
void AutoGeneratedExpander_3421();
void AutoGeneratedExpander_3422();
void AutoGeneratedExpander_3423();
void AutoGeneratedExpander_3424();
void AutoGeneratedExpander_3425();
void AutoGeneratedExpander_3426();
void AutoGeneratedExpander_3427();
void AutoGeneratedExpander_3428();
void AutoGeneratedExpander_3429();
void AutoGeneratedExpander_3430();
void AutoGeneratedExpander_3431();
void AutoGeneratedExpander_3432();
void AutoGeneratedExpander_3433();
void AutoGeneratedExpander_3434();
void AutoGeneratedExpander_3435();
void AutoGeneratedExpander_3436();
void AutoGeneratedExpander_3437();
void AutoGeneratedExpander_3438();
void AutoGeneratedExpander_3439();
void AutoGeneratedExpander_3440();
void AutoGeneratedExpander_3441();
void AutoGeneratedExpander_3442();
void AutoGeneratedExpander_3443();
void AutoGeneratedExpander_3444();
void AutoGeneratedExpander_3445();
void AutoGeneratedExpander_3446();
void AutoGeneratedExpander_3447();
void AutoGeneratedExpander_3448();
void AutoGeneratedExpander_3449();
void AutoGeneratedExpander_3450();
void AutoGeneratedExpander_3451();
void AutoGeneratedExpander_3452();
void AutoGeneratedExpander_3453();
void AutoGeneratedExpander_3454();
void AutoGeneratedExpander_3455();
void AutoGeneratedExpander_3456();
void AutoGeneratedExpander_3457();
void AutoGeneratedExpander_3458();
void AutoGeneratedExpander_3459();
void AutoGeneratedExpander_3460();
void AutoGeneratedExpander_3461();
void AutoGeneratedExpander_3462();
void AutoGeneratedExpander_3463();
void AutoGeneratedExpander_3464();
void AutoGeneratedExpander_3465();
void AutoGeneratedExpander_3466();
void AutoGeneratedExpander_3467();
void AutoGeneratedExpander_3468();
void AutoGeneratedExpander_3469();
void AutoGeneratedExpander_3470();
void AutoGeneratedExpander_3471();
void AutoGeneratedExpander_3472();
void AutoGeneratedExpander_3473();
void AutoGeneratedExpander_3474();
void AutoGeneratedExpander_3475();
void AutoGeneratedExpander_3476();
void AutoGeneratedExpander_3477();
void AutoGeneratedExpander_3478();
void AutoGeneratedExpander_3479();
void AutoGeneratedExpander_3480();
void AutoGeneratedExpander_3481();
void AutoGeneratedExpander_3482();
void AutoGeneratedExpander_3483();
void AutoGeneratedExpander_3484();
void AutoGeneratedExpander_3485();
void AutoGeneratedExpander_3486();
void AutoGeneratedExpander_3487();
void AutoGeneratedExpander_3488();
void AutoGeneratedExpander_3489();
void AutoGeneratedExpander_3490();
void AutoGeneratedExpander_3491();
void AutoGeneratedExpander_3492();
void AutoGeneratedExpander_3493();
void AutoGeneratedExpander_3494();
void AutoGeneratedExpander_3495();
void AutoGeneratedExpander_3496();
void AutoGeneratedExpander_3497();
void AutoGeneratedExpander_3498();
void AutoGeneratedExpander_3499();
void AutoGeneratedExpander_3500();
void AutoGeneratedExpander_3501();
void AutoGeneratedExpander_3502();
void AutoGeneratedExpander_3503();
void AutoGeneratedExpander_3504();
void AutoGeneratedExpander_3505();
void AutoGeneratedExpander_3506();
void AutoGeneratedExpander_3507();
void AutoGeneratedExpander_3508();
void AutoGeneratedExpander_3509();
void AutoGeneratedExpander_3510();
void AutoGeneratedExpander_3511();
void AutoGeneratedExpander_3512();
void AutoGeneratedExpander_3513();
void AutoGeneratedExpander_3514();
void AutoGeneratedExpander_3515();
void AutoGeneratedExpander_3516();
void AutoGeneratedExpander_3517();
void AutoGeneratedExpander_3518();
void AutoGeneratedExpander_3519();
void AutoGeneratedExpander_3520();
void AutoGeneratedExpander_3521();
void AutoGeneratedExpander_3522();
void AutoGeneratedExpander_3523();
void AutoGeneratedExpander_3524();
void AutoGeneratedExpander_3525();
void AutoGeneratedExpander_3526();
void AutoGeneratedExpander_3527();
void AutoGeneratedExpander_3528();
void WEIGHTED_ROUND_ROBIN_Joiner_3420();
void Identity_3122();
void Pre_CollapsedDataParallel_2_3225();
void WEIGHTED_ROUND_ROBIN_Joiner_3229();
void WEIGHTED_ROUND_ROBIN_Splitter_3529();
void BlockMultiply(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockMultiply_3531();
void BlockMultiply_3532();
void BlockMultiply_3533();
void BlockMultiply_3534();
void BlockMultiply_3535();
void BlockMultiply_3536();
void BlockMultiply_3537();
void BlockMultiply_3538();
void BlockMultiply_3539();
void BlockMultiply_3540();
void BlockMultiply_3541();
void BlockMultiply_3542();
void BlockMultiply_3543();
void BlockMultiply_3544();
void BlockMultiply_3545();
void BlockMultiply_3546();
void BlockMultiply_3547();
void BlockMultiply_3548();
void BlockMultiply_3549();
void BlockMultiply_3550();
void BlockMultiply_3551();
void BlockMultiply_3552();
void BlockMultiply_3553();
void BlockMultiply_3554();
void BlockMultiply_3555();
void BlockMultiply_3556();
void BlockMultiply_3557();
void WEIGHTED_ROUND_ROBIN_Joiner_3530();
void BlockAdd(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockAdd_3126();
void WEIGHTED_ROUND_ROBIN_Splitter_3558();
void Post_CollapsedDataParallel_1_3560();
void Post_CollapsedDataParallel_1_3561();
void Post_CollapsedDataParallel_1_3562();
void Post_CollapsedDataParallel_1_3563();
void Post_CollapsedDataParallel_1_3564();
void Post_CollapsedDataParallel_1_3565();
void Post_CollapsedDataParallel_1_3566();
void Post_CollapsedDataParallel_1_3567();
void Post_CollapsedDataParallel_1_3568();
void WEIGHTED_ROUND_ROBIN_Joiner_3559();
void Identity_3128();
void WEIGHTED_ROUND_ROBIN_Splitter_3569();
void Pre_CollapsedDataParallel_2_3571();
void Pre_CollapsedDataParallel_2_3572();
void Pre_CollapsedDataParallel_2_3573();
void WEIGHTED_ROUND_ROBIN_Joiner_3570();
void Printer(buffer_float_t *chanin);
void Printer_3132();

#ifdef __cplusplus
}
#endif
#endif
