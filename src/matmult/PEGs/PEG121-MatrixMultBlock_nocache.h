#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=104544 on the compile command line
#else
#if BUF_SIZEMAX < 104544
#error BUF_SIZEMAX too small, it must be at least 104544
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_19339_t;

typedef struct {
	float result[3][4];
} BlockAdd_19366_t;
void BlockFloatSource_19339();
void WEIGHTED_ROUND_ROBIN_Splitter_19468();
void WEIGHTED_ROUND_ROBIN_Splitter_19476();
void Post_CollapsedDataParallel_1_19478();
void Post_CollapsedDataParallel_1_19479();
void Post_CollapsedDataParallel_1_19480();
void Post_CollapsedDataParallel_1_19481();
void Post_CollapsedDataParallel_1_19482();
void Post_CollapsedDataParallel_1_19483();
void Post_CollapsedDataParallel_1_19484();
void Post_CollapsedDataParallel_1_19485();
void Post_CollapsedDataParallel_1_19486();
void Post_CollapsedDataParallel_1_19487();
void Post_CollapsedDataParallel_1_19488();
void Post_CollapsedDataParallel_1_19489();
void WEIGHTED_ROUND_ROBIN_Joiner_19477();
void Identity_19343();
void WEIGHTED_ROUND_ROBIN_Splitter_19490();
void Pre_CollapsedDataParallel_2_19492();
void Pre_CollapsedDataParallel_2_19493();
void Pre_CollapsedDataParallel_2_19494();
void WEIGHTED_ROUND_ROBIN_Joiner_19491();
void WEIGHTED_ROUND_ROBIN_Splitter_19495();
void AutoGeneratedExpander_19497();
void AutoGeneratedExpander_19498();
void AutoGeneratedExpander_19499();
void AutoGeneratedExpander_19500();
void AutoGeneratedExpander_19501();
void AutoGeneratedExpander_19502();
void AutoGeneratedExpander_19503();
void AutoGeneratedExpander_19504();
void AutoGeneratedExpander_19505();
void AutoGeneratedExpander_19506();
void AutoGeneratedExpander_19507();
void AutoGeneratedExpander_19508();
void AutoGeneratedExpander_19509();
void AutoGeneratedExpander_19510();
void AutoGeneratedExpander_19511();
void AutoGeneratedExpander_19512();
void AutoGeneratedExpander_19513();
void AutoGeneratedExpander_19514();
void AutoGeneratedExpander_19515();
void AutoGeneratedExpander_19516();
void AutoGeneratedExpander_19517();
void AutoGeneratedExpander_19518();
void AutoGeneratedExpander_19519();
void AutoGeneratedExpander_19520();
void AutoGeneratedExpander_19521();
void AutoGeneratedExpander_19522();
void AutoGeneratedExpander_19523();
void AutoGeneratedExpander_19524();
void AutoGeneratedExpander_19525();
void AutoGeneratedExpander_19526();
void AutoGeneratedExpander_19527();
void AutoGeneratedExpander_19528();
void AutoGeneratedExpander_19529();
void AutoGeneratedExpander_19530();
void AutoGeneratedExpander_19531();
void AutoGeneratedExpander_19532();
void AutoGeneratedExpander_19533();
void AutoGeneratedExpander_19534();
void AutoGeneratedExpander_19535();
void AutoGeneratedExpander_19536();
void AutoGeneratedExpander_19537();
void AutoGeneratedExpander_19538();
void AutoGeneratedExpander_19539();
void AutoGeneratedExpander_19540();
void AutoGeneratedExpander_19541();
void AutoGeneratedExpander_19542();
void AutoGeneratedExpander_19543();
void AutoGeneratedExpander_19544();
void AutoGeneratedExpander_19545();
void AutoGeneratedExpander_19546();
void AutoGeneratedExpander_19547();
void AutoGeneratedExpander_19548();
void AutoGeneratedExpander_19549();
void AutoGeneratedExpander_19550();
void AutoGeneratedExpander_19551();
void AutoGeneratedExpander_19552();
void AutoGeneratedExpander_19553();
void AutoGeneratedExpander_19554();
void AutoGeneratedExpander_19555();
void AutoGeneratedExpander_19556();
void AutoGeneratedExpander_19557();
void AutoGeneratedExpander_19558();
void AutoGeneratedExpander_19559();
void AutoGeneratedExpander_19560();
void AutoGeneratedExpander_19561();
void AutoGeneratedExpander_19562();
void AutoGeneratedExpander_19563();
void AutoGeneratedExpander_19564();
void AutoGeneratedExpander_19565();
void AutoGeneratedExpander_19566();
void AutoGeneratedExpander_19567();
void AutoGeneratedExpander_19568();
void AutoGeneratedExpander_19569();
void AutoGeneratedExpander_19570();
void AutoGeneratedExpander_19571();
void AutoGeneratedExpander_19572();
void AutoGeneratedExpander_19573();
void AutoGeneratedExpander_19574();
void AutoGeneratedExpander_19575();
void AutoGeneratedExpander_19576();
void AutoGeneratedExpander_19577();
void AutoGeneratedExpander_19578();
void AutoGeneratedExpander_19579();
void AutoGeneratedExpander_19580();
void AutoGeneratedExpander_19581();
void AutoGeneratedExpander_19582();
void AutoGeneratedExpander_19583();
void AutoGeneratedExpander_19584();
void AutoGeneratedExpander_19585();
void AutoGeneratedExpander_19586();
void AutoGeneratedExpander_19587();
void AutoGeneratedExpander_19588();
void AutoGeneratedExpander_19589();
void AutoGeneratedExpander_19590();
void AutoGeneratedExpander_19591();
void AutoGeneratedExpander_19592();
void AutoGeneratedExpander_19593();
void AutoGeneratedExpander_19594();
void AutoGeneratedExpander_19595();
void AutoGeneratedExpander_19596();
void AutoGeneratedExpander_19597();
void AutoGeneratedExpander_19598();
void AutoGeneratedExpander_19599();
void AutoGeneratedExpander_19600();
void AutoGeneratedExpander_19601();
void AutoGeneratedExpander_19602();
void AutoGeneratedExpander_19603();
void AutoGeneratedExpander_19604();
void AutoGeneratedExpander_19605();
void AutoGeneratedExpander_19606();
void AutoGeneratedExpander_19607();
void AutoGeneratedExpander_19608();
void AutoGeneratedExpander_19609();
void AutoGeneratedExpander_19610();
void AutoGeneratedExpander_19611();
void AutoGeneratedExpander_19612();
void AutoGeneratedExpander_19613();
void AutoGeneratedExpander_19614();
void AutoGeneratedExpander_19615();
void AutoGeneratedExpander_19616();
void AutoGeneratedExpander_19617();
void WEIGHTED_ROUND_ROBIN_Joiner_19496();
void Identity_19346();
void WEIGHTED_ROUND_ROBIN_Splitter_19618();
void Pre_CollapsedDataParallel_2_19620();
void Pre_CollapsedDataParallel_2_19621();
void Pre_CollapsedDataParallel_2_19622();
void WEIGHTED_ROUND_ROBIN_Joiner_19619();
void Identity_19350();
void Pre_CollapsedDataParallel_2_19462();
void WEIGHTED_ROUND_ROBIN_Splitter_19623();
void Post_CollapsedDataParallel_1_19625();
void Post_CollapsedDataParallel_1_19626();
void Post_CollapsedDataParallel_1_19627();
void Post_CollapsedDataParallel_1_19628();
void Post_CollapsedDataParallel_1_19629();
void Post_CollapsedDataParallel_1_19630();
void Post_CollapsedDataParallel_1_19631();
void Post_CollapsedDataParallel_1_19632();
void Post_CollapsedDataParallel_1_19633();
void WEIGHTED_ROUND_ROBIN_Joiner_19624();
void Identity_19359();
void WEIGHTED_ROUND_ROBIN_Splitter_19634();
void Pre_CollapsedDataParallel_2_19636();
void Pre_CollapsedDataParallel_2_19637();
void Pre_CollapsedDataParallel_2_19638();
void WEIGHTED_ROUND_ROBIN_Joiner_19635();
void WEIGHTED_ROUND_ROBIN_Splitter_19639();
void AutoGeneratedExpander_19641();
void AutoGeneratedExpander_19642();
void AutoGeneratedExpander_19643();
void AutoGeneratedExpander_19644();
void AutoGeneratedExpander_19645();
void AutoGeneratedExpander_19646();
void AutoGeneratedExpander_19647();
void AutoGeneratedExpander_19648();
void AutoGeneratedExpander_19649();
void AutoGeneratedExpander_19650();
void AutoGeneratedExpander_19651();
void AutoGeneratedExpander_19652();
void AutoGeneratedExpander_19653();
void AutoGeneratedExpander_19654();
void AutoGeneratedExpander_19655();
void AutoGeneratedExpander_19656();
void AutoGeneratedExpander_19657();
void AutoGeneratedExpander_19658();
void AutoGeneratedExpander_19659();
void AutoGeneratedExpander_19660();
void AutoGeneratedExpander_19661();
void AutoGeneratedExpander_19662();
void AutoGeneratedExpander_19663();
void AutoGeneratedExpander_19664();
void AutoGeneratedExpander_19665();
void AutoGeneratedExpander_19666();
void AutoGeneratedExpander_19667();
void AutoGeneratedExpander_19668();
void AutoGeneratedExpander_19669();
void AutoGeneratedExpander_19670();
void AutoGeneratedExpander_19671();
void AutoGeneratedExpander_19672();
void AutoGeneratedExpander_19673();
void AutoGeneratedExpander_19674();
void AutoGeneratedExpander_19675();
void AutoGeneratedExpander_19676();
void AutoGeneratedExpander_19677();
void AutoGeneratedExpander_19678();
void AutoGeneratedExpander_19679();
void AutoGeneratedExpander_19680();
void AutoGeneratedExpander_19681();
void AutoGeneratedExpander_19682();
void AutoGeneratedExpander_19683();
void AutoGeneratedExpander_19684();
void AutoGeneratedExpander_19685();
void AutoGeneratedExpander_19686();
void AutoGeneratedExpander_19687();
void AutoGeneratedExpander_19688();
void AutoGeneratedExpander_19689();
void AutoGeneratedExpander_19690();
void AutoGeneratedExpander_19691();
void AutoGeneratedExpander_19692();
void AutoGeneratedExpander_19693();
void AutoGeneratedExpander_19694();
void AutoGeneratedExpander_19695();
void AutoGeneratedExpander_19696();
void AutoGeneratedExpander_19697();
void AutoGeneratedExpander_19698();
void AutoGeneratedExpander_19699();
void AutoGeneratedExpander_19700();
void AutoGeneratedExpander_19701();
void AutoGeneratedExpander_19702();
void AutoGeneratedExpander_19703();
void AutoGeneratedExpander_19704();
void AutoGeneratedExpander_19705();
void AutoGeneratedExpander_19706();
void AutoGeneratedExpander_19707();
void AutoGeneratedExpander_19708();
void AutoGeneratedExpander_19709();
void AutoGeneratedExpander_19710();
void AutoGeneratedExpander_19711();
void AutoGeneratedExpander_19712();
void AutoGeneratedExpander_19713();
void AutoGeneratedExpander_19714();
void AutoGeneratedExpander_19715();
void AutoGeneratedExpander_19716();
void AutoGeneratedExpander_19717();
void AutoGeneratedExpander_19718();
void AutoGeneratedExpander_19719();
void AutoGeneratedExpander_19720();
void AutoGeneratedExpander_19721();
void AutoGeneratedExpander_19722();
void AutoGeneratedExpander_19723();
void AutoGeneratedExpander_19724();
void AutoGeneratedExpander_19725();
void AutoGeneratedExpander_19726();
void AutoGeneratedExpander_19727();
void AutoGeneratedExpander_19728();
void AutoGeneratedExpander_19729();
void AutoGeneratedExpander_19730();
void AutoGeneratedExpander_19731();
void AutoGeneratedExpander_19732();
void AutoGeneratedExpander_19733();
void AutoGeneratedExpander_19734();
void AutoGeneratedExpander_19735();
void AutoGeneratedExpander_19736();
void AutoGeneratedExpander_19737();
void AutoGeneratedExpander_19738();
void AutoGeneratedExpander_19739();
void AutoGeneratedExpander_19740();
void AutoGeneratedExpander_19741();
void AutoGeneratedExpander_19742();
void AutoGeneratedExpander_19743();
void AutoGeneratedExpander_19744();
void AutoGeneratedExpander_19745();
void AutoGeneratedExpander_19746();
void AutoGeneratedExpander_19747();
void AutoGeneratedExpander_19748();
void WEIGHTED_ROUND_ROBIN_Joiner_19640();
void Identity_19362();
void Pre_CollapsedDataParallel_2_19465();
void WEIGHTED_ROUND_ROBIN_Joiner_19469();
void WEIGHTED_ROUND_ROBIN_Splitter_19749();
void BlockMultiply_19751();
void BlockMultiply_19752();
void BlockMultiply_19753();
void BlockMultiply_19754();
void BlockMultiply_19755();
void BlockMultiply_19756();
void BlockMultiply_19757();
void BlockMultiply_19758();
void BlockMultiply_19759();
void BlockMultiply_19760();
void BlockMultiply_19761();
void BlockMultiply_19762();
void BlockMultiply_19763();
void BlockMultiply_19764();
void BlockMultiply_19765();
void BlockMultiply_19766();
void BlockMultiply_19767();
void BlockMultiply_19768();
void BlockMultiply_19769();
void BlockMultiply_19770();
void BlockMultiply_19771();
void BlockMultiply_19772();
void BlockMultiply_19773();
void BlockMultiply_19774();
void BlockMultiply_19775();
void BlockMultiply_19776();
void BlockMultiply_19777();
void WEIGHTED_ROUND_ROBIN_Joiner_19750();
void BlockAdd_19366();
void WEIGHTED_ROUND_ROBIN_Splitter_19778();
void Post_CollapsedDataParallel_1_19780();
void Post_CollapsedDataParallel_1_19781();
void Post_CollapsedDataParallel_1_19782();
void Post_CollapsedDataParallel_1_19783();
void Post_CollapsedDataParallel_1_19784();
void Post_CollapsedDataParallel_1_19785();
void Post_CollapsedDataParallel_1_19786();
void Post_CollapsedDataParallel_1_19787();
void Post_CollapsedDataParallel_1_19788();
void WEIGHTED_ROUND_ROBIN_Joiner_19779();
void Identity_19368();
void WEIGHTED_ROUND_ROBIN_Splitter_19789();
void Pre_CollapsedDataParallel_2_19791();
void Pre_CollapsedDataParallel_2_19792();
void Pre_CollapsedDataParallel_2_19793();
void WEIGHTED_ROUND_ROBIN_Joiner_19790();
void Printer_19372();

#ifdef __cplusplus
}
#endif
#endif
