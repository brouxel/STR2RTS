#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4320 on the compile command line
#else
#if BUF_SIZEMAX < 4320
#error BUF_SIZEMAX too small, it must be at least 4320
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_20130_t;

typedef struct {
	float result[3][4];
} BlockAdd_20157_t;
void BlockFloatSource(buffer_float_t *chanout);
void BlockFloatSource_20130();
void WEIGHTED_ROUND_ROBIN_Splitter_20259();
void WEIGHTED_ROUND_ROBIN_Splitter_20267();
void Post_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_1_20269();
void Post_CollapsedDataParallel_1_20270();
void Post_CollapsedDataParallel_1_20271();
void Post_CollapsedDataParallel_1_20272();
void Post_CollapsedDataParallel_1_20273();
void Post_CollapsedDataParallel_1_20274();
void Post_CollapsedDataParallel_1_20275();
void Post_CollapsedDataParallel_1_20276();
void Post_CollapsedDataParallel_1_20277();
void Post_CollapsedDataParallel_1_20278();
void Post_CollapsedDataParallel_1_20279();
void Post_CollapsedDataParallel_1_20280();
void WEIGHTED_ROUND_ROBIN_Joiner_20268();
void Identity(buffer_float_t *chanin, buffer_float_t *chanout);
void Identity_20134();
void WEIGHTED_ROUND_ROBIN_Splitter_20281();
void Pre_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_2_20283();
void Pre_CollapsedDataParallel_2_20284();
void Pre_CollapsedDataParallel_2_20285();
void WEIGHTED_ROUND_ROBIN_Joiner_20282();
void WEIGHTED_ROUND_ROBIN_Splitter_20286();
void AutoGeneratedExpander(buffer_float_t *chanin, buffer_float_t *chanout);
void AutoGeneratedExpander_20288();
void AutoGeneratedExpander_20289();
void AutoGeneratedExpander_20290();
void AutoGeneratedExpander_20291();
void AutoGeneratedExpander_20292();
void AutoGeneratedExpander_20293();
void AutoGeneratedExpander_20294();
void AutoGeneratedExpander_20295();
void AutoGeneratedExpander_20296();
void AutoGeneratedExpander_20297();
void AutoGeneratedExpander_20298();
void AutoGeneratedExpander_20299();
void AutoGeneratedExpander_20300();
void AutoGeneratedExpander_20301();
void AutoGeneratedExpander_20302();
void AutoGeneratedExpander_20303();
void AutoGeneratedExpander_20304();
void AutoGeneratedExpander_20305();
void AutoGeneratedExpander_20306();
void AutoGeneratedExpander_20307();
void AutoGeneratedExpander_20308();
void AutoGeneratedExpander_20309();
void AutoGeneratedExpander_20310();
void AutoGeneratedExpander_20311();
void AutoGeneratedExpander_20312();
void AutoGeneratedExpander_20313();
void AutoGeneratedExpander_20314();
void AutoGeneratedExpander_20315();
void AutoGeneratedExpander_20316();
void AutoGeneratedExpander_20317();
void AutoGeneratedExpander_20318();
void AutoGeneratedExpander_20319();
void AutoGeneratedExpander_20320();
void AutoGeneratedExpander_20321();
void AutoGeneratedExpander_20322();
void AutoGeneratedExpander_20323();
void AutoGeneratedExpander_20324();
void AutoGeneratedExpander_20325();
void AutoGeneratedExpander_20326();
void AutoGeneratedExpander_20327();
void AutoGeneratedExpander_20328();
void AutoGeneratedExpander_20329();
void AutoGeneratedExpander_20330();
void AutoGeneratedExpander_20331();
void AutoGeneratedExpander_20332();
void AutoGeneratedExpander_20333();
void AutoGeneratedExpander_20334();
void AutoGeneratedExpander_20335();
void AutoGeneratedExpander_20336();
void AutoGeneratedExpander_20337();
void AutoGeneratedExpander_20338();
void AutoGeneratedExpander_20339();
void AutoGeneratedExpander_20340();
void AutoGeneratedExpander_20341();
void AutoGeneratedExpander_20342();
void AutoGeneratedExpander_20343();
void AutoGeneratedExpander_20344();
void AutoGeneratedExpander_20345();
void AutoGeneratedExpander_20346();
void AutoGeneratedExpander_20347();
void AutoGeneratedExpander_20348();
void AutoGeneratedExpander_20349();
void AutoGeneratedExpander_20350();
void AutoGeneratedExpander_20351();
void AutoGeneratedExpander_20352();
void AutoGeneratedExpander_20353();
void AutoGeneratedExpander_20354();
void AutoGeneratedExpander_20355();
void AutoGeneratedExpander_20356();
void AutoGeneratedExpander_20357();
void AutoGeneratedExpander_20358();
void AutoGeneratedExpander_20359();
void AutoGeneratedExpander_20360();
void AutoGeneratedExpander_20361();
void AutoGeneratedExpander_20362();
void AutoGeneratedExpander_20363();
void AutoGeneratedExpander_20364();
void AutoGeneratedExpander_20365();
void AutoGeneratedExpander_20366();
void AutoGeneratedExpander_20367();
void AutoGeneratedExpander_20368();
void AutoGeneratedExpander_20369();
void AutoGeneratedExpander_20370();
void AutoGeneratedExpander_20371();
void AutoGeneratedExpander_20372();
void AutoGeneratedExpander_20373();
void AutoGeneratedExpander_20374();
void AutoGeneratedExpander_20375();
void AutoGeneratedExpander_20376();
void AutoGeneratedExpander_20377();
void AutoGeneratedExpander_20378();
void AutoGeneratedExpander_20379();
void AutoGeneratedExpander_20380();
void AutoGeneratedExpander_20381();
void AutoGeneratedExpander_20382();
void AutoGeneratedExpander_20383();
void AutoGeneratedExpander_20384();
void AutoGeneratedExpander_20385();
void AutoGeneratedExpander_20386();
void AutoGeneratedExpander_20387();
void AutoGeneratedExpander_20388();
void AutoGeneratedExpander_20389();
void AutoGeneratedExpander_20390();
void AutoGeneratedExpander_20391();
void AutoGeneratedExpander_20392();
void AutoGeneratedExpander_20393();
void AutoGeneratedExpander_20394();
void AutoGeneratedExpander_20395();
void AutoGeneratedExpander_20396();
void AutoGeneratedExpander_20397();
void AutoGeneratedExpander_20398();
void AutoGeneratedExpander_20399();
void AutoGeneratedExpander_20400();
void AutoGeneratedExpander_20401();
void AutoGeneratedExpander_20402();
void AutoGeneratedExpander_20403();
void AutoGeneratedExpander_20404();
void AutoGeneratedExpander_20405();
void AutoGeneratedExpander_20406();
void AutoGeneratedExpander_20407();
void WEIGHTED_ROUND_ROBIN_Joiner_20287();
void Identity_20137();
void WEIGHTED_ROUND_ROBIN_Splitter_20408();
void Pre_CollapsedDataParallel_2_20410();
void Pre_CollapsedDataParallel_2_20411();
void Pre_CollapsedDataParallel_2_20412();
void WEIGHTED_ROUND_ROBIN_Joiner_20409();
void Identity_20141();
void Pre_CollapsedDataParallel_2_20253();
void WEIGHTED_ROUND_ROBIN_Splitter_20413();
void Post_CollapsedDataParallel_1_20415();
void Post_CollapsedDataParallel_1_20416();
void Post_CollapsedDataParallel_1_20417();
void Post_CollapsedDataParallel_1_20418();
void Post_CollapsedDataParallel_1_20419();
void Post_CollapsedDataParallel_1_20420();
void Post_CollapsedDataParallel_1_20421();
void Post_CollapsedDataParallel_1_20422();
void Post_CollapsedDataParallel_1_20423();
void WEIGHTED_ROUND_ROBIN_Joiner_20414();
void Identity_20150();
void WEIGHTED_ROUND_ROBIN_Splitter_20424();
void Pre_CollapsedDataParallel_2_20426();
void Pre_CollapsedDataParallel_2_20427();
void Pre_CollapsedDataParallel_2_20428();
void WEIGHTED_ROUND_ROBIN_Joiner_20425();
void WEIGHTED_ROUND_ROBIN_Splitter_20429();
void AutoGeneratedExpander_20431();
void AutoGeneratedExpander_20432();
void AutoGeneratedExpander_20433();
void AutoGeneratedExpander_20434();
void AutoGeneratedExpander_20435();
void AutoGeneratedExpander_20436();
void AutoGeneratedExpander_20437();
void AutoGeneratedExpander_20438();
void AutoGeneratedExpander_20439();
void AutoGeneratedExpander_20440();
void AutoGeneratedExpander_20441();
void AutoGeneratedExpander_20442();
void AutoGeneratedExpander_20443();
void AutoGeneratedExpander_20444();
void AutoGeneratedExpander_20445();
void AutoGeneratedExpander_20446();
void AutoGeneratedExpander_20447();
void AutoGeneratedExpander_20448();
void AutoGeneratedExpander_20449();
void AutoGeneratedExpander_20450();
void AutoGeneratedExpander_20451();
void AutoGeneratedExpander_20452();
void AutoGeneratedExpander_20453();
void AutoGeneratedExpander_20454();
void AutoGeneratedExpander_20455();
void AutoGeneratedExpander_20456();
void AutoGeneratedExpander_20457();
void AutoGeneratedExpander_20458();
void AutoGeneratedExpander_20459();
void AutoGeneratedExpander_20460();
void AutoGeneratedExpander_20461();
void AutoGeneratedExpander_20462();
void AutoGeneratedExpander_20463();
void AutoGeneratedExpander_20464();
void AutoGeneratedExpander_20465();
void AutoGeneratedExpander_20466();
void AutoGeneratedExpander_20467();
void AutoGeneratedExpander_20468();
void AutoGeneratedExpander_20469();
void AutoGeneratedExpander_20470();
void AutoGeneratedExpander_20471();
void AutoGeneratedExpander_20472();
void AutoGeneratedExpander_20473();
void AutoGeneratedExpander_20474();
void AutoGeneratedExpander_20475();
void AutoGeneratedExpander_20476();
void AutoGeneratedExpander_20477();
void AutoGeneratedExpander_20478();
void AutoGeneratedExpander_20479();
void AutoGeneratedExpander_20480();
void AutoGeneratedExpander_20481();
void AutoGeneratedExpander_20482();
void AutoGeneratedExpander_20483();
void AutoGeneratedExpander_20484();
void AutoGeneratedExpander_20485();
void AutoGeneratedExpander_20486();
void AutoGeneratedExpander_20487();
void AutoGeneratedExpander_20488();
void AutoGeneratedExpander_20489();
void AutoGeneratedExpander_20490();
void AutoGeneratedExpander_20491();
void AutoGeneratedExpander_20492();
void AutoGeneratedExpander_20493();
void AutoGeneratedExpander_20494();
void AutoGeneratedExpander_20495();
void AutoGeneratedExpander_20496();
void AutoGeneratedExpander_20497();
void AutoGeneratedExpander_20498();
void AutoGeneratedExpander_20499();
void AutoGeneratedExpander_20500();
void AutoGeneratedExpander_20501();
void AutoGeneratedExpander_20502();
void AutoGeneratedExpander_20503();
void AutoGeneratedExpander_20504();
void AutoGeneratedExpander_20505();
void AutoGeneratedExpander_20506();
void AutoGeneratedExpander_20507();
void AutoGeneratedExpander_20508();
void AutoGeneratedExpander_20509();
void AutoGeneratedExpander_20510();
void AutoGeneratedExpander_20511();
void AutoGeneratedExpander_20512();
void AutoGeneratedExpander_20513();
void AutoGeneratedExpander_20514();
void AutoGeneratedExpander_20515();
void AutoGeneratedExpander_20516();
void AutoGeneratedExpander_20517();
void AutoGeneratedExpander_20518();
void AutoGeneratedExpander_20519();
void AutoGeneratedExpander_20520();
void AutoGeneratedExpander_20521();
void AutoGeneratedExpander_20522();
void AutoGeneratedExpander_20523();
void AutoGeneratedExpander_20524();
void AutoGeneratedExpander_20525();
void AutoGeneratedExpander_20526();
void AutoGeneratedExpander_20527();
void AutoGeneratedExpander_20528();
void AutoGeneratedExpander_20529();
void AutoGeneratedExpander_20530();
void AutoGeneratedExpander_20531();
void AutoGeneratedExpander_20532();
void AutoGeneratedExpander_20533();
void AutoGeneratedExpander_20534();
void AutoGeneratedExpander_20535();
void AutoGeneratedExpander_20536();
void AutoGeneratedExpander_20537();
void AutoGeneratedExpander_20538();
void WEIGHTED_ROUND_ROBIN_Joiner_20430();
void Identity_20153();
void Pre_CollapsedDataParallel_2_20256();
void WEIGHTED_ROUND_ROBIN_Joiner_20260();
void WEIGHTED_ROUND_ROBIN_Splitter_20539();
void BlockMultiply(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockMultiply_20541();
void BlockMultiply_20542();
void BlockMultiply_20543();
void BlockMultiply_20544();
void BlockMultiply_20545();
void BlockMultiply_20546();
void BlockMultiply_20547();
void BlockMultiply_20548();
void BlockMultiply_20549();
void BlockMultiply_20550();
void BlockMultiply_20551();
void BlockMultiply_20552();
void BlockMultiply_20553();
void BlockMultiply_20554();
void BlockMultiply_20555();
void BlockMultiply_20556();
void BlockMultiply_20557();
void BlockMultiply_20558();
void BlockMultiply_20559();
void BlockMultiply_20560();
void BlockMultiply_20561();
void BlockMultiply_20562();
void BlockMultiply_20563();
void BlockMultiply_20564();
void BlockMultiply_20565();
void BlockMultiply_20566();
void BlockMultiply_20567();
void WEIGHTED_ROUND_ROBIN_Joiner_20540();
void BlockAdd(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockAdd_20157();
void WEIGHTED_ROUND_ROBIN_Splitter_20568();
void Post_CollapsedDataParallel_1_20570();
void Post_CollapsedDataParallel_1_20571();
void Post_CollapsedDataParallel_1_20572();
void Post_CollapsedDataParallel_1_20573();
void Post_CollapsedDataParallel_1_20574();
void Post_CollapsedDataParallel_1_20575();
void Post_CollapsedDataParallel_1_20576();
void Post_CollapsedDataParallel_1_20577();
void Post_CollapsedDataParallel_1_20578();
void WEIGHTED_ROUND_ROBIN_Joiner_20569();
void Identity_20159();
void WEIGHTED_ROUND_ROBIN_Splitter_20579();
void Pre_CollapsedDataParallel_2_20581();
void Pre_CollapsedDataParallel_2_20582();
void Pre_CollapsedDataParallel_2_20583();
void WEIGHTED_ROUND_ROBIN_Joiner_20580();
void Printer(buffer_float_t *chanin);
void Printer_20163();

#ifdef __cplusplus
}
#endif
#endif
