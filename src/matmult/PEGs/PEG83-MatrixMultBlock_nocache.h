#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=71712 on the compile command line
#else
#if BUF_SIZEMAX < 71712
#error BUF_SIZEMAX too small, it must be at least 71712
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_47391_t;

typedef struct {
	float result[3][4];
} BlockAdd_47418_t;
void BlockFloatSource_47391();
void WEIGHTED_ROUND_ROBIN_Splitter_47520();
void WEIGHTED_ROUND_ROBIN_Splitter_47528();
void Post_CollapsedDataParallel_1_47530();
void Post_CollapsedDataParallel_1_47531();
void Post_CollapsedDataParallel_1_47532();
void Post_CollapsedDataParallel_1_47533();
void Post_CollapsedDataParallel_1_47534();
void Post_CollapsedDataParallel_1_47535();
void Post_CollapsedDataParallel_1_47536();
void Post_CollapsedDataParallel_1_47537();
void Post_CollapsedDataParallel_1_47538();
void Post_CollapsedDataParallel_1_47539();
void Post_CollapsedDataParallel_1_47540();
void Post_CollapsedDataParallel_1_47541();
void WEIGHTED_ROUND_ROBIN_Joiner_47529();
void Identity_47395();
void WEIGHTED_ROUND_ROBIN_Splitter_47542();
void Pre_CollapsedDataParallel_2_47544();
void Pre_CollapsedDataParallel_2_47545();
void Pre_CollapsedDataParallel_2_47546();
void WEIGHTED_ROUND_ROBIN_Joiner_47543();
void WEIGHTED_ROUND_ROBIN_Splitter_47547();
void AutoGeneratedExpander_47549();
void AutoGeneratedExpander_47550();
void AutoGeneratedExpander_47551();
void AutoGeneratedExpander_47552();
void AutoGeneratedExpander_47553();
void AutoGeneratedExpander_47554();
void AutoGeneratedExpander_47555();
void AutoGeneratedExpander_47556();
void AutoGeneratedExpander_47557();
void AutoGeneratedExpander_47558();
void AutoGeneratedExpander_47559();
void AutoGeneratedExpander_47560();
void AutoGeneratedExpander_47561();
void AutoGeneratedExpander_47562();
void AutoGeneratedExpander_47563();
void AutoGeneratedExpander_47564();
void AutoGeneratedExpander_47565();
void AutoGeneratedExpander_47566();
void AutoGeneratedExpander_47567();
void AutoGeneratedExpander_47568();
void AutoGeneratedExpander_47569();
void AutoGeneratedExpander_47570();
void AutoGeneratedExpander_47571();
void AutoGeneratedExpander_47572();
void AutoGeneratedExpander_47573();
void AutoGeneratedExpander_47574();
void AutoGeneratedExpander_47575();
void AutoGeneratedExpander_47576();
void AutoGeneratedExpander_47577();
void AutoGeneratedExpander_47578();
void AutoGeneratedExpander_47579();
void AutoGeneratedExpander_47580();
void AutoGeneratedExpander_47581();
void AutoGeneratedExpander_47582();
void AutoGeneratedExpander_47583();
void AutoGeneratedExpander_47584();
void AutoGeneratedExpander_47585();
void AutoGeneratedExpander_47586();
void AutoGeneratedExpander_47587();
void AutoGeneratedExpander_47588();
void AutoGeneratedExpander_47589();
void AutoGeneratedExpander_47590();
void AutoGeneratedExpander_47591();
void AutoGeneratedExpander_47592();
void AutoGeneratedExpander_47593();
void AutoGeneratedExpander_47594();
void AutoGeneratedExpander_47595();
void AutoGeneratedExpander_47596();
void AutoGeneratedExpander_47597();
void AutoGeneratedExpander_47598();
void AutoGeneratedExpander_47599();
void AutoGeneratedExpander_47600();
void AutoGeneratedExpander_47601();
void AutoGeneratedExpander_47602();
void AutoGeneratedExpander_47603();
void AutoGeneratedExpander_47604();
void AutoGeneratedExpander_47605();
void AutoGeneratedExpander_47606();
void AutoGeneratedExpander_47607();
void AutoGeneratedExpander_47608();
void AutoGeneratedExpander_47609();
void AutoGeneratedExpander_47610();
void AutoGeneratedExpander_47611();
void AutoGeneratedExpander_47612();
void AutoGeneratedExpander_47613();
void AutoGeneratedExpander_47614();
void AutoGeneratedExpander_47615();
void AutoGeneratedExpander_47616();
void AutoGeneratedExpander_47617();
void AutoGeneratedExpander_47618();
void AutoGeneratedExpander_47619();
void AutoGeneratedExpander_47620();
void AutoGeneratedExpander_47621();
void AutoGeneratedExpander_47622();
void AutoGeneratedExpander_47623();
void AutoGeneratedExpander_47624();
void AutoGeneratedExpander_47625();
void AutoGeneratedExpander_47626();
void AutoGeneratedExpander_47627();
void AutoGeneratedExpander_47628();
void AutoGeneratedExpander_47629();
void AutoGeneratedExpander_47630();
void AutoGeneratedExpander_47631();
void WEIGHTED_ROUND_ROBIN_Joiner_47548();
void Identity_47398();
void WEIGHTED_ROUND_ROBIN_Splitter_47632();
void Pre_CollapsedDataParallel_2_47634();
void Pre_CollapsedDataParallel_2_47635();
void Pre_CollapsedDataParallel_2_47636();
void WEIGHTED_ROUND_ROBIN_Joiner_47633();
void Identity_47402();
void Pre_CollapsedDataParallel_2_47514();
void WEIGHTED_ROUND_ROBIN_Splitter_47637();
void Post_CollapsedDataParallel_1_47639();
void Post_CollapsedDataParallel_1_47640();
void Post_CollapsedDataParallel_1_47641();
void Post_CollapsedDataParallel_1_47642();
void Post_CollapsedDataParallel_1_47643();
void Post_CollapsedDataParallel_1_47644();
void Post_CollapsedDataParallel_1_47645();
void Post_CollapsedDataParallel_1_47646();
void Post_CollapsedDataParallel_1_47647();
void WEIGHTED_ROUND_ROBIN_Joiner_47638();
void Identity_47411();
void WEIGHTED_ROUND_ROBIN_Splitter_47648();
void Pre_CollapsedDataParallel_2_47650();
void Pre_CollapsedDataParallel_2_47651();
void Pre_CollapsedDataParallel_2_47652();
void WEIGHTED_ROUND_ROBIN_Joiner_47649();
void WEIGHTED_ROUND_ROBIN_Splitter_47653();
void AutoGeneratedExpander_47655();
void AutoGeneratedExpander_47656();
void AutoGeneratedExpander_47657();
void AutoGeneratedExpander_47658();
void AutoGeneratedExpander_47659();
void AutoGeneratedExpander_47660();
void AutoGeneratedExpander_47661();
void AutoGeneratedExpander_47662();
void AutoGeneratedExpander_47663();
void AutoGeneratedExpander_47664();
void AutoGeneratedExpander_47665();
void AutoGeneratedExpander_47666();
void AutoGeneratedExpander_47667();
void AutoGeneratedExpander_47668();
void AutoGeneratedExpander_47669();
void AutoGeneratedExpander_47670();
void AutoGeneratedExpander_47671();
void AutoGeneratedExpander_47672();
void AutoGeneratedExpander_47673();
void AutoGeneratedExpander_47674();
void AutoGeneratedExpander_47675();
void AutoGeneratedExpander_47676();
void AutoGeneratedExpander_47677();
void AutoGeneratedExpander_47678();
void AutoGeneratedExpander_47679();
void AutoGeneratedExpander_47680();
void AutoGeneratedExpander_47681();
void AutoGeneratedExpander_47682();
void AutoGeneratedExpander_47683();
void AutoGeneratedExpander_47684();
void AutoGeneratedExpander_47685();
void AutoGeneratedExpander_47686();
void AutoGeneratedExpander_47687();
void AutoGeneratedExpander_47688();
void AutoGeneratedExpander_47689();
void AutoGeneratedExpander_47690();
void AutoGeneratedExpander_47691();
void AutoGeneratedExpander_47692();
void AutoGeneratedExpander_47693();
void AutoGeneratedExpander_47694();
void AutoGeneratedExpander_47695();
void AutoGeneratedExpander_47696();
void AutoGeneratedExpander_47697();
void AutoGeneratedExpander_47698();
void AutoGeneratedExpander_47699();
void AutoGeneratedExpander_47700();
void AutoGeneratedExpander_47701();
void AutoGeneratedExpander_47702();
void AutoGeneratedExpander_47703();
void AutoGeneratedExpander_47704();
void AutoGeneratedExpander_47705();
void AutoGeneratedExpander_47706();
void AutoGeneratedExpander_47707();
void AutoGeneratedExpander_47708();
void AutoGeneratedExpander_47709();
void AutoGeneratedExpander_47710();
void AutoGeneratedExpander_47711();
void AutoGeneratedExpander_47712();
void AutoGeneratedExpander_47713();
void AutoGeneratedExpander_47714();
void AutoGeneratedExpander_47715();
void AutoGeneratedExpander_47716();
void AutoGeneratedExpander_47717();
void AutoGeneratedExpander_47718();
void AutoGeneratedExpander_47719();
void AutoGeneratedExpander_47720();
void AutoGeneratedExpander_47721();
void AutoGeneratedExpander_47722();
void AutoGeneratedExpander_47723();
void AutoGeneratedExpander_47724();
void AutoGeneratedExpander_47725();
void AutoGeneratedExpander_47726();
void AutoGeneratedExpander_47727();
void AutoGeneratedExpander_47728();
void AutoGeneratedExpander_47729();
void AutoGeneratedExpander_47730();
void AutoGeneratedExpander_47731();
void AutoGeneratedExpander_47732();
void AutoGeneratedExpander_47733();
void AutoGeneratedExpander_47734();
void AutoGeneratedExpander_47735();
void AutoGeneratedExpander_47736();
void AutoGeneratedExpander_47737();
void WEIGHTED_ROUND_ROBIN_Joiner_47654();
void Identity_47414();
void Pre_CollapsedDataParallel_2_47517();
void WEIGHTED_ROUND_ROBIN_Joiner_47521();
void WEIGHTED_ROUND_ROBIN_Splitter_47738();
void BlockMultiply_47740();
void BlockMultiply_47741();
void BlockMultiply_47742();
void BlockMultiply_47743();
void BlockMultiply_47744();
void BlockMultiply_47745();
void BlockMultiply_47746();
void BlockMultiply_47747();
void BlockMultiply_47748();
void BlockMultiply_47749();
void BlockMultiply_47750();
void BlockMultiply_47751();
void BlockMultiply_47752();
void BlockMultiply_47753();
void BlockMultiply_47754();
void BlockMultiply_47755();
void BlockMultiply_47756();
void BlockMultiply_47757();
void BlockMultiply_47758();
void BlockMultiply_47759();
void BlockMultiply_47760();
void BlockMultiply_47761();
void BlockMultiply_47762();
void BlockMultiply_47763();
void BlockMultiply_47764();
void BlockMultiply_47765();
void BlockMultiply_47766();
void WEIGHTED_ROUND_ROBIN_Joiner_47739();
void BlockAdd_47418();
void WEIGHTED_ROUND_ROBIN_Splitter_47767();
void Post_CollapsedDataParallel_1_47769();
void Post_CollapsedDataParallel_1_47770();
void Post_CollapsedDataParallel_1_47771();
void Post_CollapsedDataParallel_1_47772();
void Post_CollapsedDataParallel_1_47773();
void Post_CollapsedDataParallel_1_47774();
void Post_CollapsedDataParallel_1_47775();
void Post_CollapsedDataParallel_1_47776();
void Post_CollapsedDataParallel_1_47777();
void WEIGHTED_ROUND_ROBIN_Joiner_47768();
void Identity_47420();
void WEIGHTED_ROUND_ROBIN_Splitter_47778();
void Pre_CollapsedDataParallel_2_47780();
void Pre_CollapsedDataParallel_2_47781();
void Pre_CollapsedDataParallel_2_47782();
void WEIGHTED_ROUND_ROBIN_Joiner_47779();
void Printer_47424();

#ifdef __cplusplus
}
#endif
#endif
