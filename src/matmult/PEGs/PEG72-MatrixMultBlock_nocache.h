#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1728 on the compile command line
#else
#if BUF_SIZEMAX < 1728
#error BUF_SIZEMAX too small, it must be at least 1728
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_54486_t;

typedef struct {
	float result[3][4];
} BlockAdd_54513_t;
void BlockFloatSource_54486();
void WEIGHTED_ROUND_ROBIN_Splitter_54615();
void WEIGHTED_ROUND_ROBIN_Splitter_54623();
void Post_CollapsedDataParallel_1_54625();
void Post_CollapsedDataParallel_1_54626();
void Post_CollapsedDataParallel_1_54627();
void Post_CollapsedDataParallel_1_54628();
void Post_CollapsedDataParallel_1_54629();
void Post_CollapsedDataParallel_1_54630();
void Post_CollapsedDataParallel_1_54631();
void Post_CollapsedDataParallel_1_54632();
void Post_CollapsedDataParallel_1_54633();
void Post_CollapsedDataParallel_1_54634();
void Post_CollapsedDataParallel_1_54635();
void Post_CollapsedDataParallel_1_54636();
void WEIGHTED_ROUND_ROBIN_Joiner_54624();
void Identity_54490();
void WEIGHTED_ROUND_ROBIN_Splitter_54637();
void Pre_CollapsedDataParallel_2_54639();
void Pre_CollapsedDataParallel_2_54640();
void Pre_CollapsedDataParallel_2_54641();
void WEIGHTED_ROUND_ROBIN_Joiner_54638();
void WEIGHTED_ROUND_ROBIN_Splitter_54642();
void AutoGeneratedExpander_54644();
void AutoGeneratedExpander_54645();
void AutoGeneratedExpander_54646();
void AutoGeneratedExpander_54647();
void AutoGeneratedExpander_54648();
void AutoGeneratedExpander_54649();
void AutoGeneratedExpander_54650();
void AutoGeneratedExpander_54651();
void AutoGeneratedExpander_54652();
void AutoGeneratedExpander_54653();
void AutoGeneratedExpander_54654();
void AutoGeneratedExpander_54655();
void AutoGeneratedExpander_54656();
void AutoGeneratedExpander_54657();
void AutoGeneratedExpander_54658();
void AutoGeneratedExpander_54659();
void AutoGeneratedExpander_54660();
void AutoGeneratedExpander_54661();
void AutoGeneratedExpander_54662();
void AutoGeneratedExpander_54663();
void AutoGeneratedExpander_54664();
void AutoGeneratedExpander_54665();
void AutoGeneratedExpander_54666();
void AutoGeneratedExpander_54667();
void AutoGeneratedExpander_54668();
void AutoGeneratedExpander_54669();
void AutoGeneratedExpander_54670();
void AutoGeneratedExpander_54671();
void AutoGeneratedExpander_54672();
void AutoGeneratedExpander_54673();
void AutoGeneratedExpander_54674();
void AutoGeneratedExpander_54675();
void AutoGeneratedExpander_54676();
void AutoGeneratedExpander_54677();
void AutoGeneratedExpander_54678();
void AutoGeneratedExpander_54679();
void AutoGeneratedExpander_54680();
void AutoGeneratedExpander_54681();
void AutoGeneratedExpander_54682();
void AutoGeneratedExpander_54683();
void AutoGeneratedExpander_54684();
void AutoGeneratedExpander_54685();
void AutoGeneratedExpander_54686();
void AutoGeneratedExpander_54687();
void AutoGeneratedExpander_54688();
void AutoGeneratedExpander_54689();
void AutoGeneratedExpander_54690();
void AutoGeneratedExpander_54691();
void AutoGeneratedExpander_54692();
void AutoGeneratedExpander_54693();
void AutoGeneratedExpander_54694();
void AutoGeneratedExpander_54695();
void AutoGeneratedExpander_54696();
void AutoGeneratedExpander_54697();
void AutoGeneratedExpander_54698();
void AutoGeneratedExpander_54699();
void AutoGeneratedExpander_54700();
void AutoGeneratedExpander_54701();
void AutoGeneratedExpander_54702();
void AutoGeneratedExpander_54703();
void AutoGeneratedExpander_54704();
void AutoGeneratedExpander_54705();
void AutoGeneratedExpander_54706();
void AutoGeneratedExpander_54707();
void AutoGeneratedExpander_54708();
void AutoGeneratedExpander_54709();
void AutoGeneratedExpander_54710();
void AutoGeneratedExpander_54711();
void AutoGeneratedExpander_54712();
void AutoGeneratedExpander_54713();
void AutoGeneratedExpander_54714();
void AutoGeneratedExpander_54715();
void WEIGHTED_ROUND_ROBIN_Joiner_54643();
void Identity_54493();
void WEIGHTED_ROUND_ROBIN_Splitter_54716();
void Pre_CollapsedDataParallel_2_54718();
void Pre_CollapsedDataParallel_2_54719();
void Pre_CollapsedDataParallel_2_54720();
void WEIGHTED_ROUND_ROBIN_Joiner_54717();
void Identity_54497();
void Pre_CollapsedDataParallel_2_54609();
void WEIGHTED_ROUND_ROBIN_Splitter_54721();
void Post_CollapsedDataParallel_1_54723();
void Post_CollapsedDataParallel_1_54724();
void Post_CollapsedDataParallel_1_54725();
void Post_CollapsedDataParallel_1_54726();
void Post_CollapsedDataParallel_1_54727();
void Post_CollapsedDataParallel_1_54728();
void Post_CollapsedDataParallel_1_54729();
void Post_CollapsedDataParallel_1_54730();
void Post_CollapsedDataParallel_1_54731();
void WEIGHTED_ROUND_ROBIN_Joiner_54722();
void Identity_54506();
void WEIGHTED_ROUND_ROBIN_Splitter_54732();
void Pre_CollapsedDataParallel_2_54734();
void Pre_CollapsedDataParallel_2_54735();
void Pre_CollapsedDataParallel_2_54736();
void WEIGHTED_ROUND_ROBIN_Joiner_54733();
void WEIGHTED_ROUND_ROBIN_Splitter_54737();
void AutoGeneratedExpander_54739();
void AutoGeneratedExpander_54740();
void AutoGeneratedExpander_54741();
void AutoGeneratedExpander_54742();
void AutoGeneratedExpander_54743();
void AutoGeneratedExpander_54744();
void AutoGeneratedExpander_54745();
void AutoGeneratedExpander_54746();
void AutoGeneratedExpander_54747();
void AutoGeneratedExpander_54748();
void AutoGeneratedExpander_54749();
void AutoGeneratedExpander_54750();
void AutoGeneratedExpander_54751();
void AutoGeneratedExpander_54752();
void AutoGeneratedExpander_54753();
void AutoGeneratedExpander_54754();
void AutoGeneratedExpander_54755();
void AutoGeneratedExpander_54756();
void AutoGeneratedExpander_54757();
void AutoGeneratedExpander_54758();
void AutoGeneratedExpander_54759();
void AutoGeneratedExpander_54760();
void AutoGeneratedExpander_54761();
void AutoGeneratedExpander_54762();
void AutoGeneratedExpander_54763();
void AutoGeneratedExpander_54764();
void AutoGeneratedExpander_54765();
void AutoGeneratedExpander_54766();
void AutoGeneratedExpander_54767();
void AutoGeneratedExpander_54768();
void AutoGeneratedExpander_54769();
void AutoGeneratedExpander_54770();
void AutoGeneratedExpander_54771();
void AutoGeneratedExpander_54772();
void AutoGeneratedExpander_54773();
void AutoGeneratedExpander_54774();
void AutoGeneratedExpander_54775();
void AutoGeneratedExpander_54776();
void AutoGeneratedExpander_54777();
void AutoGeneratedExpander_54778();
void AutoGeneratedExpander_54779();
void AutoGeneratedExpander_54780();
void AutoGeneratedExpander_54781();
void AutoGeneratedExpander_54782();
void AutoGeneratedExpander_54783();
void AutoGeneratedExpander_54784();
void AutoGeneratedExpander_54785();
void AutoGeneratedExpander_54786();
void AutoGeneratedExpander_54787();
void AutoGeneratedExpander_54788();
void AutoGeneratedExpander_54789();
void AutoGeneratedExpander_54790();
void AutoGeneratedExpander_54791();
void AutoGeneratedExpander_54792();
void AutoGeneratedExpander_54793();
void AutoGeneratedExpander_54794();
void AutoGeneratedExpander_54795();
void AutoGeneratedExpander_54796();
void AutoGeneratedExpander_54797();
void AutoGeneratedExpander_54798();
void AutoGeneratedExpander_54799();
void AutoGeneratedExpander_54800();
void AutoGeneratedExpander_54801();
void AutoGeneratedExpander_54802();
void AutoGeneratedExpander_54803();
void AutoGeneratedExpander_54804();
void AutoGeneratedExpander_54805();
void AutoGeneratedExpander_54806();
void AutoGeneratedExpander_54807();
void AutoGeneratedExpander_54808();
void AutoGeneratedExpander_54809();
void AutoGeneratedExpander_54810();
void WEIGHTED_ROUND_ROBIN_Joiner_54738();
void Identity_54509();
void Pre_CollapsedDataParallel_2_54612();
void WEIGHTED_ROUND_ROBIN_Joiner_54616();
void WEIGHTED_ROUND_ROBIN_Splitter_54811();
void BlockMultiply_54813();
void BlockMultiply_54814();
void BlockMultiply_54815();
void BlockMultiply_54816();
void BlockMultiply_54817();
void BlockMultiply_54818();
void BlockMultiply_54819();
void BlockMultiply_54820();
void BlockMultiply_54821();
void BlockMultiply_54822();
void BlockMultiply_54823();
void BlockMultiply_54824();
void BlockMultiply_54825();
void BlockMultiply_54826();
void BlockMultiply_54827();
void BlockMultiply_54828();
void BlockMultiply_54829();
void BlockMultiply_54830();
void BlockMultiply_54831();
void BlockMultiply_54832();
void BlockMultiply_54833();
void BlockMultiply_54834();
void BlockMultiply_54835();
void BlockMultiply_54836();
void BlockMultiply_54837();
void BlockMultiply_54838();
void BlockMultiply_54839();
void WEIGHTED_ROUND_ROBIN_Joiner_54812();
void BlockAdd_54513();
void WEIGHTED_ROUND_ROBIN_Splitter_54840();
void Post_CollapsedDataParallel_1_54842();
void Post_CollapsedDataParallel_1_54843();
void Post_CollapsedDataParallel_1_54844();
void Post_CollapsedDataParallel_1_54845();
void Post_CollapsedDataParallel_1_54846();
void Post_CollapsedDataParallel_1_54847();
void Post_CollapsedDataParallel_1_54848();
void Post_CollapsedDataParallel_1_54849();
void Post_CollapsedDataParallel_1_54850();
void WEIGHTED_ROUND_ROBIN_Joiner_54841();
void Identity_54515();
void WEIGHTED_ROUND_ROBIN_Splitter_54851();
void Pre_CollapsedDataParallel_2_54853();
void Pre_CollapsedDataParallel_2_54854();
void Pre_CollapsedDataParallel_2_54855();
void WEIGHTED_ROUND_ROBIN_Joiner_54852();
void Printer_54519();

#ifdef __cplusplus
}
#endif
#endif
