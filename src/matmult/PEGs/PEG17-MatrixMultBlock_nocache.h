#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=14688 on the compile command line
#else
#if BUF_SIZEMAX < 14688
#error BUF_SIZEMAX too small, it must be at least 14688
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_82609_t;

typedef struct {
	float result[3][4];
} BlockAdd_82636_t;
void BlockFloatSource_82609();
void WEIGHTED_ROUND_ROBIN_Splitter_82738();
void WEIGHTED_ROUND_ROBIN_Splitter_82746();
void Post_CollapsedDataParallel_1_82748();
void Post_CollapsedDataParallel_1_82749();
void Post_CollapsedDataParallel_1_82750();
void Post_CollapsedDataParallel_1_82751();
void Post_CollapsedDataParallel_1_82752();
void Post_CollapsedDataParallel_1_82753();
void Post_CollapsedDataParallel_1_82754();
void Post_CollapsedDataParallel_1_82755();
void Post_CollapsedDataParallel_1_82756();
void Post_CollapsedDataParallel_1_82757();
void Post_CollapsedDataParallel_1_82758();
void Post_CollapsedDataParallel_1_82759();
void WEIGHTED_ROUND_ROBIN_Joiner_82747();
void Identity_82613();
void WEIGHTED_ROUND_ROBIN_Splitter_82760();
void Pre_CollapsedDataParallel_2_82762();
void Pre_CollapsedDataParallel_2_82763();
void Pre_CollapsedDataParallel_2_82764();
void WEIGHTED_ROUND_ROBIN_Joiner_82761();
void WEIGHTED_ROUND_ROBIN_Splitter_82765();
void AutoGeneratedExpander_82767();
void AutoGeneratedExpander_82768();
void AutoGeneratedExpander_82769();
void AutoGeneratedExpander_82770();
void AutoGeneratedExpander_82771();
void AutoGeneratedExpander_82772();
void AutoGeneratedExpander_82773();
void AutoGeneratedExpander_82774();
void AutoGeneratedExpander_82775();
void AutoGeneratedExpander_82776();
void AutoGeneratedExpander_82777();
void AutoGeneratedExpander_82778();
void AutoGeneratedExpander_82779();
void AutoGeneratedExpander_82780();
void AutoGeneratedExpander_82781();
void AutoGeneratedExpander_82782();
void AutoGeneratedExpander_82783();
void WEIGHTED_ROUND_ROBIN_Joiner_82766();
void Identity_82616();
void WEIGHTED_ROUND_ROBIN_Splitter_82784();
void Pre_CollapsedDataParallel_2_82786();
void Pre_CollapsedDataParallel_2_82787();
void Pre_CollapsedDataParallel_2_82788();
void WEIGHTED_ROUND_ROBIN_Joiner_82785();
void Identity_82620();
void Pre_CollapsedDataParallel_2_82732();
void WEIGHTED_ROUND_ROBIN_Splitter_82789();
void Post_CollapsedDataParallel_1_82791();
void Post_CollapsedDataParallel_1_82792();
void Post_CollapsedDataParallel_1_82793();
void Post_CollapsedDataParallel_1_82794();
void Post_CollapsedDataParallel_1_82795();
void Post_CollapsedDataParallel_1_82796();
void Post_CollapsedDataParallel_1_82797();
void Post_CollapsedDataParallel_1_82798();
void Post_CollapsedDataParallel_1_82799();
void WEIGHTED_ROUND_ROBIN_Joiner_82790();
void Identity_82629();
void WEIGHTED_ROUND_ROBIN_Splitter_82800();
void Pre_CollapsedDataParallel_2_82802();
void Pre_CollapsedDataParallel_2_82803();
void Pre_CollapsedDataParallel_2_82804();
void WEIGHTED_ROUND_ROBIN_Joiner_82801();
void WEIGHTED_ROUND_ROBIN_Splitter_82805();
void AutoGeneratedExpander_82807();
void AutoGeneratedExpander_82808();
void AutoGeneratedExpander_82809();
void AutoGeneratedExpander_82810();
void AutoGeneratedExpander_82811();
void AutoGeneratedExpander_82812();
void AutoGeneratedExpander_82813();
void AutoGeneratedExpander_82814();
void AutoGeneratedExpander_82815();
void AutoGeneratedExpander_82816();
void AutoGeneratedExpander_82817();
void AutoGeneratedExpander_82818();
void AutoGeneratedExpander_82819();
void AutoGeneratedExpander_82820();
void AutoGeneratedExpander_82821();
void AutoGeneratedExpander_82822();
void AutoGeneratedExpander_82823();
void WEIGHTED_ROUND_ROBIN_Joiner_82806();
void Identity_82632();
void Pre_CollapsedDataParallel_2_82735();
void WEIGHTED_ROUND_ROBIN_Joiner_82739();
void WEIGHTED_ROUND_ROBIN_Splitter_82824();
void BlockMultiply_82826();
void BlockMultiply_82827();
void BlockMultiply_82828();
void BlockMultiply_82829();
void BlockMultiply_82830();
void BlockMultiply_82831();
void BlockMultiply_82832();
void BlockMultiply_82833();
void BlockMultiply_82834();
void BlockMultiply_82835();
void BlockMultiply_82836();
void BlockMultiply_82837();
void BlockMultiply_82838();
void BlockMultiply_82839();
void BlockMultiply_82840();
void BlockMultiply_82841();
void BlockMultiply_82842();
void WEIGHTED_ROUND_ROBIN_Joiner_82825();
void BlockAdd_82636();
void WEIGHTED_ROUND_ROBIN_Splitter_82843();
void Post_CollapsedDataParallel_1_82845();
void Post_CollapsedDataParallel_1_82846();
void Post_CollapsedDataParallel_1_82847();
void Post_CollapsedDataParallel_1_82848();
void Post_CollapsedDataParallel_1_82849();
void Post_CollapsedDataParallel_1_82850();
void Post_CollapsedDataParallel_1_82851();
void Post_CollapsedDataParallel_1_82852();
void Post_CollapsedDataParallel_1_82853();
void WEIGHTED_ROUND_ROBIN_Joiner_82844();
void Identity_82638();
void WEIGHTED_ROUND_ROBIN_Splitter_82854();
void Pre_CollapsedDataParallel_2_82856();
void Pre_CollapsedDataParallel_2_82857();
void Pre_CollapsedDataParallel_2_82858();
void WEIGHTED_ROUND_ROBIN_Joiner_82855();
void Printer_82642();

#ifdef __cplusplus
}
#endif
#endif
