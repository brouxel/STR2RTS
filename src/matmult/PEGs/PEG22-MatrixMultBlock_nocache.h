#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=19008 on the compile command line
#else
#if BUF_SIZEMAX < 19008
#error BUF_SIZEMAX too small, it must be at least 19008
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_80614_t;

typedef struct {
	float result[3][4];
} BlockAdd_80641_t;
void BlockFloatSource_80614();
void WEIGHTED_ROUND_ROBIN_Splitter_80743();
void WEIGHTED_ROUND_ROBIN_Splitter_80751();
void Post_CollapsedDataParallel_1_80753();
void Post_CollapsedDataParallel_1_80754();
void Post_CollapsedDataParallel_1_80755();
void Post_CollapsedDataParallel_1_80756();
void Post_CollapsedDataParallel_1_80757();
void Post_CollapsedDataParallel_1_80758();
void Post_CollapsedDataParallel_1_80759();
void Post_CollapsedDataParallel_1_80760();
void Post_CollapsedDataParallel_1_80761();
void Post_CollapsedDataParallel_1_80762();
void Post_CollapsedDataParallel_1_80763();
void Post_CollapsedDataParallel_1_80764();
void WEIGHTED_ROUND_ROBIN_Joiner_80752();
void Identity_80618();
void WEIGHTED_ROUND_ROBIN_Splitter_80765();
void Pre_CollapsedDataParallel_2_80767();
void Pre_CollapsedDataParallel_2_80768();
void Pre_CollapsedDataParallel_2_80769();
void WEIGHTED_ROUND_ROBIN_Joiner_80766();
void WEIGHTED_ROUND_ROBIN_Splitter_80770();
void AutoGeneratedExpander_80772();
void AutoGeneratedExpander_80773();
void AutoGeneratedExpander_80774();
void AutoGeneratedExpander_80775();
void AutoGeneratedExpander_80776();
void AutoGeneratedExpander_80777();
void AutoGeneratedExpander_80778();
void AutoGeneratedExpander_80779();
void AutoGeneratedExpander_80780();
void AutoGeneratedExpander_80781();
void AutoGeneratedExpander_80782();
void AutoGeneratedExpander_80783();
void AutoGeneratedExpander_80784();
void AutoGeneratedExpander_80785();
void AutoGeneratedExpander_80786();
void AutoGeneratedExpander_80787();
void AutoGeneratedExpander_80788();
void AutoGeneratedExpander_80789();
void AutoGeneratedExpander_80790();
void AutoGeneratedExpander_80791();
void AutoGeneratedExpander_80792();
void AutoGeneratedExpander_80793();
void WEIGHTED_ROUND_ROBIN_Joiner_80771();
void Identity_80621();
void WEIGHTED_ROUND_ROBIN_Splitter_80794();
void Pre_CollapsedDataParallel_2_80796();
void Pre_CollapsedDataParallel_2_80797();
void Pre_CollapsedDataParallel_2_80798();
void WEIGHTED_ROUND_ROBIN_Joiner_80795();
void Identity_80625();
void Pre_CollapsedDataParallel_2_80737();
void WEIGHTED_ROUND_ROBIN_Splitter_80799();
void Post_CollapsedDataParallel_1_80801();
void Post_CollapsedDataParallel_1_80802();
void Post_CollapsedDataParallel_1_80803();
void Post_CollapsedDataParallel_1_80804();
void Post_CollapsedDataParallel_1_80805();
void Post_CollapsedDataParallel_1_80806();
void Post_CollapsedDataParallel_1_80807();
void Post_CollapsedDataParallel_1_80808();
void Post_CollapsedDataParallel_1_80809();
void WEIGHTED_ROUND_ROBIN_Joiner_80800();
void Identity_80634();
void WEIGHTED_ROUND_ROBIN_Splitter_80810();
void Pre_CollapsedDataParallel_2_80812();
void Pre_CollapsedDataParallel_2_80813();
void Pre_CollapsedDataParallel_2_80814();
void WEIGHTED_ROUND_ROBIN_Joiner_80811();
void WEIGHTED_ROUND_ROBIN_Splitter_80815();
void AutoGeneratedExpander_80817();
void AutoGeneratedExpander_80818();
void AutoGeneratedExpander_80819();
void AutoGeneratedExpander_80820();
void AutoGeneratedExpander_80821();
void AutoGeneratedExpander_80822();
void AutoGeneratedExpander_80823();
void AutoGeneratedExpander_80824();
void AutoGeneratedExpander_80825();
void AutoGeneratedExpander_80826();
void AutoGeneratedExpander_80827();
void AutoGeneratedExpander_80828();
void AutoGeneratedExpander_80829();
void AutoGeneratedExpander_80830();
void AutoGeneratedExpander_80831();
void AutoGeneratedExpander_80832();
void AutoGeneratedExpander_80833();
void AutoGeneratedExpander_80834();
void AutoGeneratedExpander_80835();
void AutoGeneratedExpander_80836();
void AutoGeneratedExpander_80837();
void AutoGeneratedExpander_80838();
void WEIGHTED_ROUND_ROBIN_Joiner_80816();
void Identity_80637();
void Pre_CollapsedDataParallel_2_80740();
void WEIGHTED_ROUND_ROBIN_Joiner_80744();
void WEIGHTED_ROUND_ROBIN_Splitter_80839();
void BlockMultiply_80841();
void BlockMultiply_80842();
void BlockMultiply_80843();
void BlockMultiply_80844();
void BlockMultiply_80845();
void BlockMultiply_80846();
void BlockMultiply_80847();
void BlockMultiply_80848();
void BlockMultiply_80849();
void BlockMultiply_80850();
void BlockMultiply_80851();
void BlockMultiply_80852();
void BlockMultiply_80853();
void BlockMultiply_80854();
void BlockMultiply_80855();
void BlockMultiply_80856();
void BlockMultiply_80857();
void BlockMultiply_80858();
void BlockMultiply_80859();
void BlockMultiply_80860();
void BlockMultiply_80861();
void BlockMultiply_80862();
void WEIGHTED_ROUND_ROBIN_Joiner_80840();
void BlockAdd_80641();
void WEIGHTED_ROUND_ROBIN_Splitter_80863();
void Post_CollapsedDataParallel_1_80865();
void Post_CollapsedDataParallel_1_80866();
void Post_CollapsedDataParallel_1_80867();
void Post_CollapsedDataParallel_1_80868();
void Post_CollapsedDataParallel_1_80869();
void Post_CollapsedDataParallel_1_80870();
void Post_CollapsedDataParallel_1_80871();
void Post_CollapsedDataParallel_1_80872();
void Post_CollapsedDataParallel_1_80873();
void WEIGHTED_ROUND_ROBIN_Joiner_80864();
void Identity_80643();
void WEIGHTED_ROUND_ROBIN_Splitter_80874();
void Pre_CollapsedDataParallel_2_80876();
void Pre_CollapsedDataParallel_2_80877();
void Pre_CollapsedDataParallel_2_80878();
void WEIGHTED_ROUND_ROBIN_Joiner_80875();
void Printer_80647();

#ifdef __cplusplus
}
#endif
#endif
