#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3456 on the compile command line
#else
#if BUF_SIZEMAX < 3456
#error BUF_SIZEMAX too small, it must be at least 3456
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_84454_t;

typedef struct {
	float result[3][4];
} BlockAdd_84481_t;
void BlockFloatSource_84454();
void WEIGHTED_ROUND_ROBIN_Splitter_84583();
void WEIGHTED_ROUND_ROBIN_Splitter_84591();
void Post_CollapsedDataParallel_1_84593();
void Post_CollapsedDataParallel_1_84594();
void Post_CollapsedDataParallel_1_84595();
void Post_CollapsedDataParallel_1_84596();
void Post_CollapsedDataParallel_1_84597();
void Post_CollapsedDataParallel_1_84598();
void Post_CollapsedDataParallel_1_84599();
void Post_CollapsedDataParallel_1_84600();
void Post_CollapsedDataParallel_1_84601();
void Post_CollapsedDataParallel_1_84602();
void Post_CollapsedDataParallel_1_84603();
void Post_CollapsedDataParallel_1_84604();
void WEIGHTED_ROUND_ROBIN_Joiner_84592();
void Identity_84458();
void WEIGHTED_ROUND_ROBIN_Splitter_84605();
void Pre_CollapsedDataParallel_2_84607();
void Pre_CollapsedDataParallel_2_84608();
void Pre_CollapsedDataParallel_2_84609();
void WEIGHTED_ROUND_ROBIN_Joiner_84606();
void WEIGHTED_ROUND_ROBIN_Splitter_84610();
void AutoGeneratedExpander_84612();
void AutoGeneratedExpander_84613();
void AutoGeneratedExpander_84614();
void AutoGeneratedExpander_84615();
void AutoGeneratedExpander_84616();
void AutoGeneratedExpander_84617();
void AutoGeneratedExpander_84618();
void AutoGeneratedExpander_84619();
void AutoGeneratedExpander_84620();
void AutoGeneratedExpander_84621();
void AutoGeneratedExpander_84622();
void AutoGeneratedExpander_84623();
void WEIGHTED_ROUND_ROBIN_Joiner_84611();
void Identity_84461();
void WEIGHTED_ROUND_ROBIN_Splitter_84624();
void Pre_CollapsedDataParallel_2_84626();
void Pre_CollapsedDataParallel_2_84627();
void Pre_CollapsedDataParallel_2_84628();
void WEIGHTED_ROUND_ROBIN_Joiner_84625();
void Identity_84465();
void Pre_CollapsedDataParallel_2_84577();
void WEIGHTED_ROUND_ROBIN_Splitter_84629();
void Post_CollapsedDataParallel_1_84631();
void Post_CollapsedDataParallel_1_84632();
void Post_CollapsedDataParallel_1_84633();
void Post_CollapsedDataParallel_1_84634();
void Post_CollapsedDataParallel_1_84635();
void Post_CollapsedDataParallel_1_84636();
void Post_CollapsedDataParallel_1_84637();
void Post_CollapsedDataParallel_1_84638();
void Post_CollapsedDataParallel_1_84639();
void WEIGHTED_ROUND_ROBIN_Joiner_84630();
void Identity_84474();
void WEIGHTED_ROUND_ROBIN_Splitter_84640();
void Pre_CollapsedDataParallel_2_84642();
void Pre_CollapsedDataParallel_2_84643();
void Pre_CollapsedDataParallel_2_84644();
void WEIGHTED_ROUND_ROBIN_Joiner_84641();
void WEIGHTED_ROUND_ROBIN_Splitter_84645();
void AutoGeneratedExpander_84647();
void AutoGeneratedExpander_84648();
void AutoGeneratedExpander_84649();
void AutoGeneratedExpander_84650();
void AutoGeneratedExpander_84651();
void AutoGeneratedExpander_84652();
void AutoGeneratedExpander_84653();
void AutoGeneratedExpander_84654();
void AutoGeneratedExpander_84655();
void AutoGeneratedExpander_84656();
void AutoGeneratedExpander_84657();
void AutoGeneratedExpander_84658();
void WEIGHTED_ROUND_ROBIN_Joiner_84646();
void Identity_84477();
void Pre_CollapsedDataParallel_2_84580();
void WEIGHTED_ROUND_ROBIN_Joiner_84584();
void WEIGHTED_ROUND_ROBIN_Splitter_84659();
void BlockMultiply_84661();
void BlockMultiply_84662();
void BlockMultiply_84663();
void BlockMultiply_84664();
void BlockMultiply_84665();
void BlockMultiply_84666();
void BlockMultiply_84667();
void BlockMultiply_84668();
void BlockMultiply_84669();
void BlockMultiply_84670();
void BlockMultiply_84671();
void BlockMultiply_84672();
void WEIGHTED_ROUND_ROBIN_Joiner_84660();
void BlockAdd_84481();
void WEIGHTED_ROUND_ROBIN_Splitter_84673();
void Post_CollapsedDataParallel_1_84675();
void Post_CollapsedDataParallel_1_84676();
void Post_CollapsedDataParallel_1_84677();
void Post_CollapsedDataParallel_1_84678();
void Post_CollapsedDataParallel_1_84679();
void Post_CollapsedDataParallel_1_84680();
void Post_CollapsedDataParallel_1_84681();
void Post_CollapsedDataParallel_1_84682();
void Post_CollapsedDataParallel_1_84683();
void WEIGHTED_ROUND_ROBIN_Joiner_84674();
void Identity_84483();
void WEIGHTED_ROUND_ROBIN_Splitter_84684();
void Pre_CollapsedDataParallel_2_84686();
void Pre_CollapsedDataParallel_2_84687();
void Pre_CollapsedDataParallel_2_84688();
void WEIGHTED_ROUND_ROBIN_Joiner_84685();
void Printer_84487();

#ifdef __cplusplus
}
#endif
#endif
