#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=9504 on the compile command line
#else
#if BUF_SIZEMAX < 9504
#error BUF_SIZEMAX too small, it must be at least 9504
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_58152_t;

typedef struct {
	float result[3][4];
} BlockAdd_58179_t;
void BlockFloatSource(buffer_float_t *chanout);
void BlockFloatSource_58152();
void WEIGHTED_ROUND_ROBIN_Splitter_58281();
void WEIGHTED_ROUND_ROBIN_Splitter_58289();
void Post_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_1_58291();
void Post_CollapsedDataParallel_1_58292();
void Post_CollapsedDataParallel_1_58293();
void Post_CollapsedDataParallel_1_58294();
void Post_CollapsedDataParallel_1_58295();
void Post_CollapsedDataParallel_1_58296();
void Post_CollapsedDataParallel_1_58297();
void Post_CollapsedDataParallel_1_58298();
void Post_CollapsedDataParallel_1_58299();
void Post_CollapsedDataParallel_1_58300();
void Post_CollapsedDataParallel_1_58301();
void Post_CollapsedDataParallel_1_58302();
void WEIGHTED_ROUND_ROBIN_Joiner_58290();
void Identity(buffer_float_t *chanin, buffer_float_t *chanout);
void Identity_58156();
void WEIGHTED_ROUND_ROBIN_Splitter_58303();
void Pre_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_2_58305();
void Pre_CollapsedDataParallel_2_58306();
void Pre_CollapsedDataParallel_2_58307();
void WEIGHTED_ROUND_ROBIN_Joiner_58304();
void WEIGHTED_ROUND_ROBIN_Splitter_58308();
void AutoGeneratedExpander(buffer_float_t *chanin, buffer_float_t *chanout);
void AutoGeneratedExpander_58310();
void AutoGeneratedExpander_58311();
void AutoGeneratedExpander_58312();
void AutoGeneratedExpander_58313();
void AutoGeneratedExpander_58314();
void AutoGeneratedExpander_58315();
void AutoGeneratedExpander_58316();
void AutoGeneratedExpander_58317();
void AutoGeneratedExpander_58318();
void AutoGeneratedExpander_58319();
void AutoGeneratedExpander_58320();
void AutoGeneratedExpander_58321();
void AutoGeneratedExpander_58322();
void AutoGeneratedExpander_58323();
void AutoGeneratedExpander_58324();
void AutoGeneratedExpander_58325();
void AutoGeneratedExpander_58326();
void AutoGeneratedExpander_58327();
void AutoGeneratedExpander_58328();
void AutoGeneratedExpander_58329();
void AutoGeneratedExpander_58330();
void AutoGeneratedExpander_58331();
void AutoGeneratedExpander_58332();
void AutoGeneratedExpander_58333();
void AutoGeneratedExpander_58334();
void AutoGeneratedExpander_58335();
void AutoGeneratedExpander_58336();
void AutoGeneratedExpander_58337();
void AutoGeneratedExpander_58338();
void AutoGeneratedExpander_58339();
void AutoGeneratedExpander_58340();
void AutoGeneratedExpander_58341();
void AutoGeneratedExpander_58342();
void AutoGeneratedExpander_58343();
void AutoGeneratedExpander_58344();
void AutoGeneratedExpander_58345();
void AutoGeneratedExpander_58346();
void AutoGeneratedExpander_58347();
void AutoGeneratedExpander_58348();
void AutoGeneratedExpander_58349();
void AutoGeneratedExpander_58350();
void AutoGeneratedExpander_58351();
void AutoGeneratedExpander_58352();
void AutoGeneratedExpander_58353();
void AutoGeneratedExpander_58354();
void AutoGeneratedExpander_58355();
void AutoGeneratedExpander_58356();
void AutoGeneratedExpander_58357();
void AutoGeneratedExpander_58358();
void AutoGeneratedExpander_58359();
void AutoGeneratedExpander_58360();
void AutoGeneratedExpander_58361();
void AutoGeneratedExpander_58362();
void AutoGeneratedExpander_58363();
void AutoGeneratedExpander_58364();
void AutoGeneratedExpander_58365();
void AutoGeneratedExpander_58366();
void AutoGeneratedExpander_58367();
void AutoGeneratedExpander_58368();
void AutoGeneratedExpander_58369();
void AutoGeneratedExpander_58370();
void AutoGeneratedExpander_58371();
void AutoGeneratedExpander_58372();
void AutoGeneratedExpander_58373();
void AutoGeneratedExpander_58374();
void AutoGeneratedExpander_58375();
void WEIGHTED_ROUND_ROBIN_Joiner_58309();
void Identity_58159();
void WEIGHTED_ROUND_ROBIN_Splitter_58376();
void Pre_CollapsedDataParallel_2_58378();
void Pre_CollapsedDataParallel_2_58379();
void Pre_CollapsedDataParallel_2_58380();
void WEIGHTED_ROUND_ROBIN_Joiner_58377();
void Identity_58163();
void Pre_CollapsedDataParallel_2_58275();
void WEIGHTED_ROUND_ROBIN_Splitter_58381();
void Post_CollapsedDataParallel_1_58383();
void Post_CollapsedDataParallel_1_58384();
void Post_CollapsedDataParallel_1_58385();
void Post_CollapsedDataParallel_1_58386();
void Post_CollapsedDataParallel_1_58387();
void Post_CollapsedDataParallel_1_58388();
void Post_CollapsedDataParallel_1_58389();
void Post_CollapsedDataParallel_1_58390();
void Post_CollapsedDataParallel_1_58391();
void WEIGHTED_ROUND_ROBIN_Joiner_58382();
void Identity_58172();
void WEIGHTED_ROUND_ROBIN_Splitter_58392();
void Pre_CollapsedDataParallel_2_58394();
void Pre_CollapsedDataParallel_2_58395();
void Pre_CollapsedDataParallel_2_58396();
void WEIGHTED_ROUND_ROBIN_Joiner_58393();
void WEIGHTED_ROUND_ROBIN_Splitter_58397();
void AutoGeneratedExpander_58399();
void AutoGeneratedExpander_58400();
void AutoGeneratedExpander_58401();
void AutoGeneratedExpander_58402();
void AutoGeneratedExpander_58403();
void AutoGeneratedExpander_58404();
void AutoGeneratedExpander_58405();
void AutoGeneratedExpander_58406();
void AutoGeneratedExpander_58407();
void AutoGeneratedExpander_58408();
void AutoGeneratedExpander_58409();
void AutoGeneratedExpander_58410();
void AutoGeneratedExpander_58411();
void AutoGeneratedExpander_58412();
void AutoGeneratedExpander_58413();
void AutoGeneratedExpander_58414();
void AutoGeneratedExpander_58415();
void AutoGeneratedExpander_58416();
void AutoGeneratedExpander_58417();
void AutoGeneratedExpander_58418();
void AutoGeneratedExpander_58419();
void AutoGeneratedExpander_58420();
void AutoGeneratedExpander_58421();
void AutoGeneratedExpander_58422();
void AutoGeneratedExpander_58423();
void AutoGeneratedExpander_58424();
void AutoGeneratedExpander_58425();
void AutoGeneratedExpander_58426();
void AutoGeneratedExpander_58427();
void AutoGeneratedExpander_58428();
void AutoGeneratedExpander_58429();
void AutoGeneratedExpander_58430();
void AutoGeneratedExpander_58431();
void AutoGeneratedExpander_58432();
void AutoGeneratedExpander_58433();
void AutoGeneratedExpander_58434();
void AutoGeneratedExpander_58435();
void AutoGeneratedExpander_58436();
void AutoGeneratedExpander_58437();
void AutoGeneratedExpander_58438();
void AutoGeneratedExpander_58439();
void AutoGeneratedExpander_58440();
void AutoGeneratedExpander_58441();
void AutoGeneratedExpander_58442();
void AutoGeneratedExpander_58443();
void AutoGeneratedExpander_58444();
void AutoGeneratedExpander_58445();
void AutoGeneratedExpander_58446();
void AutoGeneratedExpander_58447();
void AutoGeneratedExpander_58448();
void AutoGeneratedExpander_58449();
void AutoGeneratedExpander_58450();
void AutoGeneratedExpander_58451();
void AutoGeneratedExpander_58452();
void AutoGeneratedExpander_58453();
void AutoGeneratedExpander_58454();
void AutoGeneratedExpander_58455();
void AutoGeneratedExpander_58456();
void AutoGeneratedExpander_58457();
void AutoGeneratedExpander_58458();
void AutoGeneratedExpander_58459();
void AutoGeneratedExpander_58460();
void AutoGeneratedExpander_58461();
void AutoGeneratedExpander_58462();
void AutoGeneratedExpander_58463();
void AutoGeneratedExpander_58464();
void WEIGHTED_ROUND_ROBIN_Joiner_58398();
void Identity_58175();
void Pre_CollapsedDataParallel_2_58278();
void WEIGHTED_ROUND_ROBIN_Joiner_58282();
void WEIGHTED_ROUND_ROBIN_Splitter_58465();
void BlockMultiply(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockMultiply_58467();
void BlockMultiply_58468();
void BlockMultiply_58469();
void BlockMultiply_58470();
void BlockMultiply_58471();
void BlockMultiply_58472();
void BlockMultiply_58473();
void BlockMultiply_58474();
void BlockMultiply_58475();
void BlockMultiply_58476();
void BlockMultiply_58477();
void BlockMultiply_58478();
void BlockMultiply_58479();
void BlockMultiply_58480();
void BlockMultiply_58481();
void BlockMultiply_58482();
void BlockMultiply_58483();
void BlockMultiply_58484();
void BlockMultiply_58485();
void BlockMultiply_58486();
void BlockMultiply_58487();
void BlockMultiply_58488();
void BlockMultiply_58489();
void BlockMultiply_58490();
void BlockMultiply_58491();
void BlockMultiply_58492();
void BlockMultiply_58493();
void WEIGHTED_ROUND_ROBIN_Joiner_58466();
void BlockAdd(buffer_float_t *chanin, buffer_float_t *chanout);
void BlockAdd_58179();
void WEIGHTED_ROUND_ROBIN_Splitter_58494();
void Post_CollapsedDataParallel_1_58496();
void Post_CollapsedDataParallel_1_58497();
void Post_CollapsedDataParallel_1_58498();
void Post_CollapsedDataParallel_1_58499();
void Post_CollapsedDataParallel_1_58500();
void Post_CollapsedDataParallel_1_58501();
void Post_CollapsedDataParallel_1_58502();
void Post_CollapsedDataParallel_1_58503();
void Post_CollapsedDataParallel_1_58504();
void WEIGHTED_ROUND_ROBIN_Joiner_58495();
void Identity_58181();
void WEIGHTED_ROUND_ROBIN_Splitter_58505();
void Pre_CollapsedDataParallel_2_58507();
void Pre_CollapsedDataParallel_2_58508();
void Pre_CollapsedDataParallel_2_58509();
void WEIGHTED_ROUND_ROBIN_Joiner_58506();
void Printer(buffer_float_t *chanin);
void Printer_58185();

#ifdef __cplusplus
}
#endif
#endif
