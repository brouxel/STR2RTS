#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=40608 on the compile command line
#else
#if BUF_SIZEMAX < 40608
#error BUF_SIZEMAX too small, it must be at least 40608
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} BlockFloatSource_68811_t;

typedef struct {
	float result[3][4];
} BlockAdd_68838_t;
void BlockFloatSource_68811();
void WEIGHTED_ROUND_ROBIN_Splitter_68940();
void WEIGHTED_ROUND_ROBIN_Splitter_68948();
void Post_CollapsedDataParallel_1_68950();
void Post_CollapsedDataParallel_1_68951();
void Post_CollapsedDataParallel_1_68952();
void Post_CollapsedDataParallel_1_68953();
void Post_CollapsedDataParallel_1_68954();
void Post_CollapsedDataParallel_1_68955();
void Post_CollapsedDataParallel_1_68956();
void Post_CollapsedDataParallel_1_68957();
void Post_CollapsedDataParallel_1_68958();
void Post_CollapsedDataParallel_1_68959();
void Post_CollapsedDataParallel_1_68960();
void Post_CollapsedDataParallel_1_68961();
void WEIGHTED_ROUND_ROBIN_Joiner_68949();
void Identity_68815();
void WEIGHTED_ROUND_ROBIN_Splitter_68962();
void Pre_CollapsedDataParallel_2_68964();
void Pre_CollapsedDataParallel_2_68965();
void Pre_CollapsedDataParallel_2_68966();
void WEIGHTED_ROUND_ROBIN_Joiner_68963();
void WEIGHTED_ROUND_ROBIN_Splitter_68967();
void AutoGeneratedExpander_68969();
void AutoGeneratedExpander_68970();
void AutoGeneratedExpander_68971();
void AutoGeneratedExpander_68972();
void AutoGeneratedExpander_68973();
void AutoGeneratedExpander_68974();
void AutoGeneratedExpander_68975();
void AutoGeneratedExpander_68976();
void AutoGeneratedExpander_68977();
void AutoGeneratedExpander_68978();
void AutoGeneratedExpander_68979();
void AutoGeneratedExpander_68980();
void AutoGeneratedExpander_68981();
void AutoGeneratedExpander_68982();
void AutoGeneratedExpander_68983();
void AutoGeneratedExpander_68984();
void AutoGeneratedExpander_68985();
void AutoGeneratedExpander_68986();
void AutoGeneratedExpander_68987();
void AutoGeneratedExpander_68988();
void AutoGeneratedExpander_68989();
void AutoGeneratedExpander_68990();
void AutoGeneratedExpander_68991();
void AutoGeneratedExpander_68992();
void AutoGeneratedExpander_68993();
void AutoGeneratedExpander_68994();
void AutoGeneratedExpander_68995();
void AutoGeneratedExpander_68996();
void AutoGeneratedExpander_68997();
void AutoGeneratedExpander_68998();
void AutoGeneratedExpander_68999();
void AutoGeneratedExpander_69000();
void AutoGeneratedExpander_69001();
void AutoGeneratedExpander_69002();
void AutoGeneratedExpander_69003();
void AutoGeneratedExpander_69004();
void AutoGeneratedExpander_69005();
void AutoGeneratedExpander_69006();
void AutoGeneratedExpander_69007();
void AutoGeneratedExpander_69008();
void AutoGeneratedExpander_69009();
void AutoGeneratedExpander_69010();
void AutoGeneratedExpander_69011();
void AutoGeneratedExpander_69012();
void AutoGeneratedExpander_69013();
void AutoGeneratedExpander_69014();
void AutoGeneratedExpander_69015();
void WEIGHTED_ROUND_ROBIN_Joiner_68968();
void Identity_68818();
void WEIGHTED_ROUND_ROBIN_Splitter_69016();
void Pre_CollapsedDataParallel_2_69018();
void Pre_CollapsedDataParallel_2_69019();
void Pre_CollapsedDataParallel_2_69020();
void WEIGHTED_ROUND_ROBIN_Joiner_69017();
void Identity_68822();
void Pre_CollapsedDataParallel_2_68934();
void WEIGHTED_ROUND_ROBIN_Splitter_69021();
void Post_CollapsedDataParallel_1_69023();
void Post_CollapsedDataParallel_1_69024();
void Post_CollapsedDataParallel_1_69025();
void Post_CollapsedDataParallel_1_69026();
void Post_CollapsedDataParallel_1_69027();
void Post_CollapsedDataParallel_1_69028();
void Post_CollapsedDataParallel_1_69029();
void Post_CollapsedDataParallel_1_69030();
void Post_CollapsedDataParallel_1_69031();
void WEIGHTED_ROUND_ROBIN_Joiner_69022();
void Identity_68831();
void WEIGHTED_ROUND_ROBIN_Splitter_69032();
void Pre_CollapsedDataParallel_2_69034();
void Pre_CollapsedDataParallel_2_69035();
void Pre_CollapsedDataParallel_2_69036();
void WEIGHTED_ROUND_ROBIN_Joiner_69033();
void WEIGHTED_ROUND_ROBIN_Splitter_69037();
void AutoGeneratedExpander_69039();
void AutoGeneratedExpander_69040();
void AutoGeneratedExpander_69041();
void AutoGeneratedExpander_69042();
void AutoGeneratedExpander_69043();
void AutoGeneratedExpander_69044();
void AutoGeneratedExpander_69045();
void AutoGeneratedExpander_69046();
void AutoGeneratedExpander_69047();
void AutoGeneratedExpander_69048();
void AutoGeneratedExpander_69049();
void AutoGeneratedExpander_69050();
void AutoGeneratedExpander_69051();
void AutoGeneratedExpander_69052();
void AutoGeneratedExpander_69053();
void AutoGeneratedExpander_69054();
void AutoGeneratedExpander_69055();
void AutoGeneratedExpander_69056();
void AutoGeneratedExpander_69057();
void AutoGeneratedExpander_69058();
void AutoGeneratedExpander_69059();
void AutoGeneratedExpander_69060();
void AutoGeneratedExpander_69061();
void AutoGeneratedExpander_69062();
void AutoGeneratedExpander_69063();
void AutoGeneratedExpander_69064();
void AutoGeneratedExpander_69065();
void AutoGeneratedExpander_69066();
void AutoGeneratedExpander_69067();
void AutoGeneratedExpander_69068();
void AutoGeneratedExpander_69069();
void AutoGeneratedExpander_69070();
void AutoGeneratedExpander_69071();
void AutoGeneratedExpander_69072();
void AutoGeneratedExpander_69073();
void AutoGeneratedExpander_69074();
void AutoGeneratedExpander_69075();
void AutoGeneratedExpander_69076();
void AutoGeneratedExpander_69077();
void AutoGeneratedExpander_69078();
void AutoGeneratedExpander_69079();
void AutoGeneratedExpander_69080();
void AutoGeneratedExpander_69081();
void AutoGeneratedExpander_69082();
void AutoGeneratedExpander_69083();
void AutoGeneratedExpander_69084();
void AutoGeneratedExpander_69085();
void WEIGHTED_ROUND_ROBIN_Joiner_69038();
void Identity_68834();
void Pre_CollapsedDataParallel_2_68937();
void WEIGHTED_ROUND_ROBIN_Joiner_68941();
void WEIGHTED_ROUND_ROBIN_Splitter_69086();
void BlockMultiply_69088();
void BlockMultiply_69089();
void BlockMultiply_69090();
void BlockMultiply_69091();
void BlockMultiply_69092();
void BlockMultiply_69093();
void BlockMultiply_69094();
void BlockMultiply_69095();
void BlockMultiply_69096();
void BlockMultiply_69097();
void BlockMultiply_69098();
void BlockMultiply_69099();
void BlockMultiply_69100();
void BlockMultiply_69101();
void BlockMultiply_69102();
void BlockMultiply_69103();
void BlockMultiply_69104();
void BlockMultiply_69105();
void BlockMultiply_69106();
void BlockMultiply_69107();
void BlockMultiply_69108();
void BlockMultiply_69109();
void BlockMultiply_69110();
void BlockMultiply_69111();
void BlockMultiply_69112();
void BlockMultiply_69113();
void BlockMultiply_69114();
void WEIGHTED_ROUND_ROBIN_Joiner_69087();
void BlockAdd_68838();
void WEIGHTED_ROUND_ROBIN_Splitter_69115();
void Post_CollapsedDataParallel_1_69117();
void Post_CollapsedDataParallel_1_69118();
void Post_CollapsedDataParallel_1_69119();
void Post_CollapsedDataParallel_1_69120();
void Post_CollapsedDataParallel_1_69121();
void Post_CollapsedDataParallel_1_69122();
void Post_CollapsedDataParallel_1_69123();
void Post_CollapsedDataParallel_1_69124();
void Post_CollapsedDataParallel_1_69125();
void WEIGHTED_ROUND_ROBIN_Joiner_69116();
void Identity_68840();
void WEIGHTED_ROUND_ROBIN_Splitter_69126();
void Pre_CollapsedDataParallel_2_69128();
void Pre_CollapsedDataParallel_2_69129();
void Pre_CollapsedDataParallel_2_69130();
void WEIGHTED_ROUND_ROBIN_Joiner_69127();
void Printer_68844();

#ifdef __cplusplus
}
#endif
#endif
