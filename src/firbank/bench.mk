BENCH:=sdf_firbank_nocache
BENCHSRCFILE:=$(SRC_DIR)/firbank/SDF-FIRBank_nocache.c
BENCHHFILE:=$(SRC_DIR)/firbank/SDF-FIRBank_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=768

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

BENCH:=hsdf_firbank_nocache
BENCHSRCFILE:=$(SRC_DIR)/firbank/HSDF-FIRBank_nocache.c
BENCHHFILE:=$(SRC_DIR)/firbank/HSDF-FIRBank_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=768

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

#$(foreach b, $(shell ls $(SRC_DIR)/firbank/PEGs/PEG*.h), \
   $(eval $(call BENCH_TEMPLATE, \
		 $(shell echo $$(basename -s'.h' $(b)) | tr A-Z a-z | tr - _), \
		 $(shell dirname $(b))/$(shell basename -s'.h' $(b)).c, \
		 $(b), \
		 $(shell grep "\-DBUF_SIZEMAX=" $(b) | sed -e 's/.*\(-DBUF_SIZEMAX=[0-9]*\).*/\1/')))\
)
