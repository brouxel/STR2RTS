#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=512 on the compile command line
#else
#if BUF_SIZEMAX < 512
#error BUF_SIZEMAX too small, it must be at least 512
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	int USERKEY;
	int USERKEYS[5][8];
	int IP[128];
	int SBOXES[8][16];
} TheGlobal_t;

typedef struct {
	int TEXT[5][4];
} PlainTextSourceAux_124_t;

typedef struct {
	int permutation[128];
} Permute_126_t;

typedef struct {
	int keys[33][128];
} KeySchedule_130_t;
void PlainTextSourceAux_124();
void IntoBits_125();
void Permute_126();
void WEIGHTED_ROUND_ROBIN_Splitter_358();
void Identity_129();
int KeySchedule_130_LRotate(int x, int n);
void KeySchedule_130();
void WEIGHTED_ROUND_ROBIN_Joiner_359();
void Xor_131();
void Sbox_132();
void rawL_133();
void WEIGHTED_ROUND_ROBIN_Splitter_360();
void Identity_136();
int KeySchedule_137_LRotate(int x, int n);
void KeySchedule_137();
void WEIGHTED_ROUND_ROBIN_Joiner_361();
void Xor_138();
void Sbox_139();
void rawL_140();
void WEIGHTED_ROUND_ROBIN_Splitter_362();
void Identity_143();
int KeySchedule_144_LRotate(int x, int n);
void KeySchedule_144();
void WEIGHTED_ROUND_ROBIN_Joiner_363();
void Xor_145();
void Sbox_146();
void rawL_147();
void WEIGHTED_ROUND_ROBIN_Splitter_364();
void Identity_150();
int KeySchedule_151_LRotate(int x, int n);
void KeySchedule_151();
void WEIGHTED_ROUND_ROBIN_Joiner_365();
void Xor_152();
void Sbox_153();
void rawL_154();
void WEIGHTED_ROUND_ROBIN_Splitter_366();
void Identity_157();
int KeySchedule_158_LRotate(int x, int n);
void KeySchedule_158();
void WEIGHTED_ROUND_ROBIN_Joiner_367();
void Xor_159();
void Sbox_160();
void rawL_161();
void WEIGHTED_ROUND_ROBIN_Splitter_368();
void Identity_164();
int KeySchedule_165_LRotate(int x, int n);
void KeySchedule_165();
void WEIGHTED_ROUND_ROBIN_Joiner_369();
void Xor_166();
void Sbox_167();
void rawL_168();
void WEIGHTED_ROUND_ROBIN_Splitter_370();
void Identity_171();
int KeySchedule_172_LRotate(int x, int n);
void KeySchedule_172();
void WEIGHTED_ROUND_ROBIN_Joiner_371();
void Xor_173();
void Sbox_174();
void rawL_175();
void WEIGHTED_ROUND_ROBIN_Splitter_372();
void Identity_178();
int KeySchedule_179_LRotate(int x, int n);
void KeySchedule_179();
void WEIGHTED_ROUND_ROBIN_Joiner_373();
void Xor_180();
void Sbox_181();
void rawL_182();
void WEIGHTED_ROUND_ROBIN_Splitter_374();
void Identity_185();
int KeySchedule_186_LRotate(int x, int n);
void KeySchedule_186();
void WEIGHTED_ROUND_ROBIN_Joiner_375();
void Xor_187();
void Sbox_188();
void rawL_189();
void WEIGHTED_ROUND_ROBIN_Splitter_376();
void Identity_192();
int KeySchedule_193_LRotate(int x, int n);
void KeySchedule_193();
void WEIGHTED_ROUND_ROBIN_Joiner_377();
void Xor_194();
void Sbox_195();
void rawL_196();
void WEIGHTED_ROUND_ROBIN_Splitter_378();
void Identity_199();
int KeySchedule_200_LRotate(int x, int n);
void KeySchedule_200();
void WEIGHTED_ROUND_ROBIN_Joiner_379();
void Xor_201();
void Sbox_202();
void rawL_203();
void WEIGHTED_ROUND_ROBIN_Splitter_380();
void Identity_206();
int KeySchedule_207_LRotate(int x, int n);
void KeySchedule_207();
void WEIGHTED_ROUND_ROBIN_Joiner_381();
void Xor_208();
void Sbox_209();
void rawL_210();
void WEIGHTED_ROUND_ROBIN_Splitter_382();
void Identity_213();
int KeySchedule_214_LRotate(int x, int n);
void KeySchedule_214();
void WEIGHTED_ROUND_ROBIN_Joiner_383();
void Xor_215();
void Sbox_216();
void rawL_217();
void WEIGHTED_ROUND_ROBIN_Splitter_384();
void Identity_220();
int KeySchedule_221_LRotate(int x, int n);
void KeySchedule_221();
void WEIGHTED_ROUND_ROBIN_Joiner_385();
void Xor_222();
void Sbox_223();
void rawL_224();
void WEIGHTED_ROUND_ROBIN_Splitter_386();
void Identity_227();
int KeySchedule_228_LRotate(int x, int n);
void KeySchedule_228();
void WEIGHTED_ROUND_ROBIN_Joiner_387();
void Xor_229();
void Sbox_230();
void rawL_231();
void WEIGHTED_ROUND_ROBIN_Splitter_388();
void Identity_234();
int KeySchedule_235_LRotate(int x, int n);
void KeySchedule_235();
void WEIGHTED_ROUND_ROBIN_Joiner_389();
void Xor_236();
void Sbox_237();
void rawL_238();
void WEIGHTED_ROUND_ROBIN_Splitter_390();
void Identity_241();
int KeySchedule_242_LRotate(int x, int n);
void KeySchedule_242();
void WEIGHTED_ROUND_ROBIN_Joiner_391();
void Xor_243();
void Sbox_244();
void rawL_245();
void WEIGHTED_ROUND_ROBIN_Splitter_392();
void Identity_248();
int KeySchedule_249_LRotate(int x, int n);
void KeySchedule_249();
void WEIGHTED_ROUND_ROBIN_Joiner_393();
void Xor_250();
void Sbox_251();
void rawL_252();
void WEIGHTED_ROUND_ROBIN_Splitter_394();
void Identity_255();
int KeySchedule_256_LRotate(int x, int n);
void KeySchedule_256();
void WEIGHTED_ROUND_ROBIN_Joiner_395();
void Xor_257();
void Sbox_258();
void rawL_259();
void WEIGHTED_ROUND_ROBIN_Splitter_396();
void Identity_262();
int KeySchedule_263_LRotate(int x, int n);
void KeySchedule_263();
void WEIGHTED_ROUND_ROBIN_Joiner_397();
void Xor_264();
void Sbox_265();
void rawL_266();
void WEIGHTED_ROUND_ROBIN_Splitter_398();
void Identity_269();
int KeySchedule_270_LRotate(int x, int n);
void KeySchedule_270();
void WEIGHTED_ROUND_ROBIN_Joiner_399();
void Xor_271();
void Sbox_272();
void rawL_273();
void WEIGHTED_ROUND_ROBIN_Splitter_400();
void Identity_276();
int KeySchedule_277_LRotate(int x, int n);
void KeySchedule_277();
void WEIGHTED_ROUND_ROBIN_Joiner_401();
void Xor_278();
void Sbox_279();
void rawL_280();
void WEIGHTED_ROUND_ROBIN_Splitter_402();
void Identity_283();
int KeySchedule_284_LRotate(int x, int n);
void KeySchedule_284();
void WEIGHTED_ROUND_ROBIN_Joiner_403();
void Xor_285();
void Sbox_286();
void rawL_287();
void WEIGHTED_ROUND_ROBIN_Splitter_404();
void Identity_290();
int KeySchedule_291_LRotate(int x, int n);
void KeySchedule_291();
void WEIGHTED_ROUND_ROBIN_Joiner_405();
void Xor_292();
void Sbox_293();
void rawL_294();
void WEIGHTED_ROUND_ROBIN_Splitter_406();
void Identity_297();
int KeySchedule_298_LRotate(int x, int n);
void KeySchedule_298();
void WEIGHTED_ROUND_ROBIN_Joiner_407();
void Xor_299();
void Sbox_300();
void rawL_301();
void WEIGHTED_ROUND_ROBIN_Splitter_408();
void Identity_304();
int KeySchedule_305_LRotate(int x, int n);
void KeySchedule_305();
void WEIGHTED_ROUND_ROBIN_Joiner_409();
void Xor_306();
void Sbox_307();
void rawL_308();
void WEIGHTED_ROUND_ROBIN_Splitter_410();
void Identity_311();
int KeySchedule_312_LRotate(int x, int n);
void KeySchedule_312();
void WEIGHTED_ROUND_ROBIN_Joiner_411();
void Xor_313();
void Sbox_314();
void rawL_315();
void WEIGHTED_ROUND_ROBIN_Splitter_412();
void Identity_318();
int KeySchedule_319_LRotate(int x, int n);
void KeySchedule_319();
void WEIGHTED_ROUND_ROBIN_Joiner_413();
void Xor_320();
void Sbox_321();
void rawL_322();
void WEIGHTED_ROUND_ROBIN_Splitter_414();
void Identity_325();
int KeySchedule_326_LRotate(int x, int n);
void KeySchedule_326();
void WEIGHTED_ROUND_ROBIN_Joiner_415();
void Xor_327();
void Sbox_328();
void rawL_329();
void WEIGHTED_ROUND_ROBIN_Splitter_416();
void Identity_332();
int KeySchedule_333_LRotate(int x, int n);
void KeySchedule_333();
void WEIGHTED_ROUND_ROBIN_Joiner_417();
void Xor_334();
void Sbox_335();
void rawL_336();
void WEIGHTED_ROUND_ROBIN_Splitter_418();
void Identity_339();
int KeySchedule_340_LRotate(int x, int n);
void KeySchedule_340();
void WEIGHTED_ROUND_ROBIN_Joiner_419();
void Xor_341();
void Sbox_342();
void rawL_343();
void WEIGHTED_ROUND_ROBIN_Splitter_420();
void Identity_346();
int KeySchedule_347_LRotate(int x, int n);
void KeySchedule_347();
void WEIGHTED_ROUND_ROBIN_Joiner_421();
void Xor_348();
void Sbox_349();
void WEIGHTED_ROUND_ROBIN_Splitter_422();
void Identity_351();
int KeySchedule_352_LRotate(int x, int n);
void KeySchedule_352();
void WEIGHTED_ROUND_ROBIN_Joiner_423();
void Xor_353();
void Permute_354();
void BitstoInts_356();
void HexPrinterAux_357();

#ifdef __cplusplus
}
#endif
#endif
