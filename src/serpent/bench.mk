BENCH:=sdf_serpent_nocache
BENCHSRCFILE:=$(SRC_DIR)/serpent/SDF-Serpent_nocache.c
BENCHHFILE:=$(SRC_DIR)/serpent/SDF-Serpent_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=512

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

BENCH:=hsdf_serpent_nocache
BENCHSRCFILE:=$(SRC_DIR)/serpent/HSDF-Serpent_nocache.c
BENCHHFILE:=$(SRC_DIR)/serpent/HSDF-Serpent_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=256

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

#$(foreach b, $(shell ls $(SRC_DIR)/serpent/PEGs/PEG*.h), \
   $(eval $(call BENCH_TEMPLATE, \
		 $(shell echo $$(basename -s'.h' $(b)) | tr A-Z a-z | tr - _), \
		 $(shell dirname $(b))/$(shell basename -s'.h' $(b)).c, \
		 $(b), \
		 $(shell grep "\-DBUF_SIZEMAX=" $(b) | sed -e 's/.*\(-DBUF_SIZEMAX=[0-9]*\).*/\1/')))\
)