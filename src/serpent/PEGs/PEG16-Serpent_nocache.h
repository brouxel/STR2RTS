#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=256 on the compile command line
#else
#if BUF_SIZEMAX < 256
#error BUF_SIZEMAX too small, it must be at least 256
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	int USERKEY;
	int USERKEYS[5][8];
	int IP[128];
	int SBOXES[8][16];
} TheGlobal_t;

typedef struct {
	int TEXT[5][4];
} PlainTextSourceAux_41801_t;

typedef struct {
	int permutation[128];
} Permute_41803_t;

typedef struct {
	int keys[33][128];
} KeySchedule_41807_t;
void PlainTextSourceAux_41801();
void WEIGHTED_ROUND_ROBIN_Splitter_42201();
void IntoBits_42203();
void IntoBits_42204();
void IntoBits_42205();
void IntoBits_42206();
void WEIGHTED_ROUND_ROBIN_Joiner_42202();
void Permute_41803();
void WEIGHTED_ROUND_ROBIN_Splitter_42035();
void Identity_41806();
int KeySchedule_41807_LRotate(int x, int n);
void KeySchedule_41807();
void WEIGHTED_ROUND_ROBIN_Joiner_42036();
void WEIGHTED_ROUND_ROBIN_Splitter_42207();
void Xor_42209();
void Xor_42210();
void Xor_42211();
void Xor_42212();
void Xor_42213();
void Xor_42214();
void Xor_42215();
void Xor_42216();
void Xor_42217();
void Xor_42218();
void Xor_42219();
void Xor_42220();
void Xor_42221();
void Xor_42222();
void Xor_42223();
void Xor_42224();
void WEIGHTED_ROUND_ROBIN_Joiner_42208();
void WEIGHTED_ROUND_ROBIN_Splitter_42225();
void Sbox_42227();
void Sbox_42228();
void Sbox_42229();
void Sbox_42230();
void Sbox_42231();
void Sbox_42232();
void Sbox_42233();
void Sbox_42234();
void Sbox_42235();
void Sbox_42236();
void Sbox_42237();
void Sbox_42238();
void Sbox_42239();
void Sbox_42240();
void Sbox_42241();
void Sbox_42242();
void WEIGHTED_ROUND_ROBIN_Joiner_42226();
void rawL_41810();
void WEIGHTED_ROUND_ROBIN_Splitter_42037();
void Identity_41813();
int KeySchedule_41814_LRotate(int x, int n);
void KeySchedule_41814();
void WEIGHTED_ROUND_ROBIN_Joiner_42038();
void WEIGHTED_ROUND_ROBIN_Splitter_42243();
void Xor_42245();
void Xor_42246();
void Xor_42247();
void Xor_42248();
void Xor_42249();
void Xor_42250();
void Xor_42251();
void Xor_42252();
void Xor_42253();
void Xor_42254();
void Xor_42255();
void Xor_42256();
void Xor_42257();
void Xor_42258();
void Xor_42259();
void Xor_42260();
void WEIGHTED_ROUND_ROBIN_Joiner_42244();
void WEIGHTED_ROUND_ROBIN_Splitter_42261();
void Sbox_42263();
void Sbox_42264();
void Sbox_42265();
void Sbox_42266();
void Sbox_42267();
void Sbox_42268();
void Sbox_42269();
void Sbox_42270();
void Sbox_42271();
void Sbox_42272();
void Sbox_42273();
void Sbox_42274();
void Sbox_42275();
void Sbox_42276();
void Sbox_42277();
void Sbox_42278();
void WEIGHTED_ROUND_ROBIN_Joiner_42262();
void rawL_41817();
void WEIGHTED_ROUND_ROBIN_Splitter_42039();
void Identity_41820();
int KeySchedule_41821_LRotate(int x, int n);
void KeySchedule_41821();
void WEIGHTED_ROUND_ROBIN_Joiner_42040();
void WEIGHTED_ROUND_ROBIN_Splitter_42279();
void Xor_42281();
void Xor_42282();
void Xor_42283();
void Xor_42284();
void Xor_42285();
void Xor_42286();
void Xor_42287();
void Xor_42288();
void Xor_42289();
void Xor_42290();
void Xor_42291();
void Xor_42292();
void Xor_42293();
void Xor_42294();
void Xor_42295();
void Xor_42296();
void WEIGHTED_ROUND_ROBIN_Joiner_42280();
void WEIGHTED_ROUND_ROBIN_Splitter_42297();
void Sbox_42299();
void Sbox_42300();
void Sbox_42301();
void Sbox_42302();
void Sbox_42303();
void Sbox_42304();
void Sbox_42305();
void Sbox_42306();
void Sbox_42307();
void Sbox_42308();
void Sbox_42309();
void Sbox_42310();
void Sbox_42311();
void Sbox_42312();
void Sbox_42313();
void Sbox_42314();
void WEIGHTED_ROUND_ROBIN_Joiner_42298();
void rawL_41824();
void WEIGHTED_ROUND_ROBIN_Splitter_42041();
void Identity_41827();
int KeySchedule_41828_LRotate(int x, int n);
void KeySchedule_41828();
void WEIGHTED_ROUND_ROBIN_Joiner_42042();
void WEIGHTED_ROUND_ROBIN_Splitter_42315();
void Xor_42317();
void Xor_42318();
void Xor_42319();
void Xor_42320();
void Xor_42321();
void Xor_42322();
void Xor_42323();
void Xor_42324();
void Xor_42325();
void Xor_42326();
void Xor_42327();
void Xor_42328();
void Xor_42329();
void Xor_42330();
void Xor_42331();
void Xor_42332();
void WEIGHTED_ROUND_ROBIN_Joiner_42316();
void WEIGHTED_ROUND_ROBIN_Splitter_42333();
void Sbox_42335();
void Sbox_42336();
void Sbox_42337();
void Sbox_42338();
void Sbox_42339();
void Sbox_42340();
void Sbox_42341();
void Sbox_42342();
void Sbox_42343();
void Sbox_42344();
void Sbox_42345();
void Sbox_42346();
void Sbox_42347();
void Sbox_42348();
void Sbox_42349();
void Sbox_42350();
void WEIGHTED_ROUND_ROBIN_Joiner_42334();
void rawL_41831();
void WEIGHTED_ROUND_ROBIN_Splitter_42043();
void Identity_41834();
int KeySchedule_41835_LRotate(int x, int n);
void KeySchedule_41835();
void WEIGHTED_ROUND_ROBIN_Joiner_42044();
void WEIGHTED_ROUND_ROBIN_Splitter_42351();
void Xor_42353();
void Xor_42354();
void Xor_42355();
void Xor_42356();
void Xor_42357();
void Xor_42358();
void Xor_42359();
void Xor_42360();
void Xor_42361();
void Xor_42362();
void Xor_42363();
void Xor_42364();
void Xor_42365();
void Xor_42366();
void Xor_42367();
void Xor_42368();
void WEIGHTED_ROUND_ROBIN_Joiner_42352();
void WEIGHTED_ROUND_ROBIN_Splitter_42369();
void Sbox_42371();
void Sbox_42372();
void Sbox_42373();
void Sbox_42374();
void Sbox_42375();
void Sbox_42376();
void Sbox_42377();
void Sbox_42378();
void Sbox_42379();
void Sbox_42380();
void Sbox_42381();
void Sbox_42382();
void Sbox_42383();
void Sbox_42384();
void Sbox_42385();
void Sbox_42386();
void WEIGHTED_ROUND_ROBIN_Joiner_42370();
void rawL_41838();
void WEIGHTED_ROUND_ROBIN_Splitter_42045();
void Identity_41841();
int KeySchedule_41842_LRotate(int x, int n);
void KeySchedule_41842();
void WEIGHTED_ROUND_ROBIN_Joiner_42046();
void WEIGHTED_ROUND_ROBIN_Splitter_42387();
void Xor_42389();
void Xor_42390();
void Xor_42391();
void Xor_42392();
void Xor_42393();
void Xor_42394();
void Xor_42395();
void Xor_42396();
void Xor_42397();
void Xor_42398();
void Xor_42399();
void Xor_42400();
void Xor_42401();
void Xor_42402();
void Xor_42403();
void Xor_42404();
void WEIGHTED_ROUND_ROBIN_Joiner_42388();
void WEIGHTED_ROUND_ROBIN_Splitter_42405();
void Sbox_42407();
void Sbox_42408();
void Sbox_42409();
void Sbox_42410();
void Sbox_42411();
void Sbox_42412();
void Sbox_42413();
void Sbox_42414();
void Sbox_42415();
void Sbox_42416();
void Sbox_42417();
void Sbox_42418();
void Sbox_42419();
void Sbox_42420();
void Sbox_42421();
void Sbox_42422();
void WEIGHTED_ROUND_ROBIN_Joiner_42406();
void rawL_41845();
void WEIGHTED_ROUND_ROBIN_Splitter_42047();
void Identity_41848();
int KeySchedule_41849_LRotate(int x, int n);
void KeySchedule_41849();
void WEIGHTED_ROUND_ROBIN_Joiner_42048();
void WEIGHTED_ROUND_ROBIN_Splitter_42423();
void Xor_42425();
void Xor_42426();
void Xor_42427();
void Xor_42428();
void Xor_42429();
void Xor_42430();
void Xor_42431();
void Xor_42432();
void Xor_42433();
void Xor_42434();
void Xor_42435();
void Xor_42436();
void Xor_42437();
void Xor_42438();
void Xor_42439();
void Xor_42440();
void WEIGHTED_ROUND_ROBIN_Joiner_42424();
void WEIGHTED_ROUND_ROBIN_Splitter_42441();
void Sbox_42443();
void Sbox_42444();
void Sbox_42445();
void Sbox_42446();
void Sbox_42447();
void Sbox_42448();
void Sbox_42449();
void Sbox_42450();
void Sbox_42451();
void Sbox_42452();
void Sbox_42453();
void Sbox_42454();
void Sbox_42455();
void Sbox_42456();
void Sbox_42457();
void Sbox_42458();
void WEIGHTED_ROUND_ROBIN_Joiner_42442();
void rawL_41852();
void WEIGHTED_ROUND_ROBIN_Splitter_42049();
void Identity_41855();
int KeySchedule_41856_LRotate(int x, int n);
void KeySchedule_41856();
void WEIGHTED_ROUND_ROBIN_Joiner_42050();
void WEIGHTED_ROUND_ROBIN_Splitter_42459();
void Xor_42461();
void Xor_42462();
void Xor_42463();
void Xor_42464();
void Xor_42465();
void Xor_42466();
void Xor_42467();
void Xor_42468();
void Xor_42469();
void Xor_42470();
void Xor_42471();
void Xor_42472();
void Xor_42473();
void Xor_42474();
void Xor_42475();
void Xor_42476();
void WEIGHTED_ROUND_ROBIN_Joiner_42460();
void WEIGHTED_ROUND_ROBIN_Splitter_42477();
void Sbox_42479();
void Sbox_42480();
void Sbox_42481();
void Sbox_42482();
void Sbox_42483();
void Sbox_42484();
void Sbox_42485();
void Sbox_42486();
void Sbox_42487();
void Sbox_42488();
void Sbox_42489();
void Sbox_42490();
void Sbox_42491();
void Sbox_42492();
void Sbox_42493();
void Sbox_42494();
void WEIGHTED_ROUND_ROBIN_Joiner_42478();
void rawL_41859();
void WEIGHTED_ROUND_ROBIN_Splitter_42051();
void Identity_41862();
int KeySchedule_41863_LRotate(int x, int n);
void KeySchedule_41863();
void WEIGHTED_ROUND_ROBIN_Joiner_42052();
void WEIGHTED_ROUND_ROBIN_Splitter_42495();
void Xor_42497();
void Xor_42498();
void Xor_42499();
void Xor_42500();
void Xor_42501();
void Xor_42502();
void Xor_42503();
void Xor_42504();
void Xor_42505();
void Xor_42506();
void Xor_42507();
void Xor_42508();
void Xor_42509();
void Xor_42510();
void Xor_42511();
void Xor_42512();
void WEIGHTED_ROUND_ROBIN_Joiner_42496();
void WEIGHTED_ROUND_ROBIN_Splitter_42513();
void Sbox_42515();
void Sbox_42516();
void Sbox_42517();
void Sbox_42518();
void Sbox_42519();
void Sbox_42520();
void Sbox_42521();
void Sbox_42522();
void Sbox_42523();
void Sbox_42524();
void Sbox_42525();
void Sbox_42526();
void Sbox_42527();
void Sbox_42528();
void Sbox_42529();
void Sbox_42530();
void WEIGHTED_ROUND_ROBIN_Joiner_42514();
void rawL_41866();
void WEIGHTED_ROUND_ROBIN_Splitter_42053();
void Identity_41869();
int KeySchedule_41870_LRotate(int x, int n);
void KeySchedule_41870();
void WEIGHTED_ROUND_ROBIN_Joiner_42054();
void WEIGHTED_ROUND_ROBIN_Splitter_42531();
void Xor_42533();
void Xor_42534();
void Xor_42535();
void Xor_42536();
void Xor_42537();
void Xor_42538();
void Xor_42539();
void Xor_42540();
void Xor_42541();
void Xor_42542();
void Xor_42543();
void Xor_42544();
void Xor_42545();
void Xor_42546();
void Xor_42547();
void Xor_42548();
void WEIGHTED_ROUND_ROBIN_Joiner_42532();
void WEIGHTED_ROUND_ROBIN_Splitter_42549();
void Sbox_42551();
void Sbox_42552();
void Sbox_42553();
void Sbox_42554();
void Sbox_42555();
void Sbox_42556();
void Sbox_42557();
void Sbox_42558();
void Sbox_42559();
void Sbox_42560();
void Sbox_42561();
void Sbox_42562();
void Sbox_42563();
void Sbox_42564();
void Sbox_42565();
void Sbox_42566();
void WEIGHTED_ROUND_ROBIN_Joiner_42550();
void rawL_41873();
void WEIGHTED_ROUND_ROBIN_Splitter_42055();
void Identity_41876();
int KeySchedule_41877_LRotate(int x, int n);
void KeySchedule_41877();
void WEIGHTED_ROUND_ROBIN_Joiner_42056();
void WEIGHTED_ROUND_ROBIN_Splitter_42567();
void Xor_42569();
void Xor_42570();
void Xor_42571();
void Xor_42572();
void Xor_42573();
void Xor_42574();
void Xor_42575();
void Xor_42576();
void Xor_42577();
void Xor_42578();
void Xor_42579();
void Xor_42580();
void Xor_42581();
void Xor_42582();
void Xor_42583();
void Xor_42584();
void WEIGHTED_ROUND_ROBIN_Joiner_42568();
void WEIGHTED_ROUND_ROBIN_Splitter_42585();
void Sbox_42587();
void Sbox_42588();
void Sbox_42589();
void Sbox_42590();
void Sbox_42591();
void Sbox_42592();
void Sbox_42593();
void Sbox_42594();
void Sbox_42595();
void Sbox_42596();
void Sbox_42597();
void Sbox_42598();
void Sbox_42599();
void Sbox_42600();
void Sbox_42601();
void Sbox_42602();
void WEIGHTED_ROUND_ROBIN_Joiner_42586();
void rawL_41880();
void WEIGHTED_ROUND_ROBIN_Splitter_42057();
void Identity_41883();
int KeySchedule_41884_LRotate(int x, int n);
void KeySchedule_41884();
void WEIGHTED_ROUND_ROBIN_Joiner_42058();
void WEIGHTED_ROUND_ROBIN_Splitter_42603();
void Xor_42605();
void Xor_42606();
void Xor_42607();
void Xor_42608();
void Xor_42609();
void Xor_42610();
void Xor_42611();
void Xor_42612();
void Xor_42613();
void Xor_42614();
void Xor_42615();
void Xor_42616();
void Xor_42617();
void Xor_42618();
void Xor_42619();
void Xor_42620();
void WEIGHTED_ROUND_ROBIN_Joiner_42604();
void WEIGHTED_ROUND_ROBIN_Splitter_42621();
void Sbox_42623();
void Sbox_42624();
void Sbox_42625();
void Sbox_42626();
void Sbox_42627();
void Sbox_42628();
void Sbox_42629();
void Sbox_42630();
void Sbox_42631();
void Sbox_42632();
void Sbox_42633();
void Sbox_42634();
void Sbox_42635();
void Sbox_42636();
void Sbox_42637();
void Sbox_42638();
void WEIGHTED_ROUND_ROBIN_Joiner_42622();
void rawL_41887();
void WEIGHTED_ROUND_ROBIN_Splitter_42059();
void Identity_41890();
int KeySchedule_41891_LRotate(int x, int n);
void KeySchedule_41891();
void WEIGHTED_ROUND_ROBIN_Joiner_42060();
void WEIGHTED_ROUND_ROBIN_Splitter_42639();
void Xor_42641();
void Xor_42642();
void Xor_42643();
void Xor_42644();
void Xor_42645();
void Xor_42646();
void Xor_42647();
void Xor_42648();
void Xor_42649();
void Xor_42650();
void Xor_42651();
void Xor_42652();
void Xor_42653();
void Xor_42654();
void Xor_42655();
void Xor_42656();
void WEIGHTED_ROUND_ROBIN_Joiner_42640();
void WEIGHTED_ROUND_ROBIN_Splitter_42657();
void Sbox_42659();
void Sbox_42660();
void Sbox_42661();
void Sbox_42662();
void Sbox_42663();
void Sbox_42664();
void Sbox_42665();
void Sbox_42666();
void Sbox_42667();
void Sbox_42668();
void Sbox_42669();
void Sbox_42670();
void Sbox_42671();
void Sbox_42672();
void Sbox_42673();
void Sbox_42674();
void WEIGHTED_ROUND_ROBIN_Joiner_42658();
void rawL_41894();
void WEIGHTED_ROUND_ROBIN_Splitter_42061();
void Identity_41897();
int KeySchedule_41898_LRotate(int x, int n);
void KeySchedule_41898();
void WEIGHTED_ROUND_ROBIN_Joiner_42062();
void WEIGHTED_ROUND_ROBIN_Splitter_42675();
void Xor_42677();
void Xor_42678();
void Xor_42679();
void Xor_42680();
void Xor_42681();
void Xor_42682();
void Xor_42683();
void Xor_42684();
void Xor_42685();
void Xor_42686();
void Xor_42687();
void Xor_42688();
void Xor_42689();
void Xor_42690();
void Xor_42691();
void Xor_42692();
void WEIGHTED_ROUND_ROBIN_Joiner_42676();
void WEIGHTED_ROUND_ROBIN_Splitter_42693();
void Sbox_42695();
void Sbox_42696();
void Sbox_42697();
void Sbox_42698();
void Sbox_42699();
void Sbox_42700();
void Sbox_42701();
void Sbox_42702();
void Sbox_42703();
void Sbox_42704();
void Sbox_42705();
void Sbox_42706();
void Sbox_42707();
void Sbox_42708();
void Sbox_42709();
void Sbox_42710();
void WEIGHTED_ROUND_ROBIN_Joiner_42694();
void rawL_41901();
void WEIGHTED_ROUND_ROBIN_Splitter_42063();
void Identity_41904();
int KeySchedule_41905_LRotate(int x, int n);
void KeySchedule_41905();
void WEIGHTED_ROUND_ROBIN_Joiner_42064();
void WEIGHTED_ROUND_ROBIN_Splitter_42711();
void Xor_42713();
void Xor_42714();
void Xor_42715();
void Xor_42716();
void Xor_42717();
void Xor_42718();
void Xor_42719();
void Xor_42720();
void Xor_42721();
void Xor_42722();
void Xor_42723();
void Xor_42724();
void Xor_42725();
void Xor_42726();
void Xor_42727();
void Xor_42728();
void WEIGHTED_ROUND_ROBIN_Joiner_42712();
void WEIGHTED_ROUND_ROBIN_Splitter_42729();
void Sbox_42731();
void Sbox_42732();
void Sbox_42733();
void Sbox_42734();
void Sbox_42735();
void Sbox_42736();
void Sbox_42737();
void Sbox_42738();
void Sbox_42739();
void Sbox_42740();
void Sbox_42741();
void Sbox_42742();
void Sbox_42743();
void Sbox_42744();
void Sbox_42745();
void Sbox_42746();
void WEIGHTED_ROUND_ROBIN_Joiner_42730();
void rawL_41908();
void WEIGHTED_ROUND_ROBIN_Splitter_42065();
void Identity_41911();
int KeySchedule_41912_LRotate(int x, int n);
void KeySchedule_41912();
void WEIGHTED_ROUND_ROBIN_Joiner_42066();
void WEIGHTED_ROUND_ROBIN_Splitter_42747();
void Xor_42749();
void Xor_42750();
void Xor_42751();
void Xor_42752();
void Xor_42753();
void Xor_42754();
void Xor_42755();
void Xor_42756();
void Xor_42757();
void Xor_42758();
void Xor_42759();
void Xor_42760();
void Xor_42761();
void Xor_42762();
void Xor_42763();
void Xor_42764();
void WEIGHTED_ROUND_ROBIN_Joiner_42748();
void WEIGHTED_ROUND_ROBIN_Splitter_42765();
void Sbox_42767();
void Sbox_42768();
void Sbox_42769();
void Sbox_42770();
void Sbox_42771();
void Sbox_42772();
void Sbox_42773();
void Sbox_42774();
void Sbox_42775();
void Sbox_42776();
void Sbox_42777();
void Sbox_42778();
void Sbox_42779();
void Sbox_42780();
void Sbox_42781();
void Sbox_42782();
void WEIGHTED_ROUND_ROBIN_Joiner_42766();
void rawL_41915();
void WEIGHTED_ROUND_ROBIN_Splitter_42067();
void Identity_41918();
int KeySchedule_41919_LRotate(int x, int n);
void KeySchedule_41919();
void WEIGHTED_ROUND_ROBIN_Joiner_42068();
void WEIGHTED_ROUND_ROBIN_Splitter_42783();
void Xor_42785();
void Xor_42786();
void Xor_42787();
void Xor_42788();
void Xor_42789();
void Xor_42790();
void Xor_42791();
void Xor_42792();
void Xor_42793();
void Xor_42794();
void Xor_42795();
void Xor_42796();
void Xor_42797();
void Xor_42798();
void Xor_42799();
void Xor_42800();
void WEIGHTED_ROUND_ROBIN_Joiner_42784();
void WEIGHTED_ROUND_ROBIN_Splitter_42801();
void Sbox_42803();
void Sbox_42804();
void Sbox_42805();
void Sbox_42806();
void Sbox_42807();
void Sbox_42808();
void Sbox_42809();
void Sbox_42810();
void Sbox_42811();
void Sbox_42812();
void Sbox_42813();
void Sbox_42814();
void Sbox_42815();
void Sbox_42816();
void Sbox_42817();
void Sbox_42818();
void WEIGHTED_ROUND_ROBIN_Joiner_42802();
void rawL_41922();
void WEIGHTED_ROUND_ROBIN_Splitter_42069();
void Identity_41925();
int KeySchedule_41926_LRotate(int x, int n);
void KeySchedule_41926();
void WEIGHTED_ROUND_ROBIN_Joiner_42070();
void WEIGHTED_ROUND_ROBIN_Splitter_42819();
void Xor_42821();
void Xor_42822();
void Xor_42823();
void Xor_42824();
void Xor_42825();
void Xor_42826();
void Xor_42827();
void Xor_42828();
void Xor_42829();
void Xor_42830();
void Xor_42831();
void Xor_42832();
void Xor_42833();
void Xor_42834();
void Xor_42835();
void Xor_42836();
void WEIGHTED_ROUND_ROBIN_Joiner_42820();
void WEIGHTED_ROUND_ROBIN_Splitter_42837();
void Sbox_42839();
void Sbox_42840();
void Sbox_42841();
void Sbox_42842();
void Sbox_42843();
void Sbox_42844();
void Sbox_42845();
void Sbox_42846();
void Sbox_42847();
void Sbox_42848();
void Sbox_42849();
void Sbox_42850();
void Sbox_42851();
void Sbox_42852();
void Sbox_42853();
void Sbox_42854();
void WEIGHTED_ROUND_ROBIN_Joiner_42838();
void rawL_41929();
void WEIGHTED_ROUND_ROBIN_Splitter_42071();
void Identity_41932();
int KeySchedule_41933_LRotate(int x, int n);
void KeySchedule_41933();
void WEIGHTED_ROUND_ROBIN_Joiner_42072();
void WEIGHTED_ROUND_ROBIN_Splitter_42855();
void Xor_42857();
void Xor_42858();
void Xor_42859();
void Xor_42860();
void Xor_42861();
void Xor_42862();
void Xor_42863();
void Xor_42864();
void Xor_42865();
void Xor_42866();
void Xor_42867();
void Xor_42868();
void Xor_42869();
void Xor_42870();
void Xor_42871();
void Xor_42872();
void WEIGHTED_ROUND_ROBIN_Joiner_42856();
void WEIGHTED_ROUND_ROBIN_Splitter_42873();
void Sbox_42875();
void Sbox_42876();
void Sbox_42877();
void Sbox_42878();
void Sbox_42879();
void Sbox_42880();
void Sbox_42881();
void Sbox_42882();
void Sbox_42883();
void Sbox_42884();
void Sbox_42885();
void Sbox_42886();
void Sbox_42887();
void Sbox_42888();
void Sbox_42889();
void Sbox_42890();
void WEIGHTED_ROUND_ROBIN_Joiner_42874();
void rawL_41936();
void WEIGHTED_ROUND_ROBIN_Splitter_42073();
void Identity_41939();
int KeySchedule_41940_LRotate(int x, int n);
void KeySchedule_41940();
void WEIGHTED_ROUND_ROBIN_Joiner_42074();
void WEIGHTED_ROUND_ROBIN_Splitter_42891();
void Xor_42893();
void Xor_42894();
void Xor_42895();
void Xor_42896();
void Xor_42897();
void Xor_42898();
void Xor_42899();
void Xor_42900();
void Xor_42901();
void Xor_42902();
void Xor_42903();
void Xor_42904();
void Xor_42905();
void Xor_42906();
void Xor_42907();
void Xor_42908();
void WEIGHTED_ROUND_ROBIN_Joiner_42892();
void WEIGHTED_ROUND_ROBIN_Splitter_42909();
void Sbox_42911();
void Sbox_42912();
void Sbox_42913();
void Sbox_42914();
void Sbox_42915();
void Sbox_42916();
void Sbox_42917();
void Sbox_42918();
void Sbox_42919();
void Sbox_42920();
void Sbox_42921();
void Sbox_42922();
void Sbox_42923();
void Sbox_42924();
void Sbox_42925();
void Sbox_42926();
void WEIGHTED_ROUND_ROBIN_Joiner_42910();
void rawL_41943();
void WEIGHTED_ROUND_ROBIN_Splitter_42075();
void Identity_41946();
int KeySchedule_41947_LRotate(int x, int n);
void KeySchedule_41947();
void WEIGHTED_ROUND_ROBIN_Joiner_42076();
void WEIGHTED_ROUND_ROBIN_Splitter_42927();
void Xor_42929();
void Xor_42930();
void Xor_42931();
void Xor_42932();
void Xor_42933();
void Xor_42934();
void Xor_42935();
void Xor_42936();
void Xor_42937();
void Xor_42938();
void Xor_42939();
void Xor_42940();
void Xor_42941();
void Xor_42942();
void Xor_42943();
void Xor_42944();
void WEIGHTED_ROUND_ROBIN_Joiner_42928();
void WEIGHTED_ROUND_ROBIN_Splitter_42945();
void Sbox_42947();
void Sbox_42948();
void Sbox_42949();
void Sbox_42950();
void Sbox_42951();
void Sbox_42952();
void Sbox_42953();
void Sbox_42954();
void Sbox_42955();
void Sbox_42956();
void Sbox_42957();
void Sbox_42958();
void Sbox_42959();
void Sbox_42960();
void Sbox_42961();
void Sbox_42962();
void WEIGHTED_ROUND_ROBIN_Joiner_42946();
void rawL_41950();
void WEIGHTED_ROUND_ROBIN_Splitter_42077();
void Identity_41953();
int KeySchedule_41954_LRotate(int x, int n);
void KeySchedule_41954();
void WEIGHTED_ROUND_ROBIN_Joiner_42078();
void WEIGHTED_ROUND_ROBIN_Splitter_42963();
void Xor_42965();
void Xor_42966();
void Xor_42967();
void Xor_42968();
void Xor_42969();
void Xor_42970();
void Xor_42971();
void Xor_42972();
void Xor_42973();
void Xor_42974();
void Xor_42975();
void Xor_42976();
void Xor_42977();
void Xor_42978();
void Xor_42979();
void Xor_42980();
void WEIGHTED_ROUND_ROBIN_Joiner_42964();
void WEIGHTED_ROUND_ROBIN_Splitter_42981();
void Sbox_42983();
void Sbox_42984();
void Sbox_42985();
void Sbox_42986();
void Sbox_42987();
void Sbox_42988();
void Sbox_42989();
void Sbox_42990();
void Sbox_42991();
void Sbox_42992();
void Sbox_42993();
void Sbox_42994();
void Sbox_42995();
void Sbox_42996();
void Sbox_42997();
void Sbox_42998();
void WEIGHTED_ROUND_ROBIN_Joiner_42982();
void rawL_41957();
void WEIGHTED_ROUND_ROBIN_Splitter_42079();
void Identity_41960();
int KeySchedule_41961_LRotate(int x, int n);
void KeySchedule_41961();
void WEIGHTED_ROUND_ROBIN_Joiner_42080();
void WEIGHTED_ROUND_ROBIN_Splitter_42999();
void Xor_43001();
void Xor_43002();
void Xor_43003();
void Xor_43004();
void Xor_43005();
void Xor_43006();
void Xor_43007();
void Xor_43008();
void Xor_43009();
void Xor_43010();
void Xor_43011();
void Xor_43012();
void Xor_43013();
void Xor_43014();
void Xor_43015();
void Xor_43016();
void WEIGHTED_ROUND_ROBIN_Joiner_43000();
void WEIGHTED_ROUND_ROBIN_Splitter_43017();
void Sbox_43019();
void Sbox_43020();
void Sbox_43021();
void Sbox_43022();
void Sbox_43023();
void Sbox_43024();
void Sbox_43025();
void Sbox_43026();
void Sbox_43027();
void Sbox_43028();
void Sbox_43029();
void Sbox_43030();
void Sbox_43031();
void Sbox_43032();
void Sbox_43033();
void Sbox_43034();
void WEIGHTED_ROUND_ROBIN_Joiner_43018();
void rawL_41964();
void WEIGHTED_ROUND_ROBIN_Splitter_42081();
void Identity_41967();
int KeySchedule_41968_LRotate(int x, int n);
void KeySchedule_41968();
void WEIGHTED_ROUND_ROBIN_Joiner_42082();
void WEIGHTED_ROUND_ROBIN_Splitter_43035();
void Xor_43037();
void Xor_43038();
void Xor_43039();
void Xor_43040();
void Xor_43041();
void Xor_43042();
void Xor_43043();
void Xor_43044();
void Xor_43045();
void Xor_43046();
void Xor_43047();
void Xor_43048();
void Xor_43049();
void Xor_43050();
void Xor_43051();
void Xor_43052();
void WEIGHTED_ROUND_ROBIN_Joiner_43036();
void WEIGHTED_ROUND_ROBIN_Splitter_43053();
void Sbox_43055();
void Sbox_43056();
void Sbox_43057();
void Sbox_43058();
void Sbox_43059();
void Sbox_43060();
void Sbox_43061();
void Sbox_43062();
void Sbox_43063();
void Sbox_43064();
void Sbox_43065();
void Sbox_43066();
void Sbox_43067();
void Sbox_43068();
void Sbox_43069();
void Sbox_43070();
void WEIGHTED_ROUND_ROBIN_Joiner_43054();
void rawL_41971();
void WEIGHTED_ROUND_ROBIN_Splitter_42083();
void Identity_41974();
int KeySchedule_41975_LRotate(int x, int n);
void KeySchedule_41975();
void WEIGHTED_ROUND_ROBIN_Joiner_42084();
void WEIGHTED_ROUND_ROBIN_Splitter_43071();
void Xor_43073();
void Xor_43074();
void Xor_43075();
void Xor_43076();
void Xor_43077();
void Xor_43078();
void Xor_43079();
void Xor_43080();
void Xor_43081();
void Xor_43082();
void Xor_43083();
void Xor_43084();
void Xor_43085();
void Xor_43086();
void Xor_43087();
void Xor_43088();
void WEIGHTED_ROUND_ROBIN_Joiner_43072();
void WEIGHTED_ROUND_ROBIN_Splitter_43089();
void Sbox_43091();
void Sbox_43092();
void Sbox_43093();
void Sbox_43094();
void Sbox_43095();
void Sbox_43096();
void Sbox_43097();
void Sbox_43098();
void Sbox_43099();
void Sbox_43100();
void Sbox_43101();
void Sbox_43102();
void Sbox_43103();
void Sbox_43104();
void Sbox_43105();
void Sbox_43106();
void WEIGHTED_ROUND_ROBIN_Joiner_43090();
void rawL_41978();
void WEIGHTED_ROUND_ROBIN_Splitter_42085();
void Identity_41981();
int KeySchedule_41982_LRotate(int x, int n);
void KeySchedule_41982();
void WEIGHTED_ROUND_ROBIN_Joiner_42086();
void WEIGHTED_ROUND_ROBIN_Splitter_43107();
void Xor_43109();
void Xor_43110();
void Xor_43111();
void Xor_43112();
void Xor_43113();
void Xor_43114();
void Xor_43115();
void Xor_43116();
void Xor_43117();
void Xor_43118();
void Xor_43119();
void Xor_43120();
void Xor_43121();
void Xor_43122();
void Xor_43123();
void Xor_43124();
void WEIGHTED_ROUND_ROBIN_Joiner_43108();
void WEIGHTED_ROUND_ROBIN_Splitter_43125();
void Sbox_43127();
void Sbox_43128();
void Sbox_43129();
void Sbox_43130();
void Sbox_43131();
void Sbox_43132();
void Sbox_43133();
void Sbox_43134();
void Sbox_43135();
void Sbox_43136();
void Sbox_43137();
void Sbox_43138();
void Sbox_43139();
void Sbox_43140();
void Sbox_43141();
void Sbox_43142();
void WEIGHTED_ROUND_ROBIN_Joiner_43126();
void rawL_41985();
void WEIGHTED_ROUND_ROBIN_Splitter_42087();
void Identity_41988();
int KeySchedule_41989_LRotate(int x, int n);
void KeySchedule_41989();
void WEIGHTED_ROUND_ROBIN_Joiner_42088();
void WEIGHTED_ROUND_ROBIN_Splitter_43143();
void Xor_43145();
void Xor_43146();
void Xor_43147();
void Xor_43148();
void Xor_43149();
void Xor_43150();
void Xor_43151();
void Xor_43152();
void Xor_43153();
void Xor_43154();
void Xor_43155();
void Xor_43156();
void Xor_43157();
void Xor_43158();
void Xor_43159();
void Xor_43160();
void WEIGHTED_ROUND_ROBIN_Joiner_43144();
void WEIGHTED_ROUND_ROBIN_Splitter_43161();
void Sbox_43163();
void Sbox_43164();
void Sbox_43165();
void Sbox_43166();
void Sbox_43167();
void Sbox_43168();
void Sbox_43169();
void Sbox_43170();
void Sbox_43171();
void Sbox_43172();
void Sbox_43173();
void Sbox_43174();
void Sbox_43175();
void Sbox_43176();
void Sbox_43177();
void Sbox_43178();
void WEIGHTED_ROUND_ROBIN_Joiner_43162();
void rawL_41992();
void WEIGHTED_ROUND_ROBIN_Splitter_42089();
void Identity_41995();
int KeySchedule_41996_LRotate(int x, int n);
void KeySchedule_41996();
void WEIGHTED_ROUND_ROBIN_Joiner_42090();
void WEIGHTED_ROUND_ROBIN_Splitter_43179();
void Xor_43181();
void Xor_43182();
void Xor_43183();
void Xor_43184();
void Xor_43185();
void Xor_43186();
void Xor_43187();
void Xor_43188();
void Xor_43189();
void Xor_43190();
void Xor_43191();
void Xor_43192();
void Xor_43193();
void Xor_43194();
void Xor_43195();
void Xor_43196();
void WEIGHTED_ROUND_ROBIN_Joiner_43180();
void WEIGHTED_ROUND_ROBIN_Splitter_43197();
void Sbox_43199();
void Sbox_43200();
void Sbox_43201();
void Sbox_43202();
void Sbox_43203();
void Sbox_43204();
void Sbox_43205();
void Sbox_43206();
void Sbox_43207();
void Sbox_43208();
void Sbox_43209();
void Sbox_43210();
void Sbox_43211();
void Sbox_43212();
void Sbox_43213();
void Sbox_43214();
void WEIGHTED_ROUND_ROBIN_Joiner_43198();
void rawL_41999();
void WEIGHTED_ROUND_ROBIN_Splitter_42091();
void Identity_42002();
int KeySchedule_42003_LRotate(int x, int n);
void KeySchedule_42003();
void WEIGHTED_ROUND_ROBIN_Joiner_42092();
void WEIGHTED_ROUND_ROBIN_Splitter_43215();
void Xor_43217();
void Xor_43218();
void Xor_43219();
void Xor_43220();
void Xor_43221();
void Xor_43222();
void Xor_43223();
void Xor_43224();
void Xor_43225();
void Xor_43226();
void Xor_43227();
void Xor_43228();
void Xor_43229();
void Xor_43230();
void Xor_43231();
void Xor_43232();
void WEIGHTED_ROUND_ROBIN_Joiner_43216();
void WEIGHTED_ROUND_ROBIN_Splitter_43233();
void Sbox_43235();
void Sbox_43236();
void Sbox_43237();
void Sbox_43238();
void Sbox_43239();
void Sbox_43240();
void Sbox_43241();
void Sbox_43242();
void Sbox_43243();
void Sbox_43244();
void Sbox_43245();
void Sbox_43246();
void Sbox_43247();
void Sbox_43248();
void Sbox_43249();
void Sbox_43250();
void WEIGHTED_ROUND_ROBIN_Joiner_43234();
void rawL_42006();
void WEIGHTED_ROUND_ROBIN_Splitter_42093();
void Identity_42009();
int KeySchedule_42010_LRotate(int x, int n);
void KeySchedule_42010();
void WEIGHTED_ROUND_ROBIN_Joiner_42094();
void WEIGHTED_ROUND_ROBIN_Splitter_43251();
void Xor_43253();
void Xor_43254();
void Xor_43255();
void Xor_43256();
void Xor_43257();
void Xor_43258();
void Xor_43259();
void Xor_43260();
void Xor_43261();
void Xor_43262();
void Xor_43263();
void Xor_43264();
void Xor_43265();
void Xor_43266();
void Xor_43267();
void Xor_43268();
void WEIGHTED_ROUND_ROBIN_Joiner_43252();
void WEIGHTED_ROUND_ROBIN_Splitter_43269();
void Sbox_43271();
void Sbox_43272();
void Sbox_43273();
void Sbox_43274();
void Sbox_43275();
void Sbox_43276();
void Sbox_43277();
void Sbox_43278();
void Sbox_43279();
void Sbox_43280();
void Sbox_43281();
void Sbox_43282();
void Sbox_43283();
void Sbox_43284();
void Sbox_43285();
void Sbox_43286();
void WEIGHTED_ROUND_ROBIN_Joiner_43270();
void rawL_42013();
void WEIGHTED_ROUND_ROBIN_Splitter_42095();
void Identity_42016();
int KeySchedule_42017_LRotate(int x, int n);
void KeySchedule_42017();
void WEIGHTED_ROUND_ROBIN_Joiner_42096();
void WEIGHTED_ROUND_ROBIN_Splitter_43287();
void Xor_43289();
void Xor_43290();
void Xor_43291();
void Xor_43292();
void Xor_43293();
void Xor_43294();
void Xor_43295();
void Xor_43296();
void Xor_43297();
void Xor_43298();
void Xor_43299();
void Xor_43300();
void Xor_43301();
void Xor_43302();
void Xor_43303();
void Xor_43304();
void WEIGHTED_ROUND_ROBIN_Joiner_43288();
void WEIGHTED_ROUND_ROBIN_Splitter_43305();
void Sbox_43307();
void Sbox_43308();
void Sbox_43309();
void Sbox_43310();
void Sbox_43311();
void Sbox_43312();
void Sbox_43313();
void Sbox_43314();
void Sbox_43315();
void Sbox_43316();
void Sbox_43317();
void Sbox_43318();
void Sbox_43319();
void Sbox_43320();
void Sbox_43321();
void Sbox_43322();
void WEIGHTED_ROUND_ROBIN_Joiner_43306();
void rawL_42020();
void WEIGHTED_ROUND_ROBIN_Splitter_42097();
void Identity_42023();
int KeySchedule_42024_LRotate(int x, int n);
void KeySchedule_42024();
void WEIGHTED_ROUND_ROBIN_Joiner_42098();
void WEIGHTED_ROUND_ROBIN_Splitter_43323();
void Xor_43325();
void Xor_43326();
void Xor_43327();
void Xor_43328();
void Xor_43329();
void Xor_43330();
void Xor_43331();
void Xor_43332();
void Xor_43333();
void Xor_43334();
void Xor_43335();
void Xor_43336();
void Xor_43337();
void Xor_43338();
void Xor_43339();
void Xor_43340();
void WEIGHTED_ROUND_ROBIN_Joiner_43324();
void WEIGHTED_ROUND_ROBIN_Splitter_43341();
void Sbox_43343();
void Sbox_43344();
void Sbox_43345();
void Sbox_43346();
void Sbox_43347();
void Sbox_43348();
void Sbox_43349();
void Sbox_43350();
void Sbox_43351();
void Sbox_43352();
void Sbox_43353();
void Sbox_43354();
void Sbox_43355();
void Sbox_43356();
void Sbox_43357();
void Sbox_43358();
void WEIGHTED_ROUND_ROBIN_Joiner_43342();
void WEIGHTED_ROUND_ROBIN_Splitter_42099();
void Identity_42028();
int KeySchedule_42029_LRotate(int x, int n);
void KeySchedule_42029();
void WEIGHTED_ROUND_ROBIN_Joiner_42100();
void WEIGHTED_ROUND_ROBIN_Splitter_43359();
void Xor_43361();
void Xor_43362();
void Xor_43363();
void Xor_43364();
void Xor_43365();
void Xor_43366();
void Xor_43367();
void Xor_43368();
void Xor_43369();
void Xor_43370();
void Xor_43371();
void Xor_43372();
void Xor_43373();
void Xor_43374();
void Xor_43375();
void Xor_43376();
void WEIGHTED_ROUND_ROBIN_Joiner_43360();
void Permute_42031();
void WEIGHTED_ROUND_ROBIN_Splitter_43377();
void BitstoInts_43379();
void BitstoInts_43380();
void BitstoInts_43381();
void BitstoInts_43382();
void BitstoInts_43383();
void BitstoInts_43384();
void BitstoInts_43385();
void BitstoInts_43386();
void BitstoInts_43387();
void BitstoInts_43388();
void BitstoInts_43389();
void BitstoInts_43390();
void BitstoInts_43391();
void BitstoInts_43392();
void BitstoInts_43393();
void BitstoInts_43394();
void WEIGHTED_ROUND_ROBIN_Joiner_43378();
void HexPrinterAux_42034();

#ifdef __cplusplus
}
#endif
#endif
