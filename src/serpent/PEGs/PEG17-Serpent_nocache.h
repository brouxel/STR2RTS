#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4352 on the compile command line
#else
#if BUF_SIZEMAX < 4352
#error BUF_SIZEMAX too small, it must be at least 4352
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	int USERKEY;
	int USERKEYS[5][8];
	int IP[128];
	int SBOXES[8][16];
} TheGlobal_t;

typedef struct {
	int TEXT[5][4];
} PlainTextSourceAux_38730_t;

typedef struct {
	int permutation[128];
} Permute_38732_t;

typedef struct {
	int keys[33][128];
} KeySchedule_38736_t;
void PlainTextSourceAux_38730();
void WEIGHTED_ROUND_ROBIN_Splitter_39130();
void IntoBits_39132();
void IntoBits_39133();
void IntoBits_39134();
void IntoBits_39135();
void WEIGHTED_ROUND_ROBIN_Joiner_39131();
void Permute_38732();
void WEIGHTED_ROUND_ROBIN_Splitter_38964();
void Identity_38735();
int KeySchedule_38736_LRotate(int x, int n);
void KeySchedule_38736();
void WEIGHTED_ROUND_ROBIN_Joiner_38965();
void WEIGHTED_ROUND_ROBIN_Splitter_39136();
void Xor_39138();
void Xor_39139();
void Xor_39140();
void Xor_39141();
void Xor_39142();
void Xor_39143();
void Xor_39144();
void Xor_39145();
void Xor_39146();
void Xor_39147();
void Xor_39148();
void Xor_39149();
void Xor_39150();
void Xor_39151();
void Xor_39152();
void Xor_39153();
void Xor_39154();
void WEIGHTED_ROUND_ROBIN_Joiner_39137();
void WEIGHTED_ROUND_ROBIN_Splitter_39155();
void Sbox_39157();
void Sbox_39158();
void Sbox_39159();
void Sbox_39160();
void Sbox_39161();
void Sbox_39162();
void Sbox_39163();
void Sbox_39164();
void Sbox_39165();
void Sbox_39166();
void Sbox_39167();
void Sbox_39168();
void Sbox_39169();
void Sbox_39170();
void Sbox_39171();
void Sbox_39172();
void Sbox_39173();
void WEIGHTED_ROUND_ROBIN_Joiner_39156();
void rawL_38739();
void WEIGHTED_ROUND_ROBIN_Splitter_38966();
void Identity_38742();
int KeySchedule_38743_LRotate(int x, int n);
void KeySchedule_38743();
void WEIGHTED_ROUND_ROBIN_Joiner_38967();
void WEIGHTED_ROUND_ROBIN_Splitter_39174();
void Xor_39176();
void Xor_39177();
void Xor_39178();
void Xor_39179();
void Xor_39180();
void Xor_39181();
void Xor_39182();
void Xor_39183();
void Xor_39184();
void Xor_39185();
void Xor_39186();
void Xor_39187();
void Xor_39188();
void Xor_39189();
void Xor_39190();
void Xor_39191();
void Xor_39192();
void WEIGHTED_ROUND_ROBIN_Joiner_39175();
void WEIGHTED_ROUND_ROBIN_Splitter_39193();
void Sbox_39195();
void Sbox_39196();
void Sbox_39197();
void Sbox_39198();
void Sbox_39199();
void Sbox_39200();
void Sbox_39201();
void Sbox_39202();
void Sbox_39203();
void Sbox_39204();
void Sbox_39205();
void Sbox_39206();
void Sbox_39207();
void Sbox_39208();
void Sbox_39209();
void Sbox_39210();
void Sbox_39211();
void WEIGHTED_ROUND_ROBIN_Joiner_39194();
void rawL_38746();
void WEIGHTED_ROUND_ROBIN_Splitter_38968();
void Identity_38749();
int KeySchedule_38750_LRotate(int x, int n);
void KeySchedule_38750();
void WEIGHTED_ROUND_ROBIN_Joiner_38969();
void WEIGHTED_ROUND_ROBIN_Splitter_39212();
void Xor_39214();
void Xor_39215();
void Xor_39216();
void Xor_39217();
void Xor_39218();
void Xor_39219();
void Xor_39220();
void Xor_39221();
void Xor_39222();
void Xor_39223();
void Xor_39224();
void Xor_39225();
void Xor_39226();
void Xor_39227();
void Xor_39228();
void Xor_39229();
void Xor_39230();
void WEIGHTED_ROUND_ROBIN_Joiner_39213();
void WEIGHTED_ROUND_ROBIN_Splitter_39231();
void Sbox_39233();
void Sbox_39234();
void Sbox_39235();
void Sbox_39236();
void Sbox_39237();
void Sbox_39238();
void Sbox_39239();
void Sbox_39240();
void Sbox_39241();
void Sbox_39242();
void Sbox_39243();
void Sbox_39244();
void Sbox_39245();
void Sbox_39246();
void Sbox_39247();
void Sbox_39248();
void Sbox_39249();
void WEIGHTED_ROUND_ROBIN_Joiner_39232();
void rawL_38753();
void WEIGHTED_ROUND_ROBIN_Splitter_38970();
void Identity_38756();
int KeySchedule_38757_LRotate(int x, int n);
void KeySchedule_38757();
void WEIGHTED_ROUND_ROBIN_Joiner_38971();
void WEIGHTED_ROUND_ROBIN_Splitter_39250();
void Xor_39252();
void Xor_39253();
void Xor_39254();
void Xor_39255();
void Xor_39256();
void Xor_39257();
void Xor_39258();
void Xor_39259();
void Xor_39260();
void Xor_39261();
void Xor_39262();
void Xor_39263();
void Xor_39264();
void Xor_39265();
void Xor_39266();
void Xor_39267();
void Xor_39268();
void WEIGHTED_ROUND_ROBIN_Joiner_39251();
void WEIGHTED_ROUND_ROBIN_Splitter_39269();
void Sbox_39271();
void Sbox_39272();
void Sbox_39273();
void Sbox_39274();
void Sbox_39275();
void Sbox_39276();
void Sbox_39277();
void Sbox_39278();
void Sbox_39279();
void Sbox_39280();
void Sbox_39281();
void Sbox_39282();
void Sbox_39283();
void Sbox_39284();
void Sbox_39285();
void Sbox_39286();
void Sbox_39287();
void WEIGHTED_ROUND_ROBIN_Joiner_39270();
void rawL_38760();
void WEIGHTED_ROUND_ROBIN_Splitter_38972();
void Identity_38763();
int KeySchedule_38764_LRotate(int x, int n);
void KeySchedule_38764();
void WEIGHTED_ROUND_ROBIN_Joiner_38973();
void WEIGHTED_ROUND_ROBIN_Splitter_39288();
void Xor_39290();
void Xor_39291();
void Xor_39292();
void Xor_39293();
void Xor_39294();
void Xor_39295();
void Xor_39296();
void Xor_39297();
void Xor_39298();
void Xor_39299();
void Xor_39300();
void Xor_39301();
void Xor_39302();
void Xor_39303();
void Xor_39304();
void Xor_39305();
void Xor_39306();
void WEIGHTED_ROUND_ROBIN_Joiner_39289();
void WEIGHTED_ROUND_ROBIN_Splitter_39307();
void Sbox_39309();
void Sbox_39310();
void Sbox_39311();
void Sbox_39312();
void Sbox_39313();
void Sbox_39314();
void Sbox_39315();
void Sbox_39316();
void Sbox_39317();
void Sbox_39318();
void Sbox_39319();
void Sbox_39320();
void Sbox_39321();
void Sbox_39322();
void Sbox_39323();
void Sbox_39324();
void Sbox_39325();
void WEIGHTED_ROUND_ROBIN_Joiner_39308();
void rawL_38767();
void WEIGHTED_ROUND_ROBIN_Splitter_38974();
void Identity_38770();
int KeySchedule_38771_LRotate(int x, int n);
void KeySchedule_38771();
void WEIGHTED_ROUND_ROBIN_Joiner_38975();
void WEIGHTED_ROUND_ROBIN_Splitter_39326();
void Xor_39328();
void Xor_39329();
void Xor_39330();
void Xor_39331();
void Xor_39332();
void Xor_39333();
void Xor_39334();
void Xor_39335();
void Xor_39336();
void Xor_39337();
void Xor_39338();
void Xor_39339();
void Xor_39340();
void Xor_39341();
void Xor_39342();
void Xor_39343();
void Xor_39344();
void WEIGHTED_ROUND_ROBIN_Joiner_39327();
void WEIGHTED_ROUND_ROBIN_Splitter_39345();
void Sbox_39347();
void Sbox_39348();
void Sbox_39349();
void Sbox_39350();
void Sbox_39351();
void Sbox_39352();
void Sbox_39353();
void Sbox_39354();
void Sbox_39355();
void Sbox_39356();
void Sbox_39357();
void Sbox_39358();
void Sbox_39359();
void Sbox_39360();
void Sbox_39361();
void Sbox_39362();
void Sbox_39363();
void WEIGHTED_ROUND_ROBIN_Joiner_39346();
void rawL_38774();
void WEIGHTED_ROUND_ROBIN_Splitter_38976();
void Identity_38777();
int KeySchedule_38778_LRotate(int x, int n);
void KeySchedule_38778();
void WEIGHTED_ROUND_ROBIN_Joiner_38977();
void WEIGHTED_ROUND_ROBIN_Splitter_39364();
void Xor_39366();
void Xor_39367();
void Xor_39368();
void Xor_39369();
void Xor_39370();
void Xor_39371();
void Xor_39372();
void Xor_39373();
void Xor_39374();
void Xor_39375();
void Xor_39376();
void Xor_39377();
void Xor_39378();
void Xor_39379();
void Xor_39380();
void Xor_39381();
void Xor_39382();
void WEIGHTED_ROUND_ROBIN_Joiner_39365();
void WEIGHTED_ROUND_ROBIN_Splitter_39383();
void Sbox_39385();
void Sbox_39386();
void Sbox_39387();
void Sbox_39388();
void Sbox_39389();
void Sbox_39390();
void Sbox_39391();
void Sbox_39392();
void Sbox_39393();
void Sbox_39394();
void Sbox_39395();
void Sbox_39396();
void Sbox_39397();
void Sbox_39398();
void Sbox_39399();
void Sbox_39400();
void Sbox_39401();
void WEIGHTED_ROUND_ROBIN_Joiner_39384();
void rawL_38781();
void WEIGHTED_ROUND_ROBIN_Splitter_38978();
void Identity_38784();
int KeySchedule_38785_LRotate(int x, int n);
void KeySchedule_38785();
void WEIGHTED_ROUND_ROBIN_Joiner_38979();
void WEIGHTED_ROUND_ROBIN_Splitter_39402();
void Xor_39404();
void Xor_39405();
void Xor_39406();
void Xor_39407();
void Xor_39408();
void Xor_39409();
void Xor_39410();
void Xor_39411();
void Xor_39412();
void Xor_39413();
void Xor_39414();
void Xor_39415();
void Xor_39416();
void Xor_39417();
void Xor_39418();
void Xor_39419();
void Xor_39420();
void WEIGHTED_ROUND_ROBIN_Joiner_39403();
void WEIGHTED_ROUND_ROBIN_Splitter_39421();
void Sbox_39423();
void Sbox_39424();
void Sbox_39425();
void Sbox_39426();
void Sbox_39427();
void Sbox_39428();
void Sbox_39429();
void Sbox_39430();
void Sbox_39431();
void Sbox_39432();
void Sbox_39433();
void Sbox_39434();
void Sbox_39435();
void Sbox_39436();
void Sbox_39437();
void Sbox_39438();
void Sbox_39439();
void WEIGHTED_ROUND_ROBIN_Joiner_39422();
void rawL_38788();
void WEIGHTED_ROUND_ROBIN_Splitter_38980();
void Identity_38791();
int KeySchedule_38792_LRotate(int x, int n);
void KeySchedule_38792();
void WEIGHTED_ROUND_ROBIN_Joiner_38981();
void WEIGHTED_ROUND_ROBIN_Splitter_39440();
void Xor_39442();
void Xor_39443();
void Xor_39444();
void Xor_39445();
void Xor_39446();
void Xor_39447();
void Xor_39448();
void Xor_39449();
void Xor_39450();
void Xor_39451();
void Xor_39452();
void Xor_39453();
void Xor_39454();
void Xor_39455();
void Xor_39456();
void Xor_39457();
void Xor_39458();
void WEIGHTED_ROUND_ROBIN_Joiner_39441();
void WEIGHTED_ROUND_ROBIN_Splitter_39459();
void Sbox_39461();
void Sbox_39462();
void Sbox_39463();
void Sbox_39464();
void Sbox_39465();
void Sbox_39466();
void Sbox_39467();
void Sbox_39468();
void Sbox_39469();
void Sbox_39470();
void Sbox_39471();
void Sbox_39472();
void Sbox_39473();
void Sbox_39474();
void Sbox_39475();
void Sbox_39476();
void Sbox_39477();
void WEIGHTED_ROUND_ROBIN_Joiner_39460();
void rawL_38795();
void WEIGHTED_ROUND_ROBIN_Splitter_38982();
void Identity_38798();
int KeySchedule_38799_LRotate(int x, int n);
void KeySchedule_38799();
void WEIGHTED_ROUND_ROBIN_Joiner_38983();
void WEIGHTED_ROUND_ROBIN_Splitter_39478();
void Xor_39480();
void Xor_39481();
void Xor_39482();
void Xor_39483();
void Xor_39484();
void Xor_39485();
void Xor_39486();
void Xor_39487();
void Xor_39488();
void Xor_39489();
void Xor_39490();
void Xor_39491();
void Xor_39492();
void Xor_39493();
void Xor_39494();
void Xor_39495();
void Xor_39496();
void WEIGHTED_ROUND_ROBIN_Joiner_39479();
void WEIGHTED_ROUND_ROBIN_Splitter_39497();
void Sbox_39499();
void Sbox_39500();
void Sbox_39501();
void Sbox_39502();
void Sbox_39503();
void Sbox_39504();
void Sbox_39505();
void Sbox_39506();
void Sbox_39507();
void Sbox_39508();
void Sbox_39509();
void Sbox_39510();
void Sbox_39511();
void Sbox_39512();
void Sbox_39513();
void Sbox_39514();
void Sbox_39515();
void WEIGHTED_ROUND_ROBIN_Joiner_39498();
void rawL_38802();
void WEIGHTED_ROUND_ROBIN_Splitter_38984();
void Identity_38805();
int KeySchedule_38806_LRotate(int x, int n);
void KeySchedule_38806();
void WEIGHTED_ROUND_ROBIN_Joiner_38985();
void WEIGHTED_ROUND_ROBIN_Splitter_39516();
void Xor_39518();
void Xor_39519();
void Xor_39520();
void Xor_39521();
void Xor_39522();
void Xor_39523();
void Xor_39524();
void Xor_39525();
void Xor_39526();
void Xor_39527();
void Xor_39528();
void Xor_39529();
void Xor_39530();
void Xor_39531();
void Xor_39532();
void Xor_39533();
void Xor_39534();
void WEIGHTED_ROUND_ROBIN_Joiner_39517();
void WEIGHTED_ROUND_ROBIN_Splitter_39535();
void Sbox_39537();
void Sbox_39538();
void Sbox_39539();
void Sbox_39540();
void Sbox_39541();
void Sbox_39542();
void Sbox_39543();
void Sbox_39544();
void Sbox_39545();
void Sbox_39546();
void Sbox_39547();
void Sbox_39548();
void Sbox_39549();
void Sbox_39550();
void Sbox_39551();
void Sbox_39552();
void Sbox_39553();
void WEIGHTED_ROUND_ROBIN_Joiner_39536();
void rawL_38809();
void WEIGHTED_ROUND_ROBIN_Splitter_38986();
void Identity_38812();
int KeySchedule_38813_LRotate(int x, int n);
void KeySchedule_38813();
void WEIGHTED_ROUND_ROBIN_Joiner_38987();
void WEIGHTED_ROUND_ROBIN_Splitter_39554();
void Xor_39556();
void Xor_39557();
void Xor_39558();
void Xor_39559();
void Xor_39560();
void Xor_39561();
void Xor_39562();
void Xor_39563();
void Xor_39564();
void Xor_39565();
void Xor_39566();
void Xor_39567();
void Xor_39568();
void Xor_39569();
void Xor_39570();
void Xor_39571();
void Xor_39572();
void WEIGHTED_ROUND_ROBIN_Joiner_39555();
void WEIGHTED_ROUND_ROBIN_Splitter_39573();
void Sbox_39575();
void Sbox_39576();
void Sbox_39577();
void Sbox_39578();
void Sbox_39579();
void Sbox_39580();
void Sbox_39581();
void Sbox_39582();
void Sbox_39583();
void Sbox_39584();
void Sbox_39585();
void Sbox_39586();
void Sbox_39587();
void Sbox_39588();
void Sbox_39589();
void Sbox_39590();
void Sbox_39591();
void WEIGHTED_ROUND_ROBIN_Joiner_39574();
void rawL_38816();
void WEIGHTED_ROUND_ROBIN_Splitter_38988();
void Identity_38819();
int KeySchedule_38820_LRotate(int x, int n);
void KeySchedule_38820();
void WEIGHTED_ROUND_ROBIN_Joiner_38989();
void WEIGHTED_ROUND_ROBIN_Splitter_39592();
void Xor_39594();
void Xor_39595();
void Xor_39596();
void Xor_39597();
void Xor_39598();
void Xor_39599();
void Xor_39600();
void Xor_39601();
void Xor_39602();
void Xor_39603();
void Xor_39604();
void Xor_39605();
void Xor_39606();
void Xor_39607();
void Xor_39608();
void Xor_39609();
void Xor_39610();
void WEIGHTED_ROUND_ROBIN_Joiner_39593();
void WEIGHTED_ROUND_ROBIN_Splitter_39611();
void Sbox_39613();
void Sbox_39614();
void Sbox_39615();
void Sbox_39616();
void Sbox_39617();
void Sbox_39618();
void Sbox_39619();
void Sbox_39620();
void Sbox_39621();
void Sbox_39622();
void Sbox_39623();
void Sbox_39624();
void Sbox_39625();
void Sbox_39626();
void Sbox_39627();
void Sbox_39628();
void Sbox_39629();
void WEIGHTED_ROUND_ROBIN_Joiner_39612();
void rawL_38823();
void WEIGHTED_ROUND_ROBIN_Splitter_38990();
void Identity_38826();
int KeySchedule_38827_LRotate(int x, int n);
void KeySchedule_38827();
void WEIGHTED_ROUND_ROBIN_Joiner_38991();
void WEIGHTED_ROUND_ROBIN_Splitter_39630();
void Xor_39632();
void Xor_39633();
void Xor_39634();
void Xor_39635();
void Xor_39636();
void Xor_39637();
void Xor_39638();
void Xor_39639();
void Xor_39640();
void Xor_39641();
void Xor_39642();
void Xor_39643();
void Xor_39644();
void Xor_39645();
void Xor_39646();
void Xor_39647();
void Xor_39648();
void WEIGHTED_ROUND_ROBIN_Joiner_39631();
void WEIGHTED_ROUND_ROBIN_Splitter_39649();
void Sbox_39651();
void Sbox_39652();
void Sbox_39653();
void Sbox_39654();
void Sbox_39655();
void Sbox_39656();
void Sbox_39657();
void Sbox_39658();
void Sbox_39659();
void Sbox_39660();
void Sbox_39661();
void Sbox_39662();
void Sbox_39663();
void Sbox_39664();
void Sbox_39665();
void Sbox_39666();
void Sbox_39667();
void WEIGHTED_ROUND_ROBIN_Joiner_39650();
void rawL_38830();
void WEIGHTED_ROUND_ROBIN_Splitter_38992();
void Identity_38833();
int KeySchedule_38834_LRotate(int x, int n);
void KeySchedule_38834();
void WEIGHTED_ROUND_ROBIN_Joiner_38993();
void WEIGHTED_ROUND_ROBIN_Splitter_39668();
void Xor_39670();
void Xor_39671();
void Xor_39672();
void Xor_39673();
void Xor_39674();
void Xor_39675();
void Xor_39676();
void Xor_39677();
void Xor_39678();
void Xor_39679();
void Xor_39680();
void Xor_39681();
void Xor_39682();
void Xor_39683();
void Xor_39684();
void Xor_39685();
void Xor_39686();
void WEIGHTED_ROUND_ROBIN_Joiner_39669();
void WEIGHTED_ROUND_ROBIN_Splitter_39687();
void Sbox_39689();
void Sbox_39690();
void Sbox_39691();
void Sbox_39692();
void Sbox_39693();
void Sbox_39694();
void Sbox_39695();
void Sbox_39696();
void Sbox_39697();
void Sbox_39698();
void Sbox_39699();
void Sbox_39700();
void Sbox_39701();
void Sbox_39702();
void Sbox_39703();
void Sbox_39704();
void Sbox_39705();
void WEIGHTED_ROUND_ROBIN_Joiner_39688();
void rawL_38837();
void WEIGHTED_ROUND_ROBIN_Splitter_38994();
void Identity_38840();
int KeySchedule_38841_LRotate(int x, int n);
void KeySchedule_38841();
void WEIGHTED_ROUND_ROBIN_Joiner_38995();
void WEIGHTED_ROUND_ROBIN_Splitter_39706();
void Xor_39708();
void Xor_39709();
void Xor_39710();
void Xor_39711();
void Xor_39712();
void Xor_39713();
void Xor_39714();
void Xor_39715();
void Xor_39716();
void Xor_39717();
void Xor_39718();
void Xor_39719();
void Xor_39720();
void Xor_39721();
void Xor_39722();
void Xor_39723();
void Xor_39724();
void WEIGHTED_ROUND_ROBIN_Joiner_39707();
void WEIGHTED_ROUND_ROBIN_Splitter_39725();
void Sbox_39727();
void Sbox_39728();
void Sbox_39729();
void Sbox_39730();
void Sbox_39731();
void Sbox_39732();
void Sbox_39733();
void Sbox_39734();
void Sbox_39735();
void Sbox_39736();
void Sbox_39737();
void Sbox_39738();
void Sbox_39739();
void Sbox_39740();
void Sbox_39741();
void Sbox_39742();
void Sbox_39743();
void WEIGHTED_ROUND_ROBIN_Joiner_39726();
void rawL_38844();
void WEIGHTED_ROUND_ROBIN_Splitter_38996();
void Identity_38847();
int KeySchedule_38848_LRotate(int x, int n);
void KeySchedule_38848();
void WEIGHTED_ROUND_ROBIN_Joiner_38997();
void WEIGHTED_ROUND_ROBIN_Splitter_39744();
void Xor_39746();
void Xor_39747();
void Xor_39748();
void Xor_39749();
void Xor_39750();
void Xor_39751();
void Xor_39752();
void Xor_39753();
void Xor_39754();
void Xor_39755();
void Xor_39756();
void Xor_39757();
void Xor_39758();
void Xor_39759();
void Xor_39760();
void Xor_39761();
void Xor_39762();
void WEIGHTED_ROUND_ROBIN_Joiner_39745();
void WEIGHTED_ROUND_ROBIN_Splitter_39763();
void Sbox_39765();
void Sbox_39766();
void Sbox_39767();
void Sbox_39768();
void Sbox_39769();
void Sbox_39770();
void Sbox_39771();
void Sbox_39772();
void Sbox_39773();
void Sbox_39774();
void Sbox_39775();
void Sbox_39776();
void Sbox_39777();
void Sbox_39778();
void Sbox_39779();
void Sbox_39780();
void Sbox_39781();
void WEIGHTED_ROUND_ROBIN_Joiner_39764();
void rawL_38851();
void WEIGHTED_ROUND_ROBIN_Splitter_38998();
void Identity_38854();
int KeySchedule_38855_LRotate(int x, int n);
void KeySchedule_38855();
void WEIGHTED_ROUND_ROBIN_Joiner_38999();
void WEIGHTED_ROUND_ROBIN_Splitter_39782();
void Xor_39784();
void Xor_39785();
void Xor_39786();
void Xor_39787();
void Xor_39788();
void Xor_39789();
void Xor_39790();
void Xor_39791();
void Xor_39792();
void Xor_39793();
void Xor_39794();
void Xor_39795();
void Xor_39796();
void Xor_39797();
void Xor_39798();
void Xor_39799();
void Xor_39800();
void WEIGHTED_ROUND_ROBIN_Joiner_39783();
void WEIGHTED_ROUND_ROBIN_Splitter_39801();
void Sbox_39803();
void Sbox_39804();
void Sbox_39805();
void Sbox_39806();
void Sbox_39807();
void Sbox_39808();
void Sbox_39809();
void Sbox_39810();
void Sbox_39811();
void Sbox_39812();
void Sbox_39813();
void Sbox_39814();
void Sbox_39815();
void Sbox_39816();
void Sbox_39817();
void Sbox_39818();
void Sbox_39819();
void WEIGHTED_ROUND_ROBIN_Joiner_39802();
void rawL_38858();
void WEIGHTED_ROUND_ROBIN_Splitter_39000();
void Identity_38861();
int KeySchedule_38862_LRotate(int x, int n);
void KeySchedule_38862();
void WEIGHTED_ROUND_ROBIN_Joiner_39001();
void WEIGHTED_ROUND_ROBIN_Splitter_39820();
void Xor_39822();
void Xor_39823();
void Xor_39824();
void Xor_39825();
void Xor_39826();
void Xor_39827();
void Xor_39828();
void Xor_39829();
void Xor_39830();
void Xor_39831();
void Xor_39832();
void Xor_39833();
void Xor_39834();
void Xor_39835();
void Xor_39836();
void Xor_39837();
void Xor_39838();
void WEIGHTED_ROUND_ROBIN_Joiner_39821();
void WEIGHTED_ROUND_ROBIN_Splitter_39839();
void Sbox_39841();
void Sbox_39842();
void Sbox_39843();
void Sbox_39844();
void Sbox_39845();
void Sbox_39846();
void Sbox_39847();
void Sbox_39848();
void Sbox_39849();
void Sbox_39850();
void Sbox_39851();
void Sbox_39852();
void Sbox_39853();
void Sbox_39854();
void Sbox_39855();
void Sbox_39856();
void Sbox_39857();
void WEIGHTED_ROUND_ROBIN_Joiner_39840();
void rawL_38865();
void WEIGHTED_ROUND_ROBIN_Splitter_39002();
void Identity_38868();
int KeySchedule_38869_LRotate(int x, int n);
void KeySchedule_38869();
void WEIGHTED_ROUND_ROBIN_Joiner_39003();
void WEIGHTED_ROUND_ROBIN_Splitter_39858();
void Xor_39860();
void Xor_39861();
void Xor_39862();
void Xor_39863();
void Xor_39864();
void Xor_39865();
void Xor_39866();
void Xor_39867();
void Xor_39868();
void Xor_39869();
void Xor_39870();
void Xor_39871();
void Xor_39872();
void Xor_39873();
void Xor_39874();
void Xor_39875();
void Xor_39876();
void WEIGHTED_ROUND_ROBIN_Joiner_39859();
void WEIGHTED_ROUND_ROBIN_Splitter_39877();
void Sbox_39879();
void Sbox_39880();
void Sbox_39881();
void Sbox_39882();
void Sbox_39883();
void Sbox_39884();
void Sbox_39885();
void Sbox_39886();
void Sbox_39887();
void Sbox_39888();
void Sbox_39889();
void Sbox_39890();
void Sbox_39891();
void Sbox_39892();
void Sbox_39893();
void Sbox_39894();
void Sbox_39895();
void WEIGHTED_ROUND_ROBIN_Joiner_39878();
void rawL_38872();
void WEIGHTED_ROUND_ROBIN_Splitter_39004();
void Identity_38875();
int KeySchedule_38876_LRotate(int x, int n);
void KeySchedule_38876();
void WEIGHTED_ROUND_ROBIN_Joiner_39005();
void WEIGHTED_ROUND_ROBIN_Splitter_39896();
void Xor_39898();
void Xor_39899();
void Xor_39900();
void Xor_39901();
void Xor_39902();
void Xor_39903();
void Xor_39904();
void Xor_39905();
void Xor_39906();
void Xor_39907();
void Xor_39908();
void Xor_39909();
void Xor_39910();
void Xor_39911();
void Xor_39912();
void Xor_39913();
void Xor_39914();
void WEIGHTED_ROUND_ROBIN_Joiner_39897();
void WEIGHTED_ROUND_ROBIN_Splitter_39915();
void Sbox_39917();
void Sbox_39918();
void Sbox_39919();
void Sbox_39920();
void Sbox_39921();
void Sbox_39922();
void Sbox_39923();
void Sbox_39924();
void Sbox_39925();
void Sbox_39926();
void Sbox_39927();
void Sbox_39928();
void Sbox_39929();
void Sbox_39930();
void Sbox_39931();
void Sbox_39932();
void Sbox_39933();
void WEIGHTED_ROUND_ROBIN_Joiner_39916();
void rawL_38879();
void WEIGHTED_ROUND_ROBIN_Splitter_39006();
void Identity_38882();
int KeySchedule_38883_LRotate(int x, int n);
void KeySchedule_38883();
void WEIGHTED_ROUND_ROBIN_Joiner_39007();
void WEIGHTED_ROUND_ROBIN_Splitter_39934();
void Xor_39936();
void Xor_39937();
void Xor_39938();
void Xor_39939();
void Xor_39940();
void Xor_39941();
void Xor_39942();
void Xor_39943();
void Xor_39944();
void Xor_39945();
void Xor_39946();
void Xor_39947();
void Xor_39948();
void Xor_39949();
void Xor_39950();
void Xor_39951();
void Xor_39952();
void WEIGHTED_ROUND_ROBIN_Joiner_39935();
void WEIGHTED_ROUND_ROBIN_Splitter_39953();
void Sbox_39955();
void Sbox_39956();
void Sbox_39957();
void Sbox_39958();
void Sbox_39959();
void Sbox_39960();
void Sbox_39961();
void Sbox_39962();
void Sbox_39963();
void Sbox_39964();
void Sbox_39965();
void Sbox_39966();
void Sbox_39967();
void Sbox_39968();
void Sbox_39969();
void Sbox_39970();
void Sbox_39971();
void WEIGHTED_ROUND_ROBIN_Joiner_39954();
void rawL_38886();
void WEIGHTED_ROUND_ROBIN_Splitter_39008();
void Identity_38889();
int KeySchedule_38890_LRotate(int x, int n);
void KeySchedule_38890();
void WEIGHTED_ROUND_ROBIN_Joiner_39009();
void WEIGHTED_ROUND_ROBIN_Splitter_39972();
void Xor_39974();
void Xor_39975();
void Xor_39976();
void Xor_39977();
void Xor_39978();
void Xor_39979();
void Xor_39980();
void Xor_39981();
void Xor_39982();
void Xor_39983();
void Xor_39984();
void Xor_39985();
void Xor_39986();
void Xor_39987();
void Xor_39988();
void Xor_39989();
void Xor_39990();
void WEIGHTED_ROUND_ROBIN_Joiner_39973();
void WEIGHTED_ROUND_ROBIN_Splitter_39991();
void Sbox_39993();
void Sbox_39994();
void Sbox_39995();
void Sbox_39996();
void Sbox_39997();
void Sbox_39998();
void Sbox_39999();
void Sbox_40000();
void Sbox_40001();
void Sbox_40002();
void Sbox_40003();
void Sbox_40004();
void Sbox_40005();
void Sbox_40006();
void Sbox_40007();
void Sbox_40008();
void Sbox_40009();
void WEIGHTED_ROUND_ROBIN_Joiner_39992();
void rawL_38893();
void WEIGHTED_ROUND_ROBIN_Splitter_39010();
void Identity_38896();
int KeySchedule_38897_LRotate(int x, int n);
void KeySchedule_38897();
void WEIGHTED_ROUND_ROBIN_Joiner_39011();
void WEIGHTED_ROUND_ROBIN_Splitter_40010();
void Xor_40012();
void Xor_40013();
void Xor_40014();
void Xor_40015();
void Xor_40016();
void Xor_40017();
void Xor_40018();
void Xor_40019();
void Xor_40020();
void Xor_40021();
void Xor_40022();
void Xor_40023();
void Xor_40024();
void Xor_40025();
void Xor_40026();
void Xor_40027();
void Xor_40028();
void WEIGHTED_ROUND_ROBIN_Joiner_40011();
void WEIGHTED_ROUND_ROBIN_Splitter_40029();
void Sbox_40031();
void Sbox_40032();
void Sbox_40033();
void Sbox_40034();
void Sbox_40035();
void Sbox_40036();
void Sbox_40037();
void Sbox_40038();
void Sbox_40039();
void Sbox_40040();
void Sbox_40041();
void Sbox_40042();
void Sbox_40043();
void Sbox_40044();
void Sbox_40045();
void Sbox_40046();
void Sbox_40047();
void WEIGHTED_ROUND_ROBIN_Joiner_40030();
void rawL_38900();
void WEIGHTED_ROUND_ROBIN_Splitter_39012();
void Identity_38903();
int KeySchedule_38904_LRotate(int x, int n);
void KeySchedule_38904();
void WEIGHTED_ROUND_ROBIN_Joiner_39013();
void WEIGHTED_ROUND_ROBIN_Splitter_40048();
void Xor_40050();
void Xor_40051();
void Xor_40052();
void Xor_40053();
void Xor_40054();
void Xor_40055();
void Xor_40056();
void Xor_40057();
void Xor_40058();
void Xor_40059();
void Xor_40060();
void Xor_40061();
void Xor_40062();
void Xor_40063();
void Xor_40064();
void Xor_40065();
void Xor_40066();
void WEIGHTED_ROUND_ROBIN_Joiner_40049();
void WEIGHTED_ROUND_ROBIN_Splitter_40067();
void Sbox_40069();
void Sbox_40070();
void Sbox_40071();
void Sbox_40072();
void Sbox_40073();
void Sbox_40074();
void Sbox_40075();
void Sbox_40076();
void Sbox_40077();
void Sbox_40078();
void Sbox_40079();
void Sbox_40080();
void Sbox_40081();
void Sbox_40082();
void Sbox_40083();
void Sbox_40084();
void Sbox_40085();
void WEIGHTED_ROUND_ROBIN_Joiner_40068();
void rawL_38907();
void WEIGHTED_ROUND_ROBIN_Splitter_39014();
void Identity_38910();
int KeySchedule_38911_LRotate(int x, int n);
void KeySchedule_38911();
void WEIGHTED_ROUND_ROBIN_Joiner_39015();
void WEIGHTED_ROUND_ROBIN_Splitter_40086();
void Xor_40088();
void Xor_40089();
void Xor_40090();
void Xor_40091();
void Xor_40092();
void Xor_40093();
void Xor_40094();
void Xor_40095();
void Xor_40096();
void Xor_40097();
void Xor_40098();
void Xor_40099();
void Xor_40100();
void Xor_40101();
void Xor_40102();
void Xor_40103();
void Xor_40104();
void WEIGHTED_ROUND_ROBIN_Joiner_40087();
void WEIGHTED_ROUND_ROBIN_Splitter_40105();
void Sbox_40107();
void Sbox_40108();
void Sbox_40109();
void Sbox_40110();
void Sbox_40111();
void Sbox_40112();
void Sbox_40113();
void Sbox_40114();
void Sbox_40115();
void Sbox_40116();
void Sbox_40117();
void Sbox_40118();
void Sbox_40119();
void Sbox_40120();
void Sbox_40121();
void Sbox_40122();
void Sbox_40123();
void WEIGHTED_ROUND_ROBIN_Joiner_40106();
void rawL_38914();
void WEIGHTED_ROUND_ROBIN_Splitter_39016();
void Identity_38917();
int KeySchedule_38918_LRotate(int x, int n);
void KeySchedule_38918();
void WEIGHTED_ROUND_ROBIN_Joiner_39017();
void WEIGHTED_ROUND_ROBIN_Splitter_40124();
void Xor_40126();
void Xor_40127();
void Xor_40128();
void Xor_40129();
void Xor_40130();
void Xor_40131();
void Xor_40132();
void Xor_40133();
void Xor_40134();
void Xor_40135();
void Xor_40136();
void Xor_40137();
void Xor_40138();
void Xor_40139();
void Xor_40140();
void Xor_40141();
void Xor_40142();
void WEIGHTED_ROUND_ROBIN_Joiner_40125();
void WEIGHTED_ROUND_ROBIN_Splitter_40143();
void Sbox_40145();
void Sbox_40146();
void Sbox_40147();
void Sbox_40148();
void Sbox_40149();
void Sbox_40150();
void Sbox_40151();
void Sbox_40152();
void Sbox_40153();
void Sbox_40154();
void Sbox_40155();
void Sbox_40156();
void Sbox_40157();
void Sbox_40158();
void Sbox_40159();
void Sbox_40160();
void Sbox_40161();
void WEIGHTED_ROUND_ROBIN_Joiner_40144();
void rawL_38921();
void WEIGHTED_ROUND_ROBIN_Splitter_39018();
void Identity_38924();
int KeySchedule_38925_LRotate(int x, int n);
void KeySchedule_38925();
void WEIGHTED_ROUND_ROBIN_Joiner_39019();
void WEIGHTED_ROUND_ROBIN_Splitter_40162();
void Xor_40164();
void Xor_40165();
void Xor_40166();
void Xor_40167();
void Xor_40168();
void Xor_40169();
void Xor_40170();
void Xor_40171();
void Xor_40172();
void Xor_40173();
void Xor_40174();
void Xor_40175();
void Xor_40176();
void Xor_40177();
void Xor_40178();
void Xor_40179();
void Xor_40180();
void WEIGHTED_ROUND_ROBIN_Joiner_40163();
void WEIGHTED_ROUND_ROBIN_Splitter_40181();
void Sbox_40183();
void Sbox_40184();
void Sbox_40185();
void Sbox_40186();
void Sbox_40187();
void Sbox_40188();
void Sbox_40189();
void Sbox_40190();
void Sbox_40191();
void Sbox_40192();
void Sbox_40193();
void Sbox_40194();
void Sbox_40195();
void Sbox_40196();
void Sbox_40197();
void Sbox_40198();
void Sbox_40199();
void WEIGHTED_ROUND_ROBIN_Joiner_40182();
void rawL_38928();
void WEIGHTED_ROUND_ROBIN_Splitter_39020();
void Identity_38931();
int KeySchedule_38932_LRotate(int x, int n);
void KeySchedule_38932();
void WEIGHTED_ROUND_ROBIN_Joiner_39021();
void WEIGHTED_ROUND_ROBIN_Splitter_40200();
void Xor_40202();
void Xor_40203();
void Xor_40204();
void Xor_40205();
void Xor_40206();
void Xor_40207();
void Xor_40208();
void Xor_40209();
void Xor_40210();
void Xor_40211();
void Xor_40212();
void Xor_40213();
void Xor_40214();
void Xor_40215();
void Xor_40216();
void Xor_40217();
void Xor_40218();
void WEIGHTED_ROUND_ROBIN_Joiner_40201();
void WEIGHTED_ROUND_ROBIN_Splitter_40219();
void Sbox_40221();
void Sbox_40222();
void Sbox_40223();
void Sbox_40224();
void Sbox_40225();
void Sbox_40226();
void Sbox_40227();
void Sbox_40228();
void Sbox_40229();
void Sbox_40230();
void Sbox_40231();
void Sbox_40232();
void Sbox_40233();
void Sbox_40234();
void Sbox_40235();
void Sbox_40236();
void Sbox_40237();
void WEIGHTED_ROUND_ROBIN_Joiner_40220();
void rawL_38935();
void WEIGHTED_ROUND_ROBIN_Splitter_39022();
void Identity_38938();
int KeySchedule_38939_LRotate(int x, int n);
void KeySchedule_38939();
void WEIGHTED_ROUND_ROBIN_Joiner_39023();
void WEIGHTED_ROUND_ROBIN_Splitter_40238();
void Xor_40240();
void Xor_40241();
void Xor_40242();
void Xor_40243();
void Xor_40244();
void Xor_40245();
void Xor_40246();
void Xor_40247();
void Xor_40248();
void Xor_40249();
void Xor_40250();
void Xor_40251();
void Xor_40252();
void Xor_40253();
void Xor_40254();
void Xor_40255();
void Xor_40256();
void WEIGHTED_ROUND_ROBIN_Joiner_40239();
void WEIGHTED_ROUND_ROBIN_Splitter_40257();
void Sbox_40259();
void Sbox_40260();
void Sbox_40261();
void Sbox_40262();
void Sbox_40263();
void Sbox_40264();
void Sbox_40265();
void Sbox_40266();
void Sbox_40267();
void Sbox_40268();
void Sbox_40269();
void Sbox_40270();
void Sbox_40271();
void Sbox_40272();
void Sbox_40273();
void Sbox_40274();
void Sbox_40275();
void WEIGHTED_ROUND_ROBIN_Joiner_40258();
void rawL_38942();
void WEIGHTED_ROUND_ROBIN_Splitter_39024();
void Identity_38945();
int KeySchedule_38946_LRotate(int x, int n);
void KeySchedule_38946();
void WEIGHTED_ROUND_ROBIN_Joiner_39025();
void WEIGHTED_ROUND_ROBIN_Splitter_40276();
void Xor_40278();
void Xor_40279();
void Xor_40280();
void Xor_40281();
void Xor_40282();
void Xor_40283();
void Xor_40284();
void Xor_40285();
void Xor_40286();
void Xor_40287();
void Xor_40288();
void Xor_40289();
void Xor_40290();
void Xor_40291();
void Xor_40292();
void Xor_40293();
void Xor_40294();
void WEIGHTED_ROUND_ROBIN_Joiner_40277();
void WEIGHTED_ROUND_ROBIN_Splitter_40295();
void Sbox_40297();
void Sbox_40298();
void Sbox_40299();
void Sbox_40300();
void Sbox_40301();
void Sbox_40302();
void Sbox_40303();
void Sbox_40304();
void Sbox_40305();
void Sbox_40306();
void Sbox_40307();
void Sbox_40308();
void Sbox_40309();
void Sbox_40310();
void Sbox_40311();
void Sbox_40312();
void Sbox_40313();
void WEIGHTED_ROUND_ROBIN_Joiner_40296();
void rawL_38949();
void WEIGHTED_ROUND_ROBIN_Splitter_39026();
void Identity_38952();
int KeySchedule_38953_LRotate(int x, int n);
void KeySchedule_38953();
void WEIGHTED_ROUND_ROBIN_Joiner_39027();
void WEIGHTED_ROUND_ROBIN_Splitter_40314();
void Xor_40316();
void Xor_40317();
void Xor_40318();
void Xor_40319();
void Xor_40320();
void Xor_40321();
void Xor_40322();
void Xor_40323();
void Xor_40324();
void Xor_40325();
void Xor_40326();
void Xor_40327();
void Xor_40328();
void Xor_40329();
void Xor_40330();
void Xor_40331();
void Xor_40332();
void WEIGHTED_ROUND_ROBIN_Joiner_40315();
void WEIGHTED_ROUND_ROBIN_Splitter_40333();
void Sbox_40335();
void Sbox_40336();
void Sbox_40337();
void Sbox_40338();
void Sbox_40339();
void Sbox_40340();
void Sbox_40341();
void Sbox_40342();
void Sbox_40343();
void Sbox_40344();
void Sbox_40345();
void Sbox_40346();
void Sbox_40347();
void Sbox_40348();
void Sbox_40349();
void Sbox_40350();
void Sbox_40351();
void WEIGHTED_ROUND_ROBIN_Joiner_40334();
void WEIGHTED_ROUND_ROBIN_Splitter_39028();
void Identity_38957();
int KeySchedule_38958_LRotate(int x, int n);
void KeySchedule_38958();
void WEIGHTED_ROUND_ROBIN_Joiner_39029();
void WEIGHTED_ROUND_ROBIN_Splitter_40352();
void Xor_40354();
void Xor_40355();
void Xor_40356();
void Xor_40357();
void Xor_40358();
void Xor_40359();
void Xor_40360();
void Xor_40361();
void Xor_40362();
void Xor_40363();
void Xor_40364();
void Xor_40365();
void Xor_40366();
void Xor_40367();
void Xor_40368();
void Xor_40369();
void Xor_40370();
void WEIGHTED_ROUND_ROBIN_Joiner_40353();
void Permute_38960();
void WEIGHTED_ROUND_ROBIN_Splitter_40371();
void BitstoInts_40373();
void BitstoInts_40374();
void BitstoInts_40375();
void BitstoInts_40376();
void BitstoInts_40377();
void BitstoInts_40378();
void BitstoInts_40379();
void BitstoInts_40380();
void BitstoInts_40381();
void BitstoInts_40382();
void BitstoInts_40383();
void BitstoInts_40384();
void BitstoInts_40385();
void BitstoInts_40386();
void BitstoInts_40387();
void BitstoInts_40388();
void BitstoInts_40389();
void WEIGHTED_ROUND_ROBIN_Joiner_40372();
void HexPrinterAux_38963();

#ifdef __cplusplus
}
#endif
#endif
