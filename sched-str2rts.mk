ifeq ($(shell uname), Darwin)
HEPTANE2METHANE=/Users/brouxel/SharedVMs/share/Projects/C-C++/Convert2Methane/dist/convert2Methane
HEPTANE_HOME=/Users/brouxel/SharedVMs/share/Projects/Thesis/heptane-brouxel/
STR2RTS_HOME=/Users/brouxel/SharedVMs/share/Projects/C-C++/str2rts/
METHANE=/Users/brouxel/SharedVMs/share/Projects/C-C++/Methane/dist/methane
else ifeq ($(shell uname), Linux)
    ifneq (,$(filter $(shell hostname), pacacaille.irisa.fr, mamanoel.irisa.fr))
HEPTANE2METHANE=/local/brouxel/Convert2Methane/dist/convert2Methane
HEPTANE_HOME=/local/brouxel/heptane-brouxel/
STR2RTS_HOME=/local/brouxel/str2rts/
METHANE=/udd/brouxel/ILPGen.caille/dist/methane
    else
HEPTANE2METHANE=/media/sf_SharedVMs/share/Projects/C-C++/Convert2Methane/dist/convert2Methane
HEPTANE_HOME=/media/sf_SharedVMs/share/Projects/Thesis/heptane-brouxel/
STR2RTS_HOME=/media/sf_SharedVMs/share/Projects/C-C++/str2rts/
METHANE=/media/sf_SharedVMs/share/Projects/C-C++/Methane/dist/methane
    endif
endif

where-am-i=$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THISSCHEDMAKEFILE:=$(call where-am-i)

# For STR2RTS
BUILD_DIR?=build
TARGET_DIR?=dist-ecrts2018
SRC_DIR=$(STR2RTS_HOME)/src
include $(STR2RTS_HOME)/Makefile

.PHONY:sched-patmos-heptane-methane sched-mips-heptane-methane
sched-patmos-heptane-methane:
	make TARGET=patmos TARGET_SUFFIX="" WCETTOOL=heptane $(APP)
	cp $(SRC_DIR)/patmos-heptane-annot.xml $(TARGET_DIR)/$(APP)-annot.xml
	sed -e 's/X_BENCH/$(APP)/g' $(HEPTANE_HOME)/config_files/configExtract_templatePatmos.xml \
	    | sed -e 's~X_INDIR~$(TARGET_DIR)~' \
	    | sed -e 's~X_RESDIR~$(TARGET_DIR)~' > $(TARGET_DIR)/$(APP)-CExtract.xml
	$(HEPTANE_HOME)/bin/HeptaneExtract -v $(TARGET_DIR)/$(APP)-CExtract.xml > $(TARGET_DIR)/$(APP)-Extract.xml
	sed -e 's/X_BENCH/$(APP)/g' $(HEPTANE_HOME)/config_files/configWCET_templatePatmos.xml \
	    | sed -e "s~X_RESDIR~$(TARGET_DIR)~" > $(TARGET_DIR)/$(APP)-CWCET.xml
	$(HEPTANE_HOME)/bin/HeptaneAnalysis $(TARGET_DIR)/$(APP)-CWCET.xml
	@echo "$(HEPTANE2METHANE) -t $(shell find graphs/ -iname "$(APP).dot") -w $(TARGET_DIR)/$(APP)-IPET.xml -o $(TARGET_DIR)/$(APP)-ilpgen.xml -b 1 -n 16 -m worst_concurrency -u 120 -x fls"
	@echo "Apply some corrections to the $(APP)-ilpgen.xml file if necessary then run "
	@echo "$(METHANE) $(TARGET_DIR)/$(APP)-ilpgen.xml"

sched-mips-heptane-methane:
	make TARGET=mips TARGET_SUFFIX="" WCETTOOL=heptane $(APP)
	#cp $(SRC_DIR)/heptane-annot.xml $(TARGET_DIR)/$(APP)-annot.xml
	echo "<?xml version=\"1.0\" ?>" > $(TARGET_DIR)/$(APP)-annot.xml
	echo "<ANNOT/>" >> $(TARGET_DIR)/$(APP)-annot.xml
	sed -e 's/X_BENCH/$(APP)/g' $(HEPTANE_HOME)/config_files/configExtract_templateStreamit.xml \
	    | sed -e 's~X_INDIR~$(TARGET_DIR)~' \
	    | sed -e 's~X_RESDIR~$(TARGET_DIR)~' > $(TARGET_DIR)/$(APP)-CExtract.xml
	$(HEPTANE_HOME)/bin/HeptaneExtract $(TARGET_DIR)/$(APP)-CExtract.xml
	sed -e 's/X_BENCH/$(APP)/g' $(HEPTANE_HOME)/config_files/configWCET_templateStreamit.xml \
	    | sed -e "s~X_RESDIR~$(TARGET_DIR)~" > $(TARGET_DIR)/$(APP)-CWCET.xml
	$(HEPTANE_HOME)/bin/HeptaneAnalysis $(TARGET_DIR)/$(APP)-CWCET.xml
	$(HEPTANE2METHANE) --streamit $(shell find src/ -iname "$(APP).dot") --heptane $(TARGET_DIR)/$(APP)-IPET.xml -o $(TARGET_DIR)/$(APP)-ilpgen.xml \
	    -b 1 -n 2,4,8,12,16 -t 3 -m worst_concurrency -u 120 -x fls -d sharedonly -z blocking -s 4kb:1:4kb 
	#@echo "Apply some corrections to the $(APP)-ilpgen.xml file if necessary then run "
	#@echo "$(METHANE) $(TARGET_DIR)/$(APP)-ilpgen.xml"

define SCHED_MIPS_TEMPLATE
#NOTE: I didn't use a SUFFIX when compiling the executable
.PHONY:$(strip $(1))-mips-sched
$(strip $(1))-mips-sched: $$(BUILD_DIR)/$(strip $(1))-conv-worst-bl-sho.lock $$(BUILD_DIR)/$(strip $(1))-conv-sync-bl-sho-none.lock $$(BUILD_DIR)/$(strip $(1))-conv-contaw-bl-sho.lock \
	$$(BUILD_DIR)/$(strip $(1))-conv-worst-nbl-sho.lock $$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sho-none.lock $$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sho-edge.lock $$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sho-packet.lock $$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sho-flit.lock $$(BUILD_DIR)/$(strip $(1))-conv-contaw-nbl-sho.lock \
	#$$(BUILD_DIR)/$(strip $(1))-conv-worst-nbl-sha.lock $$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sha.lock $$(BUILD_DIR)/$(strip $(1))-conv-contaw-nbl-sha.lock $$(BUILD_DIR)/$(strip $(1))-conv-worst-bl-sha.lock $$(BUILD_DIR)/$(strip $(1))-conv-sync-bl-sha.lock $$(BUILD_DIR)/$(strip $(1))-conv-contaw-bl-sha.lock

$$(BUILD_DIR)/$(strip $(1))-conv-worst-bl-sho.lock : $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m worst_concurrency -u 120 -x fls -d sharedonly -z blocking -s 4KB,2MB --spm_store_code true -p worst-bl-sho-none && touch $$@

$$(BUILD_DIR)/$(strip $(1))-conv-sync-bl-sho-none.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m test -u 120 -x splitfls -d sharedonly -z blocking -s 4KB,2MB --spm_store_code true -p sync-bl-sho-none --burst none  && touch $$@

$$(BUILD_DIR)/$(strip $(1))-conv-contaw-bl-sho.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m contention_aware -u 120 -x fls -d sharedonly -z blocking -s 4KB,2MB --spm_store_code true -p contaw-bl-sho-none && touch $$@

$$(BUILD_DIR)/$(strip $(1))-conv-worst-nbl-sho.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m worst_concurrency -u 120 -x fls -d sharedonly -z non-blocking -s 4KB,2MB --spm_store_code true -p worst-nbl-sho-none && touch $$@

$$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sho-none.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m test -u 120 -x splitfls -d sharedonly -z non-blocking -s 4KB,2MB --spm_store_code true -p sync-nbl-sho-none && touch $$@

$$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sho-edge.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m test -u 120 -x splitfls -d sharedonly -z non-blocking -s 4KB,2MB --spm_store_code true -p sync-nbl-sho-edge && touch $$@

$$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sho-packet.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m test -u 120 -x splitfls -d sharedonly -z non-blocking -s 4KB,2MB --spm_store_code true -p sync-nbl-sho-packet && touch $$@

$$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sho-flit.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m test -u 120 -x splitfls -d sharedonly -z non-blocking -s 4KB,2MB --spm_store_code true -p sync-nbl-sho-flit && touch $$@

$$(BUILD_DIR)/$(strip $(1))-conv-contaw-nbl-sho.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
	    -b 1 -n 2,4,8,12,16 -t 3 -m contention_aware -u 120 -x fls -d sharedonly -z non-blocking -s 4KB,2MB --spm_store_code true -p contaw-nbl-sho-none && touch $$@
#$$(BUILD_DIR)/$(strip $(1))-conv-worst-nbl-sha.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
#	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
#	    -b 1 -n 2,4,8,12,16 -t 3 -m worst_concurrency -u 120 -x fls -d shared -z non-blocking -s 4KB,2MB --spm_store_code true -p worst-nbl-sha && touch $$@
#$$(BUILD_DIR)/$(strip $(1))-conv-sync-nbl-sha.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
#	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
#	    -b 1 -n 2,4,8,12,16 -t 3 -m synchronized -u 120 -x fls -d shared -z non-blocking -s 4KB,2MB --spm_store_code true -p sync-nbl-sha && touch $$@
#$$(BUILD_DIR)/$(strip $(1))-conv-contaw-nbl-sha.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
#	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
#	    -b 1 -n 2,4,8,12,16 -t 3 -m contention_aware -u 120 -x fls -d shared -z non-blocking -s 4KB,2MB --spm_store_code true -p contaw-nbl-sha && touch $$@
#$$(BUILD_DIR)/$(strip $(1))-conv-worst-bl-sha.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
#	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
#	    -b 1 -n 2,4,8,12,16 -t 3 -m worst_concurrency -u 120 -x fls -d shared -z blocking -s 4KB,2MB --spm_store_code true -p worst-bl-sha && touch $$@
#$$(BUILD_DIR)/$(strip $(1))-conv-sync-bl-sha.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
#	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
#	    -b 1 -n 2,4,8,12,16 -t 3 -m synchronized -u 120 -x fls -d shared -z blocking -s 4KB,2MB --spm_store_code true -p sync-bl-sha && touch $$@
#$$(BUILD_DIR)/$(strip $(1))-conv-contaw-bl-sha.lock: $$(BUILD_DIR)/$(strip $(1))-IPET.xml
#	$(HEPTANE2METHANE) --streamit $$(shell find $(SRC_DIR) -iname "$$$$(echo $(strip $(1)).dot | sed 's/sdf_/sdf-/' | sed 's/\(peg[0-9]*\)_/\1-/' )") --heptane $$(BUILD_DIR)/$(strip $(1))-IPET.xml -o $$(TARGET_DIR)/ \
#	    -b 1 -n 2,4,8,12,16 -t 3 -m contention_aware -u 120 -x fls -d shared -z blocking -s 4KB,2MB --spm_store_code true -p contaw-bl-sha && touch $$@

$$(BUILD_DIR)/$(strip $(1))-IPET.xml: $$(BUILD_DIR)/$(strip $(1))-CWCET.xml $$(BUILD_DIR)/$(strip $(1)).xml.lock
	$(HEPTANE_HOME)/bin/HeptaneAnalysis $$(BUILD_DIR)/$(strip $(1))-CWCET.xml
	
$$(BUILD_DIR)/$(strip $(1)).xml.lock:$$(BUILD_DIR)/$(strip $(1))-annot.xml $$(BUILD_DIR)/$(strip $(1))-CExtract.xml
	$(HEPTANE_HOME)/bin/HeptaneExtract $$(BUILD_DIR)/$(strip $(1))-CExtract.xml
	touch $$(BUILD_DIR)/$(strip $(1)).xml.lock

$$(BUILD_DIR)/$(strip $(1))-annot.xml:
	echo "<?xml version=\"1.0\" ?>" > $$@
	echo "<ANNOT/>" >> $$@

$$(BUILD_DIR)/$(strip $(1))-CExtract.xml: 
	sed -e 's/X_BENCH/$(strip $(1))/g' $(HEPTANE_HOME)/config_files/configExtract_templateStreamit.xml \
	    | sed -e 's~X_INDIR~$$(BUILD_DIR)~' \
	    | sed -e 's~X_RESDIR~$$(BUILD_DIR)~' > $$@

$$(BUILD_DIR)/$(strip $(1))-CWCET.xml:
	sed -e 's/X_BENCH/$(strip $(1))/g' $(HEPTANE_HOME)/config_files/configWCET_templateStreamit.xml \
	    | sed -e "s~X_RESDIR~$$(BUILD_DIR)~" > $$@
endef

$(foreach b, $(BENCHMARKS), $(eval $(call SCHED_MIPS_TEMPLATE, $(b))))

sched-all-mips:
	mkdir -p $(TARGET_DIR) $(BUILD_DIR)
	$(MAKE) -C $(STR2RTS_HOME) -f $(STR2RTS_HOME)/Makefile -$(MAKEFLAGS) TARGET_DIR=$(CURDIR)/$(BUILD_DIR) CC=mips-gcc TARGET_SUFFIX="" WCETTOOL=heptane HEPTANE_HOME=$(HEPTANE_HOME) all-mips
	$(MAKE) -f $(THISSCHEDMAKEFILE) -$(MAKEFLAGS) $(BENCHMARKS:=-mips-sched)

CFARS=cfar sdf_cfartest_nocache hsdf_cfartest_nocache sdf_cfartest hsdf_cfartest $(foreach b, $(shell ls $(SRC_DIR)/cfar/PEGs/PEG*.h), $(shell echo $$(basename -s'.h' $(b)) | tr A-Z a-z | tr - _))
all_cfar-mips-sched: 
	mkdir -p $(TARGET_DIR) $(BUILD_DIR)
	$(MAKE) -C $(STR2RTS_HOME) -f $(STR2RTS_HOME)/Makefile -$(MAKEFLAGS) TARGET=mips TARGET_DIR=$(CURDIR)/$(BUILD_DIR) CC=mips-gcc TARGET_SUFFIX="" WCETTOOL=heptane HEPTANE_HOME=$(HEPTANE_HOME) all_cfar
	$(MAKE) -f $(THISSCHEDMAKEFILE) -$(MAKEFLAGS) $(CFARS:=-mips-sched)
	
_80211a=sdf_transmit_nocache hsdf_transmit_nocache sdf_transmit hsdf_transmit $(foreach b, $(shell ls $(SRC_DIR)/802.11a/PEGs/PEG*.h), $(shell echo $$(basename -s'.h' $(b)) | tr A-Z a-z | tr - _))
all_80211a-mips-sched: 
	mkdir -p $(TARGET_DIR) $(BUILD_DIR)
	$(MAKE) -C $(STR2RTS_HOME) -f $(STR2RTS_HOME)/Makefile -$(MAKEFLAGS) TARGET=mips TARGET_DIR=$(CURDIR)/$(BUILD_DIR) CC=mips-gcc TARGET_SUFFIX="" WCETTOOL=heptane HEPTANE_HOME=$(HEPTANE_HOME) all_80211a
	$(MAKE) -f $(THISSCHEDMAKEFILE) -$(MAKEFLAGS) $(_80211a:=-mips-sched)