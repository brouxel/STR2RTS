Please consider using the above bibtex to refer to us in your paper when using our work:
<code>
@inproceedings{rouxel2017str2rts,
  title={STR2RTS: Refactored StreamIT benchmarks into statically analysable parallel benchmarks for WCET estimation \\\& real-time scheduling},
  author={Rouxel, Benjamin and Puaut, Isabelle},
  booktitle={OASIcs-OpenAccess Series in Informatics},
  volume={57},
  year={2017},
  organization={Schloss Dagstuhl-Leibniz-Zentrum fuer Informatik}
}
</code>

Abstract
===========
We all had quite a time to find non-proprietary architecture-independent exploitable parallel
benchmarks for Worst-Case Execution Time (WCET) estimation and real-time scheduling. No
consensus on a parallel benchmark suite seems to exist as from the mono-core era the Mälardalen
benchmark suite [1] imposed itself as a de facto standard. This project bridges part of this
gap, by presenting a collection of benchmarks with the following good properties: (i) easily
analyzable by static WCET estimation tools (written in structured C language, in particular
neither goto nor dynamic memory allocation, containing flow information such as loop bound);
(ii) independent from any particular run-time system (MPI, OpenMP) or real-time operating
system. Each benchmark is composed of the C source code of its tasks, and an XML description
describing the structure of the application (tasks and amount of data exchanged between them
when applicable).  Each benchmark can be integrated in a full end-to-end empirical method
validation protocol on multi-core architecture. This proposed collection of benchmarks is derived
from the well known StreamIT [3] benchmark suite and will be integrated in the TACleBench
suite [2].

----
[1] Jan Gustafsson, Adam Betts, Andreas Ermedahl, and Björn Lisper.  The Mälardalen WCET 
benchmarks – past, present and future. pages 137–147, Brussels, Belgium, July 2010. OCG.

[2] Syed Muhammad Zeeshan Iqbal, Yuchen Liang, and Hakan Grahn. Parmibench-an open-source 
benchmark for embedded multiprocessor systems. IEEE Computer Architecture Letters, 9(2):45–48, 2010

[3] William Thies, Michal Karczmarek, and Saman Amarasinghe. Streamit: A language for
streaming applications. In Compiler Construction, pages 179–196. Springer, 2002.

Usage
===========
To have the list of compilable benchmarks:

<code>$ make</code>

To compile Audiobeam pre-configured with 15mics for your host machine:

<code>$ make audiobeam_15mics</code>

To compile Audiobeam pre-configured with 15mics for MIPS32 architecture:

<code>$ make TARGET=mips audiobeam_15mics</code>

&nbsp;&nbsp;&nbsp;&nbsp;... or modify the default value for target in config.mk

&nbsp;&nbsp;&nbsp;&nbsp;This will create an executable "audiobeam_15mics-mips" in directory "dist/". 

<code>$ make WCETTOOL=heptane TARGET=mips audiobeam_15mics</code>

To compile Audiobeam pre-configured with 15mics for MIPS32 architecture with loop
bounds annotations for the WCET analysis tool Heptane.

See "Makefile" for available compilation configuration.

Description
===========
Each benchmark is divided in different files:
- XML : the structure of the application
- C and its header : the source-code of the application
- Graphivz Dot : visual representation of the application's task graph
- bench.mk : configuration to compile the benchmark source-code using make

XML Description
---------------
- tag "appl" : root of the XML tree
- tag "tasks" : start element of the tasks' list
- tag "task" : start element of a task
    + attribute "id" : unique identifier of the task and correspond to the name of a C function present
                    in the source file. If at least 2 tasks refer to the same C function, their 
                    identifier is suffixed by "__nodeX" where "X" is a positiv integer.
    + attribute "WCET" : Worst-Case Execution Time of the C function, estimated in isolation by our tool
                        Heptane. Roughly it estimates the WCET of the instructions' listing in 
                        MIPS32 assembly.
- tag "prev" : start element to refer to a predecessor of a task
    + attribute "id" : unique identifier of the preceding task
    + attribute "data-sent" : amount of data transmitted by the predecessor
    + attribute "data-type" : type of the data transmitted (char/short/int/float/...)
- tag "config" : start element where we place configuration for our scheduler Methane
- tab "memory" : start element for configuration specific to memory stuff
- tag "data-type" : describe a user-defined data type
    + atttribute "id" : unique identifier of the data type
    + attribute "size" : size in memory of the data type as returned by the POSIX function "sizeof"

### Example:
<code>&lt;?xml version="1.0" encoding="UTF-8" ?>
&lt;appl>
    &lt;config>
        &lt;memory>
            &lt;data-type id="complex" size="16 B"/>
        &lt;/memory>
    &lt;/config>
    &lt;tasks>
        &lt;task id="start_point" WCET="42" />
        &lt;task id="dummytask__node1" WCET="12">
            &lt;prev id="start_point" data-sent="1" data-type="char" />
        &lt;/task>
        &lt;task id="dummytask__node2" WCET="30">
            &lt;prev id="start_point" data-sent="5" data-type="float" />
        &lt;/task>
        &lt;task id="endpoint" WCET="10">
            &lt;prev id="dummytask__node1" data-sent="0" />
            &lt;prev id="dummytask__node1" data-sent="1" data-type="complex" />
        &lt;/task>
    &lt;/tasks>
&lt;/appl>
</code>

Source-code Description
-----------------------
The provided source code has been extracted from the ".str" file provided by the StreamIT
benchmark collection. Most of it has been hand-rewritten to fit WCET analysis requirements.
Tasks are identified and placed in different function where the name is used to identified them
in the XML description. 

Original StreamIT benchmarks can be configured by changing some variables values inside the ".str" 
file. These variables are available in the header file of the bench as "#define" directives.
However, we do not encourage you to modify those values as it can result in changes in the
parallel version of the application (new task could be added in the task graph, or some could
be removed), thus de-synchronizing the XML description and the Dot file from the source code.

Tasks communicate by pushing/popping data-tokens through FIFO channel. A version of those 
function are available in the file "comm_shared_buffer.c" linked at compile time. This version
can be used in conjunction with the sequential main of the benchmark. We encourage you to
develop your own implementation of those functions depending on your parallel architecture
requirements for a parallel version of the benchmark.

For input/output capabilities, the file "global.c" provides an implementation for reading/writing
from/to files. This file is also link at compile time, and should be modified depending on the
targeted platform/OS/... .

To provide WCET-friendly source code, loop bounds must be given for each loop. This information 
can be provided in several ways. With this idea in mind, we do use macros to code loops. 
Those macros are described in the file "globals.h". Depending on the given "CFLAGS" at compilation
time, the C pre-processor will expand those macros with the correct notation. As example,
we provide support for our WCET analysis tool Heptane which uses specific sections in the generated
object file, and we support TACleBench notations by adding pragmas.

Compilation
-----------
The file "bench.mk" contains the configuration for the benchmark. In its simple version it just
needs to call the "BENCHMARK_TEMPLATE" rule defined in the config.mk.

Graphviz Dot file
-----------------
The Dot file allows to display the task graph of the benchmark. It has been extracted from
the StreamIT benchmark suite with the provided tools by StreamIT with some hand-made modification
to ease its exploitation.



